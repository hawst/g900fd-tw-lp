.class Lcom/vlingo/midas/CoreResourceProviderImpl;
.super Lcom/vlingo/core/internal/BaseResourceIdProvider;
.source "CoreResourceProviderImpl.java"

# interfaces
.implements Lcom/vlingo/core/internal/ResourceIdProvider;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vlingo/core/internal/BaseResourceIdProvider;-><init>()V

    .line 15
    invoke-direct {p0}, Lcom/vlingo/midas/CoreResourceProviderImpl;->initUrls()V

    .line 16
    return-void
.end method

.method private initUrls()V
    .locals 2

    .prologue
    .line 382
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_NAME_DEFAULT_CN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 384
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_DEFAULT_CN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 385
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_HOME_DEFAULT_CN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 391
    :goto_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_BING_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 392
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BING_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_BING_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 393
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_YAHOO_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 394
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_YAHOO_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_YAHOO_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 395
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_BAIDU_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 396
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_BAIDU_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_BAIDU_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 397
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT_US:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 398
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_GOOGLE_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 399
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_GOOGLE_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 400
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_NAVER_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 401
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_NAVER_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_NAVER_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 402
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_DAUM_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 403
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DAUM_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_DAUM_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 404
    return-void

    .line 387
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_NAME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_NAME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 388
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 389
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_util_WEB_SEARCH_URL_HOME_DEFAULT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->util_WEB_SEARCH_URL_HOME_DEFAULT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected initArrayMap()V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->languages_iso_cn:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 35
    :goto_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_monthMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->monthMatchers:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 36
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_dayMatchers:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->dayMatchers:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 37
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_phenomenon:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->weather_phenomenon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 38
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_force:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->weather_wind_force:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 39
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_weather_wind_direction:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->weather_wind_direction:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 40
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChinesePhone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->languages_names_cn:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 45
    :goto_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_russian_weekdays:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->core_russian_weekdays:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    .line 46
    return-void

    .line 33
    :cond_0
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_iso:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->languages_iso:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    goto :goto_0

    .line 43
    :cond_1
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$array;->core_languages_names:Lcom/vlingo/core/internal/ResourceIdProvider$array;

    sget v1, Lcom/vlingo/midas/R$array;->languages_names:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putArray(Lcom/vlingo/core/internal/ResourceIdProvider$array;Ljava/lang/Integer;)V

    goto :goto_1
.end method

.method protected initDrawableMap()V
    .locals 2

    .prologue
    .line 376
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_twitter_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    sget v1, Lcom/vlingo/midas/R$drawable;->twitter_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;Ljava/lang/Integer;)V

    .line 377
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    sget v1, Lcom/vlingo/midas/R$drawable;->facebook_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;Ljava/lang/Integer;)V

    .line 378
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_weibo_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    sget v1, Lcom/vlingo/midas/R$drawable;->weibo_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;Ljava/lang/Integer;)V

    .line 379
    return-void
.end method

.method protected initIdMap()V
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$id;->core_text:Lcom/vlingo/core/internal/ResourceIdProvider$id;

    sget v1, Lcom/vlingo/midas/R$id;->text:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putId(Lcom/vlingo/core/internal/ResourceIdProvider$id;Ljava/lang/Integer;)V

    .line 21
    return-void
.end method

.method protected initLayoutMap()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method protected initRawMap()V
    .locals 2

    .prologue
    .line 408
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_start:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 409
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_start:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 410
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_stop:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 411
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_drv:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_stop:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 412
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_start_bt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 413
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone_bt:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->new_voice_stop_bt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 414
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_processing_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    sget v1, Lcom/vlingo/midas/R$raw;->voice_processing:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putRaw(Lcom/vlingo/core/internal/ResourceIdProvider$raw;Ljava/lang/Integer;)V

    .line 415
    return-void
.end method

.method protected initStringMap()V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SMS_SENT_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SMS_SENT_CONFIRM_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 51
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_subject:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_subject:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 52
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_new_sms_from:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_new_sms_from:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 53
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alert_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_alert_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 54
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_NO_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SCHEDULE_NO_EVENTS:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 55
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_all_day:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_schedule_all_day:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 56
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_schedule_to:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_schedule_to:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 57
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_event_saved:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 58
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_EVENT_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_EVENT_SAY_TITLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 60
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_ARTISTMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 61
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ARTIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_ARTIST_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 62
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ALBUMMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_ALBUMMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 63
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_ALBUM_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_ALBUM_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 64
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_TITLEMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_TITLEMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 65
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TITLE_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_TITLE_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 66
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MUSICMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_MUSICMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 67
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MUSIC_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_MUSIC_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 68
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_ANYMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_ANYMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 70
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 71
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_twitter_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 72
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_util_loading:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 73
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_wcis_social_twitter:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 74
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_facebook_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 75
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_facebook:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 76
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_facebook:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_wcis_social_facebook:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 77
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_twitter_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 78
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_twitter:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_twitter:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 79
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_safereader_hidden_message_body:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_safereader_hidden_message_body:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 80
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wcis_social_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_wcis_social_weibo:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 82
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_weibo_err_login:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 83
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_weibo_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 84
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_weibo_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_weibo_update_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 85
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_login_to_weibo_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 86
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_weibo:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 87
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_logout_weibo_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_logout_weibo_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 89
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_status_updated:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_status_updated:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 90
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_network:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_no_network:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 91
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_no_status:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_no_status:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 92
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_status_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_social_status_prompt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 93
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_message_empty_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_message_empty_tts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 94
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NAVIGATE_TO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NAVIGATE_TO:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 95
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_MAP_OF:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_MAP_OF:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 96
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_GOTO_URL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_GOTO_URL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 97
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_MESSAGE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_MATCH_DEMAND_MESSAGE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 98
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventTitleTimeDetailBrief:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventTitleTimeDetailBrief:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 99
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventYouHave:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 101
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_multiple_phone_numbers:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 102
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_multiple_phone_numbers2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 103
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_applications:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_multiple_applications:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 104
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_opening_app:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_opening_app:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 106
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_general:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_general:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 107
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_with_locatin:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_with_locatin:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 108
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_default_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 109
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_plus_seven:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_plus_seven:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 110
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_no_results:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 111
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_current:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_current:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 112
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_date_tts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 113
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weather_date_display:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weather_date_display:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 114
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tomorrow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tomorrow:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 115
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_today:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 117
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_CALLING_CONFIRM_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_VD_CALLING_CONFIRM_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 118
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_default_alarm_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_default_alarm_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 120
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_redial:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_redial:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 122
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_single_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_single_contact:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 123
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_required_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_local_required_engine_name:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 124
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_local_fallback_engine_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_local_fallback_engine_name:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 125
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_update_failed:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_update_failed:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 126
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_contacts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_multiple_contacts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 128
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_home_phone_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 129
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_work_phone_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 130
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_mobile_phone_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_mobile_phone_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 131
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_home_address_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 132
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_address_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_work_address_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 133
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_home_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_home_email_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 134
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_work_email_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_work_email_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 135
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_email_not_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 136
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_address_not_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 137
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_phone_number_not_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 138
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_birthday_not_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_birthday_not_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 139
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_memo_saved:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 140
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_memo_saved_about:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_memo_saved_about:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 142
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_service_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_social_service_prompt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 143
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_enabled:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_enabled:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 144
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_FROM_BODY_NO_PROMPT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 145
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_multiple_found:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_memo_multiple_found:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 146
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_MATCH_DEMAND_CALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_MATCH_DEMAND_CALL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 147
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MSG_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 148
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MSG_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 149
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 150
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 151
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 152
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 153
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SHOWN_NOCALL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 154
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 155
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_NOCALL_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 156
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 157
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_SHOWN_NOCALL_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 158
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_MESSAGE_BODY_READOUT_SPOKEN_NOCALL:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 159
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NO_REPLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NO_REPLY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 161
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveToday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventYouHaveToday:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 162
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventYouHaveOnDate:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventYouHaveOnDate:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 163
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_help_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_error_help_tts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 164
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_error_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_error_sending:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 165
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_speak_msg_tts:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_speak_msg_tts:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 166
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_VD_MULTIPLE_CONTACTS_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 167
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_VD_MULTIPLE_TYPES_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_VD_MULTIPLE_TYPES_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 168
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_no_match_openquote:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contacts_no_match_openquote:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 169
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_phone:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_phone:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 170
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_email:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 171
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_address:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 172
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_birthday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_birthday:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 173
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_phone_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_phone_type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 174
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_address_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_address_type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 175
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_do_not_email_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_do_not_email_type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 176
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_phone:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contacts_do_not_phone:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 177
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contacts_do_not_email:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 178
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contacts_do_not_address:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 179
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contacts_do_not_birthday:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contacts_do_not_birthday:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 180
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_nav_home_prompt_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 181
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_nav_home_prompt_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_nav_home_prompt_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 182
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_navigate_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_navigate_home:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 183
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_notif_reading_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 184
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_reading_ticker:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_notif_reading_ticker:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 185
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_notif_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 186
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safereader_notif_title_silent:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safereader_notif_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 187
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_answer_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_answer_prompt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 188
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_err_auth1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 189
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_err_auth2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_err_auth2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 190
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_err_login:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_twitter_err_login:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 191
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_api_twitter_update_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_api_twitter_update_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 192
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 193
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_FROM_INTRO:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 194
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_MULTI_SMS_FROM_INTRO:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 195
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_too_long:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_too_long:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 196
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_facebook_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_login_to_facebook_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 197
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_network_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_login_to_network_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 198
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_login_to_twitter_msg:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_login_to_twitter_msg:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 199
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_err_msg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 200
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_social_err_msg2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_social_err_msg2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 201
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_safe_reader_default_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_safe_reader_default_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 202
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_task_save_cancel_update_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_task_save_cancel_update_prompt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 203
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SCHEDULE_EVENTS:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SCHEDULE_EVENTS:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 204
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_SAY_TITLE:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_TASK_SAY_TITLE:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 205
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_LAUNCHAPP_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_LAUNCHAPP_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 206
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_APPMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 207
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND_FOR_CAMERA_ONLY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 208
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_SPOKEN_APPMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 209
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_event_save_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_event_save_confirm:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 210
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SHOWN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 211
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_NEW_SMS_CMD_READ_SPOKEN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 212
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_WHICH_CONTACT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_WHICH_CONTACT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 213
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_final_prompt:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_social_final_prompt:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 214
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_text_who:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_text_who:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 215
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_TASK_CANCELLED:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_TASK_CANCELLED:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 216
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_NO_PLAYLISTMATCH_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_NO_PLAYLISTMATCH_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 217
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_sms_send_confirm:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_sms_send_confirm:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 218
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_who_would_you_like_to_call:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_who_would_you_like_to_call:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 219
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_PLAYPLAYLIST_PROMPT_DEMAND:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 220
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voice_recognition_service_not_thru_iux_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_voice_recognition_service_not_thru_iux_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 221
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_voicevideodial_call_name:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 222
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicevideodial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_voicevideodial_call_name_type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 223
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_voicedial_call_name:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 224
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_voicedial_call_name_type:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_voicedial_call_name_type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 225
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_multiple_phone_numbers2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_multiple_phone_numbers2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 226
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_say_command:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_say_command:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 227
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_confirm_delete:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_memo_confirm_delete:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 228
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_memo_not_saved:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_memo_not_saved:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 229
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_tts_SAFEREADER_MULTIPLE_NEW_MESSAGES:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 230
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_more:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_qa_more:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 231
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_qa_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_qa_tts_NO_ANS_WEB_SEARCH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 232
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_WEB_SEARCH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 233
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_WEB_SEARCH_1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 234
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_WEB_SEARCH_2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 235
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_WEB_SEARCH_3:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 236
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_WEB_SEARCH_4:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_WEB_SEARCH_4:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 237
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_tts_NO_ANS_GOOGLE_NOW_SEARCH:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_tts_NO_ANS_GOOGLE_NOW_SEARCH:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 238
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_call_log:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_call_log:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 239
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_alarm_set:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_alarm_set:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 240
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_no_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 241
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 242
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_provider_dianping:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_provider_dianping:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 243
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_default_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_default_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 244
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_bad_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 245
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_bad_response:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_bad_response:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 246
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_localsearch_no_results:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_localsearch_no_results:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 247
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_search_web_label_button:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_search_web_label_button:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 248
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_social_prompt_ex1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_social_prompt_ex1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 249
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_phone_in_use:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 250
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_mic_in_use:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_mic_in_use:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 251
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_what_is_the_weather_date_var:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 252
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_what_is_the_weather_today:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 253
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_today_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_what_is_the_weather_today_location_var:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 254
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_what_is_the_weather_date_var_location_var:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_what_is_the_weather_date_var_location_var:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 255
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_local_search_blank_request_message_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 256
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_local_search_blank_request_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_local_search_blank_request_message_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 257
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_address_book_is_empty:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_address_book_is_empty:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 258
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventOneYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventOneYouHave:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 259
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventRussianTwoThreeFourYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventRussianTwoThreeFourYouHave:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 260
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_checkEventJapaneseMoreTenYouHave:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_checkEventJapaneseMoreTenYouHave:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 261
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_network_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_network_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 262
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_dot:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_dot:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 263
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_comma:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_comma:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 264
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_space:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_space:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 265
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_colon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_colon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 266
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_semicolon:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_semicolon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 267
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_ellipsis:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_ellipsis:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 268
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_sending:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 269
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_sending_readout:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_sending_readout:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 270
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_preface:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_reading_preface:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 271
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_reading_none:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 272
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_single_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_reading_single_from_sender:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 273
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_multi_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_reading_multi_from_sender:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 274
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_current_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 275
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_wifi_setting_change_on_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_wifi_setting_change_on_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 276
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_playing_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_playing_music:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 277
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_music:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_music_play_music:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 278
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_music_play_playlist:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_music_play_playlist:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 279
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_not_detected_current_location:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_not_detected_current_location:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 280
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_unknown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_unknown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 281
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_AM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_format_time_AM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 282
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_format_time_PM:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_format_time_PM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 283
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 284
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_permission_internet_error:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_permission_internet_error:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 286
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_reading_none_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_reading_none:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 287
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_none_from_sender:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 288
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_message_none_from_sender_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_message_none_from_sender_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 289
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_verbose:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_more_messages_verbose:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 290
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_regular:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_more_messages_regular:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 291
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_no_more_messages_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_no_more_messages_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 292
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_no_match_found_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 293
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_no_match_found_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_no_match_found_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 294
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 295
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 296
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 297
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 298
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 299
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 300
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 301
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 302
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 303
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 304
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 305
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 306
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 307
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 308
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 309
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_overflow:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 310
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 311
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_info_verbose_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 312
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_info_verbose_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_info_verbose_overflow_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 313
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_command_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_command_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 314
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_dm_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_dm_overflow:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 315
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_overflow_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 316
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_overflow_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 317
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_sender_overflow:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_sender_overflow:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 318
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_new_messages_for_russian_1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 319
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_2:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_new_messages_for_russian_2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 320
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_new_messages_for_russian_3:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_new_messages_for_russian_3:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 321
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden_nocall:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 322
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden_nocall_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 323
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_hidden_nocall_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 324
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_nocall_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 325
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_initial_nocall_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 326
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next_nocall:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 327
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next_nocall_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 328
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_next_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_next_nocall_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 329
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_nocall:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 330
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_nocall_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 331
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_single_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_single_message_nocall_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 332
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_shown:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_nocall_shown:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 333
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_nocall_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 334
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_readout_multi_message_nocall_withoutaskingmsg_spoken:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 336
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_maximum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->volume_at_maximum:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 337
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_is_at_minimum:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->volume_at_minimum:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 338
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_down:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->volume_down:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 339
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_volume_up:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->volume_up:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 341
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_and:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_and:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 342
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_cradle_date:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_cradle_date:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 344
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_redial_confirm_number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 345
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_redial_confirm_contact:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_redial_confirm_contact:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 346
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_weibo_dialog_title:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_weibo_dialog_title:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 347
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_phone_number:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_phone_number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 348
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_email:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_email:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 349
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_contact_address:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_contact_address:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 351
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_time_at_present:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_time_at_present:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 353
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_enable:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 354
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_disable:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 355
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_enable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_already_enable:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 356
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_already_disable:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_car_already_disable:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 358
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 359
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->playlists_quicklist_default_value1:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->playlists_quicklist_default_value1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 362
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_home:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_phone_type_home:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 363
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_mobile:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_phone_type_mobile:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 364
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_other:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_phone_type_other:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 365
    sget-object v0, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_phone_type_work:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    sget v1, Lcom/vlingo/midas/R$string;->core_phone_type_work:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/midas/CoreResourceProviderImpl;->putString(Lcom/vlingo/core/internal/ResourceIdProvider$string;Ljava/lang/Integer;)V

    .line 367
    return-void
.end method
