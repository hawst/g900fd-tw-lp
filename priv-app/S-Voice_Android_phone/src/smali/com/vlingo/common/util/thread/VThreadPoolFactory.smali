.class public Lcom/vlingo/common/util/thread/VThreadPoolFactory;
.super Ljava/lang/Object;
.source "VThreadPoolFactory.java"


# static fields
.field private static defaultFactorySingleton:Lcom/vlingo/common/util/thread/VThreadPoolFactory;

.field private static final logger:Lorg/apache/log4j/Logger;


# instance fields
.field private final prefix:Ljava/lang/String;

.field private final properties:Ljava/util/Properties;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    .line 143
    const/4 v0, 0x0

    sput-object v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->defaultFactorySingleton:Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-direct {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>(Ljava/util/Properties;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/util/Properties;)V
    .locals 1
    .param p1, "properties"    # Ljava/util/Properties;

    .prologue
    .line 70
    const-string/jumbo v0, ""

    invoke-direct {p0, p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>(Ljava/util/Properties;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/util/Properties;Ljava/lang/String;)V
    .locals 0
    .param p1, "properties"    # Ljava/util/Properties;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->properties:Ljava/util/Properties;

    .line 75
    iput-object p2, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->prefix:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public static declared-synchronized getDefaultFactory()Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    .locals 2

    .prologue
    .line 195
    const-class v1, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->defaultFactorySingleton:Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    if-nez v0, :cond_0

    .line 196
    const-string/jumbo v0, "/ThreadPoolFactory.properties"

    invoke-static {v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->loadIndirectly(Ljava/lang/String;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->defaultFactorySingleton:Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    .line 198
    :cond_0
    sget-object v0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->defaultFactorySingleton:Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static loadIndirectly(Ljava/lang/String;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    .locals 6
    .param p0, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 159
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Loading thread pool factory from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 160
    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    .line 162
    .local v1, "urlProperties":Ljava/util/Properties;
    :try_start_0
    const-class v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-virtual {v3, p0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    const-string/jumbo v3, "thread.pool.overrides.url"

    invoke-virtual {v1, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, "urlString":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 172
    :cond_0
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Not overriding any thread pool defaults for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " - no override URL provided"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 173
    new-instance v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-direct {v3}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>()V

    .line 182
    .end local v2    # "urlString":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Was unable to load resource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 165
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    const-string/jumbo v4, "Not overriding any thread pool defaults"

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V

    .line 166
    new-instance v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-direct {v3}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>()V

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "urlString":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Loading thread pool override values from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 178
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->loadURL(Ljava/net/URL;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    .line 180
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unable to load from url "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V

    .line 181
    sget-object v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->logger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Not overriding any thread pool factory defaults for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " - no URL provided"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V

    .line 182
    new-instance v3, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-direct {v3}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>()V

    goto/16 :goto_0
.end method

.method public static loadStream(Ljava/io/InputStream;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    .locals 2
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 133
    .local v0, "properties":Ljava/util/Properties;
    invoke-virtual {v0, p0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 134
    new-instance v1, Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    invoke-direct {v1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;-><init>(Ljava/util/Properties;)V

    return-object v1
.end method

.method public static loadURL(Ljava/net/URL;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;
    .locals 1
    .param p0, "overrideURL"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->loadStream(Ljava/io/InputStream;)Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    move-result-object v0

    return-object v0
.end method

.method public static make(Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;
    .locals 1
    .param p0, "poolName"    # Ljava/lang/String;

    .prologue
    .line 215
    invoke-static {}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->getDefaultFactory()Lcom/vlingo/common/util/thread/VThreadPoolFactory;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->makeExecutor(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method private makeExecutorUsingBuilder(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/common/util/thread/VThreadPoolBuilder;)Lcom/vlingo/common/util/VThreadPoolExecutor;
    .locals 1
    .param p1, "poolName"    # Ljava/lang/String;
    .param p2, "jmxName"    # Ljava/lang/String;
    .param p3, "builder"    # Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .prologue
    .line 106
    invoke-virtual {p0, p1, p3}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->update(Ljava/lang/String;Lcom/vlingo/common/util/thread/VThreadPoolBuilder;)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->makeThreadPool(Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public makeExecutor(Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;
    .locals 4
    .param p1, "poolName"    # Ljava/lang/String;
    .param p2, "jmxName"    # Ljava/lang/String;

    .prologue
    .line 122
    new-instance v0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    new-instance v1, Lcom/vlingo/common/util/thread/PoolProperties;

    iget-object v2, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->prefix:Ljava/lang/String;

    iget-object v3, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->properties:Ljava/util/Properties;

    invoke-direct {v1, v2, p1, v3}, Lcom/vlingo/common/util/thread/PoolProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;)V

    invoke-direct {v0, v1}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;-><init>(Lcom/vlingo/common/util/thread/VThreadPoolOptions;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->makeExecutorUsingBuilder(Ljava/lang/String;Ljava/lang/String;Lcom/vlingo/common/util/thread/VThreadPoolBuilder;)Lcom/vlingo/common/util/VThreadPoolExecutor;

    move-result-object v0

    return-object v0
.end method

.method protected update(Ljava/lang/String;Lcom/vlingo/common/util/thread/VThreadPoolBuilder;)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 3
    .param p1, "poolName"    # Ljava/lang/String;
    .param p2, "builder"    # Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .prologue
    .line 91
    new-instance v0, Lcom/vlingo/common/util/thread/PoolProperties;

    iget-object v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->prefix:Ljava/lang/String;

    iget-object v2, p0, Lcom/vlingo/common/util/thread/VThreadPoolFactory;->properties:Ljava/util/Properties;

    invoke-direct {v0, v1, p1, v2}, Lcom/vlingo/common/util/thread/PoolProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;)V

    .line 92
    .local v0, "poolProps":Lcom/vlingo/common/util/thread/VThreadPoolOptions;
    invoke-virtual {p2, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->update(Lcom/vlingo/common/util/thread/VThreadPoolOptions;)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    move-result-object v1

    return-object v1
.end method
