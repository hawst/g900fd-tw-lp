.class public Lcom/vlingo/common/util/VlingoMBeanUtil;
.super Ljava/lang/Object;
.source "VlingoMBeanUtil.java"


# static fields
.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/vlingo/common/util/VlingoMBeanUtil;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static registerMBean(Lcom/vlingo/common/util/VlingoMBeanDecorator;)Z
    .locals 10
    .param p0, "registerMe"    # Lcom/vlingo/common/util/VlingoMBeanDecorator;

    .prologue
    const/4 v6, 0x0

    .line 30
    invoke-static {}, Ljava/lang/management/ManagementFactory;->getPlatformMBeanServer()Ljavax/management/MBeanServer;

    move-result-object v2

    .line 31
    .local v2, "mbs":Ljavax/management/MBeanServer;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getLocationInHierarchy()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ":type="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "nameForObject":Ljava/lang/String;
    :try_start_0
    new-instance v5, Ljavax/management/ObjectName;

    invoke-direct {v5, v4}, Ljavax/management/ObjectName;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljavax/management/MalformedObjectNameException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .local v5, "objName":Ljavax/management/ObjectName;
    :try_start_1
    invoke-interface {v2, v5}, Ljavax/management/MBeanServer;->isRegistered(Ljavax/management/ObjectName;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 46
    invoke-interface {v2, v5}, Ljavax/management/MBeanServer;->unregisterMBean(Ljavax/management/ObjectName;)V
    :try_end_1
    .catch Ljavax/management/InstanceNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/management/MBeanRegistrationException; {:try_start_1 .. :try_end_1} :catch_2

    .line 61
    :cond_0
    :try_start_2
    invoke-interface {v2, p0, v5}, Ljavax/management/MBeanServer;->registerMBean(Ljava/lang/Object;Ljavax/management/ObjectName;)Ljavax/management/ObjectInstance;
    :try_end_2
    .catch Ljavax/management/NotCompliantMBeanException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljavax/management/InstanceAlreadyExistsException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljavax/management/MBeanRegistrationException; {:try_start_2 .. :try_end_2} :catch_5

    .line 78
    const/4 v6, 0x1

    .end local v5    # "objName":Ljavax/management/ObjectName;
    :goto_0
    return v6

    .line 37
    :catch_0
    move-exception v3

    .line 39
    .local v3, "mone":Ljavax/management/MalformedObjectNameException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "The MBean\'s object name was malformed \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 49
    .end local v3    # "mone":Ljavax/management/MalformedObjectNameException;
    .restart local v5    # "objName":Ljavax/management/ObjectName;
    :catch_1
    move-exception v0

    .line 51
    .local v0, "e":Ljavax/management/InstanceNotFoundException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got an InstanceNotFound exception when trying to unregister MBean "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    .end local v0    # "e":Ljavax/management/InstanceNotFoundException;
    :catch_2
    move-exception v0

    .line 56
    .local v0, "e":Ljavax/management/MBeanRegistrationException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got an MBeanRegistration exception when trying to unregister MBean "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    .end local v0    # "e":Ljavax/management/MBeanRegistrationException;
    :catch_3
    move-exception v0

    .line 65
    .local v0, "e":Ljavax/management/NotCompliantMBeanException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got an NotCompliantMBeanException when trying to register MBean "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    .end local v0    # "e":Ljavax/management/NotCompliantMBeanException;
    :catch_4
    move-exception v0

    .line 70
    .local v0, "e":Ljavax/management/InstanceAlreadyExistsException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got an InstanceAlreadyExistsException when trying to register its MBean"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 73
    .end local v0    # "e":Ljavax/management/InstanceAlreadyExistsException;
    :catch_5
    move-exception v1

    .line 75
    .local v1, "infe":Ljavax/management/MBeanRegistrationException;
    sget-object v7, Lcom/vlingo/common/util/VlingoMBeanUtil;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Got an MBeanRegistration exception when trying to register MBean"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p0}, Lcom/vlingo/common/util/VlingoMBeanDecorator;->getMBeanName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
