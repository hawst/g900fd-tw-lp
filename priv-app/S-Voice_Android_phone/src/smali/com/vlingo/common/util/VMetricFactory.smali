.class public Lcom/vlingo/common/util/VMetricFactory;
.super Ljava/lang/Object;
.source "VMetricFactory.java"


# static fields
.field private static counterMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/yammer/metrics/core/Metric;",
            "Lcom/vlingo/common/util/VCounterMetric;",
            ">;"
        }
    .end annotation
.end field

.field private static gaugeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/yammer/metrics/core/Metric;",
            "Lcom/vlingo/common/util/VNumericGaugeMetric;",
            ">;"
        }
    .end annotation
.end field

.field private static ourInstance:Lcom/vlingo/common/util/VMetricFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/vlingo/common/util/VMetricFactory;

    invoke-direct {v0}, Lcom/vlingo/common/util/VMetricFactory;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/VMetricFactory;->ourInstance:Lcom/vlingo/common/util/VMetricFactory;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/VMetricFactory;->counterMap:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/VMetricFactory;->gaugeMap:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VCounterMetric;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "scope"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/common/util/VCounterMetric;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p0, p1, p2}, Lcom/yammer/metrics/Metrics;->newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/yammer/metrics/core/CounterMetric;

    move-result-object v1

    .line 74
    .local v1, "cmetric":Lcom/yammer/metrics/core/CounterMetric;
    const/4 v2, 0x0

    .line 75
    .local v2, "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    sget-object v5, Lcom/vlingo/common/util/VMetricFactory;->counterMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 77
    :try_start_0
    sget-object v4, Lcom/vlingo/common/util/VMetricFactory;->counterMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/vlingo/common/util/VCounterMetric;

    move-object v2, v0

    .line 78
    if-nez v2, :cond_0

    .line 80
    new-instance v3, Lcom/vlingo/common/util/VCounterMetricImpl;

    invoke-direct {v3, v1}, Lcom/vlingo/common/util/VCounterMetricImpl;-><init>(Lcom/yammer/metrics/core/CounterMetric;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .end local v2    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    .local v3, "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    :try_start_1
    sget-object v4, Lcom/vlingo/common/util/VMetricFactory;->counterMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v3

    .line 83
    .end local v3    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    .restart local v2    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    :cond_0
    :try_start_2
    monitor-exit v5

    .line 84
    return-object v2

    .line 83
    :catchall_0
    move-exception v4

    :goto_0
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .end local v2    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    .restart local v3    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    .restart local v2    # "ourMetric":Lcom/vlingo/common/util/VCounterMetric;
    goto :goto_0
.end method

.method public static newNumericGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VNumericGaugeMetric;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "scope"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/vlingo/common/util/VNumericGaugeMetric;"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, "klass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v2, Lcom/yammer/metrics/core/MetricName;

    invoke-direct {v2, p0, p1, p2}, Lcom/yammer/metrics/core/MetricName;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .local v2, "mName":Lcom/yammer/metrics/core/MetricName;
    sget-object v5, Lcom/vlingo/common/util/VMetricFactory;->gaugeMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 116
    :try_start_0
    invoke-static {}, Lcom/yammer/metrics/Metrics;->defaultRegistry()Lcom/yammer/metrics/core/MetricsRegistry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/yammer/metrics/core/MetricsRegistry;->allMetrics()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/yammer/metrics/core/Metric;

    .line 118
    .local v1, "lookedUp":Lcom/yammer/metrics/core/Metric;
    if-nez v1, :cond_0

    .line 124
    new-instance v3, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;

    invoke-direct {v3}, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;-><init>()V

    .line 125
    .local v3, "newGauge":Lcom/vlingo/common/util/VNumericGaugeMetricImpl;
    invoke-static {p0, p1, p2, v3}, Lcom/yammer/metrics/Metrics;->newGauge(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/yammer/metrics/core/GaugeMetric;)Lcom/yammer/metrics/core/GaugeMetric;

    move-result-object v0

    .line 126
    .local v0, "gMetric":Lcom/yammer/metrics/core/GaugeMetric;
    invoke-virtual {v3, v0}, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->setUnderlyingMetric(Lcom/yammer/metrics/core/GaugeMetric;)V

    .line 127
    sget-object v4, Lcom/vlingo/common/util/VMetricFactory;->gaugeMap:Ljava/util/HashMap;

    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    monitor-exit v5

    .line 136
    .end local v0    # "gMetric":Lcom/yammer/metrics/core/GaugeMetric;
    .end local v3    # "newGauge":Lcom/vlingo/common/util/VNumericGaugeMetricImpl;
    :goto_0
    return-object v3

    :cond_0
    sget-object v4, Lcom/vlingo/common/util/VMetricFactory;->gaugeMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/common/util/VNumericGaugeMetric;

    monitor-exit v5

    move-object v3, v4

    goto :goto_0

    .line 138
    .end local v1    # "lookedUp":Lcom/yammer/metrics/core/Metric;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
