.class public Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
.super Ljava/lang/Object;
.source "VThreadPoolBuilder.java"


# static fields
.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private allowCoreThreadTimeout:Z

.field private corePoolSize:I

.field private keepAliveSeconds:I

.field private maxPoolSize:I

.field private queueSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>(Lcom/vlingo/common/util/thread/VThreadPoolOptions;)V
    .locals 1
    .param p1, "options"    # Lcom/vlingo/common/util/thread/VThreadPoolOptions;

    .prologue
    const/4 v0, -0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->corePoolSize:I

    .line 26
    iput v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->maxPoolSize:I

    .line 27
    iput v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->keepAliveSeconds:I

    .line 28
    iput v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->queueSize:I

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->allowCoreThreadTimeout:Z

    .line 44
    invoke-virtual {p0, p1}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->update(Lcom/vlingo/common/util/thread/VThreadPoolOptions;)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 45
    return-void
.end method


# virtual methods
.method public makeThreadPool(Ljava/lang/String;)Lcom/vlingo/common/util/VThreadPoolExecutor;
    .locals 6
    .param p1, "jmxPoolName"    # Ljava/lang/String;

    .prologue
    .line 82
    sget-object v1, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Making thread pool "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 83
    new-instance v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    iget v2, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->queueSize:I

    iget v3, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->corePoolSize:I

    iget v4, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->maxPoolSize:I

    iget v5, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->keepAliveSeconds:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/common/util/VThreadPoolExecutor;-><init>(Ljava/lang/String;IIII)V

    .line 84
    .local v0, "newPool":Lcom/vlingo/common/util/VThreadPoolExecutor;
    iget-boolean v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->allowCoreThreadTimeout:Z

    invoke-virtual {v0, v1}, Lcom/vlingo/common/util/VThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 85
    return-object v0
.end method

.method public setAllowCoreThreadTimeout(Z)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 0
    .param p1, "allowCoreThreadTimeout"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->allowCoreThreadTimeout:Z

    .line 69
    return-object p0
.end method

.method public setCorePoolSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 0
    .param p1, "corePoolSize"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->corePoolSize:I

    .line 49
    return-object p0
.end method

.method public setKeepAliveSeconds(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 0
    .param p1, "keepAliveSeconds"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->keepAliveSeconds:I

    .line 64
    return-object p0
.end method

.method public setMaxPoolSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 0
    .param p1, "maxPoolSize"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->maxPoolSize:I

    .line 54
    return-object p0
.end method

.method public setQueueSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 0
    .param p1, "queueSize"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->queueSize:I

    .line 59
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Thread pool options: core pool size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->corePoolSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; max pool size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->maxPoolSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; queue size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->queueSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; keep alive seconds = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->keepAliveSeconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; allow core thread timeout = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->allowCoreThreadTimeout:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(Lcom/vlingo/common/util/thread/VThreadPoolOptions;)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;
    .locals 1
    .param p1, "options"    # Lcom/vlingo/common/util/thread/VThreadPoolOptions;

    .prologue
    .line 89
    iget v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->queueSize:I

    invoke-interface {p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolOptions;->getQueueSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->setQueueSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 90
    iget v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->corePoolSize:I

    invoke-interface {p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolOptions;->getCorePoolSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->setCorePoolSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 91
    iget v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->maxPoolSize:I

    invoke-interface {p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolOptions;->getMaxPoolSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->setMaxPoolSize(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 92
    iget v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->keepAliveSeconds:I

    invoke-interface {p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolOptions;->getKeepAliveSeconds(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->setKeepAliveSeconds(I)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 93
    iget-boolean v0, p0, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->allowCoreThreadTimeout:Z

    invoke-interface {p1, v0}, Lcom/vlingo/common/util/thread/VThreadPoolOptions;->getAllowCoreThreadTimeout(Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/thread/VThreadPoolBuilder;->setAllowCoreThreadTimeout(Z)Lcom/vlingo/common/util/thread/VThreadPoolBuilder;

    .line 95
    return-object p0
.end method
