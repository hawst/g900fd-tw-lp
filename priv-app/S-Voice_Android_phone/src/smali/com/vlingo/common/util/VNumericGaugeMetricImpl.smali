.class public Lcom/vlingo/common/util/VNumericGaugeMetricImpl;
.super Ljava/lang/Object;
.source "VNumericGaugeMetricImpl.java"

# interfaces
.implements Lcom/vlingo/common/util/VNumericGaugeMetric;
.implements Lcom/yammer/metrics/core/GaugeMetric;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vlingo/common/util/VNumericGaugeMetric;",
        "Lcom/yammer/metrics/core/GaugeMetric",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private basedOnMe:Lcom/yammer/metrics/core/GaugeMetric;

.field private final gaugeValue:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->basedOnMe:Lcom/yammer/metrics/core/GaugeMetric;

    .line 30
    iput-object v0, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->basedOnMe:Lcom/yammer/metrics/core/GaugeMetric;

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->gaugeValue:Ljava/util/concurrent/atomic/AtomicLong;

    .line 32
    return-void
.end method


# virtual methods
.method public dec()J
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->gaugeValue:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->decrementAndGet()J

    move-result-wide v0

    return-wide v0
.end method

.method public inc()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->gaugeValue:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0
.end method

.method public setUnderlyingMetric(Lcom/yammer/metrics/core/GaugeMetric;)V
    .locals 2
    .param p1, "basedOn"    # Lcom/yammer/metrics/core/GaugeMetric;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "basedOn can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iput-object p1, p0, Lcom/vlingo/common/util/VNumericGaugeMetricImpl;->basedOnMe:Lcom/yammer/metrics/core/GaugeMetric;

    .line 47
    return-void
.end method
