.class public interface abstract Lcom/vlingo/common/util/thread/VThreadPoolOptions;
.super Ljava/lang/Object;
.source "VThreadPoolOptions.java"


# virtual methods
.method public abstract getAllowCoreThreadTimeout(Z)Z
.end method

.method public abstract getCorePoolSize(I)I
.end method

.method public abstract getKeepAliveSeconds(I)I
.end method

.method public abstract getMaxPoolSize(I)I
.end method

.method public abstract getQueueSize(I)I
.end method
