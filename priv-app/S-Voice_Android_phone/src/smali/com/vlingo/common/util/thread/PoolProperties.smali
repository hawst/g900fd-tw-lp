.class public Lcom/vlingo/common/util/thread/PoolProperties;
.super Ljava/lang/Object;
.source "PoolProperties.java"

# interfaces
.implements Lcom/vlingo/common/util/thread/VThreadPoolOptions;


# instance fields
.field private final poolPrefix:Ljava/lang/String;

.field private final properties:Ljava/util/Properties;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Properties;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "poolName"    # Ljava/lang/String;
    .param p3, "properties"    # Ljava/util/Properties;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/thread/PoolProperties;->poolPrefix:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/vlingo/common/util/thread/PoolProperties;->properties:Ljava/util/Properties;

    .line 26
    return-void
.end method

.method private getBooleanProperty(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "propName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "valueString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 53
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .restart local p2    # "defaultValue":Z
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method private getIntProperty(Ljava/lang/String;I)I
    .locals 2
    .param p1, "propName"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "valueString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 43
    .end local p2    # "defaultValue":I
    :goto_0
    return p2

    .line 39
    .restart local p2    # "defaultValue":I
    :cond_0
    const-string/jumbo v1, "infinity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const p2, 0x7fffffff

    goto :goto_0

    .line 43
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0
.end method

.method private getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vlingo/common/util/thread/PoolProperties;->properties:Ljava/util/Properties;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vlingo/common/util/thread/PoolProperties;->poolPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllowCoreThreadTimeout(Z)Z
    .locals 1
    .param p1, "defaultValue"    # Z

    .prologue
    .line 73
    const-string/jumbo v0, "core.thread.timeout.allowed"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getCorePoolSize(I)I
    .locals 1
    .param p1, "defaultValue"    # I

    .prologue
    .line 61
    const-string/jumbo v0, "core.size"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getKeepAliveSeconds(I)I
    .locals 1
    .param p1, "defaultValue"    # I

    .prologue
    .line 69
    const-string/jumbo v0, "keepalive.seconds"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getMaxPoolSize(I)I
    .locals 1
    .param p1, "defaultValue"    # I

    .prologue
    .line 65
    const-string/jumbo v0, "max.size"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getQueueSize(I)I
    .locals 1
    .param p1, "defaultValue"    # I

    .prologue
    .line 57
    const-string/jumbo v0, "queue.size"

    invoke-direct {p0, v0, p1}, Lcom/vlingo/common/util/thread/PoolProperties;->getIntProperty(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
