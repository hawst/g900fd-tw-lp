.class public abstract Lcom/vlingo/common/util/RunnableBase;
.super Ljava/lang/Object;
.source "RunnableBase.java"

# interfaces
.implements Lcom/vlingo/common/util/VRunnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/common/util/RunnableBase$NDCHandling;
    }
.end annotation


# static fields
.field private static final ivLogger:Lorg/apache/log4j/Logger;

.field private static ivThreadToContext:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Thread;",
            "Lcom/vlingo/common/util/RunnableContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ivChildClass:Ljava/lang/Class;

.field private ivConstructorThread:Ljava/lang/Thread;

.field private ivContext:Lcom/vlingo/common/util/RunnableContext;

.field private final ivCreatedTime:J

.field private ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile ivLastEndTime:Ljava/lang/Long;

.field private volatile ivLastStartTime:Ljava/lang/Long;

.field private ivLogThrowable:Z

.field private ivMetricScope:Ljava/lang/String;

.field private ivNDC:Ljava/lang/String;

.field private ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

.field private ivNumTimesStarted:Ljava/util/concurrent/atomic/AtomicInteger;

.field private ivRunTimer:Lcom/yammer/metrics/core/TimerMetric;

.field private ivRunningThread:Ljava/lang/Thread;

.field private ivSkipThisRun:Z

.field private volatile ivThrowable:Ljava/lang/Throwable;

.field private ndcHandling:Lcom/vlingo/common/util/RunnableBase$NDCHandling;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vlingo/common/util/RunnableBase;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/vlingo/common/util/RunnableBase;->ivThreadToContext:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesStarted:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 36
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivThrowable:Ljava/lang/Throwable;

    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivCreatedTime:J

    .line 39
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastStartTime:Ljava/lang/Long;

    .line 40
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastEndTime:Ljava/lang/Long;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivLogThrowable:Z

    .line 42
    iput-boolean v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivSkipThisRun:Z

    .line 52
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunTimer:Lcom/yammer/metrics/core/TimerMetric;

    .line 54
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivChildClass:Ljava/lang/Class;

    .line 56
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivMetricScope:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->DefaultInformation:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ndcHandling:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    .line 74
    invoke-static {}, Lorg/apache/log4j/NDC;->get()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivNDC:Ljava/lang/String;

    .line 75
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivConstructorThread:Ljava/lang/Thread;

    .line 76
    return-void
.end method

.method private countThrowable(Ljava/lang/Throwable;)V
    .locals 8
    .param p1, "th"    # Ljava/lang/Throwable;

    .prologue
    .line 121
    iget-object v5, p0, Lcom/vlingo/common/util/RunnableBase;->ivChildClass:Ljava/lang/Class;

    if-nez v5, :cond_0

    .line 139
    :goto_0
    return-void

    .line 123
    :cond_0
    instance-of v5, p1, Lcom/vlingo/common/message/MessageException;

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 125
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 128
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    .line 129
    .local v3, "metricName":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/common/util/RunnableBase;->ivChildClass:Ljava/lang/Class;

    const-string/jumbo v6, "exceptions.$Dyn$.$NoTrack$"

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v5, v3, v6, v7}, Lcom/yammer/metrics/Metrics;->newMeter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/TimeUnit;)Lcom/yammer/metrics/core/MeterMetric;

    move-result-object v2

    .line 130
    .local v2, "m":Lcom/yammer/metrics/core/MeterMetric;
    invoke-virtual {v2}, Lcom/yammer/metrics/core/MeterMetric;->mark()V

    .line 133
    invoke-virtual {p1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v0, v5, v6

    .line 134
    .local v0, "caller":Ljava/lang/StackTraceElement;
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "className":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "simpleClassName":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 137
    const-class v5, Lcom/vlingo/common/util/RunnableBase;

    const-string/jumbo v6, "exceptionAt.$Dyn$.$NoTrack$"

    invoke-static {v5, v3, v6}, Lcom/vlingo/common/util/VMetricFactory;->newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VCounterMetric;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/common/util/VCounterMetric;->inc()V

    .line 138
    const-class v5, Lcom/vlingo/common/util/RunnableBase;

    iget-object v6, p0, Lcom/vlingo/common/util/RunnableBase;->ivChildClass:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "exceptionsIn.$Dyn$.$NoTrack$"

    invoke-static {v5, v6, v7}, Lcom/vlingo/common/util/VMetricFactory;->newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VCounterMetric;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/common/util/VCounterMetric;->inc()V

    goto/16 :goto_0
.end method

.method protected static declared-synchronized getForCurrentThread()Lcom/vlingo/common/util/RunnableContext;
    .locals 3

    .prologue
    .line 144
    const-class v1, Lcom/vlingo/common/util/RunnableBase;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vlingo/common/util/RunnableBase;->ivThreadToContext:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/common/util/RunnableContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized newForCurrentThread()Lcom/vlingo/common/util/RunnableContext;
    .locals 5

    .prologue
    .line 151
    const-class v2, Lcom/vlingo/common/util/RunnableBase;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->getForCurrentThread()Lcom/vlingo/common/util/RunnableContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 160
    .local v0, "rc":Lcom/vlingo/common/util/RunnableContext;
    :goto_0
    monitor-exit v2

    return-object v0

    .line 157
    .end local v0    # "rc":Lcom/vlingo/common/util/RunnableContext;
    :cond_0
    :try_start_1
    sget-object v1, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual {v1}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Creating context for thread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 158
    :cond_1
    new-instance v0, Lcom/vlingo/common/util/DefaultRunnableContext;

    invoke-direct {v0}, Lcom/vlingo/common/util/DefaultRunnableContext;-><init>()V

    .line 159
    .restart local v0    # "rc":Lcom/vlingo/common/util/RunnableContext;
    sget-object v1, Lcom/vlingo/common/util/RunnableBase;->ivThreadToContext:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized removeForCurrentThread()V
    .locals 4

    .prologue
    .line 165
    const-class v1, Lcom/vlingo/common/util/RunnableBase;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->getForCurrentThread()Lcom/vlingo/common/util/RunnableContext;

    move-result-object v0

    if-nez v0, :cond_0

    .line 166
    sget-object v0, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "No context for current thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/log4j/Logger;->warn(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :goto_0
    monitor-exit v1

    return-void

    .line 169
    :cond_0
    :try_start_1
    sget-object v0, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    invoke-virtual {v0}, Lorg/apache/log4j/Logger;->isTraceEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Removing context for thread"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/log4j/Logger;->trace(Ljava/lang/Object;)V

    .line 170
    :cond_1
    sget-object v0, Lcom/vlingo/common/util/RunnableBase;->ivThreadToContext:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized shouldBeSkipped()Z
    .locals 1

    .prologue
    .line 438
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivSkipThisRun:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivCreatedTime:J

    return-wide v0
.end method

.method public getNumTimesFinished()I
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public getThrowable()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ivThrowable:Ljava/lang/Throwable;

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vlingo/common/util/RunnableBase;->ndcHandling:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    invoke-virtual {p0, v0}, Lcom/vlingo/common/util/RunnableBase;->run(Lcom/vlingo/common/util/RunnableBase$NDCHandling;)V

    .line 182
    return-void
.end method

.method public run(Lcom/vlingo/common/util/RunnableBase$NDCHandling;)V
    .locals 9
    .param p1, "ndcHandling"    # Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    .prologue
    const/4 v8, 0x0

    .line 270
    invoke-direct {p0}, Lcom/vlingo/common/util/RunnableBase;->shouldBeSkipped()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 271
    sget-object v3, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Job skipped - not running task "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/log4j/Logger;->info(Ljava/lang/Object;)V

    .line 343
    :goto_0
    return-void

    .line 277
    :cond_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 279
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunningThread:Ljava/lang/Thread;

    .line 280
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 281
    :try_start_2
    new-instance v3, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastStartTime:Ljava/lang/Long;

    .line 282
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastEndTime:Ljava/lang/Long;

    .line 283
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivThrowable:Ljava/lang/Throwable;

    .line 284
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 285
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesStarted:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 287
    const-string/jumbo v0, ""

    .line 288
    .local v0, "ndc":Ljava/lang/String;
    sget-object v3, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->JustClassName:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    if-eq v3, p1, :cond_1

    .line 289
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivNDC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivConstructorThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 292
    .local v1, "sameThread":Z
    if-nez v1, :cond_1

    .line 293
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "th="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    .end local v1    # "sameThread":Z
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 298
    invoke-static {v0}, Lorg/apache/log4j/NDC;->push(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 302
    :try_start_3
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->newForCurrentThread()Lcom/vlingo/common/util/RunnableContext;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivContext:Lcom/vlingo/common/util/RunnableContext;

    .line 305
    invoke-virtual {p0}, Lcom/vlingo/common/util/RunnableBase;->runImpl()V

    .line 307
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunTimer:Lcom/yammer/metrics/core/TimerMetric;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunTimer:Lcom/yammer/metrics/core/TimerMetric;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastStartTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Lcom/yammer/metrics/core/TimerMetric;->update(JLjava/util/concurrent/TimeUnit;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 318
    :cond_2
    :try_start_4
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivContext:Lcom/vlingo/common/util/RunnableContext;

    if-eqz v3, :cond_3

    .line 319
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->removeForCurrentThread()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 327
    :cond_3
    :goto_1
    new-instance v3, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastEndTime:Ljava/lang/Long;

    .line 328
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 329
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 330
    monitor-enter p0

    .line 332
    const/4 v3, 0x0

    :try_start_5
    iput-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunningThread:Ljava/lang/Thread;

    .line 333
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 335
    invoke-static {}, Lorg/apache/log4j/NDC;->pop()Ljava/lang/String;

    .line 337
    monitor-enter p0

    .line 339
    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 340
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v3

    .line 280
    .end local v0    # "ndc":Ljava/lang/String;
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 327
    :catchall_2
    move-exception v3

    new-instance v4, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/lang/Long;-><init>(J)V

    iput-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivLastEndTime:Ljava/lang/Long;

    .line 328
    iget-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 329
    iget-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 330
    monitor-enter p0

    .line 332
    const/4 v4, 0x0

    :try_start_9
    iput-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivRunningThread:Ljava/lang/Thread;

    .line 333
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    .line 335
    invoke-static {}, Lorg/apache/log4j/NDC;->pop()Ljava/lang/String;

    .line 337
    monitor-enter p0

    .line 339
    :try_start_a
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 340
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    throw v3

    .line 309
    .restart local v0    # "ndc":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 311
    .local v2, "th":Ljava/lang/Throwable;
    :try_start_b
    iput-object v2, p0, Lcom/vlingo/common/util/RunnableBase;->ivThrowable:Ljava/lang/Throwable;

    .line 312
    invoke-direct {p0, v2}, Lcom/vlingo/common/util/RunnableBase;->countThrowable(Ljava/lang/Throwable;)V

    .line 313
    iget-boolean v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivLogThrowable:Z

    if-eqz v3, :cond_4

    .line 314
    sget-object v3, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    const-string/jumbo v4, "Exception occurred during operation"

    invoke-virtual {v3, v4, v2}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 318
    :cond_4
    :try_start_c
    iget-object v3, p0, Lcom/vlingo/common/util/RunnableBase;->ivContext:Lcom/vlingo/common/util/RunnableContext;

    if-eqz v3, :cond_3

    .line 319
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->removeForCurrentThread()V

    goto :goto_1

    .line 318
    .end local v2    # "th":Ljava/lang/Throwable;
    :catchall_3
    move-exception v3

    iget-object v4, p0, Lcom/vlingo/common/util/RunnableBase;->ivContext:Lcom/vlingo/common/util/RunnableContext;

    if-eqz v4, :cond_5

    .line 319
    invoke-static {}, Lcom/vlingo/common/util/RunnableBase;->removeForCurrentThread()V

    :cond_5
    throw v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 333
    :catchall_4
    move-exception v3

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v3

    .end local v0    # "ndc":Ljava/lang/String;
    :catchall_5
    move-exception v3

    :try_start_e
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v3

    .line 340
    :catchall_6
    move-exception v3

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v3
.end method

.method public abstract runImpl()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public waitToFinish(JI)V
    .locals 3
    .param p1, "timeout"    # J
    .param p3, "numTimes"    # I

    .prologue
    .line 186
    monitor-enter p0

    .line 188
    :try_start_0
    iget-object v1, p0, Lcom/vlingo/common/util/RunnableBase;->ivIsRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/common/util/RunnableBase;->ivNumTimesFinished:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ge v1, p3, :cond_1

    .line 192
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit p0

    .line 200
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/vlingo/common/util/RunnableBase;->ivLogger:Lorg/apache/log4j/Logger;

    const-string/jumbo v2, "Interrupted while waiting to finish"

    invoke-virtual {v1, v2, v0}, Lorg/apache/log4j/Logger;->error(Ljava/lang/Object;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
