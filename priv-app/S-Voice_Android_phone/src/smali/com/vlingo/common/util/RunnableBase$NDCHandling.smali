.class public final enum Lcom/vlingo/common/util/RunnableBase$NDCHandling;
.super Ljava/lang/Enum;
.source "RunnableBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/common/util/RunnableBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NDCHandling"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/common/util/RunnableBase$NDCHandling;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/common/util/RunnableBase$NDCHandling;

.field public static final enum DefaultInformation:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

.field public static final enum JustClassName:Lcom/vlingo/common/util/RunnableBase$NDCHandling;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    const-string/jumbo v1, "DefaultInformation"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/util/RunnableBase$NDCHandling;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->DefaultInformation:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    .line 61
    new-instance v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    const-string/jumbo v1, "JustClassName"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/common/util/RunnableBase$NDCHandling;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->JustClassName:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    sget-object v1, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->DefaultInformation:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->JustClassName:Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/common/util/RunnableBase$NDCHandling;->$VALUES:[Lcom/vlingo/common/util/RunnableBase$NDCHandling;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method
