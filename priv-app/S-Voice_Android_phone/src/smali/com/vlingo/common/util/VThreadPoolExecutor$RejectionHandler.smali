.class Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;
.super Ljava/lang/Object;
.source "VThreadPoolExecutor.java"

# interfaces
.implements Ljava/util/concurrent/RejectedExecutionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/common/util/VThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RejectionHandler"
.end annotation


# instance fields
.field ivRejectedCount:Lcom/vlingo/common/util/VCounterMetric;

.field ivRejectedMeter:Lcom/yammer/metrics/core/MeterMetric;

.field final synthetic this$0:Lcom/vlingo/common/util/VThreadPoolExecutor;


# direct methods
.method constructor <init>(Lcom/vlingo/common/util/VThreadPoolExecutor;)V
    .locals 4

    .prologue
    .line 85
    iput-object p1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->this$0:Lcom/vlingo/common/util/VThreadPoolExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "rejected"

    # getter for: Lcom/vlingo/common/util/VThreadPoolExecutor;->ivName:Ljava/lang/String;
    invoke-static {p1}, Lcom/vlingo/common/util/VThreadPoolExecutor;->access$600(Lcom/vlingo/common/util/VThreadPoolExecutor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vlingo/common/util/VMetricFactory;->newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VCounterMetric;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->ivRejectedCount:Lcom/vlingo/common/util/VCounterMetric;

    .line 87
    const-class v0, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v1, "rejectedRate"

    const-string/jumbo v2, "rejections"

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2, v3}, Lcom/yammer/metrics/Metrics;->newMeter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/TimeUnit;)Lcom/yammer/metrics/core/MeterMetric;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->ivRejectedMeter:Lcom/yammer/metrics/core/MeterMetric;

    .line 88
    return-void
.end method


# virtual methods
.method public rejectedExecution(Ljava/lang/Runnable;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 4
    .param p1, "r"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/ThreadPoolExecutor;

    .prologue
    .line 92
    iget-object v1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->ivRejectedCount:Lcom/vlingo/common/util/VCounterMetric;

    invoke-interface {v1}, Lcom/vlingo/common/util/VCounterMetric;->inc()V

    .line 93
    iget-object v1, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->ivRejectedMeter:Lcom/yammer/metrics/core/MeterMetric;

    invoke-virtual {v1}, Lcom/yammer/metrics/core/MeterMetric;->mark()V

    .line 94
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "taskName":Ljava/lang/String;
    const-class v1, Lcom/vlingo/common/util/VThreadPoolExecutor;

    const-string/jumbo v2, "rejectedTask"

    invoke-static {v1, v2, v0}, Lcom/vlingo/common/util/VMetricFactory;->newCounter(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/common/util/VCounterMetric;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/common/util/VCounterMetric;->inc()V

    .line 96
    new-instance v1, Ljava/util/concurrent/RejectedExecutionException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Rejected task in \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/common/util/VThreadPoolExecutor$RejectionHandler;->this$0:Lcom/vlingo/common/util/VThreadPoolExecutor;

    # getter for: Lcom/vlingo/common/util/VThreadPoolExecutor;->ivName:Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/common/util/VThreadPoolExecutor;->access$600(Lcom/vlingo/common/util/VThreadPoolExecutor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\" thread pool; taskType=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
