.class public Lcom/vlingo/common/message/VMessage;
.super Ljava/lang/Object;
.source "VMessage.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ivCode:Lcom/vlingo/common/message/MessageCode;

.field private ivMessage:Lcom/vlingo/common/VText;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V
    .locals 0
    .param p1, "message"    # Lcom/vlingo/common/VText;
    .param p2, "code"    # Lcom/vlingo/common/message/MessageCode;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/vlingo/common/message/VMessage;->ivMessage:Lcom/vlingo/common/VText;

    .line 22
    iput-object p2, p0, Lcom/vlingo/common/message/VMessage;->ivCode:Lcom/vlingo/common/message/MessageCode;

    .line 23
    return-void
.end method


# virtual methods
.method public getCode()Lcom/vlingo/common/message/MessageCode;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/common/message/VMessage;->ivCode:Lcom/vlingo/common/message/MessageCode;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/common/message/VMessage;->ivMessage:Lcom/vlingo/common/VText;

    invoke-virtual {v0}, Lcom/vlingo/common/VText;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/common/message/VMessage;->getCode()Lcom/vlingo/common/message/MessageCode;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/common/message/VMessage;->getCode()Lcom/vlingo/common/message/MessageCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vlingo/common/message/VMessage;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
