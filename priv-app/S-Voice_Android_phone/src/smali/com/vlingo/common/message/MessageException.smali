.class public Lcom/vlingo/common/message/MessageException;
.super Ljava/lang/RuntimeException;
.source "MessageException.java"


# instance fields
.field private ivMessage:Lcom/vlingo/common/message/VMessage;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/message/VMessage;)V
    .locals 1
    .param p1, "message"    # Lcom/vlingo/common/message/VMessage;

    .prologue
    .line 20
    invoke-static {p1}, Lcom/vlingo/common/message/MessageException;->toString(Lcom/vlingo/common/message/VMessage;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lcom/vlingo/common/message/MessageException;->ivMessage:Lcom/vlingo/common/message/VMessage;

    .line 23
    return-void
.end method

.method private static toString(Lcom/vlingo/common/message/VMessage;)Ljava/lang/String;
    .locals 3
    .param p0, "vm"    # Lcom/vlingo/common/message/VMessage;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vlingo/common/message/VMessage;->getCode()Lcom/vlingo/common/message/MessageCode;

    move-result-object v0

    .line 35
    .local v0, "code":Lcom/vlingo/common/message/MessageCode;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/common/message/VMessage;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
