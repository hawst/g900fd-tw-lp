.class public Lcom/vlingo/common/message/XMLCodec;
.super Ljava/lang/Object;
.source "XMLCodec.java"

# interfaces
.implements Lcom/vlingo/common/message/Codec;


# static fields
.field private static final ivInst:Lcom/vlingo/common/message/XMLCodec;


# instance fields
.field private ivDisableOutputFormatting:Z

.field private ivEncoding:Ljava/lang/String;

.field private ivGenXMLHeader:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/vlingo/common/message/XMLCodec;

    const-string/jumbo v1, "utf-8"

    invoke-direct {v0, v1}, Lcom/vlingo/common/message/XMLCodec;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/common/message/XMLCodec;->ivInst:Lcom/vlingo/common/message/XMLCodec;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v0, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    .line 24
    iput-boolean v0, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    .line 32
    iput-object p1, p0, Lcom/vlingo/common/message/XMLCodec;->ivEncoding:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1
    .param p1, "encoding"    # Ljava/lang/String;
    .param p2, "genXMLHeader"    # Z
    .param p3, "disableOutputFormatting"    # Z

    .prologue
    const/4 v0, 0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v0, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    .line 24
    iput-boolean v0, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    .line 37
    iput-object p1, p0, Lcom/vlingo/common/message/XMLCodec;->ivEncoding:Ljava/lang/String;

    .line 38
    iput-boolean p2, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    .line 39
    iput-boolean p3, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    .line 40
    return-void
.end method

.method private decode(Lorg/w3c/dom/Node;)Lcom/vlingo/common/message/MNode;
    .locals 10
    .param p1, "xmlNode"    # Lorg/w3c/dom/Node;

    .prologue
    .line 226
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    .line 227
    .local v7, "nodeValue":Ljava/lang/String;
    new-instance v5, Lcom/vlingo/common/message/MNode;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9, v7}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    .local v5, "mNode":Lcom/vlingo/common/message/MNode;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 230
    .local v1, "atrs":Lorg/w3c/dom/NamedNodeMap;
    if-eqz v1, :cond_0

    .line 232
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 234
    invoke-interface {v1, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 235
    .local v0, "atr":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    .line 236
    .local v6, "name":Ljava/lang/String;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v8

    .line 237
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 241
    .end local v0    # "atr":Lorg/w3c/dom/Node;
    .end local v4    # "index":I
    .end local v6    # "name":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    .local v2, "child":Lorg/w3c/dom/Node;
    :goto_1
    if-eqz v2, :cond_1

    .line 243
    invoke-direct {p0, v2}, Lcom/vlingo/common/message/XMLCodec;->decode(Lorg/w3c/dom/Node;)Lcom/vlingo/common/message/MNode;

    move-result-object v3

    .line 244
    .local v3, "childMNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v5, v3}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 241
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v2

    goto :goto_1

    .line 247
    .end local v3    # "childMNode":Lcom/vlingo/common/message/MNode;
    :cond_1
    return-object v5
.end method

.method private encode(Lcom/vlingo/common/message/MNode;Lcom/bzbyte/util/CodeGenOutputStream;)V
    .locals 9
    .param p1, "node"    # Lcom/vlingo/common/message/MNode;
    .param p2, "gout"    # Lcom/bzbyte/util/CodeGenOutputStream;

    .prologue
    .line 137
    invoke-virtual {p2}, Lcom/bzbyte/util/CodeGenOutputStream;->getTabsEnabled()Z

    move-result v5

    .line 140
    .local v5, "prevTabsEnabled":Z
    :try_start_0
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getFormating()Z

    move-result v6

    if-nez v6, :cond_0

    .line 142
    const/4 v6, 0x0

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->setTabsEnabled(Z)V

    .line 144
    :cond_0
    const-string/jumbo v6, "#text"

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 146
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 147
    .local v4, "nodeValue":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 148
    invoke-static {v4}, Lcom/bzbyte/util/xml/XMLUtil;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getFormating()Z

    move-result v6

    if-nez v6, :cond_2

    .line 194
    invoke-virtual {p2, v5}, Lcom/bzbyte/util/CodeGenOutputStream;->setTabsEnabled(Z)V

    .line 196
    :cond_2
    if-nez p1, :cond_e

    .line 198
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "encode() called with a null value"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 152
    .end local v4    # "nodeValue":Ljava/lang/String;
    :cond_3
    :try_start_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getAttributes()Ljava/util/Map;

    move-result-object v0

    .line 154
    .local v0, "atrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 156
    .local v3, "key":Ljava/lang/String;
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/bzbyte/util/xml/XMLUtil;->genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 192
    .end local v0    # "atrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    :catchall_0
    move-exception v6

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getFormating()Z

    move-result v7

    if-nez v7, :cond_4

    .line 194
    invoke-virtual {p2, v5}, Lcom/bzbyte/util/CodeGenOutputStream;->setTabsEnabled(Z)V

    .line 196
    :cond_4
    if-nez p1, :cond_d

    .line 198
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "encode() called with a null value"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 159
    .restart local v0    # "atrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_8

    .line 161
    iget-boolean v6, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    if-eqz v6, :cond_7

    .line 162
    const-string/jumbo v6, "/>"

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :goto_1
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getFormating()Z

    move-result v6

    if-nez v6, :cond_6

    .line 194
    invoke-virtual {p2, v5}, Lcom/bzbyte/util/CodeGenOutputStream;->setTabsEnabled(Z)V

    .line 196
    :cond_6
    if-nez p1, :cond_e

    .line 198
    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "encode() called with a null value"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 164
    :cond_7
    :try_start_3
    const-string/jumbo v6, "/>"

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 169
    :cond_8
    const-string/jumbo v6, ">"

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V

    .line 170
    iget-boolean v6, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    if-nez v6, :cond_9

    .line 171
    invoke-virtual {p2}, Lcom/bzbyte/util/CodeGenOutputStream;->println()V

    .line 172
    :cond_9
    invoke-virtual {p2}, Lcom/bzbyte/util/CodeGenOutputStream;->addTab()V

    .line 173
    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 175
    .local v1, "child":Lcom/vlingo/common/message/MNode;
    if-eqz v1, :cond_a

    .line 176
    invoke-direct {p0, v1, p2}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;Lcom/bzbyte/util/CodeGenOutputStream;)V

    goto :goto_2

    .line 179
    :cond_a
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Child node of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " is null"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 182
    .end local v1    # "child":Lcom/vlingo/common/message/MNode;
    :cond_b
    invoke-virtual {p2}, Lcom/bzbyte/util/CodeGenOutputStream;->removeTab()V

    .line 183
    iget-boolean v6, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    if-eqz v6, :cond_c

    .line 184
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 186
    :cond_c
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vlingo/common/message/MNode;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/bzbyte/util/CodeGenOutputStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 198
    .end local v0    # "atrs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_d
    throw v6

    .line 201
    :cond_e
    return-void
.end method

.method public static getInstance()Lcom/vlingo/common/message/XMLCodec;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/vlingo/common/message/XMLCodec;->ivInst:Lcom/vlingo/common/message/XMLCodec;

    return-object v0
.end method

.method private newEncodingWriter(Ljava/io/OutputStream;)Ljava/io/Writer;
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 82
    :try_start_0
    new-instance v1, Ljava/io/OutputStreamWriter;

    iget-object v2, p0, Lcom/vlingo/common/message/XMLCodec;->ivEncoding:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 84
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Failed to create stream encoding:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/common/message/XMLCodec;->ivEncoding:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private process(Lcom/vlingo/common/message/MNode;Lcom/bzbyte/util/CodeGenOutputStream;)V
    .locals 4
    .param p1, "node"    # Lcom/vlingo/common/message/MNode;
    .param p2, "gout"    # Lcom/bzbyte/util/CodeGenOutputStream;

    .prologue
    .line 114
    iget-boolean v1, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p2, v1}, Lcom/bzbyte/util/CodeGenOutputStream;->setTabsEnabled(Z)V

    .line 115
    iget-boolean v1, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    if-eqz v1, :cond_0

    .line 116
    const-string/jumbo v1, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {p2, v1}, Lcom/bzbyte/util/CodeGenOutputStream;->print(Ljava/lang/String;)V

    .line 117
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;Lcom/bzbyte/util/CodeGenOutputStream;)V

    .line 120
    :try_start_0
    invoke-virtual {p2}, Lcom/bzbyte/util/CodeGenOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    return-void

    .line 114
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error flushing AML output stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;)Lcom/vlingo/common/message/MNode;
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 205
    invoke-static {p1}, Lcom/bzbyte/util/xml/XMLUtil;->parseDocument(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 207
    .local v1, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v1}, Lorg/w3c/dom/Document;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    .line 208
    .local v0, "aNode":Lorg/w3c/dom/Node;
    :goto_0
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    if-eqz v0, :cond_0

    .line 210
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0

    .line 212
    :cond_0
    if-nez v0, :cond_1

    .line 214
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "No root node in document"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 216
    :cond_1
    invoke-direct {p0, v0}, Lcom/vlingo/common/message/XMLCodec;->decode(Lorg/w3c/dom/Node;)Lcom/vlingo/common/message/MNode;

    move-result-object v2

    return-object v2
.end method

.method public duplicate()Lcom/vlingo/common/message/XMLCodec;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lcom/vlingo/common/message/XMLCodec;

    iget-object v1, p0, Lcom/vlingo/common/message/XMLCodec;->ivEncoding:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    iget-boolean v3, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/common/message/XMLCodec;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public encode(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 2
    .param p1, "node"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 106
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 107
    .local v0, "bout":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;Ljava/io/OutputStream;)V

    .line 108
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public encode(Lcom/vlingo/common/message/MNode;Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "node"    # Lcom/vlingo/common/message/MNode;
    .param p2, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 93
    new-instance v0, Lcom/bzbyte/util/CodeGenOutputStream;

    invoke-direct {p0, p2}, Lcom/vlingo/common/message/XMLCodec;->newEncodingWriter(Ljava/io/OutputStream;)Ljava/io/Writer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/bzbyte/util/CodeGenOutputStream;-><init>(Ljava/io/Writer;)V

    .line 94
    .local v0, "gout":Lcom/bzbyte/util/CodeGenOutputStream;
    invoke-direct {p0, p1, v0}, Lcom/vlingo/common/message/XMLCodec;->process(Lcom/vlingo/common/message/MNode;Lcom/bzbyte/util/CodeGenOutputStream;)V

    .line 95
    return-void
.end method

.method public setDisableOutputFormatting(Z)V
    .locals 0
    .param p1, "disable"    # Z

    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/vlingo/common/message/XMLCodec;->ivDisableOutputFormatting:Z

    .line 132
    return-void
.end method

.method public setGenXMLHeader(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/vlingo/common/message/XMLCodec;->ivGenXMLHeader:Z

    .line 61
    return-void
.end method
