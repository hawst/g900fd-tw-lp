.class public final enum Lcom/vlingo/common/message/MessageCodeType;
.super Ljava/lang/Enum;
.source "MessageCodeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/common/message/MessageCodeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/common/message/MessageCodeType;

.field public static final enum ERROR:Lcom/vlingo/common/message/MessageCodeType;

.field public static final enum STATUS:Lcom/vlingo/common/message/MessageCodeType;

.field public static final enum WARNING:Lcom/vlingo/common/message/MessageCodeType;


# instance fields
.field private ivDisplayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/vlingo/common/message/MessageCodeType;

    const-string/jumbo v1, "WARNING"

    const-string/jumbo v2, "Warning"

    invoke-direct {v0, v1, v3, v2}, Lcom/vlingo/common/message/MessageCodeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/common/message/MessageCodeType;->WARNING:Lcom/vlingo/common/message/MessageCodeType;

    .line 10
    new-instance v0, Lcom/vlingo/common/message/MessageCodeType;

    const-string/jumbo v1, "ERROR"

    const-string/jumbo v2, "Error"

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/common/message/MessageCodeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/common/message/MessageCodeType;->ERROR:Lcom/vlingo/common/message/MessageCodeType;

    .line 11
    new-instance v0, Lcom/vlingo/common/message/MessageCodeType;

    const-string/jumbo v1, "STATUS"

    const-string/jumbo v2, "Status"

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/common/message/MessageCodeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/common/message/MessageCodeType;->STATUS:Lcom/vlingo/common/message/MessageCodeType;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/common/message/MessageCodeType;

    sget-object v1, Lcom/vlingo/common/message/MessageCodeType;->WARNING:Lcom/vlingo/common/message/MessageCodeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/common/message/MessageCodeType;->ERROR:Lcom/vlingo/common/message/MessageCodeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/common/message/MessageCodeType;->STATUS:Lcom/vlingo/common/message/MessageCodeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/common/message/MessageCodeType;->$VALUES:[Lcom/vlingo/common/message/MessageCodeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput-object p3, p0, Lcom/vlingo/common/message/MessageCodeType;->ivDisplayName:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/common/message/MessageCodeType;->ivDisplayName:Ljava/lang/String;

    return-object v0
.end method
