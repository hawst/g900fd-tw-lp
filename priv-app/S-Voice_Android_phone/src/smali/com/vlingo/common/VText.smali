.class public Lcom/vlingo/common/VText;
.super Ljava/lang/Object;
.source "VText.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private ivBundleName:Ljava/lang/String;

.field private ivKey:Ljava/lang/String;

.field private ivOverride:Ljava/lang/String;

.field private ivParams:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .param p1, "bundleName"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "params"    # [Ljava/lang/Object;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p3, p0, Lcom/vlingo/common/VText;->ivParams:[Ljava/lang/Object;

    .line 23
    iput-object p2, p0, Lcom/vlingo/common/VText;->ivKey:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/vlingo/common/VText;->ivBundleName:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 34
    iget-object v2, p0, Lcom/vlingo/common/VText;->ivOverride:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/vlingo/common/VText;->ivBundleName:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/PropertyResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v0

    .line 36
    .local v0, "bundle":Ljava/util/ResourceBundle;
    new-instance v1, Ljava/text/MessageFormat;

    iget-object v2, p0, Lcom/vlingo/common/VText;->ivKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 37
    .local v1, "mf":Ljava/text/MessageFormat;
    iget-object v2, p0, Lcom/vlingo/common/VText;->ivParams:[Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .end local v0    # "bundle":Ljava/util/ResourceBundle;
    .end local v1    # "mf":Ljava/text/MessageFormat;
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/vlingo/common/VText;->ivOverride:Ljava/lang/String;

    goto :goto_0
.end method
