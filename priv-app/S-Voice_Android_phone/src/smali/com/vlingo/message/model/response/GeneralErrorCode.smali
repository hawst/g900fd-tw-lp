.class public final enum Lcom/vlingo/message/model/response/GeneralErrorCode;
.super Ljava/lang/Enum;
.source "GeneralErrorCode.java"

# interfaces
.implements Lcom/vlingo/common/message/ErrorCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/message/model/response/GeneralErrorCode;",
        ">;",
        "Lcom/vlingo/common/message/ErrorCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedOperation:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToAccessDB:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToAccessFileSystem:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToAccessStorage:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToGenerateEventsXML:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToInit:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum FailedToShutdown:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum InvalidWrappedData:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum OperationTimedOut:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum RateLimitExceeded:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum SystemConfigurationError:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum TaskFailed:Lcom/vlingo/message/model/response/GeneralErrorCode;

.field public static final enum UnexpectedException:Lcom/vlingo/message/model/response/GeneralErrorCode;


# instance fields
.field private ivDoc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "UnexpectedException"

    const-string/jumbo v2, "An unexpected error occured that prevented an attempted operation."

    invoke-direct {v0, v1, v4, v2}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->UnexpectedException:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 7
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToInit"

    const-string/jumbo v2, "An unexpected error occured that prevented an attempted operation."

    invoke-direct {v0, v1, v5, v2}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToInit:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 8
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToShutdown"

    const-string/jumbo v2, "An unexpected error occured that prevented an attempted operation."

    invoke-direct {v0, v1, v6, v2}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToShutdown:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 9
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToAccessDB"

    const-string/jumbo v2, "Failed to get or save some information to the database"

    invoke-direct {v0, v1, v7, v2}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessDB:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 10
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToAccessFileSystem"

    const-string/jumbo v2, "Failed to get or save some information to the file system"

    invoke-direct {v0, v1, v8, v2}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessFileSystem:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 11
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToAccessStorage"

    const/4 v2, 0x5

    const-string/jumbo v3, "Failed to get or save some information to the file storage system"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessStorage:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 12
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "SystemConfigurationError"

    const/4 v2, 0x6

    const-string/jumbo v3, "Cannot function as expected due to some system configuration error"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->SystemConfigurationError:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 13
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "TaskFailed"

    const/4 v2, 0x7

    const-string/jumbo v3, "Failed to run a task"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->TaskFailed:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 14
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedToGenerateEventsXML"

    const/16 v2, 0x8

    const-string/jumbo v3, "Unable to create events XML"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToGenerateEventsXML:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 15
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "FailedOperation"

    const/16 v2, 0x9

    const-string/jumbo v3, "Unable to do requested operation"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedOperation:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 16
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "InvalidWrappedData"

    const/16 v2, 0xa

    const-string/jumbo v3, "The wrapped data received from the client was not valid."

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->InvalidWrappedData:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 17
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "OperationTimedOut"

    const/16 v2, 0xb

    const-string/jumbo v3, "null"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->OperationTimedOut:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 18
    new-instance v0, Lcom/vlingo/message/model/response/GeneralErrorCode;

    const-string/jumbo v1, "RateLimitExceeded"

    const/16 v2, 0xc

    const-string/jumbo v3, "null"

    invoke-direct {v0, v1, v2, v3}, Lcom/vlingo/message/model/response/GeneralErrorCode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->RateLimitExceeded:Lcom/vlingo/message/model/response/GeneralErrorCode;

    .line 4
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/vlingo/message/model/response/GeneralErrorCode;

    sget-object v1, Lcom/vlingo/message/model/response/GeneralErrorCode;->UnexpectedException:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToInit:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToShutdown:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessDB:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessFileSystem:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToAccessStorage:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->SystemConfigurationError:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->TaskFailed:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedToGenerateEventsXML:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedOperation:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->InvalidWrappedData:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->OperationTimedOut:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->RateLimitExceeded:Lcom/vlingo/message/model/response/GeneralErrorCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/message/model/response/GeneralErrorCode;->$VALUES:[Lcom/vlingo/message/model/response/GeneralErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "doc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/vlingo/message/model/response/GeneralErrorCode;->ivDoc:Ljava/lang/String;

    .line 23
    return-void
.end method
