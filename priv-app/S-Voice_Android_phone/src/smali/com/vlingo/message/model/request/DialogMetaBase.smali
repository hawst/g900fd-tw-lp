.class public Lcom/vlingo/message/model/request/DialogMetaBase;
.super Ljava/lang/Object;
.source "DialogMetaBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ClientTime:Ljava/lang/String; = "ClientTime"

.field public static final PROP_DialogGUID:Ljava/lang/String; = "DialogGUID"

.field public static final PROP_DialogMetaID:Ljava/lang/String; = "DialogMetaID"

.field public static final PROP_Disable:Ljava/lang/String; = "Disable"

.field public static final PROP_DisableDM:Ljava/lang/String; = "DisableDM"

.field public static final PROP_TurnNumber:Ljava/lang/String; = "TurnNumber"

.field public static final PROP_Use24HourTime:Ljava/lang/String; = "Use24HourTime"

.field public static final PROP_UserName:Ljava/lang/String; = "UserName"

.field public static final PROP_Voice:Ljava/lang/String; = "Voice"


# instance fields
.field private ClientTime:Ljava/lang/String;

.field private DialogGUID:Ljava/lang/String;

.field private DialogMetaID:Ljava/lang/String;

.field private Disable:Ljava/lang/String;

.field private DisableDM:Ljava/lang/Boolean;

.field private TurnNumber:Ljava/lang/Integer;

.field private Use24HourTime:Ljava/lang/Boolean;

.field private UserName:Ljava/lang/String;

.field private Voice:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/message/model/request/DialogMeta;

    const-string/jumbo v2, "/MessageModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/message/model/request/DialogMeta;

    const-string/jumbo v2, "/MessageModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getClientTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->ClientTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DialogGUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogMetaID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DialogMetaID:Ljava/lang/String;

    return-object v0
.end method

.method public getDisable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Disable:Ljava/lang/String;

    return-object v0
.end method

.method public getDisableDM()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DisableDM:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTurnNumber()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->TurnNumber:Ljava/lang/Integer;

    return-object v0
.end method

.method public getUse24HourTime()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Use24HourTime:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->UserName:Ljava/lang/String;

    return-object v0
.end method

.method public getVoice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Voice:Ljava/lang/String;

    return-object v0
.end method

.method public setClientTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->ClientTime:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setDialogGUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DialogGUID:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setDialogMetaID(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DialogMetaID:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setDisable(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Disable:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setDisableDM(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->DisableDM:Ljava/lang/Boolean;

    .line 73
    return-void
.end method

.method public setTurnNumber(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->TurnNumber:Ljava/lang/Integer;

    .line 45
    return-void
.end method

.method public setUse24HourTime(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Use24HourTime:Ljava/lang/Boolean;

    .line 87
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->UserName:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setVoice(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/message/model/request/DialogMetaBase;->Voice:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
