.class public Lcom/vlingo/message/model/response/GeneralErrorBase;
.super Ljava/lang/Object;
.source "GeneralErrorBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static getFailedOperation(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.message.model.response.GeneralError"

    const-string/jumbo v3, "FailedOperation"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->FailedOperation:Lcom/vlingo/message/model/response/GeneralErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method

.method public static getTaskFailed(Ljava/lang/String;)Lcom/vlingo/common/message/VMessage;
    .locals 6
    .param p0, "info"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Lcom/vlingo/common/message/VMessage;

    new-instance v1, Lcom/vlingo/common/VText;

    const-string/jumbo v2, "com.vlingo.message.model.response.GeneralError"

    const-string/jumbo v3, "TaskFailed"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/vlingo/common/VText;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v2, Lcom/vlingo/message/model/response/GeneralErrorCode;->TaskFailed:Lcom/vlingo/message/model/response/GeneralErrorCode;

    invoke-direct {v0, v1, v2}, Lcom/vlingo/common/message/VMessage;-><init>(Lcom/vlingo/common/VText;Lcom/vlingo/common/message/MessageCode;)V

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 75
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/message/model/response/GeneralError;

    const-string/jumbo v2, "/MessageModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
