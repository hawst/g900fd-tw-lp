.class public Lcom/vlingo/dialog/DMContext;
.super Ljava/lang/Object;
.source "DMContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/DMContext$HistoryPredicate;
    }
.end annotation


# static fields
.field private static final CODE_NOTHING_RECOGNIZED:Ljava/lang/String; = "NothingRecognized"

.field private static final HEX_CHARS:[C

.field private static final KEY_CONTACT_CLEAR_CACHE:Ljava/lang/String; = "contact_clear_cache"

.field private static final KEY_DATE_TIME_CACHE:Ljava/lang/String; = "date_time_cache"

.field private static final KEY_LIST_POSITION:Ljava/lang/String; = "list_position"

.field private static final KEY_PROMPT:Ljava/lang/String; = "prompt"

.field public static final MAX_CACHED_TIME_DIFFERENCE_MINUTES:I = 0x5

.field private static final NODE_CODE:Ljava/lang/String; = "Code"

.field private static final NODE_PG:Ljava/lang/String; = "pg"

.field private static final NODE_UL:Ljava/lang/String; = "UL"

.field private static final NODE_WARNING:Ljava/lang/String; = "Warning"

.field private static final STATE_VERSION_SER:[B

.field private static final STATE_VERSION_XML:[B

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;

.field private static metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

.field private static volatile packXml:Z


# instance fields
.field private activeParse:Lcom/vlingo/dialog/util/Parse;

.field private addedListPositionGoal:Z

.field private annotateContacts:Z

.field private autoListen:Z

.field private cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private capabilityMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private clientDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private closed:Z

.field private config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

.field private dialogMode:Ljava/lang/String;

.field private disableIncompleteWidget:Z

.field private fieldId:Ljava/lang/String;

.field private goalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;"
        }
    .end annotation
.end field

.field private goalsFinished:Z

.field private history:Lcom/vlingo/dialog/state/model/History;

.field private incomingFieldId:Ljava/lang/String;

.field private isDialogManagerFlow:Z

.field private isPassthrough:Z

.field private lastPrompt:Ljava/lang/String;

.field private listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

.field private listSize:Ljava/lang/Integer;

.field private needRecognition:Z

.field private parses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/util/Parse;",
            ">;"
        }
    .end annotation
.end field

.field private recognitionNotProcessed:Z

.field private softwareVersion:Ljava/lang/String;

.field private state:Lcom/vlingo/dialog/state/model/State;

.field private stateBytes:[B

.field private stateWasReset:Z

.field private wasResetToTop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x4

    .line 73
    const-class v0, Lcom/vlingo/dialog/DMContext;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 75
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    .line 76
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_XML:[B

    .line 90
    invoke-static {}, Lcom/vlingo/dialog/DMContext;->getPackXml()Z

    move-result v0

    sput-boolean v0, Lcom/vlingo/dialog/DMContext;->packXml:Z

    .line 394
    new-instance v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/vlingo/mda/model/PackageMeta;

    const/4 v2, 0x0

    invoke-static {}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;-><init>([Lcom/vlingo/mda/model/PackageMeta;)V

    sput-object v0, Lcom/vlingo/dialog/DMContext;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    .line 1112
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_2

    sput-object v0, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    return-void

    .line 75
    :array_0
    .array-data 1
        0x44t
        0x4dt
        0x0t
        0x3t
    .end array-data

    .line 76
    :array_1
    .array-data 1
        0x44t
        0x4dt
        0x1t
        0x0t
    .end array-data

    .line 1112
    :array_2
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;[BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/util/Map;)V
    .locals 5
    .param p1, "config"    # Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .param p2, "dialogStateBytes"    # [B
    .param p3, "incomingFieldId"    # Ljava/lang/String;
    .param p4, "clientDateTime"    # Lcom/vlingo/dialog/util/DateTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/manager/model/DialogManagerConfig;",
            "[B",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/util/DateTime;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "capabilityMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    .line 95
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 98
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 102
    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    .line 107
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    .line 116
    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 117
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    .line 118
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    .line 130
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    .line 132
    iput-object v3, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    .line 134
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    .line 136
    iput-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 140
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    .line 142
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    .line 146
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 147
    iput-object p2, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    .line 148
    iput-object p3, p0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    .line 149
    iput-object p4, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 151
    iput-object p5, p0, Lcom/vlingo/dialog/DMContext;->capabilityMap:Ljava/util/Map;

    .line 152
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "capability map = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 154
    :cond_0
    if-eqz p1, :cond_3

    .line 155
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->unpackHistory()Z

    move-result v0

    .line 156
    .local v0, "loadedHistory":Z
    if-nez v0, :cond_1

    .line 158
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "reset dialog state"

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 160
    :cond_1
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->init()V

    .line 161
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->initCachedDateTime()V

    .line 162
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "prompt"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->lastPrompt:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "prompt"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 164
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 165
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 166
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "pending queries: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->getPendingQueries()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 172
    .end local v0    # "loadedHistory":Z
    :cond_2
    :goto_0
    return-void

    .line 169
    :cond_3
    iput-boolean v4, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 170
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "no config: non-DM flow"

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private addNlgGoal(Lcom/vlingo/dialog/goal/model/NlgGoal;Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Z)V
    .locals 3
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/NlgGoal;
    .param p2, "template"    # Ljava/lang/String;
    .param p3, "templateForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p4, "autoListen"    # Z

    .prologue
    .line 735
    if-nez p2, :cond_0

    .line 736
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "template == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 738
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix()Ljava/lang/String;

    move-result-object v0

    .line 739
    .local v0, "suffix":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 740
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 742
    :cond_1
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setTemplate(Ljava/lang/String;)V

    .line 743
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setTemplateForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 744
    iget-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    or-int/2addr v1, p4

    iput-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 745
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 746
    return-void
.end method

.method private addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 793
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/History;->addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 794
    return-void
.end method

.method private addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V
    .locals 1
    .param p1, "item"    # Lcom/vlingo/dialog/state/model/HistoryItem;

    .prologue
    .line 605
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 606
    return-void
.end method

.method private addUtteranceGoal(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 642
    new-instance v0, Lcom/vlingo/dialog/goal/model/UtteranceGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/UtteranceGoal;-><init>()V

    .line 643
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/UtteranceGoal;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/goal/model/UtteranceGoal;->setText(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 645
    return-void
.end method

.method private static anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z
    .locals 4
    .param p0, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "incomingFieldId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 553
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->compatibleFieldId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 561
    :goto_0
    return v2

    .line 556
    :cond_0
    invoke-interface {p0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 557
    .local v1, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-static {v1, p1}, Lcom/vlingo/dialog/DMContext;->anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 561
    .end local v1    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 204
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 207
    .end local p0    # "string":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "string":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private assertNotClosed()V
    .locals 2

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    if-eqz v0, :cond_0

    .line 287
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :cond_0
    return-void
.end method

.method private extractUtterance(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;
    .locals 4
    .param p1, "root"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 326
    const-string/jumbo v3, "UL"

    invoke-virtual {p1, v3}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 327
    .local v2, "ulList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/common/message/MNode;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 328
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 329
    .local v1, "ul":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v3, "c"

    invoke-virtual {v1, v3}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v0

    .line 330
    .local v0, "c":Lcom/vlingo/common/message/MNode;
    if-eqz v0, :cond_0

    .line 331
    invoke-static {v0}, Lcom/vlingo/voicepad/tagtoaction/VPathUtil;->assembleStringFromWords(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v3

    .line 334
    .end local v0    # "c":Lcom/vlingo/common/message/MNode;
    .end local v1    # "ul":Lcom/vlingo/common/message/MNode;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private extractUtterance(Lcom/vlingo/voicepad/tagtoaction/model/Action;)Ljava/lang/String;
    .locals 4
    .param p1, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 338
    invoke-virtual {p1}, Lcom/vlingo/voicepad/tagtoaction/model/Action;->getParams()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;

    .line 339
    .local v1, "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    const-string/jumbo v2, "utterance"

    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    invoke-virtual {v1}, Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 343
    .end local v1    # "param":Lcom/vlingo/voicepad/tagtoaction/model/ActionParam;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private filterHistory(Lcom/vlingo/dialog/DMContext$HistoryPredicate;)V
    .locals 3
    .param p1, "predicate"    # Lcom/vlingo/dialog/DMContext$HistoryPredicate;

    .prologue
    .line 975
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 976
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 977
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-interface {p1, v1}, Lcom/vlingo/dialog/DMContext$HistoryPredicate;->apply(Lcom/vlingo/dialog/state/model/HistoryItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 978
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 981
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_1
    return-void
.end method

.method private getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mode"    # Ljava/lang/String;
    .param p2, "modeLess"    # Ljava/lang/String;

    .prologue
    .line 196
    if-nez p1, :cond_0

    .end local p2    # "modeLess":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "modeLess":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method private static getMode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "taskName"    # Ljava/lang/String;

    .prologue
    .line 382
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 383
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "taskName"    # Ljava/lang/String;

    .prologue
    .line 377
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 378
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .end local p0    # "taskName":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "taskName":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static getPackXml()Z
    .locals 8

    .prologue
    .line 1130
    const/4 v3, 0x1

    .line 1136
    .local v3, "result":Z
    :try_start_0
    const-string/jumbo v5, "com.vlingo.voicepad.util.VoicepadProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 1137
    .local v4, "voicepadPropertiesClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v5, "getProps"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1138
    .local v1, "getPropsMethod":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1139
    .local v2, "instance":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string/jumbo v6, "getDialogStateAsXml"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1140
    .local v0, "getDialogStateAsXmlMethod":Ljava/lang/reflect/Method;
    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v6, "calling VoicepadProperties.getProps().getDialogStateAsXml()"

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 1141
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1147
    .end local v0    # "getDialogStateAsXmlMethod":Ljava/lang/reflect/Method;
    .end local v1    # "getPropsMethod":Ljava/lang/reflect/Method;
    .end local v2    # "instance":Ljava/lang/Object;
    .end local v4    # "voicepadPropertiesClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "pack XML = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 1148
    return v3

    .line 1144
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private getPendingQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getPendingQueries()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getTaskName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 372
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 373
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private hijack(Lcom/vlingo/dialog/manager/model/TopFormManager;)Z
    .locals 1
    .param p1, "topManager"    # Lcom/vlingo/dialog/manager/model/TopFormManager;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {p1, p0, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->hijackParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 821
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    if-nez v1, :cond_1

    .line 822
    new-instance v1, Lcom/vlingo/dialog/state/model/History;

    invoke-direct {v1}, Lcom/vlingo/dialog/state/model/History;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 829
    :goto_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 830
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v2, "DMContext received with invalid activePath component."

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 831
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 834
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-nez v1, :cond_2

    .line 835
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    .line 836
    new-instance v1, Lcom/vlingo/dialog/state/model/State;

    invoke-direct {v1}, Lcom/vlingo/dialog/state/model/State;-><init>()V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 837
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    .line 838
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/state/model/State;->setTop(Lcom/vlingo/dialog/model/IForm;)V

    .line 839
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 840
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->preSerialize()V

    .line 846
    .end local v0    # "topForm":Lcom/vlingo/dialog/model/IForm;
    :goto_1
    return-void

    .line 824
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->fetchState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    goto :goto_0

    .line 843
    :cond_2
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->copy()Lcom/vlingo/dialog/state/model/State;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    .line 844
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/manager/model/TopFormManager;->unprune(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1
.end method

.method private initCachedDateTime()V
    .locals 4

    .prologue
    .line 175
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v3, "date_time_cache"

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "cachedDateTimeString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 177
    invoke-static {v1}, Lcom/vlingo/dialog/util/DateTime;->fromClientTime(Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 178
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/util/DateTime;->minutesUntil(Lcom/vlingo/dialog/util/DateTime;)I

    move-result v0

    .line 179
    .local v0, "ageMinutes":I
    const/4 v2, 0x5

    if-le v0, v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 185
    .end local v0    # "ageMinutes":I
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v2, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    goto :goto_0
.end method

.method public static isPackXml()Z
    .locals 1

    .prologue
    .line 1118
    sget-boolean v0, Lcom/vlingo/dialog/DMContext;->packXml:Z

    return v0
.end method

.method public static logBytes([B)V
    .locals 8
    .param p0, "bytes"    # [B

    .prologue
    const/16 v7, 0x40

    const/4 v6, 0x0

    .line 1088
    const/16 v0, 0x40

    .line 1089
    .local v0, "limit":I
    const/16 v1, 0x10

    .line 1091
    .local v1, "limitEnd":I
    if-nez p0, :cond_0

    .line 1092
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v4, "dialog state bytes = null"

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 1101
    :goto_0
    return-void

    .line 1093
    :cond_0
    array-length v3, p0

    if-gt v3, v7, :cond_1

    .line 1094
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "dialog state bytes["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-static {p0, v6, v5}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0

    .line 1096
    :cond_1
    const/16 v3, 0x10

    array-length v4, p0

    add-int/lit8 v4, v4, -0x40

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1097
    .local v2, "nEnd":I
    sget-object v3, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "dialog state bytes["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0, v6, v7}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, p0

    sub-int/2addr v5, v2

    array-length v6, p0

    invoke-static {p0, v5, v6}, Lcom/vlingo/dialog/DMContext;->toHex([BII)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private maybeSendIncompleteWidget()V
    .locals 4

    .prologue
    .line 713
    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->getPendingQueries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    if-nez v3, :cond_0

    .line 718
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 719
    .local v0, "active":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 720
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v2

    .line 721
    .local v2, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 722
    .local v1, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1, p0, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->maybeSendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 725
    .end local v0    # "active":Lcom/vlingo/dialog/model/IForm;
    .end local v1    # "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    .end local v2    # "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    :cond_0
    return-void
.end method

.method private modeConversion()V
    .locals 11

    .prologue
    .line 347
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v2

    .line 348
    .local v2, "activePath":Ljava/lang/String;
    const-string/jumbo v9, ""

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    invoke-static {v2}, Lcom/vlingo/dialog/DMContext;->getTaskName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, "activeTask":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/dialog/DMContext;->getMode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "activeMode":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {v0, v9}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 353
    invoke-static {v3}, Lcom/vlingo/dialog/DMContext;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "activeName":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9, v3}, Lcom/vlingo/dialog/model/IForm;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/model/Form;

    .line 355
    .local v4, "activeTaskForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v4}, Lcom/vlingo/dialog/model/Form;->getName()Ljava/lang/String;

    move-result-object v8

    .line 356
    .local v8, "origName":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    const-string/jumbo v10, ""

    invoke-direct {p0, v9, v10}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 357
    .local v7, "newSuffix":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 358
    .local v6, "newName":Ljava/lang/String;
    invoke-virtual {v4, v6}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 359
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v9}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 361
    .local v5, "newActivePath":Ljava/lang/String;
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v9}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 363
    iget-object v9, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v9, v5}, Lcom/vlingo/dialog/state/model/State;->setActivePath(Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :cond_2
    invoke-virtual {v4, v8}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private packHistory()[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 953
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 954
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    move-object v2, v0

    .line 955
    .local v2, "os":Ljava/io/OutputStream;
    sget-boolean v5, Lcom/vlingo/dialog/DMContext;->packXml:Z

    if-eqz v5, :cond_0

    .line 956
    sget-object v5, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_XML:[B

    invoke-virtual {v2, v5}, Ljava/io/OutputStream;->write([B)V

    .line 957
    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 958
    .end local v2    # "os":Ljava/io/OutputStream;
    .local v3, "os":Ljava/io/OutputStream;
    iget-object v5, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v5, v3}, Lcom/vlingo/dialog/state/model/History;->toXml(Ljava/io/OutputStream;)V

    .line 959
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 960
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 961
    .local v4, "result":[B
    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v5}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "packed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " bytes (XML)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    move-object v2, v3

    .line 971
    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :goto_0
    return-object v4

    .line 963
    .end local v4    # "result":[B
    :cond_0
    sget-object v5, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    invoke-virtual {v2, v5}, Ljava/io/OutputStream;->write([B)V

    .line 964
    new-instance v3, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 965
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v3    # "os":Ljava/io/OutputStream;
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v3}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 966
    .local v1, "oos":Ljava/io/ObjectOutputStream;
    iget-object v5, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v1, v5}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 967
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 968
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 969
    .restart local v4    # "result":[B
    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v5}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "packed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " bytes (Java)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .end local v1    # "oos":Ljava/io/ObjectOutputStream;
    :cond_1
    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method public static setPackXml(Z)V
    .locals 3
    .param p0, "packXml"    # Z

    .prologue
    .line 1123
    sput-boolean p0, Lcom/vlingo/dialog/DMContext;->packXml:Z

    .line 1124
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DMContext.packXml set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/vlingo/dialog/DMContext;->packXml:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 1125
    return-void
.end method

.method private startTaskCleanup(Z)V
    .locals 4
    .param p1, "clearCachedDateTime"    # Z

    .prologue
    .line 611
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    .line 612
    .local v0, "historyItems":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 613
    if-eqz p1, :cond_0

    .line 614
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 615
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "date_time_cache"

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getClientDateTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->setCandidates(Lcom/vlingo/dialog/state/model/CompletedQuery;)V

    .line 618
    return-void
.end method

.method public static final toHex([BII)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1104
    sub-int v4, p2, p1

    mul-int/lit8 v4, v4, 0x2

    new-array v0, v4, [C

    .line 1105
    .local v0, "buf":[C
    move v1, p1

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 1106
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v4, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    ushr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 1107
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v4, Lcom/vlingo/dialog/DMContext;->HEX_CHARS:[C

    aget-byte v5, p0, v1

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v2

    .line 1105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1109
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method private unpackHistory()Z
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 890
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 892
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v8, v8

    sget-object v10, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    array-length v10, v10

    if-ge v8, v10, :cond_4

    .line 893
    :cond_0
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 894
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-nez v8, :cond_2

    .line 895
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "dialog state bytes = null"

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    move v8, v9

    .line 947
    :goto_1
    return v8

    .line 897
    :cond_2
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 898
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v8}, Lcom/vlingo/dialog/DMContext;->logBytes([B)V

    goto :goto_0

    .line 900
    :cond_3
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "dialog state bytes: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v11, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 908
    :cond_4
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 909
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v8}, Lcom/bzbyte/util/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v6

    .line 910
    .local v6, "stateEncoded":Ljava/lang/String;
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Inbound DM State: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 913
    .end local v6    # "stateEncoded":Ljava/lang/String;
    :cond_5
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    sget-object v10, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    array-length v10, v10

    invoke-static {v8, v10}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v7

    .line 914
    .local v7, "versionBytes":[B
    sget-object v8, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    invoke-static {v7, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    .line 915
    .local v3, "matchSerialized":Z
    sget-object v8, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_XML:[B

    invoke-static {v7, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    .line 917
    .local v4, "matchXml":Z
    if-nez v3, :cond_8

    if-nez v4, :cond_8

    .line 918
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 919
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "dialog state bytes: non-matching version"

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 921
    :cond_6
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 922
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    invoke-static {v8}, Lcom/vlingo/dialog/DMContext;->logBytes([B)V

    :cond_7
    move v8, v9

    .line 924
    goto/16 :goto_1

    .line 928
    :cond_8
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    sget-object v10, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    array-length v10, v10

    iget-object v11, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    array-length v11, v11

    sget-object v12, Lcom/vlingo/dialog/DMContext;->STATE_VERSION_SER:[B

    array-length v12, v12

    sub-int/2addr v11, v12

    invoke-direct {v1, v8, v10, v11}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 929
    .local v1, "is":Ljava/io/InputStream;
    if-eqz v4, :cond_a

    .line 930
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_9

    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "unpacking XML serialization"

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 931
    :cond_9
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 932
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/vlingo/dialog/state/model/History;->fromXml(Ljava/io/InputStream;)Lcom/vlingo/dialog/state/model/History;

    move-result-object v8

    iput-object v8, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 933
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    iget-object v10, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 934
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    move-object v1, v2

    .line 943
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :goto_2
    iget-object v8, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v8}, Lcom/vlingo/dialog/state/model/History;->postDeserialize()V

    .line 944
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 936
    :cond_a
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_b

    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "unpacking Java serialization"

    invoke-virtual {v8, v10}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 937
    :cond_b
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 938
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 939
    .local v5, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/dialog/state/model/History;

    iput-object v8, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 940
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V

    .line 941
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 945
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v0

    .line 946
    .local v0, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v8

    if-eqz v8, :cond_c

    sget-object v8, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v10, "exception unpacking dialog state"

    invoke-virtual {v8, v10, v0}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;Ljava/lang/Throwable;)V

    :cond_c
    move v8, v9

    .line 947
    goto/16 :goto_1
.end method


# virtual methods
.method public addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V
    .locals 3
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/Goal;

    .prologue
    .line 749
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 750
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 751
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/vlingo/dialog/goal/model/ListGoal;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    if-eqz v0, :cond_1

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "list_position"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 754
    :cond_1
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "added goal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 755
    :cond_2
    return-void
.end method

.method public addListPositionGoal(Lcom/vlingo/dialog/goal/model/ListPositionGoal;)V
    .locals 4
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    .prologue
    .line 684
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->addedListPositionGoal:Z

    .line 685
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "list_position"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 687
    return-void
.end method

.method public addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "template"    # Ljava/lang/String;
    .param p2, "templateForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "fieldId"    # Ljava/lang/String;
    .param p4, "autoListen"    # Z

    .prologue
    .line 657
    new-instance v0, Lcom/vlingo/dialog/goal/model/PromptGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/PromptGoal;-><init>()V

    invoke-direct {p0, v0, p1, p2, p4}, Lcom/vlingo/dialog/DMContext;->addNlgGoal(Lcom/vlingo/dialog/goal/model/NlgGoal;Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Z)V

    .line 658
    if-eqz p3, :cond_0

    .line 659
    invoke-virtual {p0, p3}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "prompt"

    invoke-virtual {v0, v1, p1}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    return-void
.end method

.method public addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V
    .locals 1
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;
    .param p2, "clearCache"    # Z

    .prologue
    .line 665
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/goal/model/QueryGoal;->setClearCache(Z)V

    .line 666
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/DMContext;->addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 667
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 669
    instance-of v0, p1, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    if-eqz v0, :cond_0

    .line 670
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->setContactClearCache(Z)V

    .line 672
    :cond_0
    return-void
.end method

.method public addTaskGoal(Ljava/lang/String;ZZLcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "confirm"    # Z
    .param p3, "execute"    # Z
    .param p4, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 675
    new-instance v0, Lcom/vlingo/dialog/goal/model/TaskGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/TaskGoal;-><init>()V

    .line 676
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/TaskGoal;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setName(Ljava/lang/String;)V

    .line 677
    invoke-virtual {v0, p2}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setConfirm(Z)V

    .line 678
    invoke-virtual {v0, p3}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setExecute(Z)V

    .line 679
    invoke-virtual {v0, p4}, Lcom/vlingo/dialog/goal/model/TaskGoal;->setForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 680
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 681
    return-void
.end method

.method public anyListGoals()Z
    .locals 3

    .prologue
    .line 1001
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getGoals()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 1002
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/ListGoal;

    if-eqz v2, :cond_0

    .line 1003
    const/4 v2, 0x1

    .line 1006
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public appendDialogSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public appendTemplateSuffix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->appendSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cancel(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "formManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 758
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 760
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v1

    .line 761
    .local v1, "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->createForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 762
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/state/model/State;->setTop(Lcom/vlingo/dialog/model/IForm;)V

    .line 763
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2, v0}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 764
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 766
    new-instance v2, Lcom/vlingo/dialog/goal/model/DialogCancelGoal;

    invoke-direct {v2}, Lcom/vlingo/dialog/goal/model/DialogCancelGoal;-><init>()V

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 770
    return-void
.end method

.method public clearConfirm()V
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 774
    return-void
.end method

.method public clearPendingQueries()V
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->clearPendingQueries()V

    .line 810
    return-void
.end method

.method public clearQueries()V
    .locals 1

    .prologue
    .line 817
    const-class v0, Lcom/vlingo/dialog/goal/model/QueryGoal;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->clearQueriesByType(Ljava/lang/Class;)V

    .line 818
    return-void
.end method

.method public clearQueriesByType(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813
    .local p1, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/History;->clearQueriesByType(Ljava/lang/Class;)V

    .line 814
    return-void
.end method

.method public close()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 293
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->preSerialize()V

    .line 296
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "next state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 297
    :cond_0
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->packHistory()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    .line 304
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->closed:Z

    .line 305
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    return-object v0

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    if-nez v0, :cond_1

    .line 300
    new-instance v0, Lcom/vlingo/dialog/state/model/History;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/History;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    .line 301
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->preSerialize()V

    .line 302
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->packHistory()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->stateBytes:[B

    goto :goto_0
.end method

.method public completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 1
    .param p1, "queryEvent"    # Lcom/vlingo/dialog/event/model/QueryEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;"
        }
    .end annotation

    .prologue
    .line 805
    .local p2, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0, p1, p2}, Lcom/vlingo/dialog/state/model/History;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    return-object v0
.end method

.method public disableIncompleteWidget()V
    .locals 1

    .prologue
    .line 1084
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget:Z

    .line 1085
    return-void
.end method

.method public fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 3
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 862
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->getCompletedQueries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 863
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 867
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            ">;)",
            "Lcom/vlingo/dialog/event/model/QueryEvent;"
        }
    .end annotation

    .prologue
    .line 871
    .local p1, "eventClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/event/model/QueryEvent;>;"
    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/History;->getCompletedQueries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 872
    .local v2, "query":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    .line 873
    .local v0, "event":Lcom/vlingo/dialog/event/model/QueryEvent;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 877
    .end local v0    # "event":Lcom/vlingo/dialog/event/model/QueryEvent;
    .end local v2    # "query":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fetchState()Lcom/vlingo/dialog/state/model/State;
    .locals 5

    .prologue
    .line 849
    const/4 v3, 0x0

    .line 850
    .local v3, "result":Lcom/vlingo/dialog/state/model/State;
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v4}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    .line 851
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .local v0, "i":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/vlingo/dialog/state/model/HistoryItem;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 852
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 853
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    instance-of v4, v1, Lcom/vlingo/dialog/state/model/State;

    if-eqz v4, :cond_0

    move-object v3, v1

    .line 854
    check-cast v3, Lcom/vlingo/dialog/state/model/State;

    .line 858
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_1
    return-object v3
.end method

.method public finishGoals()V
    .locals 3

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 691
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->maybeSendIncompleteWidget()V

    .line 692
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 693
    new-instance v0, Lcom/vlingo/dialog/goal/model/FieldIdGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/FieldIdGoal;-><init>()V

    .line 694
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/FieldIdGoal;
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/goal/model/FieldIdGoal;->setFieldId(Ljava/lang/String;)V

    .line 695
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 697
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/FieldIdGoal;
    :cond_0
    iget-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    if-eqz v2, :cond_1

    .line 698
    new-instance v2, Lcom/vlingo/dialog/goal/model/ListenGoal;

    invoke-direct {v2}, Lcom/vlingo/dialog/goal/model/ListenGoal;-><init>()V

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 700
    :cond_1
    iget-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    if-eqz v2, :cond_3

    .line 702
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 703
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 704
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/FieldIdGoal;

    if-nez v2, :cond_2

    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/PassthroughGoal;

    if-nez v2, :cond_2

    .line 705
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 709
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 710
    return-void
.end method

.method public getActiveForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    return-object v0
.end method

.method public getActiveParse()Lcom/vlingo/dialog/util/Parse;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    return-object v0
.end method

.method public getAnnotateContacts()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    return v0
.end method

.method public getCapabilityValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "capability"    # Ljava/lang/String;

    .prologue
    .line 885
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->capabilityMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getClientDateTime()Lcom/vlingo/dialog/util/DateTime;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->cachedDateTime:Lcom/vlingo/dialog/util/DateTime;

    return-object v0
.end method

.method public getContactClearCache()Z
    .locals 3

    .prologue
    .line 992
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "contact_clear_cache"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 993
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDialogMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogSuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vlingo/dialog/DMContext;->getDialogSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getGoals()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/Goal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 728
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    if-nez v0, :cond_0

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIncomingFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getLastPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->lastPrompt:Ljava/lang/String;

    return-object v0
.end method

.method public getListPosition()Lcom/vlingo/dialog/manager/model/ListPosition;
    .locals 4

    .prologue
    .line 1027
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    if-nez v1, :cond_0

    .line 1028
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "list_position"

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->parse(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v0

    .line 1029
    .local v0, "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    if-eqz v0, :cond_1

    .line 1030
    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1031
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1032
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no list position from client: using previous value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 1043
    .end local v0    # "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    return-object v1

    .line 1036
    .restart local v0    # "previous":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_1
    new-instance v1, Lcom/vlingo/dialog/manager/model/ListPosition;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vlingo/dialog/manager/model/ListPosition;-><init>(II)V

    iput-object v1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1037
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1038
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no list position from client: using default value "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    invoke-virtual {v3}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getListSize()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1052
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollDefaultListSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1053
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1054
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1056
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "no client list size: using default "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 1058
    :cond_1
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public getNeedRecognition()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    return v0
.end method

.method public getParses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/util/Parse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    return-object v0
.end method

.method public getScrollAutoListen(Lcom/vlingo/dialog/manager/model/IFormManager;)Z
    .locals 2
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 1062
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/FormManager;->getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 1063
    .local v0, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getScrollAutoListen()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1064
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getScrollAutoListen()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1066
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollAutoListen()Z

    move-result v1

    goto :goto_0
.end method

.method public getScrollErrorAutoListen(Lcom/vlingo/dialog/manager/model/IFormManager;)Z
    .locals 2
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 1071
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/FormManager;->getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 1072
    .local v0, "taskManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getScrollErrorAutoListen()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1073
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getScrollErrorAutoListen()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1075
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollErrorAutoListen()Z

    move-result v1

    goto :goto_0
.end method

.method public getScrollShowUserTurn()Z
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getScrollShowUserTurn()Z

    move-result v0

    return v0
.end method

.method public getSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/vlingo/dialog/state/model/State;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    return-object v0
.end method

.method public isConfirmCancel()Z
    .locals 2

    .prologue
    .line 789
    const-string/jumbo v0, "cancel"

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isConfirmExecute()Z
    .locals 2

    .prologue
    .line 785
    const-string/jumbo v0, "execute"

    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isDialogManagerFlow()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    return v0
.end method

.method public isPassthrough()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    return v0
.end method

.method public peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/History;->peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v0

    return-object v0
.end method

.method public processEvents(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Event;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 309
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Event;>;"
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    if-nez v4, :cond_1

    .line 310
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "no config: ignoring events"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 323
    :cond_0
    return-void

    .line 313
    :cond_1
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processEvents: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 314
    :cond_2
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 315
    invoke-direct {p0}, Lcom/vlingo/dialog/DMContext;->modeConversion()V

    .line 316
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v4}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 317
    .local v0, "activeForm":Lcom/vlingo/dialog/model/IForm;
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "active: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v6}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 318
    :cond_3
    iget-object v4, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v4}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v4

    iget-object v5, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v5}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 319
    .local v1, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/event/model/Event;

    .line 320
    .local v2, "event":Lcom/vlingo/dialog/event/model/Event;
    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    sget-object v4, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "event: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 321
    :cond_4
    invoke-interface {v1, p0, v0, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processRecognition(Lcom/vlingo/common/message/MNode;)V
    .locals 1
    .param p1, "recognition"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 387
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 388
    return-void
.end method

.method public processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 19
    .param p1, "recognition"    # Lcom/vlingo/common/message/MNode;
    .param p2, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 411
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    .line 412
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_0

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v17, "no recognition MNode"

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 413
    :cond_0
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    .line 535
    :cond_1
    :goto_0
    return-void

    .line 417
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->assertNotClosed()V

    .line 419
    if-eqz p1, :cond_6

    .line 420
    const-string/jumbo v16, "Warning"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/common/message/MNode;

    .line 421
    .local v15, "warningNode":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v16, "Code"

    invoke-virtual/range {v15 .. v16}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v6

    .line 422
    .local v6, "codeNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v6}, Lcom/vlingo/common/message/MNode;->getTextChildNodeValue()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 423
    .local v5, "code":Ljava/lang/String;
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_4

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "rec warning code: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 425
    :cond_4
    const-string/jumbo v16, "NothingRecognized"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 426
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_5

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "handling "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " by generating FieldIdGoal = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 427
    :cond_5
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 428
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->autoListen:Z

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 430
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    goto/16 :goto_0

    .line 436
    .end local v5    # "code":Ljava/lang/String;
    .end local v6    # "codeNode":Lcom/vlingo/common/message/MNode;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v15    # "warningNode":Lcom/vlingo/common/message/MNode;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    if-nez v16, :cond_7

    .line 437
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 441
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v12

    .line 443
    .local v12, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getDmFieldIdPattern()Ljava/util/regex/Pattern;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/regex/Matcher;->matches()Z

    move-result v16

    if-eqz v16, :cond_8

    const/4 v8, 0x1

    .line 444
    .local v8, "isDmFieldId":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->isTopLevelFieldId(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_9

    const/4 v9, 0x1

    .line 446
    .local v9, "isSpecialTopLevelFieldId":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    if-nez v8, :cond_a

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->mightHijack(Ljava/lang/String;)Z

    move-result v10

    .line 448
    .local v10, "mightHijack":Z
    if-nez v9, :cond_a

    if-nez v10, :cond_a

    .line 450
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 443
    .end local v8    # "isDmFieldId":Z
    .end local v9    # "isSpecialTopLevelFieldId":Z
    .end local v10    # "mightHijack":Z
    :cond_8
    const/4 v8, 0x0

    goto :goto_1

    .line 444
    .restart local v8    # "isDmFieldId":Z
    :cond_9
    const/4 v9, 0x0

    goto :goto_2

    .line 457
    .restart local v9    # "isSpecialTopLevelFieldId":Z
    :cond_a
    if-eqz p1, :cond_b

    .line 458
    invoke-direct/range {p0 .. p1}, Lcom/vlingo/dialog/DMContext;->extractUtterance(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v14

    .line 462
    .local v14, "utterance":Ljava/lang/String;
    :goto_3
    if-nez v14, :cond_c

    .line 463
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->recognitionNotProcessed:Z

    goto/16 :goto_0

    .line 460
    .end local v14    # "utterance":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/DMContext;->extractUtterance(Lcom/vlingo/voicepad/tagtoaction/model/Action;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "utterance":Ljava/lang/String;
    goto :goto_3

    .line 467
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 468
    const/16 v16, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->clearPendingQueries()V

    .line 472
    if-eqz p1, :cond_d

    .line 473
    const-string/jumbo v16, "pg"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vlingo/common/message/MNode;

    .line 474
    .local v11, "pg":Lcom/vlingo/common/message/MNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v14}, Lcom/vlingo/dialog/util/Parse;-><init>(Lcom/vlingo/common/message/MNode;Ljava/lang/String;)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 477
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v11    # "pg":Lcom/vlingo/common/message/MNode;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v17, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/util/Parse;-><init>(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_f

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->parses:Ljava/util/List;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-interface/range {v16 .. v17}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 482
    :cond_f
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_10

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "top parse: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 484
    :cond_10
    const/4 v13, 0x0

    .line 485
    .local v13, "treatFieldIdAsTopLevel":Z
    if-eqz v9, :cond_13

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->isSkipParseType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_12

    .line 487
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_11

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "skipping DM because of fieldId "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->incomingFieldId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " and parseType "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 488
    :cond_11
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 491
    :cond_12
    const/4 v13, 0x1

    .line 495
    :cond_13
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->doPassthrough(Lcom/vlingo/dialog/DMContext;)Z

    move-result v16

    if-eqz v16, :cond_15

    .line 496
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v16

    if-eqz v16, :cond_14

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "DM passthrough for fieldId="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string/jumbo v18, " and parseType="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 497
    :cond_14
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 498
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isPassthrough:Z

    .line 499
    new-instance v16, Lcom/vlingo/dialog/goal/model/PassthroughGoal;

    invoke-direct/range {v16 .. v16}, Lcom/vlingo/dialog/goal/model/PassthroughGoal;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    goto/16 :goto_0

    .line 503
    :cond_15
    if-nez v8, :cond_16

    if-nez v9, :cond_16

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->willHijackParse(Lcom/vlingo/dialog/DMContext;)Z

    move-result v16

    if-nez v16, :cond_16

    .line 504
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    goto/16 :goto_0

    .line 508
    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/vlingo/dialog/DMContext;->addUtteranceGoal(Ljava/lang/String;)V

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 511
    invoke-direct/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->modeConversion()V

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "activePath":Ljava/lang/String;
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_17

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "active: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 514
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 515
    .local v2, "active":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/vlingo/dialog/DMContext;->hijack(Lcom/vlingo/dialog/manager/model/TopFormManager;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 516
    invoke-virtual {v12, v4}, Lcom/vlingo/dialog/manager/model/TopFormManager;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    .line 517
    .local v3, "activeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-nez v13, :cond_18

    invoke-virtual {v12}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1a

    if-eq v3, v12, :cond_1a

    .line 519
    :cond_18
    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual/range {v16 .. v16}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v16

    if-eqz v16, :cond_19

    sget-object v16, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "resetting to top level because fieldId "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 520
    :cond_19
    move-object v3, v12

    .line 521
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 522
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 523
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/DMContext;->getFieldId()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 527
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 530
    move-object/from16 v0, p0

    invoke-interface {v3, v0, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 525
    :cond_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/vlingo/dialog/DMContext;->setSaneFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;)V

    goto :goto_5
.end method

.method public processRecognition(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V
    .locals 1
    .param p1, "nluAction"    # Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/common/message/MNode;Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 392
    return-void
.end method

.method public processRecognition(Ljava/lang/String;)V
    .locals 5
    .param p1, "nluActionString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 399
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v1

    .line 400
    .local v1, "codec":Lcom/vlingo/common/message/Codec;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string/jumbo v4, "UTF-8"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 401
    .local v2, "is":Ljava/io/InputStream;
    invoke-interface {v1, v2}, Lcom/vlingo/common/message/Codec;->decode(Ljava/io/InputStream;)Lcom/vlingo/common/message/MNode;

    move-result-object v3

    .line 402
    .local v3, "root":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 403
    sget-object v4, Lcom/vlingo/dialog/DMContext;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static {v3, v4}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v0

    check-cast v0, Lcom/vlingo/voicepad/tagtoaction/model/Action;

    .line 404
    .local v0, "action":Lcom/vlingo/voicepad/tagtoaction/model/Action;
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->processRecognition(Lcom/vlingo/voicepad/tagtoaction/model/Action;)V

    .line 405
    return-void
.end method

.method public removeUtteranceGoal()V
    .locals 3

    .prologue
    .line 648
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/Goal;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 649
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/Goal;

    .line 650
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v2, v0, Lcom/vlingo/dialog/goal/model/UtteranceGoal;

    if-eqz v2, :cond_0

    .line 651
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 654
    .end local v0    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    :cond_1
    return-void
.end method

.method public resetToTop(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v3, 0x1

    .line 1010
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/TopFormManager;

    .line 1011
    .local v1, "topManager":Lcom/vlingo/dialog/manager/model/TopFormManager;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 1012
    .local v0, "topForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {v1, p0, v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->removeTasks(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 1013
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 1014
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TopFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 1015
    invoke-direct {p0, v3}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 1016
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->history:Lcom/vlingo/dialog/state/model/History;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/History;->reset()V

    .line 1019
    iput-boolean v3, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    .line 1020
    return-void
.end method

.method public setActiveForm(Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 566
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->wasResetToTop:Z

    if-nez v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 569
    :cond_0
    return-void
.end method

.method public setActiveParse(Lcom/vlingo/dialog/util/Parse;)V
    .locals 0
    .param p1, "activeParse"    # Lcom/vlingo/dialog/util/Parse;

    .prologue
    .line 592
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->activeParse:Lcom/vlingo/dialog/util/Parse;

    .line 593
    return-void
.end method

.method public setAnnotateContacts(Z)V
    .locals 0
    .param p1, "annotateContacts"    # Z

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/vlingo/dialog/DMContext;->annotateContacts:Z

    .line 236
    return-void
.end method

.method public setConfirmCancel()V
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "cancel"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 782
    return-void
.end method

.method public setConfirmExecute()V
    .locals 2

    .prologue
    .line 777
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "execute"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 778
    return-void
.end method

.method public setContactClearCache(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 984
    if-eqz p1, :cond_0

    .line 985
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "contact_clear_cache"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 989
    :goto_0
    return-void

    .line 987
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v1, "contact_clear_cache"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDialogMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "dialogMode"    # Ljava/lang/String;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->dialogMode:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 576
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 577
    return-void
.end method

.method public setListPosition(Lcom/vlingo/dialog/manager/model/ListPosition;)V
    .locals 0
    .param p1, "listPosition"    # Lcom/vlingo/dialog/manager/model/ListPosition;

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->listPosition:Lcom/vlingo/dialog/manager/model/ListPosition;

    .line 1024
    return-void
.end method

.method public setListSize(I)V
    .locals 1
    .param p1, "listSize"    # I

    .prologue
    .line 1047
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->listSize:Ljava/lang/Integer;

    .line 1048
    return-void
.end method

.method public setNeedRecognition(Z)V
    .locals 0
    .param p1, "needRecognition"    # Z

    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/vlingo/dialog/DMContext;->needRecognition:Z

    .line 270
    return-void
.end method

.method public setSaneFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;)V
    .locals 5
    .param p1, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v0

    .line 543
    .local v0, "incomingFieldId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 544
    invoke-interface {p1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v1

    .line 545
    .local v1, "myFieldId":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1, v0}, Lcom/vlingo/dialog/DMContext;->anyCompatibleFieldId(Lcom/vlingo/dialog/manager/model/IFormManager;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 546
    sget-object v2, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "incompatible fieldId ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "): setting to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 547
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 550
    .end local v1    # "myFieldId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setSoftwareVersion(Ljava/lang/String;)V
    .locals 3
    .param p1, "softwareVersion"    # Ljava/lang/String;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/vlingo/dialog/DMContext;->softwareVersion:Ljava/lang/String;

    .line 221
    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->config:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {v2}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getContactAnnotateMinSoftwareVersion()Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "contactAnnotateMinSoftwareVersion":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 223
    if-eqz p1, :cond_1

    invoke-static {p1, v0}, Lcom/vlingo/voicepad/workflow/WorkflowManager;->compareVersions(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    const/4 v1, 0x1

    .line 226
    .local v1, "doAnnotateContacts":Z
    :goto_0
    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/DMContext;->setAnnotateContacts(Z)V

    .line 228
    .end local v1    # "doAnnotateContacts":Z
    :cond_0
    return-void

    .line 223
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setupTopLevelState()V
    .locals 3

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->isConfirmCancel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->isConfirmExecute()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "clearing extraneous confirm state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 246
    :cond_1
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 248
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 249
    return-void
.end method

.method public startOutsideTask(Lcom/vlingo/dialog/util/Parse;)V
    .locals 1
    .param p1, "parse"    # Lcom/vlingo/dialog/util/Parse;

    .prologue
    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow:Z

    .line 253
    invoke-virtual {p1}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->startOutsideTask(Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/vlingo/dialog/DMContext;->goalList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->goalsFinished:Z

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vlingo/dialog/DMContext;->fieldId:Ljava/lang/String;

    .line 262
    return-void
.end method

.method public startOutsideTask(Ljava/lang/String;)V
    .locals 4
    .param p1, "parseType"    # Ljava/lang/String;

    .prologue
    .line 621
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "starting outside task for parse type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 622
    :cond_0
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 623
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "bad active form for outside task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v3}, Lcom/vlingo/dialog/state/model/State;->getActiveForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " - resetting to top"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 624
    :cond_1
    iget-object v1, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    iget-object v2, p0, Lcom/vlingo/dialog/DMContext;->state:Lcom/vlingo/dialog/state/model/State;

    invoke-virtual {v2}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 626
    :cond_2
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 627
    new-instance v0, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;-><init>()V

    .line 628
    .local v0, "item":Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;->setTaskName(Ljava/lang/String;)V

    .line 629
    invoke-direct {p0, v0}, Lcom/vlingo/dialog/DMContext;->addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V

    .line 630
    return-void
.end method

.method public startTask(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "taskName"    # Ljava/lang/String;
    .param p2, "clearCachedDateTime"    # Z

    .prologue
    .line 633
    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/DMContext;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "starting task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 634
    :cond_0
    invoke-direct {p0, p2}, Lcom/vlingo/dialog/DMContext;->startTaskCleanup(Z)V

    .line 635
    new-instance v0, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;-><init>()V

    .line 636
    .local v0, "item":Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/state/model/StartDialogHistoryItem;->setTaskName(Ljava/lang/String;)V

    .line 637
    invoke-direct {p0, v0}, Lcom/vlingo/dialog/DMContext;->addToHistory(Lcom/vlingo/dialog/state/model/HistoryItem;)V

    .line 638
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->clearDisambig(Lcom/vlingo/dialog/DMContext;)V

    .line 639
    return-void
.end method

.method public stateWasReset()Z
    .locals 1

    .prologue
    .line 881
    iget-boolean v0, p0, Lcom/vlingo/dialog/DMContext;->stateWasReset:Z

    return v0
.end method
