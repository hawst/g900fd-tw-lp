.class public Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;
.source "AlarmLookupTaskFormManager.java"


# instance fields
.field protected deleteParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected editParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    .line 21
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->setSeamlessEscape(Z)V

    .line 25
    return-void
.end method

.method private generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .prologue
    const/4 v10, 0x0

    .line 110
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getNumMatches()I

    move-result v4

    .line 111
    .local v4, "numMatches":I
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v7

    .line 112
    .local v7, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    invoke-virtual {p0, p1, v7}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 113
    .local v9, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 114
    .local v8, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v8, v9}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 115
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v8, v1, v10}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 116
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 117
    invoke-static {p1, v7}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 118
    return-void
.end method

.method private processAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .prologue
    const/4 v3, 0x0

    .line 90
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 91
    const-class v1, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    invoke-virtual {p1, p3, v1}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 92
    invoke-static {p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->filterAlarms(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 93
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getNumMatches()I

    move-result v0

    .line 94
    .local v0, "numMatches":I
    if-nez v0, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getFullBackoff()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 99
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 107
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, p2, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0

    .line 105
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    goto :goto_0
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 15
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 121
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 122
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v12

    .line 123
    .local v12, "id":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->fetchAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    move-result-object v10

    .line 124
    .local v10, "are":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    const/4 v13, 0x0

    .line 125
    .local v13, "matchFound":Z
    invoke-virtual {v10}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/dialog/event/model/Alarm;

    .line 126
    .local v8, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    const/4 v13, 0x1

    .line 128
    const-string/jumbo v1, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 129
    move-object/from16 v0, p2

    invoke-static {v0, v8}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->resolveTimeAmPmFromAlarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    .line 130
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 131
    .local v9, "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 132
    .local v14, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v14, v9}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 133
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 134
    invoke-virtual/range {p1 .. p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 135
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 139
    .end local v8    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v9    # "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    .end local v14    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    if-nez v13, :cond_2

    .line 140
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unmatched id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_2
    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 44
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 50
    const/4 v6, 0x1

    .line 51
    .local v6, "needQuery":Z
    const-string/jumbo v7, "choice"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 52
    .local v4, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 53
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->fetchAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    move-result-object v5

    .line 54
    .local v5, "event":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    if-eqz v5, :cond_0

    .line 55
    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v1

    .line 56
    .local v1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "choice":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v7, v8, v10, v9, v2}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v3

    .line 58
    .local v3, "choiceIndex":I
    if-ltz v3, :cond_0

    .line 60
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 61
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    const-string/jumbo v7, "id"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 62
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 63
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->setNumMatches(I)V

    .line 65
    invoke-direct {p0, p1, p2, v5}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 66
    const/4 v6, 0x0

    .line 69
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v1    # "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    .end local v2    # "choice":Ljava/lang/String;
    .end local v3    # "choiceIndex":I
    :cond_0
    const/4 v7, 0x0

    invoke-interface {v4, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 70
    invoke-interface {v4, v9}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 73
    .end local v5    # "event":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    :cond_1
    if-eqz v6, :cond_2

    .line 74
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 76
    :cond_2
    return-void
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 37
    const-string/jumbo v0, "time"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 38
    return-void
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;->postDeserialize()V

    .line 30
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getEditParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getDeleteParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 80
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    if-eqz v0, :cond_0

    .line 81
    check-cast p3, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->processAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V

    .line 87
    :goto_0
    return-void

    .line 82
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 83
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 85
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 146
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v0

    .line 147
    .local v0, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "parseType":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getDeleteTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->getEditTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmLookupTaskFormManagerBase;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method
