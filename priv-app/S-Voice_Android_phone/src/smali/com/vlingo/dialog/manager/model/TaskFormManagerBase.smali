.class public Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/FormManager;
.source "TaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ActionCompletedTemplate:Ljava/lang/String; = "ActionCompletedTemplate"

.field public static final PROP_ActionFailedTemplate:Ljava/lang/String; = "ActionFailedTemplate"

.field public static final PROP_Capability:Ljava/lang/String; = "Capability"

.field public static final PROP_DisabledTemplates:Ljava/lang/String; = "DisabledTemplates"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_MaxSoftwareVersion:Ljava/lang/String; = "MaxSoftwareVersion"

.field public static final PROP_MinSoftwareVersion:Ljava/lang/String; = "MinSoftwareVersion"

.field public static final PROP_RestoreFieldId:Ljava/lang/String; = "RestoreFieldId"

.field public static final PROP_ScrollAutoListen:Ljava/lang/String; = "ScrollAutoListen"

.field public static final PROP_ScrollErrorAutoListen:Ljava/lang/String; = "ScrollErrorAutoListen"

.field public static final PROP_SeamlessEscape:Ljava/lang/String; = "SeamlessEscape"

.field public static final PROP_SendIncompleteWidget:Ljava/lang/String; = "SendIncompleteWidget"


# instance fields
.field private ActionCompletedTemplate:Ljava/lang/String;

.field private ActionFailedTemplate:Ljava/lang/String;

.field private Capability:Ljava/lang/String;

.field private DisabledTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/DisabledTemplate;",
            ">;"
        }
    .end annotation
.end field

.field private ID:J

.field private MaxSoftwareVersion:Ljava/lang/String;

.field private MinSoftwareVersion:Ljava/lang/String;

.field private RestoreFieldId:Z

.field private ScrollAutoListen:Ljava/lang/Boolean;

.field private ScrollErrorAutoListen:Ljava/lang/Boolean;

.field private SeamlessEscape:Z

.field private SendIncompleteWidget:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/FormManager;-><init>()V

    .line 5
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->RestoreFieldId:Z

    .line 13
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SeamlessEscape:Z

    .line 15
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SendIncompleteWidget:Z

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->DisabledTemplates:Ljava/util/List;

    .line 31
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 113
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getActionCompletedTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ActionCompletedTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getActionFailedTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ActionFailedTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getCapability()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->Capability:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 117
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisabledTemplates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/DisabledTemplate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->DisabledTemplates:Ljava/util/List;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getMaxSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->MaxSoftwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getMinSoftwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->MinSoftwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getRestoreFieldId()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->RestoreFieldId:Z

    return v0
.end method

.method public getScrollAutoListen()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ScrollAutoListen:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getScrollErrorAutoListen()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ScrollErrorAutoListen:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSeamlessEscape()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SeamlessEscape:Z

    return v0
.end method

.method public getSendIncompleteWidget()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SendIncompleteWidget:Z

    return v0
.end method

.method public setActionCompletedTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ActionCompletedTemplate:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setActionFailedTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ActionFailedTemplate:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setCapability(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->Capability:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 108
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ID:J

    .line 109
    return-void
.end method

.method public setMaxSoftwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->MaxSoftwareVersion:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setMinSoftwareVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->MinSoftwareVersion:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setRestoreFieldId(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->RestoreFieldId:Z

    .line 37
    return-void
.end method

.method public setScrollAutoListen(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ScrollAutoListen:Ljava/lang/Boolean;

    .line 93
    return-void
.end method

.method public setScrollErrorAutoListen(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->ScrollErrorAutoListen:Ljava/lang/Boolean;

    .line 100
    return-void
.end method

.method public setSeamlessEscape(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SeamlessEscape:Z

    .line 65
    return-void
.end method

.method public setSendIncompleteWidget(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManagerBase;->SendIncompleteWidget:Z

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
