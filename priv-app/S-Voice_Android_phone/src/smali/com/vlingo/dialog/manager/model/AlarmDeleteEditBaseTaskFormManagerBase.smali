.class public Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;
.source "AlarmDeleteEditBaseTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ChooseTemplate:Ljava/lang/String; = "ChooseTemplate"

.field public static final PROP_FullBackoff:Ljava/lang/String; = "FullBackoff"

.field public static final PROP_ID:Ljava/lang/String; = "ID"


# instance fields
.field private ChooseTemplate:Ljava/lang/String;

.field private FullBackoff:Z

.field private ID:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;-><init>()V

    .line 7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getChooseTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->ChooseTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFullBackoff()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public setChooseTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->ChooseTemplate:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public setFullBackoff(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    .line 26
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/AlarmDeleteEditBaseTaskFormManagerBase;->ID:J

    .line 33
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
