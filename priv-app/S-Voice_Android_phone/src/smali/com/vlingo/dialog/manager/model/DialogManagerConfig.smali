.class public Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
.super Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;
.source "DialogManagerConfig.java"


# static fields
.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field dmFieldIdPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getDmFieldIdPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->dmFieldIdPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getFieldIdPattern()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 15
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getFieldIdPattern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->dmFieldIdPattern:Ljava/util/regex/Pattern;

    .line 17
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfig;->getTop()Lcom/vlingo/dialog/manager/model/TopFormManager;

    move-result-object v0

    .line 18
    .local v0, "top":Lcom/vlingo/dialog/manager/model/TopFormManager;
    if-eqz v0, :cond_1

    .line 19
    invoke-virtual {v0}, Lcom/vlingo/dialog/manager/model/TopFormManager;->postDeserialize()V

    .line 21
    :cond_1
    return-void
.end method
