.class public Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;
.source "CalendarLookupTaskFormManager.java"


# static fields
.field protected static final DATE_ALT_TODAY:Ljava/lang/String; = "today"

.field protected static final DATE_ALT_TOMORROW:Ljava/lang/String; = "tomorrow"

.field protected static final QUERY_HOW_LONG:Ljava/lang/String; = "howlong"

.field protected static final QUERY_WHEN:Ljava/lang/String; = "when"

.field protected static final QUERY_WHERE:Ljava/lang/String; = "where"

.field protected static final QUERY_WHO:Ljava/lang/String; = "who"

.field protected static final SLOT_QUERY:Ljava/lang/String; = "query"


# instance fields
.field protected deleteParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected editParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected queryTemplateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->queryTemplateMap:Ljava/util/Map;

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->setSeamlessEscape(Z)V

    .line 44
    return-void
.end method

.method private static constructQueryPromptTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;
    .locals 4
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointment"    # Lcom/vlingo/dialog/event/model/Appointment;

    .prologue
    .line 177
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/Form;

    .line 178
    .local v1, "templateForm":Lcom/vlingo/dialog/model/Form;
    new-instance v0, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 179
    .local v0, "aptForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v2, "appointment"

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/model/Form;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 181
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v0, p2, v3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->populateAppointment(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;Z)V

    .line 182
    return-object v1
.end method

.method private generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 13
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    .line 99
    const-string/jumbo v0, "query"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 100
    .local v9, "query":Ljava/lang/String;
    const/4 v7, 0x0

    .line 101
    .local v7, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v4

    .line 102
    .local v4, "numMatches":I
    const/4 v0, 0x1

    if-ne v4, v0, :cond_0

    .line 103
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    check-cast v7, Lcom/vlingo/dialog/event/model/Appointment;

    .line 104
    .restart local v7    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-virtual {v7}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 106
    :cond_0
    invoke-direct {p0, v9}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->selectQueryTemplate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 107
    .local v10, "queryTemplate":Ljava/lang/String;
    if-eqz v10, :cond_1

    if-eqz v7, :cond_1

    .line 108
    invoke-static {p1, p2, v7}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->constructQueryPromptTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v11

    .line 109
    .local v11, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v11}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->clearDateIfRange(Lcom/vlingo/dialog/model/IForm;)V

    .line 110
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v10, v11, v0, v1}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 111
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 120
    :goto_0
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 121
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 122
    return-void

    .line 113
    .end local v11    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v8

    .line 114
    .local v8, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    invoke-virtual {p0, p1, v8}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 115
    .local v12, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    const/4 v3, 0x0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v11

    .line 116
    .restart local v11    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p1, v11, v12}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 117
    invoke-static {v11}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->clearDateIfRange(Lcom/vlingo/dialog/model/IForm;)V

    .line 118
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v11, v1, v2}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    const/4 v6, 0x0

    .line 137
    const-string/jumbo v3, "true"

    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v4

    const-string/jumbo v5, "event_no_match_issued"

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 138
    .local v0, "noMatchIssued":Z
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v3

    const-string/jumbo v4, "event_no_match_issued"

    invoke-virtual {v3, v4}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p1, v6}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 141
    const-class v3, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    invoke-virtual {p1, p3, v3}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 142
    invoke-static {p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->filterAppointments(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 143
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v2

    .line 144
    .local v2, "numMatches":I
    if-nez v2, :cond_3

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFullBackoff()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->anyConstraintsSet(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 146
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getNoMatchFoundTemplate()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "noMatchTemplate":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 148
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1, v3, v4, v6}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 149
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v3

    const-string/jumbo v4, "event_no_match_issued"

    const-string/jumbo v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 154
    const-string/jumbo v3, "query"

    const/4 v4, 0x0

    invoke-static {p2, v3, v4}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 165
    .end local v1    # "noMatchTemplate":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 158
    :cond_2
    if-nez v0, :cond_1

    .line 159
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, p2, v4, v6}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0

    .line 163
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    goto :goto_0
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 15
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 186
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 187
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v12

    .line 188
    .local v12, "id":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->fetchAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    move-result-object v10

    .line 189
    .local v10, "are":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    const/4 v13, 0x0

    .line 190
    .local v13, "matchFound":Z
    invoke-virtual {v10}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/dialog/event/model/Appointment;

    .line 191
    .local v8, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const/4 v13, 0x1

    .line 193
    const-string/jumbo v1, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 194
    move-object/from16 v0, p2

    invoke-static {v0, v8}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->resolveTimeAmPmFromAppointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Appointment;)V

    .line 195
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 196
    .local v9, "appointmentList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 197
    .local v14, "templateForm":Lcom/vlingo/dialog/model/IForm;
    move-object/from16 v0, p1

    invoke-static {v0, v14, v9}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 198
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 199
    invoke-virtual/range {p1 .. p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 200
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 204
    .end local v8    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v9    # "appointmentList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    .end local v14    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    if-nez v13, :cond_2

    .line 205
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unmatched id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_2
    return-void
.end method

.method private selectQueryTemplate(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->queryTemplateMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    .local v0, "template":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 170
    sget-object v1, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown query type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -- ignoring"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 172
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 12
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 63
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 69
    const/4 v7, 0x1

    .line 70
    .local v7, "needQuery":Z
    const-string/jumbo v8, "choice"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 71
    .local v4, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 72
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v5

    .line 73
    .local v5, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v5}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v6

    check-cast v6, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .line 74
    .local v6, "event":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    if-eqz v6, :cond_0

    .line 75
    invoke-virtual {v6}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v1

    .line 76
    .local v1, "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "choice":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v8, v9, v11, v10, v2}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v3

    .line 78
    .local v3, "choiceIndex":I
    if-ltz v3, :cond_0

    .line 80
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Appointment;

    .line 81
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    const-string/jumbo v8, "id"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 82
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 83
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-virtual {v6, v11}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->setNumMatches(I)V

    .line 85
    invoke-direct {p0, p1, p2, v6}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 86
    const/4 v7, 0x0

    .line 89
    .end local v0    # "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    .end local v1    # "appointments":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Appointment;>;"
    .end local v2    # "choice":Ljava/lang/String;
    .end local v3    # "choiceIndex":I
    :cond_0
    const/4 v8, 0x0

    invoke-interface {v4, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 90
    invoke-interface {v4, v10}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 93
    .end local v5    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v6    # "event":Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;
    :cond_1
    if-eqz v7, :cond_2

    .line 94
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->addAppointmentQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 96
    :cond_2
    return-void
.end method

.method protected isLookup()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public postDeserialize()V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->postDeserialize()V

    .line 50
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getEditParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getDeleteParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->queryTemplateMap:Ljava/util/Map;

    const-string/jumbo v1, "who"

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getWhoTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->queryTemplateMap:Ljava/util/Map;

    const-string/jumbo v1, "when"

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getWhenTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->queryTemplateMap:Ljava/util/Map;

    const-string/jumbo v1, "where"

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getWhereTemplate()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 126
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    if-eqz v0, :cond_0

    .line 127
    check-cast p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 133
    :goto_0
    return-void

    .line 128
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 129
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 131
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 211
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v0

    .line 212
    .local v0, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "parseType":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getDeleteTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    .line 220
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 216
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->getEditTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method
