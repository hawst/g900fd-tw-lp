.class public Lcom/vlingo/dialog/manager/model/PassthroughBase;
.super Ljava/lang/Object;
.source "PassthroughBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Capabilities:Ljava/lang/String; = "Capabilities"

.field public static final PROP_FieldIds:Ljava/lang/String; = "FieldIds"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_ParseTypes:Ljava/lang/String; = "ParseTypes"


# instance fields
.field private Capabilities:Ljava/lang/String;

.field private FieldIds:Ljava/lang/String;

.field private ID:J

.field private ParseTypes:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/Passthrough;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->Capabilities:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/Passthrough;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFieldIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->FieldIds:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->ID:J

    return-wide v0
.end method

.method public getParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->ParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public setCapabilities(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->Capabilities:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setFieldIds(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->FieldIds:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->ID:J

    .line 42
    return-void
.end method

.method public setParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/PassthroughBase;->ParseTypes:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
