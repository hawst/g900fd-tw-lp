.class public Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;
.super Lcom/vlingo/dialog/manager/model/SlotManager;
.source "DateSlotManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_AllowRanges:Ljava/lang/String; = "AllowRanges"

.field public static final PROP_ID:Ljava/lang/String; = "ID"


# instance fields
.field private AllowRanges:Z

.field private ID:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SlotManager;-><init>()V

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->AllowRanges:Z

    .line 11
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 28
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/DateSlotManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllowRanges()Z
    .locals 1

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->AllowRanges:Z

    return v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 32
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/DateSlotManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->ID:J

    return-wide v0
.end method

.method public setAllowRanges(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->AllowRanges:Z

    .line 17
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/DateSlotManagerBase;->ID:J

    .line 24
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
