.class public Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;
.source "TaskLookupTaskFormManager.java"


# instance fields
.field protected deleteParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected editParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    .line 21
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->setSeamlessEscape(Z)V

    .line 25
    return-void
.end method

.method private generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .prologue
    const/4 v10, 0x0

    .line 113
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getNumMatches()I

    move-result v4

    .line 114
    .local v4, "numMatches":I
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v7

    .line 115
    .local v7, "choices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    invoke-virtual {p0, p1, v7}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 116
    .local v9, "visibleChoices":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    .line 117
    .local v8, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p1, v8, v9}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 118
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v8, v1, v10}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 119
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 120
    invoke-static {p1, v7}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTaskShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 121
    return-void
.end method

.method private processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V
    .locals 15
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .prologue
    .line 124
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 125
    invoke-virtual/range {p3 .. p3}, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;->getChoiceUid()Ljava/lang/String;

    move-result-object v11

    .line 126
    .local v11, "id":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->fetchTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    move-result-object v9

    .line 127
    .local v9, "are":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    const/4 v12, 0x0

    .line 128
    .local v12, "matchFound":Z
    invoke-virtual {v9}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vlingo/dialog/event/model/Task;

    .line 129
    .local v8, "Task":Lcom/vlingo/dialog/event/model/Task;
    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    const/4 v12, 0x1

    .line 131
    const-string/jumbo v1, "id"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-virtual {v8}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 132
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    .line 133
    .local v13, "taskList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v14

    .line 134
    .local v14, "templateForm":Lcom/vlingo/dialog/model/IForm;
    move-object/from16 v0, p1

    invoke-static {v0, v14, v13}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 135
    iget-object v1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->promptTemplateList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v14, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 136
    invoke-virtual/range {p1 .. p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 137
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTaskShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 141
    .end local v8    # "Task":Lcom/vlingo/dialog/event/model/Task;
    .end local v13    # "taskList":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    .end local v14    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    if-nez v12, :cond_2

    .line 142
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unmatched id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :cond_2
    return-void
.end method

.method private processTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 95
    const-class v1, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    invoke-virtual {p1, p3, v1}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 96
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getNumMatches()I

    move-result v0

    .line 97
    .local v0, "numMatches":I
    if-nez v0, :cond_1

    .line 98
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getFullBackoff()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V

    .line 102
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 110
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getNoneFoundTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, p2, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0

    .line 108
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    goto :goto_0
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 48
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 54
    const/4 v4, 0x1

    .line 55
    .local v4, "needQuery":Z
    const-string/jumbo v7, "choice"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 56
    .local v2, "choiceSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 57
    invoke-static {p1}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->fetchTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    move-result-object v3

    .line 58
    .local v3, "event":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    if-eqz v3, :cond_0

    .line 59
    invoke-virtual {v3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v6

    .line 60
    .local v6, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "choice":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v7

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v7, v8, v10, v9, v0}, Lcom/vlingo/dialog/util/Which;->selectChoice(Lcom/vlingo/dialog/manager/model/ListPosition;IZZLjava/lang/String;)I

    move-result v1

    .line 62
    .local v1, "choiceIndex":I
    if-ltz v1, :cond_0

    .line 64
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vlingo/dialog/event/model/Task;

    .line 65
    .local v5, "task":Lcom/vlingo/dialog/event/model/Task;
    const-string/jumbo v7, "id"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {v5}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 66
    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 67
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-virtual {v3, v10}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->setNumMatches(I)V

    .line 69
    invoke-direct {p0, p1, p2, v3}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->generateResponse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    .line 70
    const/4 v4, 0x0

    .line 73
    .end local v0    # "choice":Ljava/lang/String;
    .end local v1    # "choiceIndex":I
    .end local v5    # "task":Lcom/vlingo/dialog/event/model/Task;
    .end local v6    # "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    :cond_0
    const/4 v7, 0x0

    invoke-interface {v2, v7}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 74
    invoke-interface {v2, v9}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 77
    .end local v3    # "event":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    :cond_1
    if-eqz v4, :cond_2

    .line 78
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 80
    :cond_2
    return-void
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 37
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->anySlotsFilled(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const-string/jumbo v0, "title"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 39
    const-string/jumbo v0, "date"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V

    .line 41
    :cond_0
    return-void
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->postDeserialize()V

    .line 30
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getEditParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getDeleteParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 84
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    if-eqz v0, :cond_0

    .line 85
    check-cast p3, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->processTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/TaskResolvedEvent;)V

    .line 91
    :goto_0
    return-void

    .line 86
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    if-eqz v0, :cond_1

    .line 87
    check-cast p3, Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->processChoiceSelectedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ChoiceSelectedEvent;)V

    goto :goto_0

    .line 89
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 148
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v0

    .line 149
    .local v0, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "parseType":Ljava/lang/String;
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->deleteParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getDeleteTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    .line 157
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->editParseTypes:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->getEditTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, p2, v2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method
