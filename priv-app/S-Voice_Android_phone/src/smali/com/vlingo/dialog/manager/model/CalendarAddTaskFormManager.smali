.class public Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;
.source "CalendarAddTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private conflictTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)Lcom/vlingo/dialog/model/IForm;
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    const/4 v3, 0x0

    .line 132
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v4

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v5

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 134
    .local v7, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v7, v0}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 135
    const-string/jumbo v0, "title"

    invoke-interface {v7, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 136
    const-string/jumbo v0, "date"

    invoke-interface {v7, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 137
    const-string/jumbo v0, "time"

    invoke-interface {v7, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 138
    const-string/jumbo v0, "location"

    invoke-interface {v7, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 139
    return-object v7
.end method

.method private dateFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 86
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 87
    .local v2, "dateSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "date":Ljava/lang/String;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 89
    .local v4, "time":Ljava/lang/String;
    if-nez v1, :cond_1

    if-eqz v4, :cond_1

    const-string/jumbo v5, "date"

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v5

    invoke-interface {v5}, Lcom/vlingo/dialog/manager/model/IFormManager;->isRequired()Z

    move-result v5

    if-nez v5, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 92
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    const/4 v5, 0x0

    invoke-static {v0, v5, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 93
    .local v3, "dateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v3, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 94
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 96
    :cond_0
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 99
    .end local v0    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v3    # "dateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_1
    return-void
.end method

.method private processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .prologue
    const/4 v3, 0x0

    .line 42
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->setNeedRecognition(Z)V

    .line 43
    const-class v2, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    invoke-virtual {p1, p3, v2}, Lcom/vlingo/dialog/DMContext;->completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 44
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->getConflictTemplate()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "template":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getNumMatches()I

    move-result v2

    if-lez v2, :cond_0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->conflictTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 48
    .local v1, "templateForm":Lcom/vlingo/dialog/model/IForm;
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 50
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;->getAppointments()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->addAppointmentChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    .line 52
    .end local v1    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 53
    return-void
.end method

.method private resolveAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 58
    const-string/jumbo v8, "date"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "newDate":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 61
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "clientDate":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "clientTime":Ljava/lang/String;
    const-string/jumbo v8, "time"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 65
    .local v7, "newTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 66
    const-string/jumbo v8, "time"

    invoke-virtual {p0, v8}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v6

    .line 67
    .local v6, "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v5, :cond_0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 68
    :cond_0
    invoke-static {v6, v7, v2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 70
    :cond_1
    invoke-static {v6, v7}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 73
    .end local v6    # "newTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    const-string/jumbo v8, "endtime"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 74
    .local v4, "endTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 75
    const-string/jumbo v8, "endtime"

    invoke-virtual {p0, v8}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    .line 76
    .local v3, "endTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v4, v8}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 77
    if-eqz v5, :cond_3

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 78
    :cond_3
    invoke-static {v3, v4, v2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 80
    :cond_4
    invoke-static {v3, v4}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 83
    .end local v3    # "endTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_5
    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->issueConflictDetectionQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 30
    :cond_0
    return-void
.end method

.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 104
    new-instance v0, Lcom/vlingo/dialog/event/model/Appointment;

    invoke-direct {v0}, Lcom/vlingo/dialog/event/model/Appointment;-><init>()V

    .line 105
    .local v0, "appointment":Lcom/vlingo/dialog/event/model/Appointment;
    const-string/jumbo v6, "title"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setTitle(Ljava/lang/String;)V

    .line 106
    const-string/jumbo v6, "date"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setDate(Ljava/lang/String;)V

    .line 107
    const-string/jumbo v6, "time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setTime(Ljava/lang/String;)V

    .line 108
    const-string/jumbo v6, "duration"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setDuration(Ljava/lang/String;)V

    .line 109
    const-string/jumbo v6, "location"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setLocation(Ljava/lang/String;)V

    .line 110
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Appointment;->setAllDay(Z)V

    .line 112
    const-string/jumbo v6, "invitee_list"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 113
    .local v4, "inviteeListForm":Lcom/vlingo/dialog/model/IForm;
    const/4 v2, 0x0

    .line 114
    .local v2, "i":I
    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 115
    .local v3, "invitee":Lcom/vlingo/dialog/model/IForm;
    if-nez v3, :cond_0

    .line 123
    new-instance v5, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v5}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 124
    .local v5, "templateForm":Lcom/vlingo/dialog/model/Form;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 126
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {p1, v5, v6}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->addAppointmentsToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 128
    return-object v5

    .line 118
    .end local v5    # "templateForm":Lcom/vlingo/dialog/model/Form;
    :cond_0
    new-instance v1, Lcom/vlingo/dialog/event/model/Contact;

    invoke-direct {v1}, Lcom/vlingo/dialog/event/model/Contact;-><init>()V

    .line 119
    .local v1, "contact":Lcom/vlingo/dialog/event/model/Contact;
    invoke-interface {v3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/vlingo/dialog/event/model/Contact;->setName(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Appointment;->getInvitees()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->resolveAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 21
    const-string/jumbo v3, "date"

    const-string/jumbo v4, "time"

    const-string/jumbo v5, "duration"

    const-string/jumbo v6, "endtime"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->convertEndTimeToDuration(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->dateFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 23
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 34
    instance-of v0, p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    if-eqz v0, :cond_0

    .line 35
    check-cast p3, Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;

    .end local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManager;->processAppointmentResolvedEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AppointmentResolvedEvent;)V

    .line 39
    :goto_0
    return-void

    .line 37
    .restart local p3    # "event":Lcom/vlingo/dialog/event/model/Event;
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/CalendarAddTaskFormManagerBase;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    goto :goto_0
.end method
