.class public Lcom/vlingo/dialog/manager/model/DurationSlotManager;
.super Lcom/vlingo/dialog/manager/model/DurationSlotManagerBase;
.source "DurationSlotManager.java"


# static fields
.field private static final DURATION_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string/jumbo v0, "\\+(\\d{2}):(\\d{2})(?::\\d{2})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/DurationSlotManager;->DURATION_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/DurationSlotManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 10
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 17
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 18
    .local v5, "originalValue":Ljava/lang/String;
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/DurationSlotManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 19
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 20
    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 21
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "cf":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 23
    sget-object v6, Lcom/vlingo/dialog/manager/model/DurationSlotManager;->DURATION_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 24
    .local v3, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 25
    invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 26
    .local v1, "h":I
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 27
    .local v2, "m":I
    mul-int/lit8 v6, v1, 0x3c

    add-int v4, v6, v2

    .line 28
    .local v4, "minutes":I
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 29
    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 37
    .end local v0    # "cf":Ljava/lang/String;
    .end local v1    # "h":I
    .end local v2    # "m":I
    .end local v3    # "matcher":Ljava/util/regex/Matcher;
    .end local v4    # "minutes":I
    :cond_0
    :goto_0
    return-void

    .line 31
    .restart local v0    # "cf":Ljava/lang/String;
    .restart local v3    # "matcher":Ljava/util/regex/Matcher;
    :cond_1
    sget-object v6, Lcom/vlingo/dialog/manager/model/DurationSlotManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ignoring bad duration cf: \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 32
    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 33
    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    goto :goto_0
.end method
