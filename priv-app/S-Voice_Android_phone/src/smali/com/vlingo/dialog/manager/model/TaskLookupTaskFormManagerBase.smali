.class public Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;
.source "TaskLookupTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DeleteParseTypes:Ljava/lang/String; = "DeleteParseTypes"

.field public static final PROP_DeleteTaskName:Ljava/lang/String; = "DeleteTaskName"

.field public static final PROP_EditParseTypes:Ljava/lang/String; = "EditParseTypes"

.field public static final PROP_EditTaskName:Ljava/lang/String; = "EditTaskName"

.field public static final PROP_FullBackoff:Ljava/lang/String; = "FullBackoff"

.field public static final PROP_ID:Ljava/lang/String; = "ID"


# instance fields
.field private DeleteParseTypes:Ljava/lang/String;

.field private DeleteTaskName:Ljava/lang/String;

.field private EditParseTypes:Ljava/lang/String;

.field private EditTaskName:Ljava/lang/String;

.field private FullBackoff:Z

.field private ID:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;-><init>()V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->FullBackoff:Z

    .line 19
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDeleteParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->DeleteParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getDeleteTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->DeleteTaskName:Ljava/lang/String;

    return-object v0
.end method

.method public getEditParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->EditParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getEditTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->EditTaskName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullBackoff()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->FullBackoff:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public setDeleteParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->DeleteParseTypes:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setDeleteTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->DeleteTaskName:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setEditParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->EditParseTypes:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setEditTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->EditTaskName:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setFullBackoff(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->FullBackoff:Z

    .line 53
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/TaskLookupTaskFormManagerBase;->ID:J

    .line 60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
