.class public Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManagerBase;
.source "TaskBaseTaskFormManager.java"


# static fields
.field protected static final CONSTRAINT_SLOTS:[Ljava/lang/String;

.field protected static final SLOT_CHOICE:Ljava/lang/String; = "choice"

.field protected static final SLOT_DATE:Ljava/lang/String; = "date"

.field protected static final SLOT_ID:Ljava/lang/String; = "id"

.field protected static final SLOT_REMINDER_DATE:Ljava/lang/String; = "reminder_date"

.field protected static final SLOT_REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field protected static final SLOT_TASK_COMPLETE:Ljava/lang/String; = "complete"

.field protected static final SLOT_TASK_DATE:Ljava/lang/String; = "date"

.field protected static final SLOT_TASK_DATE_ALT:Ljava/lang/String; = "date_alt"

.field protected static final SLOT_TASK_LIST:Ljava/lang/String; = "task_list"

.field protected static final SLOT_TASK_REMINDER_DATE:Ljava/lang/String; = "reminder_date"

.field protected static final SLOT_TASK_REMINDER_DATE_ALT:Ljava/lang/String; = "reminder_date_alt"

.field protected static final SLOT_TASK_REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field protected static final SLOT_TASK_TITLE:Ljava/lang/String; = "title"

.field protected static final SLOT_TITLE:Ljava/lang/String; = "title"

.field protected static final TIME_MIDNIGHT:Ljava/lang/String; = "00:00"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "date"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method protected static addTaskChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/TaskChooseGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/TaskChooseGoal;-><init>()V

    .line 191
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/TaskChooseGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/TaskChooseGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 192
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 193
    return-void
.end method

.method protected static addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 146
    invoke-virtual {p2}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 147
    return-void
.end method

.method protected static addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V
    .locals 0
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;
    .param p3, "clearCache"    # Z

    .prologue
    .line 150
    invoke-virtual {p0, p2, p3}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 151
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 152
    return-void
.end method

.method protected static addTaskShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/TaskShowGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/TaskShowGoal;-><init>()V

    .line 197
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/TaskShowGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/TaskShowGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 198
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 199
    return-void
.end method

.method protected static addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V
    .locals 6
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p2, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Task;>;"
    new-instance v2, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v2}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 203
    .local v2, "listForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v5, "task_list"

    invoke-virtual {v2, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 205
    invoke-interface {p1, v2}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 207
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/event/model/Task;

    .line 208
    .local v3, "task":Lcom/vlingo/dialog/event/model/Task;
    new-instance v4, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v4}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 209
    .local v4, "taskForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v2}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 210
    .local v0, "i":I
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/model/Form;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 212
    invoke-static {p0, v4, v3}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->populateTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    goto :goto_0

    .line 214
    .end local v0    # "i":I
    .end local v3    # "task":Lcom/vlingo/dialog/event/model/Task;
    .end local v4    # "taskForm":Lcom/vlingo/dialog/model/Form;
    :cond_0
    return-void
.end method

.method protected static anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 164
    sget-object v0, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 165
    .local v3, "slotName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 166
    sget-object v4, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "slot "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is set"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 167
    :cond_0
    const/4 v4, 0x1

    .line 170
    .end local v3    # "slotName":Ljava/lang/String;
    :goto_1
    return v4

    .line 164
    .restart local v3    # "slotName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 170
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 155
    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    const-string/jumbo v1, "id"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 157
    .local v0, "idSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 158
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 161
    .end local v0    # "idSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method protected static fetchTaskResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 186
    const-class v0, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    return-object v0
.end method

.method protected static populateTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V
    .locals 6
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/event/model/Task;

    .prologue
    const/4 v5, 0x1

    .line 218
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 220
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    const/4 v1, 0x0

    .line 221
    .local v1, "dateAlt":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 222
    const-string/jumbo v1, "today"

    .line 227
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 228
    .local v2, "reminderDateAlt":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getReminderDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 229
    const-string/jumbo v2, "today"

    .line 234
    :cond_1
    :goto_1
    const-string/jumbo v3, "title"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string/jumbo v3, "date"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string/jumbo v3, "date_alt"

    invoke-static {p1, v3, v1}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string/jumbo v3, "reminder_date"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getReminderDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string/jumbo v3, "reminder_date_alt"

    invoke-static {p1, v3, v2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string/jumbo v3, "reminder_time"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getReminderTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string/jumbo v3, "complete"

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getComplete()Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 241
    return-void

    .line 223
    .end local v2    # "reminderDateAlt":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 224
    const-string/jumbo v1, "tomorrow"

    goto :goto_0

    .line 230
    .restart local v2    # "reminderDateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/vlingo/dialog/event/model/Task;->getReminderDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 231
    const-string/jumbo v2, "tomorrow"

    goto :goto_1
.end method

.method protected static relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 174
    sget-object v4, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "relaxing all constraints"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 175
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 176
    .local v3, "slotName":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 178
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method static slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 182
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 140
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->buildTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    move-result-object v0

    .line 141
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/TaskQueryGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->getClearCache()Z

    move-result v1

    invoke-static {p1, p2, v0, v1}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->addTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 142
    invoke-static {p2, v0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 143
    return-void
.end method

.method protected buildTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/TaskQueryGoal;
    .locals 12
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 100
    const/4 v0, 0x1

    .line 102
    .local v0, "clearCache":Z
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 104
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    new-instance v5, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    invoke-direct {v5}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;-><init>()V

    .line 105
    .local v5, "goal":Lcom/vlingo/dialog/goal/model/TaskQueryGoal;
    const-string/jumbo v8, "title"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setTitle(Ljava/lang/String;)V

    .line 107
    const-string/jumbo v8, "date"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "date":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 110
    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setMatchUndated(Z)V

    .line 129
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->getLimit()I

    move-result v2

    .line 130
    .local v2, "count":I
    if-eqz v2, :cond_0

    .line 131
    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v5, v8}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setCount(Ljava/lang/Integer;)V

    .line 134
    :cond_0
    invoke-virtual {v5, v10}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setClearCache(Z)V

    .line 136
    return-object v5

    .line 113
    .end local v2    # "count":I
    :cond_1
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->isRange(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 114
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/DateSlotManager;->splitRange(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 115
    .local v4, "dates":[Ljava/lang/String;
    aget-object v8, v4, v11

    const-string/jumbo v9, "00:00"

    invoke-static {v1, v8, v9}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v7

    .line 116
    .local v7, "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v7, v1}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 117
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->startOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v7

    .line 119
    :cond_2
    aget-object v8, v4, v10

    const-string/jumbo v9, "00:00"

    invoke-static {v1, v8, v9}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/dialog/util/DateTime;->endOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v6

    .line 124
    .end local v4    # "dates":[Ljava/lang/String;
    .local v6, "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    :goto_1
    invoke-virtual {v7}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setRangeStart(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v6}, Lcom/vlingo/dialog/util/DateTime;->getDateTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setRangeEnd(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v5, v11}, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;->setMatchUndated(Z)V

    goto :goto_0

    .line 121
    .end local v6    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    .end local v7    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    :cond_3
    const-string/jumbo v8, "00:00"

    invoke-static {v1, v3, v8}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v7

    .line 122
    .restart local v7    # "rangeStart":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v7}, Lcom/vlingo/dialog/util/DateTime;->endOfDay()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v6

    .restart local v6    # "rangeEnd":Lcom/vlingo/dialog/util/DateTime;
    goto :goto_1
.end method

.method protected fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 244
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->buildTaskQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    move-result-object v1

    .line 245
    .local v1, "queryGoal":Lcom/vlingo/dialog/goal/model/TaskQueryGoal;
    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/DMContext;->fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 246
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-nez v0, :cond_0

    .line 247
    new-instance v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 249
    .restart local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :cond_0
    return-object v0
.end method

.method protected reminderFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v7, 0x1

    .line 62
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->resolveReminderTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 64
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 66
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    const-string/jumbo v6, "reminder_date"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 67
    .local v2, "reminderDateSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "reminderDate":Ljava/lang/String;
    const-string/jumbo v6, "reminder_time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 70
    .local v5, "reminderTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v5}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    if-nez v1, :cond_2

    .line 74
    const/4 v6, 0x0

    invoke-static {v0, v6, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 75
    .local v3, "reminderDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v3, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 76
    invoke-virtual {v3, v7}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 78
    :cond_0
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 94
    .end local v3    # "reminderDateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_1
    :goto_0
    return-void

    .line 81
    :cond_2
    if-eqz v1, :cond_1

    if-nez v4, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->getDefaultReminderTime()Ljava/lang/String;

    move-result-object v4

    .line 84
    invoke-interface {v5, v4}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 85
    invoke-static {v0, v1, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 86
    .restart local v3    # "reminderDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v3, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 87
    invoke-virtual {v3, v7}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    .line 88
    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected resolveReminderTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 45
    const-string/jumbo v6, "reminder_time"

    invoke-virtual {p0, v6}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    .line 46
    .local v4, "reminderTimeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    const-string/jumbo v6, "reminder_time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 48
    .local v5, "reminderTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 49
    .local v1, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getDate()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "clientDate":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "clientTime":Ljava/lang/String;
    const-string/jumbo v6, "reminder_date"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "reminderDate":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 55
    :cond_0
    invoke-static {v4, v5, v2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 57
    :cond_1
    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 58
    return-void
.end method

.method protected scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 268
    .local p3, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .local p4, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    const/4 v3, 0x0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 269
    .local v7, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p1, v7, p3}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 270
    return-object v7
.end method

.method protected scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 255
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .line 257
    .local v1, "event":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    if-eqz v1, :cond_0

    .line 258
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v2

    .line 261
    .end local v1    # "event":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
