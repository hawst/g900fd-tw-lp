.class public Lcom/vlingo/dialog/manager/model/ContactListFormManager;
.super Lcom/vlingo/dialog/manager/model/ContactListFormManagerBase;
.source "ContactListFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManagerBase;-><init>()V

    return-void
.end method

.method private addChildToList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 12
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 47
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/ContactFormManager;

    .line 53
    .local v1, "childManager":Lcom/vlingo/dialog/manager/model/ContactFormManager;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .local v5, "instanceName":Ljava/lang/String;
    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .local v4, "instance":Lcom/vlingo/dialog/model/IForm;
    if-eqz v4, :cond_3

    .line 54
    invoke-static {p3, v4}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->equivalent(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 55
    sget-object v7, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "not adding duplicate to list: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getDuplicateContactTemplate()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "duplicateContactTemplate":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 58
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {p1, v2, v7, v11, v10}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 83
    .end local v2    # "duplicateContactTemplate":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 53
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 64
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getAddedContactTemplate()Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "addedContactTemplate":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 66
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    invoke-virtual {p1, v0, v7, v11, v10}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 70
    :cond_4
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v6

    check-cast v6, Lcom/vlingo/dialog/model/Form;

    .line 71
    .local v6, "newInstance":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v6, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v1, v6}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->removeTemporarySlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 73
    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 76
    invoke-virtual {v1, p3}, Lcom/vlingo/dialog/manager/model/ContactFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 79
    invoke-virtual {p1, v10}, Lcom/vlingo/dialog/DMContext;->setContactClearCache(Z)V

    .line 82
    const/4 v7, 0x1

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    goto :goto_1
.end method


# virtual methods
.method public childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 36
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 42
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 43
    return-void

    .line 39
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->addChildToList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/ContactListFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 21
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getSlots()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 22
    .local v1, "childManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 24
    .local v0, "child":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    invoke-direct {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->addChildToList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 27
    :cond_0
    return-void
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/ContactListFormManager;->getSlots()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 13
    .local v1, "childManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 14
    .local v0, "child":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1, p1, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 15
    return-void
.end method
