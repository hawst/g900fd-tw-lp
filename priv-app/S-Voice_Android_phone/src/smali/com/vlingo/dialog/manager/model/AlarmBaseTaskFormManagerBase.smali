.class public Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/TaskFormManager;
.source "AlarmBaseTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DaysTriggerRepeat:Ljava/lang/String; = "DaysTriggerRepeat"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Limit:Ljava/lang/String; = "Limit"

.field public static final PROP_NoneFoundTemplate:Ljava/lang/String; = "NoneFoundTemplate"


# instance fields
.field private DaysTriggerRepeat:Z

.field private ID:J

.field private Limit:I

.field private NoneFoundTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;-><init>()V

    .line 7
    iput v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    .line 9
    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->DaysTriggerRepeat:Z

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDaysTriggerRepeat()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->DaysTriggerRepeat:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    return v0
.end method

.method public getNoneFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setDaysTriggerRepeat(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->DaysTriggerRepeat:Z

    .line 35
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->ID:J

    .line 42
    return-void
.end method

.method public setLimit(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->Limit:I

    .line 28
    return-void
.end method

.method public setNoneFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
