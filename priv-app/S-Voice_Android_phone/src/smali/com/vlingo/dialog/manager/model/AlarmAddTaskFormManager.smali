.class public Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManagerBase;
.source "AlarmAddTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManager;->resolveTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 14
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManagerBase;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 15
    return-void
.end method

.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 29
    new-instance v0, Lcom/vlingo/dialog/event/model/Alarm;

    invoke-direct {v0}, Lcom/vlingo/dialog/event/model/Alarm;-><init>()V

    .line 30
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    const-string/jumbo v2, "time"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Alarm;->setTime(Ljava/lang/String;)V

    .line 31
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Alarm;->setSet(Ljava/lang/Boolean;)V

    .line 32
    const-string/jumbo v2, "days"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Alarm;->setDays(Ljava/lang/String;)V

    .line 33
    const-string/jumbo v2, "repeat"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/event/model/Alarm;->setRepeat(Ljava/lang/Boolean;)V

    .line 35
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 36
    .local v1, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 38
    return-object v1
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManagerBase;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20
    const/4 v0, 0x0

    .line 23
    :goto_0
    return v0

    .line 22
    :cond_0
    const-string/jumbo v0, "time"

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    const-string/jumbo v1, "time"

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    const-string/jumbo v2, "days"

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/vlingo/dialog/manager/model/AlarmAddTaskFormManager;->resolveDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 23
    const/4 v0, 0x1

    goto :goto_0
.end method
