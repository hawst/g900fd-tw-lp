.class public Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/TaskFormManager;
.source "CalendarBaseTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ConflictIncludeAllDay:Ljava/lang/String; = "ConflictIncludeAllDay"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Limit:Ljava/lang/String; = "Limit"

.field public static final PROP_NoMatchFoundTemplate:Ljava/lang/String; = "NoMatchFoundTemplate"

.field public static final PROP_NoneFoundTemplate:Ljava/lang/String; = "NoneFoundTemplate"

.field public static final PROP_RangeCount:Ljava/lang/String; = "RangeCount"

.field public static final PROP_RangeDays:Ljava/lang/String; = "RangeDays"


# instance fields
.field private ConflictIncludeAllDay:Z

.field private ID:J

.field private Limit:I

.field private NoMatchFoundTemplate:Ljava/lang/String;

.field private NoneFoundTemplate:Ljava/lang/String;

.field private RangeCount:I

.field private RangeDays:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;-><init>()V

    .line 9
    iput v1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->Limit:I

    .line 11
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeDays:I

    .line 13
    const/4 v0, 0x5

    iput v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeCount:I

    .line 15
    iput-boolean v1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->ConflictIncludeAllDay:Z

    .line 21
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 73
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getConflictIncludeAllDay()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->ConflictIncludeAllDay:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->Limit:I

    return v0
.end method

.method public getNoMatchFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->NoMatchFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getNoneFoundTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getRangeCount()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeCount:I

    return v0
.end method

.method public getRangeDays()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeDays:I

    return v0
.end method

.method public setConflictIncludeAllDay(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->ConflictIncludeAllDay:Z

    .line 62
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->ID:J

    .line 69
    return-void
.end method

.method public setLimit(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->Limit:I

    .line 41
    return-void
.end method

.method public setNoMatchFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->NoMatchFoundTemplate:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setNoneFoundTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->NoneFoundTemplate:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setRangeCount(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeCount:I

    .line 55
    return-void
.end method

.method public setRangeDays(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManagerBase;->RangeDays:I

    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
