.class public Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;
.super Ljava/lang/Object;
.source "AbstractFormManager.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/manager/model/AbstractFormManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ParseTypeSpec"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x2e781afc570faf9eL


# instance fields
.field fieldIdPattern:Ljava/util/regex/Pattern;

.field parseTypePattern:Ljava/util/regex/Pattern;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "spec"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string/jumbo v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "items":[Ljava/lang/String;
    array-length v1, v0

    if-ne v1, v4, :cond_1

    .line 61
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->parseTypePattern:Ljava/util/regex/Pattern;

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 63
    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->fieldIdPattern:Ljava/util/regex/Pattern;

    .line 64
    aget-object v1, v0, v4

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->parseTypePattern:Ljava/util/regex/Pattern;

    goto :goto_0
.end method


# virtual methods
.method matches(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;
    .param p2, "parseType"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->parseTypePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->fieldIdPattern:Ljava/util/regex/Pattern;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->fieldIdPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
