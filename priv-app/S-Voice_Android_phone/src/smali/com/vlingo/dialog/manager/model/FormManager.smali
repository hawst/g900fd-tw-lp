.class public Lcom/vlingo/dialog/manager/model/FormManager;
.super Lcom/vlingo/dialog/manager/model/FormManagerBase;
.source "FormManager.java"


# static fields
.field protected static final KEY_DISAMBIG:Ljava/lang/String; = "disambig"

.field private static final SLOT_PROMPT_CHOICES:Ljava/lang/String; = "prompt_choices"

.field private static final SLOT_PROMPT_CHOICES_COUNT:Ljava/lang/String; = "prompt_choices_count"

.field private static final SLOT_PROMPT_CHOICES_INDEX:Ljava/lang/String; = "prompt_choices_index"

.field private static final SLOT_PROMPT_COUNT:Ljava/lang/String; = "prompt_count"

.field private static final SLOT_SCROLL_TYPE:Ljava/lang/String; = "scroll_type"

.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field protected scrollBackwardParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected scrollForwardParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vlingo/dialog/manager/model/FormManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/FormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/FormManagerBase;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollForwardParseTypeSet:Ljava/util/Set;

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollBackwardParseTypeSet:Ljava/util/Set;

    return-void
.end method

.method protected static buildListPositionGoal(Lcom/vlingo/dialog/DMContext;ZLcom/vlingo/dialog/manager/model/ListPosition;I)Lcom/vlingo/dialog/goal/model/ListPositionGoal;
    .locals 6
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "isForward"    # Z
    .param p2, "position"    # Lcom/vlingo/dialog/manager/model/ListPosition;
    .param p3, "listSize"    # I

    .prologue
    const/4 v1, 0x0

    .line 398
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 399
    .local v2, "pageSize":I
    invoke-virtual {p2}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v3

    .line 400
    .local v3, "start":I
    if-eqz p1, :cond_1

    .line 401
    add-int/2addr v3, v2

    .line 402
    if-lt v3, p3, :cond_2

    .line 415
    :cond_0
    :goto_0
    return-object v1

    .line 406
    :cond_1
    if-eqz v3, :cond_0

    .line 409
    sub-int v4, v3, v2

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 411
    :cond_2
    add-int v4, v3, v2

    invoke-static {v4, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 412
    .local v0, "end":I
    new-instance v1, Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    invoke-direct {v1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;-><init>()V

    .line 413
    .local v1, "goal":Lcom/vlingo/dialog/goal/model/ListPositionGoal;
    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->setStartIndex(I)V

    .line 414
    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->setEndIndex(I)V

    goto :goto_0
.end method

.method public static clearDisambig(Lcom/vlingo/dialog/DMContext;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    const-string/jumbo v1, "disambig"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->removeKeyValue(Ljava/lang/String;)V

    .line 321
    return-void
.end method

.method private doProcessParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 11
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v10, 0x1

    .line 128
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->isConfirmExecute()Z

    move-result v2

    .line 129
    .local v2, "isConfirmExecute":Z
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->isConfirmCancel()Z

    move-result v1

    .line 130
    .local v1, "isConfirmCancel":Z
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 132
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->processCancel(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 212
    :cond_0
    :goto_0
    return v10

    .line 136
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->processListScroll(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 140
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v5

    .line 141
    .local v5, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v5}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v6

    .line 143
    .local v6, "parseType":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v8

    if-nez v8, :cond_2

    .line 144
    const/4 v2, 0x0

    .line 145
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 148
    :cond_2
    if-eqz v2, :cond_5

    .line 149
    iget-object v8, p0, Lcom/vlingo/dialog/manager/model/FormManager;->confirmYesParseTypeSet:Ljava/util/Set;

    invoke-interface {v8, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 150
    .local v4, "isYes":Z
    iget-object v8, p0, Lcom/vlingo/dialog/manager/model/FormManager;->confirmNoParseTypeSet:Ljava/util/Set;

    invoke-interface {v8, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 151
    .local v3, "isNo":Z
    if-eqz v4, :cond_4

    .line 152
    instance-of v8, p0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-eqz v8, :cond_3

    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->doExecuteTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v8

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v8, p1, v9, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 158
    :cond_4
    if-eqz v3, :cond_5

    .line 159
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->setConfirmExecute()V

    .line 160
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getConfirmNoTemplate()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "confirmNoTemplate":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v0, p2, v8, v10}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_0

    .line 168
    .end local v0    # "confirmNoTemplate":Ljava/lang/String;
    .end local v3    # "isNo":Z
    .end local v4    # "isYes":Z
    :cond_5
    if-eqz v1, :cond_6

    .line 169
    new-instance v8, Ljava/lang/RuntimeException;

    const-string/jumbo v9, "confirm cancel not implemented"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 172
    :cond_6
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/FormManager;->hasParseType(Lcom/vlingo/dialog/DMContext;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 174
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v8

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v9

    invoke-interface {v8, p1, v9, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 189
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->postFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 194
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 200
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->postResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 205
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->firstRequiredIncompleteSlot(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v7

    .line 206
    .local v7, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v7, :cond_8

    .line 207
    invoke-interface {v7}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-interface {v7, p1, v8}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0

    .line 185
    .end local v7    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_7
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1

    .line 209
    .restart local v7    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_8
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto/16 :goto_0
.end method

.method protected static getDisambig(Lcom/vlingo/dialog/DMContext;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    const-string/jumbo v1, "disambig"

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->getKeyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static setDisambig(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getState()Lcom/vlingo/dialog/state/model/State;

    move-result-object v0

    const-string/jumbo v1, "disambig"

    invoke-virtual {v0, v1, p1}, Lcom/vlingo/dialog/state/model/State;->setKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    return-void
.end method

.method protected static setScrollType(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 437
    const-string/jumbo v0, "scroll_type"

    invoke-static {p0, v0, p1}, Lcom/vlingo/dialog/manager/model/FormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void
.end method


# virtual methods
.method public bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->doProcessParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    return v0
.end method

.method public childClearedCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 98
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .line 99
    .local v0, "childManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->isRequired()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    const/4 v2, 0x0

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 101
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const/4 v2, 0x0

    invoke-interface {p2, v2}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 103
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 104
    .local v1, "parentManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v1, :cond_0

    .line 105
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childClearedCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 109
    .end local v1    # "parentManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_0
    return-void
.end method

.method public childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 82
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 83
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eq v1, p3, :cond_0

    .line 84
    invoke-interface {v2, p1, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 88
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_1
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->firstRequiredIncompleteSlot(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 89
    .restart local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v2, :cond_2

    .line 90
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 94
    :goto_1
    return-void

    .line 92
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1
.end method

.method protected complete(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v3, 0x1

    .line 248
    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 249
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 250
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->issueConfirmation(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v0

    .line 251
    .local v0, "doConfirmExecute":Z
    instance-of v1, p0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-eqz v1, :cond_2

    .line 252
    if-eqz v0, :cond_1

    .line 253
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v3, v2, p2}, Lcom/vlingo/dialog/DMContext;->addTaskGoal(Ljava/lang/String;ZZLcom/vlingo/dialog/model/IForm;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->doExecuteTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 257
    :cond_2
    if-nez v0, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Lcom/vlingo/dialog/manager/model/IFormManager;->childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method

.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 0
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 276
    return-object p2
.end method

.method public createForm()Lcom/vlingo/dialog/model/Form;
    .locals 6

    .prologue
    .line 37
    new-instance v0, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v0}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 38
    .local v0, "f":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setValue(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v1

    .line 41
    .local v1, "fSlots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 42
    .local v4, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->createForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 43
    .local v3, "s":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v3, v0}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 44
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    .end local v3    # "s":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/vlingo/dialog/model/Form;->setModified(Z)V

    .line 47
    return-object v0
.end method

.method public bridge synthetic createForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->createForm()Lcom/vlingo/dialog/model/Form;

    move-result-object v0

    return-object v0
.end method

.method protected doExecuteTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 280
    const/4 v0, 0x0

    .line 281
    .local v0, "autoListen":Z
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getExecuteTemplate()Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, "template":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 283
    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2, v0}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 285
    :cond_0
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 289
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/FormManager;->executeSetFieldId(Lcom/vlingo/dialog/DMContext;)V

    .line 290
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1, v2, v3, v4, p2}, Lcom/vlingo/dialog/DMContext;->addTaskGoal(Ljava/lang/String;ZZLcom/vlingo/dialog/model/IForm;)V

    .line 291
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 292
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearQueries()V

    .line 293
    return-void
.end method

.method protected executeSetFieldId(Lcom/vlingo/dialog/DMContext;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "fieldId":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSticky()Z

    move-result v5

    if-nez v5, :cond_0

    .line 58
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    .line 60
    :cond_0
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->isDisabled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 61
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v1

    .line 62
    .local v1, "parse":Lcom/vlingo/dialog/util/Parse;
    if-eqz v1, :cond_2

    .line 63
    invoke-virtual {v1}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "parseType":Ljava/lang/String;
    iget-object v5, p0, Lcom/vlingo/dialog/manager/model/FormManager;->exceptParseTypeSet:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 66
    .local v4, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 67
    .local v3, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v3, :cond_1

    .line 68
    invoke-interface {v4, p1, v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 74
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "parse":Lcom/vlingo/dialog/util/Parse;
    .end local v2    # "parseType":Ljava/lang/String;
    .end local v3    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v4    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    return-void
.end method

.method protected firstRequiredIncompleteSlot(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 4
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 237
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->isRequired()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 238
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 239
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_0

    invoke-interface {v2, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->isComplete(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 244
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 419
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrolling()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getListPosition()Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected issueConfirmation(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v2, 0x1

    .line 263
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getConfirmTemplate()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "confirmTemplate":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 265
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 266
    .local v1, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getConfirmFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v3, v2}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 267
    invoke-virtual {p1, p2}, Lcom/vlingo/dialog/DMContext;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 268
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->setConfirmExecute()V

    .line 271
    .end local v1    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 301
    invoke-super {p0}, Lcom/vlingo/dialog/manager/model/FormManagerBase;->postDeserialize()V

    .line 302
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollForwardParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollForwardParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/FormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollBackwardParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollBackwardParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/FormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method public postFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public postResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 220
    const/4 v0, 0x1

    return v0
.end method

.method protected processListScroll(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 13
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 338
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v4

    .line 339
    .local v4, "parseType":Ljava/lang/String;
    iget-object v11, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollForwardParseTypeSet:Ljava/util/Set;

    invoke-interface {v11, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 340
    .local v3, "isForward":Z
    iget-object v11, p0, Lcom/vlingo/dialog/manager/model/FormManager;->scrollBackwardParseTypeSet:Ljava/util/Set;

    invoke-interface {v11, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 341
    .local v2, "isBackward":Z
    if-nez v3, :cond_0

    if-eqz v2, :cond_3

    .line 342
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrolling()Z

    move-result v11

    if-ne v11, v10, :cond_9

    .line 343
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->disableIncompleteWidget()V

    .line 344
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getScrollShowUserTurn()Z

    move-result v11

    if-nez v11, :cond_1

    .line 345
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->removeUtteranceGoal()V

    .line 347
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getListPosition()Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v5

    .line 348
    .local v5, "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    if-nez v5, :cond_2

    .line 349
    sget-object v11, Lcom/vlingo/dialog/manager/model/FormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v12, "client did not provide list position"

    invoke-virtual {v11, v12}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 350
    new-instance v5, Lcom/vlingo/dialog/manager/model/ListPosition;

    .end local v5    # "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-direct {v5, v9, v11}, Lcom/vlingo/dialog/manager/model/ListPosition;-><init>(II)V

    .line 352
    .restart local v5    # "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;

    move-result-object v0

    .line 353
    .local v0, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    if-nez v0, :cond_4

    move v9, v10

    .line 380
    .end local v0    # "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .end local v5    # "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_3
    :goto_0
    return v9

    .line 357
    .restart local v0    # "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .restart local v5    # "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {p1, v3, v5, v9}, Lcom/vlingo/dialog/manager/model/FormManager;->buildListPositionGoal(Lcom/vlingo/dialog/DMContext;ZLcom/vlingo/dialog/manager/model/ListPosition;I)Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    move-result-object v1

    .line 358
    .local v1, "goal":Lcom/vlingo/dialog/goal/model/ListPositionGoal;
    if-eqz v1, :cond_5

    .line 360
    invoke-virtual {v1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getStartIndex()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/vlingo/dialog/manager/model/ListPosition;->setStartIndex(I)V

    .line 361
    invoke-virtual {v1}, Lcom/vlingo/dialog/goal/model/ListPositionGoal;->getEndIndex()I

    move-result v9

    invoke-virtual {v5, v9}, Lcom/vlingo/dialog/manager/model/ListPosition;->setEndIndex(I)V

    .line 362
    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/DMContext;->addListPositionGoal(Lcom/vlingo/dialog/goal/model/ListPositionGoal;)V

    .line 364
    :cond_5
    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/FormManager;->viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 365
    .local v8, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    invoke-virtual {p0, p1, p2, v8, v0}, Lcom/vlingo/dialog/manager/model/FormManager;->scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 366
    .local v7, "templateForm":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_7

    .line 367
    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollForwardTemplate()Ljava/lang/String;

    move-result-object v6

    .line 368
    .local v6, "template":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/DMContext;->getScrollAutoListen(Lcom/vlingo/dialog/manager/model/IFormManager;)Z

    move-result v11

    invoke-virtual {p1, v6, v7, v9, v11}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    :goto_2
    move v9, v10

    .line 373
    goto :goto_0

    .line 367
    .end local v6    # "template":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollBackwardTemplate()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 370
    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollForwardErrorTemplate()Ljava/lang/String;

    move-result-object v6

    .line 371
    .restart local v6    # "template":Ljava/lang/String;
    :goto_3
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, p0}, Lcom/vlingo/dialog/DMContext;->getScrollErrorAutoListen(Lcom/vlingo/dialog/manager/model/IFormManager;)Z

    move-result v11

    invoke-virtual {p1, v6, v7, v9, v11}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    goto :goto_2

    .line 370
    .end local v6    # "template":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrollBackwardErrorTemplate()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .end local v0    # "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .end local v1    # "goal":Lcom/vlingo/dialog/goal/model/ListPositionGoal;
    .end local v5    # "position":Lcom/vlingo/dialog/manager/model/ListPosition;
    .end local v7    # "templateForm":Lcom/vlingo/dialog/model/IForm;
    .end local v8    # "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    :cond_9
    move v9, v10

    .line 377
    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->doProcessParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    .line 53
    return-void
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 225
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 226
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v1, :cond_0

    .line 227
    invoke-interface {v2, p1, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    const/4 v3, 0x0

    .line 232
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 118
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->isComplete(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "prompt for complete form"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 121
    :cond_0
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/FormManager;->firstRequiredIncompleteSlot(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .line 122
    .local v0, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 123
    return-void
.end method

.method protected scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 393
    .local p3, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .local p4, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method protected setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "choices"    # Ljava/lang/String;
    .param p4, "numMatches"    # I
    .param p5, "listSize"    # I
    .param p6, "visibleSize"    # I

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrolling()Z

    move-result v2

    .line 424
    .local v2, "scrolling":Z
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/FormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 425
    .local v3, "taskForm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v4, "prompt_count"

    if-eqz v2, :cond_1

    .end local p5    # "listSize":I
    :goto_0
    invoke-static {v3, v4, p5}, Lcom/vlingo/dialog/manager/model/FormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;I)V

    .line 426
    const-string/jumbo v4, "prompt_choices_count"

    invoke-static {v3, v4, p6}, Lcom/vlingo/dialog/manager/model/FormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;I)V

    .line 427
    const-string/jumbo v4, "prompt_choices"

    invoke-static {v3, v4, p3}, Lcom/vlingo/dialog/manager/model/FormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    if-eqz v2, :cond_0

    .line 429
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/FormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v1

    .line 430
    .local v1, "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 431
    .local v0, "index":I
    :goto_1
    const-string/jumbo v4, "prompt_choices_index"

    invoke-static {v3, v4, v0}, Lcom/vlingo/dialog/manager/model/FormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;I)V

    .line 433
    .end local v0    # "index":I
    .end local v1    # "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_0
    return-object v3

    .restart local p5    # "listSize":I
    :cond_1
    move p5, p4

    .line 425
    goto :goto_0

    .line 430
    .end local p5    # "listSize":I
    .restart local v1    # "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    :cond_2
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v0

    goto :goto_1
.end method

.method protected viewableSublist(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v3, 0x1

    .line 324
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/FormManager;->getScrolling()Z

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 325
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/FormManager;->getListPosition(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v1

    .line 326
    .local v1, "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vlingo/dialog/manager/model/ListPosition;->isValid(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 327
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->getStartIndex()I

    move-result v2

    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->getEndIndex()I

    move-result v3

    invoke-interface {p2, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    .line 334
    .end local v1    # "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    .end local p2    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    :goto_0
    return-object p2

    .line 329
    .restart local v1    # "listPosition":Lcom/vlingo/dialog/manager/model/ListPosition;
    .restart local p2    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v0

    .line 330
    .local v0, "clientListSize":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 331
    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getListSize()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {p2, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    goto :goto_0
.end method
