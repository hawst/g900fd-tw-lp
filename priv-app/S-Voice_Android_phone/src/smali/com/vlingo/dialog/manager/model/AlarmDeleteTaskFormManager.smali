.class public Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManagerBase;
.source "AlarmDeleteTaskFormManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private confirmationTemplateForm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)Lcom/vlingo/dialog/model/IForm;
    .locals 2
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 31
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 32
    .local v0, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 34
    return-object v0
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 16
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 17
    .local v3, "id":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v1

    .line 19
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .line 20
    .local v4, "results":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 21
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 22
    invoke-direct {p0, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmDeleteTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 26
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v1    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "results":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    goto :goto_0
.end method
