.class public Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;
.source "AlarmBaseTaskFormManager.java"


# static fields
.field protected static final CONSTRAINT_SLOTS:[Ljava/lang/String;

.field protected static final SLOT_ALARM_DAYS:Ljava/lang/String; = "days"

.field protected static final SLOT_ALARM_ENABLED:Ljava/lang/String; = "enabled"

.field protected static final SLOT_ALARM_LIST:Ljava/lang/String; = "alarm_list"

.field protected static final SLOT_ALARM_REPEAT:Ljava/lang/String; = "repeat"

.field protected static final SLOT_ALARM_TIME:Ljava/lang/String; = "time"

.field protected static final SLOT_CHOICE:Ljava/lang/String; = "choice"

.field protected static final SLOT_DAYS:Ljava/lang/String; = "days"

.field protected static final SLOT_ID:Ljava/lang/String; = "id"

.field protected static final SLOT_NEW_DAYS:Ljava/lang/String; = "newdays"

.field protected static final SLOT_REPEAT:Ljava/lang/String; = "repeat"

.field protected static final SLOT_TIME:Ljava/lang/String; = "time"

.field protected static final dayMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;>;"
        }
    .end annotation
.end field

.field protected static final relativeDayMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static final repeatSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final repeatSetJProject:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "days"

    aput-object v1, v0, v2

    const-string/jumbo v1, "time"

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->dayMap:Ljava/util/Map;

    .line 60
    new-instance v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->relativeDayMap:Ljava/util/Map;

    .line 74
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "repeat"

    aput-object v1, v0, v2

    const-string/jumbo v1, "daily"

    aput-object v1, v0, v3

    const-string/jumbo v1, "weekday"

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->repeatSetJProject:Ljava/util/Set;

    .line 79
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "repeat"

    aput-object v1, v0, v2

    const-string/jumbo v1, "daily"

    aput-object v1, v0, v3

    const-string/jumbo v1, "weekday"

    aput-object v1, v0, v4

    const-string/jumbo v1, "weekend"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string/jumbo v2, "sun"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "mon"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "tue"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "wed"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "thu"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "fri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "sat"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->repeatSet:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;-><init>()V

    return-void
.end method

.method protected static addAlarmChooseGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/AlarmChooseGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/AlarmChooseGoal;-><init>()V

    .line 372
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AlarmChooseGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AlarmChooseGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 373
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 374
    return-void
.end method

.method protected static addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 321
    invoke-virtual {p2}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 322
    return-void
.end method

.method protected static addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V
    .locals 0
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;
    .param p3, "clearCache"    # Z

    .prologue
    .line 325
    invoke-virtual {p0, p2, p3}, Lcom/vlingo/dialog/DMContext;->addQueryGoal(Lcom/vlingo/dialog/goal/model/QueryGoal;Z)V

    .line 326
    invoke-static {p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 327
    return-void
.end method

.method protected static addAlarmShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    new-instance v0, Lcom/vlingo/dialog/goal/model/AlarmShowGoal;

    invoke-direct {v0}, Lcom/vlingo/dialog/goal/model/AlarmShowGoal;-><init>()V

    .line 378
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AlarmShowGoal;
    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AlarmShowGoal;->getChoices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 379
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->addGoal(Lcom/vlingo/dialog/goal/model/Goal;)V

    .line 380
    return-void
.end method

.method protected static addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V
    .locals 6
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    new-instance v4, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v4}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 384
    .local v4, "listForm":Lcom/vlingo/dialog/model/Form;
    const-string/jumbo v5, "alarm_list"

    invoke-virtual {v4, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 386
    invoke-interface {p0, v4}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 388
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 389
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    new-instance v1, Lcom/vlingo/dialog/model/Form;

    invoke-direct {v1}, Lcom/vlingo/dialog/model/Form;-><init>()V

    .line 390
    .local v1, "alarmForm":Lcom/vlingo/dialog/model/Form;
    invoke-virtual {v4}, Lcom/vlingo/dialog/model/Form;->getSlots()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    .line 391
    .local v2, "i":I
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/vlingo/dialog/model/Form;->setName(Ljava/lang/String;)V

    .line 392
    invoke-virtual {v4, v1}, Lcom/vlingo/dialog/model/Form;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 393
    invoke-static {v1, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->populateAlarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V

    goto :goto_0

    .line 395
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v1    # "alarmForm":Lcom/vlingo/dialog/model/Form;
    .end local v2    # "i":I
    :cond_0
    return-void
.end method

.method protected static anyConstraints(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 7
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 345
    sget-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 346
    .local v3, "slotName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 347
    sget-object v4, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "slot "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " is set"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 348
    :cond_0
    const/4 v4, 0x1

    .line 351
    .end local v3    # "slotName":Ljava/lang/String;
    :goto_1
    return v4

    .line 345
    .restart local v3    # "slotName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 351
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected static clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "goal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 336
    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    const-string/jumbo v1, "id"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 338
    .local v0, "idSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 339
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 342
    .end local v0    # "idSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    return-void
.end method

.method protected static fetchAlarmResolvedEvent(Lcom/vlingo/dialog/DMContext;)Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 367
    const-class v0, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/DMContext;->fetchQueryEvent(Ljava/lang/Class;)Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    return-object v0
.end method

.method protected static filterAlarms(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;)V
    .locals 11
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "event"    # Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v2

    .line 208
    .local v2, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    invoke-static {v2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->fixAlarms(Ljava/util/List;)V

    .line 209
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 210
    const-string/jumbo v8, "time"

    invoke-interface {p0, v8}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 211
    .local v7, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v7}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 212
    .local v4, "time":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 213
    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToAm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 214
    .local v5, "timeAm":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToPm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 215
    .local v6, "timePm":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/event/model/Alarm;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 216
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 217
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "alarmTime":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 219
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 222
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v1    # "alarmTime":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->setNumMatches(I)V

    .line 223
    sget-object v8, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v8}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "filtered alarms form AM/PM: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 226
    .end local v3    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/event/model/Alarm;>;"
    .end local v4    # "time":Ljava/lang/String;
    .end local v5    # "timeAm":Ljava/lang/String;
    .end local v6    # "timePm":Ljava/lang/String;
    .end local v7    # "timeSlot":Lcom/vlingo/dialog/model/IForm;
    :cond_2
    return-void
.end method

.method private static fixAlarms(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Alarm;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/Alarm;

    .line 186
    .local v0, "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v6

    .line 187
    .local v6, "time":Ljava/lang/String;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x3a

    if-ne v7, v8, :cond_2

    .line 188
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 189
    sget-object v7, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v7}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v7, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "fixing alarm time: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " -> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 190
    :cond_1
    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/event/model/Alarm;->setTime(Ljava/lang/String;)V

    .line 193
    :cond_2
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/Alarm;->getDays()Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, "days":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 195
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 196
    .local v2, "daySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->splitListBySpace(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 197
    .local v1, "day":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199
    .end local v1    # "day":Ljava/lang/String;
    :cond_3
    const/16 v7, 0x20

    invoke-static {v7}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 200
    invoke-virtual {v0, v3}, Lcom/vlingo/dialog/event/model/Alarm;->setDays(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 203
    .end local v0    # "alarm":Lcom/vlingo/dialog/event/model/Alarm;
    .end local v2    # "daySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "days":Ljava/lang/String;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "time":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private static getRelativeDay(Lcom/vlingo/dialog/DMContext;I)Lcom/vlingo/dialog/util/Day;
    .locals 1
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "dayOffset"    # I

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getDay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/util/Day;->get(Ljava/lang/String;)Lcom/vlingo/dialog/util/Day;

    move-result-object v0

    return-object v0
.end method

.method private getRepeatSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getDaysTriggerRepeat()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->repeatSet:Ljava/util/Set;

    .line 96
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->repeatSetJProject:Ljava/util/Set;

    goto :goto_0
.end method

.method private static overlapsTodayWithoutRepeat(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 4
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "daysSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "repeatSlot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 259
    if-eqz p2, :cond_0

    const-string/jumbo v2, "true"

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    const/4 v2, 0x0

    .line 264
    :goto_0
    return v2

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/dialog/util/DateTime;->getDay()Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "today":Ljava/lang/String;
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "days":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_0
.end method

.method protected static populateAlarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 398
    const-string/jumbo v0, "time"

    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string/jumbo v0, "enabled"

    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Alarm;->getSet()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 400
    const-string/jumbo v0, "days"

    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Alarm;->getDays()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string/jumbo v0, "repeat"

    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Alarm;->getRepeat()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 402
    return-void
.end method

.method private processDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 12
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "daysSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "repeatSlot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 117
    if-eqz p2, :cond_6

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 118
    invoke-static {}, Lcom/vlingo/dialog/util/Day;->newSet()Ljava/util/EnumSet;

    move-result-object v1

    .line 119
    .local v1, "daySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    const/4 v4, 0x0

    .line 120
    .local v4, "lastDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    const/4 v5, 0x0

    .line 121
    .local v5, "lastToken":Ljava/lang/String;
    const/4 v2, 0x0

    .line 122
    .local v2, "error":Z
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->splitListBySpace(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 123
    .local v7, "token":Ljava/lang/String;
    if-eqz p3, :cond_0

    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getRepeatSet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 124
    const-string/jumbo v9, "true"

    invoke-interface {p3, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 125
    const/4 v9, 0x1

    invoke-interface {p3, v9}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 127
    :cond_0
    sget-object v9, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->relativeDayMap:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 128
    .local v6, "relativeDay":Ljava/lang/Integer;
    if-eqz v6, :cond_2

    .line 129
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {p1, v9}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getRelativeDay(Lcom/vlingo/dialog/DMContext;I)Lcom/vlingo/dialog/util/Day;

    move-result-object v0

    .line 130
    .local v0, "day":Lcom/vlingo/dialog/util/Day;
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v8

    .line 132
    .local v8, "tokenDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    invoke-static {p1, v5, v1, v4, v8}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->processRangeInclusive(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/EnumSet;)V

    .line 133
    move-object v4, v8

    .line 150
    .end local v0    # "day":Lcom/vlingo/dialog/util/Day;
    .end local v8    # "tokenDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    :cond_1
    :goto_1
    move-object v5, v7

    .line 151
    goto :goto_0

    .line 134
    :cond_2
    const-string/jumbo v9, "repeat"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 136
    move-object v7, v5

    goto :goto_1

    .line 137
    :cond_3
    const-string/jumbo v9, "thru"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 140
    sget-object v9, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->dayMap:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/EnumSet;

    .line 141
    .restart local v8    # "tokenDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    if-nez v8, :cond_4

    .line 142
    sget-object v9, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "bad day CF element: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 143
    const/4 v2, 0x1

    goto :goto_1

    .line 145
    :cond_4
    invoke-virtual {v1, v8}, Ljava/util/EnumSet;->addAll(Ljava/util/Collection;)Z

    .line 146
    invoke-static {p1, v5, v1, v4, v8}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->processRangeInclusive(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/EnumSet;)V

    .line 147
    move-object v4, v8

    goto :goto_1

    .line 152
    .end local v6    # "relativeDay":Ljava/lang/Integer;
    .end local v7    # "token":Ljava/lang/String;
    .end local v8    # "tokenDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    :cond_5
    if-eqz v2, :cond_7

    .line 153
    sget-object v9, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "ignoring entire day CF \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "\" due to error"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 154
    const/4 v9, 0x0

    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 155
    const/4 v9, 0x0

    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setFilled(Z)V

    .line 160
    .end local v1    # "daySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .end local v2    # "error":Z
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "lastDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .end local v5    # "lastToken":Ljava/lang/String;
    :cond_6
    :goto_2
    return-void

    .line 157
    .restart local v1    # "daySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .restart local v2    # "error":Z
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "lastDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .restart local v5    # "lastToken":Ljava/lang/String;
    :cond_7
    invoke-static {v1}, Lcom/vlingo/dialog/util/Day;->normalize(Ljava/util/EnumSet;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {p2, v9}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static processRangeInclusive(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;Ljava/util/EnumSet;Ljava/util/EnumSet;Ljava/util/EnumSet;)V
    .locals 4
    .param p0, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p1, "lastToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p2, "daySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .local p3, "startDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    .local p4, "endDaySet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    const-string/jumbo v3, "thru"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    if-nez p3, :cond_0

    .line 166
    const/4 v3, 0x0

    invoke-static {p0, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getRelativeDay(Lcom/vlingo/dialog/DMContext;I)Lcom/vlingo/dialog/util/Day;

    move-result-object v2

    .line 167
    .local v2, "today":Lcom/vlingo/dialog/util/Day;
    invoke-static {v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object p3

    .line 169
    .end local v2    # "today":Lcom/vlingo/dialog/util/Day;
    :cond_0
    invoke-static {p3}, Lcom/vlingo/dialog/util/Day;->min(Ljava/util/EnumSet;)Lcom/vlingo/dialog/util/Day;

    move-result-object v1

    .line 170
    .local v1, "start":Lcom/vlingo/dialog/util/Day;
    invoke-static {p4}, Lcom/vlingo/dialog/util/Day;->max(Ljava/util/EnumSet;)Lcom/vlingo/dialog/util/Day;

    move-result-object v0

    .line 171
    .local v0, "end":Lcom/vlingo/dialog/util/Day;
    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/util/Day;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-gtz v3, :cond_2

    .line 172
    invoke-static {v1, v0}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->addAll(Ljava/util/Collection;)Z

    .line 178
    .end local v0    # "end":Lcom/vlingo/dialog/util/Day;
    .end local v1    # "start":Lcom/vlingo/dialog/util/Day;
    :cond_1
    :goto_0
    return-void

    .line 174
    .restart local v0    # "end":Lcom/vlingo/dialog/util/Day;
    .restart local v1    # "start":Lcom/vlingo/dialog/util/Day;
    :cond_2
    sget-object v3, Lcom/vlingo/dialog/util/Day;->SAT:Lcom/vlingo/dialog/util/Day;

    invoke-static {v1, v3}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->addAll(Ljava/util/Collection;)Z

    .line 175
    sget-object v3, Lcom/vlingo/dialog/util/Day;->SUN:Lcom/vlingo/dialog/util/Day;

    invoke-static {v3, v0}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected static relaxAllConstraints(Lcom/vlingo/dialog/model/IForm;)V
    .locals 6
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 355
    sget-object v4, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "relaxing all constraints"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 356
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->CONSTRAINT_SLOTS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 357
    .local v3, "slotName":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v3    # "slotName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected static resolveTimeAmPmFromAlarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Alarm;)V
    .locals 4
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "alarm"    # Lcom/vlingo/dialog/event/model/Alarm;

    .prologue
    .line 229
    const-string/jumbo v1, "time"

    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 230
    .local v0, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    invoke-virtual {p1}, Lcom/vlingo/dialog/event/model/Alarm;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 232
    sget-object v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v1}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "resolved time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 234
    :cond_0
    return-void
.end method

.method static slotIsSet(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 362
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 363
    .local v0, "slot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 330
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->buildAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    move-result-object v0

    .line 331
    .local v0, "goal":Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    invoke-static {p1, p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->addAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 332
    invoke-static {p2, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->clearIdIfClearCache(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/goal/model/QueryGoal;)V

    .line 333
    return-void
.end method

.method protected buildAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 295
    const/4 v0, 0x1

    .line 297
    .local v0, "clearCache":Z
    new-instance v2, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    invoke-direct {v2}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;-><init>()V

    .line 299
    .local v2, "goal":Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    const-string/jumbo v4, "days"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setDays(Ljava/lang/String;)V

    .line 301
    const-string/jumbo v4, "time"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 302
    .local v3, "time":Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isUnresolved(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 303
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToAm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setRangeStart(Ljava/lang/String;)V

    .line 304
    invoke-static {v3}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveToPm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setRangeEnd(Ljava/lang/String;)V

    .line 310
    :goto_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getLimit()I

    move-result v1

    .line 311
    .local v1, "count":I
    if-eqz v1, :cond_0

    .line 312
    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setCount(Ljava/lang/Integer;)V

    .line 315
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setClearCache(Z)V

    .line 317
    return-object v2

    .line 306
    .end local v1    # "count":I
    :cond_1
    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setRangeStart(Ljava/lang/String;)V

    .line 307
    invoke-virtual {v2, v3}, Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;->setRangeEnd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 405
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->buildAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    move-result-object v2

    .line 406
    .local v2, "queryGoal":Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    invoke-virtual {p1, v2}, Lcom/vlingo/dialog/DMContext;->fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 407
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-nez v0, :cond_0

    .line 408
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 409
    .local v4, "time":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->isResolved(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 410
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 411
    .local v1, "formUnresolvedAmPm":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v5, "time"

    invoke-interface {v1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    invoke-static {v4}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->markUnresolved(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p0, p1, v1}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->buildAlarmQueryGoal(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;

    move-result-object v3

    .line 413
    .local v3, "queryGoalUnresolvedAmPm":Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    invoke-virtual {p1, v3}, Lcom/vlingo/dialog/DMContext;->fetchCompletedQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 416
    .end local v1    # "formUnresolvedAmPm":Lcom/vlingo/dialog/model/IForm;
    .end local v3    # "queryGoalUnresolvedAmPm":Lcom/vlingo/dialog/goal/model/AlarmQueryGoal;
    .end local v4    # "time":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 417
    new-instance v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    const/4 v5, 0x0

    invoke-direct {v0, v2, v5}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 419
    .restart local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :cond_1
    return-object v0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 104
    const-string/jumbo v3, "days"

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 105
    .local v0, "daysSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v3, "newdays"

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 106
    .local v1, "newDaysSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v3, "repeat"

    invoke-interface {p2, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 108
    .local v2, "repeatSlot":Lcom/vlingo/dialog/model/IForm;
    if-nez v1, :cond_0

    .line 109
    invoke-direct {p0, p1, v0, v2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->processDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->processDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    .line 112
    invoke-direct {p0, p1, v1, v2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->processDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0
.end method

.method protected resolveDays(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "timeManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p3, "timeSlot"    # Lcom/vlingo/dialog/model/IForm;
    .param p4, "daysSlot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 269
    if-eqz p4, :cond_0

    .line 270
    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {p4}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 273
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    .line 274
    .local v0, "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, p3, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 275
    invoke-static {p2, p3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 278
    const/4 v3, 0x0

    invoke-interface {p3}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/vlingo/dialog/util/DateTime;->fromDateAndTime(Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v1

    .line 279
    .local v1, "dateTime":Lcom/vlingo/dialog/util/DateTime;
    invoke-virtual {v1, v0}, Lcom/vlingo/dialog/util/DateTime;->isBefore(Lcom/vlingo/dialog/util/DateTime;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 281
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vlingo/dialog/util/DateTime;->addDays(I)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/dialog/util/DateTime;->getDay()Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "day":Ljava/lang/String;
    :goto_0
    invoke-interface {p4, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 289
    .end local v0    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v1    # "dateTime":Lcom/vlingo/dialog/util/DateTime;
    .end local v2    # "day":Ljava/lang/String;
    :cond_0
    return-void

    .line 284
    .restart local v0    # "clientDateTime":Lcom/vlingo/dialog/util/DateTime;
    .restart local v1    # "dateTime":Lcom/vlingo/dialog/util/DateTime;
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/dialog/util/DateTime;->getDay()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "day":Ljava/lang/String;
    goto :goto_0
.end method

.method protected resolveTimeAmPm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 237
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/dialog/util/DateTime;->getTime()Ljava/lang/String;

    move-result-object v0

    .line 238
    .local v0, "clientTime":Ljava/lang/String;
    const-string/jumbo v6, "time"

    invoke-virtual {p0, v6}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v4

    .line 239
    .local v4, "timeManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    const-string/jumbo v6, "time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 240
    .local v5, "timeSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "newdays"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 241
    .local v2, "newDaysSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "days"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 242
    .local v1, "daysSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "repeat"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 243
    .local v3, "repeatSlot":Lcom/vlingo/dialog/model/IForm;
    if-eqz v2, :cond_1

    .line 244
    invoke-static {p1, v2, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->overlapsTodayWithoutRepeat(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 245
    invoke-static {v4, v5, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    .line 255
    :cond_0
    :goto_0
    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z

    .line 256
    return-void

    .line 247
    :cond_1
    if-eqz v1, :cond_2

    .line 248
    invoke-static {p1, v1, v3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->overlapsTodayWithoutRepeat(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 249
    invoke-static {v4, v5, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    goto :goto_0

    .line 253
    :cond_2
    invoke-static {v4, v5, v0}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method protected scrollingBuildTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 8
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 438
    .local p3, "visibleChoices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    .local p4, "choices":Ljava/util/List;, "Ljava/util/List<+Ljava/lang/Object;>;"
    const/4 v3, 0x0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->setChoosePromptSlots(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;III)Lcom/vlingo/dialog/model/IForm;

    move-result-object v7

    .line 439
    .local v7, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v7, p3}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->addAlarmsToTemplateForm(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 440
    return-object v7
.end method

.method protected scrollingFetchList(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AlarmBaseTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 425
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    .line 427
    .local v1, "event":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    if-eqz v1, :cond_0

    .line 428
    invoke-virtual {v1}, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;->getAlarms()Ljava/util/List;

    move-result-object v2

    .line 431
    .end local v1    # "event":Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
