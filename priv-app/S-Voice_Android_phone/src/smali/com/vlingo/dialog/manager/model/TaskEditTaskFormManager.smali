.class public Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManagerBase;
.source "TaskEditTaskFormManager.java"


# static fields
.field private static final SLOT_NEW_COMPLETE:Ljava/lang/String; = "newcomplete"

.field private static final SLOT_NEW_DATE:Ljava/lang/String; = "newdate"

.field private static final SLOT_NEW_TITLE:Ljava/lang/String; = "newtitle"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManagerBase;-><init>()V

    return-void
.end method

.method private confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)Lcom/vlingo/dialog/model/IForm;
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "task"    # Lcom/vlingo/dialog/event/model/Task;

    .prologue
    .line 77
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 78
    .local v0, "templateForm":Lcom/vlingo/dialog/model/IForm;
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->addTasksToTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)V

    .line 80
    return-object v0
.end method


# virtual methods
.method protected confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 6
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 62
    const-string/jumbo v5, "id"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "id":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 64
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->fetchCompletedQuery(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/state/model/CompletedQuery;

    move-result-object v0

    .line 65
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;

    .line 66
    .local v3, "results":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    invoke-virtual {v3}, Lcom/vlingo/dialog/event/model/TaskResolvedEvent;->getTasks()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/event/model/Task;

    .line 67
    .local v4, "task":Lcom/vlingo/dialog/event/model/Task;
    invoke-virtual {v4}, Lcom/vlingo/dialog/event/model/Task;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 68
    invoke-direct {p0, p1, p2, v4}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->confirmationTemplateForm(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 72
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "results":Lcom/vlingo/dialog/event/model/TaskResolvedEvent;
    .end local v4    # "task":Lcom/vlingo/dialog/event/model/Task;
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    goto :goto_0
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 20
    const-string/jumbo v0, "id"

    invoke-interface {p2, v0}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->reminderFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 23
    :cond_0
    return-void
.end method

.method protected foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "task"    # Lcom/vlingo/dialog/event/model/Task;

    .prologue
    .line 29
    invoke-virtual {p0, p2}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->deactivateConstraintSlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 31
    const-string/jumbo v6, "newtitle"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 32
    .local v3, "newTitleSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "newdate"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 33
    .local v2, "newDateSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "newcomplete"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 34
    .local v1, "newCompleteSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "reminder_date"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    .line 35
    .local v4, "reminderDateSlot":Lcom/vlingo/dialog/model/IForm;
    const-string/jumbo v6, "reminder_time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v5

    .line 37
    .local v5, "reminderTimeSlot":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->reminderFill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 39
    const/4 v0, 0x0

    .line 44
    .local v0, "anyChanges":Z
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Task;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v3, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v0, v6

    .line 45
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Task;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v2, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v0, v6

    .line 46
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Task;->getComplete()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p0, v1, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Boolean;)Z

    move-result v6

    or-int/2addr v0, v6

    .line 47
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Task;->getReminderDate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v4, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v0, v6

    .line 48
    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/Task;->getReminderTime()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z

    move-result v6

    or-int/2addr v0, v6

    .line 50
    if-eqz v0, :cond_0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManagerBase;->foundUniqueTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Task;)V

    .line 57
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->getNoChangesTemplate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {p1, v6, p2, v7, v8}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 54
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/vlingo/dialog/manager/model/TaskEditTaskFormManager;->addTaskShowGoal(Lcom/vlingo/dialog/DMContext;Ljava/util/List;)V

    goto :goto_0
.end method
