.class public Lcom/vlingo/dialog/manager/model/FormManagerBase;
.super Lcom/vlingo/dialog/manager/model/AbstractFormManager;
.source "FormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_ScrollBackwardErrorTemplate:Ljava/lang/String; = "ScrollBackwardErrorTemplate"

.field public static final PROP_ScrollBackwardParseTypes:Ljava/lang/String; = "ScrollBackwardParseTypes"

.field public static final PROP_ScrollBackwardTemplate:Ljava/lang/String; = "ScrollBackwardTemplate"

.field public static final PROP_ScrollForwardErrorTemplate:Ljava/lang/String; = "ScrollForwardErrorTemplate"

.field public static final PROP_ScrollForwardParseTypes:Ljava/lang/String; = "ScrollForwardParseTypes"

.field public static final PROP_ScrollForwardTemplate:Ljava/lang/String; = "ScrollForwardTemplate"

.field public static final PROP_Slots:Ljava/lang/String; = "Slots"


# instance fields
.field private ID:J

.field private ScrollBackwardErrorTemplate:Ljava/lang/String;

.field private ScrollBackwardParseTypes:Ljava/lang/String;

.field private ScrollBackwardTemplate:Ljava/lang/String;

.field private ScrollForwardErrorTemplate:Ljava/lang/String;

.field private ScrollForwardParseTypes:Ljava/lang/String;

.field private ScrollForwardTemplate:Ljava/lang/String;

.field private Slots:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/IFormManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->Slots:Ljava/util/List;

    .line 23
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 77
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/FormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 81
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/FormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ID:J

    return-wide v0
.end method

.method public getScrollBackwardErrorTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardErrorTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollBackwardParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollBackwardTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollForwardErrorTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardErrorTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollForwardParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getScrollForwardTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getSlots()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/IFormManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->Slots:Ljava/util/List;

    return-object v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ID:J

    .line 73
    return-void
.end method

.method public setScrollBackwardErrorTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardErrorTemplate:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setScrollBackwardParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardParseTypes:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setScrollBackwardTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollBackwardTemplate:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setScrollForwardErrorTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardErrorTemplate:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setScrollForwardParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardParseTypes:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setScrollForwardTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/FormManagerBase;->ScrollForwardTemplate:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
