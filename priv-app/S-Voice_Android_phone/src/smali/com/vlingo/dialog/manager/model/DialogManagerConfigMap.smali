.class public Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;
.super Lcom/vlingo/dialog/manager/model/DialogManagerConfigMapBase;
.source "DialogManagerConfigMap.java"


# instance fields
.field private ivCachedDialogManagerConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMapBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getCachedDialogManagerConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->ivCachedDialogManagerConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    return-object v0
.end method

.method public setCachedDialogManagerConfig(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;)V
    .locals 0
    .param p1, "cachedDialogManagerConfig"    # Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/DialogManagerConfigMap;->ivCachedDialogManagerConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 17
    return-void
.end method
