.class public Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;
.source "CalendarDeleteEditBaseTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ChooseTemplate:Ljava/lang/String; = "ChooseTemplate"

.field public static final PROP_FullBackoff:Ljava/lang/String; = "FullBackoff"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_ReadOnlyTemplate:Ljava/lang/String; = "ReadOnlyTemplate"


# instance fields
.field private ChooseTemplate:Ljava/lang/String;

.field private FullBackoff:Z

.field private ID:J

.field private ReadOnlyTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;-><init>()V

    .line 9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getChooseTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ChooseTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFullBackoff()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getReadOnlyTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ReadOnlyTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setChooseTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ChooseTemplate:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setFullBackoff(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->FullBackoff:Z

    .line 35
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ID:J

    .line 42
    return-void
.end method

.method public setReadOnlyTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarDeleteEditBaseTaskFormManagerBase;->ReadOnlyTemplate:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
