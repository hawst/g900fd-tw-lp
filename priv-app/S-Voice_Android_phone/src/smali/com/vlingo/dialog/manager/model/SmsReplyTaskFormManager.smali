.class public Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManager;
.super Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManagerBase;
.source "SmsReplyTaskFormManager.java"


# static fields
.field protected static final SLOT_MESSAGE:Ljava/lang/String; = "message"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManagerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 5
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 13
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v2

    .line 14
    .local v2, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {v2}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v3

    .line 16
    .local v3, "parseType":Ljava/lang/String;
    const-string/jumbo v4, "op"

    invoke-interface {p2, v4}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 17
    .local v1, "opSlot":Lcom/vlingo/dialog/model/IForm;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 19
    .local v0, "opSave":Ljava/lang/String;
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManagerBase;->fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 21
    sget-object v4, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManager;->PT_SET_ADD:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "contact"

    invoke-virtual {v2, v4}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 25
    if-eqz v1, :cond_0

    .line 26
    invoke-interface {v1, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 30
    :cond_0
    return-void

    .line 17
    .end local v0    # "opSave":Ljava/lang/String;
    :cond_1
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v1

    .line 45
    .local v1, "parse":Lcom/vlingo/dialog/util/Parse;
    const-string/jumbo v3, "contact:cf"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "contact":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 47
    const-string/jumbo v3, "contact"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    :cond_0
    const-string/jumbo v3, "pn"

    invoke-virtual {v1, v3}, Lcom/vlingo/dialog/util/Parse;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "pn":Ljava/lang/String;
    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    const-string/jumbo v3, "anaphora"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 52
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManagerBase;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_3
    const-string/jumbo v3, "sms_send"

    invoke-virtual {p0, p1, p2, v3}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManager;->convertTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManagerBase;->processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    const/4 v1, 0x0

    .line 38
    :goto_0
    return v1

    .line 37
    :cond_0
    const-string/jumbo v1, "message"

    invoke-virtual {p0, v1}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "messageFieldId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/SmsReplyTaskFormManager;->getAppendTemplate()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, v1, v0}, Lcom/vlingo/dialog/manager/model/SmsBaseTaskFormManager;->resolveAddReplace(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
