.class public Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;
.super Ljava/lang/Object;
.source "TopLevelFieldIdBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_FieldId:Ljava/lang/String; = "FieldId"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_SkipParseTypes:Ljava/lang/String; = "SkipParseTypes"


# instance fields
.field private FieldId:Ljava/lang/String;

.field private ID:J

.field private SkipParseTypes:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->FieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->ID:J

    return-wide v0
.end method

.method public getSkipParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->SkipParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->FieldId:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->ID:J

    .line 33
    return-void
.end method

.method public setSkipParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;->SkipParseTypes:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
