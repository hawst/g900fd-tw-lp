.class public abstract Lcom/vlingo/dialog/manager/model/AbstractFormManager;
.super Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;
.source "AbstractFormManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;
    }
.end annotation


# static fields
.field protected static final COMMA_SPLITTER:Lcom/google/common/base/Splitter;

.field protected static final SPACE_SPLITTER:Lcom/google/common/base/Splitter;

.field protected static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field protected cancelNoParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected cancelParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected cancelYesParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected confirmNoParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected confirmYesParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected exceptParseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected parent:Lcom/vlingo/dialog/manager/model/IFormManager;

.field protected parseTypeSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected parseTypeSpecList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;",
            ">;"
        }
    .end annotation
.end field

.field protected promptTemplateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 39
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    .line 40
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManagerBase;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSet:Ljava/util/Set;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSpecList:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->exceptParseTypeSet:Ljava/util/Set;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelParseTypeSet:Ljava/util/Set;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelYesParseTypeSet:Ljava/util/Set;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelNoParseTypeSet:Ljava/util/Set;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->confirmYesParseTypeSet:Ljava/util/Set;

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->confirmNoParseTypeSet:Ljava/util/Set;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->promptTemplateList:Ljava/util/List;

    .line 53
    return-void
.end method

.method protected static anySlotsFilled(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 4
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v2, 0x1

    .line 555
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 556
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v3

    if-eq v3, v2, :cond_1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->anySlotsFilled(Lcom/vlingo/dialog/model/IForm;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 560
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected static clearIfNotFilled(Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 549
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getFilled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 552
    :cond_0
    return-void
.end method

.method private static dereferencePath(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;
    .locals 3
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/model/IForm;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vlingo/dialog/model/IForm;"
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 392
    .local v1, "name":Ljava/lang/String;
    invoke-interface {p0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object p0

    .line 393
    if-nez p0, :cond_0

    .line 397
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method protected static equal(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x"    # Ljava/lang/String;
    .param p1, "y"    # Ljava/lang/String;

    .prologue
    .line 235
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    .line 236
    :cond_0
    if-nez p0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static getRelativePath(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;
    .locals 2
    .param p0, "base"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/model/IForm;",
            "Lcom/vlingo/dialog/model/IForm;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 383
    .local v0, "path":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    :goto_0
    if-eq p1, p0, :cond_0

    .line 384
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 385
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object p1

    goto :goto_0

    .line 387
    :cond_0
    return-object v0
.end method

.method public static getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 3
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 373
    :goto_0
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .local v0, "parent":Lcom/vlingo/dialog/model/IForm;
    if-eqz v0, :cond_0

    .line 374
    const-string/jumbo v1, ""

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    :cond_0
    return-object p0

    .line 373
    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public static getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 3
    .param p0, "manager"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 363
    :goto_0
    invoke-interface {p0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .local v0, "parent":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-eqz v0, :cond_0

    .line 364
    const-string/jumbo v1, ""

    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 368
    :cond_0
    return-object p0

    .line 363
    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method protected static getWithinTaskCopy(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 4
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 463
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 464
    .local v2, "taskForm":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    .line 465
    .local v3, "taskFormCopy":Lcom/vlingo/dialog/model/IForm;
    invoke-static {v2, p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getRelativePath(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/util/List;

    move-result-object v1

    .line 466
    .local v1, "formPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v3, v1}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->dereferencePath(Lcom/vlingo/dialog/model/IForm;Ljava/util/List;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 467
    .local v0, "formCopy":Lcom/vlingo/dialog/model/IForm;
    return-object v0
.end method

.method protected static resolveAmPmAfter(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "slotManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "reference"    # Ljava/lang/String;

    .prologue
    .line 569
    move-object v0, p0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;

    .line 570
    .local v0, "timeSlotManager":Lcom/vlingo/dialog/manager/model/TimeSlotManager;
    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    invoke-virtual {v0, p1, v1, p2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected static resolveAmPmAfterOrAm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "slotManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "reference"    # Ljava/lang/String;

    .prologue
    .line 574
    move-object v0, p0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;

    .line 575
    .local v0, "timeSlotManager":Lcom/vlingo/dialog/manager/model/TimeSlotManager;
    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->AFTER_OR_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    invoke-virtual {v0, p1, v1, p2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected static resolveAmPmForceAm(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 3
    .param p0, "slotManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 584
    move-object v0, p0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;

    .line 585
    .local v0, "timeSlotManager":Lcom/vlingo/dialog/manager/model/TimeSlotManager;
    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_AM:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected static resolveAmPmForceDefault(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 3
    .param p0, "slotManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 579
    move-object v0, p0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;

    .line 580
    .local v0, "timeSlotManager":Lcom/vlingo/dialog/manager/model/TimeSlotManager;
    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->FORCE_DEFAULT:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected static resolveAmPmNear(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 2
    .param p0, "slotManager"    # Lcom/vlingo/dialog/manager/model/IFormManager;
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "reference"    # Ljava/lang/String;

    .prologue
    .line 564
    move-object v0, p0

    check-cast v0, Lcom/vlingo/dialog/manager/model/TimeSlotManager;

    .line 565
    .local v0, "timeSlotManager":Lcom/vlingo/dialog/manager/model/TimeSlotManager;
    sget-object v1, Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;->NEAR:Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;

    invoke-virtual {v0, p1, v1, p2}, Lcom/vlingo/dialog/manager/model/TimeSlotManager;->resolveAmPm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/manager/model/TimeSlotManager$AmPmMode;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected static setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;I)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 522
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    return-void
.end method

.method protected static setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 526
    invoke-interface {p0, p1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/model/Slot;

    .line 527
    .local v0, "slot":Lcom/vlingo/dialog/model/Slot;
    if-nez v0, :cond_0

    .line 528
    new-instance v0, Lcom/vlingo/dialog/model/Slot;

    .end local v0    # "slot":Lcom/vlingo/dialog/model/Slot;
    invoke-direct {v0}, Lcom/vlingo/dialog/model/Slot;-><init>()V

    .line 529
    .restart local v0    # "slot":Lcom/vlingo/dialog/model/Slot;
    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/model/Slot;->setName(Ljava/lang/String;)V

    .line 530
    invoke-interface {p0, v0}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 532
    :cond_0
    invoke-virtual {v0, p2}, Lcom/vlingo/dialog/model/Slot;->setValue(Ljava/lang/String;)V

    .line 533
    invoke-interface {p0, v0}, Lcom/vlingo/dialog/model/IForm;->addSlot(Lcom/vlingo/dialog/model/IForm;)V

    .line 534
    return-void
.end method

.method protected static setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 518
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    return-void
.end method

.method protected static setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Boolean;

    .prologue
    .line 543
    if-eqz p2, :cond_0

    .line 544
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 546
    :cond_0
    return-void
.end method

.method protected static setSlotValueIfNotNull(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 537
    if-eqz p2, :cond_0

    .line 538
    invoke-static {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setSlotValue(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    :cond_0
    return-void
.end method

.method protected static splitList(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 1
    .param p0, "commaSeparatedString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    if-nez p0, :cond_0

    .line 214
    const-string/jumbo p0, ""

    .line 216
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v0, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method protected static splitList(Ljava/util/Collection;Ljava/lang/String;)V
    .locals 1
    .param p1, "commaSeparatedString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 220
    .local p0, "collection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/Collection;->clear()V

    .line 221
    if-eqz p1, :cond_0

    .line 222
    sget-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->COMMA_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v0, p1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/common/collect/Iterables;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 224
    :cond_0
    return-void
.end method

.method protected static splitListBySpace(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 1
    .param p0, "spaceSeparatedString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    if-nez p0, :cond_0

    .line 228
    const-string/jumbo p0, ""

    .line 230
    :cond_0
    sget-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v0, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bumpUpEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 278
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bumpUpParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 274
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public childClearedCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 270
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public childCompleted(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "child"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 267
    return-void
.end method

.method public compatibleFieldId(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getChooseFieldId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public configure()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public createForm()Lcom/vlingo/dialog/model/IForm;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "must override"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 175
    if-nez p1, :cond_1

    .line 176
    const/4 p0, 0x0

    .line 188
    .end local p0    # "this":Lcom/vlingo/dialog/manager/model/AbstractFormManager;
    :cond_0
    :goto_0
    return-object p0

    .line 178
    .restart local p0    # "this":Lcom/vlingo/dialog/manager/model/AbstractFormManager;
    :cond_1
    const-string/jumbo v5, ""

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 181
    move-object v1, p0

    .line 182
    .local v1, "form":Lcom/vlingo/dialog/manager/model/IFormManager;
    const-string/jumbo v5, "\\."

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 183
    .local v4, "name":Ljava/lang/String;
    invoke-interface {v1, v4}, Lcom/vlingo/dialog/manager/model/IFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 184
    if-nez v1, :cond_3

    .end local v4    # "name":Ljava/lang/String;
    :cond_2
    move-object p0, v1

    .line 188
    goto :goto_0

    .line 182
    .restart local v4    # "name":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method protected doCancelTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v3

    .line 300
    .local v3, "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 301
    .local v2, "topForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getCancelTemplate()Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "template":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 303
    sget-object v4, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "missing CancelTemplate for form: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;)V

    .line 308
    :goto_0
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    .line 309
    .local v0, "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    instance-of v4, v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-eqz v4, :cond_0

    .line 310
    check-cast v0, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .end local v0    # "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->cleanUpTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 312
    :cond_0
    invoke-virtual {p1, v3, v2}, Lcom/vlingo/dialog/DMContext;->cancel(Lcom/vlingo/dialog/manager/model/IFormManager;Lcom/vlingo/dialog/model/IForm;)V

    .line 313
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearConfirm()V

    .line 314
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->clearQueries()V

    .line 315
    return-void

    .line 305
    :cond_1
    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1, v1, v4, v5, v6}, Lcom/vlingo/dialog/DMContext;->addPromptGoal(Ljava/lang/String;Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;Z)V

    .line 306
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V

    goto :goto_0
.end method

.method protected exitSetFieldId(Lcom/vlingo/dialog/DMContext;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTop()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    .line 319
    .local v1, "topManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "fieldId":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setFieldId(Ljava/lang/String;)V

    .line 321
    return-void
.end method

.method public fill(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 259
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getFieldIdRecursive()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    move-object v0, p0

    .local v0, "f":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    if-eqz v0, :cond_1

    .line 107
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getFieldId()Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "fieldId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 112
    .end local v1    # "fieldId":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 106
    .restart local v1    # "fieldId":Ljava/lang/String;
    :cond_0
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    goto :goto_0

    .line 112
    .end local v1    # "fieldId":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getParent()Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parent:Lcom/vlingo/dialog/manager/model/IFormManager;

    return-object v0
.end method

.method public getReferencedTemplates()Ljava/util/Set;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    new-instance v18, Ljava/util/TreeSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/TreeSet;-><init>()V

    .line 473
    .local v18, "templates":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getClassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v4

    .local v4, "classMeta":Lcom/vlingo/mda/model/ClassMeta;
    :goto_0
    if-eqz v4, :cond_6

    .line 474
    invoke-virtual {v4}, Lcom/vlingo/mda/model/ClassMeta;->getPropertyMetas()Ljava/util/HashMap;

    move-result-object v16

    .line 475
    .local v16, "propertyMetas":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/mda/model/PropertyMeta;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 476
    .local v10, "key":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/mda/model/PropertyMeta;

    .line 477
    .local v15, "propertyMeta":Lcom/vlingo/mda/model/PropertyMeta;
    invoke-virtual {v15}, Lcom/vlingo/mda/model/PropertyMeta;->getAttributes()Ljava/util/Map;

    move-result-object v3

    .line 478
    .local v3, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 479
    .local v2, "attributeName":Ljava/lang/String;
    const-string/jumbo v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 480
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 481
    .local v14, "name":Ljava/lang/String;
    const-string/jumbo v20, "Template"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    .line 482
    .local v8, "isTemplate":Z
    if-nez v8, :cond_3

    const-string/jumbo v20, "Templates"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    const/4 v9, 0x1

    .line 483
    .local v9, "isTemplates":Z
    :goto_2
    if-nez v8, :cond_2

    if-eqz v9, :cond_1

    .line 484
    :cond_2
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "get"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 485
    .local v13, "methodName":Ljava/lang/String;
    const/16 v19, 0x0

    .line 487
    .local v19, "value":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v13, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v12

    .line 488
    .local v12, "method":Ljava/lang/reflect/Method;
    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Ljava/lang/String;

    move-object/from16 v19, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 494
    .end local v12    # "method":Ljava/lang/reflect/Method;
    :goto_3
    if-eqz v19, :cond_1

    .line 495
    if-eqz v9, :cond_4

    .line 496
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 497
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, v19

    invoke-static {v11, v0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 498
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 482
    .end local v9    # "isTemplates":Z
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "methodName":Ljava/lang/String;
    .end local v19    # "value":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 489
    .restart local v9    # "isTemplates":Z
    .restart local v13    # "methodName":Ljava/lang/String;
    .restart local v19    # "value":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 490
    .local v5, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v5}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_3

    .line 491
    .end local v5    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v5

    .line 492
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 500
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-interface/range {v18 .. v19}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 473
    .end local v2    # "attributeName":Ljava/lang/String;
    .end local v3    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "isTemplate":Z
    .end local v9    # "isTemplates":Z
    .end local v10    # "key":Ljava/lang/String;
    .end local v13    # "methodName":Ljava/lang/String;
    .end local v14    # "name":Ljava/lang/String;
    .end local v15    # "propertyMeta":Lcom/vlingo/mda/model/PropertyMeta;
    .end local v19    # "value":Ljava/lang/String;
    :cond_5
    invoke-virtual {v4}, Lcom/vlingo/mda/model/ClassMeta;->getSuperclassMeta()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v4

    goto/16 :goto_0

    .line 510
    .end local v16    # "propertyMetas":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vlingo/mda/model/PropertyMeta;>;"
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlots()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 511
    .local v17, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface/range {v17 .. v17}, Lcom/vlingo/dialog/manager/model/IFormManager;->getReferencedTemplates()Ljava/util/Set;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 514
    .end local v17    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_7
    return-object v18
.end method

.method public getSeamlessEscape()Z
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method public getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlots()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 129
    .local v1, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    .end local v1    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSlots()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/manager/model/IFormManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTop()Lcom/vlingo/dialog/manager/model/IFormManager;
    .locals 2

    .prologue
    .line 350
    move-object v0, p0

    .line 351
    .local v0, "config":Lcom/vlingo/dialog/manager/model/IFormManager;
    :goto_0
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 352
    invoke-interface {v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    goto :goto_0

    .line 354
    :cond_0
    return-object v0
.end method

.method public hasParseType(Lcom/vlingo/dialog/DMContext;)Z
    .locals 7
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;

    .prologue
    const/4 v5, 0x1

    .line 87
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v2

    .line 88
    .local v2, "parse":Lcom/vlingo/dialog/util/Parse;
    if-eqz v2, :cond_2

    .line 89
    invoke-virtual {v2}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "parseType":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 91
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSet:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 102
    .end local v3    # "parseType":Ljava/lang/String;
    :goto_0
    return v5

    .line 94
    .restart local v3    # "parseType":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "fieldId":Ljava/lang/String;
    iget-object v6, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSpecList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;

    .line 96
    .local v4, "parseTypeSpec":Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;
    invoke-virtual {v4, v0, v3}, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;->matches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_0

    .line 102
    .end local v0    # "fieldId":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "parseType":Ljava/lang/String;
    .end local v4    # "parseTypeSpec":Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected isChange(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Boolean;)Z
    .locals 4
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "oldBoolean"    # Ljava/lang/Boolean;

    .prologue
    const/4 v1, 0x0

    .line 447
    if-nez p1, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v1

    .line 450
    :cond_1
    if-nez p2, :cond_2

    .line 451
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 453
    :cond_2
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 454
    .local v0, "newValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v2, v3, :cond_0

    .line 455
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected isChange(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 3
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "oldValue"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 435
    if-nez p1, :cond_1

    .line 442
    :cond_0
    :goto_0
    return v1

    .line 438
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "newValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 440
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/Boolean;)Z
    .locals 4
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "oldBoolean"    # Ljava/lang/Boolean;

    .prologue
    const/4 v1, 0x0

    .line 419
    if-nez p1, :cond_0

    .line 430
    :goto_0
    return v1

    .line 422
    :cond_0
    if-nez p2, :cond_1

    .line 423
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 425
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 426
    .local v0, "newValue":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v2, v3, :cond_2

    .line 427
    const/4 v1, 0x1

    goto :goto_0

    .line 429
    :cond_2
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected isChangeOrClear(Lcom/vlingo/dialog/model/IForm;Ljava/lang/String;)Z
    .locals 3
    .param p1, "slot"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "oldValue"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 406
    if-nez p1, :cond_0

    .line 414
    :goto_0
    return v1

    .line 409
    :cond_0
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 410
    .local v0, "newValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 411
    const/4 v1, 0x1

    goto :goto_0

    .line 413
    :cond_1
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isComplete(Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 83
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 75
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getRequired()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeSendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 589
    invoke-static {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTaskManager(Lcom/vlingo/dialog/manager/model/IFormManager;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 590
    .local v2, "taskManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    instance-of v3, v2, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 591
    check-cast v1, Lcom/vlingo/dialog/manager/model/TaskFormManager;

    .line 592
    .local v1, "taskFormManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    invoke-static {p2}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getTaskForm(Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 593
    .local v0, "taskForm":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {v1}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->getSendIncompleteWidget()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcom/vlingo/dialog/model/IForm;->isComplete()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->anyListGoals()Z

    move-result v3

    if-nez v3, :cond_0

    .line 594
    invoke-virtual {v1, p1, v0}, Lcom/vlingo/dialog/manager/model/TaskFormManager;->sendIncompleteWidget(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 597
    .end local v0    # "taskForm":Lcom/vlingo/dialog/model/IForm;
    .end local v1    # "taskFormManager":Lcom/vlingo/dialog/manager/model/TaskFormManager;
    :cond_0
    return-void
.end method

.method public postDeserialize()V
    .locals 7

    .prologue
    .line 141
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 142
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->exceptParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getExceptParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 143
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getCancelParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 144
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelYesParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getCancelYesParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 145
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelNoParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getCancelNoParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 146
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->confirmYesParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getConfirmYesParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 147
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->confirmNoParseTypeSet:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getConfirmNoParseTypes()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 148
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->promptTemplateList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getPromptTemplates()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getConfirmFieldId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setConfirmFieldId(Ljava/lang/String;)V

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getCancelFieldId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getFieldIdRecursive()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->setCancelFieldId(Ljava/lang/String;)V

    .line 155
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 156
    .local v2, "slotNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlots()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 157
    .local v1, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 158
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "duplicate slot name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 160
    :cond_2
    invoke-interface {v1, p0}, Lcom/vlingo/dialog/manager/model/IFormManager;->setParent(Lcom/vlingo/dialog/manager/model/IFormManager;)V

    .line 161
    invoke-interface {v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->postDeserialize()V

    goto :goto_0

    .line 164
    .end local v1    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_3
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSet:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 165
    .local v3, "spec":Ljava/lang/String;
    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 166
    iget-object v4, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parseTypeSpecList:Ljava/util/List;

    new-instance v5, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;

    invoke-direct {v5, v3}, Lcom/vlingo/dialog/manager/model/AbstractFormManager$ParseTypeSpec;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 169
    .end local v3    # "spec":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method protected processActionNluEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/ActionNluEvent;)V
    .locals 2
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/ActionNluEvent;

    .prologue
    .line 249
    new-instance v0, Lcom/vlingo/dialog/util/Parse;

    invoke-virtual {p3}, Lcom/vlingo/dialog/event/model/ActionNluEvent;->getParseType()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/util/Parse;-><init>(Ljava/lang/String;)V

    .line 250
    .local v0, "parse":Lcom/vlingo/dialog/util/Parse;
    invoke-virtual {p1, v0}, Lcom/vlingo/dialog/DMContext;->setActiveParse(Lcom/vlingo/dialog/util/Parse;)V

    .line 251
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 252
    return-void
.end method

.method public processCancel(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 4
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/4 v1, 0x0

    .line 282
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getActiveParse()Lcom/vlingo/dialog/util/Parse;

    move-result-object v0

    .line 283
    .local v0, "parse":Lcom/vlingo/dialog/util/Parse;
    if-nez v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    return v1

    .line 286
    :cond_1
    iget-object v2, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->cancelParseTypeSet:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/vlingo/dialog/util/Parse;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 288
    invoke-virtual {p0, p1, p2}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->doCancelTask(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V

    .line 289
    const/4 v1, 0x1

    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v1

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->processCancel(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z

    move-result v1

    goto :goto_0
.end method

.method public processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 244
    sget-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v0}, Lcom/vlingo/common/log4j/VLogger;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "passing event to parent form: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/log4j/VLogger;->debug(Ljava/lang/Object;)V

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getParent()Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v0

    invoke-interface {p2}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    invoke-interface {v0, p1, v1, p3}, Lcom/vlingo/dialog/manager/model/IFormManager;->processEvent(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/event/model/Event;)V

    .line 246
    return-void
.end method

.method public processParse(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 255
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public processResolve(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)Z
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 325
    const/4 v0, 0x1

    return v0
.end method

.method public prompt(Lcom/vlingo/dialog/DMContext;Lcom/vlingo/dialog/model/IForm;)V
    .locals 1
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public removeTemporarySlots(Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 340
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/model/IForm;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/model/IForm;

    .line 342
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/IFormManager;

    move-result-object v2

    .line 343
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    if-nez v2, :cond_0

    .line 344
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 347
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_1
    return-void
.end method

.method public reset(Lcom/vlingo/dialog/model/IForm;)V
    .locals 4
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 329
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->reset()V

    .line 330
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/vlingo/dialog/model/IForm;->setValue(Ljava/lang/String;)V

    .line 331
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Lcom/vlingo/dialog/model/IForm;->setModified(Z)V

    .line 332
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlots()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 333
    .local v2, "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v2}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 334
    .local v1, "slot":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v2, v1}, Lcom/vlingo/dialog/manager/model/IFormManager;->reset(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_0

    .line 336
    .end local v1    # "slot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "slotManager":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->removeTemporarySlots(Lcom/vlingo/dialog/model/IForm;)V

    .line 337
    return-void
.end method

.method public setComplete(ZLcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "complete"    # Z
    .param p2, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 79
    invoke-interface {p2, p1}, Lcom/vlingo/dialog/model/IForm;->setComplete(Z)V

    .line 80
    return-void
.end method

.method public setParent(Lcom/vlingo/dialog/manager/model/IFormManager;)V
    .locals 0
    .param p1, "parent"    # Lcom/vlingo/dialog/manager/model/IFormManager;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->parent:Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 117
    return-void
.end method

.method public unprune(Lcom/vlingo/dialog/model/IForm;)V
    .locals 8
    .param p1, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->equal(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 193
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "name mismatch: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " != "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->getSlots()Ljava/util/List;

    move-result-object v4

    .line 196
    .local v4, "slots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/manager/model/IFormManager;>;"
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getSlots()Ljava/util/List;

    move-result-object v1

    .line 197
    .local v1, "formSlots":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/model/IForm;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 198
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 199
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/manager/model/IFormManager;

    .line 200
    .local v3, "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    invoke-interface {v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 201
    .local v0, "formSlot":Lcom/vlingo/dialog/model/IForm;
    if-nez v0, :cond_1

    .line 202
    invoke-interface {v3}, Lcom/vlingo/dialog/manager/model/IFormManager;->createForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    .line 203
    invoke-interface {v0, p1}, Lcom/vlingo/dialog/model/IForm;->setParent(Lcom/vlingo/dialog/model/IForm;)V

    .line 204
    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 198
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 206
    :cond_1
    invoke-interface {v3, v0}, Lcom/vlingo/dialog/manager/model/IFormManager;->unprune(Lcom/vlingo/dialog/model/IForm;)V

    goto :goto_1

    .line 210
    .end local v0    # "formSlot":Lcom/vlingo/dialog/model/IForm;
    .end local v2    # "i":I
    .end local v3    # "slot":Lcom/vlingo/dialog/manager/model/IFormManager;
    :cond_2
    return-void
.end method
