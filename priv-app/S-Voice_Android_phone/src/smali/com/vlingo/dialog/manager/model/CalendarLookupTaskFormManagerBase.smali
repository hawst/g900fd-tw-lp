.class public Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;
.super Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;
.source "CalendarLookupTaskFormManagerBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DeleteParseTypes:Ljava/lang/String; = "DeleteParseTypes"

.field public static final PROP_DeleteTaskName:Ljava/lang/String; = "DeleteTaskName"

.field public static final PROP_EditParseTypes:Ljava/lang/String; = "EditParseTypes"

.field public static final PROP_EditTaskName:Ljava/lang/String; = "EditTaskName"

.field public static final PROP_FullBackoff:Ljava/lang/String; = "FullBackoff"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_WhenTemplate:Ljava/lang/String; = "WhenTemplate"

.field public static final PROP_WhereTemplate:Ljava/lang/String; = "WhereTemplate"

.field public static final PROP_WhoTemplate:Ljava/lang/String; = "WhoTemplate"


# instance fields
.field private DeleteParseTypes:Ljava/lang/String;

.field private DeleteTaskName:Ljava/lang/String;

.field private EditParseTypes:Ljava/lang/String;

.field private EditTaskName:Ljava/lang/String;

.field private FullBackoff:Z

.field private ID:J

.field private WhenTemplate:Ljava/lang/String;

.field private WhereTemplate:Ljava/lang/String;

.field private WhoTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/CalendarBaseTaskFormManager;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->FullBackoff:Z

    .line 25
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 95
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManager;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDeleteParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->DeleteParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getDeleteTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->DeleteTaskName:Ljava/lang/String;

    return-object v0
.end method

.method public getEditParseTypes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->EditParseTypes:Ljava/lang/String;

    return-object v0
.end method

.method public getEditTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->EditTaskName:Ljava/lang/String;

    return-object v0
.end method

.method public getFullBackoff()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->FullBackoff:Z

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->ID:J

    return-wide v0
.end method

.method public getWhenTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhenTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getWhereTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhereTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getWhoTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhoTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public setDeleteParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->DeleteParseTypes:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setDeleteTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->DeleteTaskName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setEditParseTypes(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->EditParseTypes:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setEditTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->EditTaskName:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setFullBackoff(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->FullBackoff:Z

    .line 80
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->ID:J

    .line 87
    return-void
.end method

.method public setWhenTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhenTemplate:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setWhereTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhereTemplate:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setWhoTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vlingo/dialog/manager/model/CalendarLookupTaskFormManagerBase;->WhoTemplate:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
