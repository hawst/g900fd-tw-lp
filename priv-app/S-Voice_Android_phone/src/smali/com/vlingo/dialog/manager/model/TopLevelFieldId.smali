.class public Lcom/vlingo/dialog/manager/model/TopLevelFieldId;
.super Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;
.source "TopLevelFieldId.java"


# instance fields
.field private skipParseTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/vlingo/dialog/manager/model/TopLevelFieldIdBase;-><init>()V

    .line 8
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->skipParseTypes:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public isSkipParseType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "parseType"    # Ljava/lang/String;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->skipParseTypes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public postDeserialize()V
    .locals 2

    .prologue
    .line 11
    iget-object v0, p0, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->skipParseTypes:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/vlingo/dialog/manager/model/TopLevelFieldId;->getSkipParseTypes()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vlingo/dialog/manager/model/AbstractFormManager;->splitList(Ljava/util/Collection;Ljava/lang/String;)V

    .line 12
    return-void
.end method
