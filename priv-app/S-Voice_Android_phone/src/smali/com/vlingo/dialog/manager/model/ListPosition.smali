.class public Lcom/vlingo/dialog/manager/model/ListPosition;
.super Ljava/lang/Object;
.source "ListPosition.java"


# instance fields
.field private end:I

.field private start:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput p1, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->start:I

    .line 10
    iput p2, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->end:I

    .line 11
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/ListPosition;
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 14
    if-eqz p0, :cond_0

    .line 15
    const-string/jumbo v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 16
    .local v1, "values":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 17
    new-instance v0, Lcom/vlingo/dialog/manager/model/ListPosition;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/vlingo/dialog/manager/model/ListPosition;-><init>(II)V

    .line 21
    .end local v1    # "values":[Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getEndIndex()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->end:I

    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->start:I

    return v0
.end method

.method public isValid(I)Z
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 41
    iget v0, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->start:I

    if-ge v0, p1, :cond_0

    iget v0, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->end:I

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEndIndex(I)V
    .locals 0
    .param p1, "end"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->end:I

    .line 38
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "start"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/vlingo/dialog/manager/model/ListPosition;->start:I

    .line 34
    return-void
.end method
