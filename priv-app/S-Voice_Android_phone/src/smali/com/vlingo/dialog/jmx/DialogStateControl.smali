.class public Lcom/vlingo/dialog/jmx/DialogStateControl;
.super Ljava/lang/Object;
.source "DialogStateControl.java"

# interfaces
.implements Lcom/vlingo/common/util/VlingoMBeanDecorator;
.implements Lcom/vlingo/dialog/jmx/DialogStateControlMBean;


# static fields
.field private static final SINGLETON:Lcom/vlingo/dialog/jmx/DialogStateControl;

.field private static beanInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vlingo/dialog/jmx/DialogStateControl;->beanInitialized:Z

    .line 11
    new-instance v0, Lcom/vlingo/dialog/jmx/DialogStateControl;

    invoke-direct {v0}, Lcom/vlingo/dialog/jmx/DialogStateControl;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/jmx/DialogStateControl;->SINGLETON:Lcom/vlingo/dialog/jmx/DialogStateControl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static declared-synchronized initializeMBean()V
    .locals 2

    .prologue
    .line 34
    const-class v1, Lcom/vlingo/dialog/jmx/DialogStateControl;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/vlingo/dialog/jmx/DialogStateControl;->beanInitialized:Z

    if-nez v0, :cond_0

    .line 35
    sget-object v0, Lcom/vlingo/dialog/jmx/DialogStateControl;->SINGLETON:Lcom/vlingo/dialog/jmx/DialogStateControl;

    invoke-static {v0}, Lcom/vlingo/common/util/VlingoMBeanUtil;->registerMBean(Lcom/vlingo/common/util/VlingoMBeanDecorator;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_0
    monitor-exit v1

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getLocationInHierarchy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string/jumbo v0, "com.vlingo.dialog"

    return-object v0
.end method

.method public getMBeanName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "DialogState"

    return-object v0
.end method

.method public isUseXml()Z
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/vlingo/dialog/DMContext;->isPackXml()Z

    move-result v0

    return v0
.end method

.method public setUseXml(Z)Ljava/lang/String;
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 17
    invoke-static {p1}, Lcom/vlingo/dialog/DMContext;->setPackXml(Z)V

    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "UseXml is now "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/vlingo/dialog/DMContext;->isPackXml()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
