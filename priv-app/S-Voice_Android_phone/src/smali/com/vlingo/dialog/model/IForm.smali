.class public interface abstract Lcom/vlingo/dialog/model/IForm;
.super Ljava/lang/Object;
.source "IForm.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;


# virtual methods
.method public abstract addSlot(Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract anyModifications()Z
.end method

.method public abstract copy()Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract get(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getFilled()Z
.end method

.method public abstract getModified()Z
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getParent()Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getPathList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPromptCountAsInt()I
.end method

.method public abstract getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract getSlots()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/model/IForm;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTop()Lcom/vlingo/dialog/model/IForm;
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public abstract isComplete()Z
.end method

.method public abstract isDisabled()Z
.end method

.method public abstract postDeserialize()V
.end method

.method public abstract preSerialize()V
.end method

.method public abstract prune()V
.end method

.method public abstract removeSlot(Ljava/lang/String;)V
.end method

.method public abstract reset()V
.end method

.method public abstract setComplete(Z)V
.end method

.method public abstract setDisabled(Ljava/lang/Boolean;)V
.end method

.method public abstract setFilled(Z)V
.end method

.method public abstract setModified(Z)V
.end method

.method public abstract setParent(Lcom/vlingo/dialog/model/IForm;)V
.end method

.method public abstract setPromptCount(I)V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method
