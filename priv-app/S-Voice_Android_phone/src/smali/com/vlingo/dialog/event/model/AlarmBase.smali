.class public Lcom/vlingo/dialog/event/model/AlarmBase;
.super Ljava/lang/Object;
.source "AlarmBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Days:Ljava/lang/String; = "Days"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Id:Ljava/lang/String; = "Id"

.field public static final PROP_Repeat:Ljava/lang/String; = "Repeat"

.field public static final PROP_Set:Ljava/lang/String; = "Set"

.field public static final PROP_Time:Ljava/lang/String; = "Time"


# instance fields
.field private Days:Ljava/lang/String;

.field private ID:J

.field private Id:Ljava/lang/String;

.field private Repeat:Ljava/lang/Boolean;

.field private Set:Ljava/lang/Boolean;

.field private Time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Alarm;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Alarm;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDays()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Days:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->ID:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getRepeat()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Repeat:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSet()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Set:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Time:Ljava/lang/String;

    return-object v0
.end method

.method public setDays(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Days:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->ID:J

    .line 60
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Id:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setRepeat(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Repeat:Ljava/lang/Boolean;

    .line 53
    return-void
.end method

.method public setSet(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Set:Ljava/lang/Boolean;

    .line 46
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/AlarmBase;->Time:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
