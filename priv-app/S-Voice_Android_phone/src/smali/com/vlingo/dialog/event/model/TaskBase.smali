.class public Lcom/vlingo/dialog/event/model/TaskBase;
.super Ljava/lang/Object;
.source "TaskBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Complete:Ljava/lang/String; = "Complete"

.field public static final PROP_Date:Ljava/lang/String; = "Date"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Id:Ljava/lang/String; = "Id"

.field public static final PROP_ReminderDate:Ljava/lang/String; = "ReminderDate"

.field public static final PROP_ReminderSet:Ljava/lang/String; = "ReminderSet"

.field public static final PROP_ReminderTime:Ljava/lang/String; = "ReminderTime"

.field public static final PROP_Title:Ljava/lang/String; = "Title"


# instance fields
.field private Complete:Ljava/lang/Boolean;

.field private Date:Ljava/lang/String;

.field private ID:J

.field private Id:Ljava/lang/String;

.field private ReminderDate:Ljava/lang/String;

.field private ReminderSet:Ljava/lang/Boolean;

.field private ReminderTime:Ljava/lang/String;

.field private Title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Task;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 86
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/Task;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getComplete()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Complete:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Date:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ID:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getReminderDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderDate:Ljava/lang/String;

    return-object v0
.end method

.method public getReminderSet()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderSet:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getReminderTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public setComplete(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Complete:Ljava/lang/Boolean;

    .line 71
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Date:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 77
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ID:J

    .line 78
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Id:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setReminderDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderDate:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setReminderSet(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Boolean;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderSet:Ljava/lang/Boolean;

    .line 64
    return-void
.end method

.method public setReminderTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->ReminderTime:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/TaskBase;->Title:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
