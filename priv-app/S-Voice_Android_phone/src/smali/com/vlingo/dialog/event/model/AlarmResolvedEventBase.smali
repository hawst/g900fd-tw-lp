.class public Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;
.super Lcom/vlingo/dialog/event/model/QueryEvent;
.source "AlarmResolvedEventBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Alarms:Ljava/lang/String; = "Alarms"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_NumMatches:Ljava/lang/String; = "NumMatches"


# instance fields
.field private Alarms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;"
        }
    .end annotation
.end field

.field private ID:J

.field private NumMatches:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/event/model/QueryEvent;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->Alarms:Ljava/util/List;

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 32
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlarms()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/event/model/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->Alarms:Ljava/util/List;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 36
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/AlarmResolvedEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->ID:J

    return-wide v0
.end method

.method public getNumMatches()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->NumMatches:I

    return v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->ID:J

    .line 28
    return-void
.end method

.method public setNumMatches(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/vlingo/dialog/event/model/AlarmResolvedEventBase;->NumMatches:I

    .line 19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
