.class public abstract Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;
.super Lcom/vlingo/dialog/event/model/Event;
.source "ActionSafeReaderButtonEventBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_FieldId:Ljava/lang/String; = "FieldId"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Id:Ljava/lang/String; = "Id"

.field public static final PROP_SenderName:Ljava/lang/String; = "SenderName"

.field public static final PROP_SenderPhone:Ljava/lang/String; = "SenderPhone"


# instance fields
.field private FieldId:Ljava/lang/String;

.field private ID:J

.field private Id:Ljava/lang/String;

.field private SenderName:Ljava/lang/String;

.field private SenderPhone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/event/model/Event;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEvent;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->FieldId:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->ID:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->Id:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->SenderName:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->SenderPhone:Ljava/lang/String;

    return-object v0
.end method

.method public setFieldId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->FieldId:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->ID:J

    .line 51
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->Id:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setSenderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->SenderName:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setSenderPhone(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/event/model/ActionSafeReaderButtonEventBase;->SenderPhone:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
