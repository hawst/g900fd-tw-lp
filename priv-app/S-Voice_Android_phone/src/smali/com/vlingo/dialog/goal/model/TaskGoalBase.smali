.class public Lcom/vlingo/dialog/goal/model/TaskGoalBase;
.super Lcom/vlingo/dialog/goal/model/Goal;
.source "TaskGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Confirm:Ljava/lang/String; = "Confirm"

.field public static final PROP_Execute:Ljava/lang/String; = "Execute"

.field public static final PROP_Form:Ljava/lang/String; = "Form"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Name:Ljava/lang/String; = "Name"


# instance fields
.field private Confirm:Z

.field private Execute:Z

.field private Form:Lcom/vlingo/dialog/model/IForm;

.field private ID:J

.field private Name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/Goal;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/TaskGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/TaskGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getConfirm()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Confirm:Z

    return v0
.end method

.method public getExecute()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Execute:Z

    return v0
.end method

.method public getForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Form:Lcom/vlingo/dialog/model/IForm;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->ID:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public setConfirm(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Confirm:Z

    .line 37
    return-void
.end method

.method public setExecute(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Execute:Z

    .line 30
    return-void
.end method

.method public setForm(Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Form:Lcom/vlingo/dialog/model/IForm;

    .line 44
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->ID:J

    .line 51
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskGoalBase;->Name:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
