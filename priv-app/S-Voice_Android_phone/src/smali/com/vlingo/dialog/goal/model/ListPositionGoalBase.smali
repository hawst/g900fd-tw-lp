.class public Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;
.super Lcom/vlingo/dialog/goal/model/Goal;
.source "ListPositionGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_EndIndex:Ljava/lang/String; = "EndIndex"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_StartIndex:Ljava/lang/String; = "StartIndex"


# instance fields
.field private EndIndex:I

.field private ID:J

.field private StartIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/Goal;-><init>()V

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/ListPositionGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->EndIndex:I

    return v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->ID:J

    return-wide v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->StartIndex:I

    return v0
.end method

.method public setEndIndex(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->EndIndex:I

    .line 26
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->ID:J

    .line 33
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/vlingo/dialog/goal/model/ListPositionGoalBase;->StartIndex:I

    .line 19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
