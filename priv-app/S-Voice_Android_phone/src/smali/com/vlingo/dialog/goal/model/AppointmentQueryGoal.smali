.class public Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
.super Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;
.source "AppointmentQueryGoal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoalBase;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 9
    instance-of v2, p1, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 10
    check-cast v0, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;

    .line 11
    .local v0, "that":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getInvitee()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getInvitee()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getRangeStart()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getRangeStart()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getRangeEnd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getRangeEnd()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getMatchType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getMatchType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getIncludeAllDay()Z

    move-result v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getIncludeAllDay()Z

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getCount()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;->getCount()Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 20
    .end local v0    # "that":Lcom/vlingo/dialog/goal/model/AppointmentQueryGoal;
    :cond_0
    return v1
.end method
