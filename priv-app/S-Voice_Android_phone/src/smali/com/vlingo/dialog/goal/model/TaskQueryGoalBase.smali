.class public Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;
.super Lcom/vlingo/dialog/goal/model/QueryGoal;
.source "TaskQueryGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Count:Ljava/lang/String; = "Count"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_MatchUndated:Ljava/lang/String; = "MatchUndated"

.field public static final PROP_RangeEnd:Ljava/lang/String; = "RangeEnd"

.field public static final PROP_RangeStart:Ljava/lang/String; = "RangeStart"

.field public static final PROP_Title:Ljava/lang/String; = "Title"


# instance fields
.field private Count:Ljava/lang/Integer;

.field private ID:J

.field private MatchUndated:Z

.field private RangeEnd:Ljava/lang/String;

.field private RangeStart:Ljava/lang/String;

.field private Title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/QueryGoal;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->MatchUndated:Z

    .line 19
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 64
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/TaskQueryGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->Count:Ljava/lang/Integer;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->ID:J

    return-wide v0
.end method

.method public getMatchUndated()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->MatchUndated:Z

    return v0
.end method

.method public getRangeEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->RangeEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getRangeStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->RangeStart:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->Title:Ljava/lang/String;

    return-object v0
.end method

.method public setCount(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/Integer;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->Count:Ljava/lang/Integer;

    .line 46
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->ID:J

    .line 60
    return-void
.end method

.method public setMatchUndated(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->MatchUndated:Z

    .line 53
    return-void
.end method

.method public setRangeEnd(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->RangeEnd:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setRangeStart(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->RangeStart:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/TaskQueryGoalBase;->Title:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
