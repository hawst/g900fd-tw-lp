.class public abstract Lcom/vlingo/dialog/goal/model/NlgGoalBase;
.super Lcom/vlingo/dialog/goal/model/Goal;
.source "NlgGoalBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_DisplayText:Ljava/lang/String; = "DisplayText"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_SpeakText:Ljava/lang/String; = "SpeakText"

.field public static final PROP_Template:Ljava/lang/String; = "Template"

.field public static final PROP_TemplateForm:Ljava/lang/String; = "TemplateForm"


# instance fields
.field private DisplayText:Ljava/lang/String;

.field private ID:J

.field private SpeakText:Ljava/lang/String;

.field private Template:Ljava/lang/String;

.field private TemplateForm:Lcom/vlingo/dialog/model/IForm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/goal/model/Goal;-><init>()V

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/NlgGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 59
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/goal/model/NlgGoal;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->DisplayText:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->ID:J

    return-wide v0
.end method

.method public getSpeakText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->SpeakText:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->Template:Ljava/lang/String;

    return-object v0
.end method

.method public getTemplateForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->TemplateForm:Lcom/vlingo/dialog/model/IForm;

    return-object v0
.end method

.method public setDisplayText(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->DisplayText:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->ID:J

    .line 51
    return-void
.end method

.method public setSpeakText(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->SpeakText:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->Template:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setTemplateForm(Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/goal/model/NlgGoalBase;->TemplateForm:Lcom/vlingo/dialog/model/IForm;

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
