.class public final enum Lcom/vlingo/dialog/goal/model/MatchTypeEnum;
.super Ljava/lang/Enum;
.source "MatchTypeEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/dialog/goal/model/MatchTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

.field public static final enum begin:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

.field public static final enum end:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

.field public static final enum intersect:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    const-string/jumbo v1, "begin"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->begin:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    new-instance v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    const-string/jumbo v1, "end"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->end:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    new-instance v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    const-string/jumbo v1, "intersect"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->intersect:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    .line 2
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    sget-object v1, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->begin:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->end:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->intersect:Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->$VALUES:[Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/dialog/goal/model/MatchTypeEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2
    const-class v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/dialog/goal/model/MatchTypeEnum;
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->$VALUES:[Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    invoke-virtual {v0}, [Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/dialog/goal/model/MatchTypeEnum;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/vlingo/dialog/goal/model/MatchTypeEnum;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
