.class public Lcom/vlingo/dialog/util/EditDistance$Match;
.super Ljava/lang/Object;
.source "EditDistance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/util/EditDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Match"
.end annotation


# instance fields
.field public distance:I

.field public hypIndex:I

.field public normalizedDistance:F

.field public refIndex:I

.field final synthetic this$0:Lcom/vlingo/dialog/util/EditDistance;


# direct methods
.method public constructor <init>(Lcom/vlingo/dialog/util/EditDistance;IIIF)V
    .locals 0
    .param p2, "refIndex"    # I
    .param p3, "hypIndex"    # I
    .param p4, "distance"    # I
    .param p5, "normalizedDistance"    # F

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vlingo/dialog/util/EditDistance$Match;->this$0:Lcom/vlingo/dialog/util/EditDistance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p2, p0, Lcom/vlingo/dialog/util/EditDistance$Match;->refIndex:I

    .line 21
    iput p3, p0, Lcom/vlingo/dialog/util/EditDistance$Match;->hypIndex:I

    .line 22
    iput p4, p0, Lcom/vlingo/dialog/util/EditDistance$Match;->distance:I

    .line 23
    iput p5, p0, Lcom/vlingo/dialog/util/EditDistance$Match;->normalizedDistance:F

    .line 24
    return-void
.end method
