.class public Lcom/vlingo/dialog/util/DateTime$Range;
.super Ljava/lang/Object;
.source "DateTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/util/DateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Range"
.end annotation


# instance fields
.field public begin:Lcom/vlingo/dialog/util/DateTime;

.field public end:Lcom/vlingo/dialog/util/DateTime;


# direct methods
.method public constructor <init>(Lcom/vlingo/dialog/util/DateTime;Lcom/vlingo/dialog/util/DateTime;)V
    .locals 0
    .param p1, "begin"    # Lcom/vlingo/dialog/util/DateTime;
    .param p2, "end"    # Lcom/vlingo/dialog/util/DateTime;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vlingo/dialog/util/DateTime$Range;->begin:Lcom/vlingo/dialog/util/DateTime;

    .line 20
    iput-object p2, p0, Lcom/vlingo/dialog/util/DateTime$Range;->end:Lcom/vlingo/dialog/util/DateTime;

    .line 21
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime$Range;->begin:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/util/DateTime$Range;->end:Lcom/vlingo/dialog/util/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
