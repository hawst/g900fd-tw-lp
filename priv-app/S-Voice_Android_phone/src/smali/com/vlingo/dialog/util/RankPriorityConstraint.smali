.class public Lcom/vlingo/dialog/util/RankPriorityConstraint;
.super Ljava/lang/Object;
.source "RankPriorityConstraint.java"


# static fields
.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/vlingo/dialog/util/RankPriorityConstraint;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/RankPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;
    .locals 6
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x1

    .line 41
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 42
    .local v2, "n":I
    if-ltz p1, :cond_0

    shl-int v4, v5, v2

    if-lt p1, v4, :cond_2

    .line 43
    :cond_0
    const/4 v3, 0x0

    .line 54
    :cond_1
    return-object v3

    .line 46
    :cond_2
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 47
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    shl-int v4, v5, v2

    add-int/lit8 v4, v4, -0x1

    sub-int v1, v4, p1

    .line 48
    .local v1, "mask":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 49
    add-int/lit8 v4, v2, -0x1

    sub-int/2addr v4, v0

    shl-int v4, v5, v4

    and-int/2addr v4, v1

    if-eqz v4, :cond_3

    .line 50
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 58
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "A"

    aput-object v6, v4, v5

    const-string/jumbo v5, "B"

    aput-object v5, v4, v7

    const/4 v5, 0x2

    const-string/jumbo v6, "C"

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 60
    .local v0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/vlingo/dialog/util/RankPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v5, "start"

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 61
    invoke-static {v0}, Lcom/vlingo/dialog/util/RankPriorityConstraint;->orderedSubsets(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 62
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    sget-object v4, Lcom/vlingo/dialog/util/RankPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 64
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    shl-int v2, v7, v4

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 65
    sget-object v4, Lcom/vlingo/dialog/util/RankPriorityConstraint;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-static {v0, v1}, Lcom/vlingo/dialog/util/RankPriorityConstraint;->getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public static orderedSubsets(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "constraints":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    shl-int v1, v3, v4

    .line 33
    .local v1, "n":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 34
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 35
    invoke-static {p0, v0}, Lcom/vlingo/dialog/util/RankPriorityConstraint;->getNthOrderedSubset(Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-object v2
.end method
