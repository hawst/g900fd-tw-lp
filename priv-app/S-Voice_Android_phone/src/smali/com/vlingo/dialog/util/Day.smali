.class public final enum Lcom/vlingo/dialog/util/Day;
.super Ljava/lang/Enum;
.source "Day.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/dialog/util/Day;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/dialog/util/Day;

.field public static final enum FRI:Lcom/vlingo/dialog/util/Day;

.field public static final enum MON:Lcom/vlingo/dialog/util/Day;

.field public static final enum SAT:Lcom/vlingo/dialog/util/Day;

.field private static final SPACE_JOINER:Lcom/google/common/base/Joiner;

.field private static final SPACE_SPLITTER:Lcom/google/common/base/Splitter;

.field public static final enum SUN:Lcom/vlingo/dialog/util/Day;

.field public static final enum THU:Lcom/vlingo/dialog/util/Day;

.field public static final enum TUE:Lcom/vlingo/dialog/util/Day;

.field public static final enum WED:Lcom/vlingo/dialog/util/Day;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "SUN"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->SUN:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "MON"

    invoke-direct {v0, v1, v4}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->MON:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "TUE"

    invoke-direct {v0, v1, v5}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->TUE:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "WED"

    invoke-direct {v0, v1, v6}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->WED:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "THU"

    invoke-direct {v0, v1, v7}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->THU:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "FRI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->FRI:Lcom/vlingo/dialog/util/Day;

    new-instance v0, Lcom/vlingo/dialog/util/Day;

    const-string/jumbo v1, "SAT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/util/Day;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/util/Day;->SAT:Lcom/vlingo/dialog/util/Day;

    .line 8
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vlingo/dialog/util/Day;

    sget-object v1, Lcom/vlingo/dialog/util/Day;->SUN:Lcom/vlingo/dialog/util/Day;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vlingo/dialog/util/Day;->MON:Lcom/vlingo/dialog/util/Day;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vlingo/dialog/util/Day;->TUE:Lcom/vlingo/dialog/util/Day;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vlingo/dialog/util/Day;->WED:Lcom/vlingo/dialog/util/Day;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vlingo/dialog/util/Day;->THU:Lcom/vlingo/dialog/util/Day;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vlingo/dialog/util/Day;->FRI:Lcom/vlingo/dialog/util/Day;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vlingo/dialog/util/Day;->SAT:Lcom/vlingo/dialog/util/Day;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/util/Day;->$VALUES:[Lcom/vlingo/dialog/util/Day;

    .line 11
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->trimResults()Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/Day;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    .line 12
    const/16 v0, 0x20

    invoke-static {v0}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/util/Day;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static get(Ljava/lang/String;)Lcom/vlingo/dialog/util/Day;
    .locals 1
    .param p0, "day"    # Ljava/lang/String;

    .prologue
    .line 15
    sget-object v0, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/util/Day;->valueOf(Ljava/lang/String;)Lcom/vlingo/dialog/util/Day;

    move-result-object v0

    return-object v0
.end method

.method public static max(Ljava/util/EnumSet;)Lcom/vlingo/dialog/util/Day;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;)",
            "Lcom/vlingo/dialog/util/Day;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "days":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    const/4 v2, 0x0

    .line 53
    .local v2, "lastDay":Lcom/vlingo/dialog/util/Day;
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/util/Day;

    .line 54
    .local v0, "day":Lcom/vlingo/dialog/util/Day;
    move-object v2, v0

    goto :goto_0

    .line 56
    .end local v0    # "day":Lcom/vlingo/dialog/util/Day;
    :cond_0
    return-object v2
.end method

.method public static min(Ljava/util/EnumSet;)Lcom/vlingo/dialog/util/Day;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;)",
            "Lcom/vlingo/dialog/util/Day;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "days":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/util/Day;

    .line 48
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newSet()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    const-class v0, Lcom/vlingo/dialog/util/Day;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method public static newSet(Ljava/lang/String;)Ljava/util/EnumSet;
    .locals 4
    .param p0, "days"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {}, Lcom/vlingo/dialog/util/Day;->newSet()Ljava/util/EnumSet;

    move-result-object v2

    .line 24
    .local v2, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    if-eqz p0, :cond_0

    .line 25
    sget-object v3, Lcom/vlingo/dialog/util/Day;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v3, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 26
    .local v0, "day":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/util/Day;->get(Ljava/lang/String;)Lcom/vlingo/dialog/util/Day;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 29
    .end local v0    # "day":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v2
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "days"    # Ljava/lang/String;

    .prologue
    .line 37
    if-nez p0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/vlingo/dialog/util/Day;->newSet(Ljava/lang/String;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/util/Day;->normalize(Ljava/util/EnumSet;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static normalize(Ljava/util/EnumSet;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/vlingo/dialog/util/Day;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "days":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/vlingo/dialog/util/Day;>;"
    sget-object v0, Lcom/vlingo/dialog/util/Day;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    invoke-virtual {v0, p0}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vlingo/dialog/util/DateTime;->LOCALE:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/dialog/util/Day;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/vlingo/dialog/util/Day;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/util/Day;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/dialog/util/Day;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/vlingo/dialog/util/Day;->$VALUES:[Lcom/vlingo/dialog/util/Day;

    invoke-virtual {v0}, [Lcom/vlingo/dialog/util/Day;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/dialog/util/Day;

    return-object v0
.end method
