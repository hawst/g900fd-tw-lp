.class public Lcom/vlingo/dialog/state/model/State;
.super Lcom/vlingo/dialog/state/model/StateBase;
.source "State.java"


# static fields
.field public static final CONFIRM_CANCEL:Ljava/lang/String; = "cancel"

.field public static final CONFIRM_EXECUTE:Ljava/lang/String; = "execute"

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;


# instance fields
.field private activeForm:Lcom/vlingo/dialog/model/IForm;

.field private transient keyValueMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/vlingo/dialog/state/model/State;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/state/model/State;->logger:Lcom/vlingo/common/log4j/VLogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/StateBase;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    return-void
.end method

.method private static adjustReference(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;
    .locals 2
    .param p0, "newTop"    # Lcom/vlingo/dialog/model/IForm;
    .param p1, "oldForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 69
    if-nez p1, :cond_1

    .line 70
    const/4 p0, 0x0

    .line 74
    .end local p0    # "newTop":Lcom/vlingo/dialog/model/IForm;
    :cond_0
    :goto_0
    return-object p0

    .line 71
    .restart local p0    # "newTop":Lcom/vlingo/dialog/model/IForm;
    :cond_1
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getParent()Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vlingo/dialog/state/model/State;->adjustReference(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v0

    invoke-interface {p1}, Lcom/vlingo/dialog/model/IForm;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vlingo/dialog/model/IForm;->getSlot(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object p0

    goto :goto_0
.end method

.method private static getPath(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 1
    .param p0, "form"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 114
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/vlingo/dialog/model/IForm;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public copy()Lcom/vlingo/dialog/state/model/State;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcom/vlingo/dialog/state/model/State;

    invoke-direct {v0}, Lcom/vlingo/dialog/state/model/State;-><init>()V

    .line 46
    .local v0, "newState":Lcom/vlingo/dialog/state/model/State;
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->copy()Lcom/vlingo/dialog/model/IForm;

    move-result-object v1

    .line 47
    .local v1, "newTop":Lcom/vlingo/dialog/model/IForm;
    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/state/model/State;->setTop(Lcom/vlingo/dialog/model/IForm;)V

    .line 48
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/state/model/State;->setActivePath(Ljava/lang/String;)V

    .line 49
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    invoke-static {v1, v2}, Lcom/vlingo/dialog/state/model/State;->adjustReference(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/state/model/State;->setActiveForm(Lcom/vlingo/dialog/model/IForm;)V

    .line 50
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getConfirm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vlingo/dialog/state/model/State;->setConfirm(Ljava/lang/String;)V

    .line 51
    iget-object v2, v0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 52
    return-object v0
.end method

.method public getActiveForm()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    return-object v0
.end method

.method public getCandidates()Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return-object v0
.end method

.method public getKeyValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public postDeserialize()V
    .locals 6

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v2

    .line 95
    .local v2, "top":Lcom/vlingo/dialog/model/IForm;
    invoke-interface {v2}, Lcom/vlingo/dialog/model/IForm;->postDeserialize()V

    .line 97
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/vlingo/dialog/model/IForm;->dereferencePath(Ljava/lang/String;)Lcom/vlingo/dialog/model/IForm;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    .line 99
    iget-object v3, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 100
    sget-object v3, Lcom/vlingo/dialog/state/model/State;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v3}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    sget-object v3, Lcom/vlingo/dialog/state/model/State;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "resetting inconsistent State to top: activePath="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getActivePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    .line 103
    :cond_0
    iput-object v2, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    .line 104
    const-string/jumbo v3, ""

    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/state/model/State;->setActivePath(Ljava/lang/String;)V

    .line 107
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    .line 108
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getKeyValues()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/KeyValue;

    .line 109
    .local v1, "kv":Lcom/vlingo/dialog/state/model/KeyValue;
    iget-object v3, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 111
    .end local v1    # "kv":Lcom/vlingo/dialog/state/model/KeyValue;
    :cond_2
    return-void
.end method

.method public preSerialize()V
    .locals 5

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getTop()Lcom/vlingo/dialog/model/IForm;

    move-result-object v4

    invoke-interface {v4}, Lcom/vlingo/dialog/model/IForm;->preSerialize()V

    .line 81
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    invoke-static {v4}, Lcom/vlingo/dialog/state/model/State;->getPath(Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/state/model/State;->setActivePath(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->getKeyValues()Ljava/util/List;

    move-result-object v2

    .line 84
    .local v2, "keyValues":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/state/model/KeyValue;>;"
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 85
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 86
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Lcom/vlingo/dialog/state/model/KeyValue;

    invoke-direct {v3}, Lcom/vlingo/dialog/state/model/KeyValue;-><init>()V

    .line 87
    .local v3, "kv":Lcom/vlingo/dialog/state/model/KeyValue;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/vlingo/dialog/state/model/KeyValue;->setKey(Ljava/lang/String;)V

    .line 88
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/vlingo/dialog/state/model/KeyValue;->setValue(Ljava/lang/String;)V

    .line 89
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "kv":Lcom/vlingo/dialog/state/model/KeyValue;
    :cond_0
    return-void
.end method

.method public processEvent(Lcom/vlingo/dialog/event/model/Event;)V
    .locals 0
    .param p1, "event"    # Lcom/vlingo/dialog/event/model/Event;

    .prologue
    .line 119
    return-void
.end method

.method public removeKeyValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public setActiveForm(Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "activeForm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vlingo/dialog/state/model/State;->activeForm:Lcom/vlingo/dialog/model/IForm;

    .line 38
    return-void
.end method

.method public setCandidates(Lcom/vlingo/dialog/state/model/CompletedQuery;)V
    .locals 0
    .param p1, "completedQuery"    # Lcom/vlingo/dialog/state/model/CompletedQuery;

    .prologue
    .line 29
    return-void
.end method

.method public setKeyValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/State;->keyValueMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/State;->preSerialize()V

    .line 124
    invoke-super {p0}, Lcom/vlingo/dialog/state/model/StateBase;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
