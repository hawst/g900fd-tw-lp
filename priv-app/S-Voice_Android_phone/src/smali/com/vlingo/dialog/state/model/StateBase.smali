.class public Lcom/vlingo/dialog/state/model/StateBase;
.super Lcom/vlingo/dialog/state/model/HistoryItem;
.source "StateBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ActivePath:Ljava/lang/String; = "ActivePath"

.field public static final PROP_Confirm:Ljava/lang/String; = "Confirm"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_KeyValues:Ljava/lang/String; = "KeyValues"

.field public static final PROP_Top:Ljava/lang/String; = "Top"


# instance fields
.field private ActivePath:Ljava/lang/String;

.field private Confirm:Ljava/lang/String;

.field private ID:J

.field private KeyValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/state/model/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field private Top:Lcom/vlingo/dialog/model/IForm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/HistoryItem;-><init>()V

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->KeyValues:Ljava/util/List;

    .line 17
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getActivePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->ActivePath:Ljava/lang/String;

    return-object v0
.end method

.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/state/model/State;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getConfirm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->Confirm:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->ID:J

    return-wide v0
.end method

.method public getKeyValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/state/model/KeyValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->KeyValues:Ljava/util/List;

    return-object v0
.end method

.method public getTop()Lcom/vlingo/dialog/model/IForm;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/StateBase;->Top:Lcom/vlingo/dialog/model/IForm;

    return-object v0
.end method

.method public setActivePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/vlingo/dialog/state/model/StateBase;->ActivePath:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setConfirm(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vlingo/dialog/state/model/StateBase;->Confirm:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/vlingo/dialog/state/model/StateBase;->ID:J

    .line 46
    return-void
.end method

.method public setTop(Lcom/vlingo/dialog/model/IForm;)V
    .locals 0
    .param p1, "val"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vlingo/dialog/state/model/StateBase;->Top:Lcom/vlingo/dialog/model/IForm;

    .line 39
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
