.class public final enum Lcom/vlingo/dialog/state/model/ConfirmEnum;
.super Ljava/lang/Enum;
.source "ConfirmEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vlingo/dialog/state/model/ConfirmEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vlingo/dialog/state/model/ConfirmEnum;

.field public static final enum cancel:Lcom/vlingo/dialog/state/model/ConfirmEnum;

.field public static final enum execute:Lcom/vlingo/dialog/state/model/ConfirmEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;

    const-string/jumbo v1, "execute"

    invoke-direct {v0, v1, v2}, Lcom/vlingo/dialog/state/model/ConfirmEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;->execute:Lcom/vlingo/dialog/state/model/ConfirmEnum;

    new-instance v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;

    const-string/jumbo v1, "cancel"

    invoke-direct {v0, v1, v3}, Lcom/vlingo/dialog/state/model/ConfirmEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;->cancel:Lcom/vlingo/dialog/state/model/ConfirmEnum;

    .line 2
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vlingo/dialog/state/model/ConfirmEnum;

    sget-object v1, Lcom/vlingo/dialog/state/model/ConfirmEnum;->execute:Lcom/vlingo/dialog/state/model/ConfirmEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vlingo/dialog/state/model/ConfirmEnum;->cancel:Lcom/vlingo/dialog/state/model/ConfirmEnum;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;->$VALUES:[Lcom/vlingo/dialog/state/model/ConfirmEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vlingo/dialog/state/model/ConfirmEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2
    const-class v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;

    return-object v0
.end method

.method public static values()[Lcom/vlingo/dialog/state/model/ConfirmEnum;
    .locals 1

    .prologue
    .line 2
    sget-object v0, Lcom/vlingo/dialog/state/model/ConfirmEnum;->$VALUES:[Lcom/vlingo/dialog/state/model/ConfirmEnum;

    invoke-virtual {v0}, [Lcom/vlingo/dialog/state/model/ConfirmEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vlingo/dialog/state/model/ConfirmEnum;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/ConfirmEnum;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
