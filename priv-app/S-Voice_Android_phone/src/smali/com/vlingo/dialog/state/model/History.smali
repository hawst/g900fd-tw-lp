.class public Lcom/vlingo/dialog/state/model/History;
.super Lcom/vlingo/dialog/state/model/HistoryBase;
.source "History.java"


# static fields
.field private static final COMPLETED_QUERIES:Ljava/lang/String; = "CompletedQueries"

.field private static final COMPLETED_QUERY:Ljava/lang/String; = "CompletedQuery"

.field private static final PENDING_QUERIES:Ljava/lang/String; = "PendingQueries"

.field private static final QUERY_EVENT:Ljava/lang/String; = "QueryEvent"

.field private static final QUERY_GOAL:Ljava/lang/String; = "QueryGoal"

.field private static final logger:Lcom/vlingo/common/log4j/VLogger;

.field private static final metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

.field private static final serialVersionUID:J = 0x7575358b997fbb06L


# instance fields
.field private completedQueries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;",
            ">;"
        }
    .end annotation
.end field

.field private pendingQueries:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    const-class v0, Lcom/vlingo/dialog/state/model/History;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    .line 35
    new-instance v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/vlingo/mda/model/PackageMeta;

    const/4 v2, 0x0

    invoke-static {}, Lcom/vlingo/dialog/state/model/History;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {}, Lcom/vlingo/dialog/event/model/QueryEvent;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {}, Lcom/vlingo/dialog/model/Form;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;-><init>([Lcom/vlingo/mda/model/PackageMeta;)V

    sput-object v0, Lcom/vlingo/dialog/state/model/History;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/HistoryBase;-><init>()V

    .line 46
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    .line 49
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    return-void
.end method

.method public static fromXml(Ljava/io/InputStream;)Lcom/vlingo/dialog/state/model/History;
    .locals 17
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 190
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v15

    invoke-virtual {v15}, Lcom/vlingo/common/message/XMLCodec;->duplicate()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v1

    .line 192
    .local v1, "codec":Lcom/vlingo/common/message/XMLCodec;
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/vlingo/common/message/XMLCodec;->decode(Ljava/io/InputStream;)Lcom/vlingo/common/message/MNode;

    move-result-object v8

    .line 194
    .local v8, "inputNode":Lcom/vlingo/common/message/MNode;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 200
    :goto_0
    :try_start_1
    sget-object v15, Lcom/vlingo/dialog/state/model/History;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-static {v8, v15, v0}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;Z)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v6

    check-cast v6, Lcom/vlingo/dialog/state/model/History;

    .line 202
    .local v6, "history":Lcom/vlingo/dialog/state/model/History;
    const-string/jumbo v15, "PendingQueries"

    invoke-virtual {v8, v15}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v9

    .line 203
    .local v9, "pendingQueriesNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v9}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vlingo/common/message/MNode;

    .line 204
    .local v10, "pendingQueryNode":Lcom/vlingo/common/message/MNode;
    sget-object v15, Lcom/vlingo/dialog/state/model/History;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static {v10, v15}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v13

    check-cast v13, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 205
    .local v13, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    iget-object v15, v6, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v15, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 225
    .end local v6    # "history":Lcom/vlingo/dialog/state/model/History;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "pendingQueriesNode":Lcom/vlingo/common/message/MNode;
    .end local v10    # "pendingQueryNode":Lcom/vlingo/common/message/MNode;
    .end local v13    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :catch_0
    move-exception v5

    .line 226
    .local v5, "e":Ljava/lang/Exception;
    sget-object v15, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    const-string/jumbo v16, "couldn\'t load history from XML"

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/vlingo/common/log4j/VLogger;->warn(Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 227
    const/4 v6, 0x0

    .end local v5    # "e":Ljava/lang/Exception;
    :cond_0
    return-object v6

    .line 208
    .restart local v6    # "history":Lcom/vlingo/dialog/state/model/History;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v9    # "pendingQueriesNode":Lcom/vlingo/common/message/MNode;
    :cond_1
    :try_start_2
    const-string/jumbo v15, "CompletedQueries"

    invoke-virtual {v8, v15}, Lcom/vlingo/common/message/MNode;->getChildWithName(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v2

    .line 209
    .local v2, "completedQueriesNode":Lcom/vlingo/common/message/MNode;
    if-eqz v2, :cond_0

    .line 210
    invoke-virtual {v2}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/common/message/MNode;

    .line 212
    .local v4, "completedQueryNode":Lcom/vlingo/common/message/MNode;
    const-string/jumbo v15, "QueryGoal"

    invoke-virtual {v4, v15}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v14

    .line 213
    .local v14, "queryGoalNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v14}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/common/message/MNode;

    sget-object v16, Lcom/vlingo/dialog/state/model/History;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static/range {v15 .. v16}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v13

    check-cast v13, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 215
    .restart local v13    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    const-string/jumbo v15, "QueryEvent"

    invoke-virtual {v4, v15}, Lcom/vlingo/common/message/MNode;->findFirstChild(Ljava/lang/String;)Lcom/vlingo/common/message/MNode;

    move-result-object v12

    .line 216
    .local v12, "queryEventNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v12}, Lcom/vlingo/common/message/MNode;->getChildren()Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vlingo/common/message/MNode;

    sget-object v16, Lcom/vlingo/dialog/state/model/History;->metaProvider:Lcom/vlingo/mda/util/MDAMetaProvider;

    invoke-static/range {v15 .. v16}, Lcom/vlingo/mda/util/MDAObjectFromTree;->loadObjectFromTree(Lcom/vlingo/common/message/MNode;Lcom/vlingo/mda/util/MDAMetaProvider;)Lcom/vlingo/mda/util/MDAObject;

    move-result-object v11

    check-cast v11, Lcom/vlingo/dialog/event/model/QueryEvent;

    .line 218
    .local v11, "queryEvent":Lcom/vlingo/dialog/event/model/QueryEvent;
    new-instance v3, Lcom/vlingo/dialog/state/model/CompletedQuery;

    invoke-direct {v3, v13, v11}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 219
    .local v3, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    iget-object v15, v6, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v15, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 195
    .end local v2    # "completedQueriesNode":Lcom/vlingo/common/message/MNode;
    .end local v3    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v4    # "completedQueryNode":Lcom/vlingo/common/message/MNode;
    .end local v6    # "history":Lcom/vlingo/dialog/state/model/History;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "pendingQueriesNode":Lcom/vlingo/common/message/MNode;
    .end local v11    # "queryEvent":Lcom/vlingo/dialog/event/model/QueryEvent;
    .end local v12    # "queryEventNode":Lcom/vlingo/common/message/MNode;
    .end local v13    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    .end local v14    # "queryGoalNode":Lcom/vlingo/common/message/MNode;
    :catch_1
    move-exception v15

    goto/16 :goto_0
.end method

.method private getCodec()Lcom/vlingo/common/message/XMLCodec;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 147
    invoke-static {}, Lcom/vlingo/common/message/XMLCodec;->getInstance()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/common/message/XMLCodec;->duplicate()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    .line 148
    .local v0, "codec":Lcom/vlingo/common/message/XMLCodec;
    invoke-virtual {v0, v2}, Lcom/vlingo/common/message/XMLCodec;->setDisableOutputFormatting(Z)V

    .line 149
    invoke-virtual {v0, v2}, Lcom/vlingo/common/message/XMLCodec;->setGenXMLHeader(Z)V

    .line 150
    return-object v0
.end method

.method private toMNode()Lcom/vlingo/common/message/MNode;
    .locals 11

    .prologue
    .line 155
    invoke-static {}, Lcom/vlingo/mda/util/MDAObjectToTree;->getDefault()Lcom/vlingo/mda/util/MDAObjectToTree;

    move-result-object v5

    .line 156
    .local v5, "mdaObjectToTree":Lcom/vlingo/mda/util/MDAObjectToTree;
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Lcom/vlingo/mda/util/MDAObjectToTree;->setGenSensitiveProperties(Z)V

    .line 158
    invoke-virtual {v5, p0}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;

    move-result-object v6

    .line 160
    .local v6, "outputNode":Lcom/vlingo/common/message/MNode;
    new-instance v7, Lcom/vlingo/common/message/MNode;

    const-string/jumbo v10, "PendingQueries"

    invoke-direct {v7, v10}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 161
    .local v7, "pendingQueriesNode":Lcom/vlingo/common/message/MNode;
    iget-object v10, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 162
    .local v3, "goal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    invoke-virtual {v5, v3}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto :goto_0

    .line 164
    .end local v3    # "goal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_0
    invoke-virtual {v6, v7}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 166
    new-instance v0, Lcom/vlingo/common/message/MNode;

    const-string/jumbo v10, "CompletedQueries"

    invoke-direct {v0, v10}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 167
    .local v0, "completedQueriesNode":Lcom/vlingo/common/message/MNode;
    iget-object v10, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 169
    .local v1, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    new-instance v9, Lcom/vlingo/common/message/MNode;

    const-string/jumbo v10, "QueryGoal"

    invoke-direct {v9, v10}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 170
    .local v9, "queryGoalNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 172
    new-instance v8, Lcom/vlingo/common/message/MNode;

    const-string/jumbo v10, "QueryEvent"

    invoke-direct {v8, v10}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 173
    .local v8, "queryEventNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryEvent()Lcom/vlingo/dialog/event/model/QueryEvent;

    move-result-object v10

    invoke-virtual {v5, v10}, Lcom/vlingo/mda/util/MDAObjectToTree;->convertObjectToMNode(Lcom/vlingo/mda/util/MDAObject;)Lcom/vlingo/common/message/MNode;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 175
    new-instance v2, Lcom/vlingo/common/message/MNode;

    const-string/jumbo v10, "CompletedQuery"

    invoke-direct {v2, v10}, Lcom/vlingo/common/message/MNode;-><init>(Ljava/lang/String;)V

    .line 176
    .local v2, "completedQueryNode":Lcom/vlingo/common/message/MNode;
    invoke-virtual {v2, v9}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 177
    invoke-virtual {v2, v8}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 179
    invoke-virtual {v0, v2}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    goto :goto_1

    .line 181
    .end local v1    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v2    # "completedQueryNode":Lcom/vlingo/common/message/MNode;
    .end local v8    # "queryEventNode":Lcom/vlingo/common/message/MNode;
    .end local v9    # "queryGoalNode":Lcom/vlingo/common/message/MNode;
    :cond_1
    invoke-virtual {v6, v0}, Lcom/vlingo/common/message/MNode;->add(Lcom/vlingo/common/message/MNode;)V

    .line 183
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Lcom/vlingo/common/message/MNode;->setFormating(Z)V

    .line 185
    return-object v6
.end method


# virtual methods
.method public addPendingQuery(Lcom/vlingo/dialog/goal/model/QueryGoal;)V
    .locals 1
    .param p1, "queryGoal"    # Lcom/vlingo/dialog/goal/model/QueryGoal;

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/vlingo/dialog/goal/model/QueryGoal;->getClearCache()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/state/model/History;->clearQueriesByType(Ljava/lang/Class;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public clearPendingQueries()V
    .locals 5

    .prologue
    .line 93
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 94
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 95
    .local v1, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "clearing pending query: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_1
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 99
    return-void
.end method

.method public clearQueriesByType(Ljava/lang/Class;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 103
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 104
    .local v3, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 105
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 106
    sget-object v4, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v4}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "clearing pending query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    .end local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_1
    iget-object v4, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vlingo/dialog/state/model/CompletedQuery;>;"
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 110
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    .line 111
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    invoke-virtual {v0}, Lcom/vlingo/dialog/state/model/CompletedQuery;->getQueryGoal()Lcom/vlingo/dialog/goal/model/QueryGoal;

    move-result-object v3

    .line 112
    .restart local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 116
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    .end local v3    # "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    :cond_3
    return-void
.end method

.method public completeQuery(Lcom/vlingo/dialog/event/model/QueryEvent;Ljava/lang/Class;)Lcom/vlingo/dialog/state/model/CompletedQuery;
    .locals 5
    .param p1, "queryEvent"    # Lcom/vlingo/dialog/event/model/QueryEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/event/model/QueryEvent;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;)",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;"
        }
    .end annotation

    .prologue
    .line 77
    .local p2, "queryClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/vlingo/dialog/goal/model/QueryGoal;>;"
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pollFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .local v1, "queryGoal":Lcom/vlingo/dialog/goal/model/QueryGoal;
    if-eqz v1, :cond_2

    .line 78
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne p2, v2, :cond_1

    .line 79
    new-instance v0, Lcom/vlingo/dialog/state/model/CompletedQuery;

    invoke-direct {v0, v1, p1}, Lcom/vlingo/dialog/state/model/CompletedQuery;-><init>(Lcom/vlingo/dialog/goal/model/QueryGoal;Lcom/vlingo/dialog/event/model/QueryEvent;)V

    .line 80
    .local v0, "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    iget-object v2, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 89
    .end local v0    # "completedQuery":Lcom/vlingo/dialog/state/model/CompletedQuery;
    :goto_1
    return-object v0

    .line 83
    :cond_1
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    invoke-virtual {v2}, Lcom/vlingo/common/log4j/VLogger;->isInfoEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    sget-object v2, Lcom/vlingo/dialog/state/model/History;->logger:Lcom/vlingo/common/log4j/VLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "removing unmatched query of type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/common/log4j/VLogger;->info(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getCompletedQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/state/model/CompletedQuery;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPendingQueries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/goal/model/QueryGoal;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public peekQuery()Lcom/vlingo/dialog/goal/model/QueryGoal;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/goal/model/QueryGoal;

    .line 62
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public postDeserialize()V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 127
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/HistoryItem;->postDeserialize()V

    goto :goto_0

    .line 129
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_0
    return-void
.end method

.method public preSerialize()V
    .locals 3

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/state/model/HistoryItem;

    .line 121
    .local v1, "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    invoke-virtual {v1}, Lcom/vlingo/dialog/state/model/HistoryItem;->preSerialize()V

    goto :goto_0

    .line 123
    .end local v1    # "item":Lcom/vlingo/dialog/state/model/HistoryItem;
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/vlingo/dialog/state/model/History;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 133
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->pendingQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 134
    iget-object v0, p0, Lcom/vlingo/dialog/state/model/History;->completedQueries:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 135
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/History;->getCodec()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/History;->toMNode()Lcom/vlingo/common/message/MNode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toXml(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "os"    # Ljava/io/OutputStream;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/History;->getCodec()Lcom/vlingo/common/message/XMLCodec;

    move-result-object v0

    invoke-direct {p0}, Lcom/vlingo/dialog/state/model/History;->toMNode()Lcom/vlingo/common/message/MNode;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/vlingo/common/message/XMLCodec;->encode(Lcom/vlingo/common/message/MNode;Ljava/io/OutputStream;)V

    .line 144
    return-void
.end method
