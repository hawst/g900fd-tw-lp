.class final Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;
.super Ljava/util/HashMap;
.source "LangTemplateFunctions_en.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 100
    const-string/jumbo v0, "sun mon tue wed thu fri sat"

    const-string/jumbo v1, "daily"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string/jumbo v0, "mon tue wed thu fri"

    const-string/jumbo v1, "weekday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string/jumbo v0, "sun sat"

    const-string/jumbo v1, "Saturday and Sunday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string/jumbo v0, "sun"

    const-string/jumbo v1, "Sunday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string/jumbo v0, "mon"

    const-string/jumbo v1, "Monday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string/jumbo v0, "tue"

    const-string/jumbo v1, "Tuesday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string/jumbo v0, "wed"

    const-string/jumbo v1, "Wednesday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string/jumbo v0, "thu"

    const-string/jumbo v1, "Thursday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string/jumbo v0, "fri"

    const-string/jumbo v1, "Friday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string/jumbo v0, "sat"

    const-string/jumbo v1, "Saturday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method
