.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_ko.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "\ud558\ub8e8\uc885\uc77c"

.field private static final AM:Ljava/lang/String; = "\uc624\uc804"

.field private static final AND:Ljava/lang/String; = "\uacfc"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "\uc77c\uc815"

.field private static final APPOINTMENT_LOCATION:Ljava/lang/String; = "\uc7a5\uc18c"

.field private static final APPOINTMENT_TITLE:Ljava/lang/String; = "\uc81c\ubaa9"

.field private static final CHINESE_ONES_ARRAY:[Ljava/lang/String;

.field private static final CHINESE_TENS_ARRAY:[Ljava/lang/String;

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DAY:Ljava/lang/String; = "\uc77c"

.field private static final DAYS_OF_WEEK_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DISPLAY_DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DO_NOT_ADD_HONORIFIC_PATTERN:Ljava/util/regex/Pattern;

.field private static final HOURS:Ljava/lang/String; = "\uc2dc"

.field private static final KOREAN_ONES_ARRAY:[Ljava/lang/String;

.field private static final KOREAN_TENS_ARRAY:[Ljava/lang/String;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final MINUTES:Ljava/lang/String; = "\ubd84"

.field private static final MONTH:Ljava/lang/String; = "\uc6d4"

.field private static final MONTHS_ARRAY:[Ljava/lang/String;

.field private static final NIM:Ljava/lang/String; = "\ub2d8"

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final PM:Ljava/lang/String; = "\uc624\ud6c4"

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final YEAR:Ljava/lang/String; = "\ub144"

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->LOCALE:Ljava/util/Locale;

    .line 22
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "\uc601"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\ud55c"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\ub450"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\uc138"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\ub124"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\ub2e4\uc12f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\uc5ec\uc12f"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\uc77c\uacf1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\uc5ec\ub35f"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\uc544\ud649"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->KOREAN_ONES_ARRAY:[Ljava/lang/String;

    .line 35
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "\uc5f4"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\uc2a4\ubb3c"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\uc11c\ub978"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\ub9c8\ud754"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\uc270"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\uc608\uc21c"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\uc77c\ud754"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\uc5ec\ub4e0"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\uc544\ud754"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->KOREAN_TENS_ARRAY:[Ljava/lang/String;

    .line 48
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "\uc77c"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\uc774"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\uc0bc"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\uc0ac"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\uc624"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\uc721"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\uce60"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\ud314"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\uad6c"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->CHINESE_ONES_ARRAY:[Ljava/lang/String;

    .line 61
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "\uc2ed"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\uc774\uc2ed"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\uc0bc\uc2ed"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\uc0ac\uc2ed"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\uc624\uc2ed"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\uc721\uc2ed"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\uce60\uc2ed"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\ud314\uc2ed"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\uad6c\uc2ed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->CHINESE_TENS_ARRAY:[Ljava/lang/String;

    .line 74
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "\uc77c\uc6d4"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\uc774\uc6d4"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\uc0bc\uc6d4"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\uc0ac\uc6d4"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\uc624\uc6d4"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\uc720\uc6d4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\uce60\uc6d4"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\ud314\uc6d4"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\uad6c\uc6d4"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\uc2dc\uc6d4"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "\uc2ed\uc77c\uc6d4"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\uc2ed\uc774\uc6d4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->MONTHS_ARRAY:[Ljava/lang/String;

    .line 89
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DAYS_OF_WEEK_MAP:Ljava/util/Map;

    .line 101
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DATE_ALT_MAP:Ljava/util/Map;

    .line 127
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "MMMM d\uc77c EEEE"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DISPLAY_DATE_FORMAT:Ljava/text/DateFormat;

    .line 129
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, ", "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    .line 130
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->OR_FORMAT:[Ljava/lang/String;

    .line 132
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->TYPE_MAP:Ljava/util/Map;

    .line 302
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->alarmDaysMap:Ljava/util/Map;

    .line 415
    const-string/jumbo v0, ".*[0-9\ub2d8]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DO_NOT_ADD_HONORIFIC_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private static chineseNumber(I)Ljava/lang/String;
    .locals 2
    .param p0, "n"    # I

    .prologue
    .line 208
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->CHINESE_TENS_ARRAY:[Ljava/lang/String;

    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->CHINESE_ONES_ARRAY:[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->formatNumber(I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private dateDisplay(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 166
    :try_start_0
    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DISPLAY_DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 168
    :goto_0
    return-object v1

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->badDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private dateSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 154
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->dateAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 155
    .local v0, "c":Ljava/util/Calendar;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->MONTHS_ARRAY:[Ljava/lang/String;

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aget-object v4, v5, v6

    .line 156
    .local v4, "month":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->chineseNumber(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "\uc77c"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "day":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DAYS_OF_WEEK_MAP:Ljava/util/Map;

    const/4 v6, 0x7

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 158
    .local v2, "dayOfWeek":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 160
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v1    # "day":Ljava/lang/String;
    .end local v2    # "dayOfWeek":Ljava/lang/String;
    .end local v4    # "month":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 159
    :catch_0
    move-exception v3

    .line 160
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->badDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private static formatNumber(I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "n"    # I
    .param p1, "tensArray"    # [Ljava/lang/String;
    .param p2, "onesArray"    # [Ljava/lang/String;

    .prologue
    .line 212
    if-ltz p0, :cond_0

    const/16 v3, 0x63

    if-le p0, v3, :cond_1

    .line 213
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unsupported number: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 215
    :cond_1
    div-int/lit8 v2, p0, 0xa

    .line 216
    .local v2, "tens":I
    mul-int/lit8 v3, v2, 0xa

    sub-int v0, p0, v3

    .line 217
    .local v0, "ones":I
    aget-object v1, p1, v2

    .line 218
    .local v1, "result":Ljava/lang/String;
    if-eqz v2, :cond_2

    if-eqz v0, :cond_3

    .line 219
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 221
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 222
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "unsupported number: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 224
    :cond_4
    return-object v1
.end method

.method private static koreanNumber(I)Ljava/lang/String;
    .locals 2
    .param p0, "n"    # I

    .prologue
    .line 204
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->KOREAN_TENS_ARRAY:[Ljava/lang/String;

    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->KOREAN_ONES_ARRAY:[Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->formatNumber(I[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private time(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 9
    .param p1, "rawTime"    # Ljava/lang/String;
    .param p2, "spoken"    # Z

    .prologue
    .line 179
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v1

    .line 184
    .local v1, "c":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->use24HourTime()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 185
    const/16 v7, 0xb

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 186
    .local v3, "h":I
    const-string/jumbo v0, ""

    .line 194
    .local v0, "amPm":Ljava/lang/String;
    :cond_0
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_3

    invoke-static {v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->koreanNumber(I)Ljava/lang/String;

    move-result-object v7

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\uc2dc"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 195
    .local v4, "hours":Ljava/lang/String;
    const/16 v7, 0xc

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 196
    .local v5, "m":I
    if-nez v5, :cond_4

    const-string/jumbo v6, ""

    .line 197
    .local v6, "minutes":Ljava/lang/String;
    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 199
    .end local v0    # "amPm":Ljava/lang/String;
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v3    # "h":I
    .end local v4    # "hours":Ljava/lang/String;
    .end local v5    # "m":I
    .end local v6    # "minutes":Ljava/lang/String;
    :goto_3
    return-object v7

    .line 188
    .restart local v1    # "c":Ljava/util/Calendar;
    :cond_1
    const/16 v7, 0xa

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 189
    .restart local v3    # "h":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v7, 0x9

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v7, "\uc624\uc804"

    :goto_4
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    .restart local v0    # "amPm":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 191
    const/16 v3, 0xc

    goto :goto_0

    .line 189
    .end local v0    # "amPm":Ljava/lang/String;
    :cond_2
    const-string/jumbo v7, "\uc624\ud6c4"

    goto :goto_4

    .line 194
    .restart local v0    # "amPm":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_1

    .line 196
    .restart local v4    # "hours":Ljava/lang/String;
    .restart local v5    # "m":I
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p2, :cond_5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->chineseNumber(I)Ljava/lang/String;

    move-result-object v7

    :goto_5
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\ubd84"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_5

    .line 198
    .end local v0    # "amPm":Ljava/lang/String;
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v3    # "h":I
    .end local v4    # "hours":Ljava/lang/String;
    .end local v5    # "m":I
    :catch_0
    move-exception v2

    .line 199
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->badTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_3
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 323
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 326
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_0

    .line 329
    if-eqz v2, :cond_5

    .line 330
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 331
    const-string/jumbo v5, "\ub9e4\uc77c"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    :cond_0
    :goto_0
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 354
    .local v4, "time":Ljava/lang/String;
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 358
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 359
    const-string/jumbo v5, ", \ud574\uc81c\ub418\uc5c8\uc2b5\ub2c8\ub2e4"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 332
    .end local v1    # "enabled":Z
    .end local v4    # "time":Ljava/lang/String;
    :cond_2
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 333
    const-string/jumbo v5, "\ub9e4\uc8fc \ud3c9\uc77c"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 334
    :cond_3
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 335
    const-string/jumbo v5, "\ub9e4\uc8fc \uc8fc\ub9d0"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 337
    :cond_4
    const-string/jumbo v5, "\ub9e4\uc8fc "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 341
    :cond_5
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 342
    const-string/jumbo v5, "\uc77c\uc8fc\uc77c"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 343
    :cond_6
    const-string/jumbo v5, "weekday"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 344
    const-string/jumbo v5, "\ud3c9\uc77c"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 345
    :cond_7
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 346
    const-string/jumbo v5, "\uc8fc\ub9d0"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 348
    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 247
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 248
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 249
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 252
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 259
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 260
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 262
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 269
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 271
    const-string/jumbo v5, "\ud558\ub8e8\uc885\uc77c"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :goto_2
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 279
    :cond_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 280
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 282
    const-string/jumbo v5, "\uc7a5\uc18c"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 289
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 254
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "\uc77c\uc815"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 264
    .end local v4    # "title":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 273
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 274
    .local v3, "time":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public count(I)Ljava/lang/String;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isSpoken()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->koreanNumber(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;->count(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isDisplay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->dateDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->dateSpoken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected nameAddHonorific(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 420
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DO_NOT_ADD_HONORIFIC_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\ub2d8"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 9
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v8, 0x2c

    .line 372
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 374
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 375
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 376
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 380
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 382
    const-string/jumbo v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    const-string/jumbo v7, "\uc644\ub8cc\uc77c"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    const-string/jumbo v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const-string/jumbo v7, "date_alt"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 387
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    :goto_0
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 394
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 395
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 396
    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 397
    const-string/jumbo v7, "reminder_date_alt"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 398
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 399
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 405
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 406
    const-string/jumbo v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    :cond_2
    const-string/jumbo v7, " \uc5d0 \uc54c\ub9bc"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 389
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 401
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 367
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->isSpoken()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->time(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
