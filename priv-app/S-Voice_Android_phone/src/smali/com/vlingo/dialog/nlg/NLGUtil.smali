.class public Lcom/vlingo/dialog/nlg/NLGUtil;
.super Ljava/lang/Object;
.source "NLGUtil.java"


# static fields
.field private static SPACE_JOINER:Lcom/google/common/base/Joiner;

.field private static SPACE_SPLITTER:Lcom/google/common/base/Splitter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x20

    .line 18
    invoke-static {v1}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/NLGUtil;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    .line 19
    invoke-static {v1}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Joiner;->skipNulls()Lcom/google/common/base/Joiner;

    move-result-object v0

    sput-object v0, Lcom/vlingo/dialog/nlg/NLGUtil;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static separateLargeIntegersIntoWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 23
    move-object v7, p0

    .line 24
    .local v7, "out":Ljava/lang/String;
    const-string/jumbo v9, ".*[0-9].*"

    invoke-virtual {p0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 26
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v8, "outWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v9, Lcom/vlingo/dialog/nlg/NLGUtil;->SPACE_SPLITTER:Lcom/google/common/base/Splitter;

    invoke-virtual {v9, p0}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 29
    .local v5, "inWord":Ljava/lang/String;
    const-string/jumbo v9, "\\+?[0-9]+\\.?"

    invoke-virtual {v5, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x9

    if-le v9, v10, :cond_3

    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .local v1, "b":Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_2

    aget-char v9, v0, v4

    invoke-static {v9}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    .line 34
    .local v2, "c":Ljava/lang/Character;
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v9

    const/16 v10, 0x2e

    if-eq v9, v10, :cond_1

    .line 35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v9

    const/16 v10, 0x2b

    if-eq v9, v10, :cond_0

    .line 37
    const/16 v9, 0x20

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 32
    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 40
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v10

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_2

    .line 44
    .end local v2    # "c":Ljava/lang/Character;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    .end local v0    # "arr$":[C
    .end local v1    # "b":Ljava/lang/StringBuilder;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_3
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 51
    .end local v5    # "inWord":Ljava/lang/String;
    :cond_4
    sget-object v9, Lcom/vlingo/dialog/nlg/NLGUtil;->SPACE_JOINER:Lcom/google/common/base/Joiner;

    invoke-virtual {v9, v8}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    .line 53
    .end local v8    # "outWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    return-object v7
.end method
