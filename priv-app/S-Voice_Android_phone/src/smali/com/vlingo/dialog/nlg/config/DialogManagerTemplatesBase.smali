.class public Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;
.super Ljava/lang/Object;
.source "DialogManagerTemplatesBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Imports:Ljava/lang/String; = "Imports"

.field public static final PROP_Templates:Ljava/lang/String; = "Templates"


# instance fields
.field private ID:J

.field private Imports:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/nlg/config/Import;",
            ">;"
        }
    .end annotation
.end field

.field private Templates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/nlg/config/Template;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->Imports:Ljava/util/Set;

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->Templates:Ljava/util/List;

    .line 13
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 27
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 31
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplates;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->ID:J

    return-wide v0
.end method

.method public getImports()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/vlingo/dialog/nlg/config/Import;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->Imports:Ljava/util/Set;

    return-object v0
.end method

.method public getTemplates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/nlg/config/Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->Templates:Ljava/util/List;

    return-object v0
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/vlingo/dialog/nlg/config/DialogManagerTemplatesBase;->ID:J

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
