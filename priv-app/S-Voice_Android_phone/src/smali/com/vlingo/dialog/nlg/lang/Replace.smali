.class public Lcom/vlingo/dialog/nlg/lang/Replace;
.super Ljava/lang/Object;
.source "Replace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vlingo/dialog/nlg/lang/Replace$Rule;
    }
.end annotation


# instance fields
.field private rules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/nlg/lang/Replace$Rule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([[Ljava/lang/String;)V
    .locals 8
    .param p1, "rulesIn"    # [[Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/vlingo/dialog/nlg/lang/Replace;->rules:Ljava/util/List;

    .line 22
    move-object v0, p1

    .local v0, "arr$":[[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 23
    .local v3, "ruleIn":[Ljava/lang/String;
    array-length v4, v3

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 24
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "not length 2"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 26
    :cond_0
    iget-object v4, p0, Lcom/vlingo/dialog/nlg/lang/Replace;->rules:Ljava/util/List;

    new-instance v5, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-direct {v5, v6, v7}, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28
    .end local v3    # "ruleIn":[Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public replace(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 31
    iget-object v2, p0, Lcom/vlingo/dialog/nlg/lang/Replace;->rules:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;

    .line 32
    .local v1, "rule":Lcom/vlingo/dialog/nlg/lang/Replace$Rule;
    iget-object v2, v1, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    iget-object v3, v1, Lcom/vlingo/dialog/nlg/lang/Replace$Rule;->replacement:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 34
    .end local v1    # "rule":Lcom/vlingo/dialog/nlg/lang/Replace$Rule;
    :cond_0
    return-object p1
.end method
