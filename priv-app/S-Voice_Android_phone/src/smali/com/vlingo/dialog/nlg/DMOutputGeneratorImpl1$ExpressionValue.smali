.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl1.java"

# interfaces
.implements Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExpressionValue"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x24dc6358984cfdf1L


# instance fields
.field private transient expression:Lorg/apache/commons/jexl2/Expression;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->text:Ljava/lang/String;

    .line 92
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$000()Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->expression:Lorg/apache/commons/jexl2/Expression;

    .line 93
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2
    .param p1, "ois"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 101
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->engine:Lorg/apache/commons/jexl2/JexlEngine;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$000()Lorg/apache/commons/jexl2/JexlEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/commons/jexl2/JexlEngine;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->expression:Lorg/apache/commons/jexl2/Expression;

    .line 102
    return-void
.end method


# virtual methods
.method public generate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$ExpressionValue;->expression:Lorg/apache/commons/jexl2/Expression;

    invoke-interface {v0, p1}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
