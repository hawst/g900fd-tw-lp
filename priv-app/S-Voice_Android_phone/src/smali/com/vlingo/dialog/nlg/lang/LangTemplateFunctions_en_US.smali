.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;
.source "LangTemplateFunctions_en_US.java"


# static fields
.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final HOURS_SPOKEN_24:[Ljava/lang/String;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final MINUTES_SPOKEN_24:[Ljava/lang/String;

.field private static final TIME_24H_FORMAT:Ljava/text/DateFormat;

.field private static final TIME_FORMAT:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->LOCALE:Ljava/util/Locale;

    .line 18
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, MMMM d"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 19
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm a"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->TIME_FORMAT:Ljava/text/DateFormat;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->TIME_24H_FORMAT:Ljava/text/DateFormat;

    .line 22
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "zero"

    aput-object v1, v0, v3

    const-string/jumbo v1, "zero one"

    aput-object v1, v0, v4

    const-string/jumbo v1, "zero two"

    aput-object v1, v0, v5

    const-string/jumbo v1, "zero three"

    aput-object v1, v0, v6

    const-string/jumbo v1, "zero four"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "zero five"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "zero six"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "zero seven"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "zero eight"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "zero nine"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "ten"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "eleven"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "twelve"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "thirteen"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "fourteen"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "fifteen"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "sixteen"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "seventeen"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "eighteen"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "nineteen"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "twenty"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "twenty-one"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "twenty-two"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "twenty-three"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->HOURS_SPOKEN_24:[Ljava/lang/String;

    .line 49
    const/16 v0, 0x3c

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "hundred"

    aput-object v1, v0, v3

    const-string/jumbo v1, "oh one"

    aput-object v1, v0, v4

    const-string/jumbo v1, "oh two"

    aput-object v1, v0, v5

    const-string/jumbo v1, "oh three"

    aput-object v1, v0, v6

    const-string/jumbo v1, "oh four"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "oh five"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "oh six"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "oh seven"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "oh eight"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "oh nine"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "ten"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "eleven"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "twelve"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "thirteen"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "fourteen"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "fifteen"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "sixteen"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "seventeen"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "eighteen"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "nineteen"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "twenty"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "twenty-one"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "twenty-two"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "twenty-three"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "twenty-four"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "twenty-five"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "twenty-six"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "twenty-seven"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "twenty-eight"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "twenty-nine"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "thirty"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "thirty-one"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "thirty-two"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "thirty-three"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "thirty-four"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "thirty-five"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "thirty-six"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "thirty-seven"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "thirty-eight"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "thirty-nine"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "forty"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "forty-one"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "forty-two"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "forty-three"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "forty-four"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "forty-five"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "forty-six"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "forty-seven"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "forty-eight"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "forty-nine"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "fifty"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "fifty-one"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "fifty-two"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "fifty-three"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "fifty-four"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "fifty-five"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "fifty-six"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "fifty-seven"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "fifty-eight"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "fifty-nine"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->MINUTES_SPOKEN_24:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en;-><init>()V

    return-void
.end method

.method private timeSpoken24(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 132
    .local v0, "calendar":Ljava/util/Calendar;
    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 133
    .local v1, "h":I
    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 134
    .local v2, "m":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->HOURS_SPOKEN_24:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->MINUTES_SPOKEN_24:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 114
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 120
    :try_start_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->isSpoken()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->timeSpoken24(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    :goto_0
    return-object v1

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->use24HourTime()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->TIME_24H_FORMAT:Ljava/text/DateFormat;

    :goto_1
    invoke-virtual {p0, p1, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->TIME_FORMAT:Ljava/text/DateFormat;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_en_US;->badTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
