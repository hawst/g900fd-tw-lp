.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl3.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompiledTemplate"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x48c05da0c8918bc2L


# instance fields
.field conditions:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

.field language:Ljava/lang/String;

.field name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vlingo/common/message/MNode;)V
    .locals 5
    .param p1, "templateNode"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    const-string/jumbo v4, "Name"

    invoke-virtual {p1, v4}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->name:Ljava/lang/String;

    .line 172
    const-string/jumbo v4, "Language"

    invoke-virtual {p1, v4}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->language:Ljava/lang/String;

    .line 173
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 174
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;>;"
    const-string/jumbo v4, "Condition"

    invoke-virtual {p1, v4}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/common/message/MNode;

    .line 175
    .local v1, "conditionNode":Lcom/vlingo/common/message/MNode;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->compile(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    move-result-object v0

    .line 176
    .local v0, "compiledCondition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    if-eqz v0, :cond_0

    .line 177
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    .end local v0    # "compiledCondition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    .end local v1    # "conditionNode":Lcom/vlingo/common/message/MNode;
    :cond_1
    const-class v4, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    invoke-static {v3, v4}, Lcom/google/common/collect/Iterables;->toArray(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    iput-object v4, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->conditions:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    .line 181
    return-void
.end method


# virtual methods
.method public generate(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/Integer;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;
    .locals 7
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .param p2, "index"    # Ljava/lang/Integer;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->conditions:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    .local v0, "arr$":[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 185
    .local v1, "condition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    invoke-virtual {v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->whenIsTrue(Lorg/apache/commons/jexl2/JexlContext;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 186
    invoke-virtual {v1, p2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->selectVariant(Ljava/lang/Integer;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    move-result-object v4

    return-object v4

    .line 184
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "condition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    :cond_1
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "no true condition for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->language:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public generateAll(Lorg/apache/commons/jexl2/JexlContext;)Ljava/util/List;
    .locals 7
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/jexl2/JexlContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->conditions:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    .local v0, "arr$":[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 194
    .local v1, "condition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    invoke-virtual {v1, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->whenIsTrue(Lorg/apache/commons/jexl2/JexlContext;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 195
    iget-object v4, v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    invoke-static {v4}, Lcom/google/common/collect/ImmutableList;->copyOf([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    return-object v4

    .line 193
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "condition":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    :cond_1
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "no true condition for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledTemplate;->language:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
