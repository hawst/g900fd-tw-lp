.class final Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;
.super Ljava/util/HashMap;
.source "LangTemplateFunctions_ru.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 299
    const-string/jumbo v0, "sun mon tue wed thu fri sat"

    const-string/jumbo v1, "daily"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const-string/jumbo v0, "mon tue wed thu fri"

    const-string/jumbo v1, "weekday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    const-string/jumbo v0, "sun sat"

    const-string/jumbo v1, "weekend"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    const-string/jumbo v0, "sun"

    const-string/jumbo v1, "\u0432\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    const-string/jumbo v0, "mon"

    const-string/jumbo v1, "\u043f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    const-string/jumbo v0, "tue"

    const-string/jumbo v1, "\u0432\u0442\u043e\u0440\u043d\u0438\u043a"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    const-string/jumbo v0, "wed"

    const-string/jumbo v1, "\u0441\u0440\u0435\u0434\u0443"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    const-string/jumbo v0, "thu"

    const-string/jumbo v1, "\u0447\u0435\u0442\u0432\u0435\u0440\u0433"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    const-string/jumbo v0, "fri"

    const-string/jumbo v1, "\u043f\u044f\u0442\u043d\u0438\u0446\u0443"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    const-string/jumbo v0, "sat"

    const-string/jumbo v1, "\u0441\u0443\u0431\u0431\u043e\u0442\u0443"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    return-void
.end method
