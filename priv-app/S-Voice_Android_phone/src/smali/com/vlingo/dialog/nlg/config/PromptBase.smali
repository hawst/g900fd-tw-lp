.class public Lcom/vlingo/dialog/nlg/config/PromptBase;
.super Ljava/lang/Object;
.source "PromptBase.java"

# interfaces
.implements Lcom/vlingo/mda/util/MDAObject;
.implements Ljava/io/Serializable;


# static fields
.field public static final PROP_Display:Ljava/lang/String; = "Display"

.field public static final PROP_ID:Ljava/lang/String; = "ID"

.field public static final PROP_Spoken:Ljava/lang/String; = "Spoken"

.field public static final PROP_SpokenDisplay:Ljava/lang/String; = "SpokenDisplay"


# instance fields
.field private Display:Ljava/lang/String;

.field private ID:J

.field private Spoken:Ljava/lang/String;

.field private SpokenDisplay:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/nlg/config/Prompt;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getClassMeta()Lcom/vlingo/mda/model/ClassMeta;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/vlingo/mda/util/ModelFactory;->getFactory()Lcom/vlingo/mda/util/ModelFactory;

    move-result-object v0

    const-class v1, Lcom/vlingo/dialog/nlg/config/Prompt;

    const-string/jumbo v2, "/VPRuntimeModel.xml"

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/mda/util/ModelFactory;->getClassMetaFromResource(Ljava/lang/Class;Ljava/lang/String;)Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v0

    return-object v0
.end method

.method public getDisplay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->Display:Ljava/lang/String;

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->ID:J

    return-wide v0
.end method

.method public getSpoken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->Spoken:Ljava/lang/String;

    return-object v0
.end method

.method public getSpokenDisplay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->SpokenDisplay:Ljava/lang/String;

    return-object v0
.end method

.method public setDisplay(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->Display:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setID(J)V
    .locals 0
    .param p1, "val"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->ID:J

    .line 42
    return-void
.end method

.method public setSpoken(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->Spoken:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setSpokenDisplay(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vlingo/dialog/nlg/config/PromptBase;->SpokenDisplay:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    invoke-static {p0}, Lcom/vlingo/mda/util/MDAUtil;->toString(Lcom/vlingo/mda/util/MDAObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
