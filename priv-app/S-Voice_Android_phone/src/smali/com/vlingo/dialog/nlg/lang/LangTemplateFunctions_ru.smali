.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_ru.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "\u0432\u0435\u0441\u044c \u0434\u0435\u043d\u044c"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "\u0432\u0441\u0442\u0440\u0435\u0447\u0430"

.field private static final APPOINTMENT_LOCATION:Ljava/lang/String; = "\u043c\u0435\u0441\u0442\u043e \u0432\u0441\u0442\u0440\u0435\u0447\u0438"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final DATE_ON:Ljava/lang/String; = "\u043d\u0430"

.field private static final LOCALE:Ljava/util/Locale;

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

.field private static final TIME_AT:Ljava/lang/String; = "\u043d\u0430"

.field private static final TIME_FORMAT_12:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_24_DISPLAY:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_24_SPOKEN:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final inDayOfWeekArray:[Ljava/lang/String;

.field private static final monthArray:[Ljava/lang/String;

.field private static final ordinals:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Ljava/util/Locale;

    const-string/jumbo v1, "ru"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->LOCALE:Ljava/util/Locale;

    .line 21
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v4

    const-string/jumbo v1, "\u0432 \u0432\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\u0432 \u043f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\u0432\u043e \u0432\u0442\u043e\u0440\u043d\u0438\u043a"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u0432 \u0441\u0440\u0435\u0434\u0443"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\u0432 \u0447\u0435\u0442\u0432\u0435\u0440\u0433"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u0432 \u043f\u044f\u0442\u043d\u0438\u0446\u0443"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\u0432 \u0441\u0443\u0431\u0431\u043e\u0442\u0443"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->inDayOfWeekArray:[Ljava/lang/String;

    .line 33
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "\u044f\u043d\u0432\u0430\u0440\u044f"

    aput-object v1, v0, v4

    const-string/jumbo v1, "\u0444\u0435\u0432\u0440\u0430\u043b\u044f"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\u043c\u0430\u0440\u0442\u0430"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\u0430\u043f\u0440\u0435\u043b\u044f"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u043c\u0430\u044f"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\u0438\u044e\u043d\u044f"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u0438\u044e\u043b\u044f"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\u0430\u0432\u0433\u0443\u0441\u0442\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\u0441\u0435\u043d\u0442\u044f\u0431\u0440\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\u043e\u043a\u0442\u044f\u0431\u0440\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "\u043d\u043e\u044f\u0431\u0440\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\u0434\u0435\u043a\u0430\u0431\u0440\u044f"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->monthArray:[Ljava/lang/String;

    .line 48
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v4

    const-string/jumbo v1, "\u043f\u0435\u0440\u0432\u043e\u0433\u043e"

    aput-object v1, v0, v5

    const-string/jumbo v1, "\u0432\u0442\u043e\u0440\u043e\u0433\u043e"

    aput-object v1, v0, v6

    const-string/jumbo v1, "\u0442\u0440\u0435\u0442\u044c\u0435\u0433\u043e"

    aput-object v1, v0, v3

    const-string/jumbo v1, "\u0447\u0435\u0442\u0432\u0435\u0440\u0442\u043e\u0433\u043e"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "\u043f\u044f\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u0448\u0435\u0441\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\u0441\u0435\u0434\u044c\u043c\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\u0432\u043e\u0441\u044c\u043c\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\u0434\u0435\u0432\u044f\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "\u0434\u0435\u0441\u044f\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\u043e\u0434\u0438\u043d\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "\u0434\u0432\u0435\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "\u0442\u0440\u0438\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "\u0447\u0435\u0442\u044b\u0440\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "\u043f\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "\u0448\u0435\u0441\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "\u0432\u043e\u0441\u0435\u043c\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "\u0434\u0435\u0432\u044f\u0442\u043d\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u043f\u0435\u0440\u0432\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0432\u0442\u043e\u0440\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0442\u0440\u0435\u0442\u044c\u0435\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0447\u0435\u0442\u0432\u0435\u0440\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u043f\u044f\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0448\u0435\u0441\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0441\u0435\u0434\u044c\u043c\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0432\u043e\u0441\u044c\u043c\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "\u0434\u0432\u0430\u0434\u0446\u0430\u0442\u044c \u0434\u0435\u0432\u044f\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "\u0442\u0440\u0438\u0434\u0446\u0430\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "\u0442\u0440\u0438\u0434\u0446\u0430\u0442\u044c \u043f\u0435\u0440\u0432\u043e\u0433\u043e"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->ordinals:[Ljava/lang/String;

    .line 84
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, d MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 86
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->TIME_FORMAT_12:Ljava/text/DateFormat;

    .line 87
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->TIME_FORMAT_24_DISPLAY:Ljava/text/DateFormat;

    .line 88
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "H:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->TIME_FORMAT_24_SPOKEN:Ljava/text/DateFormat;

    .line 90
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->DATE_ALT_MAP:Ljava/util/Map;

    .line 97
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, " \u0438 "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, " \u0438 "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_FORMAT:[Ljava/lang/String;

    .line 98
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, " \u0438 "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, ", \u0438 "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    .line 99
    new-array v0, v3, [Ljava/lang/String;

    const-string/jumbo v1, " \u0438\u043b\u0438 "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, " \u0438\u043b\u0438 "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->OR_FORMAT:[Ljava/lang/String;

    .line 111
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->TYPE_MAP:Ljava/util/Map;

    .line 297
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->alarmDaysMap:Ljava/util/Map;

    .line 406
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/Replace;

    new-array v1, v6, [[Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "\\b\u0432 (?=\u0432\u0442\u043e\u0440\u043d\u0438\u043a\\b)"

    aput-object v3, v2, v4

    const-string/jumbo v3, "\u0432\u043e "

    aput-object v3, v2, v5

    aput-object v2, v1, v4

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "\\b\u0432\u043e (?=(\u0432\u043e\u0441\u043a\u0440\u0435\u0441\u0435\u043d\u044c\u0435|\u043f\u043e\u043d\u0435\u0434\u0435\u043b\u044c\u043d\u0438\u043a|\u0441\u0440\u0435\u0434\u0443|\u0447\u0435\u0442\u0432\u0435\u0440\u0433|\u043f\u044f\u0442\u043d\u0438\u0446\u0443|\u0441\u0443\u0431\u0431\u043e\u0442\u0443)\\b)"

    aput-object v3, v2, v4

    const-string/jumbo v3, "\u0432 "

    aput-object v3, v2, v5

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/nlg/lang/Replace;-><init>([[Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private static expandSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "formatted"    # Ljava/lang/String;

    .prologue
    .line 222
    const-string/jumbo v0, "00:"

    const-string/jumbo v1, "0:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ":0"

    const-string/jumbo v2, " 0 "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ":"

    const-string/jumbo v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static partOfDay(I)Ljava/lang/String;
    .locals 1
    .param p0, "h"    # I

    .prologue
    .line 210
    const/4 v0, 0x4

    if-ge p0, v0, :cond_0

    .line 211
    const-string/jumbo v0, "\u043d\u043e\u0447\u0438"

    .line 217
    :goto_0
    return-object v0

    .line 212
    :cond_0
    const/16 v0, 0xc

    if-ge p0, v0, :cond_1

    .line 213
    const-string/jumbo v0, "\u0443\u0442\u0440\u0430"

    goto :goto_0

    .line 214
    :cond_1
    const/16 v0, 0x11

    if-ge p0, v0, :cond_2

    .line 215
    const-string/jumbo v0, "\u0434\u043d\u044f"

    goto :goto_0

    .line 217
    :cond_2
    const-string/jumbo v0, "\u0432\u0435\u0447\u0435\u0440\u0430"

    goto :goto_0
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 8
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v6, "time"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 317
    .local v5, "time":Ljava/lang/String;
    const-string/jumbo v6, "\u043d\u0430 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string/jumbo v6, "repeat"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 321
    .local v3, "repeat":Z
    const-string/jumbo v6, "days"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 322
    .local v1, "days":Ljava/lang/String;
    const/4 v6, 0x3

    new-array v0, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, " \u0438 "

    aput-object v7, v0, v6

    const/4 v6, 0x1

    const-string/jumbo v7, ", "

    aput-object v7, v0, v6

    const/4 v6, 0x2

    const-string/jumbo v7, " \u0438 \u0432 "

    aput-object v7, v0, v6

    .line 323
    .local v0, "andFormat":[Ljava/lang/String;
    sget-object v6, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v1, v6, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->renderDays(Ljava/lang/String;Ljava/util/Map;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_0

    .line 326
    if-eqz v3, :cond_5

    .line 327
    const-string/jumbo v6, "daily"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 328
    const-string/jumbo v6, " \u043a\u0430\u0436\u0434\u044b\u0439 \u0434\u0435\u043d\u044c"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_0
    :goto_0
    const-string/jumbo v6, "enabled"

    invoke-interface {p2, v6}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 352
    .local v2, "enabled":Z
    if-nez v2, :cond_1

    .line 353
    const-string/jumbo v6, ", \u043d\u0435 \u043f\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, " ,"

    invoke-static {v6, v7}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 329
    .end local v2    # "enabled":Z
    :cond_2
    const-string/jumbo v6, "weekday"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 330
    const-string/jumbo v6, " \u043a\u0430\u0436\u0434\u044b\u0439 \u0431\u0443\u0434\u043d\u0438\u0439 \u0434\u0435\u043d\u044c"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 331
    :cond_3
    const-string/jumbo v6, "weekend"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 332
    const-string/jumbo v6, " \u043a\u0430\u0436\u0434\u044b\u0435 \u0432\u044b\u0445\u043e\u0434\u043d\u044b\u0435"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 334
    :cond_4
    const-string/jumbo v6, " \u043a\u0430\u0436\u0434\u044b\u0439 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 338
    :cond_5
    const-string/jumbo v6, "daily"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 339
    const-string/jumbo v6, " \u043d\u0430 \u0441\u043b\u0435\u0434\u0443\u044e\u0449\u0438\u0435 \u0441\u0435\u043c\u044c \u0434\u043d\u0435\u0439"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 340
    :cond_6
    const-string/jumbo v6, "weekday"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 341
    const-string/jumbo v6, " \u0432 \u0431\u0443\u0434\u043d\u0438\u0435 \u0434\u043d\u0438"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 342
    :cond_7
    const-string/jumbo v6, "weekend"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 343
    const-string/jumbo v6, " \u0432 \u0432\u044b\u0445\u043e\u0434\u043d\u044b\u0435"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 345
    :cond_8
    const-string/jumbo v6, " \u0432 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 245
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 248
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 249
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 250
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 257
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 258
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 259
    const-string/jumbo v5, "\u0432\u0435\u0441\u044c \u0434\u0435\u043d\u044c"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 269
    :cond_1
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 272
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :goto_2
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 279
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 280
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 282
    const-string/jumbo v5, "\u043c\u0435\u0441\u0442\u043e \u0432\u0441\u0442\u0440\u0435\u0447\u0438"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 289
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 252
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "\u0432\u0441\u0442\u0440\u0435\u0447\u0430"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 261
    .end local v4    # "title":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 262
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, "\u043d\u0430"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 264
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 274
    .end local v3    # "time":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x2

    .line 128
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->dateAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 129
    .local v0, "c":Ljava/util/Calendar;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->inDayOfWeekArray:[Ljava/lang/String;

    const/4 v6, 0x7

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aget-object v3, v5, v6

    .line 130
    .local v3, "inDayOfWeek":Ljava/lang/String;
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 131
    .local v2, "dayOfMonth":I
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->monthArray:[Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    aget-object v4, v5, v6

    .line 132
    .local v4, "month":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isDisplay()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 133
    const-string/jumbo v5, "%s, %d %s"

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    aput-object v4, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 136
    :goto_0
    return-object v5

    .line 135
    :cond_0
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->ordinals:[Ljava/lang/String;

    aget-object v1, v5, v2

    .line 136
    .local v1, "day":Ljava/lang/String;
    const-string/jumbo v5, "%s, %s %s"

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v3, v6, v7

    aput-object v1, v6, v9

    aput-object v4, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public postProcess(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 413
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/nlg/lang/Replace;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x2c

    .line 366
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 369
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 370
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 374
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 376
    const-string/jumbo v7, " \u0441\u0440\u043e\u043a \u0438\u0441\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 378
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 379
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 386
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 387
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 388
    const-string/jumbo v7, " \u043d\u0430\u043f\u043e\u043c\u0438\u043d\u0430\u043d\u0438\u0435 "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 390
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 391
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 397
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 398
    const-string/jumbo v7, " \u0432 "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 381
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 393
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x3a

    const/16 v8, 0x30

    const/16 v7, 0xc

    const/16 v6, 0x20

    const/16 v5, 0xa

    .line 142
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 143
    .local v0, "c":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 144
    .local v1, "h":I
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 145
    .local v2, "m":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->use24HourTime()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 147
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isSpoken()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 149
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    .line 150
    const-string/jumbo v4, "0 \u0447\u0430\u0441\u043e\u0432 0 \u043c\u0438\u043d\u0443\u0442"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 152
    :cond_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 154
    if-ge v2, v5, :cond_1

    .line 155
    const-string/jumbo v4, "0 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 161
    :cond_2
    if-ge v1, v5, :cond_3

    .line 162
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    :cond_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 165
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    if-ge v2, v5, :cond_4

    .line 167
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 169
    :cond_4
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 172
    :cond_5
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->isSpoken()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 174
    if-nez v1, :cond_7

    .line 175
    const-string/jumbo v4, "12"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :goto_1
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    if-ge v2, v5, :cond_6

    .line 183
    const-string/jumbo v4, "0 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_6
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 186
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 187
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->partOfDay(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 176
    :cond_7
    if-le v1, v7, :cond_8

    .line 177
    add-int/lit8 v4, v1, -0xc

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 179
    :cond_8
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 190
    :cond_9
    if-nez v1, :cond_b

    .line 191
    const-string/jumbo v4, "12"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :goto_2
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    if-ge v2, v5, :cond_a

    .line 199
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    :cond_a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->partOfDay(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 192
    :cond_b
    if-le v1, v7, :cond_c

    .line 193
    add-int/lit8 v4, v1, -0xc

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 195
    :cond_c
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ru;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
