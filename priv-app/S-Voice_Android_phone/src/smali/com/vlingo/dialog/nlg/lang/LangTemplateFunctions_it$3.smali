.class final Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;
.super Ljava/util/HashMap;
.source "LangTemplateFunctions_it.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 137
    const-string/jumbo v0, "sun mon tue wed thu fri sat"

    const-string/jumbo v1, "daily"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string/jumbo v0, "mon tue wed thu fri"

    const-string/jumbo v1, "weekday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string/jumbo v0, "sun sat"

    const-string/jumbo v1, "weekend"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string/jumbo v0, "sun"

    const-string/jumbo v1, "domenica"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string/jumbo v0, "mon"

    const-string/jumbo v1, "luned\u00ec"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string/jumbo v0, "tue"

    const-string/jumbo v1, "marted\u00ec"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const-string/jumbo v0, "wed"

    const-string/jumbo v1, "mercoled\u00ec"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string/jumbo v0, "thu"

    const-string/jumbo v1, "gioved\u00ec"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    const-string/jumbo v0, "fri"

    const-string/jumbo v1, "venerd\u00ec"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    const-string/jumbo v0, "sat"

    const-string/jumbo v1, "sabato"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_it$3;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    return-void
.end method
