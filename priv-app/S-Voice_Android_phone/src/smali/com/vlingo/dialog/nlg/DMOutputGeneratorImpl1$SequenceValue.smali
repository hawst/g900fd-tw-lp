.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl1.java"

# interfaces
.implements Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SequenceValue"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x6a4f8a901db77d19L


# instance fields
.field private values:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 111
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;>;"
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$100()Ljava/util/regex/Pattern;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 112
    .local v3, "matcher":Ljava/util/regex/Matcher;
    const/4 v0, 0x0

    .line 113
    .local v0, "c":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 114
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    .line 115
    .local v4, "s":I
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    .line 116
    .local v1, "e":I
    if-le v4, v0, :cond_0

    .line 117
    invoke-virtual {p1, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v5}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileExpression(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v5}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$300(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    move v0, v1

    .line 121
    goto :goto_0

    .line 122
    .end local v1    # "e":I
    .end local v4    # "s":I
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 123
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v5}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_2
    const-class v5, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    invoke-static {v2, v5}, Lcom/google/common/collect/Iterables;->toArray(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    iput-object v5, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;->values:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 126
    return-void
.end method


# virtual methods
.method public generate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .local v3, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$SequenceValue;->values:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .local v0, "arr$":[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 131
    .local v4, "value":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-interface {v4, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;->generate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    .end local v4    # "value":Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
