.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl1.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompiledPrompt"
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x56a7b57c308fd19fL


# instance fields
.field display:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

.field spoken:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;


# direct methods
.method public constructor <init>(Lcom/vlingo/dialog/nlg/config/Prompt;)V
    .locals 1
    .param p1, "prompt"    # Lcom/vlingo/dialog/nlg/config/Prompt;

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpokenDisplay()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpokenDisplay()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->display:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->spoken:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 149
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getSpoken()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->spoken:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    .line 147
    invoke-virtual {p1}, Lcom/vlingo/dialog/nlg/config/Prompt;->getDisplay()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->compileString(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1;->access$200(Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    move-result-object v0

    iput-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->display:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    goto :goto_0
.end method


# virtual methods
.method public generateDisplay(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->display:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    invoke-interface {v0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;->generate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateSpoken(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$CompiledPrompt;->spoken:Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;

    invoke-interface {v0, p1}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl1$Value;->generate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
