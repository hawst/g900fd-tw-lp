.class final Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;
.super Ljava/util/HashMap;
.source "LangTemplateFunctions_ko.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 308
    const-string/jumbo v0, "sun mon tue wed thu fri sat"

    const-string/jumbo v1, "daily"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    const-string/jumbo v0, "mon tue wed thu fri"

    const-string/jumbo v1, "weekday"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string/jumbo v0, "sun sat"

    const-string/jumbo v1, "weekend"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    const-string/jumbo v0, "sun"

    const-string/jumbo v1, "\uc77c\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    const-string/jumbo v0, "mon"

    const-string/jumbo v1, "\uc6d4\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    const-string/jumbo v0, "tue"

    const-string/jumbo v1, "\ud654\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    const-string/jumbo v0, "wed"

    const-string/jumbo v1, "\uc218\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    const-string/jumbo v0, "thu"

    const-string/jumbo v1, "\ubaa9\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    const-string/jumbo v0, "fri"

    const-string/jumbo v1, "\uae08\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    const-string/jumbo v0, "sat"

    const-string/jumbo v1, "\ud1a0\uc694\uc77c"

    invoke-virtual {p0, v0, v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_ko$4;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    return-void
.end method
