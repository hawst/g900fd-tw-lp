.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_es.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "d\u00eda entero"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "cita"

.field private static final APPOINTMENT_PRE_DATE:Ljava/lang/String; = "el"

.field private static final APPOINTMENT_PRE_LOCATION:Ljava/lang/String; = "en"

.field private static final APPOINTMENT_PRE_TIME:Ljava/lang/String; = "a las"

.field private static final APPOINTMENT_PRE_TIME_1:Ljava/lang/String; = "a la"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

.field private static final SKIP_CONTACT_XML:Ljava/lang/String; = "(?:<[^>]*>)?"

.field private static final SPOKEN_HOURS_12H:[Ljava/lang/String;

.field private static final SPOKEN_HOURS_24H:[Ljava/lang/String;

.field private static final TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_SPOKEN:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Ljava/util/Locale;

    const-string/jumbo v1, "es"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->LOCALE:Ljava/util/Locale;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, d \'de\' MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 21
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "h:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TIME_FORMAT_SPOKEN:Ljava/text/DateFormat;

    .line 24
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_ALT_MAP:Ljava/util/Map;

    .line 31
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " y "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, " y "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT:[Ljava/lang/String;

    .line 32
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " y "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, ", y "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    .line 33
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, " o "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", "

    aput-object v1, v0, v5

    const-string/jumbo v1, " o "

    aput-object v1, v0, v6

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->OR_FORMAT:[Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TYPE_MAP:Ljava/util/Map;

    .line 69
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "12 de la medianoche"

    aput-object v1, v0, v4

    const-string/jumbo v1, "1 da manh\u00e3"

    aput-object v1, v0, v5

    const-string/jumbo v1, "2 da manh\u00e3"

    aput-object v1, v0, v6

    const-string/jumbo v1, "3 da manh\u00e3"

    aput-object v1, v0, v7

    const-string/jumbo v1, "4 da manh\u00e3"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "5 da manh\u00e3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 da manh\u00e3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 da manh\u00e3"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "12 del mediod\u00eda"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "1 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "2 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "3 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "4 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "5 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "6 da tarde"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "7 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "8 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "9 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "10 da noite"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "11 da noite"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->SPOKEN_HOURS_12H:[Ljava/lang/String;

    .line 97
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "24 horas"

    aput-object v1, v0, v4

    const-string/jumbo v1, "1 hora"

    aput-object v1, v0, v5

    const-string/jumbo v1, "2 horas"

    aput-object v1, v0, v6

    const-string/jumbo v1, "3 horas"

    aput-object v1, v0, v7

    const-string/jumbo v1, "4 horas"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "5 horas"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "6 horas"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "7 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "8 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "9 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "10 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "11 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "12 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "13 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "14 horas"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "15 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "16 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "17 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "18 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "19 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "20 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "21 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "22 horas"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "23 horas"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->SPOKEN_HOURS_24H:[Ljava/lang/String;

    .line 247
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->alarmDaysMap:Ljava/util/Map;

    .line 361
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/Replace;

    const/4 v1, 0x5

    new-array v1, v1, [[Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, " y (?=(?:<[^>]*>)?[Ii])"

    aput-object v3, v2, v4

    const-string/jumbo v3, " e "

    aput-object v3, v2, v5

    aput-object v2, v1, v4

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, " o (?=(?:<[^>]*>)?[Oo])"

    aput-object v3, v2, v4

    const-string/jumbo v3, " u "

    aput-object v3, v2, v5

    aput-object v2, v1, v5

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, " las (?=una )"

    aput-object v3, v2, v4

    const-string/jumbo v3, " la "

    aput-object v3, v2, v5

    aput-object v2, v1, v6

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, " las (?=0*1\\D)"

    aput-object v3, v2, v4

    const-string/jumbo v3, " la "

    aput-object v3, v2, v5

    aput-object v2, v1, v7

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, " la (?=0*(?:[23456789]|1[0-9]))"

    aput-object v3, v2, v4

    const-string/jumbo v3, " las "

    aput-object v3, v2, v5

    aput-object v2, v1, v8

    invoke-direct {v0, v1}, Lcom/vlingo/dialog/nlg/lang/Replace;-><init>([[Ljava/lang/String;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private appendMinutes(Ljava/lang/StringBuilder;I)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "m"    # I

    .prologue
    .line 135
    if-lez p2, :cond_0

    .line 136
    const-string/jumbo v0, " y "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 138
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string/jumbo v0, "minuto"

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    return-void

    .line 139
    :cond_1
    const-string/jumbo v0, "minutos"

    goto :goto_0
.end method

.method private static prefixDays(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "days"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string/jumbo v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " los "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " el "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 274
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 277
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 280
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    .line 284
    if-eqz v2, :cond_4

    .line 285
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 286
    const-string/jumbo v5, " todos los d\u00edas"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    :cond_0
    :goto_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 305
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 306
    const-string/jumbo v5, ", desactivada"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 287
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 288
    const-string/jumbo v5, " todos los fines de semana"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 290
    :cond_3
    const-string/jumbo v5, " todos"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->prefixDays(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 294
    :cond_4
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 295
    const-string/jumbo v5, " durante los pr\u00f3ximo siete d\u00edas"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 296
    :cond_5
    const-string/jumbo v5, "weekend"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 297
    const-string/jumbo v5, " el fin de semana"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 299
    :cond_6
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->prefixDays(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 198
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 205
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 206
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 208
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 217
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 218
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 219
    const-string/jumbo v5, "d\u00eda entero"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :goto_2
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    :cond_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 230
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 232
    const-string/jumbo v5, "en"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 234
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 239
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 200
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "cita"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 210
    .end local v4    # "title":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "el"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 212
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 221
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, "time":Ljava/lang/String;
    const-string/jumbo v5, "a las"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 131
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public postProcess(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 374
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->POST_PROCESS_REPLACE:Lcom/vlingo/dialog/nlg/lang/Replace;

    invoke-virtual {v0, p1}, Lcom/vlingo/dialog/nlg/lang/Replace;->replace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x2c

    .line 319
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 321
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 322
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 323
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 327
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 329
    const-string/jumbo v7, " para "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 331
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 332
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 339
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 340
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 341
    const-string/jumbo v7, " recordatorio "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 343
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 344
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 350
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 351
    const-string/jumbo v7, " a las "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 334
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 346
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->AND_FORMAT_SERIES_COMMA:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xc

    .line 145
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->isDisplay()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->use24HourTime()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146
    :cond_0
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    .line 172
    :goto_0
    return-object v4

    .line 148
    :cond_1
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 149
    .local v0, "c":Ljava/util/Calendar;
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 150
    .local v1, "h":I
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 151
    .local v2, "m":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .local v3, "sb":Ljava/lang/StringBuilder;
    if-nez v1, :cond_2

    .line 153
    const-string/jumbo v4, "12 de la medianoche"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->appendMinutes(Ljava/lang/StringBuilder;I)V

    .line 172
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 155
    :cond_2
    if-ne v1, v7, :cond_3

    .line 156
    const-string/jumbo v4, "12 del mediod\u00eda"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-direct {p0, v3, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->appendMinutes(Ljava/lang/StringBuilder;I)V

    goto :goto_1

    .line 159
    :cond_3
    const/4 v4, 0x1

    if-eq v1, v4, :cond_4

    const/16 v4, 0xd

    if-ne v1, v4, :cond_5

    :cond_4
    if-nez v2, :cond_5

    .line 160
    const-string/jumbo v4, "una"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :goto_2
    if-ge v1, v7, :cond_6

    .line 165
    const-string/jumbo v4, " de la ma\u00f1ana"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 162
    :cond_5
    sget-object v4, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TIME_FORMAT_SPOKEN:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ":00"

    const-string/jumbo v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 166
    :cond_6
    const/16 v4, 0x13

    if-ge v1, v4, :cond_7

    .line 167
    const-string/jumbo v4, " de la tarde"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 169
    :cond_7
    const-string/jumbo v4, " de la noche"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_es;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
