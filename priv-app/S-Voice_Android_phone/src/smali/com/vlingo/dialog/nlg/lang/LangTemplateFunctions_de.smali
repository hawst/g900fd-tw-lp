.class public Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;
.super Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;
.source "LangTemplateFunctions_de.java"


# static fields
.field private static final ALL_DAY:Ljava/lang/String; = "ganzt\u00e4gig"

.field private static final AND_FORMAT:[Ljava/lang/String;

.field private static final AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

.field private static final APPOINTMENT:Ljava/lang/String; = "Termin"

.field private static final APPOINTMENT_DATE_ON:Ljava/lang/String; = "am"

.field private static final APPOINTMENT_LOCATION:Ljava/lang/String; = "Standort"

.field private static final APPOINTMENT_TIME_AT:Ljava/lang/String; = "um"

.field private static final DATE_ALT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/DateFormat;

.field private static final LOCALE:Ljava/util/Locale;

.field private static final OR_FORMAT:[Ljava/lang/String;

.field private static final SPOKEN_0_TO_59:[Ljava/lang/String;

.field private static final TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

.field private static final TIME_FORMAT_SPOKEN:Ljava/text/DateFormat;

.field private static final TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static final alarmDaysMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    sget-object v0, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->LOCALE:Ljava/util/Locale;

    .line 20
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "EEEE, d. MMMM"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_FORMAT:Ljava/text/DateFormat;

    .line 21
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "HH:mm"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "H \'Uhr\' m"

    sget-object v2, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->LOCALE:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->TIME_FORMAT_SPOKEN:Ljava/text/DateFormat;

    .line 24
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$1;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$1;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_ALT_MAP:Ljava/util/Map;

    .line 31
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " und "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, " und "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_FORMAT:[Ljava/lang/String;

    .line 32
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " und "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, ", und "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    .line 33
    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v1, " oder "

    aput-object v1, v0, v3

    const-string/jumbo v1, ", "

    aput-object v1, v0, v4

    const-string/jumbo v1, " oder "

    aput-object v1, v0, v5

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->OR_FORMAT:[Ljava/lang/String;

    .line 35
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$2;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$2;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->TYPE_MAP:Ljava/util/Map;

    .line 54
    const/16 v0, 0x3c

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "null"

    aput-object v1, v0, v3

    const-string/jumbo v1, "ein"

    aput-object v1, v0, v4

    const-string/jumbo v1, "zwei"

    aput-object v1, v0, v5

    const-string/jumbo v1, "drei"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "vier"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "f\u00fcnf"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "sechs"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "sieben"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "acht"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "neun"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "zehn"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "elf"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "zw\u00f6lf"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "dreizehn"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "vierzehn"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "f\u00fcnfzehn"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "sechzehn"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "siebzehn"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "achtzehn"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "neunzehn"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "zwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "einundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "zweiundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "dreiundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "vierundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "f\u00fcnfundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "sechsundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "siebenundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "achtundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "neunundzwanzig"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "drei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "einunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "zweiunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "dreiunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "vierunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "f\u00fcnfunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "sechsunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "siebenunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "achtunddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "neununddrei\u00dfig"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "vierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "einundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "zweiundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "dreiundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "vierundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "f\u00fcnfundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "sechsundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "siebenundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "achtundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "neunundvierzig"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "f\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string/jumbo v2, "einundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "zweiundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "dreiundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "vierundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "f\u00fcnfundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "sechsundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "siebenundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "achtundf\u00fcnfzig"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "neunundf\u00fcnfzig"

    aput-object v2, v0, v1

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->SPOKEN_0_TO_59:[Ljava/lang/String;

    .line 221
    new-instance v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;

    invoke-direct {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de$3;-><init>()V

    sput-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->alarmDaysMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions;-><init>()V

    return-void
.end method

.method private timeDisplay(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 128
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->TIME_FORMAT_DISPLAY:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->time(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private timeSpoken(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 133
    :try_start_0
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->timeAsCalendar(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 134
    .local v0, "c":Ljava/util/Calendar;
    const/16 v5, 0xb

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 135
    .local v2, "h":I
    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 136
    .local v3, "m":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v4, "sb":Ljava/lang/StringBuilder;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->SPOKEN_0_TO_59:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string/jumbo v5, " Uhr"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    if-eqz v3, :cond_0

    .line 140
    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 141
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->SPOKEN_0_TO_59:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 145
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v2    # "h":I
    .end local v3    # "m":I
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-object v5

    .line 144
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->badTime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method protected alarm(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "alarm"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    .line 240
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    .local v3, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 243
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string/jumbo v5, "repeat"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 246
    .local v2, "repeat":Z
    const-string/jumbo v5, "days"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "days":Ljava/lang/String;
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->alarmDaysMap:Ljava/util/Map;

    invoke-virtual {p0, v0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->renderDays(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_0

    .line 250
    if-eqz v2, :cond_3

    .line 251
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 252
    const-string/jumbo v5, " jeden Tag"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_0
    :goto_0
    const-string/jumbo v5, "enabled"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 268
    .local v1, "enabled":Z
    if-nez v1, :cond_1

    .line 269
    const-string/jumbo v5, ", deaktiviert"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 254
    .end local v1    # "enabled":Z
    :cond_2
    const-string/jumbo v5, " jeden "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 258
    :cond_3
    const-string/jumbo v5, "daily"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 259
    const-string/jumbo v5, " f\u00fcr die n\u00e4chsten sieben Tage"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 261
    :cond_4
    const-string/jumbo v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected alarmAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected andFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected appointment(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;Z)Ljava/lang/String;
    .locals 7
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "appointmentForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p3, "brief"    # Z

    .prologue
    const/16 v6, 0x20

    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v5, "title"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 174
    const-string/jumbo v5, "title"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 175
    .local v4, "title":Ljava/lang/String;
    invoke-static {v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 176
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :goto_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    const-string/jumbo v5, "time"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isEmptyOrUnspecifiedAmPm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 184
    const-string/jumbo v5, "all_day"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 185
    const-string/jumbo v5, "ganzt\u00e4gig"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    :cond_1
    const-string/jumbo v5, "date"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 194
    const-string/jumbo v5, "date_alt"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "dateAlt":Ljava/lang/String;
    invoke-static {v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 196
    sget-object v5, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_ALT_MAP:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :goto_2
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 203
    .end local v0    # "dateAlt":Ljava/lang/String;
    :cond_2
    if-nez p3, :cond_3

    const-string/jumbo v5, "location"

    invoke-interface {p1, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 204
    const-string/jumbo v5, "location"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isNotEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 206
    const-string/jumbo v5, "Standort"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 208
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 213
    .end local v1    # "location":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, " ,"

    invoke-static {v5, v6}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 178
    .restart local v4    # "title":Ljava/lang/String;
    :cond_4
    const-string/jumbo v5, "Termin"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 187
    .end local v4    # "title":Ljava/lang/String;
    :cond_5
    const-string/jumbo v5, "time"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 188
    .local v3, "time":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 198
    .end local v3    # "time":Ljava/lang/String;
    .restart local v0    # "dateAlt":Ljava/lang/String;
    :cond_6
    const-string/jumbo v5, "date"

    invoke-interface {p2, v5}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method protected appointmentAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public date(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawDate"    # Ljava/lang/String;

    .prologue
    .line 119
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {p0, p1, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->date(Ljava/lang/String;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected orFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->OR_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method protected task(Lcom/vlingo/dialog/model/IForm;Lcom/vlingo/dialog/model/IForm;)Ljava/lang/String;
    .locals 10
    .param p1, "taskForm"    # Lcom/vlingo/dialog/model/IForm;
    .param p2, "task"    # Lcom/vlingo/dialog/model/IForm;

    .prologue
    const/16 v9, 0x2c

    .line 282
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v7, "title"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 286
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 290
    :cond_0
    const-string/jumbo v7, "date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "date":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 292
    const-string/jumbo v7, " f\u00e4llig "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 294
    .local v1, "dateAlt":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 295
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    :goto_0
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 302
    .end local v1    # "dateAlt":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "reminder_date"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 303
    .local v2, "reminderDate":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 304
    const-string/jumbo v7, " Erinnerung "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    sget-object v7, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->DATE_ALT_MAP:Ljava/util/Map;

    const-string/jumbo v8, "reminder_date_alt"

    invoke-interface {p2, v8}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 306
    .local v3, "reminderDateAlt":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 307
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :goto_1
    const-string/jumbo v7, "reminder_timer"

    invoke-interface {p2, v7}, Lcom/vlingo/dialog/model/IForm;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 313
    .local v4, "reminderTime":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 314
    const-string/jumbo v7, " um "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    invoke-virtual {p0, v4}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->time(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    .end local v3    # "reminderDateAlt":Ljava/lang/String;
    .end local v4    # "reminderTime":Ljava/lang/String;
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, " ,"

    invoke-static {v7, v8}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->strip(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 297
    .end local v2    # "reminderDate":Ljava/lang/String;
    .restart local v1    # "dateAlt":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 309
    .end local v1    # "dateAlt":Ljava/lang/String;
    .restart local v2    # "reminderDate":Ljava/lang/String;
    .restart local v3    # "reminderDateAlt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v2}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected taskAndFormat()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->AND_SERIES_COMMA_FORMAT:[Ljava/lang/String;

    return-object v0
.end method

.method public time(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "rawTime"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->isDisplay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->timeDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->timeSpoken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected typeMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lcom/vlingo/dialog/nlg/lang/LangTemplateFunctions_de;->TYPE_MAP:Ljava/util/Map;

    return-object v0
.end method
