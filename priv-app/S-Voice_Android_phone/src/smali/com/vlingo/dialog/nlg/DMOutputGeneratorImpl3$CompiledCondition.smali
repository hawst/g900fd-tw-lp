.class Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
.super Ljava/lang/Object;
.source "DMOutputGeneratorImpl3.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CompiledCondition"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x45008077b2bfe4f0L


# instance fields
.field variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

.field when:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/vlingo/common/message/MNode;)V
    .locals 4
    .param p1, "conditionNode"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    const-string/jumbo v3, "When"

    invoke-virtual {p1, v3}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->uniquify(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->when:Ljava/lang/String;

    .line 135
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 136
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;>;"
    const-string/jumbo v3, "Prompt"

    invoke-virtual {p1, v3}, Lcom/vlingo/common/message/MNode;->findChildrenRecursive(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vlingo/common/message/MNode;

    .line 137
    .local v2, "promptNode":Lcom/vlingo/common/message/MNode;
    new-instance v3, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    invoke-direct {v3, v2}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;-><init>(Lcom/vlingo/common/message/MNode;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    .end local v2    # "promptNode":Lcom/vlingo/common/message/MNode;
    :cond_0
    const-class v3, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    invoke-static {v1, v3}, Lcom/google/common/collect/Iterables;->toArray(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    iput-object v3, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    .line 140
    return-void
.end method

.method public static compile(Lcom/vlingo/common/message/MNode;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;
    .locals 3
    .param p0, "conditionNode"    # Lcom/vlingo/common/message/MNode;

    .prologue
    .line 123
    const-string/jumbo v1, "When"

    invoke-virtual {p0, v1}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 124
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->VARIABLE_PATTERN:Ljava/util/regex/Pattern;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$200()Ljava/util/regex/Pattern;

    move-result-object v1

    const-string/jumbo v2, "When"

    invoke-virtual {p0, v2}, Lcom/vlingo/common/message/MNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 125
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    const/4 v1, 0x0

    .line 130
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;

    invoke-direct {v1, p0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;-><init>(Lcom/vlingo/common/message/MNode;)V

    goto :goto_0
.end method


# virtual methods
.method public selectVariant(Ljava/lang/Integer;)Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;
    .locals 4
    .param p1, "index"    # Ljava/lang/Integer;

    .prologue
    .line 148
    if-nez p1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 150
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->random:Ljava/util/Random;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$400()Ljava/util/Random;

    move-result-object v2

    monitor-enter v2

    .line 151
    :try_start_0
    # getter for: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->random:Ljava/util/Random;
    invoke-static {}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$400()Ljava/util/Random;

    move-result-object v1

    iget-object v3, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    array-length v3, v3

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 152
    .local v0, "i":I
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :goto_0
    iget-object v1, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->variants:[Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledPrompt;

    aget-object v1, v1, v0

    return-object v1

    .line 152
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 154
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    goto :goto_0

    .line 157
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "i":I
    goto :goto_0
.end method

.method public whenIsTrue(Lorg/apache/commons/jexl2/JexlContext;)Z
    .locals 1
    .param p1, "context"    # Lorg/apache/commons/jexl2/JexlContext;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->when:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3$CompiledCondition;->when:Ljava/lang/String;

    # invokes: Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->evaluateExpression(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/vlingo/dialog/nlg/DMOutputGeneratorImpl3;->access$300(Lorg/apache/commons/jexl2/JexlContext;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
