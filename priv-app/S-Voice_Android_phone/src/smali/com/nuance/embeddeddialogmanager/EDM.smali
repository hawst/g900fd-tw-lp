.class public Lcom/nuance/embeddeddialogmanager/EDM;
.super Ljava/lang/Object;
.source "EDM.java"


# static fields
.field private static final CONFIG_FILE_NAME:Ljava/lang/String; = "ttacfg.jar"

.field public static final C_SVN:Ljava/lang/String; = "13002"

.field private static final LOCK:Ljava/lang/Object;

.field private static final LOGGER:Lcom/vlingo/common/log4j/VLogger;

.field private static final SERIALIZED_FILE_NAME:Ljava/lang/String; = "vvscfg.ser"

.field private static instance:Lcom/nuance/embeddeddialogmanager/EDM;


# instance fields
.field private final capabilityMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dmConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

.field private edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

.field private eventProcessing:Lcom/nuance/embeddeddialogmanager/EventProcessing;

.field private volatile initializingStarted:Z

.field private outputGenerators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vlingo/dialog/nlg/DMOutputGenerator;",
            ">;"
        }
    .end annotation
.end field

.field private softwareVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/nuance/embeddeddialogmanager/EDM;

    invoke-static {v0}, Lcom/vlingo/common/log4j/VLogger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/common/log4j/VLogger;

    move-result-object v0

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->LOGGER:Lcom/vlingo/common/log4j/VLogger;

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->capabilityMap:Ljava/util/Map;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->outputGenerators:Ljava/util/Map;

    return-void
.end method

.method private copyAssets(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    const-string/jumbo v0, "ttacfg.jar"

    invoke-static {p1, v0}, Lcom/nuance/embeddeddialogmanager/utils/FileUtils;->copyAsset(Landroid/content/Context;Ljava/lang/String;)V

    .line 304
    const-string/jumbo v0, "vvscfg.ser"

    invoke-static {p1, v0}, Lcom/nuance/embeddeddialogmanager/utils/FileUtils;->copyAsset(Landroid/content/Context;Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method private getContext(Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/vlingo/dialog/DMContext;
    .locals 7
    .param p1, "data"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData;

    .prologue
    .line 245
    new-instance v0, Lcom/vlingo/dialog/DMContext;

    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->dmConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getDialogStateBytes()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getIncomingFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getClientDateTime()Lcom/vlingo/dialog/util/DateTime;

    move-result-object v4

    iget-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->capabilityMap:Ljava/util/Map;

    invoke-direct/range {v0 .. v5}, Lcom/vlingo/dialog/DMContext;-><init>(Lcom/vlingo/dialog/manager/model/DialogManagerConfig;[BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/util/Map;)V

    .line 246
    .local v0, "context":Lcom/vlingo/dialog/DMContext;
    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->capabilityMap:Ljava/util/Map;

    const-string/jumbo v2, "dmDialogMode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 247
    .local v6, "dmDialogMode":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 248
    invoke-virtual {v0, v6}, Lcom/vlingo/dialog/DMContext;->setDialogMode(Ljava/lang/String;)V

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->softwareVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setSoftwareVersion(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getViewableListSize()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 252
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getViewableListSize()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setListSize(I)V

    .line 254
    :cond_1
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getListPosition()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 255
    invoke-virtual {p1}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getListPosition()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/dialog/manager/model/ListPosition;->parse(Ljava/lang/String;)Lcom/vlingo/dialog/manager/model/ListPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->setListPosition(Lcom/vlingo/dialog/manager/model/ListPosition;)V

    .line 258
    :cond_2
    return-object v0
.end method

.method private getGenerator(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGenerator;
    .locals 4
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->outputGenerators:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/nlg/DMOutputGenerator;

    .line 286
    .local v1, "outputGenerator":Lcom/vlingo/dialog/nlg/DMOutputGenerator;
    if-nez v1, :cond_0

    .line 287
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "ttacfg.jar"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 289
    .local v0, "file":Ljava/io/File;
    const-string/jumbo v2, "tproject"

    invoke-static {v0, v2, p2}, Lcom/vlingo/voicepad/config/EDMConfig;->loadPromptsForLanguage(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGenerator;

    move-result-object v1

    .line 290
    iget-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->outputGenerators:Ljava/util/Map;

    invoke-interface {v2, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    return-object v1
.end method

.method public static getInstance()Lcom/nuance/embeddeddialogmanager/EDM;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/nuance/embeddeddialogmanager/EDM;

    invoke-direct {v0}, Lcom/nuance/embeddeddialogmanager/EDM;-><init>()V

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    .line 64
    :cond_0
    sget-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    return-object v0
.end method

.method private loadFromJar(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Lcom/vlingo/voicepad/config/EDMConfig;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string/jumbo v3, "ttacfg.jar"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/vlingo/voicepad/config/EDMConfig;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    .line 207
    return-void
.end method

.method private loadSerialized(Landroid/content/Context;)V
    .locals 6
    .param p1, "appContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/StreamCorruptedException;,
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 213
    const/4 v1, 0x0

    .line 214
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 216
    .local v2, "ois":Ljava/io/ObjectInputStream;
    :try_start_0
    const-string/jumbo v4, "vvscfg.ser"

    invoke-virtual {p1, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 217
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 218
    .local v0, "bis":Ljava/io/BufferedInputStream;
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    .local v3, "ois":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vlingo/voicepad/config/EDMConfig;

    iput-object v4, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    .line 221
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    .line 222
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 224
    if-eqz v1, :cond_0

    .line 226
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 231
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 233
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 242
    :cond_1
    :goto_1
    return-void

    .line 224
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "ois":Ljava/io/ObjectInputStream;
    .restart local v2    # "ois":Ljava/io/ObjectInputStream;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_2

    .line 226
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 231
    :cond_2
    :goto_3
    if-eqz v2, :cond_3

    .line 233
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 236
    :cond_3
    :goto_4
    throw v4

    .line 227
    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "ois":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 234
    :catch_1
    move-exception v4

    goto :goto_1

    .line 227
    .end local v0    # "bis":Ljava/io/BufferedInputStream;
    .end local v3    # "ois":Ljava/io/ObjectInputStream;
    .restart local v2    # "ois":Ljava/io/ObjectInputStream;
    :catch_2
    move-exception v5

    goto :goto_3

    .line 234
    :catch_3
    move-exception v5

    goto :goto_4

    .line 224
    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    .restart local v0    # "bis":Ljava/io/BufferedInputStream;
    .restart local v3    # "ois":Ljava/io/ObjectInputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "ois":Ljava/io/ObjectInputStream;
    .restart local v2    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_2
.end method

.method private processGoals(Lcom/vlingo/dialog/DMContext;)Ljava/util/List;
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vlingo/dialog/DMContext;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vlingo/voicepad/model/VLActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    iget-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/config/EDMConfig;->getGTAGenerator()Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;

    move-result-object v1

    .line 298
    .local v1, "gtaGenerator":Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getGoals()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vlingo/voicepad/tagtoaction/GoalToActionGenerator;->generateActionList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 299
    .local v0, "actionResults":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/voicepad/model/VLActionResult;>;"
    return-object v0
.end method

.method private processPrompts(Lcom/vlingo/dialog/DMContext;Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "appContext"    # Landroid/content/Context;
    .param p3, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vlingo/dialog/nlg/DMOutputGenerator$BadIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-direct {p0, p2, p3}, Lcom/nuance/embeddeddialogmanager/EDM;->getGenerator(Landroid/content/Context;Ljava/lang/String;)Lcom/vlingo/dialog/nlg/DMOutputGenerator;

    move-result-object v6

    .line 263
    .local v6, "outputGenerator":Lcom/vlingo/dialog/nlg/DMOutputGenerator;
    const/4 v7, 0x0

    .line 265
    .local v7, "use24":Z
    new-instance v0, Lcom/vlingo/message/model/request/DialogMeta;

    invoke-direct {v0}, Lcom/vlingo/message/model/request/DialogMeta;-><init>()V

    .line 266
    .local v0, "dialogMeta":Lcom/vlingo/message/model/request/DialogMeta;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/vlingo/message/model/request/DialogMeta;->setUse24HourTime(Ljava/lang/Boolean;)V

    .line 268
    invoke-virtual {p1}, Lcom/vlingo/dialog/DMContext;->getGoals()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vlingo/dialog/goal/model/Goal;

    .line 269
    .local v1, "goal":Lcom/vlingo/dialog/goal/model/Goal;
    instance-of v8, v1, Lcom/vlingo/dialog/goal/model/NlgGoal;

    if-eqz v8, :cond_0

    move-object v3, v1

    .line 270
    check-cast v3, Lcom/vlingo/dialog/goal/model/NlgGoal;

    .line 272
    .local v3, "nlgGoal":Lcom/vlingo/dialog/goal/model/NlgGoal;
    new-instance v4, Lcom/vlingo/tts/model/NLGRequest;

    invoke-direct {v4}, Lcom/vlingo/tts/model/NLGRequest;-><init>()V

    .line 273
    .local v4, "nlgReq":Lcom/vlingo/tts/model/NLGRequest;
    invoke-virtual {v4, p3}, Lcom/vlingo/tts/model/NLGRequest;->setLanguage(Ljava/lang/String;)V

    .line 274
    invoke-virtual {v3}, Lcom/vlingo/dialog/goal/model/NlgGoal;->getTemplate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/tts/model/NLGRequest;->setType(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v3}, Lcom/vlingo/dialog/goal/model/NlgGoal;->getTemplateForm()Lcom/vlingo/dialog/model/IForm;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/vlingo/tts/model/NLGRequest;->setContext(Lcom/vlingo/mda/util/MDAObject;)V

    .line 277
    invoke-interface {v6, v4, v0}, Lcom/vlingo/dialog/nlg/DMOutputGenerator;->generate(Lcom/vlingo/tts/model/NLGRequest;Lcom/vlingo/message/model/request/DialogMeta;)Lcom/vlingo/tts/model/NLGResponse;

    move-result-object v5

    .line 279
    .local v5, "nlgResp":Lcom/vlingo/tts/model/NLGResponse;
    invoke-virtual {v5}, Lcom/vlingo/tts/model/NLGResponse;->getDisplayForm()Lcom/vlingo/tts/model/NLGSegment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/tts/model/NLGSegment;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setDisplayText(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v5}, Lcom/vlingo/tts/model/NLGResponse;->getSpokenForm()Lcom/vlingo/tts/model/NLGSegment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/tts/model/NLGSegment;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/vlingo/dialog/goal/model/NlgGoal;->setSpeakText(Ljava/lang/String;)V

    goto :goto_0

    .line 282
    .end local v1    # "goal":Lcom/vlingo/dialog/goal/model/Goal;
    .end local v3    # "nlgGoal":Lcom/vlingo/dialog/goal/model/NlgGoal;
    .end local v4    # "nlgReq":Lcom/vlingo/tts/model/NLGRequest;
    .end local v5    # "nlgResp":Lcom/vlingo/tts/model/NLGResponse;
    :cond_1
    return-void
.end method

.method public static setTestInstance(Lcom/nuance/embeddeddialogmanager/EDM;)V
    .locals 1
    .param p0, "testInstance"    # Lcom/nuance/embeddeddialogmanager/EDM;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 333
    sget-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    if-eqz v0, :cond_0

    .line 334
    sget-object v0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    invoke-virtual {v0}, Lcom/nuance/embeddeddialogmanager/EDM;->destroy()V

    .line 337
    :cond_0
    sput-object p0, Lcom/nuance/embeddeddialogmanager/EDM;->instance:Lcom/nuance/embeddeddialogmanager/EDM;

    .line 338
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 119
    sget-object v1, Lcom/nuance/embeddeddialogmanager/EDM;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_0
    iget-boolean v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->eventProcessing:Lcom/nuance/embeddeddialogmanager/EventProcessing;

    invoke-virtual {v0}, Lcom/nuance/embeddeddialogmanager/EventProcessing;->destroyTemplates()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->dmConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    .line 126
    :cond_0
    monitor-exit v1

    .line 127
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCapabilityMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 342
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDM;->capabilityMap:Ljava/util/Map;

    return-object v0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "appVersion"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 76
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "EDM must be initialized on a worker thread"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 78
    :cond_0
    sget-object v4, Lcom/nuance/embeddeddialogmanager/EDM;->LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 79
    :try_start_0
    iget-boolean v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-nez v5, :cond_2

    .line 81
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    .line 82
    iget-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->outputGenerators:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/nuance/embeddeddialogmanager/EDM;->updateDynamicFeatureConfig(Landroid/content/Context;)V

    .line 87
    iput-object p2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->softwareVersion:Ljava/lang/String;

    .line 92
    invoke-direct {p0, p1}, Lcom/nuance/embeddeddialogmanager/EDM;->copyAssets(Landroid/content/Context;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/nuance/embeddeddialogmanager/EDM;->loadSerialized(Landroid/content/Context;)V

    .line 95
    invoke-static {p1}, Lcom/nuance/embeddeddialogmanager/EventProcessing;->getEventProcessing(Landroid/content/Context;)Lcom/nuance/embeddeddialogmanager/EventProcessing;

    move-result-object v5

    iput-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->eventProcessing:Lcom/nuance/embeddeddialogmanager/EventProcessing;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :goto_0
    :try_start_2
    iget-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    if-nez v5, :cond_1

    .line 103
    monitor-exit v4

    .line 111
    :goto_1
    return v2

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/nuance/embeddeddialogmanager/EDM;->LOGGER:Lcom/vlingo/common/log4j/VLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception during load of VVSConfig"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Lcom/vlingo/common/log4j/VLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    .line 100
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    goto :goto_0

    .line 112
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 105
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->edmConfig:Lcom/vlingo/voicepad/config/EDMConfig;

    invoke-virtual {v2}, Lcom/vlingo/voicepad/config/EDMConfig;->getDMCLoader()Lcom/vlingo/voicepad/config/DMConfigLoader;

    move-result-object v1

    .line 106
    .local v1, "loader":Lcom/vlingo/voicepad/config/DMConfigLoader;
    invoke-virtual {v1}, Lcom/vlingo/voicepad/config/DMConfigLoader;->getSingleConfig()Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDM;->dmConfig:Lcom/vlingo/dialog/manager/model/DialogManagerConfig;

    .line 111
    .end local v1    # "loader":Lcom/vlingo/voicepad/config/DMConfigLoader;
    :cond_2
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v2, v3

    goto :goto_1
.end method

.method public processEvents(Landroid/content/Context;Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;
    .locals 6
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "data"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 163
    iget-boolean v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-nez v1, :cond_0

    .line 164
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "EDM isn\'t initialized yet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 166
    :cond_0
    sget-object v2, Lcom/nuance/embeddeddialogmanager/EDM;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 168
    :try_start_0
    iget-boolean v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-nez v1, :cond_1

    .line 169
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "EDM isn\'t initialized yet"

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 172
    :cond_1
    :try_start_1
    invoke-direct {p0, p2}, Lcom/nuance/embeddeddialogmanager/EDM;->getContext(Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/vlingo/dialog/DMContext;

    move-result-object v0

    .line 173
    .local v0, "dmContext":Lcom/vlingo/dialog/DMContext;
    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->eventProcessing:Lcom/nuance/embeddeddialogmanager/EventProcessing;

    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getEventString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/nuance/embeddeddialogmanager/EventProcessing;->processEvents(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V

    .line 174
    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->finishGoals()V

    .line 176
    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 177
    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/nuance/embeddeddialogmanager/EDM;->processPrompts(Lcom/vlingo/dialog/DMContext;Landroid/content/Context;Ljava/lang/String;)V

    .line 178
    new-instance v1, Lcom/nuance/embeddeddialogmanager/EDMResults;

    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->getFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->close()[B

    move-result-object v4

    invoke-direct {p0, v0}, Lcom/nuance/embeddeddialogmanager/EDM;->processGoals(Lcom/vlingo/dialog/DMContext;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMResults;-><init>(Ljava/lang/String;[BLjava/util/List;)V

    monitor-exit v2

    return-object v1

    .line 180
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "No DM actions defined for NLU action string: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getNluActionString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public processNLUActionString(Landroid/content/Context;Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/nuance/embeddeddialogmanager/EDMResults;
    .locals 6
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "data"    # Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    iget-boolean v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-nez v1, :cond_0

    .line 132
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "EDM isn\'t initialized yet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_0
    sget-object v2, Lcom/nuance/embeddeddialogmanager/EDM;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 136
    :try_start_0
    iget-boolean v1, p0, Lcom/nuance/embeddeddialogmanager/EDM;->initializingStarted:Z

    if-nez v1, :cond_1

    .line 137
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "EDM isn\'t initialized yet"

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 158
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 140
    :cond_1
    :try_start_1
    invoke-direct {p0, p2}, Lcom/nuance/embeddeddialogmanager/EDM;->getContext(Lcom/nuance/embeddeddialogmanager/EDMInputStateData;)Lcom/vlingo/dialog/DMContext;

    move-result-object v0

    .line 141
    .local v0, "dmContext":Lcom/vlingo/dialog/DMContext;
    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->getNeedRecognition()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getNluActionString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/dialog/DMContext;->processRecognition(Ljava/lang/String;)V

    .line 144
    :cond_2
    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->finishGoals()V

    .line 146
    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->isDialogManagerFlow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 147
    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/nuance/embeddeddialogmanager/EDM;->processPrompts(Lcom/vlingo/dialog/DMContext;Landroid/content/Context;Ljava/lang/String;)V

    .line 153
    new-instance v1, Lcom/nuance/embeddeddialogmanager/EDMResults;

    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->getFieldId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/vlingo/dialog/DMContext;->close()[B

    move-result-object v4

    invoke-direct {p0, v0}, Lcom/nuance/embeddeddialogmanager/EDM;->processGoals(Lcom/vlingo/dialog/DMContext;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v1, v3, v4, v5}, Lcom/nuance/embeddeddialogmanager/EDMResults;-><init>(Ljava/lang/String;[BLjava/util/List;)V

    monitor-exit v2

    return-object v1

    .line 155
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "No DM actions defined for NLU action string: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;->getNluActionString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method

.method public updateDynamicFeatureConfig(Landroid/content/Context;)V
    .locals 7
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    .line 186
    const-wide/16 v0, 0x0

    .line 187
    .local v0, "counter":J
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 188
    .local v3, "settingsEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 189
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 190
    .local v4, "settingsValue":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 191
    :cond_1
    iget-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDM;->capabilityMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    const-wide/16 v5, 0x1

    add-long/2addr v0, v5

    goto :goto_0

    .line 199
    .end local v3    # "settingsEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v4    # "settingsValue":Ljava/lang/String;
    :cond_2
    return-void
.end method
