.class Lcom/nuance/embeddeddialogmanager/EventProcessing;
.super Ljava/lang/Object;
.source "EventProcessing.java"


# static fields
.field private static final EVENT_LIST:Ljava/lang/String; = "eventslist.xsl"

.field private static final META_PROVIDER:Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

.field private static final ivLogger:Lorg/apache/log4j/Logger;


# instance fields
.field private templates:Ljavax/xml/transform/Templates;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    const-class v0, Lcom/nuance/embeddeddialogmanager/EventProcessing;

    invoke-static {v0}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/Class;)Lorg/apache/log4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->ivLogger:Lorg/apache/log4j/Logger;

    .line 33
    new-instance v0, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/vlingo/mda/model/PackageMeta;

    const/4 v2, 0x0

    invoke-static {}, Lcom/vlingo/dialog/event/model/EventList;->getClassMetaStatic()Lcom/vlingo/mda/model/ClassMeta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/mda/model/ClassMeta;->getPackageMeta()Lcom/vlingo/mda/model/PackageMeta;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/vlingo/mda/util/DefaultMDAMetaProvider;-><init>([Lcom/vlingo/mda/model/PackageMeta;)V

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->META_PROVIDER:Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    return-void
.end method

.method private constructor <init>(Ljavax/xml/transform/Templates;)V
    .locals 1
    .param p1, "templates"    # Ljavax/xml/transform/Templates;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->templates:Ljavax/xml/transform/Templates;

    .line 40
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->templates:Ljavax/xml/transform/Templates;

    .line 41
    return-void
.end method

.method static getEventProcessing(Landroid/content/Context;)Lcom/nuance/embeddeddialogmanager/EventProcessing;
    .locals 4
    .param p0, "appContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerConfigurationException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v0

    .line 45
    .local v0, "factory":Ljavax/xml/transform/TransformerFactory;
    new-instance v1, Ljavax/xml/transform/stream/StreamSource;

    invoke-static {p0}, Lcom/nuance/embeddeddialogmanager/EventProcessing;->getInputStream(Landroid/content/Context;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljavax/xml/transform/stream/StreamSource;-><init>(Ljava/io/InputStream;)V

    .line 46
    .local v1, "stylesource":Ljavax/xml/transform/stream/StreamSource;
    invoke-virtual {v0, v1}, Ljavax/xml/transform/TransformerFactory;->newTemplates(Ljavax/xml/transform/Source;)Ljavax/xml/transform/Templates;

    move-result-object v2

    .line 47
    .local v2, "templates":Ljavax/xml/transform/Templates;
    new-instance v3, Lcom/nuance/embeddeddialogmanager/EventProcessing;

    invoke-direct {v3, v2}, Lcom/nuance/embeddeddialogmanager/EventProcessing;-><init>(Ljavax/xml/transform/Templates;)V

    return-object v3
.end method

.method private static getInputStream(Landroid/content/Context;)Ljava/io/InputStream;
    .locals 2
    .param p0, "appContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 67
    .local v0, "mgr":Landroid/content/res/AssetManager;
    const-string/jumbo v1, "eventslist.xsl"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    return-object v1
.end method

.method private transformEvents(Ljava/lang/String;)Ljava/lang/Object;
    .locals 8
    .param p1, "eventXml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v6, p0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->templates:Ljavax/xml/transform/Templates;

    invoke-interface {v6}, Ljavax/xml/transform/Templates;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v5

    .line 72
    .local v5, "transformer":Ljavax/xml/transform/Transformer;
    const-string/jumbo v6, "UTF-8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 73
    .local v0, "bytes":[B
    new-instance v1, Ljavax/xml/transform/stream/StreamSource;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v6}, Ljavax/xml/transform/stream/StreamSource;-><init>(Ljava/io/InputStream;)V

    .line 74
    .local v1, "eventSource":Ljavax/xml/transform/stream/StreamSource;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    array-length v6, v0

    invoke-direct {v3, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 75
    .local v3, "resultOutputStream":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljavax/xml/transform/stream/StreamResult;

    invoke-direct {v2, v3}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/OutputStream;)V

    .line 76
    .local v2, "result":Ljavax/xml/transform/stream/StreamResult;
    invoke-virtual {v5, v1, v2}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 77
    new-instance v4, Lcom/vlingo/mda/util/MDAObjectInputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v7, Lcom/nuance/embeddeddialogmanager/EventProcessing;->META_PROVIDER:Lcom/vlingo/mda/util/DefaultMDAMetaProvider;

    invoke-direct {v4, v6, v7}, Lcom/vlingo/mda/util/MDAObjectInputStream;-><init>(Ljava/io/InputStream;Lcom/vlingo/mda/util/MDAMetaProvider;)V

    .line 78
    .local v4, "resultTransformer":Lcom/vlingo/mda/util/MDAObjectInputStream;
    invoke-virtual {v4}, Lcom/vlingo/mda/util/MDAObjectInputStream;->readObject()Lcom/vlingo/mda/util/MDAObject;

    move-result-object v6

    return-object v6
.end method


# virtual methods
.method destroyTemplates()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EventProcessing;->templates:Ljavax/xml/transform/Templates;

    .line 52
    return-void
.end method

.method processEvents(Lcom/vlingo/dialog/DMContext;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Lcom/vlingo/dialog/DMContext;
    .param p2, "eventXml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p2}, Lcom/nuance/embeddeddialogmanager/EventProcessing;->transformEvents(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vlingo/dialog/event/model/EventList;

    .line 57
    .local v0, "eventList":Lcom/vlingo/dialog/event/model/EventList;
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Lcom/vlingo/dialog/event/model/EventList;->getEvents()Ljava/util/List;

    move-result-object v1

    .line 59
    .local v1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Event;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 60
    invoke-virtual {p1, v1}, Lcom/vlingo/dialog/DMContext;->processEvents(Ljava/util/List;)V

    .line 63
    .end local v1    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vlingo/dialog/event/model/Event;>;"
    :cond_0
    return-void
.end method
