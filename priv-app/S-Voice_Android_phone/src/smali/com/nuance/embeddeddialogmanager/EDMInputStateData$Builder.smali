.class public Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
.super Ljava/lang/Object;
.source "EDMInputStateData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static final X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation
.end field


# instance fields
.field private clientDateTime:Lcom/vlingo/dialog/util/DateTime;

.field private dialogStateBytes:[B

.field private eventsXmlList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private incomingFieldId:Ljava/lang/String;

.field private language:Ljava/lang/String;

.field private listPosition:Ljava/lang/String;

.field private nluActionName:Ljava/lang/String;

.field private nluActionParameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nluJson:Ljava/lang/String;

.field private viewableListSize:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string/jumbo v0, "dm_main"

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->incomingFieldId:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionParameters:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->eventsXmlList:Ljava/util/List;

    .line 30
    const-string/jumbo v0, "en-US"

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->language:Ljava/lang/String;

    return-void
.end method

.method private buildEventsXml()Ljava/lang/String;
    .locals 4

    .prologue
    .line 118
    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->eventsXmlList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 119
    const/4 v1, 0x0

    .line 123
    :goto_0
    return-object v1

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "<Events>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ""

    iget-object v3, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->eventsXmlList:Ljava/util/List;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</Events>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private buildNluActionXml()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 94
    iget-object v4, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    const/4 v4, 0x0

    .line 114
    :goto_0
    return-object v4

    .line 97
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v4, "<Action Name=\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "\">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 99
    const-string/jumbo v4, "<Params>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    iget-object v4, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionParameters:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 102
    .local v1, "key":Ljava/lang/String;
    const-string/jumbo v4, "<ActionParam Name=\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string/jumbo v4, "\" Value=\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v4, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionParameters:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 106
    .local v3, "value":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 107
    const-string/jumbo v3, ""

    .line 109
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string/jumbo v4, "\"/>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 112
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_2
    const-string/jumbo v4, "</Params>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 113
    const-string/jumbo v4, "</Action>"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public addEventXml(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 1
    .param p1, "xml"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->eventsXmlList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    return-object p0
.end method

.method public addNluActionParam(Ljava/lang/String;Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 1
    .param p1, "paramName"    # Ljava/lang/String;
    .param p2, "paramValue"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionParameters:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-object p0
.end method

.method public build()Lcom/nuance/embeddeddialogmanager/EDMInputStateData;
    .locals 10

    .prologue
    .line 85
    iget-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluJson:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 86
    invoke-direct {p0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->buildNluActionXml()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluJson:Ljava/lang/String;

    .line 88
    :cond_0
    invoke-direct {p0}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->buildEventsXml()Ljava/lang/String;

    move-result-object v5

    .line 89
    .local v5, "eventsXml":Ljava/lang/String;
    new-instance v0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;

    iget-object v1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->dialogStateBytes:[B

    iget-object v2, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->incomingFieldId:Ljava/lang/String;

    iget-object v3, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    iget-object v4, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluJson:Ljava/lang/String;

    iget-object v6, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->language:Ljava/lang/String;

    iget-object v7, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->viewableListSize:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->listPosition:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/nuance/embeddeddialogmanager/EDMInputStateData;-><init>([BLjava/lang/String;Lcom/vlingo/dialog/util/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/nuance/embeddeddialogmanager/EDMInputStateData$1;)V

    return-object v0
.end method

.method public setDateTime(Ljava/util/Date;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 1
    .param p1, "dateTime"    # Ljava/util/Date;

    .prologue
    .line 46
    sget-object v0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->X_VLDM_CLIENT_TIME_FORMAT:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/dialog/util/DateTime;->fromClientTime(Ljava/lang/String;)Lcom/vlingo/dialog/util/DateTime;

    move-result-object v0

    iput-object v0, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->clientDateTime:Lcom/vlingo/dialog/util/DateTime;

    .line 47
    return-object p0
.end method

.method public setDialogStateBytes([B)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "dialogStateBytes"    # [B

    .prologue
    .line 36
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->dialogStateBytes:[B

    .line 37
    return-object p0
.end method

.method public setFieldId(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->incomingFieldId:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->language:Ljava/lang/String;

    .line 67
    return-object p0
.end method

.method public setListPosition(Ljava/lang/String;)V
    .locals 0
    .param p1, "listPosition"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->listPosition:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setNluActionName(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluActionName:Ljava/lang/String;

    .line 52
    return-object p0
.end method

.method public setNluJson(Ljava/lang/String;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "nluJson"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->nluJson:Ljava/lang/String;

    .line 81
    return-object p0
.end method

.method public setViewableListSize(Ljava/lang/Integer;)Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;
    .locals 0
    .param p1, "size"    # Ljava/lang/Integer;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/nuance/embeddeddialogmanager/EDMInputStateData$Builder;->viewableListSize:Ljava/lang/Integer;

    .line 72
    return-object p0
.end method
