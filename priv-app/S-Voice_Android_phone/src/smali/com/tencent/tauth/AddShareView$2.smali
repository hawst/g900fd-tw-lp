.class Lcom/tencent/tauth/AddShareView$2;
.super Landroid/os/Handler;
.source "AddShareView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/AddShareView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/AddShareView;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/AddShareView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    .line 122
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 126
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 135
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/tencent/tauth/AddShareView;->access$1(Lcom/tencent/tauth/AddShareView;Landroid/app/ProgressDialog;)V

    .line 137
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string/jumbo v2, "\u8bf7\u6c42\u4e2d,\u8bf7\u7a0d\u5019..."

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-virtual {v1}, Lcom/tencent/tauth/AddShareView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :cond_1
    :goto_0
    return-void

    .line 128
    :pswitch_0
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 130
    iget-object v1, p0, Lcom/tencent/tauth/AddShareView$2;->this$0:Lcom/tencent/tauth/AddShareView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/tencent/tauth/AddShareView;->access$1(Lcom/tencent/tauth/AddShareView;Landroid/app/ProgressDialog;)V

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "AddShareView"

    const-string/jumbo v2, "activity is finished."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
