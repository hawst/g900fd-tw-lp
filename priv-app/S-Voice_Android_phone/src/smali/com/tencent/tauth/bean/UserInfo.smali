.class public Lcom/tencent/tauth/bean/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"


# instance fields
.field private mIcon_100:Ljava/lang/String;

.field private mIcon_30:Ljava/lang/String;

.field private mIcon_50:Ljava/lang/String;

.field private mNickName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "nickName"    # Ljava/lang/String;
    .param p2, "icon_30"    # Ljava/lang/String;
    .param p3, "icon_50"    # Ljava/lang/String;
    .param p4, "icon_100"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserInfo;->mNickName:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_30:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_50:Ljava/lang/String;

    .line 17
    iput-object p4, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_100:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getIcon_100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_100:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon_30()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_30:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon_50()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_50:Ljava/lang/String;

    return-object v0
.end method

.method public getNickName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserInfo;->mNickName:Ljava/lang/String;

    return-object v0
.end method

.method public setIcon_100(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_100"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_100:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setIcon_30(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_30"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_30:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setIcon_50(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_50"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_50:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setNickName(Ljava/lang/String;)V
    .locals 0
    .param p1, "nickName"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserInfo;->mNickName:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "nickname: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserInfo;->mNickName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_30: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_30:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_50: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_50:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_100: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserInfo;->mIcon_100:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
