.class public Lcom/tencent/tauth/bean/OpenId;
.super Ljava/lang/Object;
.source "OpenId.java"


# instance fields
.field private mClientId:Ljava/lang/String;

.field private mOpenId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "openId"    # Ljava/lang/String;
    .param p2, "clientId"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/tencent/tauth/bean/OpenId;->mOpenId:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/tencent/tauth/bean/OpenId;->mClientId:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/tencent/tauth/bean/OpenId;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/tencent/tauth/bean/OpenId;->mOpenId:Ljava/lang/String;

    return-object v0
.end method

.method public setClientId(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/tencent/tauth/bean/OpenId;->mClientId:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setOpenId(Ljava/lang/String;)V
    .locals 0
    .param p1, "openId"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/bean/OpenId;->mOpenId:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/tencent/tauth/bean/OpenId;->mOpenId:Ljava/lang/String;

    return-object v0
.end method
