.class public Lcom/tencent/tauth/bean/Album;
.super Ljava/lang/Object;
.source "Album.java"


# instance fields
.field private mAlbumid:Ljava/lang/String;

.field private mClassid:I

.field private mCreatetime:J

.field private mDesc:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPicnum:J

.field private mPriv:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;JI)V
    .locals 0
    .param p1, "albumid"    # Ljava/lang/String;
    .param p2, "classid"    # I
    .param p3, "createtime"    # J
    .param p5, "desc"    # Ljava/lang/String;
    .param p6, "name"    # Ljava/lang/String;
    .param p7, "picnum"    # J
    .param p9, "priv"    # I

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/tencent/tauth/bean/Album;->mAlbumid:Ljava/lang/String;

    .line 18
    iput p2, p0, Lcom/tencent/tauth/bean/Album;->mClassid:I

    .line 19
    iput-wide p3, p0, Lcom/tencent/tauth/bean/Album;->mCreatetime:J

    .line 20
    iput-object p5, p0, Lcom/tencent/tauth/bean/Album;->mDesc:Ljava/lang/String;

    .line 21
    iput-object p6, p0, Lcom/tencent/tauth/bean/Album;->mName:Ljava/lang/String;

    .line 22
    iput-wide p7, p0, Lcom/tencent/tauth/bean/Album;->mPicnum:J

    .line 23
    iput p9, p0, Lcom/tencent/tauth/bean/Album;->mPriv:I

    .line 24
    return-void
.end method


# virtual methods
.method public getAlbumid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/tencent/tauth/bean/Album;->mAlbumid:Ljava/lang/String;

    return-object v0
.end method

.method public getClassid()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/tencent/tauth/bean/Album;->mClassid:I

    return v0
.end method

.method public getCreatetime()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/tencent/tauth/bean/Album;->mCreatetime:J

    return-wide v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/tencent/tauth/bean/Album;->mDesc:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/tencent/tauth/bean/Album;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPicnum()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/tencent/tauth/bean/Album;->mPicnum:J

    return-wide v0
.end method

.method public getPriv()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/tencent/tauth/bean/Album;->mPriv:I

    return v0
.end method

.method public setAlbumid(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumid"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/tencent/tauth/bean/Album;->mAlbumid:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setClassid(I)V
    .locals 0
    .param p1, "classid"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/tencent/tauth/bean/Album;->mClassid:I

    .line 39
    return-void
.end method

.method public setCreatetime(J)V
    .locals 0
    .param p1, "createtime"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/tencent/tauth/bean/Album;->mCreatetime:J

    .line 48
    return-void
.end method

.method public setDesc(Ljava/lang/String;)V
    .locals 0
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/tencent/tauth/bean/Album;->mDesc:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/tencent/tauth/bean/Album;->mName:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setPicnum(J)V
    .locals 0
    .param p1, "picnum"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/tencent/tauth/bean/Album;->mPicnum:J

    .line 75
    return-void
.end method

.method public setPriv(I)V
    .locals 0
    .param p1, "priv"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/tencent/tauth/bean/Album;->mPriv:I

    .line 84
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "albumid: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/tencent/tauth/bean/Album;->mAlbumid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 89
    const-string/jumbo v1, "\nclassid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/tencent/tauth/bean/Album;->mClassid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 90
    const-string/jumbo v1, "\ncreatetime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/tencent/tauth/bean/Album;->mCreatetime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 91
    const-string/jumbo v1, "\ndesc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/Album;->mDesc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    const-string/jumbo v1, "\nname: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/Album;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    const-string/jumbo v1, "\npicnum: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/tencent/tauth/bean/Album;->mPicnum:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    const-string/jumbo v1, "\npriv: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/tencent/tauth/bean/Album;->mPriv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
