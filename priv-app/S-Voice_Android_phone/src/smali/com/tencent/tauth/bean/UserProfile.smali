.class public Lcom/tencent/tauth/bean/UserProfile;
.super Ljava/lang/Object;
.source "UserProfile.java"


# instance fields
.field private mGender:I

.field private mIcon_100:Ljava/lang/String;

.field private mIcon_30:Ljava/lang/String;

.field private mIcon_50:Ljava/lang/String;

.field private mRealName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "realName"    # Ljava/lang/String;
    .param p2, "gender"    # I
    .param p3, "icon_30"    # Ljava/lang/String;
    .param p4, "icon_50"    # Ljava/lang/String;
    .param p5, "icon_100"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mRealName:Ljava/lang/String;

    .line 16
    iput p2, p0, Lcom/tencent/tauth/bean/UserProfile;->mGender:I

    .line 17
    iput-object p3, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_30:Ljava/lang/String;

    .line 18
    iput-object p4, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_50:Ljava/lang/String;

    .line 19
    iput-object p5, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_100:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getGender()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mGender:I

    return v0
.end method

.method public getIcon_100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_100:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon_30()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_30:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon_50()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_50:Ljava/lang/String;

    return-object v0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mRealName:Ljava/lang/String;

    return-object v0
.end method

.method public setGender(I)V
    .locals 0
    .param p1, "gender"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mGender:I

    .line 65
    return-void
.end method

.method public setIcon_100(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_100"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_100:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setIcon_30(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_30"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_30:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setIcon_50(Ljava/lang/String;)V
    .locals 0
    .param p1, "icon_50"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_50:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setNickName(Ljava/lang/String;)V
    .locals 0
    .param p1, "realName"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/tencent/tauth/bean/UserProfile;->mRealName:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "realName: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserProfile;->mRealName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\ngender: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/tencent/tauth/bean/UserProfile;->mGender:I

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u5973"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_30: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_30:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_50: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_50:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\nicon_100: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/UserProfile;->mIcon_100:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "\u7537"

    goto :goto_0
.end method
