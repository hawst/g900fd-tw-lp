.class public Lcom/tencent/tauth/bean/Pic;
.super Ljava/lang/Object;
.source "Pic.java"


# instance fields
.field private mAlbumId:Ljava/lang/String;

.field private mHeight:I

.field private mLloc:Ljava/lang/String;

.field private mSloc:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "lloc"    # Ljava/lang/String;
    .param p3, "sloc"    # Ljava/lang/String;
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/tencent/tauth/bean/Pic;->mAlbumId:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/tencent/tauth/bean/Pic;->mLloc:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/tencent/tauth/bean/Pic;->mSloc:Ljava/lang/String;

    .line 18
    iput p4, p0, Lcom/tencent/tauth/bean/Pic;->mWidth:I

    .line 19
    iput p5, p0, Lcom/tencent/tauth/bean/Pic;->mHeight:I

    .line 20
    return-void
.end method


# virtual methods
.method public getAlbumId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/tencent/tauth/bean/Pic;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/tencent/tauth/bean/Pic;->mHeight:I

    return v0
.end method

.method public getLloc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/tencent/tauth/bean/Pic;->mLloc:Ljava/lang/String;

    return-object v0
.end method

.method public getSloc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/tencent/tauth/bean/Pic;->mSloc:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/tencent/tauth/bean/Pic;->mWidth:I

    return v0
.end method

.method public setAlbumId(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumId"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/tencent/tauth/bean/Pic;->mAlbumId:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/tencent/tauth/bean/Pic;->mHeight:I

    .line 65
    return-void
.end method

.method public setLloc(Ljava/lang/String;)V
    .locals 0
    .param p1, "lloc"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/tencent/tauth/bean/Pic;->mLloc:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setSloc(Ljava/lang/String;)V
    .locals 0
    .param p1, "sloc"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/tencent/tauth/bean/Pic;->mSloc:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/tencent/tauth/bean/Pic;->mWidth:I

    .line 56
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "albumid :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/tencent/tauth/bean/Pic;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    const-string/jumbo v1, "\nlloc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/Pic;->mLloc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 71
    const-string/jumbo v1, "\nsloc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/bean/Pic;->mSloc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 72
    const-string/jumbo v1, "\nheight: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/tencent/tauth/bean/Pic;->mHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 73
    const-string/jumbo v1, "\nwidth: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/tencent/tauth/bean/Pic;->mWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 74
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
