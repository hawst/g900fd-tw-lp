.class Lcom/tencent/tauth/AddShareView$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "AddShareView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/AddShareView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/AddShareView;


# direct methods
.method private constructor <init>(Lcom/tencent/tauth/AddShareView;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/tencent/tauth/AddShareView$MyWebViewClient;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/tencent/tauth/AddShareView;Lcom/tencent/tauth/AddShareView$MyWebViewClient;)V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0, p1}, Lcom/tencent/tauth/AddShareView$MyWebViewClient;-><init>(Lcom/tencent/tauth/AddShareView;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$MyWebViewClient;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$2(Lcom/tencent/tauth/AddShareView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 407
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 402
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$MyWebViewClient;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$2(Lcom/tencent/tauth/AddShareView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 403
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$MyWebViewClient;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$2(Lcom/tencent/tauth/AddShareView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 412
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 414
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "handler"    # Landroid/webkit/SslErrorHandler;
    .param p3, "error"    # Landroid/net/http/SslError;

    .prologue
    .line 418
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 419
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$MyWebViewClient;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$2(Lcom/tencent/tauth/AddShareView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 399
    const/4 v0, 0x0

    return v0
.end method
