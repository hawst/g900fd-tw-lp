.class public Lcom/tencent/tauth/TencentOpenHost;
.super Ljava/lang/Object;
.source "TencentOpenHost.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final AUTH_BROADCAST:Ljava/lang/String; = "com.tencent.auth.BROWSER"

.field public static final CALLBACK:Ljava/lang/String; = "callback"

.field public static final CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final ERROR_DES:Ljava/lang/String; = "error_description"

.field public static final ERROR_RET:Ljava/lang/String; = "error"

.field public static final EXPIRES_IN:Ljava/lang/String; = "expires_in"

.field public static final FROM_CMD:Ljava/lang/String; = "from_cmd"

.field public static final FROM_CMD_CALLBACK_STRING:Ljava/lang/String; = "auth://tauth.qq.com/"

.field public static final GRAPH_ACCESS_TOKEN_URL:Ljava/lang/String; = "https://graph.qq.com/oauth2.0/authorize?response_type=token&display=mobile&client_id=%s&scope=%s&redirect_uri=%s&status_userip=%s&status_os=%s&status_machine=%s&status_version=%s&cancel_display=1"

.field public static final GRAPH_ACCESS_TOKEN_URL_FOR_SYSBROWER_STRING:Ljava/lang/String; = "https://graph.qq.com/oauth2.0/authorize?response_type=token&display=mobile&client_id=%s&scope=%s&redirect_uri=%s&status_userip=%s&status_os=%s&status_machine=%s&status_version=%s"

.field public static final GRAPH_ADD_ALBUM:Ljava/lang/String; = "https://graph.qq.com/photo/add_album"

.field public static final GRAPH_ADD_SHARE:Ljava/lang/String; = "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory.html?access_token=%s&oauth_consumer_key=%s&openid=%s&"

.field public static final GRAPH_ADD_SHARE_H5:Ljava/lang/String; = "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory.html?access_token=%s&oauth_consumer_key=%s&openid=%s&title=%s&url=%s&comment=%s&summary=%s&images=%s&type=%s&playurl=%s&source=%s&site=%s"

.field public static final GRAPH_ADD_TOPIC:Ljava/lang/String; = "https://graph.qq.com/shuoshuo/add_topic"

.field static final GRAPH_ADD_WEIBO:Ljava/lang/String; = "https://graph.qq.com/wb/add_weibo"

.field public static final GRAPH_LIST_ALBUM:Ljava/lang/String; = "https://graph.qq.com/photo/list_album?access_token=%s&oauth_consumer_key=%s&openid=%s"

.field public static final GRAPH_OPENID_URL:Ljava/lang/String; = "https://graph.qq.com/oauth2.0/me?access_token=%s"

.field public static final GRAPH_UPLOAD_PIC:Ljava/lang/String; = "https://graph.qq.com/photo/upload_pic"

.field public static final GRAPH_USERINFO_URL:Ljava/lang/String; = "https://graph.qq.com/user/get_user_info?access_token=%s&oauth_consumer_key=%s&openid=%s"

.field public static final GRAPH_USERPROFILE_URL:Ljava/lang/String; = "https://graph.qq.com/user/get_user_profile?access_token=%s&oauth_consumer_key=%s&openid=%s"

.field public static final OPENID:Ljava/lang/String; = "openid"

.field public static final SCOPE:Ljava/lang/String; = "scope"

.field public static final TARGET:Ljava/lang/String; = "target"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
