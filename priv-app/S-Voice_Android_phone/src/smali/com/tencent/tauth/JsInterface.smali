.class public Lcom/tencent/tauth/JsInterface;
.super Ljava/lang/Object;
.source "JsInterface.java"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 17
    return-void
.end method


# virtual methods
.method public onAddShare(Ljava/lang/String;)V
    .locals 7
    .param p1, "json_result"    # Ljava/lang/String;

    .prologue
    const/high16 v6, -0x80000000

    .line 22
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 24
    .local v2, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 25
    .local v3, "ret":I
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_3

    .line 27
    .local v1, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v4, "ret"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 28
    const-string/jumbo v4, "msg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 34
    :goto_0
    if-nez v3, :cond_0

    .line 35
    :try_start_2
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v2}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V

    .line 49
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :goto_1
    return-void

    .line 29
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Lorg/json/JSONException;
    const/4 v3, -0x1

    goto :goto_0

    .line 37
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v3, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 39
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :catch_1
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 41
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 42
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 43
    .local v0, "e":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 44
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 45
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 46
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 47
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public onCancelAddShare(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v0, p1}, Lcom/tencent/tauth/http/Callback;->onCancel(I)V

    .line 54
    return-void
.end method

.method public onCancelGift()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/tencent/tauth/http/Callback;->onCancel(I)V

    .line 135
    return-void
.end method

.method public onCancelInvite()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/tencent/tauth/http/Callback;->onCancel(I)V

    .line 172
    return-void
.end method

.method public onCancelLogin()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/tencent/tauth/http/Callback;->onCancel(I)V

    .line 59
    return-void
.end method

.method public onCancelRequest()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/tencent/tauth/http/Callback;->onCancel(I)V

    .line 97
    return-void
.end method

.method public onGift(Ljava/lang/String;)V
    .locals 7
    .param p1, "json_result"    # Ljava/lang/String;

    .prologue
    const/high16 v6, -0x80000000

    .line 102
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 104
    .local v2, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 105
    .local v3, "ret":I
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_3

    .line 107
    .local v1, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v4, "ret"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 108
    const-string/jumbo v4, "msg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 114
    :goto_0
    if-nez v3, :cond_0

    .line 115
    :try_start_2
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v2}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V

    .line 129
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :goto_1
    return-void

    .line 109
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Lorg/json/JSONException;
    const/4 v3, -0x1

    goto :goto_0

    .line 117
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v3, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 119
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :catch_1
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 122
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 123
    .local v0, "e":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 124
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 125
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 126
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public onInvite(Ljava/lang/String;)V
    .locals 7
    .param p1, "json_result"    # Ljava/lang/String;

    .prologue
    const/high16 v6, -0x80000000

    .line 140
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 142
    .local v2, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 143
    .local v3, "ret":I
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_3

    .line 145
    .local v1, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v4, "ret"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 146
    const-string/jumbo v4, "msg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 152
    :goto_0
    if-nez v3, :cond_0

    .line 153
    :try_start_2
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v2}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V

    .line 167
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :goto_1
    return-void

    .line 147
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Lorg/json/JSONException;
    const/4 v3, -0x1

    goto :goto_0

    .line 155
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v3, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 157
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :catch_1
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 159
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 160
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 161
    .local v0, "e":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 162
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 163
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 164
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 165
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public onRequest(Ljava/lang/String;)V
    .locals 7
    .param p1, "json_result"    # Ljava/lang/String;

    .prologue
    const/high16 v6, -0x80000000

    .line 64
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 66
    .local v2, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 67
    .local v3, "ret":I
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_3

    .line 69
    .local v1, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v4, "ret"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 70
    const-string/jumbo v4, "msg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    .line 76
    :goto_0
    if-nez v3, :cond_0

    .line 77
    :try_start_2
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v2}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V

    .line 91
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :goto_1
    return-void

    .line 71
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Lorg/json/JSONException;
    const/4 v3, -0x1

    goto :goto_0

    .line 79
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v3, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 81
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :catch_1
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 83
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 84
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v0

    .line 85
    .local v0, "e":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 86
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 87
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v0

    .line 88
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v4, p0, Lcom/tencent/tauth/JsInterface;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 89
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1
.end method
