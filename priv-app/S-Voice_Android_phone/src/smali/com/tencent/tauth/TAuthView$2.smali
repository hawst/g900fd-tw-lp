.class Lcom/tencent/tauth/TAuthView$2;
.super Ljava/lang/Object;
.source "TAuthView.java"

# interfaces
.implements Lcom/tencent/tauth/http/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/TAuthView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/TAuthView;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/TAuthView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-virtual {v0}, Lcom/tencent/tauth/TAuthView;->finish()V

    .line 148
    return-void
.end method

.method public onFail(ILjava/lang/String;)V
    .locals 1
    .param p1, "ret"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-virtual {v0}, Lcom/tencent/tauth/TAuthView;->finish()V

    .line 171
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$2;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-virtual {v0}, Lcom/tencent/tauth/TAuthView;->finish()V

    .line 160
    return-void
.end method
