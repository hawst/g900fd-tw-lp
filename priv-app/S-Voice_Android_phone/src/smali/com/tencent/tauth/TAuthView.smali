.class public Lcom/tencent/tauth/TAuthView;
.super Landroid/app/Activity;
.source "TAuthView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/tencent/tauth/TAuthView$MyWebChromeClient;,
        Lcom/tencent/tauth/TAuthView$MyWebViewClient;
    }
.end annotation


# instance fields
.field private cache_callback:Ljava/lang/String;

.field private cache_client_id:Ljava/lang/String;

.field private cache_comment:Ljava/lang/String;

.field private cache_images:Ljava/lang/String;

.field private cache_playurl:Ljava/lang/String;

.field private cache_scope:Ljava/lang/String;

.field private cache_share_url:Ljava/lang/String;

.field private cache_site:Ljava/lang/String;

.field private cache_source:Ljava/lang/String;

.field private cache_summary:Ljava/lang/String;

.field private cache_target:Ljava/lang/String;

.field private cache_title:Ljava/lang/String;

.field private cache_type:Ljava/lang/String;

.field private cache_url:Ljava/lang/String;

.field private dialog:Landroid/app/ProgressDialog;

.field private from_cmd:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private js_interface:Lcom/tencent/tauth/JsInterface;

.field private mAccessToken:Ljava/lang/String;

.field private mAuthResult:Ljava/lang/String;

.field private mCallback:Ljava/lang/String;

.field private mErrorDes:Ljava/lang/String;

.field private mExpiresIn:Ljava/lang/String;

.field private mGraphURL:Ljava/lang/String;

.field private mRet:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;

.field private openidString:Ljava/lang/String;

.field private titleTxt:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    const-string/jumbo v0, "auth://tauth.qq.com/"

    iput-object v0, p0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/tencent/tauth/TAuthView;->openidString:Ljava/lang/String;

    .line 103
    new-instance v0, Lcom/tencent/tauth/TAuthView$1;

    invoke-direct {v0, p0}, Lcom/tencent/tauth/TAuthView$1;-><init>(Lcom/tencent/tauth/TAuthView;)V

    iput-object v0, p0, Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;

    .line 136
    new-instance v0, Lcom/tencent/tauth/JsInterface;

    new-instance v1, Lcom/tencent/tauth/TAuthView$2;

    invoke-direct {v1, p0}, Lcom/tencent/tauth/TAuthView$2;-><init>(Lcom/tencent/tauth/TAuthView;)V

    invoke-direct {v0, v1}, Lcom/tencent/tauth/JsInterface;-><init>(Lcom/tencent/tauth/http/Callback;)V

    iput-object v0, p0, Lcom/tencent/tauth/TAuthView;->js_interface:Lcom/tencent/tauth/JsInterface;

    .line 48
    return-void
.end method

.method static synthetic access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/tencent/tauth/TAuthView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$10(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_summary:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$11(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_images:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$12(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_type:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_playurl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$14(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_source:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$15(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_site:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$16(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$17(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_client_id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$18(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->openidString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$19(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_target:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$20(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/tencent/tauth/TAuthView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/tencent/tauth/TAuthView;->parseResult(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/tencent/tauth/TAuthView;)V
    .locals 0

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/tencent/tauth/TAuthView;->returnResult()V

    return-void
.end method

.method static synthetic access$6(Lcom/tencent/tauth/TAuthView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView;->openidString:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_share_url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->cache_comment:Ljava/lang/String;

    return-object v0
.end method

.method private getIp()Ljava/lang/String;
    .locals 7

    .prologue
    .line 530
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_1

    .line 542
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    const-string/jumbo v5, ""

    :goto_1
    return-object v5

    .line 531
    .restart local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 532
    .local v4, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 533
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 534
    .local v3, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    if-nez v5, :cond_2

    .line 535
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_1

    .line 539
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v1    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    :catch_0
    move-exception v2

    .line 540
    .local v2, "ex":Ljava/net/SocketException;
    const-string/jumbo v5, "TAG"

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getMachine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 551
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private getOS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 546
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method private getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    return-object v0
.end method

.method private parseResult(Ljava/lang/String;)V
    .locals 10
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 360
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView;->mAuthResult:Ljava/lang/String;

    .line 361
    move-object v4, p1

    .line 362
    .local v4, "tmp":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "?#"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 363
    const/16 v5, 0x23

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 367
    :goto_0
    const-string/jumbo v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "arr":[Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 369
    .local v3, "res":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    array-length v7, v0

    move v5, v6

    :goto_1
    if-lt v5, v7, :cond_1

    .line 374
    const-string/jumbo v5, "access_token"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/tencent/tauth/TAuthView;->mAccessToken:Ljava/lang/String;

    .line 375
    const-string/jumbo v5, "expires_in"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/tencent/tauth/TAuthView;->mExpiresIn:Ljava/lang/String;

    .line 376
    const-string/jumbo v5, "error"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/tencent/tauth/TAuthView;->mRet:Ljava/lang/String;

    .line 377
    const-string/jumbo v5, "error_description"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/tencent/tauth/TAuthView;->mErrorDes:Ljava/lang/String;

    .line 379
    return-void

    .line 365
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v3    # "res":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    const/16 v5, 0x3f

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 369
    .restart local v0    # "arr":[Ljava/lang/String;
    .restart local v3    # "res":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    aget-object v2, v0, v5

    .line 370
    .local v2, "item":Ljava/lang/String;
    const-string/jumbo v8, "="

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 371
    .local v1, "data":[Ljava/lang/String;
    aget-object v8, v1, v6

    const/4 v9, 0x1

    aget-object v9, v1, v9

    invoke-virtual {v3, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private returnResult()V
    .locals 2

    .prologue
    .line 382
    invoke-direct {p0}, Lcom/tencent/tauth/TAuthView;->sendBroadcastResult()V

    .line 383
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;

    const-string/jumbo v1, "send_store"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->mAccessToken:Ljava/lang/String;

    new-instance v1, Lcom/tencent/tauth/TAuthView$3;

    invoke-direct {v1, p0}, Lcom/tencent/tauth/TAuthView$3;-><init>(Lcom/tencent/tauth/TAuthView;)V

    invoke-static {v0, v1}, Lcom/tencent/tauth/TencentOpenAPI;->openid(Ljava/lang/String;Lcom/tencent/tauth/http/Callback;)V

    .line 461
    :cond_0
    invoke-virtual {p0}, Lcom/tencent/tauth/TAuthView;->finish()V

    .line 462
    return-void
.end method

.method private sendBroadcastResult()V
    .locals 3

    .prologue
    .line 471
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.tencent.auth.BROWSER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 472
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "raw"

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView;->mAuthResult:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 473
    const-string/jumbo v1, "access_token"

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 474
    const-string/jumbo v1, "expires_in"

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView;->mExpiresIn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 475
    const-string/jumbo v1, "error"

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView;->mRet:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 476
    const-string/jumbo v1, "error_description"

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView;->mErrorDes:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    invoke-virtual {p0, v0}, Lcom/tencent/tauth/TAuthView;->sendBroadcast(Landroid/content/Intent;)V

    .line 479
    return-void
.end method

.method private setWinTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 482
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->titleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 182
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 184
    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "https://graph.qq.com/oauth2.0/authorize?response_type=token&display=mobile&client_id=%s&scope=%s&redirect_uri=%s&status_userip=%s&status_os=%s&status_machine=%s&status_version=%s&cancel_display=1#"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->mGraphURL:Ljava/lang/String;

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 188
    .local v3, "bundle":Landroid/os/Bundle;
    if-eqz v3, :cond_3

    .line 189
    const-string/jumbo v17, "client_id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "client_id":Ljava/lang/String;
    const-string/jumbo v17, "scope"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 191
    .local v11, "scope":Ljava/lang/String;
    const-string/jumbo v17, "callback"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "callback":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string/jumbo v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 193
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    .line 196
    :cond_0
    const-string/jumbo v17, "title"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_title:Ljava/lang/String;

    .line 197
    const-string/jumbo v17, "url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_share_url:Ljava/lang/String;

    .line 198
    const-string/jumbo v17, "comment"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_comment:Ljava/lang/String;

    .line 199
    const-string/jumbo v17, "summary"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_summary:Ljava/lang/String;

    .line 200
    const-string/jumbo v17, "images"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_images:Ljava/lang/String;

    .line 201
    const-string/jumbo v17, "type"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_type:Ljava/lang/String;

    .line 202
    const-string/jumbo v17, "playurl"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_playurl:Ljava/lang/String;

    .line 203
    const-string/jumbo v17, "source"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_source:Ljava/lang/String;

    .line 204
    const-string/jumbo v17, "site"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->cache_site:Ljava/lang/String;

    .line 206
    const-string/jumbo v17, "from_cmd"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mGraphURL:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x7

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    const/16 v19, 0x1

    aput-object v11, v18, v19

    const/16 v19, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getIp()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getOS()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x5

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getMachine()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    .line 209
    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getVersion()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    .line 208
    invoke-static/range {v17 .. v18}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 211
    .local v15, "url":Ljava/lang/String;
    const-string/jumbo v17, "target"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 213
    .local v13, "target":Ljava/lang/String;
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/tencent/tauth/TAuthView;->cache_client_id:Ljava/lang/String;

    .line 214
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/tencent/tauth/TAuthView;->cache_scope:Ljava/lang/String;

    .line 215
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/tencent/tauth/TAuthView;->cache_callback:Ljava/lang/String;

    .line 216
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/tencent/tauth/TAuthView;->cache_url:Ljava/lang/String;

    .line 217
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/tencent/tauth/TAuthView;->cache_target:Ljava/lang/String;

    .line 219
    if-nez v13, :cond_1

    .line 222
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    .line 223
    .local v14, "uri":Landroid/net/Uri;
    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    .line 224
    .local v16, "url2":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/tencent/tauth/TAuthView;->parseResult(Ljava/lang/String;)V

    .line 225
    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->sendBroadcastResult()V

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    .end local v4    # "callback":Ljava/lang/String;
    .end local v5    # "client_id":Ljava/lang/String;
    .end local v11    # "scope":Ljava/lang/String;
    .end local v13    # "target":Ljava/lang/String;
    .end local v14    # "uri":Landroid/net/Uri;
    .end local v15    # "url":Ljava/lang/String;
    .end local v16    # "url2":Ljava/lang/String;
    :goto_0
    return-void

    .line 227
    .restart local v4    # "callback":Ljava/lang/String;
    .restart local v5    # "client_id":Ljava/lang/String;
    .restart local v11    # "scope":Ljava/lang/String;
    .restart local v13    # "target":Ljava/lang/String;
    .restart local v15    # "url":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 229
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 232
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_1
    const-string/jumbo v17, "_blank"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 234
    new-instance v17, Ljava/lang/StringBuilder;

    const-string/jumbo v18, "https://graph.qq.com/oauth2.0/authorize?response_type=token&display=mobile&client_id=%s&scope=%s&redirect_uri=%s&status_userip=%s&status_os=%s&status_machine=%s&status_version=%s#"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 235
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 234
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 236
    .local v7, "graphString":Ljava/lang/String;
    const/16 v17, 0x7

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v5, v17, v18

    const/16 v18, 0x1

    aput-object v11, v17, v18

    const/16 v18, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    const/16 v18, 0x3

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getIp()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x4

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getOS()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x5

    .line 237
    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getMachine()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x6

    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getVersion()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    .line 236
    move-object/from16 v0, v17

    invoke-static {v7, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 238
    .local v9, "no_display_url":Ljava/lang/String;
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 239
    .restart local v14    # "uri":Landroid/net/Uri;
    new-instance v8, Landroid/content/Intent;

    const-string/jumbo v17, "android.intent.action.VIEW"

    move-object/from16 v0, v17

    invoke-direct {v8, v0, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 240
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/tencent/tauth/TAuthView;->startActivity(Landroid/content/Intent;)V

    .line 241
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->finish()V

    goto :goto_0

    .line 244
    .end local v7    # "graphString":Ljava/lang/String;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "no_display_url":Ljava/lang/String;
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_2
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/tencent/tauth/TAuthView;->requestWindowFeature(I)Z

    .line 248
    new-instance v10, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 249
    .local v10, "root":Landroid/widget/LinearLayout;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 250
    new-instance v17, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v18, -0x1

    .line 251
    const/16 v19, -0x1

    invoke-direct/range {v17 .. v19}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 250
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 290
    new-instance v17, Landroid/webkit/WebView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    new-instance v18, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v19, -0x1

    .line 292
    const/16 v20, -0x1

    invoke-direct/range {v18 .. v20}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 291
    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    const/16 v18, 0x64

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    new-instance v18, Lcom/tencent/tauth/TAuthView$MyWebViewClient;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/tencent/tauth/TAuthView$MyWebViewClient;-><init>(Lcom/tencent/tauth/TAuthView;Lcom/tencent/tauth/TAuthView$MyWebViewClient;)V

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/webkit/WebView;->clearFormData()V

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v17

    sget-object v18, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v12

    .line 306
    .local v12, "setting":Landroid/webkit/WebSettings;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 307
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 308
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 309
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 310
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 311
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    .line 312
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 313
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->js_interface:Lcom/tencent/tauth/JsInterface;

    move-object/from16 v18, v0

    const-string/jumbo v19, "sdk_js_if"

    invoke-virtual/range {v17 .. v19}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    new-instance v18, Lcom/tencent/tauth/TAuthView$MyWebChromeClient;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/tencent/tauth/TAuthView$MyWebChromeClient;-><init>(Lcom/tencent/tauth/TAuthView;Lcom/tencent/tauth/TAuthView$MyWebChromeClient;)V

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 328
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/tencent/tauth/TAuthView;->setContentView(Landroid/view/View;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/TAuthView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/webkit/WebView;->requestFocus()Z

    goto/16 :goto_0

    .line 339
    .end local v4    # "callback":Ljava/lang/String;
    .end local v5    # "client_id":Ljava/lang/String;
    .end local v10    # "root":Landroid/widget/LinearLayout;
    .end local v11    # "scope":Ljava/lang/String;
    .end local v12    # "setting":Landroid/webkit/WebSettings;
    .end local v13    # "target":Ljava/lang/String;
    .end local v15    # "url":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    .line 340
    .restart local v14    # "uri":Landroid/net/Uri;
    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    .line 341
    .restart local v15    # "url":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/tencent/tauth/TAuthView;->parseResult(Ljava/lang/String;)V

    .line 342
    invoke-direct/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->sendBroadcastResult()V

    .line 343
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/TAuthView;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 344
    .end local v14    # "uri":Landroid/net/Uri;
    .end local v15    # "url":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 346
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 561
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 562
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 566
    :cond_0
    return-void
.end method
