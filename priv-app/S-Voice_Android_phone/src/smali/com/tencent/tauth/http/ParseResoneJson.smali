.class public Lcom/tencent/tauth/http/ParseResoneJson;
.super Ljava/lang/Object;
.source "ParseResoneJson.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseJson(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3
    .param p0, "response"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NumberFormatException;,
            Lcom/tencent/tauth/http/CommonException;
        }
    .end annotation

    .prologue
    .line 15
    const-string/jumbo v1, "false"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16
    new-instance v1, Lcom/tencent/tauth/http/CommonException;

    const-string/jumbo v2, "request failed"

    invoke-direct {v1, v2}, Lcom/tencent/tauth/http/CommonException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 18
    :cond_0
    const-string/jumbo v1, "true"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19
    const-string/jumbo p0, "{value : true}"

    .line 22
    :cond_1
    const-string/jumbo v1, "allback("

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 24
    const-string/jumbo v1, "[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z"

    const-string/jumbo v2, "$1"

    .line 23
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 25
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 33
    :cond_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, "json":Lorg/json/JSONObject;
    return-object v0
.end method
