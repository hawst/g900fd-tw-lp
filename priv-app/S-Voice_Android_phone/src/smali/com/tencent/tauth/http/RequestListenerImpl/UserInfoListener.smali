.class public Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "UserInfoListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UserInfoListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 22
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 12
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v11, -0x80000000

    .line 26
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 30
    .local v7, "obj":Lorg/json/JSONObject;
    const/4 v8, 0x0

    .line 31
    .local v8, "ret":I
    const-string/jumbo v5, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 33
    .local v5, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v9, "ret"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 34
    const-string/jumbo v9, "msg"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    .line 39
    :goto_0
    if-nez v8, :cond_0

    .line 40
    :try_start_2
    const-string/jumbo v9, "nickname"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 41
    .local v6, "nickname":Ljava/lang/String;
    const-string/jumbo v9, "figureurl"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "icon_30":Ljava/lang/String;
    const-string/jumbo v9, "figureurl_1"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    .local v3, "icon_50":Ljava/lang/String;
    const-string/jumbo v9, "figureurl_2"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "icon_100":Ljava/lang/String;
    new-instance v4, Lcom/tencent/tauth/bean/UserInfo;

    invoke-direct {v4, v6, v2, v3, v1}, Lcom/tencent/tauth/bean/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .local v4, "info":Lcom/tencent/tauth/bean/UserInfo;
    iget-object v9, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v9, v4}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 60
    .end local v1    # "icon_100":Ljava/lang/String;
    .end local v2    # "icon_30":Ljava/lang/String;
    .end local v3    # "icon_50":Ljava/lang/String;
    .end local v4    # "info":Lcom/tencent/tauth/bean/UserInfo;
    .end local v5    # "msg":Ljava/lang/String;
    .end local v6    # "nickname":Ljava/lang/String;
    .end local v7    # "obj":Lorg/json/JSONObject;
    .end local v8    # "ret":I
    :goto_1
    const-string/jumbo v9, "UserInfoListener"

    invoke-static {v9, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void

    .line 48
    .restart local v5    # "msg":Ljava/lang/String;
    .restart local v7    # "obj":Lorg/json/JSONObject;
    .restart local v8    # "ret":I
    :cond_0
    :try_start_3
    iget-object v9, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v9, v8, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 50
    .end local v5    # "msg":Ljava/lang/String;
    .end local v7    # "obj":Lorg/json/JSONObject;
    .end local v8    # "ret":I
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v9, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v11, v10}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 52
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 53
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v0

    .line 54
    .local v0, "e":Lorg/json/JSONException;
    iget-object v9, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v11, v10}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 55
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 56
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 57
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v9, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v11, v10}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 58
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 35
    .end local v0    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v5    # "msg":Ljava/lang/String;
    .restart local v7    # "obj":Lorg/json/JSONObject;
    .restart local v8    # "ret":I
    :catch_3
    move-exception v9

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 66
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 71
    return-void
.end method
