.class public Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "AddAlbumListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AddAlbumListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 22
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 14
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 26
    invoke-super/range {p0 .. p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 30
    .local v12, "obj":Lorg/json/JSONObject;
    const/4 v13, 0x0

    .line 31
    .local v13, "ret":I
    const-string/jumbo v11, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 33
    .local v11, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v1, "ret"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 34
    const-string/jumbo v1, "msg"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v11

    .line 39
    :goto_0
    if-nez v13, :cond_0

    .line 40
    :try_start_2
    new-instance v0, Lcom/tencent/tauth/bean/Album;

    const-string/jumbo v1, "albumid"

    invoke-virtual {v12, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    const-string/jumbo v2, "classid"

    invoke-virtual {v12, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 42
    const-string/jumbo v3, "createtime"

    invoke-virtual {v12, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 43
    const-string/jumbo v5, "desc"

    invoke-virtual {v12, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 44
    const-string/jumbo v6, "name"

    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 45
    const-wide/16 v7, 0x0

    .line 46
    const-string/jumbo v9, "priv"

    invoke-virtual {v12, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 40
    invoke-direct/range {v0 .. v9}, Lcom/tencent/tauth/bean/Album;-><init>(Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;JI)V

    .line 47
    .local v0, "album":Lcom/tencent/tauth/bean/Album;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v0}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 61
    .end local v0    # "album":Lcom/tencent/tauth/bean/Album;
    .end local v11    # "msg":Ljava/lang/String;
    .end local v12    # "obj":Lorg/json/JSONObject;
    .end local v13    # "ret":I
    :goto_1
    const-string/jumbo v1, "AddAlbumListener"

    invoke-static {v1, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void

    .line 49
    .restart local v11    # "msg":Ljava/lang/String;
    .restart local v12    # "obj":Lorg/json/JSONObject;
    .restart local v13    # "ret":I
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v13, v11}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 51
    .end local v11    # "msg":Ljava/lang/String;
    .end local v12    # "obj":Lorg/json/JSONObject;
    .end local v13    # "ret":I
    :catch_0
    move-exception v10

    .line 52
    .local v10, "e":Ljava/lang/NumberFormatException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v2, -0x80000000

    invoke-virtual {v10}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 53
    invoke-virtual {v10}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 54
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v10

    .line 55
    .local v10, "e":Lorg/json/JSONException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v2, -0x80000000

    invoke-virtual {v10}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 56
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 57
    .end local v10    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v10

    .line 58
    .local v10, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v2, -0x80000000

    invoke-virtual {v10}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 59
    invoke-virtual {v10}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 35
    .end local v10    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v11    # "msg":Ljava/lang/String;
    .restart local v12    # "obj":Lorg/json/JSONObject;
    .restart local v13    # "ret":I
    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 67
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 72
    return-void
.end method
