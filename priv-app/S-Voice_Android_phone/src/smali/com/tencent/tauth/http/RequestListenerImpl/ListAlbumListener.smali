.class public Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "ListAlbumListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ListAlbumListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 24
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 21
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 28
    invoke-super/range {p0 .. p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 30
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 32
    .local v19, "obj":Lorg/json/JSONObject;
    const/16 v20, 0x0

    .line 33
    .local v20, "ret":I
    const-string/jumbo v18, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 35
    .local v18, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v3, "ret"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    .line 36
    const-string/jumbo v3, "msg"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v18

    .line 41
    :goto_0
    if-nez v20, :cond_1

    .line 42
    :try_start_2
    const-string/jumbo v3, "albumnum"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 43
    .local v14, "albumnum":I
    const-string/jumbo v3, "album"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 44
    .local v12, "albumArray":Lorg/json/JSONArray;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v15, "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/tencent/tauth/bean/Album;>;"
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    move/from16 v0, v17

    if-lt v0, v14, :cond_0

    .line 56
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v3, v15}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 70
    .end local v12    # "albumArray":Lorg/json/JSONArray;
    .end local v14    # "albumnum":I
    .end local v15    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/tencent/tauth/bean/Album;>;"
    .end local v17    # "i":I
    .end local v18    # "msg":Ljava/lang/String;
    .end local v19    # "obj":Lorg/json/JSONObject;
    .end local v20    # "ret":I
    :goto_2
    const-string/jumbo v3, "ListAlbumListener"

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void

    .line 46
    .restart local v12    # "albumArray":Lorg/json/JSONArray;
    .restart local v14    # "albumnum":I
    .restart local v15    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/tencent/tauth/bean/Album;>;"
    .restart local v17    # "i":I
    .restart local v18    # "msg":Ljava/lang/String;
    .restart local v19    # "obj":Lorg/json/JSONObject;
    .restart local v20    # "ret":I
    :cond_0
    :try_start_3
    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 47
    .local v13, "albumObj":Lorg/json/JSONObject;
    new-instance v2, Lcom/tencent/tauth/bean/Album;

    const-string/jumbo v3, "albumid"

    invoke-virtual {v13, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 48
    const-string/jumbo v4, "classid"

    invoke-virtual {v13, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 49
    const-string/jumbo v5, "createtime"

    invoke-virtual {v13, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 50
    const-string/jumbo v7, "desc"

    invoke-virtual {v13, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 51
    const-string/jumbo v8, "name"

    invoke-virtual {v13, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 52
    const-string/jumbo v9, "picnum"

    invoke-virtual {v13, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    int-to-long v9, v9

    .line 53
    const-string/jumbo v11, "priv"

    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 47
    invoke-direct/range {v2 .. v11}, Lcom/tencent/tauth/bean/Album;-><init>(Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;JI)V

    .line 54
    .local v2, "album":Lcom/tencent/tauth/bean/Album;
    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 58
    .end local v2    # "album":Lcom/tencent/tauth/bean/Album;
    .end local v12    # "albumArray":Lorg/json/JSONArray;
    .end local v13    # "albumObj":Lorg/json/JSONObject;
    .end local v14    # "albumnum":I
    .end local v15    # "albums":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/tencent/tauth/bean/Album;>;"
    .end local v17    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    move/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v3, v0, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 60
    .end local v18    # "msg":Ljava/lang/String;
    .end local v19    # "obj":Lorg/json/JSONObject;
    .end local v20    # "ret":I
    :catch_0
    move-exception v16

    .line 61
    .local v16, "e":Ljava/lang/NumberFormatException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v4, -0x80000000

    invoke-virtual/range {v16 .. v16}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 62
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2

    .line 63
    .end local v16    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v16

    .line 64
    .local v16, "e":Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v4, -0x80000000

    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 65
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 66
    .end local v16    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v16

    .line 67
    .local v16, "e":Lcom/tencent/tauth/http/CommonException;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v4, -0x80000000

    invoke-virtual/range {v16 .. v16}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 68
    invoke-virtual/range {v16 .. v16}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto/16 :goto_2

    .line 37
    .end local v16    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v18    # "msg":Ljava/lang/String;
    .restart local v19    # "obj":Lorg/json/JSONObject;
    .restart local v20    # "ret":I
    :catch_3
    move-exception v3

    goto/16 :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 76
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 81
    return-void
.end method
