.class public Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "UserProfileListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UserProfileListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 27
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 12
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v11, -0x80000000

    .line 31
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 35
    .local v8, "obj":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 36
    .local v9, "ret":I
    const-string/jumbo v7, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 38
    .local v7, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v1, "ret"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 39
    const-string/jumbo v1, "msg"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    .line 44
    :goto_0
    if-nez v9, :cond_2

    .line 45
    :try_start_2
    const-string/jumbo v1, "gender"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 46
    .local v10, "sex":Ljava/lang/String;
    const/4 v2, 0x0

    .line 48
    .local v2, "i":I
    const-string/jumbo v1, "\u7537"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const/4 v2, 0x1

    .line 61
    :goto_1
    new-instance v0, Lcom/tencent/tauth/bean/UserProfile;

    const-string/jumbo v1, "realname"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    const-string/jumbo v3, "figureurl"

    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    const-string/jumbo v4, "figureurl_1"

    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    const-string/jumbo v5, "figureurl_2"

    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 61
    invoke-direct/range {v0 .. v5}, Lcom/tencent/tauth/bean/UserProfile;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .local v0, "profile":Lcom/tencent/tauth/bean/UserProfile;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v0}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 80
    .end local v0    # "profile":Lcom/tencent/tauth/bean/UserProfile;
    .end local v2    # "i":I
    .end local v7    # "msg":Ljava/lang/String;
    .end local v8    # "obj":Lorg/json/JSONObject;
    .end local v9    # "ret":I
    .end local v10    # "sex":Ljava/lang/String;
    :goto_2
    const-string/jumbo v1, "UserProfileListener"

    invoke-static {v1, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void

    .line 52
    .restart local v2    # "i":I
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v8    # "obj":Lorg/json/JSONObject;
    .restart local v9    # "ret":I
    .restart local v10    # "sex":Ljava/lang/String;
    :cond_0
    :try_start_3
    const-string/jumbo v1, "\u5973"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    const/4 v2, 0x0

    goto :goto_1

    .line 58
    :cond_1
    const/4 v2, 0x2

    goto :goto_1

    .line 68
    .end local v2    # "i":I
    .end local v10    # "sex":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v9, v7}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 70
    .end local v7    # "msg":Ljava/lang/String;
    .end local v8    # "obj":Lorg/json/JSONObject;
    .end local v9    # "ret":I
    :catch_0
    move-exception v6

    .line 71
    .local v6, "e":Ljava/lang/NumberFormatException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v11, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 72
    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2

    .line 73
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v6

    .line 74
    .local v6, "e":Lorg/json/JSONException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v11, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 75
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 76
    .end local v6    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v6

    .line 77
    .local v6, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v11, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 78
    invoke-virtual {v6}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_2

    .line 40
    .end local v6    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v8    # "obj":Lorg/json/JSONObject;
    .restart local v9    # "ret":I
    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 86
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 91
    return-void
.end method
