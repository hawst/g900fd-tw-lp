.class public Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "OpenIDListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OpenIDListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 22
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 10
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v9, -0x80000000

    .line 26
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 30
    .local v4, "obj":Lorg/json/JSONObject;
    const/4 v6, 0x0

    .line 31
    .local v6, "ret":I
    const-string/jumbo v3, ""
    :try_end_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    .line 33
    .local v3, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v7, "error"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 34
    const-string/jumbo v7, "error_description"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 39
    :goto_0
    if-nez v6, :cond_0

    .line 40
    :try_start_2
    const-string/jumbo v7, "openid"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 41
    .local v5, "openid":Ljava/lang/String;
    const-string/jumbo v7, "client_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "client_id":Ljava/lang/String;
    new-instance v2, Lcom/tencent/tauth/bean/OpenId;

    invoke-direct {v2, v5, v0}, Lcom/tencent/tauth/bean/OpenId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .local v2, "id":Lcom/tencent/tauth/bean/OpenId;
    iget-object v7, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v7, v2}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 56
    .end local v0    # "client_id":Ljava/lang/String;
    .end local v2    # "id":Lcom/tencent/tauth/bean/OpenId;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "obj":Lorg/json/JSONObject;
    .end local v5    # "openid":Ljava/lang/String;
    .end local v6    # "ret":I
    :goto_1
    const-string/jumbo v7, "OpenIDListener"

    invoke-static {v7, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void

    .line 45
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "obj":Lorg/json/JSONObject;
    .restart local v6    # "ret":I
    :cond_0
    :try_start_3
    iget-object v7, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v7, v6, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 48
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "obj":Lorg/json/JSONObject;
    .end local v6    # "ret":I
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v7, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v1}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v9, v8}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 50
    invoke-virtual {v1}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 51
    .end local v1    # "e":Lcom/tencent/tauth/http/CommonException;
    :catch_1
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v9, v8}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    goto :goto_1

    .line 53
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :catch_2
    move-exception v1

    .line 54
    .local v1, "e":Lorg/json/JSONException;
    iget-object v7, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v9, v8}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    goto :goto_1

    .line 35
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "obj":Lorg/json/JSONObject;
    .restart local v6    # "ret":I
    :catch_3
    move-exception v7

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 62
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 67
    return-void
.end method
