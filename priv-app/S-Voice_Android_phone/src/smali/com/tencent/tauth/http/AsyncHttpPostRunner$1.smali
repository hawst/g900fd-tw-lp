.class Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;
.super Ljava/lang/Thread;
.source "AsyncHttpPostRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/tencent/tauth/http/AsyncHttpPostRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/http/AsyncHttpPostRunner;

.field private final synthetic val$httpMethod:Ljava/lang/String;

.field private final synthetic val$listener:Lcom/tencent/tauth/http/IRequestListener;

.field private final synthetic val$parameters:Landroid/os/Bundle;

.field private final synthetic val$state:Ljava/lang/Object;

.field private final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/http/AsyncHttpPostRunner;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->this$0:Lcom/tencent/tauth/http/AsyncHttpPostRunner;

    iput-object p2, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$httpMethod:Ljava/lang/String;

    iput-object p4, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$parameters:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$listener:Lcom/tencent/tauth/http/IRequestListener;

    iput-object p6, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$state:Ljava/lang/Object;

    .line 49
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 53
    :try_start_0
    iget-object v2, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$url:Ljava/lang/String;

    iget-object v3, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$httpMethod:Ljava/lang/String;

    iget-object v4, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$parameters:Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/tencent/tauth/http/ClientHttpRequest;->openUrl(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "resp":Ljava/lang/String;
    iget-object v2, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$listener:Lcom/tencent/tauth/http/IRequestListener;

    iget-object v3, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$state:Ljava/lang/Object;

    invoke-interface {v2, v1, v3}, Lcom/tencent/tauth/http/IRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 60
    .end local v1    # "resp":Ljava/lang/String;
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/FileNotFoundException;
    iget-object v2, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$listener:Lcom/tencent/tauth/http/IRequestListener;

    iget-object v3, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$state:Ljava/lang/Object;

    invoke-interface {v2, v0, v3}, Lcom/tencent/tauth/http/IRequestListener;->onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$listener:Lcom/tencent/tauth/http/IRequestListener;

    iget-object v3, p0, Lcom/tencent/tauth/http/AsyncHttpPostRunner$1;->val$state:Ljava/lang/Object;

    invoke-interface {v2, v0, v3}, Lcom/tencent/tauth/http/IRequestListener;->onIOException(Ljava/io/IOException;Ljava/lang/Object;)V

    goto :goto_0
.end method
