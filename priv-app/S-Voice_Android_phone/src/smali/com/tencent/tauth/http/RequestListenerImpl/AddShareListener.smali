.class public Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "AddShareListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AddShareListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 21
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v6, -0x80000000

    .line 25
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 29
    .local v2, "obj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 30
    .local v3, "ret":I
    const-string/jumbo v1, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 32
    .local v1, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v4, "ret"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 33
    const-string/jumbo v4, "msg"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 38
    :goto_0
    if-nez v3, :cond_0

    .line 39
    :try_start_2
    iget-object v4, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const-string/jumbo v5, "share_id"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 53
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :goto_1
    const-string/jumbo v4, "AddShareListener"

    invoke-static {v4, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-void

    .line 41
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :cond_0
    :try_start_3
    iget-object v4, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v4, v3, v1}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 43
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "obj":Lorg/json/JSONObject;
    .end local v3    # "ret":I
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v4, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 45
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 46
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v0

    .line 47
    .local v0, "e":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 48
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 49
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 50
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v4, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v6, v5}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 51
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 34
    .end local v0    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v1    # "msg":Ljava/lang/String;
    .restart local v2    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "ret":I
    :catch_3
    move-exception v4

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 59
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 64
    return-void
.end method
