.class public Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "UploadPicListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UploadPicListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 22
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 11
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v10, -0x80000000

    .line 26
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 30
    .local v8, "obj":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 31
    .local v9, "ret":I
    const-string/jumbo v7, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 33
    .local v7, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v1, "ret"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 34
    const-string/jumbo v1, "msg"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v7

    .line 39
    :goto_0
    if-nez v9, :cond_0

    .line 40
    :try_start_2
    new-instance v0, Lcom/tencent/tauth/bean/Pic;

    const-string/jumbo v1, "albumid"

    invoke-virtual {v8, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    const-string/jumbo v2, "lloc"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    const-string/jumbo v3, "sloc"

    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    const-string/jumbo v4, "width"

    invoke-virtual {v8, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 44
    const-string/jumbo v5, "height"

    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 40
    invoke-direct/range {v0 .. v5}, Lcom/tencent/tauth/bean/Pic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 45
    .local v0, "pic":Lcom/tencent/tauth/bean/Pic;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v0}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 59
    .end local v0    # "pic":Lcom/tencent/tauth/bean/Pic;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v8    # "obj":Lorg/json/JSONObject;
    .end local v9    # "ret":I
    :goto_1
    const-string/jumbo v1, "UploadPicListener"

    invoke-static {v1, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-void

    .line 47
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v8    # "obj":Lorg/json/JSONObject;
    .restart local v9    # "ret":I
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v1, v9, v7}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 49
    .end local v7    # "msg":Ljava/lang/String;
    .end local v8    # "obj":Lorg/json/JSONObject;
    .end local v9    # "ret":I
    :catch_0
    move-exception v6

    .line 50
    .local v6, "e":Ljava/lang/NumberFormatException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v10, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 51
    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 52
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v6

    .line 53
    .local v6, "e":Lorg/json/JSONException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v10, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 54
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 55
    .end local v6    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v6

    .line 56
    .local v6, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v6}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v10, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 57
    invoke-virtual {v6}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 35
    .end local v6    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v8    # "obj":Lorg/json/JSONObject;
    .restart local v9    # "ret":I
    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 65
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 70
    return-void
.end method
