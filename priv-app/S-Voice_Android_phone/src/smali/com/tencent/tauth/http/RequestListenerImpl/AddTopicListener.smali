.class public Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;
.super Lcom/tencent/tauth/http/BaseRequestListener;
.source "AddTopicListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AddShareListener"


# instance fields
.field private mCallback:Lcom/tencent/tauth/http/Callback;


# direct methods
.method public constructor <init>(Lcom/tencent/tauth/http/Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/tencent/tauth/http/BaseRequestListener;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    .line 22
    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 11
    .param p1, "response"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    const/high16 v10, -0x80000000

    .line 26
    invoke-super {p0, p1, p2}, Lcom/tencent/tauth/http/BaseRequestListener;->onComplete(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    :try_start_0
    invoke-static {p1}, Lcom/tencent/tauth/http/ParseResoneJson;->parseJson(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 30
    .local v4, "obj":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 31
    .local v5, "ret":I
    const-string/jumbo v3, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_0 .. :try_end_0} :catch_2

    .line 33
    .local v3, "msg":Ljava/lang/String;
    :try_start_1
    const-string/jumbo v6, "ret"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 34
    const-string/jumbo v6, "msg"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 35
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v6, v5, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_1 .. :try_end_1} :catch_2

    .line 40
    :goto_0
    if-nez v5, :cond_1

    .line 41
    const/4 v1, 0x0

    .line 42
    .local v1, "info":Lcom/tencent/tauth/bean/TopicRichInfo;
    :try_start_2
    const-string/jumbo v6, "richinfo"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 44
    .local v2, "infoObj":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 45
    new-instance v1, Lcom/tencent/tauth/bean/TopicRichInfo;

    .line 46
    .end local v1    # "info":Lcom/tencent/tauth/bean/TopicRichInfo;
    const-string/jumbo v6, "rtype"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 47
    const-string/jumbo v7, "url2"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 48
    const-string/jumbo v8, "url3"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 49
    const-string/jumbo v9, "who"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 45
    invoke-direct {v1, v6, v7, v8, v9}, Lcom/tencent/tauth/bean/TopicRichInfo;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    .line 52
    .restart local v1    # "info":Lcom/tencent/tauth/bean/TopicRichInfo;
    :cond_0
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v6, v1}, Lcom/tencent/tauth/http/Callback;->onSuccess(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_2 .. :try_end_2} :catch_2

    .line 67
    .end local v1    # "info":Lcom/tencent/tauth/bean/TopicRichInfo;
    .end local v2    # "infoObj":Lorg/json/JSONObject;
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "obj":Lorg/json/JSONObject;
    .end local v5    # "ret":I
    :goto_1
    const-string/jumbo v6, "AddShareListener"

    invoke-static {v6, p1}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void

    .line 54
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "obj":Lorg/json/JSONObject;
    .restart local v5    # "ret":I
    :cond_1
    :try_start_3
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-interface {v6, v5, v3}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/tencent/tauth/http/CommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 57
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "obj":Lorg/json/JSONObject;
    .end local v5    # "ret":I
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v10, v7}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 59
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 60
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v0

    .line 61
    .local v0, "e":Lorg/json/JSONException;
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v10, v7}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 62
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 63
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 64
    .local v0, "e":Lcom/tencent/tauth/http/CommonException;
    iget-object v6, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v10, v7}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 65
    invoke-virtual {v0}, Lcom/tencent/tauth/http/CommonException;->printStackTrace()V

    goto :goto_1

    .line 36
    .end local v0    # "e":Lcom/tencent/tauth/http/CommonException;
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "obj":Lorg/json/JSONObject;
    .restart local v5    # "ret":I
    :catch_3
    move-exception v6

    goto :goto_0
.end method

.method public onFileNotFoundException(Ljava/io/FileNotFoundException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/FileNotFoundException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Resource not found:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 73
    return-void
.end method

.method public onIOException(Ljava/io/IOException;Ljava/lang/Object;)V
    .locals 4
    .param p1, "e"    # Ljava/io/IOException;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;->mCallback:Lcom/tencent/tauth/http/Callback;

    const/high16 v1, -0x80000000

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Network Error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/tencent/tauth/http/Callback;->onFail(ILjava/lang/String;)V

    .line 78
    return-void
.end method
