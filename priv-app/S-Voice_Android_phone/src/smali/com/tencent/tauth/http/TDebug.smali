.class public Lcom/tencent/tauth/http/TDebug;
.super Ljava/lang/Object;
.source "TDebug.java"


# static fields
.field private static bDebugMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    sput-boolean v0, Lcom/tencent/tauth/http/TDebug;->bDebugMode:Z

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 23
    sget-boolean v0, Lcom/tencent/tauth/http/TDebug;->bDebugMode:Z

    if-eqz v0, :cond_0

    .line 24
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    :cond_0
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 17
    sget-boolean v0, Lcom/tencent/tauth/http/TDebug;->bDebugMode:Z

    if-eqz v0, :cond_0

    .line 18
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    :cond_0
    return-void
.end method

.method public static msg(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "con"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-static {p1, p0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 30
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 31
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 32
    return-void
.end method

.method public static setMode(Z)V
    .locals 0
    .param p0, "mode"    # Z

    .prologue
    .line 13
    sput-boolean p0, Lcom/tencent/tauth/http/TDebug;->bDebugMode:Z

    .line 14
    return-void
.end method
