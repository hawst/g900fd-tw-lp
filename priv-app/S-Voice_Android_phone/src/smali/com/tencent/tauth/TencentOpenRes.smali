.class public Lcom/tencent/tauth/TencentOpenRes;
.super Landroid/view/View;
.source "TencentOpenRes.java"


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public static getBigLoginBtn(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 54
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 56
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_big_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_big_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 57
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_big_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_big_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-object v0

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static getCloseBtnSrc(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 19
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 21
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "close_btn_src_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "close_btn_src_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 22
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "close_btn_src_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "close_btn_src_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :goto_0
    return-object v0

    .line 23
    :catch_0
    move-exception v1

    .line 24
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static getCloseBtnSrcBg(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 30
    const/4 v2, 0x0

    .line 31
    .local v2, "is":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 33
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :try_start_0
    const-string/jumbo v3, "close_btn_bg.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 34
    const-string/jumbo v3, "close_btn_bg"

    invoke-static {v2, v3}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 38
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getLoginBtn(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 65
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 67
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 68
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-object v0

    .line 69
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getLogoutBtn(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 87
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 89
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "logout_btn_src_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "logout_btn_src_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 90
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "logout_btn_src_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "logout_btn_src_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-object v0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSmallLoginBtn(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 76
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 78
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_small_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_small_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 79
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "login_btn_src_small_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "login_btn_src_small_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-object v0

    .line 80
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getSmallLogoutBtn(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 98
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 100
    .local v0, "drawable":Landroid/graphics/drawable/StateListDrawable;
    :try_start_0
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->PRESSED_ENABLED_STATE_SET:[I

    const-string/jumbo v3, "logout_btn_src_small_pressed.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "logout_btn_src_small_pressed"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 101
    sget-object v2, Lcom/tencent/tauth/TencentOpenRes;->EMPTY_STATE_SET:[I

    const-string/jumbo v3, "logout_btn_src_small_normal.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    const-string/jumbo v4, "logout_btn_src_small_normal"

    invoke-static {v3, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static getTitleBg(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "am"    # Landroid/content/res/AssetManager;

    .prologue
    .line 42
    const/4 v2, 0x0

    .line 43
    .local v2, "is":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 45
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :try_start_0
    const-string/jumbo v3, "title_bg.png"

    invoke-virtual {p0, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 46
    const-string/jumbo v3, "title_bg"

    invoke-static {v2, v3}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    .line 47
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
