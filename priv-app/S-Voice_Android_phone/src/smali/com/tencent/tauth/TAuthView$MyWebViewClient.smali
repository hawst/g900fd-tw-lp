.class Lcom/tencent/tauth/TAuthView$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "TAuthView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/TAuthView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/TAuthView;


# direct methods
.method private constructor <init>(Lcom/tencent/tauth/TAuthView;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/tencent/tauth/TAuthView;Lcom/tencent/tauth/TAuthView$MyWebViewClient;)V
    .locals 0

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/tencent/tauth/TAuthView$MyWebViewClient;-><init>(Lcom/tencent/tauth/TAuthView;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 515
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 510
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 511
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 518
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/tencent/tauth/TAuthView;->access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 519
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 520
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "handler"    # Landroid/webkit/SslErrorHandler;
    .param p3, "error"    # Landroid/net/http/SslError;

    .prologue
    .line 524
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 525
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 497
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->mCallback:Ljava/lang/String;
    invoke-static {v3}, Lcom/tencent/tauth/TAuthView;->access$2(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 499
    iget-object v2, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # invokes: Lcom/tencent/tauth/TAuthView;->parseResult(Ljava/lang/String;)V
    invoke-static {v2, p2}, Lcom/tencent/tauth/TAuthView;->access$3(Lcom/tencent/tauth/TAuthView;Ljava/lang/String;)V

    .line 500
    iget-object v2, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/tencent/tauth/TAuthView;->access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 501
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # invokes: Lcom/tencent/tauth/TAuthView;->returnResult()V
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$5(Lcom/tencent/tauth/TAuthView;)V

    .line 506
    :goto_0
    return v0

    .line 504
    :cond_0
    iget-object v2, p0, Lcom/tencent/tauth/TAuthView$MyWebViewClient;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/tencent/tauth/TAuthView;->access$4(Lcom/tencent/tauth/TAuthView;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    move v0, v1

    .line 506
    goto :goto_0
.end method
