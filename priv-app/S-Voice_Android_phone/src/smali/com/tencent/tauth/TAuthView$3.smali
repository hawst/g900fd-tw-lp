.class Lcom/tencent/tauth/TAuthView$3;
.super Ljava/lang/Object;
.source "TAuthView.java"

# interfaces
.implements Lcom/tencent/tauth/http/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/tencent/tauth/TAuthView;->returnResult()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/TAuthView;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/TAuthView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/tencent/tauth/TAuthView$3;)Lcom/tencent/tauth/TAuthView;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    return-object v0
.end method


# virtual methods
.method public onCancel(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 390
    return-void
.end method

.method public onFail(ILjava/lang/String;)V
    .locals 2
    .param p1, "ret"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 451
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    new-instance v1, Lcom/tencent/tauth/TAuthView$3$2;

    invoke-direct {v1, p0, p2}, Lcom/tencent/tauth/TAuthView$3$2;-><init>(Lcom/tencent/tauth/TAuthView$3;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/tencent/tauth/TAuthView;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 457
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    check-cast p1, Lcom/tencent/tauth/bean/OpenId;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/tencent/tauth/bean/OpenId;->getOpenId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/tencent/tauth/TAuthView;->access$6(Lcom/tencent/tauth/TAuthView;Ljava/lang/String;)V

    .line 396
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 398
    .local v5, "send_store_parameters":Landroid/os/Bundle;
    const-string/jumbo v0, "title"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_title:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$7(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_share_url:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$8(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string/jumbo v0, "comment"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_comment:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$9(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string/jumbo v0, "summary"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_summary:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$10(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string/jumbo v0, "images"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_images:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$11(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string/jumbo v0, "type"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_type:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$12(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const-string/jumbo v0, "playurl"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_playurl:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$13(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string/jumbo v0, "source"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_source:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$14(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    const-string/jumbo v0, "site"

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_site:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$15(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-virtual {v0}, Lcom/tencent/tauth/TAuthView;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->mAccessToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$16(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_client_id:Ljava/lang/String;
    invoke-static {v2}, Lcom/tencent/tauth/TAuthView;->access$17(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->openidString:Ljava/lang/String;
    invoke-static {v3}, Lcom/tencent/tauth/TAuthView;->access$18(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v3

    .line 409
    iget-object v4, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->cache_target:Ljava/lang/String;
    invoke-static {v4}, Lcom/tencent/tauth/TAuthView;->access$19(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lcom/tencent/tauth/TAuthView$3$1;

    invoke-direct {v6, p0}, Lcom/tencent/tauth/TAuthView$3$1;-><init>(Lcom/tencent/tauth/TAuthView$3;)V

    .line 444
    iget-object v7, p0, Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->from_cmd:Ljava/lang/String;
    invoke-static {v7}, Lcom/tencent/tauth/TAuthView;->access$20(Lcom/tencent/tauth/TAuthView;)Ljava/lang/String;

    move-result-object v7

    .line 408
    invoke-static/range {v0 .. v7}, Lcom/tencent/tauth/TencentOpenAPI2;->sendStore(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;Ljava/lang/String;)V

    .line 447
    return-void
.end method
