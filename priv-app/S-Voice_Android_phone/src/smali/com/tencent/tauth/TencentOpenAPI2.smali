.class public Lcom/tencent/tauth/TencentOpenAPI2;
.super Ljava/lang/Object;
.source "TencentOpenAPI2.java"


# static fields
.field private static final IfCallQzone:Z = false

.field private static final TAG:Ljava/lang/String; = "TencentOpenAPI2"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logIn(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "openid"    # Ljava/lang/String;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;
    .param p5, "callback"    # Ljava/lang/String;
    .param p6, "parameters"    # Landroid/os/Bundle;
    .param p7, "from_cmd"    # Ljava/lang/String;

    .prologue
    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v12, "ACTION"

    const/16 v13, 0x2329

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    const-string/jumbo v12, "APPID"

    move-object/from16 v0, p3

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string/jumbo v12, "APPPKGNAME"

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string/jumbo v12, "openid"

    invoke-virtual {v1, v12, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string/jumbo v12, "from_cmd"

    move-object/from16 v0, p7

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string/jumbo v12, "scope"

    move-object/from16 v0, p2

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string/jumbo v12, "client_id"

    move-object/from16 v0, p3

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string/jumbo v12, "target"

    move-object/from16 v0, p4

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string/jumbo v12, "callback"

    move-object/from16 v0, p5

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    if-eqz p6, :cond_0

    const-string/jumbo v12, "send_store"

    move-object/from16 v0, p7

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 41
    const-string/jumbo v12, "title"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 42
    .local v9, "title":Ljava/lang/String;
    const-string/jumbo v12, "url"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 43
    .local v11, "url":Ljava/lang/String;
    const-string/jumbo v12, "comment"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 44
    .local v2, "comment":Ljava/lang/String;
    const-string/jumbo v12, "summary"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 45
    .local v8, "summary":Ljava/lang/String;
    const-string/jumbo v12, "images"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    .local v3, "images":Ljava/lang/String;
    const-string/jumbo v12, "type"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 47
    .local v10, "type":Ljava/lang/String;
    const-string/jumbo v12, "playurl"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 48
    .local v5, "playurl":Ljava/lang/String;
    const-string/jumbo v12, "source"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 49
    .local v7, "source":Ljava/lang/String;
    const-string/jumbo v12, "site"

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 50
    .local v6, "site":Ljava/lang/String;
    const-string/jumbo v12, "title"

    invoke-virtual {v1, v12, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string/jumbo v12, "url"

    invoke-virtual {v1, v12, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string/jumbo v12, "comment"

    invoke-virtual {v1, v12, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string/jumbo v12, "summary"

    invoke-virtual {v1, v12, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string/jumbo v12, "images"

    invoke-virtual {v1, v12, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string/jumbo v12, "type"

    invoke-virtual {v1, v12, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string/jumbo v12, "playurl"

    invoke-virtual {v1, v12, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v12, "source"

    invoke-virtual {v1, v12, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string/jumbo v12, "site"

    invoke-virtual {v1, v12, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .end local v2    # "comment":Ljava/lang/String;
    .end local v3    # "images":Ljava/lang/String;
    .end local v5    # "playurl":Ljava/lang/String;
    .end local v6    # "site":Ljava/lang/String;
    .end local v7    # "source":Ljava/lang/String;
    .end local v8    # "summary":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    .end local v10    # "type":Ljava/lang/String;
    .end local v11    # "url":Ljava/lang/String;
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v12, Lcom/tencent/tauth/TAuthView;

    invoke-direct {v4, p0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v12, 0x10000000

    invoke-virtual {v4, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 109
    return-void
.end method

.method public static sendStore(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;Ljava/lang/String;)V
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "access_token"    # Ljava/lang/String;
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "openid"    # Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;
    .param p5, "parameters"    # Landroid/os/Bundle;
    .param p6, "callback"    # Lcom/tencent/tauth/http/Callback;
    .param p7, "fromcmd"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 127
    .local v9, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "ACTION"

    const/16 v2, 0x232a

    invoke-virtual {v9, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    const-string/jumbo v1, "APPID"

    move-object/from16 v0, p2

    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string/jumbo v1, "APPPKGNAME"

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string/jumbo v1, "OPENID"

    move-object/from16 v0, p3

    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    if-eqz p7, :cond_2

    if-eqz p1, :cond_0

    if-nez p3, :cond_2

    .line 176
    :cond_0
    const-string/jumbo v1, "\u518d\u6b21\u767b\u9646\u5931\u8d25\uff0c\u6b64\u6b21\u5206\u4eab\u53d6\u6d88\uff01"

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 177
    .local v18, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 178
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    .line 221
    .end local v18    # "toast":Landroid/widget/Toast;
    :cond_1
    :goto_0
    return-void

    .line 182
    :cond_2
    if-eqz p1, :cond_3

    if-nez p3, :cond_4

    :cond_3
    if-nez p7, :cond_4

    .line 184
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v7, "send_store_params":Landroid/os/Bundle;
    const-string/jumbo v1, "title"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 187
    .local v17, "title":Ljava/lang/String;
    const-string/jumbo v1, "url"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 188
    .local v20, "url":Ljava/lang/String;
    const-string/jumbo v1, "comment"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 189
    .local v10, "comment":Ljava/lang/String;
    const-string/jumbo v1, "summary"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 190
    .local v16, "summary":Ljava/lang/String;
    const-string/jumbo v1, "images"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 191
    .local v11, "images":Ljava/lang/String;
    const-string/jumbo v1, "type"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 192
    .local v19, "type":Ljava/lang/String;
    const-string/jumbo v1, "playurl"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 193
    .local v13, "playurl":Ljava/lang/String;
    const-string/jumbo v1, "source"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 194
    .local v15, "source":Ljava/lang/String;
    const-string/jumbo v1, "site"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 195
    .local v14, "site":Ljava/lang/String;
    const-string/jumbo v1, "title"

    move-object/from16 v0, v17

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string/jumbo v1, "url"

    move-object/from16 v0, v20

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string/jumbo v1, "comment"

    invoke-virtual {v7, v1, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string/jumbo v1, "summary"

    move-object/from16 v0, v16

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string/jumbo v1, "images"

    invoke-virtual {v7, v1, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string/jumbo v1, "type"

    move-object/from16 v0, v19

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string/jumbo v1, "playurl"

    invoke-virtual {v7, v1, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string/jumbo v1, "source"

    invoke-virtual {v7, v1, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string/jumbo v1, "site"

    invoke-virtual {v7, v1, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string/jumbo v3, "add_share"

    const-string/jumbo v5, "_self"

    const-string/jumbo v6, "auth://tauth.qq.com/"

    const-string/jumbo v8, "send_store"

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v8}, Lcom/tencent/tauth/TencentOpenAPI2;->logIn(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 207
    .end local v7    # "send_store_params":Landroid/os/Bundle;
    .end local v10    # "comment":Ljava/lang/String;
    .end local v11    # "images":Ljava/lang/String;
    .end local v13    # "playurl":Ljava/lang/String;
    .end local v14    # "site":Ljava/lang/String;
    .end local v15    # "source":Ljava/lang/String;
    .end local v16    # "summary":Ljava/lang/String;
    .end local v17    # "title":Ljava/lang/String;
    .end local v19    # "type":Ljava/lang/String;
    .end local v20    # "url":Ljava/lang/String;
    :cond_4
    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    .line 210
    new-instance v12, Landroid/content/Intent;

    const-class v1, Lcom/tencent/tauth/AddShareView;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    .local v12, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "client_id"

    move-object/from16 v0, p2

    invoke-virtual {v12, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const-string/jumbo v1, "access_token"

    move-object/from16 v0, p1

    invoke-virtual {v12, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string/jumbo v1, "openid"

    move-object/from16 v0, p3

    invoke-virtual {v12, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string/jumbo v1, "target"

    move-object/from16 v0, p4

    invoke-virtual {v12, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    move-object/from16 v0, p5

    invoke-virtual {v12, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 216
    const/high16 v1, 0x10000000

    invoke-virtual {v12, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 217
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
