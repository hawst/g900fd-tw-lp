.class Lcom/tencent/tauth/TAuthView$1;
.super Landroid/os/Handler;
.source "TAuthView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/TAuthView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/TAuthView;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/TAuthView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    .line 103
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 107
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 116
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/tencent/tauth/TAuthView;->access$1(Lcom/tencent/tauth/TAuthView;Landroid/app/ProgressDialog;)V

    .line 118
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    const-string/jumbo v2, "\u8bf7\u6c42\u4e2d,\u8bf7\u7a0d\u5019..."

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    invoke-virtual {v1}, Lcom/tencent/tauth/TAuthView;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    :try_start_0
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_1
    :goto_0
    return-void

    .line 109
    :pswitch_0
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    # getter for: Lcom/tencent/tauth/TAuthView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView;->access$0(Lcom/tencent/tauth/TAuthView;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 111
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$1;->this$0:Lcom/tencent/tauth/TAuthView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/tencent/tauth/TAuthView;->access$1(Lcom/tencent/tauth/TAuthView;Landroid/app/ProgressDialog;)V

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "tauthdemo"

    const-string/jumbo v2, "activity is finished."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
