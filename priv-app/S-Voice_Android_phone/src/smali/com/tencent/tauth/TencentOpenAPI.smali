.class public Lcom/tencent/tauth/TencentOpenAPI;
.super Ljava/lang/Object;
.source "TencentOpenAPI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TencentOpenAPI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addAlbum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;)V
    .locals 2
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "parameters"    # Landroid/os/Bundle;
    .param p4, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 148
    const-string/jumbo v0, "format"

    const-string/jumbo v1, "json"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string/jumbo v0, "access_token"

    invoke-virtual {p3, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string/jumbo v0, "oauth_consumer_key"

    invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string/jumbo v0, "openid"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v0, "https://graph.qq.com/photo/add_album"

    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;

    invoke-direct {v1, p4}, Lcom/tencent/tauth/http/RequestListenerImpl/AddAlbumListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, p3, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncPost(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 154
    return-void
.end method

.method public static addShare(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;)V
    .locals 2
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "parameters"    # Landroid/os/Bundle;
    .param p4, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 78
    const-string/jumbo v0, "format"

    const-string/jumbo v1, "json"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string/jumbo v0, "source"

    const-string/jumbo v1, "2"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string/jumbo v0, "access_token"

    invoke-virtual {p3, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string/jumbo v0, "oauth_consumer_key"

    invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string/jumbo v0, "openid"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string/jumbo v0, "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory.html?access_token=%s&oauth_consumer_key=%s&openid=%s&"

    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;

    invoke-direct {v1, p4}, Lcom/tencent/tauth/http/RequestListenerImpl/AddShareListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, p3, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncPost(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 85
    return-void
.end method

.method public static addTopic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;)V
    .locals 2
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "parameters"    # Landroid/os/Bundle;
    .param p4, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 110
    const-string/jumbo v0, "format"

    const-string/jumbo v1, "json"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string/jumbo v0, "access_token"

    invoke-virtual {p3, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string/jumbo v0, "oauth_consumer_key"

    invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string/jumbo v0, "openid"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string/jumbo v0, "https://graph.qq.com/shuoshuo/add_topic"

    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;

    invoke-direct {v1, p4}, Lcom/tencent/tauth/http/RequestListenerImpl/AddTopicListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, p3, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncPost(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 117
    return-void
.end method

.method private static asyncPost(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "parameters"    # Landroid/os/Bundle;
    .param p2, "listener"    # Lcom/tencent/tauth/http/IRequestListener;

    .prologue
    .line 183
    const-string/jumbo v0, "TencentOpenAPI"

    invoke-static {v0, p0}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/tencent/tauth/http/AsyncHttpPostRunner;

    invoke-direct {v0}, Lcom/tencent/tauth/http/AsyncHttpPostRunner;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Lcom/tencent/tauth/http/AsyncHttpPostRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 185
    return-void
.end method

.method private static asyncRequest(Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;)V
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "listener"    # Lcom/tencent/tauth/http/IRequestListener;

    .prologue
    .line 178
    const-string/jumbo v0, "TencentOpenAPI"

    invoke-static {v0, p0}, Lcom/tencent/tauth/http/TDebug;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    new-instance v0, Lcom/tencent/tauth/http/AsyncHttpRequestRunner;

    invoke-direct {v0}, Lcom/tencent/tauth/http/AsyncHttpRequestRunner;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1, p1}, Lcom/tencent/tauth/http/AsyncHttpRequestRunner;->request(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 180
    return-void
.end method

.method public static listAlbum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/tencent/tauth/http/Callback;)V
    .locals 4
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 131
    const-string/jumbo v1, "https://graph.qq.com/photo/list_album?access_token=%s&oauth_consumer_key=%s&openid=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "url":Ljava/lang/String;
    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;

    invoke-direct {v1, p3}, Lcom/tencent/tauth/http/RequestListenerImpl/ListAlbumListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncRequest(Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 133
    return-void
.end method

.method public static openid(Ljava/lang/String;Lcom/tencent/tauth/http/Callback;)V
    .locals 4
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 37
    const-string/jumbo v1, "https://graph.qq.com/oauth2.0/me?access_token=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "url":Ljava/lang/String;
    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;

    invoke-direct {v1, p1}, Lcom/tencent/tauth/http/RequestListenerImpl/OpenIDListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncRequest(Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 39
    return-void
.end method

.method public static uploadPic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/Callback;)V
    .locals 2
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "parameters"    # Landroid/os/Bundle;
    .param p4, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 169
    const-string/jumbo v0, "format"

    const-string/jumbo v1, "json"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string/jumbo v0, "access_token"

    invoke-virtual {p3, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string/jumbo v0, "oauth_consumer_key"

    invoke-virtual {p3, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string/jumbo v0, "openid"

    invoke-virtual {p3, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string/jumbo v0, "https://graph.qq.com/photo/upload_pic"

    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;

    invoke-direct {v1, p4}, Lcom/tencent/tauth/http/RequestListenerImpl/UploadPicListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, p3, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncPost(Ljava/lang/String;Landroid/os/Bundle;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 175
    return-void
.end method

.method public static userInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/tencent/tauth/http/Callback;)V
    .locals 4
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 49
    const-string/jumbo v1, "https://graph.qq.com/user/get_user_info?access_token=%s&oauth_consumer_key=%s&openid=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "url":Ljava/lang/String;
    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;

    invoke-direct {v1, p3}, Lcom/tencent/tauth/http/RequestListenerImpl/UserInfoListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncRequest(Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 51
    return-void
.end method

.method public static userProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/tencent/tauth/http/Callback;)V
    .locals 4
    .param p0, "access_token"    # Ljava/lang/String;
    .param p1, "appid"    # Ljava/lang/String;
    .param p2, "openid"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/tencent/tauth/http/Callback;

    .prologue
    .line 63
    const-string/jumbo v1, "https://graph.qq.com/user/get_user_profile?access_token=%s&oauth_consumer_key=%s&openid=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "url":Ljava/lang/String;
    new-instance v1, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;

    invoke-direct {v1, p3}, Lcom/tencent/tauth/http/RequestListenerImpl/UserProfileListener;-><init>(Lcom/tencent/tauth/http/Callback;)V

    invoke-static {v0, v1}, Lcom/tencent/tauth/TencentOpenAPI;->asyncRequest(Ljava/lang/String;Lcom/tencent/tauth/http/IRequestListener;)V

    .line 65
    return-void
.end method
