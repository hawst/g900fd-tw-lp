.class public Lcom/tencent/tauth/AddShareView;
.super Landroid/app/Activity;
.source "AddShareView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/tencent/tauth/AddShareView$MyWebChromeClient;,
        Lcom/tencent/tauth/AddShareView$MyWebViewClient;
    }
.end annotation


# instance fields
.field private dialog:Landroid/app/ProgressDialog;

.field private handler:Landroid/os/Handler;

.field private js_interface:Lcom/tencent/tauth/JsInterface;

.field private mWebView:Landroid/webkit/WebView;

.field private titleTxt:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    new-instance v0, Lcom/tencent/tauth/JsInterface;

    new-instance v1, Lcom/tencent/tauth/AddShareView$1;

    invoke-direct {v1, p0}, Lcom/tencent/tauth/AddShareView$1;-><init>(Lcom/tencent/tauth/AddShareView;)V

    invoke-direct {v0, v1}, Lcom/tencent/tauth/JsInterface;-><init>(Lcom/tencent/tauth/http/Callback;)V

    iput-object v0, p0, Lcom/tencent/tauth/AddShareView;->js_interface:Lcom/tencent/tauth/JsInterface;

    .line 122
    new-instance v0, Lcom/tencent/tauth/AddShareView$2;

    invoke-direct {v0, p0}, Lcom/tencent/tauth/AddShareView$2;-><init>(Lcom/tencent/tauth/AddShareView;)V

    iput-object v0, p0, Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/tencent/tauth/AddShareView;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$2(Lcom/tencent/tauth/AddShareView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private getIp()Ljava/lang/String;
    .locals 7

    .prologue
    .line 424
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_1

    .line 436
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :goto_0
    const-string/jumbo v5, ""

    :goto_1
    return-object v5

    .line 425
    .restart local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 426
    .local v4, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 427
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 428
    .local v3, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    if-nez v5, :cond_2

    .line 429
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_1

    .line 433
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v1    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v4    # "intf":Ljava/net/NetworkInterface;
    :catch_0
    move-exception v2

    .line 434
    .local v2, "ex":Ljava/net/SocketException;
    const-string/jumbo v5, "TAG"

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getMachine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private getOS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method private getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    return-object v0
.end method

.method private setWinTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->titleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 33
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 166
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/AddShareView;->getIntent()Landroid/content/Intent;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 169
    .local v6, "bundle":Landroid/os/Bundle;
    if-eqz v6, :cond_a

    .line 170
    const-string/jumbo v29, "client_id"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 171
    .local v7, "client_id":Ljava/lang/String;
    const-string/jumbo v29, "scope"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 172
    .local v16, "scope":Ljava/lang/String;
    const-string/jumbo v29, "access_token"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 173
    .local v5, "access_token":Ljava/lang/String;
    const-string/jumbo v29, "target"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 174
    .local v21, "target":Ljava/lang/String;
    const-string/jumbo v29, "openid"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 176
    .local v12, "openid":Ljava/lang/String;
    const-string/jumbo v29, "title"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 177
    .local v22, "title":Ljava/lang/String;
    const-string/jumbo v29, "url"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 178
    .local v27, "url":Ljava/lang/String;
    const-string/jumbo v29, "comment"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, "comment":Ljava/lang/String;
    const-string/jumbo v29, "summary"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 180
    .local v20, "summary":Ljava/lang/String;
    const-string/jumbo v29, "images"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 181
    .local v10, "images":Ljava/lang/String;
    const-string/jumbo v29, "type"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 182
    .local v25, "type":Ljava/lang/String;
    const-string/jumbo v29, "playurl"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 183
    .local v13, "playurl":Ljava/lang/String;
    const-string/jumbo v29, "source"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 184
    .local v19, "source":Ljava/lang/String;
    const-string/jumbo v29, "site"

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 193
    .local v18, "site":Ljava/lang/String;
    const-string/jumbo v29, "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory.html?access_token=%s&oauth_consumer_key=%s&openid=%s&"

    const/16 v30, 0x3

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aput-object v5, v30, v31

    const/16 v31, 0x1

    aput-object v7, v30, v31

    const/16 v31, 0x2

    aput-object v12, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    .line 195
    .local v28, "whole_url":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .local v15, "sb":Ljava/lang/StringBuilder;
    if-eqz v22, :cond_0

    .line 198
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "title"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v22 .. v22}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_0
    if-eqz v27, :cond_1

    .line 203
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "url"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v27 .. v27}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_1
    if-eqz v8, :cond_2

    .line 208
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "comment"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static {v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    :cond_2
    if-eqz v20, :cond_3

    .line 213
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "summary"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v20 .. v20}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_3
    if-eqz v10, :cond_4

    .line 218
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "images"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static {v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_4
    if-eqz v25, :cond_5

    .line 223
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "type"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v25 .. v25}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_5
    if-eqz v13, :cond_6

    .line 228
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "playurl"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static {v13}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_6
    if-eqz v19, :cond_7

    .line 233
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "source"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v19 .. v19}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    const-string/jumbo v29, "&"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_7
    if-eqz v18, :cond_8

    .line 239
    new-instance v29, Ljava/lang/StringBuilder;

    const-string/jumbo v30, "site"

    invoke-static/range {v30 .. v30}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v30, "="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-static/range {v18 .. v18}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_8
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 245
    const-string/jumbo v29, "_blank"

    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 247
    invoke-static/range {v28 .. v28}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v26

    .line 248
    .local v26, "uri":Landroid/net/Uri;
    new-instance v11, Landroid/content/Intent;

    const-string/jumbo v29, "android.intent.action.VIEW"

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 249
    .local v11, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/tencent/tauth/AddShareView;->startActivity(Landroid/content/Intent;)V

    .line 250
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/AddShareView;->finish()V

    .line 378
    .end local v5    # "access_token":Ljava/lang/String;
    .end local v7    # "client_id":Ljava/lang/String;
    .end local v8    # "comment":Ljava/lang/String;
    .end local v10    # "images":Ljava/lang/String;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v12    # "openid":Ljava/lang/String;
    .end local v13    # "playurl":Ljava/lang/String;
    .end local v15    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "scope":Ljava/lang/String;
    .end local v18    # "site":Ljava/lang/String;
    .end local v19    # "source":Ljava/lang/String;
    .end local v20    # "summary":Ljava/lang/String;
    .end local v21    # "target":Ljava/lang/String;
    .end local v22    # "title":Ljava/lang/String;
    .end local v25    # "type":Ljava/lang/String;
    .end local v26    # "uri":Landroid/net/Uri;
    .end local v27    # "url":Ljava/lang/String;
    .end local v28    # "whole_url":Ljava/lang/String;
    :goto_0
    return-void

    .line 253
    .restart local v5    # "access_token":Ljava/lang/String;
    .restart local v7    # "client_id":Ljava/lang/String;
    .restart local v8    # "comment":Ljava/lang/String;
    .restart local v10    # "images":Ljava/lang/String;
    .restart local v12    # "openid":Ljava/lang/String;
    .restart local v13    # "playurl":Ljava/lang/String;
    .restart local v15    # "sb":Ljava/lang/StringBuilder;
    .restart local v16    # "scope":Ljava/lang/String;
    .restart local v18    # "site":Ljava/lang/String;
    .restart local v19    # "source":Ljava/lang/String;
    .restart local v20    # "summary":Ljava/lang/String;
    .restart local v21    # "target":Ljava/lang/String;
    .restart local v22    # "title":Ljava/lang/String;
    .restart local v25    # "type":Ljava/lang/String;
    .restart local v27    # "url":Ljava/lang/String;
    .restart local v28    # "whole_url":Ljava/lang/String;
    :cond_9
    const/16 v29, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/tencent/tauth/AddShareView;->requestWindowFeature(I)Z

    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/AddShareView;->getWindow()Landroid/view/Window;

    move-result-object v29

    const/16 v30, 0x400

    .line 255
    const/16 v31, 0x400

    .line 254
    invoke-virtual/range {v29 .. v31}, Landroid/view/Window;->setFlags(II)V

    .line 257
    new-instance v14, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 258
    .local v14, "root":Landroid/widget/LinearLayout;
    const/16 v29, 0x1

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 259
    new-instance v29, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v30, -0x1

    .line 260
    const/16 v31, -0x1

    invoke-direct/range {v29 .. v31}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 259
    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    new-instance v23, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 263
    .local v23, "titleBar":Landroid/widget/RelativeLayout;
    new-instance v24, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v29, -0x1

    .line 264
    const/16 v30, -0x2

    .line 263
    move-object/from16 v0, v24

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 265
    .local v24, "titleBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v29

    move/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 299
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/AddShareView;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/tencent/tauth/TencentOpenRes;->getTitleBg(Landroid/content/res/AssetManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v29

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 301
    new-instance v29, Landroid/view/ViewGroup$LayoutParams;

    const/16 v30, -0x1

    const/16 v31, -0x2

    invoke-direct/range {v29 .. v31}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v14, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 303
    new-instance v29, Landroid/webkit/WebView;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    new-instance v30, Landroid/widget/LinearLayout$LayoutParams;

    .line 305
    const/16 v31, -0x1

    .line 306
    const/16 v32, -0x1

    invoke-direct/range {v30 .. v32}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 304
    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    const/16 v30, 0x64

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    new-instance v30, Lcom/tencent/tauth/AddShareView$MyWebViewClient;

    const/16 v31, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/tencent/tauth/AddShareView$MyWebViewClient;-><init>(Lcom/tencent/tauth/AddShareView;Lcom/tencent/tauth/AddShareView$MyWebViewClient;)V

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/webkit/WebView;->clearFormData()V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v29

    sget-object v30, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v17

    .line 320
    .local v17, "setting":Landroid/webkit/WebSettings;
    const/16 v29, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 321
    const/16 v29, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 322
    const/16 v29, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 323
    const/16 v29, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 324
    const/16 v29, 0x2

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 325
    const/16 v29, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    .line 326
    const/16 v29, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 327
    const/16 v29, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->js_interface:Lcom/tencent/tauth/JsInterface;

    move-object/from16 v30, v0

    const-string/jumbo v31, "sdk_js_if"

    invoke-virtual/range {v29 .. v31}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    new-instance v30, Lcom/tencent/tauth/AddShareView$MyWebChromeClient;

    const/16 v31, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/tencent/tauth/AddShareView$MyWebChromeClient;-><init>(Lcom/tencent/tauth/AddShareView;Lcom/tencent/tauth/AddShareView$MyWebChromeClient;)V

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 353
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/tencent/tauth/AddShareView;->setContentView(Landroid/view/View;)V

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    invoke-virtual/range {v29 .. v30}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/tencent/tauth/AddShareView;->mWebView:Landroid/webkit/WebView;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/webkit/WebView;->requestFocus()Z

    goto/16 :goto_0

    .line 369
    .end local v5    # "access_token":Ljava/lang/String;
    .end local v7    # "client_id":Ljava/lang/String;
    .end local v8    # "comment":Ljava/lang/String;
    .end local v10    # "images":Ljava/lang/String;
    .end local v12    # "openid":Ljava/lang/String;
    .end local v13    # "playurl":Ljava/lang/String;
    .end local v14    # "root":Landroid/widget/LinearLayout;
    .end local v15    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "scope":Ljava/lang/String;
    .end local v17    # "setting":Landroid/webkit/WebSettings;
    .end local v18    # "site":Ljava/lang/String;
    .end local v19    # "source":Ljava/lang/String;
    .end local v20    # "summary":Ljava/lang/String;
    .end local v21    # "target":Ljava/lang/String;
    .end local v22    # "title":Ljava/lang/String;
    .end local v23    # "titleBar":Landroid/widget/RelativeLayout;
    .end local v24    # "titleBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v25    # "type":Ljava/lang/String;
    .end local v27    # "url":Ljava/lang/String;
    .end local v28    # "whole_url":Ljava/lang/String;
    :cond_a
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/tencent/tauth/AddShareView;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 370
    :catch_0
    move-exception v9

    .line 372
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 455
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 456
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 460
    :cond_0
    return-void
.end method
