.class Lcom/tencent/tauth/TAuthView$3$1$2;
.super Ljava/lang/Object;
.source "TAuthView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/tencent/tauth/TAuthView$3$1;->onFail(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/tencent/tauth/TAuthView$3$1;

.field private final synthetic val$msg:Ljava/lang/String;

.field private final synthetic val$ret:I


# direct methods
.method constructor <init>(Lcom/tencent/tauth/TAuthView$3$1;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->this$2:Lcom/tencent/tauth/TAuthView$3$1;

    iput p2, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->val$ret:I

    iput-object p3, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->val$msg:Ljava/lang/String;

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 431
    iget-object v1, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->this$2:Lcom/tencent/tauth/TAuthView$3$1;

    # getter for: Lcom/tencent/tauth/TAuthView$3$1;->this$1:Lcom/tencent/tauth/TAuthView$3;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView$3$1;->access$0(Lcom/tencent/tauth/TAuthView$3$1;)Lcom/tencent/tauth/TAuthView$3;

    move-result-object v1

    # getter for: Lcom/tencent/tauth/TAuthView$3;->this$0:Lcom/tencent/tauth/TAuthView;
    invoke-static {v1}, Lcom/tencent/tauth/TAuthView$3;->access$0(Lcom/tencent/tauth/TAuthView$3;)Lcom/tencent/tauth/TAuthView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/tencent/tauth/TAuthView;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "\u5206\u4eab\u5931\u8d25\uff0c\u9519\u8bef\u4fe1\u606f:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->val$ret:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 432
    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/tencent/tauth/TAuthView$3$1$2;->val$msg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 431
    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 433
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 434
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 435
    return-void
.end method
