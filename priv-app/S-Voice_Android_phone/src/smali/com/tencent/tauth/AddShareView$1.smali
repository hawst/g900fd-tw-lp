.class Lcom/tencent/tauth/AddShareView$1;
.super Ljava/lang/Object;
.source "AddShareView.java"

# interfaces
.implements Lcom/tencent/tauth/http/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/tencent/tauth/AddShareView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/tencent/tauth/AddShareView;


# direct methods
.method constructor <init>(Lcom/tencent/tauth/AddShareView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/tencent/tauth/AddShareView$1;)Lcom/tencent/tauth/AddShareView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    return-object v0
.end method


# virtual methods
.method public onCancel(I)V
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-virtual {v0}, Lcom/tencent/tauth/AddShareView;->finish()V

    .line 69
    return-void
.end method

.method public onFail(ILjava/lang/String;)V
    .locals 2
    .param p1, "ret"    # I
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    new-instance v1, Lcom/tencent/tauth/AddShareView$1$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/tencent/tauth/AddShareView$1$2;-><init>(Lcom/tencent/tauth/AddShareView$1;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/tencent/tauth/AddShareView;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 112
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-virtual {v0}, Lcom/tencent/tauth/AddShareView;->finish()V

    .line 119
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    new-instance v1, Lcom/tencent/tauth/AddShareView$1$1;

    invoke-direct {v1, p0}, Lcom/tencent/tauth/AddShareView$1$1;-><init>(Lcom/tencent/tauth/AddShareView$1;)V

    invoke-virtual {v0, v1}, Lcom/tencent/tauth/AddShareView;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 85
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    # getter for: Lcom/tencent/tauth/AddShareView;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/tencent/tauth/AddShareView;->access$0(Lcom/tencent/tauth/AddShareView;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/tencent/tauth/AddShareView$1;->this$0:Lcom/tencent/tauth/AddShareView;

    invoke-virtual {v0}, Lcom/tencent/tauth/AddShareView;->finish()V

    .line 91
    return-void
.end method
