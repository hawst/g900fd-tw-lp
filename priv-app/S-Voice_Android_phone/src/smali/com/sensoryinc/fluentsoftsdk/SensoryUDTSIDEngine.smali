.class public Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;
.super Ljava/lang/Object;
.source "SensoryUDTSIDEngine.java"


# static fields
.field private static final MODELS_WA38:Ljava/lang/String; = "/system/wakeupdata/samsung/models_wa38.bin"

.field private static final SUBTAG:Ljava/lang/String; = "secMM: "

.field private static final TAG:Ljava/lang/String; = "SensoryUDTSIDEngine"

.field private static final UDTSID_ENROLL_CNT:S = 0x4s

.field private static final UDTSID_NUM_USERS:S = 0x5s

.field private static final UDTSIDnetfile:Ljava/lang/String; = "/system/wakeupdata/sensory/nn_en_us_mfcc_16k_15_250_v5.1.1.raw"

.field private static final UDTSIDsearchfile:Ljava/lang/String; = "/system/wakeupdata/sensory/phonemeSearch_1_3.raw"

.field public static VERSION_MAJOR:I = 0x0

.field public static VERSION_MINOR:I = 0x0

.field private static m_strFPSVnet:Ljava/lang/String; = null

.field private static m_strFPSVsearch:Ljava/lang/String; = null

.field private static m_strFPSVsvsid:Ljava/lang/String; = null

.field private static m_strLanguage:Ljava/lang/String; = null

.field private static m_strResourceDir:Ljava/lang/String; = null

.field private static final udtsvsidfile:Ljava/lang/String; = "/system/wakeupdata/sensory/svsid_1_1.raw"


# instance fields
.field private m_lSessionValue:J

.field private m_lSessionValueFPSV:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVnet:Ljava/lang/String;

    .line 19
    sput-object v0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsearch:Ljava/lang/String;

    .line 20
    sput-object v0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsvsid:Ljava/lang/String;

    .line 22
    sput-object v0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    .line 23
    sput-object v0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    .line 33
    sput v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MAJOR:I

    .line 34
    sput v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MINOR:I

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 31
    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 9
    return-void
.end method

.method private GetLanguageIndex()I
    .locals 4

    .prologue
    .line 572
    const/4 v0, 0x1

    .line 574
    .local v0, "nIndex":I
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 576
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "GetLanguageIndex, m_strLanguage is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :goto_0
    return v0

    .line 578
    :cond_0
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "ko_kr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 579
    :cond_1
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "en_us"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 580
    :cond_2
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "en_gb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    .line 581
    :cond_3
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "es_es"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    .line 582
    :cond_4
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "es_us"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x4

    goto :goto_0

    .line 583
    :cond_5
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "fr_fr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x5

    goto :goto_0

    .line 584
    :cond_6
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "de_de"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v0, 0x6

    goto :goto_0

    .line 585
    :cond_7
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "it_it"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v0, 0x7

    goto :goto_0

    .line 586
    :cond_8
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "zh_cn"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v0, 0x8

    goto :goto_0

    .line 587
    :cond_9
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "ja_jp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v0, 0x9

    goto/16 :goto_0

    .line 588
    :cond_a
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "ru_ru"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v0, 0xa

    goto/16 :goto_0

    .line 589
    :cond_b
    sget-object v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    const-string/jumbo v2, "pt_br"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v0, 0xb

    goto/16 :goto_0

    .line 592
    :cond_c
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "GetLanguageIndex, strLanguage is unknown : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private SetFPSVResource()Z
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 529
    const/16 v5, 0xc

    new-array v1, v5, [[Ljava/lang/String;

    .line 530
    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "am_ko_kr.raw"

    aput-object v6, v5, v3

    const-string/jumbo v6, "search_ko_kr.raw"

    aput-object v6, v5, v4

    const-string/jumbo v6, "ubm_ko_kr.raw"

    aput-object v6, v5, v8

    aput-object v5, v1, v3

    .line 531
    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "am_en_us.raw"

    aput-object v6, v5, v3

    const-string/jumbo v6, "search_en_us.raw"

    aput-object v6, v5, v4

    const-string/jumbo v6, "ubm_en_us.raw"

    aput-object v6, v5, v8

    aput-object v5, v1, v4

    .line 532
    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "am_en_gb.raw"

    aput-object v6, v5, v3

    const-string/jumbo v6, "search_en_gb.raw"

    aput-object v6, v5, v4

    const-string/jumbo v6, "ubm_en_gb.raw"

    aput-object v6, v5, v8

    aput-object v5, v1, v8

    .line 533
    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "am_es_es.raw"

    aput-object v6, v5, v3

    const-string/jumbo v6, "search_es_es.raw"

    aput-object v6, v5, v4

    const-string/jumbo v6, "ubm_es_es.raw"

    aput-object v6, v5, v8

    aput-object v5, v1, v9

    .line 534
    new-array v5, v9, [Ljava/lang/String;

    const-string/jumbo v6, "am_es_us.raw"

    aput-object v6, v5, v3

    const-string/jumbo v6, "search_es_us.raw"

    aput-object v6, v5, v4

    const-string/jumbo v6, "ubm_es_us.raw"

    aput-object v6, v5, v8

    aput-object v5, v1, v10

    const/4 v5, 0x5

    .line 535
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_fr_fr.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_fr_fr.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_fr_fr.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/4 v5, 0x6

    .line 536
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_de_de.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_de_de.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_de_de.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/4 v5, 0x7

    .line 537
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_it_it.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_it_it.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_it_it.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/16 v5, 0x8

    .line 538
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_zh_cn.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_zh_cn.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_zh_cn.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/16 v5, 0x9

    .line 539
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_ja_jp.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_ja_jp.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_ja_jp.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/16 v5, 0xa

    .line 540
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_ru_ru.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_ru_ru.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_ru_ru.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    const/16 v5, 0xb

    .line 541
    new-array v6, v9, [Ljava/lang/String;

    const-string/jumbo v7, "am_pt_br.raw"

    aput-object v7, v6, v3

    const-string/jumbo v7, "search_pt_br.raw"

    aput-object v7, v6, v4

    const-string/jumbo v7, "ubm_pt_br.raw"

    aput-object v7, v6, v8

    aput-object v6, v1, v5

    .line 544
    .local v1, "strResources":[[Ljava/lang/String;
    invoke-direct {p0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->GetLanguageIndex()I

    move-result v0

    .line 546
    .local v0, "nIndex":I
    const-string/jumbo v2, "higalaxy_"

    .line 547
    .local v2, "strTrigger":Ljava/lang/String;
    sget v5, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-ne v5, v10, :cond_0

    .line 549
    const-string/jumbo v2, "okgoogle_"

    .line 552
    :cond_0
    const-string/jumbo v5, "SensoryUDTSIDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "FPSV recog : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    aget-object v7, v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const-string/jumbo v5, "SensoryUDTSIDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "FPSV search : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    const-string/jumbo v5, "SensoryUDTSIDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "FPSV ubm : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v0

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    sget-object v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 558
    const-string/jumbo v4, "SensoryUDTSIDEngine"

    const-string/jumbo v5, "SetResourceforFPSV, m_strResourceDir is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    :goto_0
    return v3

    .line 562
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    sget-object v6, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v0

    aget-object v3, v6, v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVnet:Ljava/lang/String;

    .line 563
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v5, v1, v0

    aget-object v5, v5, v4

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsearch:Ljava/lang/String;

    .line 564
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v5, v1, v0

    aget-object v5, v5, v8

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsvsid:Ljava/lang/String;

    move v3, v4

    .line 566
    goto :goto_0
.end method

.method public static init()I
    .locals 4

    .prologue
    .line 82
    const/4 v1, 0x0

    .line 87
    .local v1, "nResult":I
    :try_start_0
    const-string/jumbo v2, "SensoryUDTSIDEngine"

    const-string/jumbo v3, "Trying to load libSensoryUDTSIDEngine.so"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string/jumbo v2, "SensoryUDTSIDEngine"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 89
    const-string/jumbo v2, "SensoryUDTSIDEngine"

    const-string/jumbo v3, "Loading libSensoryUDTSIDEngine.so"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 102
    :goto_0
    return v1

    .line 91
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    const-string/jumbo v2, "SensoryUDTSIDEngine"

    const-string/jumbo v3, "WARNING: libSensoryUDTSIDEngine Could not load libSensoryUDTSIDEngine.so"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const/4 v1, -0x1

    goto :goto_0

    .line 96
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 99
    const/4 v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public native CombineModel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public GetFWVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    invoke-static {}, Lcom/samsung/voiceshell/WakeUpUtils;->GetVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public native GetVersion()Ljava/lang/String;
.end method

.method public native GetWakeUpModel(IS)Ljava/lang/String;
.end method

.method public SetResourceInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "strResourceDir"    # Ljava/lang/String;
    .param p2, "strLanguage"    # Ljava/lang/String;

    .prologue
    .line 519
    sput-object p1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strResourceDir:Ljava/lang/String;

    .line 520
    sput-object p2, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strLanguage:Ljava/lang/String;

    .line 521
    return-void
.end method

.method public SetSessionValueFPSV(J)V
    .locals 3
    .param p1, "lSessionValueFPSV"    # J

    .prologue
    .line 124
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: setSessionValue, trigFPSV : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iput-wide p1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 126
    return-void
.end method

.method public SetSessionValueUDT(J)V
    .locals 3
    .param p1, "lSessionValue"    # J

    .prologue
    .line 117
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: setSessionValue, trig : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iput-wide p1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 119
    return-void
.end method

.method public SetVersion(Ljava/lang/String;)V
    .locals 4
    .param p1, "strVersion"    # Ljava/lang/String;

    .prologue
    .line 497
    const-string/jumbo v1, "\\."

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 499
    .local v0, "str":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 501
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "secMM: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "is error"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    :goto_0
    return-void

    .line 505
    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MAJOR:I

    .line 506
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MINOR:I

    .line 508
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "secMM: version : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MAJOR:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MINOR:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native SetWakeUpMode(I)V
.end method

.method public native SetWakeUpType(I)V
.end method

.method public native checkNewRecordings(JS)I
.end method

.method public native checkNewRecordings(JSI)I
.end method

.method public checkResultEnroll(SS)I
    .locals 8
    .param p1, "enrollIdx"    # S
    .param p2, "userID"    # S

    .prologue
    const v7, 0xffff

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 210
    const/4 v2, 0x0

    .line 212
    .local v2, "recResult":I
    const/4 v1, 0x0

    .line 213
    .local v1, "nUDTResult":I
    const/4 v0, 0x0

    .line 215
    .local v0, "nFPSVResult":I
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-eq v3, v5, :cond_0

    .line 216
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-eq v3, v6, :cond_0

    .line 217
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-nez v3, :cond_5

    .line 219
    :cond_0
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_2

    .line 221
    iget-wide v3, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v3, v4, p1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getResultEnroll(JSS)I

    move-result v2

    .line 222
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v3, v7, :cond_1

    if-ne v2, v5, :cond_1

    .line 224
    const-string/jumbo v3, "SensoryUDTSIDEngine"

    const-string/jumbo v4, "checkResultEnroll, Set WMode to WMODE_FPSV"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    sput v6, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    .line 226
    invoke-virtual {p0, v6}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->SetWakeUpMode(I)V

    .line 229
    :cond_1
    move v0, v2

    .line 232
    :cond_2
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_4

    .line 234
    iget-wide v3, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v3, v4, p1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getResultEnroll(JSS)I

    move-result v2

    .line 235
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v3, v7, :cond_3

    if-ne v2, v5, :cond_3

    .line 237
    const-string/jumbo v3, "SensoryUDTSIDEngine"

    const-string/jumbo v4, "checkResultEnroll, Set WMode to WMODE_UDT"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    sput v5, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    .line 239
    invoke-virtual {p0, v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->SetWakeUpMode(I)V

    .line 242
    :cond_3
    move v1, v2

    .line 256
    :cond_4
    :goto_0
    const-string/jumbo v3, "SensoryUDTSIDEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "VSG: checkResultEnroll, ResultUDT : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", ResultFPSV : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    return v2

    .line 245
    :cond_5
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v3, v5, :cond_6

    .line 247
    iget-wide v3, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v3, v4, p1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getResultEnroll(JSS)I

    move-result v2

    .line 248
    move v1, v2

    goto :goto_0

    .line 250
    :cond_6
    sget v3, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v3, v6, :cond_4

    .line 252
    iget-wide v3, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v3, v4, p1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getResultEnroll(JSS)I

    move-result v2

    .line 253
    move v0, v2

    goto :goto_0
.end method

.method public closeModels()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 481
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: closeModels, trig : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", trigFPSV : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 484
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v0, v1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->closeRecog(J)V

    .line 485
    iput-wide v4, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 488
    :cond_0
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 490
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v0, v1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->closeRecog(J)V

    .line 491
    iput-wide v4, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 493
    :cond_1
    return-void
.end method

.method public native closeRecog(J)V
.end method

.method public native closeSession(J)V
.end method

.method public consistencyCheckEnroll(S[SI)I
    .locals 10
    .param p1, "shUserID"    # S
    .param p2, "shProIndex"    # [S
    .param p3, "nEnrollResult"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 264
    const/4 v1, 0x0

    .line 267
    .local v1, "nResult":I
    const-wide/16 v2, 0x0

    .line 268
    .local v2, "trig":J
    sget v5, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v5, v6, :cond_1

    .line 270
    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 277
    :goto_0
    sget v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MAJOR:I

    if-gt v5, v6, :cond_2

    sget v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MINOR:I

    const/16 v6, 0x40

    if-gt v5, v6, :cond_2

    .line 279
    invoke-virtual {p0, v2, v3, p1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->checkNewRecordings(JS)I

    move-result v1

    .line 286
    :goto_1
    const-string/jumbo v5, "SensoryUDTSIDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "secMM: consistencyCheckEnroll.result : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", CommandNumber : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    aput-short v9, p2, v9

    .line 289
    const/4 v5, 0x5

    if-ne v1, v5, :cond_0

    .line 291
    invoke-virtual {p0, v2, v3, p1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getFeedbackDetails(JS)[J

    move-result-object v0

    .line 293
    .local v0, "details":[J
    const/4 v4, 0x0

    .local v4, "x":I
    :goto_2
    const/4 v5, 0x4

    if-lt v4, v5, :cond_3

    .line 304
    .end local v0    # "details":[J
    .end local v4    # "x":I
    :cond_0
    return v1

    .line 274
    :cond_1
    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    goto :goto_0

    .line 283
    :cond_2
    invoke-virtual {p0, v2, v3, p1, p3}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->checkNewRecordings(JSI)I

    move-result v1

    goto :goto_1

    .line 295
    .restart local v0    # "details":[J
    .restart local v4    # "x":I
    :cond_3
    aget-wide v5, v0, v4

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_4

    .line 297
    add-int/lit8 v5, v4, 0x1

    int-to-short v5, v5

    aput-short v5, p2, v9

    .line 300
    :cond_4
    const-string/jumbo v5, "SensoryUDTSIDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "secMM: consistencyCheckEnroll, Feedback details: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-wide v7, v0, v4

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public deleteModel(Ljava/lang/String;S)I
    .locals 5
    .param p1, "ROOT"    # Ljava/lang/String;
    .param p2, "CommandNumber"    # S

    .prologue
    .line 375
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "secMM: deleteModel, CommandNumber : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    const/4 v0, 0x0

    .line 378
    .local v0, "nResult":I
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 380
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "deleteModel, trig is ZERO!. need to load model"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const/4 v1, -0x1

    .line 391
    :goto_0
    return v1

    .line 384
    :cond_0
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v1, v2, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->deleteUser(JS)I

    move-result v0

    .line 385
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 387
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "secMM: deleteModel, re-doEnroll"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    invoke-virtual {p0, p1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->doEnrollModel(Ljava/lang/String;S)I

    .line 391
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public native deleteUser(JS)I
.end method

.method public doCombineModel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p1, "recogFILE1"    # Ljava/lang/String;
    .param p2, "searchFILE1"    # Ljava/lang/String;
    .param p3, "recogFILE2"    # Ljava/lang/String;
    .param p4, "searchFILE2"    # Ljava/lang/String;
    .param p5, "saveDIR"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 332
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "VSG: doCombineModel"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const-string/jumbo v1, "/system/wakeupdata/samsung/models_wa38.bin"

    invoke-static {v1}, Lcom/samsung/voiceshell/WakeUpUtils;->IsFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v6, "wa38"

    .line 335
    .local v6, "strTarget":Ljava/lang/String;
    :goto_0
    const/4 v7, 0x0

    .line 337
    .local v7, "nNullCount":I
    sget v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MAJOR:I

    if-gt v1, v3, :cond_1

    sget v1, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->VERSION_MINOR:I

    const/16 v2, 0x47

    if-gt v1, v2, :cond_1

    .line 339
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "CombineModel is not supported!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_1
    return v0

    .line 333
    .end local v6    # "strTarget":Ljava/lang/String;
    .end local v7    # "nNullCount":I
    :cond_0
    const-string/jumbo v6, "pc36"

    goto :goto_0

    .line 343
    .restart local v6    # "strTarget":Ljava/lang/String;
    .restart local v7    # "nNullCount":I
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 345
    :cond_2
    add-int/lit8 v7, v7, 0x1

    .line 347
    const-string/jumbo p1, ""

    .line 348
    const-string/jumbo p2, ""

    .line 351
    :cond_3
    if-eqz p3, :cond_4

    if-nez p4, :cond_5

    .line 353
    :cond_4
    add-int/lit8 v7, v7, 0x1

    .line 355
    const-string/jumbo p3, ""

    .line 356
    const-string/jumbo p4, ""

    .line 359
    :cond_5
    if-le v7, v3, :cond_6

    .line 361
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    const-string/jumbo v2, "File path is error!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG: doCombineModel, recogFile1 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG: doCombineModel, searchFile1 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG: doCombineModel, recogFile2 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "VSG: doCombineModel, searchFile2 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 369
    invoke-virtual/range {v0 .. v6}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->CombineModel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1
.end method

.method public native doEnroll(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public doEnrollModel(Ljava/lang/String;S)I
    .locals 5
    .param p1, "appDir"    # Ljava/lang/String;
    .param p2, "userIdx"    # S

    .prologue
    const/4 v4, 0x1

    .line 310
    const-string/jumbo v1, "SensoryUDTSIDEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "secMM: doEnrollModel, CommandNumber : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const-string/jumbo v0, ""

    .line 314
    .local v0, "strTarget":Ljava/lang/String;
    if-ne p2, v4, :cond_0

    .line 316
    const-string/jumbo v1, "/system/wakeupdata/samsung/models_wa38.bin"

    invoke-static {v1}, Lcom/samsung/voiceshell/WakeUpUtils;->IsFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v0, "wa38"

    .line 319
    :cond_0
    :goto_0
    sget v1, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v1, v4, :cond_2

    .line 321
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->doEnroll(JLjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 325
    :goto_1
    return v1

    .line 316
    :cond_1
    const-string/jumbo v0, "pc36"

    goto :goto_0

    .line 325
    :cond_2
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v1, v2, p1, v0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->doEnroll(JLjava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_1
.end method

.method public native getFeedbackDetails(JS)[J
.end method

.method public native getPhraseQuality(JS)F
.end method

.method public native getResultEnroll(JSS)I
.end method

.method public native getResultTest(JILjava/lang/String;)I
.end method

.method public getSessionValue()J
    .locals 4

    .prologue
    .line 131
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: getSessionValue, trig : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    return-wide v0
.end method

.method public getSessionValueFPSV()J
    .locals 4

    .prologue
    .line 139
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: getSessionValue, trigFPSV : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    return-wide v0
.end method

.method public native initRecog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;SSS)J
.end method

.method public native initSession()J
.end method

.method public loadModels(Ljava/lang/String;S)I
    .locals 13
    .param p1, "ROOT"    # Ljava/lang/String;
    .param p2, "CommandNumber"    # S

    .prologue
    .line 397
    const-wide/16 v10, 0x0

    .line 398
    .local v10, "UDTSIDinitRecogReturn":J
    const/4 v12, 0x0

    .line 400
    .local v12, "nResult":I
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->SetFPSVResource()Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    const-string/jumbo v1, "loadModels, Set WMode to WMODE_UDT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x1

    sput v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    .line 404
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    invoke-virtual {p0, v0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->SetWakeUpMode(I)V

    .line 406
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 408
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 409
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 417
    :cond_0
    :goto_0
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "loadModels, m_lSessionValue : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", m_lSessionValueFPSV : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 421
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: loadModels, Call initRecog, trigFPSV : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    sget-object v3, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVnet:Ljava/lang/String;

    sget-object v4, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsvsid:Ljava/lang/String;

    sget-object v5, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_strFPSVsearch:Ljava/lang/String;

    const/4 v7, 0x5

    const/4 v8, 0x4

    move-object v0, p0

    move-object v6, p1

    move v9, p2

    invoke-virtual/range {v0 .. v9}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->initRecog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;SSS)J

    move-result-wide v10

    .line 424
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_5

    .line 426
    invoke-static {}, Lcom/samsung/voiceshell/AudioRecordHaydn;->GetRecordingMode()I

    move-result v0

    if-nez v0, :cond_4

    .line 428
    const/4 v12, 0x1

    .line 449
    :cond_1
    :goto_1
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 451
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: loadModels, Call initRecog, trig : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const-string/jumbo v3, "/system/wakeupdata/sensory/nn_en_us_mfcc_16k_15_250_v5.1.1.raw"

    const-string/jumbo v4, "/system/wakeupdata/sensory/svsid_1_1.raw"

    const-string/jumbo v5, "/system/wakeupdata/sensory/phonemeSearch_1_3.raw"

    const/4 v7, 0x5

    const/4 v8, 0x4

    move-object v0, p0

    move-object v6, p1

    move v9, p2

    invoke-virtual/range {v0 .. v9}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->initRecog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;SSS)J

    move-result-wide v10

    .line 454
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_7

    .line 456
    invoke-static {}, Lcom/samsung/voiceshell/AudioRecordHaydn;->GetRecordingMode()I

    move-result v0

    if-nez v0, :cond_6

    .line 458
    const/4 v12, 0x1

    .line 476
    :cond_2
    :goto_2
    return v12

    .line 411
    :cond_3
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 413
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v0, v1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->closeRecog(J)V

    .line 414
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    goto/16 :goto_0

    .line 432
    :cond_4
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v0, v1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->loadUsers(JS)I

    move-result v12

    goto :goto_1

    .line 437
    :cond_5
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "LoadModels, failed to initRecog, result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", CommandNumber : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", trigFPSV : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    invoke-virtual {p0, v0, v1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->closeRecog(J)V

    .line 441
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 443
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    const-string/jumbo v1, "loadModels, Set WMode to WMODE_UDT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v0, 0x1

    sput v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    .line 445
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    invoke-virtual {p0, v0}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->SetWakeUpMode(I)V

    goto/16 :goto_1

    .line 462
    :cond_6
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v0, v1, p2}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->loadUsers(JS)I

    move-result v12

    goto :goto_2

    .line 467
    :cond_7
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "LoadModels, failed to initRecog, result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", CommandNumber : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", trig : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    invoke-virtual {p0, v0, v1}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->closeRecog(J)V

    .line 470
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 472
    const/4 v12, 0x0

    goto/16 :goto_2
.end method

.method public native loadUsers(JS)I
.end method

.method public native pipeRecogEnroll(J[SJ)I
.end method

.method public native pipeRecogTest(J[SJ)I
.end method

.method public processEnroll([SI)I
    .locals 7
    .param p1, "recordBuffer"    # [S
    .param p2, "readLength"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 147
    const/4 v6, 0x0

    .line 149
    .local v6, "nResult":I
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-eq v0, v1, :cond_0

    .line 150
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-eq v0, v2, :cond_0

    .line 151
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WType:I

    if-nez v0, :cond_3

    .line 153
    :cond_0
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 155
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    int-to-long v4, p2

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->pipeRecogEnroll(J[SJ)I

    move-result v6

    .line 158
    :cond_1
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 160
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    int-to-long v4, p2

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->pipeRecogEnroll(J[SJ)I

    move-result v6

    .line 172
    :cond_2
    :goto_0
    return v6

    .line 163
    :cond_3
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v0, v1, :cond_4

    .line 165
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    int-to-long v4, p2

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->pipeRecogEnroll(J[SJ)I

    move-result v6

    goto :goto_0

    .line 167
    :cond_4
    sget v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->WMode:I

    if-ne v0, v2, :cond_2

    .line 169
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    int-to-long v4, p2

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->pipeRecogEnroll(J[SJ)I

    move-result v6

    goto :goto_0
.end method

.method public processVerify([SILjava/lang/String;[S)I
    .locals 9
    .param p1, "recordBuffer"    # [S
    .param p2, "readLength"    # I
    .param p3, "ROOT"    # Ljava/lang/String;
    .param p4, "CommandType"    # [S

    .prologue
    const/4 v8, 0x0

    .line 178
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 180
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    const-string/jumbo v1, "secMM: processVerify , trig is ZERO, terminate to verify"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/16 v6, -0xc

    .line 204
    :goto_0
    return v6

    .line 184
    :cond_0
    const/4 v6, 0x0

    .line 185
    .local v6, "iResult":I
    iget-wide v1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    int-to-long v4, p2

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->pipeRecogTest(J[SJ)I

    move-result v7

    .line 187
    .local v7, "nRecogResult":I
    const/4 v0, 0x2

    if-ne v7, v0, :cond_2

    .line 189
    const/4 v6, 0x0

    .line 191
    iget-wide v0, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2, p3}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->getResultTest(JILjava/lang/String;)I

    move-result v0

    int-to-short v0, v0

    aput-short v0, p4, v8

    .line 192
    aget-short v0, p4, v8

    if-ltz v0, :cond_1

    .line 194
    const/4 v6, 0x1

    .line 202
    :cond_1
    :goto_1
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: processVerify, pipeRecogTest, nRecogResult : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", iResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 199
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public native resetRecog(J)V
.end method

.method public setSessionValue(JJ)V
    .locals 3
    .param p1, "lSessionValue"    # J
    .param p3, "lSessionValueFPSV"    # J

    .prologue
    .line 108
    const-string/jumbo v0, "SensoryUDTSIDEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "secMM: setSessionValue, trig : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", trigFPSV : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iput-wide p1, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValue:J

    .line 111
    iput-wide p3, p0, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->m_lSessionValueFPSV:J

    .line 112
    return-void
.end method
