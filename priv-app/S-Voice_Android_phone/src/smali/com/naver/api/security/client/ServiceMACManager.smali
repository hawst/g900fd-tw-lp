.class public Lcom/naver/api/security/client/ServiceMACManager;
.super Ljava/lang/Object;
.source "ServiceMACManager.java"


# static fields
.field private static final EXTSERVICE:Ljava/lang/String; = "__extservice__"

.field private static final KEY_FILENAME:Ljava/lang/String; = "/NHNAPIGatewayKey.properties"

.field private static mac:Ljavax/crypto/Mac;

.field private static serviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEncryptUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/naver/api/security/client/ServiceMACManager;->mac:Ljavax/crypto/Mac;

    if-nez v0, :cond_0

    .line 79
    invoke-static {}, Lcom/naver/api/security/client/ServiceMACManager;->initialize()V

    .line 81
    :cond_0
    const-string/jumbo v0, "__extservice__"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "wrong url : \'__extservice__\' not exists"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_1
    const-string/jumbo v0, "__extservice__"

    sget-object v1, Lcom/naver/api/security/client/ServiceMACManager;->serviceId:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 88
    sget-object v0, Lcom/naver/api/security/client/ServiceMACManager;->mac:Ljavax/crypto/Mac;

    invoke-static {v0, p0}, Lcom/naver/api/security/HmacUtil;->makeEncryptUrl(Ljavax/crypto/Mac;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initialize()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    const/4 v2, 0x0

    .line 46
    .local v2, "key":Ljava/lang/String;
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 47
    .local v3, "properties":Ljava/util/Properties;
    const/4 v0, 0x0

    .line 50
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    const-class v4, Lcom/naver/api/security/client/ServiceMACManager;

    const-string/jumbo v5, "/NHNAPIGatewayKey.properties"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 51
    invoke-virtual {v3, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 52
    invoke-virtual {v3}, Ljava/util/Properties;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 53
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sput-object v4, Lcom/naver/api/security/client/ServiceMACManager;->serviceId:Ljava/lang/String;

    .line 54
    sget-object v4, Lcom/naver/api/security/client/ServiceMACManager;->serviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 56
    if-nez v2, :cond_1

    .line 57
    new-instance v4, Ljava/lang/Exception;

    const-string/jumbo v5, "HMAC key Not Exists"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    :catchall_0
    move-exception v4

    .line 62
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 65
    :cond_0
    throw v4

    .line 60
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Object;>;"
    :cond_1
    :try_start_1
    invoke-static {v2}, Lcom/naver/api/security/HmacUtil;->getMac(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4

    sput-object v4, Lcom/naver/api/security/client/ServiceMACManager;->mac:Ljavax/crypto/Mac;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 66
    :cond_2
    return-void
.end method
