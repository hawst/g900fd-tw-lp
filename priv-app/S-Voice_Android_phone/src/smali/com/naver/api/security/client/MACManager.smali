.class public Lcom/naver/api/security/client/MACManager;
.super Ljava/lang/Object;
.source "MACManager.java"


# static fields
.field private static final KEY_FILENAME:Ljava/lang/String; = "/NHNAPIGatewayKey.properties"

.field private static mac:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEncryptUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/naver/api/security/client/MACManager;->mac:Ljavax/crypto/Mac;

    if-nez v0, :cond_0

    .line 64
    invoke-static {}, Lcom/naver/api/security/client/MACManager;->initialize()V

    .line 67
    :cond_0
    sget-object v0, Lcom/naver/api/security/client/MACManager;->mac:Ljavax/crypto/Mac;

    invoke-static {v0, p0}, Lcom/naver/api/security/HmacUtil;->makeEncryptUrl(Ljavax/crypto/Mac;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initialize()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v2, 0x0

    .line 38
    .local v2, "key":Ljava/lang/String;
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 39
    .local v3, "properties":Ljava/util/Properties;
    const/4 v1, 0x0

    .line 42
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    const-class v4, Lcom/naver/api/security/client/MACManager;

    const-string/jumbo v5, "/NHNAPIGatewayKey.properties"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 43
    invoke-virtual {v3, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 44
    invoke-virtual {v3}, Ljava/util/Properties;->elements()Ljava/util/Enumeration;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 45
    invoke-static {v2}, Lcom/naver/api/security/HmacUtil;->getMac(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v4

    sput-object v4, Lcom/naver/api/security/client/MACManager;->mac:Ljavax/crypto/Mac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 51
    :cond_0
    return-void

    .line 46
    :catchall_0
    move-exception v4

    .line 47
    if-eqz v1, :cond_1

    .line 48
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 50
    :cond_1
    throw v4
.end method
