.class public Lcom/facebook/android/FbDialog;
.super Landroid/app/Dialog;
.source "FbDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/android/FbDialog$FbWebViewClient;
    }
.end annotation


# static fields
.field static final DIMENSIONS_LANDSCAPE:[F

.field static final DIMENSIONS_LANDSCAPE_TABLET:[F

.field static final DIMENSIONS_PORTRAIT:[F

.field static final DIMENSIONS_PORTRAIT_TABLET:[F

.field static final DISPLAY_STRING:Ljava/lang/String; = "touch"

.field static final FB_BLUE:I = -0x927b4c

.field static final FB_ICON:Ljava/lang/String; = "icon.png"

.field static final FILL:Landroid/widget/FrameLayout$LayoutParams;

.field static final MARGIN:I = 0x4

.field static final PADDING:I = 0x2


# instance fields
.field private handler:Landroid/os/Handler;

.field public isDialogActive:Z

.field private mContent:Landroid/widget/LinearLayout;

.field private mDialog:Lcom/facebook/android/FbDialog;

.field private mListener:Lcom/facebook/android/Facebook$DialogListener;

.field private mSpinner:Landroid/app/ProgressDialog;

.field private mTitle:Landroid/widget/TextView;

.field private mUrl:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x2

    .line 56
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE:[F

    .line 57
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT:[F

    .line 58
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE_TABLET:[F

    .line 59
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT_TABLET:[F

    .line 60
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/facebook/android/FbDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    return-void

    .line 56
    :array_0
    .array-data 4
        0x43e60000    # 460.0f
        0x43820000    # 260.0f
    .end array-data

    .line 57
    :array_1
    .array-data 4
        0x438c0000    # 280.0f
        0x43d20000    # 420.0f
    .end array-data

    .line 58
    :array_2
    .array-data 4
        0x438c0000    # 280.0f
        0x43af0000    # 350.0f
    .end array-data

    .line 59
    :array_3
    .array-data 4
        0x438c0000    # 280.0f
        0x43b40000    # 360.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/facebook/android/Facebook$DialogListener;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/android/FbDialog;->isDialogActive:Z

    .line 81
    iput-object p2, p0, Lcom/facebook/android/FbDialog;->mUrl:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lcom/facebook/android/FbDialog;->mListener:Lcom/facebook/android/Facebook$DialogListener;

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/android/FbDialog;->handler:Landroid/os/Handler;

    .line 84
    iput-object p0, p0, Lcom/facebook/android/FbDialog;->mDialog:Lcom/facebook/android/FbDialog;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/facebook/android/FbDialog;)Lcom/facebook/android/FbDialog;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mDialog:Lcom/facebook/android/FbDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/facebook/android/FbDialog;)Lcom/facebook/android/Facebook$DialogListener;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mListener:Lcom/facebook/android/Facebook$DialogListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/facebook/android/FbDialog;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/facebook/android/FbDialog;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/facebook/android/FbDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/facebook/android/FbDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/facebook/android/FbDialog;->removeSpinner()V

    return-void
.end method

.method static synthetic access$700(Lcom/facebook/android/FbDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/facebook/android/FbDialog;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private isWindowShowing()Z
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized removeSpinner()V
    .locals 1

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 294
    invoke-direct {p0}, Lcom/facebook/android/FbDialog;->isWindowShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    :try_start_1
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 298
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 301
    :cond_0
    monitor-exit p0

    return-void

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 297
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setUpTitle()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 174
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/android/FbDialog;->requestWindowFeature(I)Z

    .line 175
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$drawable;->core_facebook_icon:Lcom/vlingo/core/internal/ResourceIdProvider$drawable;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getDrawable(Lcom/vlingo/core/internal/ResourceIdProvider$drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 176
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    .line 177
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    const-string/jumbo v2, "Facebook"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 180
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    const v2, -0x927b4c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 181
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v5, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 182
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 183
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/android/FbDialog;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 187
    return-void
.end method

.method private setUpWebView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 190
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    .line 192
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    sget-object v1, Lcom/facebook/android/FbDialog;->FILL:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/facebook/android/FbDialog$FbWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/facebook/android/FbDialog$FbWebViewClient;-><init>(Lcom/facebook/android/FbDialog;Lcom/facebook/android/FbDialog$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 198
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 200
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 201
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/facebook/android/FbDialog;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mUrl:Ljava/lang/String;

    new-instance v2, Lcom/facebook/android/FbDialog$3;

    invoke-direct {v2, p0}, Lcom/facebook/android/FbDialog$3;-><init>(Lcom/facebook/android/FbDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    .line 205
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 89
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$string;->core_car_util_loading:Lcom/vlingo/core/internal/ResourceIdProvider$string;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getString(Lcom/vlingo/core/internal/ResourceIdProvider$string;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "loadingMsg":Ljava/lang/String;
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 92
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    .line 93
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 94
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mSpinner:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/facebook/android/FbDialog$1;

    invoke-direct {v2, p0}, Lcom/facebook/android/FbDialog$1;-><init>(Lcom/facebook/android/FbDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 101
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 102
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 103
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 105
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    .line 106
    iget-object v1, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 108
    invoke-direct {p0}, Lcom/facebook/android/FbDialog;->setUpTitle()V

    .line 109
    invoke-direct {p0}, Lcom/facebook/android/FbDialog;->setUpWebView()V

    .line 110
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->updateView()V

    .line 112
    new-instance v1, Lcom/facebook/android/FbDialog$2;

    invoke-direct {v1, p0}, Lcom/facebook/android/FbDialog$2;-><init>(Lcom/facebook/android/FbDialog;)V

    invoke-virtual {p0, v1}, Lcom/facebook/android/FbDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 117
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/facebook/android/FbDialog;->removeSpinner()V

    .line 210
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 211
    return-void
.end method

.method public updateDialog()V
    .locals 12

    .prologue
    const v6, 0x3f333333    # 0.7f

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v9, 0x3f000000    # 0.5f

    .line 146
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 147
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 148
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->density:F

    .line 151
    .local v4, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 152
    const/4 v5, 0x2

    new-array v0, v5, [F

    .line 153
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v10

    .line 154
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    aput v5, v0, v11

    .line 155
    const/high16 v4, 0x3f800000    # 1.0f

    .line 166
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    aget v6, v0, v10

    mul-float/2addr v6, v4

    add-float/2addr v6, v9

    float-to-int v6, v6

    aget v7, v0, v11

    mul-float/2addr v7, v4

    add-float/2addr v7, v9

    float-to-int v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/view/Window;->setLayout(II)V

    .line 167
    iget-object v5, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 168
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    aget v5, v0, v10

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 169
    aget v5, v0, v11

    mul-float/2addr v5, v4

    add-float/2addr v5, v9

    float-to-int v5, v5

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 170
    iget-object v5, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    return-void

    .line 156
    .end local v0    # "dimensions":[F
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    float-to-double v5, v4

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v5, v7

    if-nez v5, :cond_2

    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v6, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v5, v6

    const/16 v6, 0x7f0

    if-ne v5, v6, :cond_2

    iget v5, v3, Landroid/util/DisplayMetrics;->xdpi:F

    const v6, 0x4315d32c

    cmpl-float v5, v5, v6

    if-nez v5, :cond_2

    iget v5, v3, Landroid/util/DisplayMetrics;->ydpi:F

    const v6, 0x431684be

    cmpl-float v5, v5, v6

    if-nez v5, :cond_2

    .line 159
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_1

    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT_TABLET:[F

    .line 161
    .restart local v0    # "dimensions":[F
    :goto_1
    const/high16 v4, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 159
    .end local v0    # "dimensions":[F
    :cond_1
    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE_TABLET:[F

    goto :goto_1

    .line 163
    :cond_2
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_3

    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_2
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_3
    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_2
.end method

.method public updateView()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const v5, 0x3f333333    # 0.7f

    const/high16 v8, 0x3f000000    # 0.5f

    .line 120
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 121
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {p0}, Lcom/facebook/android/FbDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 122
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    .line 125
    .local v3, "scale":F
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isMiniTabletDevice()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 126
    const/4 v4, 0x2

    new-array v0, v4, [F

    .line 127
    .local v0, "dimensions":[F
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v9

    .line 128
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    aput v4, v0, v10

    .line 129
    const/high16 v3, 0x3f800000    # 1.0f

    .line 140
    :goto_0
    iget-object v4, p0, Lcom/facebook/android/FbDialog;->mContent:Landroid/widget/LinearLayout;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    aget v6, v0, v9

    mul-float/2addr v6, v3

    add-float/2addr v6, v8

    float-to-int v6, v6

    aget v7, v0, v10

    mul-float/2addr v7, v3

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4, v5}, Lcom/facebook/android/FbDialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    return-void

    .line 130
    .end local v0    # "dimensions":[F
    :cond_0
    float-to-double v4, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_2

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v4, v5

    const/16 v5, 0x7f0

    if-ne v4, v5, :cond_2

    iget v4, v2, Landroid/util/DisplayMetrics;->xdpi:F

    const v5, 0x4315d32c

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    iget v4, v2, Landroid/util/DisplayMetrics;->ydpi:F

    const v5, 0x431684be

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    .line 133
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    if-ge v4, v5, :cond_1

    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT_TABLET:[F

    .line 135
    .restart local v0    # "dimensions":[F
    :goto_1
    const/high16 v3, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 133
    .end local v0    # "dimensions":[F
    :cond_1
    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE_TABLET:[F

    goto :goto_1

    .line 137
    :cond_2
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v5

    if-ge v4, v5, :cond_3

    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_PORTRAIT:[F

    .restart local v0    # "dimensions":[F
    :goto_2
    goto :goto_0

    .end local v0    # "dimensions":[F
    :cond_3
    sget-object v0, Lcom/facebook/android/FbDialog;->DIMENSIONS_LANDSCAPE:[F

    goto :goto_2
.end method
