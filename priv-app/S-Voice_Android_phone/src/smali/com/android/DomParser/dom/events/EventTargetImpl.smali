.class public Lcom/android/DomParser/dom/events/EventTargetImpl;
.super Ljava/lang/Object;
.source "EventTargetImpl.java"

# interfaces
.implements Lorg/w3c/dom/events/EventTarget;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/EventTargetImpl"


# instance fields
.field private mListenerEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mNodeTarget:Lorg/w3c/dom/events/EventTarget;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/events/EventTarget;)V
    .locals 0
    .param p1, "target"    # Lorg/w3c/dom/events/EventTarget;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mNodeTarget:Lorg/w3c/dom/events/EventTarget;

    .line 50
    return-void
.end method


# virtual methods
.method public addEventListener(Ljava/lang/String;Lorg/w3c/dom/events/EventListener;Z)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "listener"    # Lorg/w3c/dom/events/EventListener;
    .param p3, "useCapture"    # Z

    .prologue
    .line 53
    if-eqz p1, :cond_0

    const-string/jumbo v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/DomParser/dom/events/EventTargetImpl;->removeEventListener(Ljava/lang/String;Lorg/w3c/dom/events/EventListener;Z)V

    .line 60
    iget-object v0, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;-><init>(Ljava/lang/String;Lorg/w3c/dom/events/EventListener;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public dispatchEvent(Lorg/w3c/dom/events/Event;)Z
    .locals 6
    .param p1, "evt"    # Lorg/w3c/dom/events/Event;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/events/EventException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 68
    move-object v0, p1

    check-cast v0, Lcom/android/DomParser/dom/events/EventImpl;

    .line 70
    .local v0, "eventImpl":Lcom/android/DomParser/dom/events/EventImpl;
    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_0

    .line 71
    new-instance v3, Lorg/w3c/dom/events/EventException;

    .line 72
    const-string/jumbo v4, "Event not initialized"

    .line 71
    invoke-direct {v3, v5, v4}, Lorg/w3c/dom/events/EventException;-><init>(SLjava/lang/String;)V

    throw v3

    .line 73
    :cond_0
    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->getType()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    :cond_1
    new-instance v3, Lorg/w3c/dom/events/EventException;

    .line 75
    const-string/jumbo v4, "Unspecified even type"

    .line 74
    invoke-direct {v3, v5, v4}, Lorg/w3c/dom/events/EventException;-><init>(SLjava/lang/String;)V

    throw v3

    .line 79
    :cond_2
    iget-object v3, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mNodeTarget:Lorg/w3c/dom/events/EventTarget;

    invoke-virtual {v0, v3}, Lcom/android/DomParser/dom/events/EventImpl;->setTarget(Lorg/w3c/dom/events/EventTarget;)V

    .line 89
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/android/DomParser/dom/events/EventImpl;->setEventPhase(S)V

    .line 90
    iget-object v3, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mNodeTarget:Lorg/w3c/dom/events/EventTarget;

    invoke-virtual {v0, v3}, Lcom/android/DomParser/dom/events/EventImpl;->setCurrentTarget(Lorg/w3c/dom/events/EventTarget;)V

    .line 91
    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->isPropogationStopped()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 92
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_4

    .line 105
    .end local v1    # "i":I
    :cond_3
    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->getBubbles()Z

    .line 109
    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->isPreventDefault()Z

    move-result v3

    return v3

    .line 93
    .restart local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;

    .line 94
    .local v2, "listenerEntry":Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;
    iget-boolean v3, v2, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mUseCapture:Z

    if-nez v3, :cond_5

    iget-object v3, v2, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mType:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/android/DomParser/dom/events/EventImpl;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    :try_start_0
    iget-object v3, v2, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mListener:Lorg/w3c/dom/events/EventListener;

    invoke-interface {v3, v0}, Lorg/w3c/dom/events/EventListener;->handleEvent(Lorg/w3c/dom/events/Event;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_5
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public removeEventListener(Ljava/lang/String;Lorg/w3c/dom/events/EventListener;Z)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "listener"    # Lorg/w3c/dom/events/EventListener;
    .param p3, "useCapture"    # Z

    .prologue
    .line 113
    iget-object v2, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;

    .line 118
    .local v1, "listenerEntry":Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;
    iget-boolean v2, v1, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mUseCapture:Z

    if-ne v2, p3, :cond_2

    iget-object v2, v1, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mListener:Lorg/w3c/dom/events/EventListener;

    if-ne v2, p2, :cond_2

    .line 119
    iget-object v2, v1, Lcom/android/DomParser/dom/events/EventTargetImpl$EventListenerEntry;->mType:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    iget-object v2, p0, Lcom/android/DomParser/dom/events/EventTargetImpl;->mListenerEntries:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
