.class public Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;
.super Lcom/android/DomParser/dom/smil/SmilElementImpl;
.source "SmilMediaElementImpl.java"

# interfaces
.implements Lorg/w3c/dom/smil/SMILMediaElement;


# static fields
.field private static final DEBUG:Z = false

.field private static final LOCAL_LOGV:Z = false

.field public static final SMIL_MEDIA_END_EVENT:Ljava/lang/String; = "SmilMediaEnd"

.field public static final SMIL_MEDIA_PAUSE_EVENT:Ljava/lang/String; = "SmilMediaPause"

.field public static final SMIL_MEDIA_SEEK_EVENT:Ljava/lang/String; = "SmilMediaSeek"

.field public static final SMIL_MEDIA_START_EVENT:Ljava/lang/String; = "SmilMediaStart"

.field private static final TAG:Ljava/lang/String; = "Mms:smil"


# instance fields
.field mElementTime:Lorg/w3c/dom/smil/ElementTime;


# direct methods
.method constructor <init>(Lcom/android/DomParser/dom/smil/SmilDocumentImpl;Ljava/lang/String;)V
    .locals 1
    .param p1, "owner"    # Lcom/android/DomParser/dom/smil/SmilDocumentImpl;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/android/DomParser/dom/smil/SmilElementImpl;-><init>(Lcom/android/DomParser/dom/smil/SmilDocumentImpl;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl$1;

    invoke-direct {v0, p0, p0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl$1;-><init>(Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;Lorg/w3c/dom/smil/SMILElement;)V

    iput-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    .line 118
    return-void
.end method


# virtual methods
.method public beginElement()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->beginElement()Z

    move-result v0

    return v0
.end method

.method public endElement()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->endElement()Z

    move-result v0

    return v0
.end method

.method public getAbstractAttr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const-string/jumbo v0, "abstract"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    const-string/jumbo v0, "alt"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const-string/jumbo v0, "author"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBegin()Lorg/w3c/dom/smil/TimeList;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getBegin()Lorg/w3c/dom/smil/TimeList;

    move-result-object v0

    return-object v0
.end method

.method public getClipBegin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    const-string/jumbo v0, "clipBegin"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClipEnd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string/jumbo v0, "clipEnd"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCopyright()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    const-string/jumbo v0, "copyright"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDur()F
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getDur()F

    move-result v0

    return v0
.end method

.method public getEnd()Lorg/w3c/dom/smil/TimeList;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getEnd()Lorg/w3c/dom/smil/TimeList;

    move-result-object v0

    return-object v0
.end method

.method public getFill()S
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getFill()S

    move-result v0

    return v0
.end method

.method public getFillDefault()S
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getFillDefault()S

    move-result v0

    return v0
.end method

.method public getLongdesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const-string/jumbo v0, "longdesc"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    const-string/jumbo v0, "port"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadIndex()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string/jumbo v0, "readIndex"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRepeatCount()F
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getRepeatCount()F

    move-result v0

    return v0
.end method

.method public getRepeatDur()F
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getRepeatDur()F

    move-result v0

    return v0
.end method

.method public getRestart()S
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->getRestart()S

    move-result v0

    return v0
.end method

.method public getRtpformat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const-string/jumbo v0, "rtpformat"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSrc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const-string/jumbo v0, "src"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStripRepeat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    const-string/jumbo v0, "stripRepeat"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransport()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    const-string/jumbo v0, "transport"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public pauseElement()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->pauseElement()V

    .line 291
    return-void
.end method

.method public resumeElement()V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0}, Lorg/w3c/dom/smil/ElementTime;->resumeElement()V

    .line 295
    return-void
.end method

.method public seekElement(F)V
    .locals 1
    .param p1, "seekTo"    # F

    .prologue
    .line 298
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->seekElement(F)V

    .line 299
    return-void
.end method

.method public setAbstractAttr(Ljava/lang/String;)V
    .locals 1
    .param p1, "abstractAttr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 185
    const-string/jumbo v0, "abstract"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public setAlt(Ljava/lang/String;)V
    .locals 1
    .param p1, "alt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 189
    const-string/jumbo v0, "alt"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 193
    const-string/jumbo v0, "author"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method public setBegin(Lorg/w3c/dom/smil/TimeList;)V
    .locals 1
    .param p1, "begin"    # Lorg/w3c/dom/smil/TimeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setBegin(Lorg/w3c/dom/smil/TimeList;)V

    .line 303
    return-void
.end method

.method public setClipBegin(Ljava/lang/String;)V
    .locals 1
    .param p1, "clipBegin"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 197
    const-string/jumbo v0, "clipBegin"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method public setClipEnd(Ljava/lang/String;)V
    .locals 1
    .param p1, "clipEnd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 201
    const-string/jumbo v0, "clipEnd"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public setCopyright(Ljava/lang/String;)V
    .locals 1
    .param p1, "copyright"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 205
    const-string/jumbo v0, "copyright"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method public setDur(F)V
    .locals 1
    .param p1, "dur"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 306
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setDur(F)V

    .line 307
    return-void
.end method

.method public setEnd(Lorg/w3c/dom/smil/TimeList;)V
    .locals 1
    .param p1, "end"    # Lorg/w3c/dom/smil/TimeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setEnd(Lorg/w3c/dom/smil/TimeList;)V

    .line 311
    return-void
.end method

.method public setFill(S)V
    .locals 1
    .param p1, "fill"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 314
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setFill(S)V

    .line 315
    return-void
.end method

.method public setFillDefault(S)V
    .locals 1
    .param p1, "fillDefault"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setFillDefault(S)V

    .line 319
    return-void
.end method

.method public setLongdesc(Ljava/lang/String;)V
    .locals 1
    .param p1, "longdesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 209
    const-string/jumbo v0, "longdesc"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method public setPort(Ljava/lang/String;)V
    .locals 1
    .param p1, "port"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 214
    const-string/jumbo v0, "port"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public setReadIndex(Ljava/lang/String;)V
    .locals 1
    .param p1, "readIndex"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 218
    const-string/jumbo v0, "readIndex"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public setRepeatCount(F)V
    .locals 1
    .param p1, "repeatCount"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 322
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setRepeatCount(F)V

    .line 323
    return-void
.end method

.method public setRepeatDur(F)V
    .locals 1
    .param p1, "repeatDur"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 326
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setRepeatDur(F)V

    .line 327
    return-void
.end method

.method public setRestart(S)V
    .locals 1
    .param p1, "restart"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->mElementTime:Lorg/w3c/dom/smil/ElementTime;

    invoke-interface {v0, p1}, Lorg/w3c/dom/smil/ElementTime;->setRestart(S)V

    .line 331
    return-void
.end method

.method public setRtpformat(Ljava/lang/String;)V
    .locals 1
    .param p1, "rtpformat"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 222
    const-string/jumbo v0, "rtpformat"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public setSrc(Ljava/lang/String;)V
    .locals 1
    .param p1, "src"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 226
    const-string/jumbo v0, "src"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public setStripRepeat(Ljava/lang/String;)V
    .locals 1
    .param p1, "stripRepeat"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 230
    const-string/jumbo v0, "stripRepeat"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 234
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public setTransport(Ljava/lang/String;)V
    .locals 1
    .param p1, "transport"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 238
    const-string/jumbo v0, "transport"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/w3c/dom/DOMException;
        }
    .end annotation

    .prologue
    .line 242
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0, p1}, Lcom/android/DomParser/dom/smil/SmilMediaElementImpl;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    return-void
.end method
