.class public Lcom/android/DomParser/dom/model/SmilHelper;
.super Ljava/lang/Object;
.source "SmilHelper.java"


# static fields
.field private static final DEBUG:Z = false

.field public static final ELEMENT_TAG_AUDIO:Ljava/lang/String; = "audio"

.field public static final ELEMENT_TAG_IMAGE:Ljava/lang/String; = "img"

.field public static final ELEMENT_TAG_REF:Ljava/lang/String; = "ref"

.field public static final ELEMENT_TAG_TEXT:Ljava/lang/String; = "text"

.field public static final ELEMENT_TAG_VIDEO:Ljava/lang/String; = "video"

.field private static final LOCAL_LOGV:Z = false

.field private static final TAG:Ljava/lang/String; = "Mms/smil"

.field private static isMixedSmil:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    return-void
.end method

.method public static addPar(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILParElement;
    .locals 2
    .param p0, "document"    # Lorg/w3c/dom/smil/SMILDocument;

    .prologue
    .line 143
    const-string/jumbo v1, "par"

    invoke-interface {p0, v1}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/SMILParElement;

    .line 144
    .local v0, "par":Lorg/w3c/dom/smil/SMILParElement;
    invoke-interface {p0}, Lorg/w3c/dom/smil/SMILDocument;->getBody()Lorg/w3c/dom/smil/SMILElement;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/w3c/dom/smil/SMILElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 145
    return-object v0
.end method

.method public static createMediaElement(Ljava/lang/String;Lorg/w3c/dom/smil/SMILDocument;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILMediaElement;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "document"    # Lorg/w3c/dom/smil/SMILDocument;
    .param p2, "src"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-interface {p1, p0}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/SMILMediaElement;

    .line 150
    .local v0, "mediaElement":Lorg/w3c/dom/smil/SMILMediaElement;
    invoke-static {p2}, Lcom/android/DomParser/dom/model/SmilHelper;->escapeXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3c/dom/smil/SMILMediaElement;->setSrc(Ljava/lang/String;)V

    .line 151
    return-object v0
.end method

.method private static createSmilDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;
    .locals 20
    .param p0, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    new-instance v5, Lcom/android/DomParser/dom/smil/SmilDocumentImpl;

    invoke-direct {v5}, Lcom/android/DomParser/dom/smil/SmilDocumentImpl;-><init>()V

    .line 165
    .local v5, "document":Lorg/w3c/dom/smil/SMILDocument;
    const-string/jumbo v18, "smil"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v15

    check-cast v15, Lorg/w3c/dom/smil/SMILElement;

    .line 166
    .local v15, "smil":Lorg/w3c/dom/smil/SMILElement;
    const-string/jumbo v18, "xmlns"

    const-string/jumbo v19, "http://www.w3.org/2001/SMIL20/Language"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v15, v0, v1}, Lorg/w3c/dom/smil/SMILElement;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-interface {v5, v15}, Lorg/w3c/dom/smil/SMILDocument;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 170
    const-string/jumbo v18, "head"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/smil/SMILElement;

    .line 171
    .local v8, "head":Lorg/w3c/dom/smil/SMILElement;
    invoke-interface {v15, v8}, Lorg/w3c/dom/smil/SMILElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 173
    const-string/jumbo v18, "layout"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/smil/SMILLayoutElement;

    .line 174
    .local v11, "layout":Lorg/w3c/dom/smil/SMILLayoutElement;
    invoke-interface {v8, v11}, Lorg/w3c/dom/smil/SMILElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 177
    const-string/jumbo v18, "body"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Lorg/w3c/dom/smil/SMILDocument;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    check-cast v3, Lorg/w3c/dom/smil/SMILElement;

    .line 178
    .local v3, "body":Lorg/w3c/dom/smil/SMILElement;
    invoke-interface {v15, v3}, Lorg/w3c/dom/smil/SMILElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 179
    invoke-static {v5}, Lcom/android/DomParser/dom/model/SmilHelper;->addPar(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILParElement;

    move-result-object v12

    .line 182
    .local v12, "par":Lorg/w3c/dom/smil/SMILParElement;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/mms/pdu/PduBody;->getPartsNum()I

    move-result v14

    .line 183
    .local v14, "partsNum":I
    if-nez v14, :cond_1

    .line 231
    :cond_0
    return-object v5

    .line 187
    :cond_1
    const/4 v7, 0x0

    .line 188
    .local v7, "hasText":Z
    const/4 v6, 0x0

    .line 189
    .local v6, "hasMedia":Z
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v14, :cond_0

    .line 191
    sget-boolean v18, Lcom/android/DomParser/dom/model/SmilHelper;->isMixedSmil:Z

    if-eqz v18, :cond_6

    if-nez v6, :cond_2

    if-eqz v7, :cond_6

    .line 192
    :cond_2
    invoke-static {v5}, Lcom/android/DomParser/dom/model/SmilHelper;->addPar(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILParElement;

    move-result-object v12

    .line 201
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/mms/pdu/PduBody;->getPart(I)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v13

    .line 202
    .local v13, "part":Lcom/google/android/mms/pdu/PduPart;
    new-instance v4, Ljava/lang/String;

    invoke-virtual {v13}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V

    .line 204
    .local v4, "contentType":Ljava/lang/String;
    const-string/jumbo v18, "text/plain"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 205
    const-string/jumbo v18, "application/vnd.wap.xhtml+xml"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 206
    const-string/jumbo v18, "text/html"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 207
    :cond_4
    const-string/jumbo v18, "text"

    .line 208
    invoke-virtual {v13}, Lcom/google/android/mms/pdu/PduPart;->generateLocation()Ljava/lang/String;

    move-result-object v19

    .line 207
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v5, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->createMediaElement(Ljava/lang/String;Lorg/w3c/dom/smil/SMILDocument;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v16

    .line 209
    .local v16, "textElement":Lorg/w3c/dom/smil/SMILMediaElement;
    move-object/from16 v0, v16

    invoke-interface {v12, v0}, Lorg/w3c/dom/smil/SMILParElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 210
    const/4 v7, 0x1

    .line 189
    .end local v16    # "textElement":Lorg/w3c/dom/smil/SMILMediaElement;
    :cond_5
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 195
    .end local v4    # "contentType":Ljava/lang/String;
    .end local v13    # "part":Lcom/google/android/mms/pdu/PduPart;
    :cond_6
    if-eqz v12, :cond_7

    if-eqz v6, :cond_3

    if-eqz v7, :cond_3

    .line 196
    :cond_7
    invoke-static {v5}, Lcom/android/DomParser/dom/model/SmilHelper;->addPar(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILParElement;

    move-result-object v12

    .line 197
    const/4 v7, 0x0

    .line 198
    const/4 v6, 0x0

    goto :goto_1

    .line 211
    .restart local v4    # "contentType":Ljava/lang/String;
    .restart local v13    # "part":Lcom/google/android/mms/pdu/PduPart;
    :cond_8
    invoke-static {v4}, Lcom/google/android/mms/ContentType;->isImageType(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 212
    const-string/jumbo v18, "img"

    .line 213
    invoke-virtual {v13}, Lcom/google/android/mms/pdu/PduPart;->generateLocation()Ljava/lang/String;

    move-result-object v19

    .line 212
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v5, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->createMediaElement(Ljava/lang/String;Lorg/w3c/dom/smil/SMILDocument;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v10

    .line 214
    .local v10, "imageElement":Lorg/w3c/dom/smil/SMILMediaElement;
    invoke-interface {v12, v10}, Lorg/w3c/dom/smil/SMILParElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 215
    const/4 v6, 0x1

    .line 216
    goto :goto_2

    .end local v10    # "imageElement":Lorg/w3c/dom/smil/SMILMediaElement;
    :cond_9
    invoke-static {v4}, Lcom/google/android/mms/ContentType;->isVideoType(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 217
    const-string/jumbo v18, "video"

    .line 218
    invoke-virtual {v13}, Lcom/google/android/mms/pdu/PduPart;->generateLocation()Ljava/lang/String;

    move-result-object v19

    .line 217
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v5, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->createMediaElement(Ljava/lang/String;Lorg/w3c/dom/smil/SMILDocument;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v17

    .line 219
    .local v17, "videoElement":Lorg/w3c/dom/smil/SMILMediaElement;
    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Lorg/w3c/dom/smil/SMILParElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 220
    const/4 v6, 0x1

    .line 221
    goto :goto_2

    .end local v17    # "videoElement":Lorg/w3c/dom/smil/SMILMediaElement;
    :cond_a
    invoke-static {v4}, Lcom/google/android/mms/ContentType;->isAudioType(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 222
    const-string/jumbo v18, "audio"

    .line 223
    invoke-virtual {v13}, Lcom/google/android/mms/pdu/PduPart;->generateLocation()Ljava/lang/String;

    move-result-object v19

    .line 222
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v5, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->createMediaElement(Ljava/lang/String;Lorg/w3c/dom/smil/SMILDocument;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v2

    .line 224
    .local v2, "audioElement":Lorg/w3c/dom/smil/SMILMediaElement;
    invoke-interface {v12, v2}, Lorg/w3c/dom/smil/SMILParElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 225
    const/4 v6, 0x1

    goto :goto_2
.end method

.method public static escapeXML(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 155
    const-string/jumbo v0, "&"

    const-string/jumbo v1, "&amp;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "<"

    const-string/jumbo v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ">"

    const-string/jumbo v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    const-string/jumbo v1, "\""

    const-string/jumbo v2, "&quot;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\'"

    const-string/jumbo v2, "&apos;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    return-object v0
.end method

.method private static findRegionElementById(Ljava/util/ArrayList;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILRegionElement;
    .locals 3
    .param p1, "rId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/smil/SMILRegionElement;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lorg/w3c/dom/smil/SMILRegionElement;"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "smilRegions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/smil/SMILRegionElement;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 241
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 236
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/smil/SMILRegionElement;

    .line 237
    .local v0, "smilRegion":Lorg/w3c/dom/smil/SMILRegionElement;
    invoke-interface {v0}, Lorg/w3c/dom/smil/SMILRegionElement;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private static findSmilPart(Lcom/google/android/mms/pdu/PduBody;)Lcom/google/android/mms/pdu/PduPart;
    .locals 5
    .param p0, "body"    # Lcom/google/android/mms/pdu/PduBody;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/mms/pdu/PduBody;->getPartsNum()I

    move-result v2

    .line 105
    .local v2, "partNum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 112
    const/4 v1, 0x0

    :cond_0
    return-object v1

    .line 106
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/mms/pdu/PduBody;->getPart(I)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v1

    .line 107
    .local v1, "part":Lcom/google/android/mms/pdu/PduPart;
    invoke-virtual {v1}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v3

    const-string/jumbo v4, "application/smil"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;
    .locals 3
    .param p0, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-static {p0}, Lcom/android/DomParser/dom/model/SmilHelper;->findSmilPart(Lcom/google/android/mms/pdu/PduBody;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v1

    .line 80
    .local v1, "smilPart":Lcom/google/android/mms/pdu/PduPart;
    const/4 v0, 0x0

    .line 83
    .local v0, "document":Lorg/w3c/dom/smil/SMILDocument;
    if-nez v1, :cond_0

    .line 84
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/DomParser/dom/model/SmilHelper;->isMixedSmil:Z

    .line 86
    :cond_0
    if-eqz v1, :cond_1

    .line 87
    invoke-static {v1}, Lcom/android/DomParser/dom/model/SmilHelper;->getSmilDocument(Lcom/google/android/mms/pdu/PduPart;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v0

    .line 90
    :cond_1
    if-nez v0, :cond_2

    .line 92
    invoke-static {p0, p1}, Lcom/android/DomParser/dom/model/SmilHelper;->createSmilDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v0

    .line 95
    :cond_2
    return-object v0
.end method

.method private static getSmilDocument(Lcom/google/android/mms/pdu/PduPart;)Lorg/w3c/dom/smil/SMILDocument;
    .locals 4
    .param p0, "smilPart"    # Lcom/google/android/mms/pdu/PduPart;

    .prologue
    .line 127
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v1

    .line 130
    .local v1, "data":[B
    if-eqz v1, :cond_0

    .line 131
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 132
    .local v0, "bais":Ljava/io/ByteArrayInputStream;
    new-instance v3, Lcom/android/DomParser/dom/smil/parser/SmilXmlParser;

    invoke-direct {v3}, Lcom/android/DomParser/dom/smil/parser/SmilXmlParser;-><init>()V

    invoke-virtual {v3, v0}, Lcom/android/DomParser/dom/smil/parser/SmilXmlParser;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v2

    .line 133
    .local v2, "document":Lorg/w3c/dom/smil/SMILDocument;
    invoke-static {v2}, Lcom/android/DomParser/dom/model/SmilHelper;->validate(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILDocument;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 139
    .end local v0    # "bais":Ljava/io/ByteArrayInputStream;
    .end local v1    # "data":[B
    .end local v2    # "document":Lorg/w3c/dom/smil/SMILDocument;
    :goto_0
    return-object v3

    .line 137
    :catch_0
    move-exception v3

    .line 139
    :cond_0
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v3

    goto :goto_1

    .line 135
    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method private static setRegion(Lorg/w3c/dom/smil/SMILRegionMediaElement;Ljava/util/ArrayList;Lorg/w3c/dom/smil/SMILLayoutElement;Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "srme"    # Lorg/w3c/dom/smil/SMILRegionMediaElement;
    .param p2, "smilLayout"    # Lorg/w3c/dom/smil/SMILLayoutElement;
    .param p3, "regionId"    # Ljava/lang/String;
    .param p4, "regionPresentInLayout"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/smil/SMILRegionMediaElement;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/smil/SMILRegionElement;",
            ">;",
            "Lorg/w3c/dom/smil/SMILLayoutElement;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "smilRegions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/smil/SMILRegionElement;>;"
    invoke-static {p1, p3}, Lcom/android/DomParser/dom/model/SmilHelper;->findRegionElementById(Ljava/util/ArrayList;Ljava/lang/String;)Lorg/w3c/dom/smil/SMILRegionElement;

    move-result-object v0

    .line 248
    .local v0, "smilRegion":Lorg/w3c/dom/smil/SMILRegionElement;
    if-nez p4, :cond_0

    if-eqz v0, :cond_0

    .line 249
    invoke-interface {p0, v0}, Lorg/w3c/dom/smil/SMILRegionMediaElement;->setRegion(Lorg/w3c/dom/smil/SMILRegionElement;)V

    .line 250
    invoke-interface {p2, v0}, Lorg/w3c/dom/smil/SMILLayoutElement;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 251
    const/4 v1, 0x1

    .line 253
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static validate(Lorg/w3c/dom/smil/SMILDocument;)Lorg/w3c/dom/smil/SMILDocument;
    .locals 0
    .param p0, "in"    # Lorg/w3c/dom/smil/SMILDocument;

    .prologue
    .line 117
    return-object p0
.end method
