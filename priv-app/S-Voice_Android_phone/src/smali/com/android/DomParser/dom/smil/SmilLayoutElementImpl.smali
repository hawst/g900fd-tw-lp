.class public Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;
.super Lcom/android/DomParser/dom/smil/SmilElementImpl;
.source "SmilLayoutElementImpl.java"

# interfaces
.implements Lorg/w3c/dom/smil/SMILLayoutElement;


# direct methods
.method constructor <init>(Lcom/android/DomParser/dom/smil/SmilDocumentImpl;Ljava/lang/String;)V
    .locals 0
    .param p1, "owner"    # Lcom/android/DomParser/dom/smil/SmilDocumentImpl;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/android/DomParser/dom/smil/SmilElementImpl;-><init>(Lcom/android/DomParser/dom/smil/SmilDocumentImpl;Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getRegions()Lorg/w3c/dom/NodeList;
    .locals 1

    .prologue
    .line 39
    const-string/jumbo v0, "region"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    return-object v0
.end method

.method public getResolved()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public getRootLayout()Lorg/w3c/dom/smil/SMILRootLayoutElement;
    .locals 6

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 44
    .local v0, "childNodes":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .line 45
    .local v3, "rootLayoutNode":Lorg/w3c/dom/smil/SMILRootLayoutElement;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 46
    .local v1, "childrenCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_1

    .line 51
    if-nez v3, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v4

    const-string/jumbo v5, "root-layout"

    invoke-interface {v4, v5}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    .end local v3    # "rootLayoutNode":Lorg/w3c/dom/smil/SMILRootLayoutElement;
    check-cast v3, Lorg/w3c/dom/smil/SMILRootLayoutElement;

    .line 54
    .restart local v3    # "rootLayoutNode":Lorg/w3c/dom/smil/SMILRootLayoutElement;
    invoke-virtual {p0, v3}, Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 56
    :cond_0
    return-object v3

    .line 47
    :cond_1
    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "root-layout"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    invoke-interface {v0, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .end local v3    # "rootLayoutNode":Lorg/w3c/dom/smil/SMILRootLayoutElement;
    check-cast v3, Lorg/w3c/dom/smil/SMILRootLayoutElement;

    .line 46
    .restart local v3    # "rootLayoutNode":Lorg/w3c/dom/smil/SMILRootLayoutElement;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0}, Lcom/android/DomParser/dom/smil/SmilLayoutElementImpl;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
