.class public Lcom/samsung/chatbot/ChatbotFilter;
.super Ljava/lang/Object;
.source "ChatbotFilter.java"


# static fields
.field static final CHATBOTFILTER_FAILURE:I = 0x1

.field static final CHATBOTFILTER_SUCCEESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native initializeChatbotFilter()I
.end method

.method private native runChatbotFilter([SII)I
.end method

.method private native shutdownChatbotFilter()I
.end method


# virtual methods
.method public initialize(Ljava/lang/String;)I
    .locals 2
    .param p1, "szNativeLibraryPath"    # Ljava/lang/String;

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "libchatbotfilter.so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/samsung/chatbot/ChatbotFilter;->initializeChatbotFilter()I

    move-result v0

    return v0
.end method

.method public run([SII)I
    .locals 1
    .param p1, "asPCMBuffer"    # [S
    .param p2, "iLength"    # I
    .param p3, "iModeIndex"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/chatbot/ChatbotFilter;->runChatbotFilter([SII)I

    move-result v0

    return v0
.end method

.method public shutdown()I
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/chatbot/ChatbotFilter;->shutdownChatbotFilter()I

    move-result v0

    return v0
.end method
