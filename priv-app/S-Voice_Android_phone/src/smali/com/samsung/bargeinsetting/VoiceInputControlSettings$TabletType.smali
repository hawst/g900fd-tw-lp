.class final enum Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;
.super Ljava/lang/Enum;
.source "VoiceInputControlSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/bargeinsetting/VoiceInputControlSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TabletType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

.field public static final enum OTHERS:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

.field public static final enum SANTOS_10:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    new-instance v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    const-string/jumbo v1, "SANTOS_10"

    invoke-direct {v0, v1, v2}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    new-instance v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    const-string/jumbo v1, "OTHERS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    sget-object v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->$VALUES:[Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-class v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->$VALUES:[Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    invoke-virtual {v0}, [Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    return-object v0
.end method
