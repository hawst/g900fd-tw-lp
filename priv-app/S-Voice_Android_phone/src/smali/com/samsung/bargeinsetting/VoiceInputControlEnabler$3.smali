.class Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;
.super Ljava/lang/Object;
.source "VoiceInputControlEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

.field final synthetic val$check:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    iput-object p2, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;->val$check:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v3, 0x1

    .line 120
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;->val$check:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    # getter for: Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->access$100(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "voiceinputcontrol_showNeverAgain"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 122
    :cond_0
    const-string/jumbo v0, "temp_input_voice_control"

    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    # getter for: Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->access$100(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 123
    return-void
.end method
