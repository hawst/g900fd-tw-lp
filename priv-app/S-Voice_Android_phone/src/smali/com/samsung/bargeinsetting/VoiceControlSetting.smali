.class public Lcom/samsung/bargeinsetting/VoiceControlSetting;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "VoiceControlSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;,
        Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "VoiceControlSetting"

.field public static anim:Landroid/widget/ImageView;

.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private actionBarAll:Landroid/view/View;

.field private actionBarSwitch:Landroid/widget/Switch;

.field activity:Landroid/app/Activity;

.field private currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

.field private desc:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private helpBubble:Landroid/widget/TextView;

.field private listView:Landroid/widget/ListView;

.field private mBlinkAnimation:Landroid/view/animation/Animation;

.field mCloseActivityHandler:Landroid/os/Handler;

.field private mListMainLayout:Landroid/widget/RelativeLayout;

.field private mToolTipScaleAnimation:Landroid/view/animation/Animation;

.field private title:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private toolTipHelpLayout:Landroid/widget/RelativeLayout;

.field private voiceControlListAdapter:Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 82
    sget-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    .line 195
    new-instance v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;

    invoke-direct {v0, p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting;)V

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mCloseActivityHandler:Landroid/os/Handler;

    .line 395
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mBlinkAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mToolTipScaleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/bargeinsetting/VoiceControlSetting;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->processCheckUI(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method private isChatOnVInstalled()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 223
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 226
    .local v0, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string/jumbo v2, "com.coolots.chaton"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :goto_0
    return v1

    .line 229
    :catch_0
    move-exception v2

    .line 237
    :try_start_1
    const-string/jumbo v2, "com.coolots.chatonforcanada"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 240
    :catch_1
    move-exception v1

    .line 246
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private processCheckUI(Z)V
    .locals 10
    .param p1, "isChecked"    # Z

    .prologue
    const/4 v7, 0x4

    const/high16 v9, 0x41b00000    # 22.0f

    const/high16 v8, 0x41900000    # 18.0f

    .line 265
    sget-object v6, Lcom/samsung/bargeinsetting/VoiceControlSetting;->anim:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->clearAnimation()V

    .line 266
    sget-object v6, Lcom/samsung/bargeinsetting/VoiceControlSetting;->anim:Landroid/widget/ImageView;

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    iget-object v6, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 268
    iget-object v6, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 270
    sget v6, Lcom/vlingo/midas/R$id;->ListView:I

    invoke-virtual {p0, v6}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 272
    .local v2, "lv":Landroid/widget/ListView;
    if-eqz p1, :cond_3

    .line 273
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 274
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 276
    .local v5, "view":Landroid/view/View;
    if-nez v5, :cond_1

    .line 339
    .end local v5    # "view":Landroid/view/View;
    :cond_0
    :goto_1
    return-void

    .line 279
    .restart local v5    # "view":Landroid/view/View;
    :cond_1
    iget-object v6, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    sget-object v7, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    if-ne v6, v7, :cond_2

    .line 280
    sget v6, Lcom/vlingo/midas/R$id;->titleText:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 281
    .local v3, "tv1":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->titleDesc:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 283
    .local v4, "tv2":Landroid/widget/TextView;
    const-string/jumbo v6, "#000000"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 284
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 285
    const-string/jumbo v6, "#364f67"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 286
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextSize(F)V

    .line 273
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 288
    .end local v3    # "tv1":Landroid/widget/TextView;
    .end local v4    # "tv2":Landroid/widget/TextView;
    :cond_2
    sget v6, Lcom/vlingo/midas/R$id;->titleText:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 289
    .restart local v3    # "tv1":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->titleDesc:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 290
    .restart local v4    # "tv2":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->myPlacecheckBox:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 292
    .local v0, "check":Landroid/widget/CheckBox;
    const-string/jumbo v6, "#ffffff"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 293
    const-string/jumbo v6, "#afbcc2"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 294
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_2

    .line 299
    .end local v0    # "check":Landroid/widget/CheckBox;
    .end local v1    # "i":I
    .end local v3    # "tv1":Landroid/widget/TextView;
    .end local v4    # "tv2":Landroid/widget/TextView;
    .end local v5    # "view":Landroid/view/View;
    :cond_3
    iget-object v6, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    sget-object v7, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    if-ne v6, v7, :cond_8

    .line 300
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 301
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 303
    .restart local v5    # "view":Landroid/view/View;
    if-nez v5, :cond_5

    .line 335
    .end local v5    # "view":Landroid/view/View;
    :cond_4
    iget-object v6, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mCloseActivityHandler:Landroid/os/Handler;

    const/4 v7, 0x2

    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 305
    .restart local v5    # "view":Landroid/view/View;
    :cond_5
    sget v6, Lcom/vlingo/midas/R$id;->titleText:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 306
    .restart local v3    # "tv1":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->titleDesc:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 307
    .restart local v4    # "tv2":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->myPlacecheckBox:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 310
    .restart local v0    # "check":Landroid/widget/CheckBox;
    if-eqz v3, :cond_6

    .line 311
    const-string/jumbo v6, "#999999"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 312
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 315
    :cond_6
    if-eqz v4, :cond_7

    .line 316
    const-string/jumbo v6, "#999999"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 317
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextSize(F)V

    .line 300
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 321
    .end local v0    # "check":Landroid/widget/CheckBox;
    .end local v1    # "i":I
    .end local v3    # "tv1":Landroid/widget/TextView;
    .end local v4    # "tv2":Landroid/widget/TextView;
    .end local v5    # "view":Landroid/view/View;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 322
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 324
    .restart local v5    # "view":Landroid/view/View;
    if-eqz v5, :cond_4

    .line 326
    sget v6, Lcom/vlingo/midas/R$id;->titleText:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 327
    .restart local v3    # "tv1":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->titleDesc:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 328
    .restart local v4    # "tv2":Landroid/widget/TextView;
    sget v6, Lcom/vlingo/midas/R$id;->myPlacecheckBox:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 330
    .restart local v0    # "check":Landroid/widget/CheckBox;
    const-string/jumbo v6, "#666666"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 331
    const-string/jumbo v6, "#666666"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 332
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_4
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->help_invalid_action:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 553
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 557
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 558
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    sget v13, Lcom/vlingo/midas/R$anim;->blinker:I

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mBlinkAnimation:Landroid/view/animation/Animation;

    .line 88
    sget v13, Lcom/vlingo/midas/R$style;->actionBarStyle:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->setTheme(I)V

    .line 89
    sget v13, Lcom/vlingo/midas/R$layout;->voice_control:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->setContentView(I)V

    .line 90
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    .line 91
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    .line 93
    sget v13, Lcom/vlingo/midas/R$id;->toolTipHelpLayout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;

    .line 94
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getWindow()Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v10

    .line 95
    .local v10, "v":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 97
    sget v13, Lcom/vlingo/midas/R$id;->help_popup_bubble_voice_control:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->helpBubble:Landroid/widget/TextView;

    .line 98
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->helpBubble:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->help_tooltip_voice_tap:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v9

    .line 102
    .local v9, "titlebar":Landroid/app/ActionBar;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 103
    if-eqz v9, :cond_0

    .line 104
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 111
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$layout;->help_anim_switch:I

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarAll:Landroid/view/View;

    .line 113
    sget v13, Lcom/vlingo/midas/R$anim;->scale_tool_tip:I

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mToolTipScaleAnimation:Landroid/view/animation/Animation;

    .line 115
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    .line 116
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v13}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 118
    .local v6, "padding":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarAll:Landroid/view/View;

    sget v14, Lcom/vlingo/midas/R$id;->switch1:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v14, v15, v6, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v13}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    const/16 v14, 0x1e

    invoke-virtual {v13, v14}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 124
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v13}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarAll:Landroid/view/View;

    new-instance v15, Landroid/app/ActionBar$LayoutParams;

    const/16 v16, -0x2

    const/16 v17, -0x2

    const/16 v18, 0x15

    invoke-direct/range {v15 .. v18}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v13, v14, v15}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v13}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_title:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 132
    sget v13, Lcom/vlingo/midas/R$id;->imageViewAnim:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    sput-object v13, Lcom/samsung/bargeinsetting/VoiceControlSetting;->anim:Landroid/widget/ImageView;

    .line 133
    sget v13, Lcom/vlingo/midas/R$id;->switch1:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Switch;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;

    .line 134
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v13}, Landroid/widget/Switch;->requestFocus()Z

    .line 139
    sget v13, Lcom/vlingo/midas/R$id;->ListView:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->listView:Landroid/widget/ListView;

    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasDialing()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 142
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_incomming_calls:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_incomming_calls_summary:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_alarm:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_alarm_summary:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_camera:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    sget v14, Lcom/vlingo/midas/R$string;->voice_input_control_camera_summary:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_music:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/vlingo/midas/R$string;->voice_input_control_music_summary:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v13, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->title:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v13, v0, v1, v14, v15}, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting;Lcom/samsung/bargeinsetting/VoiceControlSetting;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->voiceControlListAdapter:Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;

    .line 157
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->listView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->voiceControlListAdapter:Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;

    invoke-virtual {v13, v14}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->listView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 160
    sget v13, Lcom/vlingo/midas/R$id;->listMainLayout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->mListMainLayout:Landroid/widget/RelativeLayout;

    .line 164
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v13

    sget-object v14, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-eq v13, v14, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v13

    sget-object v14, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_LOW_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v13, v14, :cond_3

    .line 165
    :cond_2
    sget-object v13, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    .line 170
    :cond_3
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getWindow()Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 171
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "privateFlags"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 172
    .local v7, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    .line 173
    .local v8, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-string/jumbo v14, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 175
    .local v3, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .local v2, "currentPrivateFlags":I
    const/4 v12, 0x0

    .local v12, "valueofFlagsEnableStatusBar":I
    const/4 v11, 0x0

    .line 176
    .local v11, "valueofFlagsDisableTray":I
    invoke-virtual {v7, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 177
    invoke-virtual {v8, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v12

    .line 178
    invoke-virtual {v3, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v11

    .line 180
    or-int/2addr v2, v12

    .line 181
    or-int/2addr v2, v11

    .line 183
    invoke-virtual {v7, v5, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getWindow()Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v2    # "currentPrivateFlags":I
    .end local v3    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v5    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v7    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v8    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v11    # "valueofFlagsDisableTray":I
    .end local v12    # "valueofFlagsEnableStatusBar":I
    :goto_1
    return-void

    .line 107
    .end local v6    # "padding":I
    :cond_4
    if-eqz v9, :cond_0

    .line 108
    const/16 v13, 0xe

    invoke-virtual {v9, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_0

    .line 188
    .restart local v6    # "padding":I
    :catch_0
    move-exception v4

    .line 191
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 185
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v13

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->help_invalid_action:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 545
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 251
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 261
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 253
    :pswitch_0
    sget v0, Lcom/vlingo/midas/R$string;->help_invalid_action:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 343
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 344
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 375
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 376
    .local v0, "titlebar":Landroid/app/ActionBar;
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 378
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->voice_input_control_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 386
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;

    new-instance v2, Lcom/samsung/bargeinsetting/VoiceControlSetting$2;

    invoke-direct {v2, p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$2;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 392
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->processCheckUI(Z)V

    .line 393
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 533
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->help_invalid_action:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 536
    return v2
.end method
