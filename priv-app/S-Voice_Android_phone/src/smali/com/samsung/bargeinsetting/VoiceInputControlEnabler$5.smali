.class Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;
.super Ljava/lang/Object;
.source "VoiceInputControlEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;


# direct methods
.method constructor <init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 145
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 146
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 147
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    # getter for: Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->access$000(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 148
    const-string/jumbo v1, "temp_input_voice_control"

    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;->this$0:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    # getter for: Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->access$100(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-static {v1, v0}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 149
    const/4 v0, 0x1

    .line 151
    :cond_0
    return v0
.end method
