.class public Lcom/samsung/bargeinsetting/UpdatePreferenceReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UpdatePreferenceReceiver.java"


# static fields
.field public static final DB_KEY_DRIVING_MODE_ON:Ljava/lang/String; = "driving_mode_on"

.field public static final DRIVING_MODE_ALARM_NOTIFICATION:Ljava/lang/String; = "driving_mode_alarm_notification"

.field private static final DRIVING_MODE_CHATON_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_chaton_call_notification"

.field public static final DRIVING_MODE_INCOMING_CALL_NOTIFICATION:Ljava/lang/String; = "driving_mode_incoming_call_notification"

.field public static final DRIVING_MODE_MESSAGE_NOTIFICATION:Ljava/lang/String; = "driving_mode_message_notification"

.field public static final DRIVING_MODE_SCHEDULE_NOTIFICATION:Ljava/lang/String; = "driving_mode_schedule_notification"

.field private static final NOT_SET:I = -0x1

.field private static final VOICEINPUTCONTROL_ALARM:Ljava/lang/String; = "voice_input_control_alarm"

.field private static final VOICEINPUTCONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_camera"

.field private static final VOICEINPUTCONTROL_CHATONV:Ljava/lang/String; = "voice_input_control_chatonv"

.field private static final VOICEINPUTCONTROL_INCOMMING_CALL:Ljava/lang/String; = "voice_input_control_incomming_calls"

.field private static final VOICEINPUTCONTROL_MUSIC:Ljava/lang/String; = "voice_input_control_music"

.field private static final VOICEINPUTCONTROL_RADIO:Ljava/lang/String; = "voice_input_control_radio"


# instance fields
.field private final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 13
    const-string/jumbo v0, "voice_input_control"

    iput-object v0, p0, Lcom/samsung/bargeinsetting/UpdatePreferenceReceiver;->KEY_VOICE_INPUT_CONTROL:Ljava/lang/String;

    return-void
.end method

.method private onDrivingModePreference(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v11, -0x1

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_on"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 96
    .local v1, "previousConfig_all":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_incoming_call_notification"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 97
    .local v3, "previousConfig_call":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_message_notification"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 98
    .local v4, "previousConfig_message":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_alarm_notification"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 99
    .local v0, "previousConfig_alarm":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_schedule_notification"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 100
    .local v5, "previousConfig_schedule":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_chaton_call_notification"

    invoke-static {v6, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 102
    .local v2, "previousConfig_cahtoncall":I
    if-ne v1, v11, :cond_0

    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_on"

    const-string/jumbo v6, "driving_mode_on"

    invoke-interface {p2, v6, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_6

    move v6, v7

    :goto_0
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 105
    :cond_0
    if-ne v3, v11, :cond_1

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_incoming_call_notification"

    const-string/jumbo v6, "driving_mode_incoming_call_notification"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_7

    move v6, v7

    :goto_1
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 108
    :cond_1
    if-ne v4, v11, :cond_2

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_message_notification"

    const-string/jumbo v6, "driving_mode_message_notification"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_8

    move v6, v7

    :goto_2
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 111
    :cond_2
    if-ne v0, v11, :cond_3

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_alarm_notification"

    const-string/jumbo v6, "driving_mode_alarm_notification"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_9

    move v6, v7

    :goto_3
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 114
    :cond_3
    if-ne v5, v11, :cond_4

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "driving_mode_schedule_notification"

    const-string/jumbo v6, "driving_mode_schedule_notification"

    invoke-interface {p2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_a

    move v6, v7

    :goto_4
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 117
    :cond_4
    if-ne v2, v11, :cond_5

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v9, "driving_mode_chaton_call_notification"

    const-string/jumbo v10, "driving_mode_chaton_call_notification"

    invoke-interface {p2, v10, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_b

    :goto_5
    invoke-static {v6, v9, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 120
    :cond_5
    return-void

    :cond_6
    move v6, v8

    .line 103
    goto :goto_0

    :cond_7
    move v6, v8

    .line 106
    goto :goto_1

    :cond_8
    move v6, v8

    .line 109
    goto :goto_2

    :cond_9
    move v6, v8

    .line 112
    goto :goto_3

    :cond_a
    move v6, v8

    .line 115
    goto :goto_4

    :cond_b
    move v7, v8

    .line 118
    goto :goto_5
.end method

.method private onVoiceControlPreference(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 50
    .local v1, "previousConfig_all":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_incomming_calls"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 51
    .local v2, "previousConfig_call":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_chatonv"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 52
    .local v4, "previousConfig_chatonv":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_alarm"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 53
    .local v0, "previousConfig_alarm":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_camera"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 54
    .local v3, "previousConfig_camera":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_music"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 55
    .local v5, "previousConfig_music":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string/jumbo v10, "voice_input_control_radio"

    const/4 v11, -0x1

    invoke-static {v9, v10, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 62
    .local v6, "previousConfig_radio":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 63
    const/4 v8, 0x1

    .line 64
    .local v8, "voice_control_master":I
    const/4 v7, 0x0

    .line 70
    .local v7, "voice_control_item":I
    :goto_0
    const/4 v9, -0x1

    if-ne v1, v9, :cond_0

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control"

    const-string/jumbo v12, "voice_input_control"

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    const/4 v9, 0x1

    :goto_1
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v9, 0x1

    :goto_2
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 73
    :cond_0
    const/4 v9, -0x1

    if-ne v2, v9, :cond_1

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_incomming_calls"

    const-string/jumbo v12, "voice_input_control_incomming_calls"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_a

    const/4 v9, 0x1

    :goto_3
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v9, 0x1

    :goto_4
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 76
    :cond_1
    const/4 v9, -0x1

    if-ne v4, v9, :cond_2

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_chatonv"

    const-string/jumbo v12, "voice_input_control_chatonv"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_c

    const/4 v9, 0x1

    :goto_5
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_d

    const/4 v9, 0x1

    :goto_6
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 79
    :cond_2
    const/4 v9, -0x1

    if-ne v0, v9, :cond_3

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_alarm"

    const-string/jumbo v12, "voice_input_control_alarm"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_e

    const/4 v9, 0x1

    :goto_7
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_f

    const/4 v9, 0x1

    :goto_8
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 82
    :cond_3
    const/4 v9, -0x1

    if-ne v3, v9, :cond_4

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_camera"

    const-string/jumbo v12, "voice_input_control_camera"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_10

    const/4 v9, 0x1

    :goto_9
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_11

    const/4 v9, 0x1

    :goto_a
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 85
    :cond_4
    const/4 v9, -0x1

    if-ne v5, v9, :cond_5

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_music"

    const-string/jumbo v12, "voice_input_control_music"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_12

    const/4 v9, 0x1

    :goto_b
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_13

    const/4 v9, 0x1

    :goto_c
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 88
    :cond_5
    const/4 v9, -0x1

    if-ne v6, v9, :cond_6

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string/jumbo v11, "voice_input_control_radio"

    const-string/jumbo v12, "voice_input_control_radio"

    const/4 v9, 0x1

    if-ne v7, v9, :cond_14

    const/4 v9, 0x1

    :goto_d
    invoke-interface {p2, v12, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_15

    const/4 v9, 0x1

    :goto_e
    invoke-static {v10, v11, v9}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 91
    :cond_6
    return-void

    .line 66
    .end local v7    # "voice_control_item":I
    .end local v8    # "voice_control_master":I
    :cond_7
    const/4 v8, 0x0

    .line 67
    .restart local v8    # "voice_control_master":I
    const/4 v7, 0x1

    .restart local v7    # "voice_control_item":I
    goto/16 :goto_0

    .line 71
    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 74
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 77
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_5

    :cond_d
    const/4 v9, 0x0

    goto/16 :goto_6

    .line 80
    :cond_e
    const/4 v9, 0x0

    goto :goto_7

    :cond_f
    const/4 v9, 0x0

    goto :goto_8

    .line 83
    :cond_10
    const/4 v9, 0x0

    goto :goto_9

    :cond_11
    const/4 v9, 0x0

    goto :goto_a

    .line 86
    :cond_12
    const/4 v9, 0x0

    goto :goto_b

    :cond_13
    const/4 v9, 0x0

    goto :goto_c

    .line 89
    :cond_14
    const/4 v9, 0x0

    goto :goto_d

    :cond_15
    const/4 v9, 0x0

    goto :goto_e
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 39
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-direct {p0, p1, v1}, Lcom/samsung/bargeinsetting/UpdatePreferenceReceiver;->onVoiceControlPreference(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 41
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDrivingModeSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    invoke-direct {p0, p1, v1}, Lcom/samsung/bargeinsetting/UpdatePreferenceReceiver;->onDrivingModePreference(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 45
    .end local v1    # "settings":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method
