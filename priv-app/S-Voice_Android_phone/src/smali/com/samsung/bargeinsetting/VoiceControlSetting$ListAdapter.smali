.class Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VoiceControlSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/bargeinsetting/VoiceControlSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field desc1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mCOntext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

.field title1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/bargeinsetting/VoiceControlSetting;Lcom/samsung/bargeinsetting/VoiceControlSetting;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "mainActivity"    # Lcom/samsung/bargeinsetting/VoiceControlSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/bargeinsetting/VoiceControlSetting;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 408
    .local p3, "title":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "desc":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->title1:Ljava/util/ArrayList;

    .line 398
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->desc1:Ljava/util/ArrayList;

    .line 410
    iput-object p3, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->title1:Ljava/util/ArrayList;

    .line 411
    iput-object p4, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->desc1:Ljava/util/ArrayList;

    .line 412
    iput-object p2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->mCOntext:Landroid/content/Context;

    .line 413
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->desc:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$400(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 422
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 427
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor",
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 436
    if-nez p2, :cond_1

    .line 438
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->mCOntext:Landroid/content/Context;

    sget v2, Lcom/vlingo/midas/R$layout;->voice_control_listitems:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 440
    new-instance v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;)V

    .line 441
    .local v0, "holder":Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;
    sget v1, Lcom/vlingo/midas/R$id;->titleText:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 443
    sget v1, Lcom/vlingo/midas/R$id;->myPlacecheckBox:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 445
    sget v1, Lcom/vlingo/midas/R$id;->titleDesc:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    .line 452
    :goto_0
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->currentTablet:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
    invoke-static {v1}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$500(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    move-result-object v1

    sget-object v2, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    if-ne v1, v2, :cond_0

    .line 453
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const-string/jumbo v2, "#999999"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 454
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const/high16 v2, 0x41b00000    # 22.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 456
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    const-string/jumbo v2, "#999999"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 457
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 459
    :cond_0
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 461
    iget-object v2, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->desc1:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v2, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->title1:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    iput p1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->position:I

    .line 466
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 468
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->actionBarSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$600(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 469
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const-string/jumbo v2, "#ffffff"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 470
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    const-string/jumbo v2, "#afbcc2"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 472
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 480
    :goto_1
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    new-instance v2, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$1;

    invoke-direct {v2, p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$1;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 493
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    new-instance v2, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$2;

    invoke-direct {v2, p0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$2;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$3;

    invoke-direct {v2, p0, v0}, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$3;-><init>(Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter;Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 520
    return-object p2

    .line 449
    .end local v0    # "holder":Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;
    goto/16 :goto_0

    .line 474
    :cond_2
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const-string/jumbo v2, "#666666"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 475
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->type:Landroid/widget/TextView;

    const-string/jumbo v2, "#666666"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 477
    iget-object v1, v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$ListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 404
    const/4 v0, 0x1

    return v0
.end method
