.class public Lcom/samsung/bargeinsetting/VoiceInputControlSettings;
.super Lcom/vlingo/midas/settings/VLPreferenceActivity;
.source "VoiceInputControlSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;
    }
.end annotation


# static fields
.field private static final VOICEINPUTCONTROL_ALARM:Ljava/lang/String; = "voice_input_control_alarm"

.field private static final VOICEINPUTCONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_camera"

.field private static final VOICEINPUTCONTROL_CHATONV:Ljava/lang/String; = "voice_input_control_chatonv"

.field private static final VOICEINPUTCONTROL_INCOMMING_CALL:Ljava/lang/String; = "voice_input_control_incomming_calls"

.field private static final VOICEINPUTCONTROL_MUSIC:Ljava/lang/String; = "voice_input_control_music"

.field private static final VOICEINPUTCONTROL_RADIO:Ljava/lang/String; = "voice_input_control_radio"

.field private static mTheme:I


# instance fields
.field private final KEY_EASY_MODE_SWITCH:Ljava/lang/String;

.field private final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String;

.field private actionBarSwitch:Landroid/widget/Switch;

.field private currentTablet:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

.field private mAlarm:Landroid/preference/CheckBoxPreference;

.field private mCamera:Landroid/preference/CheckBoxPreference;

.field private mChatonV:Landroid/preference/CheckBoxPreference;

.field private mDensity:F

.field private mIncommingCalls:Landroid/preference/CheckBoxPreference;

.field private mMusic:Landroid/preference/CheckBoxPreference;

.field private mOldLX:F

.field private mOldLY:F

.field private mOldPX:F

.field private mOldPY:F

.field private mOldX:F

.field private mOldY:F

.field private mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private mRadio:Landroid/preference/CheckBoxPreference;

.field private mVoiceInputControlEnabler:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

.field private mVoiceInputControlObserver:Landroid/database/ContentObserver;

.field private my_orientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;-><init>()V

    .line 44
    const-string/jumbo v0, "voice_input_control"

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->KEY_VOICE_INPUT_CONTROL:Ljava/lang/String;

    .line 45
    const-string/jumbo v0, "easy_mode_switch"

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->KEY_EASY_MODE_SWITCH:Ljava/lang/String;

    .line 59
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mDensity:F

    .line 61
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldX:F

    .line 62
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldY:F

    .line 63
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldPX:F

    .line 64
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldPY:F

    .line 66
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldLX:F

    .line 67
    iput v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mOldLY:F

    .line 71
    new-instance v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$1;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlSettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlObserver:Landroid/database/ContentObserver;

    .line 83
    sget-object v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->currentTablet:Lcom/samsung/bargeinsetting/VoiceInputControlSettings$TabletType;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/bargeinsetting/VoiceInputControlSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceInputControlSettings;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->updateUIVoiceInputControl()V

    return-void
.end method

.method private getCurrentSystemLanguage()Ljava/util/Locale;
    .locals 9

    .prologue
    .line 261
    :try_start_0
    const-string/jumbo v7, "android.app.ActivityManagerNative"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 262
    .local v1, "amnClass":Ljava/lang/Class;
    const/4 v0, 0x0

    .line 263
    .local v0, "amn":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 266
    .local v2, "config":Landroid/content/res/Configuration;
    const-string/jumbo v7, "getDefault"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 267
    .local v6, "methodGetDefault":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 268
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 271
    const-string/jumbo v7, "getConfiguration"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 272
    .local v5, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 273
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "config":Landroid/content/res/Configuration;
    check-cast v2, Landroid/content/res/Configuration;

    .line 274
    .restart local v2    # "config":Landroid/content/res/Configuration;
    iget-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    .end local v0    # "amn":Ljava/lang/Object;
    .end local v1    # "amnClass":Ljava/lang/Class;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetDefault":Ljava/lang/reflect/Method;
    .local v3, "currentLocale":Ljava/util/Locale;
    :goto_0
    return-object v3

    .line 276
    .end local v3    # "currentLocale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 278
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 279
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .restart local v3    # "currentLocale":Ljava/util/Locale;
    goto :goto_0
.end method

.method private isChatOnVInstalled()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 230
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 233
    .local v0, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string/jumbo v2, "com.coolots.chaton"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return v1

    .line 236
    :catch_0
    move-exception v2

    .line 244
    :try_start_1
    const-string/jumbo v2, "com.coolots.chatonforcanada"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 247
    :catch_1
    move-exception v1

    .line 253
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isEasyModeOn()I
    .locals 3

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "easy_mode_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private setPreferenceStatusBasedOnEasyMode(Landroid/preference/CheckBoxPreference;)V
    .locals 1
    .param p1, "mPref"    # Landroid/preference/CheckBoxPreference;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    if-ne v0, p1, :cond_0

    .line 357
    invoke-direct {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->isEasyModeOn()I

    move-result v0

    if-nez v0, :cond_1

    .line 358
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateUIVoiceInputControl()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 365
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 368
    .local v0, "voiceInputControState":I
    if-ne v0, v4, :cond_6

    .line 369
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 371
    :cond_0
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    .line 372
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_2

    .line 374
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 376
    :cond_2
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_3

    .line 377
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->setPreferenceStatusBasedOnEasyMode(Landroid/preference/CheckBoxPreference;)V

    .line 378
    :cond_3
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_4

    .line 379
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 380
    :cond_4
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_5

    .line 381
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 396
    :cond_5
    :goto_0
    return-void

    .line 383
    :cond_6
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_7

    .line 384
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 385
    :cond_7
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_8

    .line 386
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 387
    :cond_8
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_9

    .line 388
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 389
    :cond_9
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_a

    .line 390
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 391
    :cond_a
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_b

    .line 392
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 393
    :cond_b
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_5

    .line 394
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public isAllOptionDisabled()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 469
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_incomming_calls"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 470
    .local v1, "call":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_alarm"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 471
    .local v0, "alarm":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_camera"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 472
    .local v2, "camera":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_music"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 473
    .local v4, "music":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_radio"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 474
    .local v5, "radio":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voice_input_control_chatonv"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 476
    .local v3, "chatonv":I
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    if-nez v4, :cond_0

    if-nez v5, :cond_0

    if-nez v3, :cond_0

    .line 477
    const/4 v6, 0x1

    .line 479
    :cond_0
    return v6
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "desiredState"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 295
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control"

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 297
    invoke-direct {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->updateUIVoiceInputControl()V

    .line 299
    if-nez p2, :cond_1

    .line 300
    const-string/jumbo v0, "temp_input_voice_control"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    .line 303
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 295
    goto :goto_0

    .line 302
    :cond_1
    const-string/jumbo v0, "temp_input_voice_control"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 289
    if-eqz p1, :cond_0

    .line 290
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 291
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 293
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 30
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    new-instance v24, Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 90
    move-object/from16 v4, p0

    .line 93
    .local v4, "activity":Landroid/app/Activity;
    new-instance v12, Landroid/util/DisplayMetrics;

    invoke-direct {v12}, Landroid/util/DisplayMetrics;-><init>()V

    .line 94
    .local v12, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 95
    iget v0, v12, Landroid/util/DisplayMetrics;->density:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mDensity:F

    .line 97
    iget v0, v12, Landroid/util/DisplayMetrics;->density:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v24, v24, v26

    if-nez v24, :cond_0

    iget v0, v12, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v24, v0

    iget v0, v12, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v25, v0

    add-int v24, v24, v25

    const/16 v25, 0x7f0

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_0

    iget v0, v12, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v24, v0

    const v25, 0x4315d32c

    sub-float v24, v24, v25

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-nez v24, :cond_0

    iget v0, v12, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v24, v0

    const v25, 0x431684be

    sub-float v24, v24, v25

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-eqz v24, :cond_1

    :cond_0
    iget v0, v12, Landroid/util/DisplayMetrics;->density:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v24, v24, v26

    if-nez v24, :cond_b

    iget v0, v12, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v24, v0

    iget v0, v12, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v25, v0

    add-int v24, v24, v25

    const/16 v25, 0x820

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    iget v0, v12, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v24, v0

    const v25, 0x4315d2f2

    sub-float v24, v24, v25

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-nez v24, :cond_b

    iget v0, v12, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v24, v0

    const v25, 0x4316849c

    sub-float v24, v24, v25

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-nez v24, :cond_b

    .line 101
    :cond_1
    sget v24, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v24, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mTheme:I

    .line 102
    sget v24, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mTheme:I

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->setTheme(I)V

    .line 109
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string/jumbo v25, "voicetalk_language"

    invoke-static/range {v24 .. v25}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 110
    .local v7, "currentLocale":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v20

    .line 111
    .local v20, "target":Ljava/util/Locale;
    if-eqz v20, :cond_2

    .line 112
    invoke-virtual/range {v20 .. v20}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v19

    .line 113
    .local v19, "stringLanguage":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v18

    .line 115
    .local v18, "stringCountry":Ljava/lang/String;
    if-nez v7, :cond_c

    const-string/jumbo v24, "pt"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 116
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 117
    .local v15, "res":Landroid/content/res/Resources;
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 118
    .local v6, "conf":Landroid/content/res/Configuration;
    move-object/from16 v0, v20

    iput-object v0, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 119
    const/16 v24, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v15, v6, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 134
    .end local v6    # "conf":Landroid/content/res/Configuration;
    .end local v15    # "res":Landroid/content/res/Resources;
    .end local v18    # "stringCountry":Ljava/lang/String;
    .end local v19    # "stringLanguage":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x5f

    const/16 v26, 0x2d

    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v16

    .line 135
    .local v16, "rightArgs":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string/jumbo v25, "voicetalk_language"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 136
    sget v24, Lcom/vlingo/midas/R$xml;->voice_input_control_settings:I

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->addPreferencesFromResource(I)V

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    .line 141
    .local v21, "titlebar":Landroid/app/ActionBar;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v24

    if-eqz v24, :cond_d

    .line 142
    if-eqz v21, :cond_3

    .line 143
    const/16 v24, 0x8

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 150
    :cond_3
    :goto_2
    new-instance v24, Landroid/widget/Switch;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    .line 152
    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$dimen;->action_bar_switch_padding:I

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 155
    .local v13, "padding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v13, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Switch;->requestFocus()Z

    .line 157
    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v24

    const/16 v25, 0x1e

    invoke-virtual/range {v24 .. v25}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 160
    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    move-object/from16 v25, v0

    new-instance v26, Landroid/app/ActionBar$LayoutParams;

    const/16 v27, -0x2

    const/16 v28, -0x2

    const/16 v29, 0x15

    invoke-direct/range {v26 .. v29}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual/range {v24 .. v26}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 166
    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v24

    sget v25, Lcom/vlingo/midas/R$string;->voice_input_control_title:I

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getString(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/Display;->getRotation()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/Display;->getRotation()I

    move-result v24

    const/16 v25, 0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 172
    :cond_4
    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 175
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isChatONVPhone()Z

    move-result v24

    if-nez v24, :cond_e

    const/4 v5, 0x1

    .line 177
    .local v5, "chatONVHide1":Z
    :goto_3
    new-instance v24, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v4, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlEnabler:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    .line 178
    const-string/jumbo v24, "voice_input_control_incomming_calls"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    .line 179
    const-string/jumbo v24, "voice_input_control_chatonv"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    .line 180
    const-string/jumbo v24, "voice_input_control_alarm"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    .line 181
    const-string/jumbo v24, "voice_input_control_camera"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    .line 182
    const-string/jumbo v24, "voice_input_control_music"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    .line 183
    const-string/jumbo v24, "voice_input_control_radio"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v24

    check-cast v24, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    .line 185
    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasDialing()Z

    move-result v24

    if-nez v24, :cond_6

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string/jumbo v25, "voice_input_control_incomming_calls"

    const/16 v26, 0x0

    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 190
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v24, v0

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasRadio()Z

    move-result v24

    if-eqz v24, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasPenFeature()Z

    move-result v24

    if-nez v24, :cond_7

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isInCarFunctionSupported()Z

    move-result v24

    if-nez v24, :cond_8

    .line 191
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string/jumbo v25, "voice_input_control_radio"

    const/16 v26, 0x0

    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 195
    :cond_8
    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v5, v0, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->isChatOnVInstalled()Z

    move-result v24

    if-nez v24, :cond_a

    .line 196
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string/jumbo v25, "voice_input_control_chatonv"

    const/16 v26, 0x0

    invoke-static/range {v24 .. v26}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 202
    :cond_a
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getWindow()Landroid/view/Window;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v11

    .line 203
    .local v11, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v24

    const-string/jumbo v25, "privateFlags"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v14

    .line 204
    .local v14, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v24

    const-string/jumbo v25, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v17

    .line 205
    .local v17, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v24

    const-string/jumbo v25, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    .line 207
    .local v9, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v8, 0x0

    .local v8, "currentPrivateFlags":I
    const/16 v23, 0x0

    .local v23, "valueofFlagsEnableStatusBar":I
    const/16 v22, 0x0

    .line 208
    .local v22, "valueofFlagsDisableTray":I
    invoke-virtual {v14, v11}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 209
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v23

    .line 210
    invoke-virtual {v9, v11}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v22

    .line 212
    or-int v8, v8, v23

    .line 213
    or-int v8, v8, v22

    .line 215
    invoke-virtual {v14, v11, v8}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 216
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getWindow()Landroid/view/Window;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local v8    # "currentPrivateFlags":I
    .end local v9    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v11    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v14    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v17    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v22    # "valueofFlagsDisableTray":I
    .end local v23    # "valueofFlagsEnableStatusBar":I
    :goto_4
    return-void

    .line 104
    .end local v5    # "chatONVHide1":Z
    .end local v7    # "currentLocale":Ljava/lang/String;
    .end local v13    # "padding":I
    .end local v16    # "rightArgs":Ljava/lang/String;
    .end local v20    # "target":Ljava/util/Locale;
    .end local v21    # "titlebar":Landroid/app/ActionBar;
    :cond_b
    sget v24, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v24, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mTheme:I

    goto/16 :goto_0

    .line 121
    .restart local v7    # "currentLocale":Ljava/lang/String;
    .restart local v18    # "stringCountry":Ljava/lang/String;
    .restart local v19    # "stringLanguage":Ljava/lang/String;
    .restart local v20    # "target":Ljava/util/Locale;
    :cond_c
    const-string/jumbo v24, "Brazil"

    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v25

    const-string/jumbo v26, "ro.csc.country_code"

    invoke-virtual/range {v25 .. v26}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    if-eqz v7, :cond_2

    const-string/jumbo v24, "en-US"

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    const-string/jumbo v24, "pt"

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    const-string/jumbo v24, "BR"

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 126
    .restart local v15    # "res":Landroid/content/res/Resources;
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    .line 127
    .restart local v6    # "conf":Landroid/content/res/Configuration;
    move-object/from16 v0, v20

    iput-object v0, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 128
    const/16 v24, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v15, v6, v0}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto/16 :goto_1

    .line 146
    .end local v6    # "conf":Landroid/content/res/Configuration;
    .end local v15    # "res":Landroid/content/res/Resources;
    .end local v18    # "stringCountry":Ljava/lang/String;
    .end local v19    # "stringLanguage":Ljava/lang/String;
    .restart local v16    # "rightArgs":Ljava/lang/String;
    .restart local v21    # "titlebar":Landroid/app/ActionBar;
    :cond_d
    if-eqz v21, :cond_3

    .line 147
    const/16 v24, 0xe

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_2

    .line 175
    .restart local v13    # "padding":I
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 220
    .restart local v5    # "chatONVHide1":Z
    :catch_0
    move-exception v10

    .line 223
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 217
    .end local v10    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v24

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 454
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onDestroy()V

    .line 455
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 459
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 463
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 461
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->finish()V

    goto :goto_0

    .line 459
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 400
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPause()V

    .line 402
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 403
    .local v0, "state":I
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->isAllOptionDisabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 406
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 410
    :cond_0
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlEnabler:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    invoke-virtual {v1}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->pause()V

    .line 411
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 412
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 417
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 418
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_incomming_calls"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 438
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->isAllOptionDisabled()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 449
    :goto_2
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    move v0, v2

    .line 418
    goto :goto_0

    .line 421
    :cond_3
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 422
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_chatonv"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_3

    .line 424
    :cond_5
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 425
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_alarm"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_4
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_4

    .line 427
    :cond_7
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 428
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_camera"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_5
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto :goto_5

    .line 430
    :cond_9
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 431
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_music"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_6
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto :goto_6

    .line 433
    :cond_b
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_radio"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    :goto_7
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_1

    :cond_c
    move v0, v2

    goto :goto_7

    .line 446
    :cond_d
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    goto/16 :goto_2
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 307
    invoke-super {p0}, Lcom/vlingo/midas/settings/VLPreferenceActivity;->onResume()V

    .line 309
    invoke-direct {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->updateUIVoiceInputControl()V

    .line 310
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 311
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlEnabler:Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    invoke-virtual {v0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->resume()V

    .line 312
    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "voice_input_control"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mVoiceInputControlObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 317
    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mIncommingCalls:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "voice_input_control_incomming_calls"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 323
    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mChatonV:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "voice_input_control_chatonv"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    .line 328
    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mAlarm:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "voice_input_control_alarm"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    .line 333
    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mCamera:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "voice_input_control_camera"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 337
    :cond_3
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    .line 338
    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mMusic:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "voice_input_control_music"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 342
    :cond_4
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    .line 343
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->mRadio:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/samsung/bargeinsetting/VoiceInputControlSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "voice_input_control_radio"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_b

    :goto_5
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 346
    :cond_5
    return-void

    :cond_6
    move v0, v2

    .line 317
    goto :goto_0

    :cond_7
    move v0, v2

    .line 323
    goto :goto_1

    :cond_8
    move v0, v2

    .line 328
    goto :goto_2

    :cond_9
    move v0, v2

    .line 333
    goto :goto_3

    :cond_a
    move v0, v2

    .line 338
    goto :goto_4

    :cond_b
    move v1, v2

    .line 343
    goto :goto_5
.end method
