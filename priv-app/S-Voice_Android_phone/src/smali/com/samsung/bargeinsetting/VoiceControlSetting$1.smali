.class Lcom/samsung/bargeinsetting/VoiceControlSetting$1;
.super Landroid/os/Handler;
.source "VoiceControlSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/bargeinsetting/VoiceControlSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;


# direct methods
.method constructor <init>(Lcom/samsung/bargeinsetting/VoiceControlSetting;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 197
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 199
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    invoke-virtual {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->finish()V

    goto :goto_0

    .line 203
    :pswitch_1
    sget-object v2, Lcom/samsung/bargeinsetting/VoiceControlSetting;->anim:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    sget-object v2, Lcom/samsung/bargeinsetting/VoiceControlSetting;->anim:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->mBlinkAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$000(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 206
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 207
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v0, v2

    .line 209
    .local v0, "centreX":F
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v1, v2

    .line 211
    .local v1, "centreY":F
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setPivotX(F)V

    .line 212
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setPivotY(F)V

    .line 214
    iget-object v2, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->toolTipHelpLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$100(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/bargeinsetting/VoiceControlSetting$1;->this$0:Lcom/samsung/bargeinsetting/VoiceControlSetting;

    # getter for: Lcom/samsung/bargeinsetting/VoiceControlSetting;->mToolTipScaleAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/samsung/bargeinsetting/VoiceControlSetting;->access$200(Lcom/samsung/bargeinsetting/VoiceControlSetting;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
