.class public final Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;
.super Ljava/lang/Object;
.source "VoiceInputControlEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field private static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"


# instance fields
.field private mAutoHapticDialog:Landroid/app/AlertDialog;

.field private final mContext:Landroid/content/Context;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switch_"    # Landroid/widget/Switch;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    .line 43
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public dismissDialog()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 82
    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 13
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 85
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "voiceinputcontrol_showNeverAgain"

    invoke-static {v8, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 86
    .local v6, "showNeverAgain":I
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "voice_input_control"

    invoke-static {v8, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 88
    .local v7, "voiceState":I
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "voice_input_control"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 90
    if-eqz p2, :cond_0

    if-nez v7, :cond_0

    if-nez v6, :cond_0

    .line 91
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 92
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 93
    .local v3, "layout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 94
    .local v0, "check":Landroid/widget/CheckBox;
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/ui/PackageInfoProvider;->hasDialing()Z

    move-result v9

    if-eqz v9, :cond_2

    sget v9, Lcom/vlingo/midas/R$string;->voice_input_control_message:I

    :goto_0
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(I)V

    .line 100
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 101
    .local v1, "customWarning":Landroid/widget/TextView;
    sget v8, Lcom/vlingo/midas/R$string;->voice_input_control_message:I

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(I)V

    .line 106
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v5, v8, Landroid/util/DisplayMetrics;->density:F

    .line 107
    .local v5, "mDensity":F
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v8

    const/high16 v9, 0x41000000    # 8.0f

    mul-float/2addr v9, v5

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    add-int v4, v8, v9

    .line 108
    .local v4, "leftPadding":I
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v8

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v9

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v0, v4, v8, v9, v10}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 111
    new-instance v8, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$1;

    invoke-direct {v8, p0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$1;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)V

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v9, Lcom/vlingo/midas/R$string;->voice_input_control_title:I

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->voice_input_control_ok:I

    new-instance v10, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;

    invoke-direct {v10, p0, v0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$3;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    sget v9, Lcom/vlingo/midas/R$string;->voice_input_control_cancel:I

    new-instance v10, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$2;

    invoke-direct {v10, p0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$2;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    .line 136
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 137
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    new-instance v9, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$4;

    invoke-direct {v9, p0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$4;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)V

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 142
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mAutoHapticDialog:Landroid/app/AlertDialog;

    new-instance v9, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;

    invoke-direct {v9, p0}, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler$5;-><init>(Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;)V

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 157
    .end local v0    # "check":Landroid/widget/CheckBox;
    .end local v1    # "customWarning":Landroid/widget/TextView;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "layout":Landroid/view/View;
    .end local v4    # "leftPadding":I
    .end local v5    # "mDensity":F
    :cond_0
    if-eqz p2, :cond_3

    if-nez v7, :cond_3

    .line 158
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v8, v12}, Landroid/widget/Switch;->setChecked(Z)V

    .line 159
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "voice_input_control"

    invoke-static {v8, v9, v12}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 164
    :cond_1
    :goto_1
    return-void

    .line 94
    .restart local v0    # "check":Landroid/widget/CheckBox;
    .restart local v2    # "inflater":Landroid/view/LayoutInflater;
    .restart local v3    # "layout":Landroid/view/View;
    :cond_2
    sget v9, Lcom/vlingo/midas/R$string;->voice_input_control_message_no_call:I

    goto/16 :goto_0

    .line 160
    .end local v0    # "check":Landroid/widget/CheckBox;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "layout":Landroid/view/View;
    :cond_3
    if-nez p2, :cond_1

    if-ne v7, v12, :cond_1

    .line 161
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v8, v11}, Landroid/widget/Switch;->setChecked(Z)V

    .line 162
    iget-object v8, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string/jumbo v9, "voice_input_control"

    invoke-static {v8, v9, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 58
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 59
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "voice_input_control"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 54
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0
.end method

.method public setSwitch(Landroid/widget/Switch;)V
    .locals 5
    .param p1, "switch_"    # Landroid/widget/Switch;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    if-ne v1, p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 66
    iput-object p1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    .line 67
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 69
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "voice_input_control"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 71
    .local v0, "voiceTalkState":I
    if-ne v0, v4, :cond_1

    .line 72
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 74
    :cond_1
    iget-object v1, p0, Lcom/samsung/bargeinsetting/VoiceInputControlEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0
.end method
