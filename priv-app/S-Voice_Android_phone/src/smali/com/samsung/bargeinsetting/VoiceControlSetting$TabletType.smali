.class final enum Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
.super Ljava/lang/Enum;
.source "VoiceControlSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/bargeinsetting/VoiceControlSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TabletType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

.field public static final enum OTHERS:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

.field public static final enum SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    const-string/jumbo v1, "SANTOS_10"

    invoke-direct {v0, v1, v2}, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    new-instance v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    const-string/jumbo v1, "OTHERS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    sget-object v1, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->SANTOS_10:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->OTHERS:Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->$VALUES:[Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    const-class v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->$VALUES:[Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    invoke-virtual {v0}, [Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/bargeinsetting/VoiceControlSetting$TabletType;

    return-object v0
.end method
