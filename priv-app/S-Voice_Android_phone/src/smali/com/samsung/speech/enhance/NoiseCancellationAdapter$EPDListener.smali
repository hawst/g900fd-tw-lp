.class Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;
.super Ljava/lang/Object;
.source "NoiseCancellationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/speech/enhance/NoiseCancellationAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EPDListener"
.end annotation


# static fields
.field private static final END_SPEECH:I = 0x1

.field private static final END_nonSPEECH:I = 0x2

.field private static final KEEP_LISTENING:I


# instance fields
.field private DEFAULT_ENDPOINT_LONG_LONG:I

.field private DEFAULT_ENDPOINT_MEDIUM:I

.field private final HANGOVER_DEFAULT:I

.field private final HANGOVER_LONG:I

.field private final HANGOVER_SWITCH_LIMIT:I

.field private SpeechFrameCount:J

.field private samolesSilenceNoSpeechLimit:J

.field private samolesSilenceSpeechLimit:J

.field private final sampleRate:I

.field private samplesSilence:J

.field private speechWasDetected:Z

.field final synthetic this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

.field private use_dynamicHANGOVER:I


# direct methods
.method public constructor <init>(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;III)V
    .locals 10
    .param p2, "sampleRate"    # I
    .param p3, "speechEndpointTimeout"    # I
    .param p4, "noSpeechEndPointTimeout"    # I

    .prologue
    .line 307
    iput-object p1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    const/16 v6, 0x3e80

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->sampleRate:I

    .line 293
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    .line 295
    const/16 v6, 0x2ee

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_DEFAULT:I

    .line 296
    const/16 v6, 0x6d6

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_LONG:I

    .line 297
    const v6, 0xbb80

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->HANGOVER_SWITCH_LIMIT:I

    .line 303
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_MEDIUM:I

    .line 304
    const-string/jumbo v6, "endpoint.time.speech.long.msg"

    const/16 v7, 0x8ca

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_LONG_LONG:I

    .line 308
    int-to-long v6, p2

    int-to-long v8, p3

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 309
    int-to-long v6, p2

    int-to-long v8, p4

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    .line 310
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: DEFAULT_ENDPOINT_LONG_LONG is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_LONG_LONG:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->DEFAULT_ENDPOINT_LONG_LONG:I

    if-ne p3, v6, :cond_0

    .line 313
    const/4 v6, 0x1

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    .line 314
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: Using dynamic HANGOVER! ... speechEndpointTimeout is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :goto_0
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    .line 321
    const-string/jumbo v6, "endpoint.time.speech.short"

    const/16 v7, 0x190

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 322
    .local v5, "epdShort":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: SHORT = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const-string/jumbo v6, "endpoint.time.speech.medium"

    const/16 v7, 0x2ee

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 324
    .local v3, "epdMedium":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: MEDIUM = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const-string/jumbo v6, "endpoint.time.speech.medium.long"

    const/16 v7, 0x4e2

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 326
    .local v4, "epdMediumLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: MEDIUM_LONG = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 328
    .local v0, "epdLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: LONG = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const-string/jumbo v6, "endpoint.time.speech.long.msg"

    const/16 v7, 0x8ca

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 330
    .local v2, "epdLongLong":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: LONG_LONG = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getSmEndpointManager()Lcom/vlingo/core/internal/endpoints/EndpointManager;

    move-result-object v6

    sget-object v7, Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;->LONG:Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;

    const/16 v8, 0x6d6

    invoke-virtual {v6, v7, v8}, Lcom/vlingo/core/internal/endpoints/EndpointManager;->setSilenceDurationWithSpeech(Lcom/vlingo/core/internal/endpoints/WithSpeechSilenceDurationCategory;I)I

    .line 333
    const-string/jumbo v6, "endpoint.time.speech.long"

    const/16 v7, 0x6d6

    invoke-static {v6, v7}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 334
    .local v1, "epdLong1":I
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG1: LONG = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void

    .line 317
    .end local v0    # "epdLong":I
    .end local v1    # "epdLong1":I
    .end local v2    # "epdLongLong":I
    .end local v3    # "epdMedium":I
    .end local v4    # "epdMediumLong":I
    .end local v5    # "epdShort":I
    :cond_0
    const-string/jumbo v6, "VSG"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "VSG: Not using dynamic HANGOVER ... speechEndpointTimeout is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    goto/16 :goto_0
.end method


# virtual methods
.method public processValue(II)I
    .locals 10
    .param p1, "speechDetected"    # I
    .param p2, "samplesAnalyzed"    # I

    .prologue
    const-wide/32 v8, 0xbb80

    const-wide/16 v6, 0x640

    const/4 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v0, 0x1

    .line 338
    iget-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z
    invoke-static {v2}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$000(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 339
    iput-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    .line 340
    const/4 p1, 0x0

    .line 341
    iput-wide v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    .line 342
    iput-wide v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    .line 345
    :cond_0
    if-eqz p1, :cond_4

    .line 348
    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    .line 349
    iput-wide v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    .line 350
    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    int-to-long v4, p2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    .line 351
    const-string/jumbo v2, "VSG"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VSG: SpeechFrameCount is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->use_dynamicHANGOVER:I

    if-ne v2, v0, :cond_3

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->SpeechFrameCount:J

    cmp-long v0, v2, v8

    if-ltz v0, :cond_3

    .line 354
    const-wide/16 v2, 0x6d60

    iput-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 361
    :goto_0
    const-string/jumbo v0, "VSG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VSG: JAVA_HANGOVER is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move v0, v1

    .line 384
    :cond_2
    :goto_1
    return v0

    .line 358
    :cond_3
    const-wide/16 v2, 0x2ee0

    iput-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    goto :goto_0

    .line 364
    :cond_4
    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    int-to-long v4, p2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    .line 365
    iget-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->this$0:Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    # getter for: Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsTTSplay:Z
    invoke-static {v2}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->access$100(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-wide v8, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    .line 370
    :cond_5
    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    cmp-long v2, v2, v6

    if-gez v2, :cond_6

    iput-wide v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    .line 371
    :cond_6
    iget-boolean v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    if-eqz v2, :cond_7

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    iget-wide v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceSpeechLimit:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_8

    :cond_7
    iget-boolean v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samplesSilence:J

    iget-wide v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->samolesSilenceNoSpeechLimit:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 378
    :cond_8
    iget-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->speechWasDetected:Z

    if-nez v1, :cond_2

    .line 381
    const/4 v0, 0x2

    goto :goto_1
.end method
