.class public Lcom/samsung/speech/enhance/NoiseCancellationAdapter;
.super Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;
.source "NoiseCancellationAdapter.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioFilterAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;
    }
.end annotation


# static fields
.field private static final CHUNK_SIZE:I = 0x640

.field static final SAMPLE_SIZE:I = 0x140

.field private static final SATURATION_COUNT_MAX_SIZE:I = 0xa

.field private static isCurrentUTTSaturated:Z

.field private static isPreviousUTTSaturated:Z

.field private static mLastResult:I

.field private static toastedOnce:Z


# instance fields
.field private HANGOVER_FRAME_CNT:I

.field private HANGOVER_FRAME_FLAG:Z

.field private IsSeamlessWakeUp:Z

.field private IsTTSplay:Z

.field private final MAX_TTS:I

.field private SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private TTScount:I

.field private acousticModelPathname:Ljava/lang/String;

.field public consoleInitReturn:J

.field public consoleResult:Ljava/lang/String;

.field private epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

.field private globalAudioSourceId:I

.field private isDRCon:Z

.field private isFirstFrame:Z

.field private mContext:Landroid/content/Context;

.field public mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

.field private saturationCount:[I

.field private saturationCountIndex:I

.field private saturationReadSamples:I

.field private searchGrammarPathname:Ljava/lang/String;

.field stats:[I

.field temp:[S

.field private final threadPriority:I

.field private final useSeperateThreadFromMicAnim:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->toastedOnce:Z

    .line 42
    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 43
    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 55
    sput v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;-><init>()V

    .line 35
    iput-object v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 40
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    .line 44
    iput-boolean v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 45
    iput-boolean v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 46
    iput-boolean v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->useSeperateThreadFromMicAnim:Z

    .line 47
    const/4 v0, -0x8

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->threadPriority:I

    .line 52
    iput v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    .line 54
    iput v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    .line 61
    invoke-static {}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngineWrapper;->getInstance()Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    .line 63
    iput-object v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleResult:Ljava/lang/String;

    .line 64
    const-string/jumbo v0, "/system/wakeupdata/sensory/samsung_wakeup_am_quiet_ko_kr_v2.raw"

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->acousticModelPathname:Ljava/lang/String;

    .line 65
    const-string/jumbo v0, "/system/wakeupdata/sensory/samsung_wakeup_grammar_quiet_ko_kr_v2.raw"

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->searchGrammarPathname:Ljava/lang/String;

    .line 67
    iput-boolean v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    .line 68
    iput-boolean v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsTTSplay:Z

    .line 70
    iput v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->TTScount:I

    .line 71
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->MAX_TTS:I

    .line 73
    iput-object v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mContext:Landroid/content/Context;

    .line 75
    iput v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    .line 76
    iput-boolean v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z

    .line 285
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/speech/enhance/NoiseCancellationAdapter;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsTTSplay:Z

    return v0
.end method

.method private getContext()V
    .locals 1

    .prologue
    .line 79
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mContext:Landroid/content/Context;

    .line 80
    return-void
.end method

.method private getLanguageString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 447
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 448
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voicetalk_language"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 450
    .local v1, "languageString":Ljava/lang/String;
    const-string/jumbo v2, "-"

    const-string/jumbo v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 451
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 453
    const-string/jumbo v2, "v_es_la"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "es_us"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454
    :cond_0
    const-string/jumbo v1, "es_la"

    .line 456
    :cond_1
    return-object v1
.end method

.method public static getResult()I
    .locals 1

    .prologue
    .line 466
    sget v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    return v0
.end method

.method private declared-synchronized initializeVoiceEngine()Lcom/samsung/voiceshell/VoiceEngine;
    .locals 2

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 282
    :goto_0
    monitor-exit p0

    return-object v1

    .line 273
    :catch_0
    move-exception v0

    .line 282
    .local v0, "error":Ljava/lang/Throwable;
    const/4 v1, 0x0

    goto :goto_0

    .line 272
    .end local v0    # "error":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public checkSaturation([SI)Z
    .locals 7
    .param p1, "audioData"    # [S
    .param p2, "sizeInShorts"    # I

    .prologue
    const/4 v6, 0x0

    .line 416
    const/4 v1, 0x0

    .line 417
    .local v1, "isSaturated":Z
    const/4 v2, 0x0

    .line 419
    .local v2, "saturationCountSum":I
    iget-object v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCount:[I

    iget v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    aput v6, v3, v4

    .line 420
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 421
    aget-short v3, p1, v0

    const/16 v4, 0x2000

    if-gt v3, v4, :cond_0

    aget-short v3, p1, v0

    const/16 v4, -0x2000

    if-ge v3, v4, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCount:[I

    iget v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4

    .line 420
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 424
    :cond_2
    const/4 v0, 0x0

    :goto_1
    const/16 v3, 0xa

    if-ge v0, v3, :cond_3

    .line 425
    iget-object v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCount:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 428
    :cond_3
    const/16 v3, 0x64

    if-le v2, v3, :cond_4

    .line 429
    const/4 v1, 0x1

    .line 431
    :cond_4
    const/4 v3, 0x1

    if-le v2, v3, :cond_5

    .line 436
    :cond_5
    iget v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    .line 437
    iget v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    add-int/2addr v3, p2

    iput v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    .line 438
    iget v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    const/16 v4, 0x640

    if-lt v3, v4, :cond_6

    .line 439
    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    .line 440
    iput v6, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    .line 443
    :cond_6
    return v1
.end method

.method public filter([SII)I
    .locals 10
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    if-nez v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return p3

    .line 172
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->filter([SII)I

    .line 177
    new-array v3, p3, [S

    .line 179
    .local v3, "tmpAudioData":[S
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p1, v0, v3, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    const/4 v9, 0x0

    .line 182
    .local v9, "retValue":I
    iget v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v0, p1, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processNSFrame([SI)I

    move-result v9

    .line 185
    :goto_1
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    invoke-virtual {v0, v9, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;->processValue(II)I

    move-result v8

    .line 187
    .local v8, "mEPDResult":I
    if-nez v8, :cond_8

    .line 200
    :cond_2
    :goto_2
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    if-eqz v0, :cond_3

    .line 201
    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_a

    .line 202
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    int-to-long v4, p3

    const-wide/16 v6, 0x3e80

    invoke-virtual/range {v0 .. v7}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotPipe(J[SJJ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleResult:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleResult:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 206
    const-string/jumbo v0, "VSG"

    const-string/jumbo v1, "VSG:hi galaxy is detected => HANGOVER PARAM UPDATE START READY"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    invoke-virtual {v0, v1, v2}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotClose(J)V

    .line 208
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    .line 209
    iput p3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    .line 226
    :cond_3
    :goto_3
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    if-eqz v0, :cond_4

    .line 228
    sget-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 233
    :goto_4
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 237
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    if-nez v0, :cond_6

    .line 239
    :cond_5
    invoke-virtual {p0, p1, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->checkSaturation([SI)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 241
    sget-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    if-eqz v0, :cond_6

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    .line 243
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isPreviousUTTSaturated:Z

    .line 249
    :cond_6
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->isAssociatedService()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v0, p1, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processDRC([SI)I

    move-result v9

    goto/16 :goto_0

    .line 183
    .end local v8    # "mEPDResult":I
    :cond_7
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v0, v3, p3}, Lcom/samsung/voiceshell/VoiceEngine;->processEPDFrame([SI)I

    move-result v9

    goto :goto_1

    .line 190
    .restart local v8    # "mEPDResult":I
    :cond_8
    const/4 v0, 0x1

    if-ne v8, v0, :cond_9

    .line 191
    const/4 v0, 0x1

    sput v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    .line 192
    const-string/jumbo v0, "NoiseEPDListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "END_SPEECH : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    goto/16 :goto_2

    .line 194
    :cond_9
    const/4 v0, 0x2

    if-ne v8, v0, :cond_2

    .line 195
    const/4 v0, 0x2

    sput v0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    .line 196
    const-string/jumbo v0, "NoiseEPDListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "END_nonSPEECH : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-static {}, Lcom/vlingo/sdk/VLSdk;->getInstance()Lcom/vlingo/sdk/VLSdk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/sdk/VLSdk;->getRecognizer()Lcom/vlingo/sdk/recognition/VLRecognizer;

    move-result-object v0

    invoke-interface {v0}, Lcom/vlingo/sdk/recognition/VLRecognizer;->stopRecognition()V

    goto/16 :goto_2

    .line 211
    :cond_a
    iget v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    const/16 v1, 0x1040

    if-ge v0, v1, :cond_b

    .line 212
    iget v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z

    .line 214
    const-string/jumbo v0, "VSG"

    const-string/jumbo v1, "VSG:Wait for EPD param update"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 216
    :cond_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z

    goto/16 :goto_3

    .line 229
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isDRCon:Z

    goto/16 :goto_4
.end method

.method public filter([SIII)I
    .locals 1
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    .line 265
    iput p4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->globalAudioSourceId:I

    .line 267
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->filter([SII)I

    move-result v0

    return v0
.end method

.method public init(III)V
    .locals 7
    .param p1, "sampleRate"    # I
    .param p2, "speechEndpointTimeout"    # I
    .param p3, "noSpeechEndPointTimeout"    # I

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 88
    invoke-direct {p0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->initializeVoiceEngine()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 89
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    if-nez v1, :cond_0

    .line 158
    :goto_0
    return-void

    .line 94
    :cond_0
    const/4 v1, -0x8

    invoke-super {p0, p1, p2, p3, v1}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->init(IIII)V

    .line 97
    invoke-direct {p0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->getContext()V

    .line 98
    sput v4, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mLastResult:I

    .line 100
    const-string/jumbo v1, "VSG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "VSG: speechEndpointTimeout = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/16 v1, 0x80

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    .line 103
    const/16 v1, 0x140

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->temp:[S

    .line 105
    new-instance v1, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;-><init>(Lcom/samsung/speech/enhance/NoiseCancellationAdapter;III)V

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->epdListener:Lcom/samsung/speech/enhance/NoiseCancellationAdapter$EPDListener;

    .line 106
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeNS()I

    move-result v0

    .line 114
    .local v0, "retValue":I
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    invoke-virtual {v1}, Lcom/samsung/voiceshell/VoiceEngine;->initializeDRC()I

    move-result v0

    .line 122
    sput-boolean v4, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isCurrentUTTSaturated:Z

    .line 124
    iput-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->isFirstFrame:Z

    .line 126
    invoke-static {}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->init()V

    .line 128
    iput v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_CNT:I

    .line 129
    iput-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->HANGOVER_FRAME_FLAG:Z

    .line 131
    const-string/jumbo v1, "is_seamless_wakeup_started"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    .line 133
    const-string/jumbo v1, "count_for_tts_play"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->TTScount:I

    .line 136
    iget-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    if-nez v1, :cond_1

    .line 137
    iput-boolean v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    .line 138
    const-string/jumbo v1, "VSG"

    const-string/jumbo v2, "VSG: Cannot use Sensory // no Sensory Lib"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    if-eqz v1, :cond_2

    .line 142
    invoke-virtual {p0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->setWakeUpModel()V

    .line 143
    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-object v2, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->acousticModelPathname:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->searchGrammarPathname:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotInit(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    .line 146
    :cond_2
    const-string/jumbo v1, "VSG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "TTS played count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->TTScount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-boolean v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->TTScount:I

    if-ge v1, v6, :cond_3

    .line 150
    iput-boolean v5, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsTTSplay:Z

    .line 151
    const-string/jumbo v1, "VSG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "TTS played count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->TTScount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " need to Hangover for 3sec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :goto_1
    iput v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCountIndex:I

    .line 156
    iput v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationReadSamples:I

    .line 157
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->saturationCount:[I

    goto/16 :goto_0

    .line 153
    :cond_3
    iput-boolean v4, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsTTSplay:Z

    goto :goto_1
.end method

.method public quit()Z
    .locals 5

    .prologue
    const-wide/16 v3, -0x1

    .line 408
    iget-boolean v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->IsSeamlessWakeUp:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->mSensoryWakeupEngine:Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;

    iget-wide v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    invoke-virtual {v0, v1, v2}, Lcom/sensoryinc/fluentsoftsdk/SensoryWakeUpEngine;->phrasespotClose(J)V

    .line 410
    iput-wide v3, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->consoleInitReturn:J

    .line 413
    :cond_0
    invoke-super {p0}, Lcom/vlingo/core/internal/audio/ASyncAudioDataSink;->quit()Z

    move-result v0

    return v0
.end method

.method public setWakeUpModel()V
    .locals 3

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->getLanguageString()Ljava/lang/String;

    move-result-object v0

    .line 461
    .local v0, "getSvoiceLang":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/samsung_wakeup_am_quiet_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_v2.raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->acousticModelPathname:Ljava/lang/String;

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/samsung_wakeup_grammar_quiet_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_v2.raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->searchGrammarPathname:Ljava/lang/String;

    .line 463
    return-void
.end method

.method public sink([SII)I
    .locals 2
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I

    .prologue
    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->SamsungVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    iget-object v1, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    invoke-virtual {v0, p1, v1}, Lcom/samsung/voiceshell/VoiceEngine;->getSpectrum([S[I)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :goto_0
    iget-object v0, p0, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->stats:[I

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/MicAnimationUtils;->notifyListeners([I)V

    .line 396
    const/4 v0, 0x0

    return v0

    .line 392
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public sink([SIII)I
    .locals 1
    .param p1, "audioData"    # [S
    .param p2, "offsetInShorts"    # I
    .param p3, "sizeInShorts"    # I
    .param p4, "audioSourceId"    # I

    .prologue
    .line 402
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/speech/enhance/NoiseCancellationAdapter;->sink([SII)I

    .line 403
    const/4 v0, 0x0

    return v0
.end method
