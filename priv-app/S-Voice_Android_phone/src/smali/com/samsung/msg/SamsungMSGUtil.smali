.class public Lcom/samsung/msg/SamsungMSGUtil;
.super Ljava/lang/Object;
.source "SamsungMSGUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/MsgUtil;


# static fields
.field private static final CID_COLON:Ljava/lang/String; = "cid:"

.field private static final CID_COLON_CAP:Ljava/lang/String; = "Cid:"

.field private static final MMS_STATUS_PROJECTION:[Ljava/lang/String;

.field private static final NEW_INCOMING_MM_CONSTRAINT:Ljava/lang/String; = "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))"


# instance fields
.field private pduPersister:Lcom/google/android/mms/pdu/PduPersister;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "thread_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "sub_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "pri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/msg/SamsungMSGUtil;->MMS_STATUS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    return-void
.end method

.method private countAttachmentFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)I
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p3, "addAttachmentSlide"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/mms/MmsException;
        }
    .end annotation

    .prologue
    .line 368
    invoke-static {p2, p1}, Lcom/android/DomParser/dom/model/SmilHelper;->getDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v2

    .line 371
    .local v2, "document":Lorg/w3c/dom/smil/SMILDocument;
    invoke-interface {v2}, Lorg/w3c/dom/smil/SMILDocument;->getBody()Lorg/w3c/dom/smil/SMILElement;

    move-result-object v1

    .line 372
    .local v1, "docBody":Lorg/w3c/dom/smil/SMILElement;
    invoke-interface {v1}, Lorg/w3c/dom/smil/SMILElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 373
    .local v8, "slideNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    .line 374
    .local v9, "slidesNum":I
    const/4 v0, 0x0

    .line 376
    .local v0, "attatchments":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v9, :cond_4

    .line 377
    invoke-interface {v8, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    check-cast v7, Lorg/w3c/dom/smil/SMILParElement;

    .line 380
    .local v7, "par":Lorg/w3c/dom/smil/SMILParElement;
    invoke-interface {v7}, Lorg/w3c/dom/smil/SMILParElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 381
    .local v5, "mediaNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 383
    .local v6, "mediaNum":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v6, :cond_3

    .line 385
    invoke-interface {v5, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/samsung/msg/SamsungMSGUtil;->getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v10

    .line 387
    .local v10, "sme":Lorg/w3c/dom/smil/SMILMediaElement;
    if-nez v10, :cond_1

    .line 383
    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 389
    :cond_1
    invoke-interface {v10}, Lorg/w3c/dom/smil/SMILMediaElement;->getTagName()Ljava/lang/String;

    move-result-object v11

    .line 391
    .local v11, "tag":Ljava/lang/String;
    const-string/jumbo v12, "img"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string/jumbo v12, "audio"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string/jumbo v12, "video"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 392
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 376
    .end local v10    # "sme":Lorg/w3c/dom/smil/SMILMediaElement;
    .end local v11    # "tag":Ljava/lang/String;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 397
    .end local v4    # "j":I
    .end local v5    # "mediaNodes":Lorg/w3c/dom/NodeList;
    .end local v6    # "mediaNum":I
    .end local v7    # "par":Lorg/w3c/dom/smil/SMILParElement;
    :cond_4
    return v0
.end method

.method private escapeXML(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 219
    const-string/jumbo v0, "&lt;"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "&gt;"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private findPart(Lcom/google/android/mms/pdu/PduBody;Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;
    .locals 6
    .param p1, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p2, "src"    # Ljava/lang/String;

    .prologue
    .line 181
    const/4 v1, 0x0

    .line 182
    .local v1, "index":I
    const/4 v3, 0x0

    .line 183
    .local v3, "part":Lcom/google/android/mms/pdu/PduPart;
    const/4 v2, 0x0

    .line 184
    .local v2, "newSrc":Ljava/lang/String;
    const/4 v0, 0x0

    .line 185
    .local v0, "extention":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 186
    invoke-direct {p0, p2}, Lcom/samsung/msg/SamsungMSGUtil;->escapeXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 187
    const-string/jumbo v4, "cid:"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 188
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "cid:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 212
    :cond_0
    :goto_0
    if-eqz v3, :cond_4

    .line 213
    return-object v3

    .line 189
    :cond_1
    const-string/jumbo v4, "Cid:"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 190
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "Cid:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    goto :goto_0

    .line 192
    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentId(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 193
    if-nez v3, :cond_3

    .line 194
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByName(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 196
    :cond_3
    if-nez v3, :cond_0

    .line 197
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByFileName(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 198
    if-nez v3, :cond_0

    .line 199
    invoke-virtual {p1, p2}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentLocation(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    .line 201
    if-nez v3, :cond_0

    .line 202
    const/16 v4, 0x2e

    invoke-virtual {p2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 203
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 204
    const/4 v4, 0x0

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 205
    invoke-virtual {p1, v2}, Lcom/google/android/mms/pdu/PduBody;->getPartByContentLocation(Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v3

    goto/16 :goto_0

    .line 215
    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "No part found for the model."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private getBodyTextLineFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)Ljava/lang/String;
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pb"    # Lcom/google/android/mms/pdu/PduBody;
    .param p3, "addAttachmentSlide"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/mms/MmsException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 74
    .local v3, "TempText":Ljava/lang/StringBuffer;
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/android/DomParser/dom/model/SmilHelper;->getDocument(Lcom/google/android/mms/pdu/PduBody;Landroid/content/Context;)Lorg/w3c/dom/smil/SMILDocument;

    move-result-object v8

    .line 77
    .local v8, "document":Lorg/w3c/dom/smil/SMILDocument;
    invoke-interface {v8}, Lorg/w3c/dom/smil/SMILDocument;->getBody()Lorg/w3c/dom/smil/SMILElement;

    move-result-object v7

    .line 78
    .local v7, "docBody":Lorg/w3c/dom/smil/SMILElement;
    invoke-interface {v7}, Lorg/w3c/dom/smil/SMILElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 79
    .local v18, "slideNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v19

    .line 80
    .local v19, "slidesNum":I
    const/4 v4, 0x0

    .line 82
    .local v4, "attachments":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, v19

    if-ge v10, v0, :cond_9

    .line 83
    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v16

    check-cast v16, Lorg/w3c/dom/smil/SMILParElement;

    .line 86
    .local v16, "par":Lorg/w3c/dom/smil/SMILParElement;
    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/smil/SMILParElement;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v13

    .line 87
    .local v13, "mediaNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v13}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    .line 89
    .local v14, "mediaNum":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    if-ge v11, v14, :cond_8

    .line 91
    invoke-interface {v13, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/samsung/msg/SamsungMSGUtil;->getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v20

    .line 93
    .local v20, "sme":Lorg/w3c/dom/smil/SMILMediaElement;
    if-nez v20, :cond_1

    .line 89
    :cond_0
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 95
    :cond_1
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/smil/SMILMediaElement;->getTagName()Ljava/lang/String;

    move-result-object v22

    .line 96
    .local v22, "tag":Ljava/lang/String;
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/smil/SMILMediaElement;->getSrc()Ljava/lang/String;

    move-result-object v21

    .line 97
    .local v21, "src":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/msg/SamsungMSGUtil;->findPart(Lcom/google/android/mms/pdu/PduBody;Ljava/lang/String;)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v17

    .line 99
    .local v17, "part":Lcom/google/android/mms/pdu/PduPart;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/mms/pdu/PduPart;->getCharset()I

    move-result v5

    .line 100
    .local v5, "charset":I
    const-string/jumbo v23, "text"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 101
    if-nez v5, :cond_2

    .line 104
    :cond_2
    const/16 v23, 0x3e8

    move/from16 v0, v23

    if-ne v5, v0, :cond_3

    .line 105
    const/4 v5, 0x4

    .line 108
    :cond_3
    const/4 v12, 0x0

    .line 110
    .local v12, "mText":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v23

    if-eqz v23, :cond_4

    .line 111
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v6

    .line 113
    .local v6, "data":[B
    if-nez v5, :cond_5

    .line 114
    :try_start_0
    new-instance v12, Ljava/lang/String;

    .end local v12    # "mText":Ljava/lang/String;
    invoke-direct {v12, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v6    # "data":[B
    .restart local v12    # "mText":Ljava/lang/String;
    :cond_4
    :goto_3
    invoke-virtual {v3, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 116
    .restart local v6    # "data":[B
    :cond_5
    :try_start_1
    invoke-static {v5}, Lcom/google/android/mms/pdu/CharacterSets;->getMimeName(I)Ljava/lang/String;

    move-result-object v15

    .line 117
    .local v15, "name":Ljava/lang/String;
    new-instance v12, Ljava/lang/String;

    .end local v12    # "mText":Ljava/lang/String;
    invoke-direct {v12, v6, v15}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v12    # "mText":Ljava/lang/String;
    goto :goto_3

    .line 119
    .end local v12    # "mText":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 120
    .local v9, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v6}, Ljava/lang/String;-><init>([B)V

    .restart local v12    # "mText":Ljava/lang/String;
    goto :goto_3

    .line 133
    .end local v6    # "data":[B
    .end local v9    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v12    # "mText":Ljava/lang/String;
    :cond_6
    const-string/jumbo v23, "img"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_7

    const-string/jumbo v23, "audio"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_7

    const-string/jumbo v23, "video"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 134
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 82
    .end local v5    # "charset":I
    .end local v17    # "part":Lcom/google/android/mms/pdu/PduPart;
    .end local v20    # "sme":Lorg/w3c/dom/smil/SMILMediaElement;
    .end local v21    # "src":Ljava/lang/String;
    .end local v22    # "tag":Ljava/lang/String;
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 149
    .end local v11    # "j":I
    .end local v13    # "mediaNodes":Lorg/w3c/dom/NodeList;
    .end local v14    # "mediaNum":I
    .end local v16    # "par":Lorg/w3c/dom/smil/SMILParElement;
    :cond_9
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    return-object v23
.end method

.method private getDualSimSmsManager()Landroid/telephony/SmsManager;
    .locals 12

    .prologue
    .line 426
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v5

    .line 428
    .local v5, "smsManager":Landroid/telephony/SmsManager;
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x13

    if-gt v9, v10, :cond_0

    move-object v6, v5

    .line 452
    .end local v5    # "smsManager":Landroid/telephony/SmsManager;
    .local v6, "smsManager":Landroid/telephony/SmsManager;
    :goto_0
    return-object v6

    .line 433
    .end local v6    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v5    # "smsManager":Landroid/telephony/SmsManager;
    :cond_0
    const/4 v9, 0x1

    :try_start_0
    invoke-static {v9}, Lcom/samsung/android/telephony/MultiSimManager;->getDefaultSubId(I)J

    move-result-wide v7

    .line 434
    .local v7, "subId":J
    invoke-static {v7, v8}, Lcom/samsung/android/telephony/MultiSimManager;->getSlotId(J)I

    move-result v9

    int-to-long v2, v9

    .line 435
    .local v2, "slotId":J
    const/4 v1, 0x0

    .line 436
    .local v1, "simType":I
    const-wide/16 v9, 0x0

    cmp-long v9, v2, v9

    if-nez v9, :cond_2

    .line 437
    const/4 v1, 0x0

    .line 443
    :cond_1
    :goto_1
    invoke-static {v1}, Lcom/samsung/android/telephony/MultiSimManager;->getSubId(I)[J

    move-result-object v4

    .line 444
    .local v4, "slotSubId":[J
    const/4 v9, 0x2

    const/4 v10, 0x0

    aget-wide v10, v4, v10

    invoke-static {v9, v10, v11}, Lcom/samsung/android/telephony/MultiSimManager;->setDefaultSubId(IJ)V

    .line 445
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .end local v1    # "simType":I
    .end local v2    # "slotId":J
    .end local v4    # "slotSubId":[J
    .end local v7    # "subId":J
    :goto_2
    move-object v6, v5

    .line 452
    .end local v5    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v6    # "smsManager":Landroid/telephony/SmsManager;
    goto :goto_0

    .line 439
    .end local v6    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v1    # "simType":I
    .restart local v2    # "slotId":J
    .restart local v5    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v7    # "subId":J
    :cond_2
    const-wide/16 v9, 0x1

    cmp-long v9, v2, v9

    if-nez v9, :cond_1

    .line 440
    const/4 v1, 0x1

    goto :goto_1

    .line 447
    .end local v1    # "simType":I
    .end local v2    # "slotId":J
    .end local v7    # "subId":J
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/msg/SamsungMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    if-nez v0, :cond_0

    .line 154
    invoke-static {p1}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/msg/SamsungMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/samsung/msg/SamsungMSGUtil;->pduPersister:Lcom/google/android/mms/pdu/PduPersister;

    return-object v0
.end method

.method private getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;
    .locals 6
    .param p1, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 161
    move-object v2, p1

    .line 165
    .local v2, "mNode":Lorg/w3c/dom/Node;
    const/4 v4, 0x0

    .line 167
    .local v4, "mSME":Lorg/w3c/dom/smil/SMILMediaElement;
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 168
    .local v3, "mNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 170
    .local v1, "mChildCount":I
    instance-of v5, v2, Lorg/w3c/dom/smil/SMILMediaElement;

    if-eqz v5, :cond_1

    move-object v4, v2

    .line 171
    check-cast v4, Lorg/w3c/dom/smil/SMILMediaElement;

    .line 177
    :cond_0
    return-object v4

    .line 173
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 174
    invoke-interface {v3, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/msg/SamsungMSGUtil;->getSmilMediaElement(Lorg/w3c/dom/Node;)Lorg/w3c/dom/smil/SMILMediaElement;

    move-result-object v4

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isSalesCodeChinaTelecom()Z
    .locals 2

    .prologue
    .line 364
    const-string/jumbo v0, "CTC"

    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->getSalesCodeProperty()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public countAttachmentMMS(Landroid/database/Cursor;)I
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 402
    const/4 v0, 0x1

    .line 403
    .local v0, "COLUMN_MSG_ID":I
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 404
    .local v2, "context":Landroid/content/Context;
    const/4 v1, 0x0

    .line 408
    .local v1, "attachments":I
    const/4 v8, 0x1

    :try_start_0
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 409
    .local v4, "msgId":J
    sget-object v8, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 412
    .local v6, "msgUri":Landroid/net/Uri;
    invoke-direct {p0, v2}, Lcom/samsung/msg/SamsungMSGUtil;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v7

    .line 413
    .local v7, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-eqz v7, :cond_0

    instance-of v8, v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    if-eqz v8, :cond_0

    .line 414
    check-cast v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    .end local v7    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    invoke-virtual {v7}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {p0, v2, v8, v9}, Lcom/samsung/msg/SamsungMSGUtil;->countAttachmentFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)I
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 421
    .end local v4    # "msgId":J
    .end local v6    # "msgUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return v1

    .line 416
    :catch_0
    move-exception v3

    .line 418
    .local v3, "e":Lcom/google/android/mms/MmsException;
    invoke-virtual {v3}, Lcom/google/android/mms/MmsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMmsQueryCursor(J)Landroid/database/Cursor;
    .locals 12
    .param p1, "queryTimeMS"    # J

    .prologue
    const/4 v11, 0x0

    .line 266
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 267
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 275
    .local v1, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v4, ""

    .line 276
    .local v4, "where":Ljava/lang/String;
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_1

    .line 277
    const-wide/16 v2, 0x3e8

    div-long v7, p1, v2

    .line 278
    .local v7, "adjustedQueryTimeMS":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132)) AND date > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 283
    .end local v7    # "adjustedQueryTimeMS":J
    :goto_0
    const-string/jumbo v2, "Temp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[MMS readout] where : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const/4 v9, 0x0

    .line 287
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/msg/SamsungMSGUtil;->MMS_STATUS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "date asc"

    invoke-static/range {v0 .. v6}, Lcom/google/android/mms/util/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    :cond_0
    move-object v2, v9

    .line 296
    :goto_1
    return-object v2

    .line 280
    .end local v9    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string/jumbo v4, "(msg_box=1 AND read=0 AND (m_type=130 OR m_type=132))"

    goto :goto_0

    .line 289
    .restart local v9    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 290
    .local v10, "e":Ljava/lang/Exception;
    if-eqz v9, :cond_0

    .line 291
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-object v2, v11

    .line 292
    goto :goto_1
.end method

.method public getMmsTextByCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 226
    const-string/jumbo v1, ""

    .line 227
    .local v1, "bodyText":Ljava/lang/String;
    const/4 v0, 0x1

    .line 228
    .local v0, "COLUMN_MSG_ID":I
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 230
    .local v2, "context":Landroid/content/Context;
    const/4 v8, 0x1

    :try_start_0
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 231
    .local v4, "msgId":J
    sget-object v8, Lcom/vlingo/core/internal/messages/SMSMMSProvider;->MMS_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 239
    .local v6, "msgUri":Landroid/net/Uri;
    invoke-direct {p0, v2}, Lcom/samsung/msg/SamsungMSGUtil;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v7

    .line 240
    .local v7, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-eqz v7, :cond_0

    instance-of v8, v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    if-eqz v8, :cond_0

    .line 243
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    check-cast v7, Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    .end local v7    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    invoke-virtual {v7}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {p0, v2, v9, v10}, Lcom/samsung/msg/SamsungMSGUtil;->getBodyTextLineFromPduBody(Landroid/content/Context;Lcom/google/android/mms/pdu/PduBody;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    :cond_0
    const-string/jumbo v8, "Temp"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Return_Value : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    .end local v4    # "msgId":J
    .end local v6    # "msgUri":Landroid/net/Uri;
    :goto_0
    const-string/jumbo v8, "\n"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 257
    const-string/jumbo v1, ""

    .line 260
    :cond_1
    return-object v1

    .line 249
    :catch_0
    move-exception v3

    .line 251
    .local v3, "e":Lcom/google/android/mms/MmsException;
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/mms/MmsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    .end local v3    # "e":Lcom/google/android/mms/MmsException;
    :catchall_0
    move-exception v8

    throw v8
.end method

.method public sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9
    .param p1, "destinationAddress"    # Ljava/lang/String;
    .param p2, "scAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/PendingIntent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 335
    .local p3, "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "sentIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .local p5, "deliveryIntents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    invoke-direct {p0}, Lcom/samsung/msg/SamsungMSGUtil;->getDualSimSmsManager()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 337
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentcardstatus"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 338
    .local v7, "defaultSlot":Z
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentcardstatus2"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 339
    .local v8, "secondarySlot":Z
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentnetwork"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 341
    .local v6, "currentNetwork":I
    invoke-direct {p0}, Lcom/samsung/msg/SamsungMSGUtil;->isSalesCodeChinaTelecom()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 342
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 343
    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 344
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 361
    :goto_0
    return-void

    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 346
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 349
    :cond_1
    if-eqz v7, :cond_2

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 350
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 352
    :cond_2
    if-eqz v8, :cond_3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 353
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 356
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 359
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .locals 9
    .param p1, "destinationAddress"    # Ljava/lang/String;
    .param p2, "scAddress"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "sentIntent"    # Landroid/app/PendingIntent;
    .param p5, "deliveryIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/samsung/msg/SamsungMSGUtil;->getDualSimSmsManager()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 306
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentcardstatus"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 307
    .local v7, "defaultSlot":Z
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentcardstatus2"

    invoke-virtual {v1, v2}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 308
    .local v8, "secondarySlot":Z
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "gsm.sim.currentnetwork"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 310
    .local v6, "currentNetwork":I
    invoke-direct {p0}, Lcom/samsung/msg/SamsungMSGUtil;->isSalesCodeChinaTelecom()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 311
    if-eqz v7, :cond_1

    if-eqz v8, :cond_1

    .line 312
    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 313
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 330
    :goto_0
    return-void

    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 315
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 318
    :cond_1
    if-eqz v7, :cond_2

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 319
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 321
    :cond_2
    if-eqz v8, :cond_3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 322
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 325
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0

    :cond_4
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 328
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method
