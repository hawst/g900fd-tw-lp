.class public Lcom/samsung/bdevice/SamsungBDeviceUtil;
.super Ljava/lang/Object;
.source "SamsungBDeviceUtil.java"

# interfaces
.implements Lcom/vlingo/core/internal/util/BDeviceUtil;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothHeadset;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 2
    .param p1, "headset"    # Landroid/bluetooth/BluetoothHeadset;
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 31
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 33
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/bluetooth/BluetoothHeadset;->getSamsungHandsfreeDeviceType(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    :cond_0
    :goto_0
    return-object v0

    .line 37
    :catch_0
    move-exception v1

    goto :goto_0
.end method
