.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    sput p3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    .line 389
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->checkPreferenceValue()V
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 392
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_function:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 399
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 400
    const/4 v0, 0x1

    # setter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$202(Z)Z

    .line 401
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 412
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchWakeupDialog()V
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$300(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    goto :goto_0
.end method
