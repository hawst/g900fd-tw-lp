.class public final enum Lcom/samsung/wakeupsetting/Wave$TrainState;
.super Ljava/lang/Enum;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrainState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/Wave$TrainState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field public static final enum SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 49
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "SPAWN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 50
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "RECORD"

    invoke-direct {v0, v1, v5}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 51
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "SNAPSHOT_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 52
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "SNAPSHOT_REDO"

    invoke-direct {v0, v1, v7}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 53
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    const-string/jumbo v1, "SNAPSHOT_RESTART"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$TrainState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 47
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/wakeupsetting/Wave$TrainState;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/Wave$TrainState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/Wave$TrainState;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$TrainState;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/Wave$TrainState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-object v0
.end method
