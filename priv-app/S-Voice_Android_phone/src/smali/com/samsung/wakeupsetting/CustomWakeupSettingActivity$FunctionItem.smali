.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FunctionItem"
.end annotation


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mIndex:I

.field private mSubTitle:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mSubTitle:Ljava/lang/String;

    .line 196
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mTitle:Ljava/lang/String;

    .line 197
    iput p2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mIndex:I

    .line 198
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mSubTitle:Ljava/lang/String;

    .line 200
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mTitle:Ljava/lang/String;

    .line 201
    iput p3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mIndex:I

    .line 202
    iput-object p2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mDescription:Ljava/lang/String;

    .line 203
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mIndex:I

    return v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mSubTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mDescription:Ljava/lang/String;

    .line 231
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 227
    iput p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mIndex:I

    .line 228
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mSubTitle:Ljava/lang/String;

    .line 220
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->mTitle:Ljava/lang/String;

    .line 212
    return-void
.end method
