.class public interface abstract Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;
.super Ljava/lang/Object;
.source "WaveView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/WaveView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AnimationCompleteListener"
.end annotation


# virtual methods
.method public abstract onEnrollAnimationComplete(I)V
.end method

.method public abstract onEnrollAnimationStarted()V
.end method

.method public abstract onProcessingAnimationComplete()V
.end method

.method public abstract onProcessingAnimationStarted()V
.end method
