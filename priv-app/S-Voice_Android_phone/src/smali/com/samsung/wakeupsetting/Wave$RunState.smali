.class final enum Lcom/samsung/wakeupsetting/Wave$RunState;
.super Ljava/lang/Enum;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RunState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/Wave$RunState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/Wave$RunState;

.field public static final enum EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

.field public static final enum IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

.field public static final enum RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 561
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$RunState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 562
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$RunState;

    const-string/jumbo v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/Wave$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 563
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$RunState;

    const-string/jumbo v1, "EXIT"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/Wave$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 560
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$RunState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/Wave$RunState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 560
    const-class v0, Lcom/samsung/wakeupsetting/Wave$RunState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/Wave$RunState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/Wave$RunState;
    .locals 1

    .prologue
    .line 560
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$RunState;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/Wave$RunState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/Wave$RunState;

    return-object v0
.end method
