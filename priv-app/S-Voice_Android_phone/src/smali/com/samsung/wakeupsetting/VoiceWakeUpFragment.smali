.class public Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;
.super Landroid/preference/PreferenceFragment;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath",
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;
    }
.end annotation


# static fields
.field static final REQUEST_WAKEUP_COMMAND_RECORD:I = 0x1

.field private static final VOICE_SET_WAKE_UP_COMMAND:Ljava/lang/String; = "set_wake_up_command"

.field private static final VOICE_WAKEUP_IN_SECURED_LOCK:Ljava/lang/String; = "voice_wakeup_in_secured_lock"

.field private static final VOICE_WAKE_UP_CARMODE:Ljava/lang/String; = "carmode_desc"

.field public static final VOICE_WAKE_UP_POP_SCREEN:Ljava/lang/String; = "VOICE_WAKE_UP_POP_SCREEN"

.field private static final VOICE_WAKE_UP_TIME:Ljava/lang/String; = "wake_up_time"

.field private static final WAKEUP_WHEN_LOCKED:Ljava/lang/String; = "secure_voice_wake_up"


# instance fields
.field private actionBarSwitch:Landroid/widget/Switch;

.field private audioManager:Landroid/media/AudioManager;

.field fromCarMode:Z

.field private isFromMainApp:Z

.field private isResultOk:Z

.field public isSensoryUDTSIDsoFileExist:Z

.field private mCarmode:Lcom/vlingo/midas/settings/CustomPreference;

.field private mDensity:F

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

.field private mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

.field private mValueIndex:I

.field private mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

.field private mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

.field private mWakeup:Landroid/preference/CheckBoxPreference;

.field private mWakeupSecure:Landroid/preference/CheckBoxPreference;

.field preferenceNameArray:[Ljava/lang/String;

.field private resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

.field private final sensoryUDTSIDSoFilePath:Ljava/lang/String;

.field private startRecording:Z

.field title:Ljava/lang/String;

.field private voiceWakeupStopped:Z

.field final wakeupDialogAutoFunctionArray:[Ljava/lang/String;

.field wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field wakeupDialogListView:Landroid/widget/ListView;

.field private wakeupPreference:[Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mDensity:F

    .line 82
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isSensoryUDTSIDsoFileExist:Z

    .line 83
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    .line 99
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isFromMainApp:Z

    .line 100
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isResultOk:Z

    .line 101
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startRecording:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->voiceWakeupStopped:Z

    .line 103
    new-array v0, v1, [Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupPreference:[Landroid/content/SharedPreferences;

    .line 104
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "WakeupCommandPreference1"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->preferenceNameArray:[Ljava/lang/String;

    .line 105
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->title:Ljava/lang/String;

    .line 108
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    .line 118
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    .line 702
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;)V
    .locals 3
    .param p1, "onFragmentLoadListener"    # Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mDensity:F

    .line 82
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isSensoryUDTSIDsoFileExist:Z

    .line 83
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    .line 99
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isFromMainApp:Z

    .line 100
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isResultOk:Z

    .line 101
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startRecording:Z

    .line 102
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->voiceWakeupStopped:Z

    .line 103
    new-array v0, v1, [Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupPreference:[Landroid/content/SharedPreferences;

    .line 104
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "WakeupCommandPreference1"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->preferenceNameArray:[Ljava/lang/String;

    .line 105
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->title:Ljava/lang/String;

    .line 108
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    .line 118
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    .line 702
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    .line 115
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    .line 116
    return-void
.end method

.method private PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    if-nez p1, :cond_1

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    const/4 v7, 0x0

    .line 299
    .local v7, "byteData":[B
    const/4 v10, 0x0

    .line 300
    .local v10, "file":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    .end local v10    # "file":Ljava/io/File;
    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    .restart local v10    # "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v1

    long-to-int v1, v1

    new-array v7, v1, [B

    .line 302
    const/4 v11, 0x0

    .line 305
    .local v11, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    .end local v11    # "in":Ljava/io/FileInputStream;
    .local v12, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v12, v7}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 310
    if-eqz v12, :cond_4

    .line 312
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v11, v12

    .line 319
    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    const/16 v1, 0x3e80

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    .line 321
    .local v5, "intSize":I
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    const/16 v2, 0x3e80

    const/4 v3, 0x2

    const/4 v4, 0x2

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    .line 324
    .local v0, "at":Landroid/media/AudioTrack;
    if-eqz v0, :cond_0

    .line 327
    :try_start_3
    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 329
    const/4 v1, 0x0

    array-length v2, v7

    invoke-virtual {v0, v7, v1, v2}, Landroid/media/AudioTrack;->write([BII)I

    .line 330
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 335
    :try_start_4
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 336
    :catch_0
    move-exception v9

    .line 337
    .local v9, "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 313
    .end local v0    # "at":Landroid/media/AudioTrack;
    .end local v5    # "intSize":I
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v8

    .line 314
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    move-object v11, v12

    .line 315
    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_1

    .line 307
    .end local v8    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    .line 308
    .local v8, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 310
    if-eqz v11, :cond_2

    .line 312
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 313
    :catch_3
    move-exception v8

    .line 314
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 310
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    :goto_3
    if-eqz v11, :cond_3

    .line 312
    :try_start_7
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 315
    :cond_3
    :goto_4
    throw v1

    .line 313
    :catch_4
    move-exception v8

    .line 314
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 331
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v0    # "at":Landroid/media/AudioTrack;
    .restart local v5    # "intSize":I
    :catch_5
    move-exception v8

    .line 332
    .local v8, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 335
    :try_start_9
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 336
    :catch_6
    move-exception v9

    .line 337
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    .line 335
    :try_start_a
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 338
    :goto_5
    throw v1

    .line 336
    :catch_7
    move-exception v9

    .line 337
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 310
    .end local v0    # "at":Landroid/media/AudioTrack;
    .end local v5    # "intSize":I
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v1

    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 307
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catch_8
    move-exception v8

    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :cond_4
    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startCustomCommandRecordingActivity()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->launchResetAlertDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Lcom/vlingo/midas/settings/TwoLinePreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    return-object v0
.end method

.method private getCurrentSystemLanguage()Ljava/util/Locale;
    .locals 9

    .prologue
    .line 247
    :try_start_0
    const-string/jumbo v7, "android.app.ActivityManagerNative"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 248
    .local v1, "amnClass":Ljava/lang/Class;
    const/4 v0, 0x0

    .line 249
    .local v0, "amn":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 251
    .local v2, "config":Landroid/content/res/Configuration;
    const-string/jumbo v7, "getDefault"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 252
    .local v6, "methodGetDefault":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 253
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 255
    const-string/jumbo v7, "getConfiguration"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 256
    .local v5, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 257
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "config":Landroid/content/res/Configuration;
    check-cast v2, Landroid/content/res/Configuration;

    .line 258
    .restart local v2    # "config":Landroid/content/res/Configuration;
    iget-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    .end local v0    # "amn":Ljava/lang/Object;
    .end local v1    # "amnClass":Ljava/lang/Class;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetDefault":Ljava/lang/reflect/Method;
    .local v3, "currentLocale":Ljava/util/Locale;
    :goto_0
    return-object v3

    .line 259
    .end local v3    # "currentLocale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 260
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 261
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .restart local v3    # "currentLocale":Ljava/util/Locale;
    goto :goto_0
.end method

.method private initializeAutoFunctionAdapter()V
    .locals 7

    .prologue
    .line 706
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/vlingo/midas/R$string;->title_listen_my_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 707
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    const/4 v1, 0x1

    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 708
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    const/4 v1, 0x2

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_reset:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 709
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$dimen;->wakeup_dialog_padding_tb:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v6, v0

    .line 710
    .local v6, "paddingTopBottom":I
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$dimen;->wakeup_dialog_padding_lr:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v5, v0

    .line 712
    .local v5, "paddingLeftRight":I
    new-instance v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090003

    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/content/Context;I[Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

    .line 749
    return-void
.end method

.method private launchResetAlertDialog()V
    .locals 5

    .prologue
    .line 836
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->reset_dialog_message:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 837
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->alert_yes:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 838
    .local v2, "yes":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->alert_no:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 839
    .local v1, "no":Ljava/lang/String;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 840
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_reset:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 841
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 842
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 882
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$18;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$18;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 890
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$19;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$19;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 896
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 897
    return-void
.end method

.method private launchWakeupDialog()V
    .locals 6

    .prologue
    .line 752
    const-string/jumbo v4, "/system/lib/libSensoryUDTSIDEngine.so"

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isSensoryUDTSIDsoFileExist:Z

    .line 753
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v5, Lcom/vlingo/midas/R$string;->wcis_wake_up:I

    invoke-virtual {p0, v5}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 754
    .local v2, "myQuittingDialogBox":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 755
    .local v1, "inflater1":Landroid/view/LayoutInflater;
    sget v4, Lcom/vlingo/midas/R$layout;->wakeupdialog:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 756
    .local v3, "myview":Landroid/view/View;
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 757
    sget v4, Lcom/vlingo/midas/R$id;->wakeupDialogOptions:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListView:Landroid/widget/ListView;

    .line 758
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->initializeAutoFunctionAdapter()V

    .line 759
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 760
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidate()V

    .line 761
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 762
    .local v0, "diaBox":Landroid/app/AlertDialog;
    new-instance v4, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$14;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$14;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 770
    new-instance v4, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$15;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$15;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 777
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 778
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogListView:Landroid/widget/ListView;

    new-instance v5, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;

    invoke-direct {v5, p0, v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 805
    return-void
.end method

.method private startCustomCommandRecordingActivity()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 668
    iput-boolean v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startRecording:Z

    .line 669
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 670
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "trainType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 671
    const-string/jumbo v1, "svoice"

    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isFromMainApp:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 672
    const-string/jumbo v1, "fromSettings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 674
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 675
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->unalbe_to_set_wakeup_using_bt_toast:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 680
    :goto_0
    return-void

    .line 678
    :cond_1
    invoke-virtual {p0, v0, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method


# virtual methods
.method public deleteData(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 900
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 901
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 902
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 904
    :cond_0
    return-void
.end method

.method public isAllOptionDisabled()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 643
    const-string/jumbo v2, "secure_voice_wake_up"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 644
    .local v0, "wakeUp":Z
    if-nez v0, :cond_0

    .line 647
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 808
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 809
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 810
    const/4 v1, 0x1

    .line 812
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 684
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 685
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 686
    const/4 v0, 0x0

    .line 687
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupPreference:[Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 688
    const-string/jumbo v1, "isRecorded"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 689
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 690
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 691
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v2, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body:I

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 696
    :goto_0
    iput-boolean v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isResultOk:Z

    .line 698
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 693
    .restart local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v2, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body_without_carmode:I

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, -0x2

    const/4 v3, 0x0

    .line 211
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 212
    new-instance v1, Landroid/widget/Switch;

    invoke-direct {v1, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->actionBarSwitch:Landroid/widget/Switch;

    .line 213
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 214
    .local v0, "padding":I
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 215
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->requestFocus()Z

    .line 216
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 219
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    if-nez v1, :cond_1

    .line 220
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->actionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 221
    :cond_1
    new-instance v1, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v1, p1, v2}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    .line 222
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-virtual {v1, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->setVoiceWakeUpUIChangeListener(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    .line 223
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 908
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    if-eqz v0, :cond_0

    .line 909
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/twopane/SettingsScreenTwoPane;->onHeadlineSelected(I)V

    .line 911
    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "desiredState"    # Z

    .prologue
    .line 352
    invoke-virtual {p0, p2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->updateUIVoiceWakeupSettings(Z)V

    .line 354
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 355
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 345
    if-eqz p1, :cond_0

    .line 346
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 347
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 349
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 124
    invoke-super/range {p0 .. p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 125
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 126
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    iget v13, v5, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mDensity:F

    .line 127
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v13

    const/16 v14, 0x400

    const/16 v15, 0x400

    invoke-virtual {v13, v14, v15}, Landroid/view/Window;->setFlags(II)V

    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const/4 v14, 0x3

    invoke-virtual {v13, v14}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-static {v13}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string/jumbo v14, "svoice"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isFromMainApp:Z

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string/jumbo v14, "voicetalk_language"

    invoke-static {v13, v14}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "currentLocale":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v3

    .line 133
    .local v3, "currentSystem":Ljava/util/Locale;
    if-eqz v3, :cond_0

    .line 134
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    .line 135
    .local v10, "stringLanguage":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    .line 136
    .local v9, "stringCountry":Ljava/lang/String;
    if-nez v2, :cond_7

    const-string/jumbo v13, "pt"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 138
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 139
    .local v1, "conf":Landroid/content/res/Configuration;
    iput-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 140
    const/4 v13, 0x0

    invoke-virtual {v7, v1, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 153
    .end local v1    # "conf":Landroid/content/res/Configuration;
    .end local v7    # "res":Landroid/content/res/Resources;
    .end local v9    # "stringCountry":Ljava/lang/String;
    .end local v10    # "stringLanguage":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const-string/jumbo v14, "audio"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->audioManager:Landroid/media/AudioManager;

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget-object v13, v13, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v13}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x5f

    const/16 v15, 0x2d

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v8

    .line 160
    .local v8, "rightArgs":Ljava/lang/String;
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/vlingo/midas/VlingoApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string/jumbo v14, "voicetalk_language"

    invoke-static {v13, v14, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 161
    sget v13, Lcom/vlingo/midas/R$xml;->voice_wakeup_settings:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->addPreferencesFromResource(I)V

    .line 162
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupPreference:[Landroid/content/SharedPreferences;

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->preferenceNameArray:[Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    aput-object v15, v13, v14

    .line 163
    const-string/jumbo v13, "voice_wakeup_in_secured_lock"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    .line 164
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 165
    const-string/jumbo v13, "carmode_desc"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Lcom/vlingo/midas/settings/CustomPreference;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mCarmode:Lcom/vlingo/midas/settings/CustomPreference;

    .line 166
    const-string/jumbo v13, "set_wake_up_command"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    .line 167
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/vlingo/midas/settings/TwoLinePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 168
    const-string/jumbo v13, "wake_up_time"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Lcom/vlingo/midas/settings/CustomListPreference;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    .line 169
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/vlingo/midas/settings/CustomListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 170
    const-string/jumbo v13, "secure_voice_wake_up"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    check-cast v13, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    .line 171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 173
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->preferenceNameArray:[Ljava/lang/String;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 174
    .local v6, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v13, "isRecorded"

    const/4 v14, 0x0

    invoke-interface {v6, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 175
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 176
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v14, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body:I

    invoke-virtual {v13, v14}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 181
    :cond_1
    :goto_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v13, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 186
    :goto_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v13

    if-nez v13, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v13

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    if-eqz v13, :cond_3

    .line 188
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v13, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 191
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    invoke-virtual {v13, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 193
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v13

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    if-nez v13, :cond_5

    .line 194
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mCarmode:Lcom/vlingo/midas/settings/CustomPreference;

    invoke-virtual {v13, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 197
    :cond_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$layout;->settings_path_view:I

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 199
    .local v12, "view":Landroid/view/View;
    sget v13, Lcom/vlingo/midas/R$id;->fragment_description:I

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 200
    .local v11, "textView":Landroid/widget/TextView;
    sget v13, Lcom/vlingo/midas/R$id;->icon:I

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 201
    .local v4, "iconView":Landroid/widget/ImageView;
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v14, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    if-eqz v13, :cond_6

    .line 204
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

    const/4 v14, 0x1

    invoke-interface {v13, v12, v14}, Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;->updatePathHeader(Landroid/view/View;I)V

    .line 206
    .end local v4    # "iconView":Landroid/widget/ImageView;
    .end local v11    # "textView":Landroid/widget/TextView;
    .end local v12    # "view":Landroid/view/View;
    :cond_6
    return-void

    .line 141
    .end local v6    # "prefs":Landroid/content/SharedPreferences;
    .end local v8    # "rightArgs":Ljava/lang/String;
    .restart local v9    # "stringCountry":Ljava/lang/String;
    .restart local v10    # "stringLanguage":Ljava/lang/String;
    :cond_7
    const-string/jumbo v13, "Brazil"

    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v14

    const-string/jumbo v15, "ro.csc.country_code"

    invoke-virtual {v14, v15}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    if-eqz v2, :cond_0

    const-string/jumbo v13, "en-US"

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    const-string/jumbo v13, "pt"

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    const-string/jumbo v13, "BR"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 147
    .restart local v7    # "res":Landroid/content/res/Resources;
    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 148
    .restart local v1    # "conf":Landroid/content/res/Configuration;
    iput-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 149
    const/4 v13, 0x0

    invoke-virtual {v7, v1, v13}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto/16 :goto_0

    .line 178
    .end local v1    # "conf":Landroid/content/res/Configuration;
    .end local v7    # "res":Landroid/content/res/Resources;
    .end local v9    # "stringCountry":Ljava/lang/String;
    .end local v10    # "stringLanguage":Ljava/lang/String;
    .restart local v6    # "prefs":Landroid/content/SharedPreferences;
    .restart local v8    # "rightArgs":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    sget v14, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body_without_carmode:I

    invoke-virtual {v13, v14}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    goto/16 :goto_1

    .line 184
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    invoke-virtual {v13, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 239
    sget v0, Lcom/vlingo/midas/R$layout;->settings_placeholder:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 228
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 634
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 638
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 636
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 634
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 398
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 399
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pause()V

    .line 401
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startRecording:Z

    if-nez v0, :cond_0

    .line 402
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startVoiceWakeup()V

    .line 403
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isResultOk:Z

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startRecording:Z

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 487
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    if-ne p1, v4, :cond_4

    .line 488
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string/jumbo v5, "preferences_voice_wake_up"

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v4, v5, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 489
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "VOICE_WAKE_UP_IN_SECURED_POP"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_1

    .line 490
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->showWakeupInSecuredLockDialog()Z

    :goto_0
    move v2, v3

    .line 536
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    .end local p2    # "value":Ljava/lang/Object;
    :cond_0
    :goto_1
    return v2

    .line 492
    .restart local v0    # "prefs":Landroid/content/SharedPreferences;
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_1
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 493
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 497
    :goto_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 498
    const-string/jumbo v2, "secure_voice_wake_up"

    invoke-static {v2, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 495
    :cond_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_2

    .line 500
    :cond_3
    const-string/jumbo v4, "secure_voice_wake_up"

    invoke-static {v4, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 504
    .end local v0    # "prefs":Landroid/content/SharedPreferences;
    :cond_4
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    if-ne p1, v4, :cond_6

    .line 505
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/vlingo/midas/settings/CustomListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    .line 507
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    iget v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    invoke-virtual {v2, v4}, Lcom/vlingo/midas/settings/CustomListPreference;->setValueIndex(I)V

    .line 508
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Lcom/vlingo/midas/settings/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 510
    iget v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    if-nez v2, :cond_5

    .line 511
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 513
    :cond_5
    const-string/jumbo v2, "preferences_voice_wake_up_time"

    iget v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mValueIndex:I

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 515
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    const-string/jumbo v5, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    move v2, v3

    .line 516
    goto :goto_1

    .line 518
    .restart local p2    # "value":Ljava/lang/Object;
    :cond_6
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    if-ne p1, v4, :cond_0

    .line 519
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 520
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 530
    :goto_3
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 531
    const-string/jumbo v4, "secure_voice_wake_up"

    invoke-static {v4, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 522
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 523
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    const-string/jumbo v4, "VOICE_WAKE_UP_POP_SCREEN"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_8

    .line 524
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->showWakeupLockScreenDialog()V

    goto :goto_3

    .line 527
    :cond_8
    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_3

    .line 533
    .end local v1    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_9
    const-string/jumbo v3, "secure_voice_wake_up"

    invoke-static {v3, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v0, 0x0

    .line 652
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupPreference:[Landroid/content/SharedPreferences;

    aget-object v1, v1, v0

    const-string/jumbo v2, "isRecorded"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 655
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startCustomCommandRecordingActivity()V

    .line 662
    :goto_0
    const/4 v0, 0x1

    .line 664
    :cond_0
    return v0

    .line 657
    :cond_1
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->launchWakeupDialog()V

    goto :goto_0

    .line 660
    :cond_2
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startCustomCommandRecordingActivity()V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 625
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 379
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 380
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 381
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->resume()V

    .line 383
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "secure_voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    if-eqz v0, :cond_1

    .line 387
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    const-string/jumbo v1, "preferences_voice_wake_up_time"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/CustomListPreference;->setValueIndex(I)V

    .line 388
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    invoke-virtual {v1}, Lcom/vlingo/midas/settings/CustomListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/CustomListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v1, "secure_voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 394
    :cond_2
    return-void
.end method

.method public setmode(Z)V
    .locals 0
    .param p1, "carmode"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    .line 121
    return-void
.end method

.method public showWakeupInSecuredLockDialog()Z
    .locals 11

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 411
    .local v3, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 412
    .local v2, "checkboxLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 413
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const-string/jumbo v8, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v8}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 415
    .local v6, "samsungSans_text":Landroid/graphics/Typeface;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v8

    const/high16 v9, 0x41000000    # 8.0f

    iget v10, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mDensity:F

    mul-float/2addr v9, v10

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    add-int v4, v8, v9

    .line 416
    .local v4, "leftPadding":I
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v8

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v9

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v1, v4, v8, v9, v10}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 419
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$1;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$1;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 424
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$2;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$2;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 430
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v5, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 431
    .local v5, "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 432
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 433
    .local v7, "text":Landroid/widget/TextView;
    if-eqz v7, :cond_0

    .line 434
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 435
    :cond_0
    if-eqz v1, :cond_1

    .line 436
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 437
    :cond_1
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 438
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_secure_popupbody:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 440
    const v8, 0x104000a

    new-instance v9, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$3;

    invoke-direct {v9, p0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$3;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 454
    const/high16 v8, 0x1040000

    new-instance v9, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;

    invoke-direct {v9, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 464
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$5;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$5;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 475
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 476
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 477
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$6;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$6;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 481
    const/4 v8, 0x0

    return v8
.end method

.method public showWakeupLockScreenDialog()V
    .locals 11

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 541
    .local v3, "inflater":Landroid/view/LayoutInflater;
    sget v8, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 542
    .local v2, "checkboxLayout":Landroid/view/View;
    sget v8, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 543
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const-string/jumbo v8, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v8}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 545
    .local v6, "samsungSans_text":Landroid/graphics/Typeface;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v8

    const/high16 v9, 0x41000000    # 8.0f

    iget v10, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mDensity:F

    mul-float/2addr v9, v10

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    add-int v4, v8, v9

    .line 546
    .local v4, "leftPadding":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 547
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v4

    .line 549
    :cond_0
    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v8

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v9

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v1, v4, v8, v9, v10}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 552
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$7;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$7;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$8;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$8;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 562
    const/4 v5, 0x0

    .line 563
    .local v5, "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->isFinishing()Z

    move-result v8

    if-nez v8, :cond_3

    .line 564
    new-instance v5, Landroid/app/AlertDialog$Builder;

    .end local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v5, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 565
    .restart local v5    # "mAlertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 566
    sget v8, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 567
    .local v7, "text":Landroid/widget/TextView;
    if-eqz v7, :cond_1

    .line 568
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 569
    :cond_1
    if-eqz v1, :cond_2

    .line 570
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 571
    :cond_2
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_secure_title:I

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 572
    sget v8, Lcom/vlingo/midas/R$string;->setting_wakeup_secure_popupbody:I

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 573
    const v8, 0x104000a

    new-instance v9, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;

    invoke-direct {v9, p0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 589
    const/high16 v8, 0x1040000

    new-instance v9, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$10;

    invoke-direct {v9, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$10;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v5, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 599
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$11;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$11;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v5, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 610
    .end local v7    # "text":Landroid/widget/TextView;
    :cond_3
    if-eqz v5, :cond_4

    .line 611
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 612
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 613
    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$12;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$12;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 618
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_4
    return-void
.end method

.method public startVoiceWakeup()V
    .locals 3

    .prologue
    .line 816
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->voiceWakeupStopped:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 819
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->voiceWakeupStopped:Z

    .line 821
    :cond_0
    return-void
.end method

.method public stopVoiceWakeup()V
    .locals 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->audioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 826
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 830
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->voiceWakeupStopped:Z

    .line 831
    return-void
.end method

.method public updateUIVoiceWakeupSettings(Z)V
    .locals 3
    .param p1, "turnOn"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 359
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/settings/TwoLinePreference;->setEnabled(Z)V

    .line 360
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->fromCarMode:Z

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/TwoLinePreference;->setEnabled(Z)V

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 365
    if-eqz p1, :cond_2

    .line 366
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 367
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/settings/CustomListPreference;->setEnabled(Z)V

    .line 368
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 375
    :cond_1
    :goto_0
    return-void

    .line 370
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 371
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeUpTime:Lcom/vlingo/midas/settings/CustomListPreference;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/settings/CustomListPreference;->setEnabled(Z)V

    .line 372
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method
