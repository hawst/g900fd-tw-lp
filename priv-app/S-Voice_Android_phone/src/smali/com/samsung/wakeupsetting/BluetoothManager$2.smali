.class Lcom/samsung/wakeupsetting/BluetoothManager$2;
.super Ljava/lang/Object;
.source "BluetoothManager.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/BluetoothManager;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/BluetoothManager;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "paramInt"    # I
    .param p2, "paramBluetoothProfile"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    const/4 v2, 0x1

    .line 102
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$100(Lcom/samsung/wakeupsetting/BluetoothManager;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 103
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, p2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 104
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$102(Lcom/samsung/wakeupsetting/BluetoothManager;I)I

    .line 105
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$202(Lcom/samsung/wakeupsetting/BluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 126
    .end local p2    # "paramBluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    :cond_0
    :goto_0
    return-void

    .line 107
    .restart local p2    # "paramBluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    :cond_1
    if-ne p1, v2, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "paramBluetoothProfile":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, p2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$202(Lcom/samsung/wakeupsetting/BluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 111
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-static {v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->setBluetoothAudioOn(Z)V

    .line 114
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # invokes: Lcom/samsung/wakeupsetting/BluetoothManager;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$400(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    # setter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$302(Lcom/samsung/wakeupsetting/BluetoothManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 116
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    const-string/jumbo v0, "SV_BluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const-string/jumbo v0, "SV_BluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Device Class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const-string/jumbo v0, "SV_BluetoothManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Major Device Class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothClass;->getMajorDeviceClass()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$200(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothHeadset;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager$2;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # getter for: Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    goto/16 :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 0
    .param p1, "paramInt"    # I

    .prologue
    .line 96
    return-void
.end method
