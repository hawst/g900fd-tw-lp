.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingErrorPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1503
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1507
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v2

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startEnroll(SS)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;SS)V

    .line 1508
    return-void
.end method
