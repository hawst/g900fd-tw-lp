.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;)V
    .locals 0

    .prologue
    .line 731
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 734
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z
    invoke-static {v0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1402(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z

    .line 735
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 736
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1600(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomWaveLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setSuccessCount(IZ)V

    .line 737
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1600(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomWaveLayout;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$id;->wake_up_count:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;->this$1:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v2

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startEnroll(SS)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;SS)V

    .line 741
    :cond_0
    return-void
.end method
