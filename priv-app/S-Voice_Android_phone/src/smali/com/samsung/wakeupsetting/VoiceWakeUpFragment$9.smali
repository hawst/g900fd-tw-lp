.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;
.super Ljava/lang/Object;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->showWakeupLockScreenDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 573
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    iput-object p2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 576
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 577
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 578
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 579
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "VOICE_WAKE_UP_POP_SCREEN"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 580
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 581
    const-string/jumbo v2, "preferences_voice_wake_up_time"

    invoke-static {v2, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 583
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$200(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 584
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$200(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 587
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$9;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeupSecure:Landroid/preference/CheckBoxPreference;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$200(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method
