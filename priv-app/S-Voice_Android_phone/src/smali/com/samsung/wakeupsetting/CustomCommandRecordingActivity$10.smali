.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingEndPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1717
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 1720
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1300()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 1723
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING_IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1724
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget v1, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK_ADAPT:I

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$300(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;I)V

    .line 1725
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK_ADAPT:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 1729
    :goto_0
    return-void

    .line 1727
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    goto :goto_0
.end method
