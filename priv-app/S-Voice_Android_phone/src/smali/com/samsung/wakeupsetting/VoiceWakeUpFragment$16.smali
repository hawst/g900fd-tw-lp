.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;
.super Ljava/lang/Object;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->launchWakeupDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

.field final synthetic val$diaBox:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    iput-object p2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->val$diaBox:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 784
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    packed-switch p3, :pswitch_data_0

    .line 801
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 802
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->val$diaBox:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 803
    :goto_1
    return-void

    .line 786
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->saveStreamVloume(Landroid/content/Context;)V

    .line 787
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setVolumeMaximum(Landroid/content/Context;)V

    .line 788
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 789
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 790
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->val$diaBox:Landroid/app/AlertDialog;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 791
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->stopVoiceWakeup()V

    .line 792
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$300(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 795
    .end local v0    # "message":Landroid/os/Message;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # invokes: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->startCustomCommandRecordingActivity()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$400(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    goto :goto_0

    .line 798
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$16;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # invokes: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->launchResetAlertDialog()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$500(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V

    goto :goto_0

    .line 784
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
