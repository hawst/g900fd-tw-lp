.class public Lcom/samsung/wakeupsetting/WaveView;
.super Landroid/view/TextureView;
.source "WaveView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/WaveView$1;,
        Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;,
        Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WaveView"


# instance fields
.field private mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

.field private mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

.field private mWave:Lcom/samsung/wakeupsetting/Wave;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/WaveView;->init()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/WaveView;->init()V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/WaveView;->init()V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/WaveView;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/WaveView;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/WaveView;->onWaveResponse(Landroid/os/Message;)V

    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 181
    new-instance v0, Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/WaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;

    invoke-direct {v2, p0}, Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;-><init>(Lcom/samsung/wakeupsetting/WaveView;)V

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/wakeupsetting/Wave;-><init>(Landroid/content/Context;Landroid/view/TextureView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    .line 182
    invoke-virtual {p0, p0}, Lcom/samsung/wakeupsetting/WaveView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 188
    return-void
.end method

.method private onWaveResponse(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 191
    iget v0, p1, Landroid/os/Message;->what:I

    .line 192
    .local v0, "response":I
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 193
    .local v1, "trial":I
    const-string/jumbo v2, "WaveView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onWaveResponse: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    if-eqz v2, :cond_3

    .line 196
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_0

    .line 197
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    add-int/lit8 v3, v1, 0x1

    invoke-interface {v2, v3}, Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;->onEnrollAnimationComplete(I)V

    .line 199
    :cond_0
    and-int/lit8 v2, v0, 0x8

    if-eqz v2, :cond_1

    .line 200
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    invoke-interface {v2}, Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;->onProcessingAnimationStarted()V

    .line 202
    :cond_1
    and-int/lit8 v2, v0, 0x4

    if-eqz v2, :cond_2

    .line 203
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    invoke-interface {v2}, Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;->onProcessingAnimationComplete()V

    .line 205
    :cond_2
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_3

    .line 206
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    invoke-interface {v2}, Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;->onEnrollAnimationStarted()V

    .line 209
    :cond_3
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_4

    .line 210
    iget-object v2, p0, Lcom/samsung/wakeupsetting/WaveView;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->nextEnrollment()V

    .line 241
    :cond_4
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->initialize()V

    .line 149
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onMeasure(II)V

    .line 60
    return-void
.end method

.method public onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V
    .locals 2
    .param p1, "result"    # Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .prologue
    .line 128
    sget-object v0, Lcom/samsung/wakeupsetting/WaveView$1;->$SwitchMap$com$samsung$wakeupsetting$WaveViewAdapter$Result:[I

    invoke-virtual {p1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 130
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 133
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 136
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onProcessingStarted()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    .line 124
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->start()V

    .line 68
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/wakeupsetting/Wave;->setSize(II)V

    .line 69
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->stop()V

    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 82
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 88
    return-void
.end method

.method public onTrainingComplete(ILcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V
    .locals 2
    .param p1, "trial"    # I
    .param p2, "result"    # Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->getCurrentState()Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v1, :cond_0

    .line 103
    sget-object v0, Lcom/samsung/wakeupsetting/WaveView$1;->$SwitchMap$com$samsung$wakeupsetting$WaveViewAdapter$Result:[I

    invoke-virtual {p2}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 105
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 111
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTrainingStarted(I)V
    .locals 2
    .param p1, "trial"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    .line 97
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->reset(Z)V

    .line 174
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->reset()V

    .line 175
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/WaveView;->setVisibility(I)V

    .line 177
    return-void
.end method

.method public setAdapter(Lcom/samsung/wakeupsetting/WaveViewAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/samsung/wakeupsetting/WaveViewAdapter;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/wakeupsetting/WaveView;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    .line 157
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0, p1}, Lcom/samsung/wakeupsetting/Wave;->setAdapter(Lcom/samsung/wakeupsetting/WaveViewAdapter;)V

    .line 158
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    invoke-virtual {v0, p0}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->setCallback(Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;)V

    .line 159
    return-void
.end method

.method public setAnimationCompleteListener(Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/wakeupsetting/WaveView;->mListener:Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;

    .line 163
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->shutdown()V

    .line 153
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->getCurrentState()Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave;->getCurrentState()Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v1, :cond_1

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveView;->mWave:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->START:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    .line 168
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/WaveView;->setVisibility(I)V

    .line 169
    return-void
.end method
