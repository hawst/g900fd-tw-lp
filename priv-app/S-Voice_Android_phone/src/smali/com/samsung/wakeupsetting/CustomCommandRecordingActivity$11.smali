.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isDowndloadedFPSVApk()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1965
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "paramDialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "paramInt"    # I

    .prologue
    const/4 v3, 0x1

    .line 1967
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 1968
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1969
    .local v0, "localIntent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.samsungapps"

    const-string/jumbo v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1971
    const-string/jumbo v1, "directcall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1972
    const-string/jumbo v1, "CallerType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1973
    const-string/jumbo v1, "GUID"

    const-string/jumbo v2, "com.sec.android.voiceservice.fpsvmodel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1974
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1976
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1977
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isShowingDialog:Z

    .line 1978
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z
    invoke-static {v1, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2702(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z

    .line 1979
    return-void
.end method
