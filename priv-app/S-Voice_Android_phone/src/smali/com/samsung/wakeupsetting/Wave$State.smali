.class public final enum Lcom/samsung/wakeupsetting/Wave$State;
.super Ljava/lang/Enum;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/Wave$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum NONE:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum PROCESS:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum PROCESS_HOLD:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum RESULT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

.field public static final enum TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    .line 38
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "BEGIN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

    .line 39
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "TRAIN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    .line 40
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "TRAIN_COMPLETE"

    invoke-direct {v0, v1, v6}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;

    .line 41
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "PROCESS_HOLD"

    invoke-direct {v0, v1, v7}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS_HOLD:Lcom/samsung/wakeupsetting/Wave$State;

    .line 42
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "PROCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS:Lcom/samsung/wakeupsetting/Wave$State;

    .line 43
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "RESULT_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$State;

    .line 44
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$State;

    const-string/jumbo v1, "RESULT_FAILURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    .line 36
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/wakeupsetting/Wave$State;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS_HOLD:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$State;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/Wave$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/samsung/wakeupsetting/Wave$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/Wave$State;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/Wave$State;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$State;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/Wave$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/Wave$State;

    return-object v0
.end method
