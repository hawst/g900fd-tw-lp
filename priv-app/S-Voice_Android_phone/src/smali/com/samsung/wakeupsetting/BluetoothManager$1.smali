.class Lcom/samsung/wakeupsetting/BluetoothManager$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/BluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/BluetoothManager;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/BluetoothManager;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/samsung/wakeupsetting/BluetoothManager$1;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "paramIntent"    # Landroid/content/Intent;

    .prologue
    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    const-string/jumbo v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    const-string/jumbo v3, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 49
    const-string/jumbo v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 53
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 54
    .local v2, "new_state":I
    iget-object v3, p0, Lcom/samsung/wakeupsetting/BluetoothManager$1;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # invokes: Lcom/samsung/wakeupsetting/BluetoothManager;->handleStateChange(I)V
    invoke-static {v3, v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$000(Lcom/samsung/wakeupsetting/BluetoothManager;I)V

    goto :goto_0

    .line 56
    .end local v2    # "new_state":I
    :cond_2
    const-string/jumbo v3, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 57
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 61
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 62
    .restart local v2    # "new_state":I
    iget-object v3, p0, Lcom/samsung/wakeupsetting/BluetoothManager$1;->this$0:Lcom/samsung/wakeupsetting/BluetoothManager;

    # invokes: Lcom/samsung/wakeupsetting/BluetoothManager;->handleStateChange(I)V
    invoke-static {v3, v2}, Lcom/samsung/wakeupsetting/BluetoothManager;->access$000(Lcom/samsung/wakeupsetting/BluetoothManager;I)V

    goto :goto_0

    .line 64
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "new_state":I
    :cond_3
    const-string/jumbo v3, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method
