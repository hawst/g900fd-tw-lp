.class public Lcom/samsung/wakeupsetting/Wave;
.super Ljava/lang/Object;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/Wave$1;,
        Lcom/samsung/wakeupsetting/Wave$DrawThread;,
        Lcom/samsung/wakeupsetting/Wave$RunState;,
        Lcom/samsung/wakeupsetting/Wave$Anim;,
        Lcom/samsung/wakeupsetting/Wave$Request;,
        Lcom/samsung/wakeupsetting/Wave$TrainState;,
        Lcom/samsung/wakeupsetting/Wave$State;
    }
.end annotation


# static fields
.field private static final ALPHA_LIMIT:F = 0.2f

.field private static final CIRCLE_SCALING:F = 0.625f

.field private static final CIRCLE_SPACING:F = 1.625f

.field public static COLOR_INNER_BG:I = 0x0

.field private static COLOR_SWEEP:[I = null

.field private static COLOR_TRAINING:[I = null

.field private static final DELAY_DELTA:I = 0x64

.field public static final EXTRA_DOTS:I = 0x1

.field public static final EXTRA_DOTS_END:I = 0x4

.field private static final FADEIN_DURATION:I = 0x1f4

.field private static final FADEIN_DURATION_LONG:I = 0x3e8

.field private static final FADEOUT_DURATION:I = 0x1f4

.field public static final GRADIENT_STRETCH_FACTOR:F = 0.6f

.field private static GUIDELINE_HEIGHT:I = 0x0

.field private static LINE_GAP:I = 0x0

.field private static LINE_SPAN:I = 0x0

.field private static LINE_THICKNESS:I = 0x0

.field public static final MOVE_DISTANCE:I = 0xa

.field private static final MOVE_DURATION:I = 0x1f4

.field private static final MOVE_INTERVAL:I = 0x32

.field public static final NUM_SAMPLES:I = 0x1e

.field private static PADDING_BORDER:I = 0x0

.field private static PADDING_LEFT:I = 0x0

.field private static PADDING_RIGHT:I = 0x0

.field private static final PICK_INDEX:I = 0x3

.field private static final PROCESS_DURATION:I = 0x3e8

.field public static final RESPONSE_ACTION_NEXTENROLL:I = 0x1

.field public static final RESPONSE_ANIMCOMPLETE_ENROLL:I = 0x2

.field public static final RESPONSE_ANIMCOMPLETE_PROCESSING:I = 0x4

.field public static final RESPONSE_ANIMSTART_PROCESSING:I = 0x8

.field private static final REV_PROB:F = 0.3f

.field private static final SCROLL_INTERVAL:I = 0x32

.field public static final SCROLL_RECORDING:Z = true

.field private static final SHADER_SWEEP_INC:I = 0x5

.field private static final SHIFT_FACTOR:I = 0x4

.field private static final SHIFT_STEPS_MAX:I = 0x7

.field private static final SWEEP_DURATION:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "Wave"


# instance fields
.field private mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

.field private mContext:Landroid/content/Context;

.field private mDirCount:I

.field mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

.field mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

.field private mHeight:I

.field mLinePaint:Landroid/graphics/Paint;

.field private mMatrix:Landroid/graphics/Matrix;

.field mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

.field private mNextState:Lcom/samsung/wakeupsetting/Wave$State;

.field private mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field private mNextTrialNumber:I

.field private mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

.field private mRand:Ljava/util/Random;

.field private mResponseHandler:Landroid/os/Handler;

.field private mReverse:Z

.field private mScrollIndex:I

.field private mShader:Landroid/graphics/Shader;

.field private mShiftValues:[I

.field private mState:Lcom/samsung/wakeupsetting/Wave$State;

.field mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

.field mSweepPaint:Landroid/graphics/Paint;

.field private mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

.field mTimeTracker:Lcom/samsung/wakeupsetting/Wave$Anim;

.field private mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

.field private mTranslate:F

.field private mTrialNumber:I

.field private mView:Landroid/view/TextureView;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/TextureView;Landroid/os/Handler;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/TextureView;
    .param p3, "responseHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, 0x1

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    const/16 v0, 0x1e

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I

    .line 215
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mRand:Ljava/util/Random;

    .line 219
    iput v3, p0, Lcom/samsung/wakeupsetting/Wave;->mWidth:I

    iput v3, p0, Lcom/samsung/wakeupsetting/Wave;->mHeight:I

    .line 223
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mMatrix:Landroid/graphics/Matrix;

    .line 228
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave;->mContext:Landroid/content/Context;

    .line 229
    iput-object p2, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    .line 230
    iput-object p3, p0, Lcom/samsung/wakeupsetting/Wave;->mResponseHandler:Landroid/os/Handler;

    .line 232
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave;->init()V

    .line 233
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/Wave;)Landroid/view/TextureView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/wakeupsetting/Wave;)Ljava/util/Random;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mRand:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/wakeupsetting/Wave;Lcom/samsung/wakeupsetting/Wave$State;)Lcom/samsung/wakeupsetting/Wave$State;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # Lcom/samsung/wakeupsetting/Wave$State;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/wakeupsetting/Wave;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave;->mReverse:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/wakeupsetting/Wave;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/Wave;->mReverse:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/wakeupsetting/Wave;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/wakeupsetting/Wave;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTranslate:F

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/wakeupsetting/Wave;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/wakeupsetting/Wave;->mTranslate:F

    return p1
.end method

.method static synthetic access$1316(Lcom/samsung/wakeupsetting/Wave;F)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # F

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTranslate:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTranslate:F

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/wakeupsetting/Wave;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/wakeupsetting/Wave;)Landroid/graphics/Shader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShader:Landroid/graphics/Shader;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mWidth:I

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I

    return v0
.end method

.method static synthetic access$1710(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I

    return v0
.end method

.method static synthetic access$1712(Lcom/samsung/wakeupsetting/Wave;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I

    return v0
.end method

.method static synthetic access$1800()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I

    return v0
.end method

.method static synthetic access$1900()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mHeight:I

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/wakeupsetting/Wave;Lcom/samsung/wakeupsetting/Wave$TrainState;)Lcom/samsung/wakeupsetting/Wave$TrainState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # Lcom/samsung/wakeupsetting/Wave$TrainState;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    return-object v0
.end method

.method static synthetic access$2200()[I
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    return-object v0
.end method

.method static synthetic access$2300()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I

    return v0
.end method

.method static synthetic access$2400()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/wakeupsetting/Wave;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mDirCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/wakeupsetting/Wave;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/wakeupsetting/Wave;->mDirCount:I

    return p1
.end method

.method static synthetic access$708(Lcom/samsung/wakeupsetting/Wave;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mDirCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mDirCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/wakeupsetting/Wave;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/wakeupsetting/Wave;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/Wave;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave;->processPendingRequests()V

    return-void
.end method

.method private changeTrainState(Lcom/samsung/wakeupsetting/Wave$Request;)Lcom/samsung/wakeupsetting/Wave$State;
    .locals 5
    .param p1, "request"    # Lcom/samsung/wakeupsetting/Wave$Request;

    .prologue
    const/4 v4, 0x3

    .line 420
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 421
    .local v0, "state":Lcom/samsung/wakeupsetting/Wave$State;
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    monitor-enter v2

    .line 422
    :try_start_0
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$TrainState:[I

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/Wave$TrainState;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 464
    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    const-string/jumbo v1, "Wave"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "TrainState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    return-object v0

    .line 424
    :pswitch_0
    :try_start_1
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 427
    :pswitch_1
    :try_start_2
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_1

    .line 428
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 429
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave;->clearPendingRequests()V

    goto :goto_0

    .line 430
    :cond_1
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_2

    .line 431
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    goto :goto_0

    .line 433
    :cond_2
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_0

    .line 434
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    goto :goto_0

    .line 438
    :pswitch_2
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    .line 439
    goto :goto_0

    .line 442
    :pswitch_3
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    .line 443
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    goto :goto_0

    .line 446
    :pswitch_4
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-eq p1, v1, :cond_3

    .line 447
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->pendRequest(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 448
    :cond_3
    iget v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    if-ne v1, v4, :cond_4

    .line 449
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;

    goto :goto_0

    .line 451
    :cond_4
    iget v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 452
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 453
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    goto :goto_0

    .line 457
    :pswitch_5
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private clearPendingRequests()V
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->NONE:Lcom/samsung/wakeupsetting/Wave$Request;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 372
    return-void
.end method

.method private init()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 492
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 493
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 495
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 496
    .local v11, "res":Landroid/content/res/Resources;
    sget v0, Lcom/vlingo/midas/R$color;->inner_bg:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_INNER_BG:I

    .line 497
    sget v0, Lcom/vlingo/midas/R$array;->training:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    .line 498
    sget v0, Lcom/vlingo/midas/R$dimen;->line_thickness:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->LINE_THICKNESS:I

    .line 499
    sget v0, Lcom/vlingo/midas/R$dimen;->line_gap:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I

    .line 500
    sget v0, Lcom/vlingo/midas/R$dimen;->guideline_height:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->GUIDELINE_HEIGHT:I

    .line 501
    sget v0, Lcom/samsung/wakeupsetting/Wave;->LINE_THICKNESS:I

    sget v1, Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I

    add-int/2addr v0, v1

    sput v0, Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I

    .line 502
    sget v0, Lcom/vlingo/midas/R$dimen;->padding_left:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget v1, Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sput v0, Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I

    .line 503
    sget v0, Lcom/vlingo/midas/R$dimen;->padding_right:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->PADDING_RIGHT:I

    .line 504
    sget v0, Lcom/vlingo/midas/R$dimen;->padding_border:I

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I

    .line 506
    sget-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    .line 507
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    sget-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    array-length v0, v0

    if-ge v8, v0, :cond_0

    .line 508
    sget-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    sget-object v1, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    aget v1, v1, v8

    aput v1, v0, v8

    .line 507
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 511
    :cond_0
    sget-object v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    sget-object v1, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    aget v2, v2, v3

    aput v2, v0, v1

    .line 513
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    .line 514
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 515
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    sget v1, Lcom/samsung/wakeupsetting/Wave;->LINE_THICKNESS:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 516
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 517
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 519
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    .line 520
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 521
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    sget v1, Lcom/samsung/wakeupsetting/Wave;->LINE_THICKNESS:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 522
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 523
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 526
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, p0, Lcom/samsung/wakeupsetting/Wave;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/wakeupsetting/Wave;->mWidth:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/wakeupsetting/Wave;->mHeight:I

    int-to-float v4, v4

    sget-object v5, Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShader:Landroid/graphics/Shader;

    .line 528
    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 529
    .local v10, "interpolator":Landroid/view/animation/Interpolator;
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-direct {v0, p0, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;-><init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    .line 531
    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    .end local v10    # "interpolator":Landroid/view/animation/Interpolator;
    invoke-direct {v10}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 532
    .restart local v10    # "interpolator":Landroid/view/animation/Interpolator;
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-direct {v0, p0, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;-><init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    .line 533
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-direct {v0, p0, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;-><init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    .line 534
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-direct {v0, p0, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;-><init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTimeTracker:Lcom/samsung/wakeupsetting/Wave$Anim;

    .line 536
    new-array v0, v13, [Lcom/samsung/wakeupsetting/Wave$Anim;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    .line 537
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v13, :cond_1

    .line 538
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    new-instance v1, Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-direct {v1, p0, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;-><init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V

    aput-object v1, v0, v8

    .line 537
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 541
    :cond_1
    const/4 v12, 0x7

    .line 542
    .local v12, "shift":I
    const/4 v9, 0x3

    .line 543
    .local v9, "index":I
    const/4 v8, 0x0

    :goto_2
    const/16 v0, 0x1e

    if-ge v8, v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I

    aput v9, v0, v8

    .line 545
    if-lez v8, :cond_2

    rem-int v0, v8, v12

    if-nez v0, :cond_2

    .line 546
    add-int/lit8 v0, v9, 0x1

    rem-int/lit8 v9, v0, 0x4

    .line 543
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 550
    :cond_3
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/Wave$DrawThread;-><init>(Lcom/samsung/wakeupsetting/Wave;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    .line 551
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    const-string/jumbo v1, "WaveView.DrawThread"

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->setName(Ljava/lang/String;)V

    .line 552
    return-void
.end method

.method private mapRequest(Lcom/samsung/wakeupsetting/Wave$Request;)Z
    .locals 3
    .param p1, "request"    # Lcom/samsung/wakeupsetting/Wave$Request;

    .prologue
    const/4 v2, 0x0

    .line 391
    const/4 v0, 0x1

    .line 392
    .local v0, "ret":Z
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_0

    .line 393
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 394
    const/4 v1, 0x3

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 395
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 408
    :goto_0
    return v0

    .line 396
    :cond_0
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_1

    .line 397
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 398
    iput v2, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 399
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    goto :goto_0

    .line 400
    :cond_1
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v1, :cond_2

    .line 401
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 402
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 403
    iput v2, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    goto :goto_0

    .line 405
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyView(I)V
    .locals 2
    .param p1, "response"    # I

    .prologue
    .line 555
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mResponseHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 556
    .local v0, "m":Landroid/os/Message;
    iget v1, p0, Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 557
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 558
    return-void
.end method

.method private paintBackground()V
    .locals 3

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 257
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 258
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 259
    if-eqz v0, :cond_0

    .line 260
    :try_start_1
    sget v1, Lcom/samsung/wakeupsetting/Wave;->COLOR_INNER_BG:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 262
    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    if-eqz v0, :cond_1

    .line 265
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 267
    :cond_1
    return-void

    .line 262
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 264
    :catchall_1
    move-exception v1

    if-eqz v0, :cond_2

    .line 265
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    invoke-virtual {v2, v0}, Landroid/view/TextureView;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    throw v1
.end method

.method private pendRequest(Lcom/samsung/wakeupsetting/Wave$Request;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/wakeupsetting/Wave$Request;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NONE:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne v0, v1, :cond_0

    .line 376
    const-string/jumbo v0, "Wave"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Pending request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave;->mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    const-string/jumbo v0, "Wave"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignoring request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processPendingRequests()V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/Wave;->mapRequest(Lcom/samsung/wakeupsetting/Wave$Request;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->NONE:Lcom/samsung/wakeupsetting/Wave$Request;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mPendingRequest:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 386
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    .line 388
    :cond_0
    return-void
.end method


# virtual methods
.method public changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/wakeupsetting/Wave$Request;

    .prologue
    .line 304
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    monitor-enter v1

    .line 306
    :try_start_0
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$State:[I

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/Wave$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 366
    :cond_0
    :goto_0
    :pswitch_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    const-string/jumbo v0, "Wave"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "(Request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    return-void

    .line 308
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v0, :cond_1

    .line 309
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave;->clearPendingRequests()V

    .line 310
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 311
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 312
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->changeTrainState(Lcom/samsung/wakeupsetting/Wave$Request;)Lcom/samsung/wakeupsetting/Wave$State;

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 314
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 315
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    goto :goto_0

    .line 320
    :pswitch_2
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->START:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v0, :cond_0

    .line 321
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    goto :goto_0

    .line 324
    :pswitch_3
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v0, :cond_2

    .line 325
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    goto :goto_0

    .line 327
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->pendRequest(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 331
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->mapRequest(Lcom/samsung/wakeupsetting/Wave$Request;)Z

    .line 332
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    goto :goto_0

    .line 335
    :pswitch_5
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v0, :cond_3

    .line 337
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 338
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 339
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V

    goto/16 :goto_0

    .line 344
    :cond_3
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->START:Lcom/samsung/wakeupsetting/Wave$Request;

    if-ne p1, v0, :cond_0

    .line 345
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 346
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    goto/16 :goto_0

    .line 353
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->changeTrainState(Lcom/samsung/wakeupsetting/Wave$Request;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    goto/16 :goto_0

    .line 356
    :pswitch_7
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    if-eq p1, v0, :cond_4

    .line 357
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave;->pendRequest(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto/16 :goto_0

    .line 359
    :cond_4
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS_HOLD:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getCurrentState()Lcom/samsung/wakeupsetting/Wave$State;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    return-object v0
.end method

.method public getCurrentTrial()I
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->start()V

    .line 237
    return-void
.end method

.method public reset(Z)V
    .locals 3
    .param p1, "force"    # Z

    .prologue
    .line 470
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    monitor-enter v2

    .line 471
    :try_start_0
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 472
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 473
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I

    .line 475
    if-eqz p1, :cond_0

    .line 476
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$State;->NONE:Lcom/samsung/wakeupsetting/Wave$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;

    .line 477
    sget-object v1, Lcom/samsung/wakeupsetting/Wave$TrainState;->NONE:Lcom/samsung/wakeupsetting/Wave$TrainState;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 478
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I

    .line 481
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->reset()V

    .line 482
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->reset()V

    .line 483
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->reset()V

    .line 484
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mTimeTracker:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->reset()V

    .line 485
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->reset()V

    .line 485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488
    .end local v0    # "i":I
    :cond_0
    monitor-exit v2

    .line 489
    return-void

    .line 488
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAdapter(Lcom/samsung/wakeupsetting/WaveViewAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/samsung/wakeupsetting/WaveViewAdapter;

    .prologue
    .line 300
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;

    .line 301
    return-void
.end method

.method public setSize(II)V
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 288
    iget-object v8, p0, Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;

    monitor-enter v8

    .line 289
    :try_start_0
    iput p1, p0, Lcom/samsung/wakeupsetting/Wave;->mWidth:I

    .line 290
    iput p2, p0, Lcom/samsung/wakeupsetting/Wave;->mHeight:I

    .line 293
    new-instance v0, Landroid/graphics/LinearGradient;

    const/4 v1, 0x0

    div-int/lit8 v2, p2, 0x2

    int-to-float v2, v2

    mul-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    int-to-float v4, p2

    const v5, 0x3f19999a    # 0.6f

    mul-float/2addr v4, v5

    sget-object v5, Lcom/samsung/wakeupsetting/Wave;->COLOR_SWEEP:[I

    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mShader:Landroid/graphics/Shader;

    .line 295
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave;->mShader:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 296
    monitor-exit v8

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateRunState(Lcom/samsung/wakeupsetting/Wave$RunState;)V

    .line 241
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave;->paintBackground()V

    .line 247
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateRunState(Lcom/samsung/wakeupsetting/Wave$RunState;)V

    .line 252
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave;->mThread:Lcom/samsung/wakeupsetting/Wave$DrawThread;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$RunState;->IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateRunState(Lcom/samsung/wakeupsetting/Wave$RunState;)V

    .line 285
    return-void
.end method
