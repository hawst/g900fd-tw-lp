.class public Lcom/samsung/wakeupsetting/BluetoothManager;
.super Ljava/lang/Object;
.source "BluetoothManager.java"


# static fields
.field private static final PROXY_NOT_ACQUIRED:I = 0x0

.field private static final PROXY_RELEASE_REQUESTED:I = 0x2

.field private static final PROXY_REQUESTED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SV_BluetoothManager"

.field private static mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;


# instance fields
.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mHeadset:Landroid/bluetooth/BluetoothHeadset;

.field private mProxyState:I

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private final mSystemUpdateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/wakeupsetting/BluetoothManager;->mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    .line 31
    new-instance v0, Lcom/samsung/wakeupsetting/BluetoothManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/BluetoothManager$1;-><init>(Lcom/samsung/wakeupsetting/BluetoothManager;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mSystemUpdateReceiver:Landroid/content/BroadcastReceiver;

    .line 90
    new-instance v0, Lcom/samsung/wakeupsetting/BluetoothManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/BluetoothManager$2;-><init>(Lcom/samsung/wakeupsetting/BluetoothManager;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 209
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 210
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 211
    iput v3, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    .line 212
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/BluetoothManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/BluetoothManager;->handleStateChange(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/BluetoothManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/wakeupsetting/BluetoothManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothHeadset;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/wakeupsetting/BluetoothManager;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothHeadset;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/wakeupsetting/BluetoothManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/wakeupsetting/BluetoothManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/BluetoothManager;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/BluetoothManager;->getConnectedDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method private getConnectedDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 2

    .prologue
    .line 216
    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 221
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 222
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 226
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/wakeupsetting/BluetoothManager;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/samsung/wakeupsetting/BluetoothManager;->mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Lcom/samsung/wakeupsetting/BluetoothManager;

    invoke-direct {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;-><init>()V

    sput-object v0, Lcom/samsung/wakeupsetting/BluetoothManager;->mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;

    .line 134
    :cond_0
    sget-object v0, Lcom/samsung/wakeupsetting/BluetoothManager;->mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;

    return-object v0
.end method

.method private handleStateChange(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 88
    :goto_0
    :pswitch_0
    return-void

    .line 78
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/BluetoothManager;->stopSCO()V

    goto :goto_0

    .line 84
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/BluetoothManager;->startSCO()V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static isBluetoothAudioOn()Z
    .locals 4

    .prologue
    .line 152
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 153
    .local v0, "aMgr":Landroid/media/AudioManager;
    const-string/jumbo v1, "SV_BluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isBluetoothAudioOn "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    return v1
.end method

.method public static isHeadsetConnected()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 140
    const/4 v0, 0x1

    .line 141
    .local v0, "val":Z
    const/4 v1, 0x2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v1

    if-eq v3, v1, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 145
    :cond_0
    const-string/jumbo v1, "SV_BluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "isHeadsetConnected(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    return v0
.end method

.method public static setBluetoothAudioOn(Z)V
    .locals 4
    .param p0, "value"    # Z

    .prologue
    .line 186
    const-string/jumbo v1, "SV_BluetoothManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setBluetoothAudioOn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 189
    .local v0, "aMgr":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 190
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 232
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mSystemUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v1, :cond_1

    .line 238
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    invoke-static {v4}, Lcom/samsung/wakeupsetting/BluetoothManager;->setBluetoothAudioOn(Z)V

    .line 242
    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    .line 247
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 248
    iput v4, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    .line 249
    iput-object v5, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    .line 254
    :goto_1
    sput-object v5, Lcom/samsung/wakeupsetting/BluetoothManager;->mInstance:Lcom/samsung/wakeupsetting/BluetoothManager;

    .line 255
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 251
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mProxyState:I

    goto :goto_1
.end method

.method public init()V
    .locals 3

    .prologue
    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 197
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 198
    const-string/jumbo v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 199
    const-string/jumbo v1, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 200
    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 201
    const-string/jumbo v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 202
    const-string/jumbo v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mSystemUpdateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 205
    return-void
.end method

.method public startSCO()V
    .locals 2

    .prologue
    .line 159
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->setBluetoothAudioOn(Z)V

    .line 163
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    .line 168
    :cond_0
    return-void
.end method

.method public stopSCO()V
    .locals 2

    .prologue
    .line 172
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->setBluetoothAudioOn(Z)V

    .line 174
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    .line 175
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mHeadset:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/BluetoothManager;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    .line 181
    :cond_0
    return-void
.end method
