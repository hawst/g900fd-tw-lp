.class public Lcom/samsung/wakeupsetting/ResetCustomWakeupSettingsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ResetCustomWakeupSettingsBroadcastReceiver.java"


# static fields
.field private static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field public static final TAG:Ljava/lang/String; = " VERTHANDI : "

.field private static final VOICEINPUTCONTROL_ALARM:Ljava/lang/String; = "voice_input_control_alarm"

.field private static final VOICEINPUTCONTROL_CAMERA:Ljava/lang/String; = "voice_input_control_camera"

.field private static final VOICEINPUTCONTROL_INCOMMING_CALL:Ljava/lang/String; = "voice_input_control_incomming_calls"

.field private static final VOICEINPUTCONTROL_MUSIC:Ljava/lang/String; = "voice_input_control_music"


# instance fields
.field private mCustomWakeupSettingActivity:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private resetVoiceControl()V
    .locals 5

    .prologue
    .line 47
    const-string/jumbo v2, " VERTHANDI : "

    const-string/jumbo v3, "resetVoiceControl() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const-string/jumbo v2, " VERTHANDI : "

    const-string/jumbo v3, "isKitkatPhoneGUI() :TRUE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v1, 0x1

    .line 51
    .local v1, "voice_control_master":I
    const/4 v0, 0x0

    .line 57
    .local v0, "voice_control_item":I
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control_incomming_calls"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 60
    const-string/jumbo v2, " VERTHANDI : "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VOICEINPUTCONTROL_INCOMMING_CALL : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control_alarm"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 65
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control_camera"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 68
    const-string/jumbo v2, " VERTHANDI : "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "VOICEINPUTCONTROL_CAMERA : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control_music"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 73
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/midas/VlingoApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "voice_input_control"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 76
    const-string/jumbo v2, " VERTHANDI : "

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "KEY_VOICE_INPUT_CONTROL : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void

    .line 53
    .end local v0    # "voice_control_item":I
    .end local v1    # "voice_control_master":I
    :cond_0
    const-string/jumbo v2, " VERTHANDI : "

    const-string/jumbo v3, "isKitkatPhoneGUI() :FALSE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v1, 0x0

    .line 55
    .restart local v1    # "voice_control_master":I
    const/4 v0, 0x1

    .restart local v0    # "voice_control_item":I
    goto/16 :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/ResetCustomWakeupSettingsBroadcastReceiver;->resetVoiceControl()V

    .line 33
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/ResetCustomWakeupSettingsBroadcastReceiver;->mCustomWakeupSettingActivity:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .line 37
    iget-object v0, p0, Lcom/samsung/wakeupsetting/ResetCustomWakeupSettingsBroadcastReceiver;->mCustomWakeupSettingActivity:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetWakeup()V

    .line 38
    return-void
.end method
