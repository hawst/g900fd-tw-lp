.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingEndPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1591
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x4

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1594
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    if-nez v4, :cond_0

    .line 1595
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1596
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v4, "com.android.settings"

    const-string/jumbo v5, "com.android.settings.ChooseLockGeneric"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1597
    const-string/jumbo v4, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v2, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1598
    const-string/jumbo v4, "lockscreen.biometric_weak_with_voice_fallback"

    invoke-virtual {v2, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1599
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startActivity(Landroid/content/Intent;)V

    .line 1600
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 1712
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1602
    :cond_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 1603
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_recog.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1604
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_search.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1606
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_recog_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_recog.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1608
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_search_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_VoiceKey_AP_search.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1610
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    goto :goto_0

    .line 1613
    :cond_1
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    if-ne v4, v9, :cond_6

    .line 1614
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    if-eq v4, v6, :cond_2

    .line 1616
    const-string/jumbo v4, "samsung_wakeup_engine_enable"

    invoke-static {v4, v10}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1617
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v5, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 1620
    :cond_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    if-eq v4, v6, :cond_3

    .line 1621
    const-string/jumbo v4, "samsung_wakeup_engine_enable"

    invoke-static {v4, v9}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 1632
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1655
    :cond_4
    :goto_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2100(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1656
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 1657
    sput-boolean v9, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->enrolled:Z

    .line 1658
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z
    invoke-static {v4, v10}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2102(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z

    .line 1661
    :cond_5
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setResult(I)V

    .line 1662
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "com.vlingo.midas"

    const-string/jumbo v6, "WKCE"

    invoke-static {v4, v5, v6}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1664
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 1666
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1667
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/GoogleNow_AP_recog.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1668
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/GoogleNow_AP_search.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1670
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/GoogleNow_AP_recog_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/GoogleNow_AP_recog.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1672
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/GoogleNow_AP_search_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/GoogleNow_AP_search.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1705
    :goto_3
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButtonClicked:Z
    invoke-static {v4, v9}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2302(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z

    .line 1707
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v4, "com.samsung.CUSTOM_COMMAND_CHANGED"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1708
    .local v0, "forGSA":Landroid/content/Intent;
    const-string/jumbo v4, "isFromGSA"

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1709
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v4, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1711
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v4, v10}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1502(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z

    goto/16 :goto_0

    .line 1624
    .end local v0    # "forGSA":Landroid/content/Intent;
    :cond_6
    const-string/jumbo v4, "samsung_multi_engine_enable"

    invoke-static {v4, v10}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1625
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    iget-boolean v4, v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-nez v4, :cond_3

    .line 1626
    sget-object v4, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    const-class v5, Lcom/vlingo/midas/phrasespotter/SamsungPhraseSpotter;

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->registerHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;Ljava/lang/Class;)V

    .line 1627
    const-string/jumbo v4, "samsung_multi_engine_enable"

    invoke-static {v4, v9}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 1638
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    iget-boolean v4, v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-nez v4, :cond_4

    .line 1643
    const-string/jumbo v4, "CustomCommandRecordingActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "displayRecordingEndPopup, commandType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1644
    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2000()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v3, v4, -0x2

    .line 1645
    .local v3, "nLen":I
    const-string/jumbo v4, "CustomCommandRecordingActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "displayRecordingPopup, nLen : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1646
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    if-ge v1, v3, :cond_7

    .line 1647
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2000()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".bin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2000()[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->fileRename(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1649
    :cond_7
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2000()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".pcm"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2000()[Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v8, v3, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".pcm"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->fileRename(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1675
    .end local v1    # "i":I
    .end local v3    # "nLen":I
    :cond_8
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_0.wav"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1676
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_1.wav"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1677
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_2.wav"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1678
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_3.wav"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1679
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/ListenToMyVoice.pcm"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1681
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1682
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1683
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog.bin"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1684
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_Deep_search.bin"

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->deleteData(Ljava/lang/String;)V

    .line 1686
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_0_tmp.wav"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/user0_0.wav"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1687
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_1_tmp.wav"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/user0_1.wav"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1688
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_2_tmp.wav"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/user0_2.wav"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/user0_3_tmp.wav"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/user0_3.wav"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/ListenToMyVoice_tmp.pcm"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/ListenToMyVoice.pcm"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1693
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_AP_recog_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_AP_search_tmp.raw"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1697
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog_tmp.bin"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog.bin"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/UDT_Always_Deep_search_tmp.bin"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/UDT_Always_Deep_search.bin"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    .line 1702
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    const-string/jumbo v5, "/data/data/com.vlingo.midas/THRESHOLD_tmp.bin"

    const-string/jumbo v6, "/data/data/com.vlingo.midas/THRESHOLD.bin"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->renameData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1632
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
