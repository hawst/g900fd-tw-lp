.class public interface abstract Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;
.super Ljava/lang/Object;
.source "WaveViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/WaveViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V
.end method

.method public abstract onProcessingStarted()V
.end method

.method public abstract onTrainingComplete(ILcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V
.end method

.method public abstract onTrainingStarted(I)V
.end method
