.class Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;
.super Ljava/lang/Object;
.source "VoiceWakeupSettingsEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->showWakeupLockScreenDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$000(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 194
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->updateUIVoiceWakeupSettings(Z)V

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "checkBoxPrefForAlert"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 197
    const-string/jumbo v1, "isCheckBoxChecked"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 198
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 199
    return-void
.end method
