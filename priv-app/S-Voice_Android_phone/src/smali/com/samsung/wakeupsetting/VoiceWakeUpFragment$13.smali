.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;
.super Landroid/widget/ArrayAdapter;
.source "VoiceWakeUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->initializeAutoFunctionAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

.field final synthetic val$paddingLeftRight:I

.field final synthetic val$paddingTopBottom:I


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Landroid/content/Context;I[Ljava/lang/String;II)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # [Ljava/lang/String;

    .prologue
    .line 712
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    iput p5, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingLeftRight:I

    iput p6, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingTopBottom:I

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 716
    if-nez p2, :cond_0

    .line 717
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 718
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090003

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 723
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .local v2, "row":Landroid/widget/TextView;
    :goto_0
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$color;->edit_text_color_santos_theme:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 731
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 732
    const/high16 v3, 0x41b00000    # 22.0f

    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 733
    .local v1, "padding":I
    invoke-virtual {v2, v1, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 734
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v2, v7, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 740
    .end local v1    # "padding":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 741
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 742
    return-object v2

    .end local v2    # "row":Landroid/widget/TextView;
    :cond_0
    move-object v2, p2

    .line 720
    check-cast v2, Landroid/widget/TextView;

    .restart local v2    # "row":Landroid/widget/TextView;
    goto :goto_0

    .line 736
    :cond_1
    iget v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingLeftRight:I

    iget v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingTopBottom:I

    iget v5, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingLeftRight:I

    iget v6, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->val$paddingTopBottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 737
    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$13;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$dimen;->wakeup_dialog_text_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v2, v7, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 747
    const/4 v0, 0x1

    return v0
.end method
