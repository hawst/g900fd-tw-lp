.class public Lcom/samsung/wakeupsetting/CustomWakeupSetting;
.super Landroid/app/Activity;
.source "CustomWakeupSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;
    }
.end annotation


# instance fields
.field private alert:Landroid/app/AlertDialog;

.field private customCommandlist:Landroid/view/View;

.field final wakeupCommand:[Ljava/lang/String;

.field wakeupList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_voice_talk:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_1:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_2:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_3:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->wakeupCommand:[Ljava/lang/String;

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/CustomWakeupSetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSetting;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->showCustomCommandlist()V

    return-void
.end method

.method private showCustomCommandlist()V
    .locals 6

    .prologue
    .line 85
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;>;"
    new-instance v1, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_voice_talk:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Hi, Galaxy"

    invoke-direct {v1, v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v1, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_1:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Check schedule"

    invoke-direct {v1, v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v1, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_2:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Play radio"

    invoke-direct {v1, v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v1, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;

    sget v2, Lcom/vlingo/midas/R$string;->wakeup_auto_function_3:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-direct {v1, v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;

    const v3, 0x1090004

    move-object v1, p0

    move-object v2, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSetting;Landroid/content/Context;ILjava/util/List;Ljava/util/List;)V

    .line 124
    .local v0, "adapter":Landroid/widget/ArrayAdapter;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->wakeupList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    sget v0, Lcom/vlingo/midas/R$layout;->wakeupcommand:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->setContentView(I)V

    .line 42
    sget v0, Lcom/vlingo/midas/R$id;->wakeupList:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->wakeupList:Landroid/widget/ListView;

    .line 44
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 50
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 56
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 62
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 63
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string/jumbo v1, "WakeUp Command is available only when Settings > Security > Screen Lock is set as None only. Set?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->alert_yes:I

    new-instance v3, Lcom/samsung/wakeupsetting/CustomWakeupSetting$2;

    invoke-direct {v3, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$2;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSetting;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->alert_no:I

    new-instance v3, Lcom/samsung/wakeupsetting/CustomWakeupSetting$1;

    invoke-direct {v3, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$1;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSetting;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->alert:Landroid/app/AlertDialog;

    .line 80
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 81
    return-void
.end method
