.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CustomCommandRecordingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextToSpeechBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 2611
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p2, "x1"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;

    .prologue
    .line 2611
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2613
    if-nez p2, :cond_1

    .line 2628
    :cond_0
    :goto_0
    return-void

    .line 2616
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2617
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2618
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 2619
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2621
    :try_start_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2622
    :catch_0
    move-exception v1

    .line 2624
    .local v1, "excp":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v2, "CustomCommandRecordingActivity"

    const-string/jumbo v3, "TTS receiver is already unregistered."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
