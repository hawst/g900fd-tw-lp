.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchResetAlertDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0

    .prologue
    .line 959
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 963
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetPreferenceValue()V
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1300(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 995
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1400(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 997
    const/4 v2, 0x4

    new-array v0, v2, [I

    .line 998
    .local v0, "assignCommandArray":[I
    const/4 v2, 0x4

    new-array v1, v2, [I

    .line 1001
    .local v1, "resetCheck":[I
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$700(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v2

    sget-object v3, Lcom/samsung/voiceshell/VoiceEngine;->typeDefine:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v5}, Lcom/samsung/voiceshell/VoiceEngine;->functionAssignment(Ljava/lang/String;[II)I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 1005
    aput v4, v0, v5

    .line 1006
    aput v4, v0, v6

    .line 1007
    aput v4, v0, v7

    .line 1008
    aput v4, v0, v8

    .line 1011
    :cond_0
    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    packed-switch v2, :pswitch_data_0

    .line 1096
    :goto_0
    const-string/jumbo v2, "kew_wake_up_and_auto_function1"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v5

    .line 1097
    const-string/jumbo v2, "kew_wake_up_and_auto_function2"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 1098
    const-string/jumbo v2, "kew_wake_up_and_auto_function3"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v7

    .line 1099
    const-string/jumbo v2, "kew_wake_up_and_auto_function4"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->getInt(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v8

    .line 1101
    aget v2, v1, v5

    if-eq v2, v4, :cond_1

    aget v2, v1, v6

    if-eq v2, v4, :cond_1

    aget v2, v1, v7

    if-eq v2, v4, :cond_1

    aget v2, v1, v8

    if-ne v2, v4, :cond_2

    .line 1103
    :cond_1
    const-string/jumbo v2, "samsung_multi_engine_enable"

    invoke-static {v2, v5}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1105
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-boolean v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-nez v2, :cond_2

    .line 1107
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->unregisterHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)V

    .line 1108
    const-string/jumbo v2, "samsung_multi_engine_enable"

    invoke-static {v2, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 1119
    :cond_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$700(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v2

    sget-object v3, Lcom/samsung/voiceshell/VoiceEngine;->typeDefine:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v6}, Lcom/samsung/voiceshell/VoiceEngine;->functionAssignment(Ljava/lang/String;[II)I

    .line 1122
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 1123
    return-void

    .line 1013
    :pswitch_0
    const-string/jumbo v2, "kew_wake_up_and_auto_function1"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1014
    aput v4, v0, v5

    .line 1015
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-boolean v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v2, :cond_3

    .line 1017
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user1_0.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1018
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user1_1.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1019
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user1_2.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1020
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user1_3.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1024
    :cond_3
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/kwd_2.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1025
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/modelStatus_2.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1026
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR_2.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1027
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR4_2.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1028
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/lastEnrollUtt_2.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1029
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/realLastEnrollUtt_2.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1033
    :pswitch_1
    const-string/jumbo v2, "kew_wake_up_and_auto_function2"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1034
    aput v4, v0, v6

    .line 1035
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-boolean v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v2, :cond_4

    .line 1037
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user2_0.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1038
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user2_1.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1039
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user2_2.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1040
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user2_3.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1044
    :cond_4
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/kwd_3.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1045
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/modelStatus_3.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1046
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR_3.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1047
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR4_3.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1048
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/lastEnrollUtt_3.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1049
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/realLastEnrollUtt_3.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1053
    :pswitch_2
    const-string/jumbo v2, "kew_wake_up_and_auto_function3"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1054
    aput v4, v0, v7

    .line 1055
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-boolean v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v2, :cond_5

    .line 1057
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user3_0.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1058
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user3_1.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1059
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user3_2.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1060
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user3_3.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1064
    :cond_5
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/kwd_4.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1065
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/modelStatus_4.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1066
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR_4.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1067
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR4_4.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1068
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/lastEnrollUtt_4.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1069
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/realLastEnrollUtt_4.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1073
    :pswitch_3
    const-string/jumbo v2, "kew_wake_up_and_auto_function4"

    invoke-static {v2, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1074
    aput v4, v0, v8

    .line 1075
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-boolean v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v2, :cond_6

    .line 1077
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user4_0.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1078
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user4_1.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1079
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user4_2.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1080
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user4_3.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1084
    :cond_6
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/kwd_5.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1085
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/modelStatus_5.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1086
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR_5.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1087
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR4_5.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1088
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/lastEnrollUtt_5.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1089
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/realLastEnrollUtt_5.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1011
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
