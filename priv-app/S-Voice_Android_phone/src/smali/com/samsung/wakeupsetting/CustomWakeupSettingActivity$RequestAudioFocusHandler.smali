.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;
.super Landroid/os/Handler;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestAudioFocusHandler"
.end annotation


# instance fields
.field private final mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 2056
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2057
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;->mActivity:Ljava/lang/ref/WeakReference;

    .line 2058
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2061
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .line 2063
    .local v1, "mainActivity":Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
    if-eqz v1, :cond_0

    .line 2065
    :try_start_0
    iget-boolean v2, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v2, :cond_1

    .line 2066
    sget-object v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->m_UDTSIDlastEnroll:[Ljava/lang/String;

    sget v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2074
    :goto_0
    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 2075
    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 2076
    if-eqz p1, :cond_0

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 2078
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/app/Dialog;

    .line 2079
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2086
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    :goto_1
    return-void

    .line 2068
    :cond_1
    :try_start_2
    sget-object v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->m_realLastEnroll:[Ljava/lang/String;

    sget v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2070
    :catch_0
    move-exception v2

    goto :goto_0

    .line 2080
    :catch_1
    move-exception v2

    goto :goto_1
.end method
