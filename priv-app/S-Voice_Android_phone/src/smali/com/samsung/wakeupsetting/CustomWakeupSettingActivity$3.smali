.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;
.super Landroid/widget/ArrayAdapter;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 416
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;>;"
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 420
    const/4 v2, 0x0

    .line 421
    .local v2, "pos":I
    const/4 v3, 0x0

    .line 424
    .local v3, "prefs":Landroid/content/SharedPreferences;
    if-nez p2, :cond_0

    .line 425
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 427
    .local v1, "inflater":Landroid/view/LayoutInflater;
    sget v5, Lcom/vlingo/midas/R$layout;->function_list_item:I

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 432
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .local v4, "view":Landroid/widget/TextView;
    :goto_0
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v5, v5, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v5, v5, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v6, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v6, v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    sget v7, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v6, v6, v7

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 436
    const-string/jumbo v5, "Function"

    const-string/jumbo v6, ""

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "functionName":Ljava/lang/String;
    return-object v4

    .end local v0    # "functionName":Ljava/lang/String;
    .end local v4    # "view":Landroid/widget/TextView;
    :cond_0
    move-object v4, p2

    .line 429
    check-cast v4, Landroid/widget/TextView;

    .restart local v4    # "view":Landroid/widget/TextView;
    goto :goto_0
.end method
