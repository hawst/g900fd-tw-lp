.class Lcom/samsung/wakeupsetting/Wave$DrawThread;
.super Ljava/lang/Thread;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawThread"
.end annotation


# static fields
.field static final SLEEP_TIME:I = 0x64

.field static final WAIT_TIME:I = 0xfa0


# instance fields
.field mLock:Ljava/lang/Object;

.field mNextRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

.field mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

.field mTimeStamp:J

.field final synthetic this$0:Lcom/samsung/wakeupsetting/Wave;


# direct methods
.method public constructor <init>(Lcom/samsung/wakeupsetting/Wave;)V
    .locals 1

    .prologue
    .line 575
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 577
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$RunState;->IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mNextRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 578
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mLock:Ljava/lang/Object;

    .line 579
    return-void
.end method

.method private draw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 866
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->drawBackground(Landroid/graphics/Canvas;)V

    .line 867
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->drawSnapShotLayer(Landroid/graphics/Canvas;)V

    .line 868
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->drawRecordLayer(Landroid/graphics/Canvas;)V

    .line 869
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->drawProgressLayer(Landroid/graphics/Canvas;)V

    .line 870
    return-void
.end method

.method private drawBackground(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 873
    sget v0, Lcom/samsung/wakeupsetting/Wave;->COLOR_INNER_BG:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 875
    return-void
.end method

.method private drawProgressLayer(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v13, v0

    .line 1050
    .local v13, "y":F
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    int-to-float v1, v0

    .line 1051
    .local v1, "x":F
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_4

    .line 1052
    const/4 v6, 0x3

    .line 1053
    .local v6, "T":I
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    sub-int v11, v6, v0

    .line 1054
    .local v11, "t":I
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v0, v2, :cond_1

    .line 1057
    :cond_0
    add-int/lit8 v11, v11, -0x1

    .line 1060
    :cond_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-gt v7, v11, :cond_3

    .line 1061
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    sub-int v3, v6, v7

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1062
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    if-nez v0, :cond_2

    .line 1063
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->toFadeInAlpha()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1065
    :cond_2
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f200000    # 0.625f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v13, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1066
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fd00000    # 1.625f

    mul-float/2addr v0, v2

    add-float/2addr v1, v0

    .line 1060
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1070
    :cond_3
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x420e0000    # 35.5f

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float v1, v0, v2

    .line 1071
    const/4 v7, 0x0

    :goto_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    if-ge v7, v0, :cond_f

    .line 1072
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    aget v2, v2, v7

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1074
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f200000    # 0.625f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v13, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1075
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fd00000    # 1.625f

    mul-float/2addr v0, v2

    sub-float/2addr v1, v0

    .line 1071
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1077
    .end local v6    # "T":I
    .end local v7    # "i":I
    .end local v11    # "t":I
    :cond_4
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_5

    .line 1078
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x420e0000    # 35.5f

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float v1, v0, v2

    .line 1081
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    if-ge v7, v0, :cond_f

    .line 1082
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    aget v2, v2, v7

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1083
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v0, v0, v7

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->toDistance(I)I

    move-result v0

    int-to-float v0, v0

    add-float v12, v1, v0

    .line 1085
    .local v12, "tx":F
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f200000    # 0.625f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v13, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1086
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fd00000    # 1.625f

    mul-float/2addr v0, v2

    sub-float/2addr v1, v0

    .line 1081
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1088
    .end local v7    # "i":I
    .end local v12    # "tx":F
    :cond_5
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS_HOLD:Lcom/samsung/wakeupsetting/Wave$State;

    if-eq v0, v2, :cond_6

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->PROCESS:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_9

    .line 1089
    :cond_6
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    add-int v9, v0, v2

    .line 1091
    .local v9, "offsetX":I
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v9

    int-to-float v1, v0

    .line 1092
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1094
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_3
    const/4 v0, 0x1

    if-ge v7, v0, :cond_7

    .line 1095
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v3

    const/4 v4, 0x0

    aget v3, v3, v4

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1096
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v2, v13, v0

    const/high16 v0, 0x3f800000    # 1.0f

    add-float v4, v13, v0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1097
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1094
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1100
    :cond_7
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v10

    .line 1102
    .local v10, "snapshot":[F
    const/4 v7, 0x0

    :goto_4
    const/16 v0, 0x1e

    if-ge v7, v0, :cond_f

    .line 1104
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v3

    aget v3, v3, v7

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1106
    aget v0, v10, v7

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float v8, v0, v2

    .line 1108
    .local v8, "length":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v8, v0

    if-gez v0, :cond_8

    .line 1109
    const/high16 v8, 0x3f800000    # 1.0f

    .line 1110
    :cond_8
    sub-float v2, v13, v8

    add-float v4, v13, v8

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1111
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1102
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1115
    .end local v7    # "i":I
    .end local v8    # "length":F
    .end local v9    # "offsetX":I
    .end local v10    # "snapshot":[F
    :cond_9
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_c

    .line 1116
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    add-int v9, v0, v2

    .line 1118
    .restart local v9    # "offsetX":I
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v9

    int-to-float v1, v0

    .line 1119
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1122
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_5
    const/4 v0, 0x1

    if-ge v7, v0, :cond_a

    .line 1123
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v2, v13, v0

    const/high16 v0, 0x3f800000    # 1.0f

    add-float v4, v13, v0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1124
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1122
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 1127
    :cond_a
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v10

    .line 1129
    .restart local v10    # "snapshot":[F
    const/4 v7, 0x0

    :goto_6
    const/16 v0, 0x1e

    if-ge v7, v0, :cond_f

    .line 1132
    aget v0, v10, v7

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float v8, v0, v2

    .line 1134
    .restart local v8    # "length":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, v8, v0

    if-gez v0, :cond_b

    .line 1135
    const/high16 v8, 0x3f800000    # 1.0f

    .line 1136
    :cond_b
    sub-float v2, v13, v8

    add-float v4, v13, v8

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1137
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1129
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 1141
    .end local v7    # "i":I
    .end local v8    # "length":F
    .end local v9    # "offsetX":I
    .end local v10    # "snapshot":[F
    :cond_c
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_f

    .line 1142
    const/4 v6, 0x3

    .line 1143
    .restart local v6    # "T":I
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    sub-int v11, v6, v0

    .line 1145
    .restart local v11    # "t":I
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v0, v2, :cond_d

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1d

    int-to-float v8, v0

    .line 1147
    .restart local v8    # "length":F
    :goto_7
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_8
    if-gt v7, v11, :cond_e

    .line 1148
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    sub-int v3, v6, v7

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1149
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    sub-int v2, v6, v7

    aget-object v0, v0, v2

    invoke-virtual {v0, v8}, Lcom/samsung/wakeupsetting/Wave$Anim;->toReverseDistance(F)F

    move-result v0

    add-float v12, v1, v0

    .line 1151
    .restart local v12    # "tx":F
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f200000    # 0.625f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v13, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1152
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fd00000    # 1.625f

    mul-float/2addr v0, v2

    add-float/2addr v1, v0

    .line 1147
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 1145
    .end local v7    # "i":I
    .end local v8    # "length":F
    .end local v12    # "tx":F
    :cond_d
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mWidth:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$1600(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    int-to-float v8, v0

    goto :goto_7

    .line 1155
    .restart local v7    # "i":I
    .restart local v8    # "length":F
    :cond_e
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v0, v2, :cond_f

    .line 1156
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x420e0000    # 35.5f

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float v1, v0, v2

    .line 1157
    const/4 v7, 0x0

    :goto_9
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    if-ge v7, v0, :cond_f

    .line 1158
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    aget v2, v2, v7

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1160
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f200000    # 0.625f

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v13, v0, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1161
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3fd00000    # 1.625f

    mul-float/2addr v0, v2

    sub-float/2addr v1, v0

    .line 1157
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 1165
    .end local v6    # "T":I
    .end local v7    # "i":I
    .end local v8    # "length":F
    .end local v11    # "t":I
    :cond_f
    return-void
.end method

.method private drawRecordLayer(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v12, 0x1e

    const/high16 v11, 0x3f800000    # 1.0f

    .line 999
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v2

    rsub-int/lit8 v2, v2, 0x4

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x3fd00000    # 1.625f

    mul-float/2addr v2, v3

    add-float v9, v0, v2

    .line 1000
    .local v9, "offsetX":F
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v10, v0

    .line 1001
    .local v10, "y":F
    move v1, v9

    .line 1003
    .local v1, "x":F
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->BEGIN:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_1

    .line 1045
    :cond_0
    return-void

    .line 1005
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_4

    .line 1006
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v0, v2, :cond_2

    .line 1008
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1009
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->toFadeInAlpha()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1013
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v12, :cond_0

    .line 1015
    sub-float v2, v10, v11

    add-float v4, v10, v11

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1016
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1013
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1018
    .end local v7    # "i":I
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$TrainState;->RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v0, v2, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getCurrentAudio()[F

    move-result-object v6

    .line 1021
    .local v6, "data":[F
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1023
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_1
    array-length v0, v6

    if-ge v7, v0, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$1700(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    add-int/2addr v0, v7

    rem-int/lit8 v0, v0, 0x1e

    aget v0, v6, v0

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float v8, v0, v2

    .line 1026
    .local v8, "length":F
    cmpg-float v0, v8, v11

    if-gez v0, :cond_3

    .line 1027
    const/high16 v8, 0x3f800000    # 1.0f

    .line 1028
    :cond_3
    sub-float v2, v10, v8

    add-float v4, v10, v8

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1029
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1023
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1035
    .end local v6    # "data":[F
    .end local v7    # "i":I
    .end local v8    # "length":F
    :cond_4
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$State;->RESULT_FAILURE:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v0, v2, :cond_0

    .line 1036
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1037
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->toFadeInAlpha()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1039
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    if-ge v7, v12, :cond_0

    .line 1041
    sub-float v2, v10, v11

    add-float v4, v10, v11

    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1042
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v1, v0

    .line 1039
    add-int/lit8 v7, v7, 0x1

    goto :goto_2
.end method

.method private drawSnapShotLayer(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 878
    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_LEFT:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1800()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    rsub-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, -0x1

    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v5

    mul-int/2addr v3, v5

    int-to-float v3, v3

    const/high16 v5, 0x3fd00000    # 1.625f

    mul-float/2addr v3, v5

    add-float v13, v1, v3

    .line 879
    .local v13, "offsetX":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v15, v1

    .line 880
    .local v15, "y":F
    move v2, v13

    .line 882
    .local v2, "x":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v1, v3, :cond_10

    .line 883
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->SPAWN:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v1, v3, :cond_2

    .line 884
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    if-eqz v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v1

    if-lez v1, :cond_f

    .line 885
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aget v3, v3, v5

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 886
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3fd00000    # 1.625f

    mul-float/2addr v1, v3

    add-float/2addr v2, v1

    .line 887
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v14

    .line 889
    .local v14, "snapshot":[F
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const/4 v1, 0x1

    if-ge v11, v1, :cond_0

    .line 890
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v3, v15, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v5, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 891
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 889
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 894
    :cond_0
    const/4 v11, 0x0

    :goto_1
    array-length v1, v14

    add-int/lit8 v1, v1, -0x1

    if-ge v11, v1, :cond_f

    .line 895
    aget v1, v14, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v12, v1, v3

    .line 897
    .local v12, "length":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v12, v1

    if-gez v1, :cond_1

    .line 898
    const/high16 v12, 0x3f800000    # 1.0f

    .line 899
    :cond_1
    sub-float v3, v15, v12

    add-float v5, v15, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 900
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 894
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 903
    .end local v11    # "i":I
    .end local v12    # "length":F
    .end local v14    # "snapshot":[F
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->RECORD:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v1, v3, :cond_5

    .line 904
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    if-eqz v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v1

    if-lez v1, :cond_f

    .line 905
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aget v3, v3, v5

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 906
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3fd00000    # 1.625f

    mul-float/2addr v1, v3

    add-float/2addr v2, v1

    .line 907
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v14

    .line 908
    .restart local v14    # "snapshot":[F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/Wave;->mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    const v5, 0x3e4ccccd    # 0.2f

    invoke-virtual {v3, v5}, Lcom/samsung/wakeupsetting/Wave$Anim;->toFadeOutAlpha(F)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 910
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_2
    const/4 v1, 0x1

    if-ge v11, v1, :cond_3

    .line 911
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v3, v15, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v5, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 912
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 910
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 915
    :cond_3
    const/4 v11, 0x0

    :goto_3
    array-length v1, v14

    add-int/lit8 v1, v1, -0x1

    if-ge v11, v1, :cond_f

    .line 916
    aget v1, v14, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v12, v1, v3

    .line 918
    .restart local v12    # "length":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v12, v1

    if-gez v1, :cond_4

    .line 919
    const/high16 v12, 0x3f800000    # 1.0f

    .line 920
    :cond_4
    sub-float v3, v15, v12

    add-float v5, v15, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 921
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 915
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 924
    .end local v11    # "i":I
    .end local v12    # "length":F
    .end local v14    # "snapshot":[F
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_SUCCESS:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-eq v1, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_RESTART:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-eq v1, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v1, v3, :cond_f

    .line 927
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 928
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v5

    aget v3, v3, v5

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 929
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v14

    .line 930
    .restart local v14    # "snapshot":[F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    array-length v3, v14

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/Wave$Anim;->toDistance(I)I

    move-result v9

    .line 932
    .local v9, "cutoff":I
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    const/4 v1, 0x1

    if-ge v11, v1, :cond_7

    .line 933
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v3, v15, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v5, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 934
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 932
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 937
    :cond_7
    const/4 v11, 0x0

    :goto_5
    array-length v1, v14

    if-ge v11, v1, :cond_e

    .line 938
    aget v1, v14, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v12, v1, v3

    .line 940
    .restart local v12    # "length":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v12, v1

    if-ltz v1, :cond_8

    if-le v11, v9, :cond_9

    .line 941
    :cond_8
    const/high16 v12, 0x3f800000    # 1.0f

    .line 944
    :cond_9
    if-ne v11, v9, :cond_b

    .line 946
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f200000    # 0.625f

    mul-float/2addr v1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 953
    :cond_a
    :goto_6
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 937
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 947
    :cond_b
    array-length v1, v14

    if-lt v9, v1, :cond_c

    array-length v1, v14

    add-int/lit8 v1, v1, -0x1

    if-ge v11, v1, :cond_a

    .line 950
    :cond_c
    add-int/lit8 v1, v9, -0x1

    if-lt v11, v1, :cond_d

    add-int/lit8 v1, v9, 0x1

    if-le v11, v1, :cond_a

    .line 951
    :cond_d
    sub-float v3, v15, v12

    add-float v5, v15, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v6, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_6

    .line 957
    .end local v12    # "length":F
    :cond_e
    array-length v1, v14

    if-lt v9, v1, :cond_f

    .line 958
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3ec00000    # 0.375f

    mul-float/2addr v1, v3

    sub-float/2addr v2, v1

    .line 959
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_GAP:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2400()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f200000    # 0.625f

    mul-float/2addr v1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 996
    .end local v9    # "cutoff":I
    .end local v11    # "i":I
    .end local v14    # "snapshot":[F
    :cond_f
    return-void

    .line 964
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v1

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$State;->TRAIN_COMPLETE:Lcom/samsung/wakeupsetting/Wave$State;

    if-ne v1, v3, :cond_f

    .line 965
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    if-eqz v1, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v1

    if-lez v1, :cond_f

    .line 966
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->COLOR_TRAINING:[I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2200()[I

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v5

    aget v3, v3, v5

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 968
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    mul-int/lit8 v10, v1, 0x4

    .line 971
    .local v10, "diff":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mAdapter:Lcom/samsung/wakeupsetting/WaveViewAdapter;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$2100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/WaveViewAdapter;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/WaveViewAdapter;->getAudioSnapshot(I)[F

    move-result-object v14

    .line 973
    .restart local v14    # "snapshot":[F
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_7
    const/4 v1, 0x1

    if-ge v11, v1, :cond_11

    .line 974
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;->toDistance(I)I

    move-result v1

    int-to-float v1, v1

    add-float v4, v1, v2

    .line 975
    .local v4, "tx":F
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v5, v15, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v7, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v8, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move v6, v4

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 976
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 973
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 979
    .end local v4    # "tx":F
    :cond_11
    const/4 v11, 0x0

    :goto_8
    array-length v1, v14

    if-ge v11, v1, :cond_f

    .line 980
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v1, v10}, Lcom/samsung/wakeupsetting/Wave$Anim;->toDistance(I)I

    move-result v1

    int-to-float v1, v1

    add-float v4, v1, v2

    .line 981
    .restart local v4    # "tx":F
    aget v1, v14, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mHeight:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$2000(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    # getter for: Lcom/samsung/wakeupsetting/Wave;->PADDING_BORDER:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$2300()I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v12, v1, v3

    .line 983
    .restart local v12    # "length":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v12, v1

    if-gez v1, :cond_12

    .line 984
    const/high16 v12, 0x3f800000    # 1.0f

    .line 985
    :cond_12
    sub-float v5, v15, v12

    add-float v7, v15, v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v8, v1, Lcom/samsung/wakeupsetting/Wave;->mLinePaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move v6, v4

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 986
    # getter for: Lcom/samsung/wakeupsetting/Wave;->LINE_SPAN:I
    invoke-static {}, Lcom/samsung/wakeupsetting/Wave;->access$1900()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v2, v1

    .line 979
    add-int/lit8 v11, v11, 0x1

    goto :goto_8
.end method

.method private onEnterState(Lcom/samsung/wakeupsetting/Wave$State;)V
    .locals 6
    .param p1, "state"    # Lcom/samsung/wakeupsetting/Wave$State;

    .prologue
    const/16 v5, 0x3e8

    const/16 v4, 0x1f4

    .line 682
    sget-object v2, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$State:[I

    invoke-virtual {p1}, Lcom/samsung/wakeupsetting/Wave$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 733
    :goto_0
    :pswitch_0
    return-void

    .line 691
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/Wave$Anim;->startEndless(I)V

    .line 692
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mTimeTracker:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2, v5}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    .line 693
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mDirCount:I
    invoke-static {v2, v3}, Lcom/samsung/wakeupsetting/Wave;->access$702(Lcom/samsung/wakeupsetting/Wave;I)I

    .line 694
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    const/16 v3, 0x8

    # invokes: Lcom/samsung/wakeupsetting/Wave;->notifyView(I)V
    invoke-static {v2, v3}, Lcom/samsung/wakeupsetting/Wave;->access$800(Lcom/samsung/wakeupsetting/Wave;I)V

    goto :goto_0

    .line 697
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # invokes: Lcom/samsung/wakeupsetting/Wave;->processPendingRequests()V
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$900(Lcom/samsung/wakeupsetting/Wave;)V

    goto :goto_0

    .line 702
    :pswitch_3
    const/4 v0, 0x0

    .line 703
    .local v0, "delay":I
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v2

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v2, v3, :cond_1

    .line 706
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$500(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2, v4}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    .line 714
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2, v5}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    goto :goto_0

    .line 709
    :cond_1
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 710
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v2, v2, v1

    invoke-virtual {v2, v4, v0}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(II)V

    .line 711
    add-int/lit8 v0, v0, 0x64

    .line 709
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 721
    .end local v0    # "delay":I
    .end local v1    # "i":I
    :pswitch_4
    const/4 v0, 0x0

    .line 722
    .restart local v0    # "delay":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    const/4 v2, 0x4

    if-ge v1, v2, :cond_2

    .line 723
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v2, v2, v1

    invoke-virtual {v2, v4, v0}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(II)V

    .line 724
    add-int/lit8 v0, v0, 0x64

    .line 722
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 727
    :cond_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v2, v4, v0}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(II)V

    goto :goto_0

    .line 682
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private onEnterTrainState(Lcom/samsung/wakeupsetting/Wave$TrainState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/wakeupsetting/Wave$TrainState;

    .prologue
    const/16 v2, 0x1f4

    .line 736
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$TrainState:[I

    invoke-virtual {p1}, Lcom/samsung/wakeupsetting/Wave$TrainState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 755
    :goto_0
    :pswitch_0
    return-void

    .line 740
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    .line 741
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->startEndless(I)V

    goto :goto_0

    .line 746
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    goto :goto_0

    .line 749
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/Wave$Anim;->start(I)V

    goto :goto_0

    .line 736
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateAnimation(J)V
    .locals 10
    .param p1, "delta"    # J

    .prologue
    const/16 v9, 0x1d

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 759
    sget-object v4, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$State:[I

    iget-object v6, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v6}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/wakeupsetting/Wave$State;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 830
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 765
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mTimeTracker:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v4

    if-nez v4, :cond_1

    .line 766
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    sget-object v6, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v4, v6}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    .line 770
    :cond_1
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tock(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 771
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # operator++ for: Lcom/samsung/wakeupsetting/Wave;->mDirCount:I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$708(Lcom/samsung/wakeupsetting/Wave;)I

    .line 772
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mDirCount:I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$700(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v4

    const/4 v6, 0x7

    if-lt v4, v6, :cond_2

    .line 773
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mDirCount:I
    invoke-static {v4, v5}, Lcom/samsung/wakeupsetting/Wave;->access$702(Lcom/samsung/wakeupsetting/Wave;I)I

    .line 774
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mRand:Ljava/util/Random;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1000(Lcom/samsung/wakeupsetting/Wave;)Ljava/util/Random;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    const v6, 0x3e99999a    # 0.3f

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    .line 775
    iget-object v6, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mReverse:Z
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1100(Lcom/samsung/wakeupsetting/Wave;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    # setter for: Lcom/samsung/wakeupsetting/Wave;->mReverse:Z
    invoke-static {v6, v4}, Lcom/samsung/wakeupsetting/Wave;->access$1102(Lcom/samsung/wakeupsetting/Wave;Z)Z

    .line 777
    :cond_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mReverse:Z
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1100(Lcom/samsung/wakeupsetting/Wave;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 778
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    aget v3, v4, v9

    .line 779
    .local v3, "last":I
    const/16 v2, 0x1c

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_4

    .line 780
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    add-int/lit8 v6, v2, 0x1

    iget-object v7, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v7}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v7

    aget v7, v7, v2

    aput v7, v4, v6

    .line 779
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .end local v2    # "i":I
    .end local v3    # "last":I
    :cond_3
    move v4, v5

    .line 775
    goto :goto_1

    .line 782
    .restart local v2    # "i":I
    .restart local v3    # "last":I
    :cond_4
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    aput v3, v4, v5

    goto/16 :goto_0

    .line 784
    .end local v2    # "i":I
    .end local v3    # "last":I
    :cond_5
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    aget v3, v4, v5

    .line 785
    .restart local v3    # "last":I
    const/4 v2, 0x1

    .restart local v2    # "i":I
    :goto_3
    const/16 v4, 0x1e

    if-ge v2, v4, :cond_6

    .line 786
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    add-int/lit8 v5, v2, -0x1

    iget-object v6, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v6}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v6

    aget v6, v6, v2

    aput v6, v4, v5

    .line 785
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 788
    :cond_6
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShiftValues:[I
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1200(Lcom/samsung/wakeupsetting/Wave;)[I

    move-result-object v4

    aput v3, v4, v9

    goto/16 :goto_0

    .line 793
    .end local v2    # "i":I
    .end local v3    # "last":I
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    .line 794
    const/4 v0, 0x1

    .line 795
    .local v0, "done":Z
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    if-ge v2, v8, :cond_8

    .line 796
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v4, v4, v2

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v1

    .line 797
    .local v1, "going":Z
    if-eqz v1, :cond_7

    .line 798
    const/4 v0, 0x0

    .line 795
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 800
    .end local v1    # "going":Z
    :cond_8
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v4

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$TrainState;->SNAPSHOT_REDO:Lcom/samsung/wakeupsetting/Wave$TrainState;

    if-ne v4, v5, :cond_0

    .line 801
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto/16 :goto_0

    .line 804
    .end local v0    # "done":Z
    .end local v2    # "i":I
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1400(Lcom/samsung/wakeupsetting/Wave;)Landroid/graphics/Matrix;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTranslate:F
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$1300(Lcom/samsung/wakeupsetting/Wave;)F

    move-result v5

    neg-float v5, v5

    invoke-virtual {v4, v5, v7}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 806
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mShader:Landroid/graphics/Shader;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1500(Lcom/samsung/wakeupsetting/Wave;)Landroid/graphics/Shader;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$1400(Lcom/samsung/wakeupsetting/Wave;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 807
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    const/high16 v5, 0x40a00000    # 5.0f

    # += operator for: Lcom/samsung/wakeupsetting/Wave;->mTranslate:F
    invoke-static {v4, v5}, Lcom/samsung/wakeupsetting/Wave;->access$1316(Lcom/samsung/wakeupsetting/Wave;F)F

    .line 808
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTranslate:F
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$1300(Lcom/samsung/wakeupsetting/Wave;)F

    move-result v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mWidth:I
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$1600(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    .line 809
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mTranslate:F
    invoke-static {v4, v7}, Lcom/samsung/wakeupsetting/Wave;->access$1302(Lcom/samsung/wakeupsetting/Wave;F)F

    goto/16 :goto_0

    .line 813
    :pswitch_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateTrainAnimation(J)V

    goto/16 :goto_0

    .line 816
    :pswitch_6
    const/4 v0, 0x0

    .line 817
    .restart local v0    # "done":Z
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    if-ge v2, v8, :cond_a

    .line 818
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mMoveAnim:[Lcom/samsung/wakeupsetting/Wave$Anim;

    aget-object v4, v4, v2

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v1

    .line 819
    .restart local v1    # "going":Z
    if-nez v2, :cond_9

    if-nez v1, :cond_9

    .line 820
    const/4 v0, 0x1

    .line 817
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 823
    .end local v1    # "going":Z
    :cond_a
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v4, v4, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v4, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 824
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v4, v5}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto/16 :goto_0

    .line 759
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private updateState()V
    .locals 4

    .prologue
    .line 662
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$100(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v0

    .line 663
    .local v0, "oldState":Lcom/samsung/wakeupsetting/Wave$State;
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    .line 665
    .local v1, "oldTrainState":Lcom/samsung/wakeupsetting/Wave$TrainState;
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$300(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v3

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v2, v3}, Lcom/samsung/wakeupsetting/Wave;->access$102(Lcom/samsung/wakeupsetting/Wave;Lcom/samsung/wakeupsetting/Wave$State;)Lcom/samsung/wakeupsetting/Wave$State;

    .line 666
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$400(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v3

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v2, v3}, Lcom/samsung/wakeupsetting/Wave;->access$202(Lcom/samsung/wakeupsetting/Wave;Lcom/samsung/wakeupsetting/Wave$TrainState;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    .line 667
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextTrialNumber:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/Wave;->access$600(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v3

    # setter for: Lcom/samsung/wakeupsetting/Wave;->mTrialNumber:I
    invoke-static {v2, v3}, Lcom/samsung/wakeupsetting/Wave;->access$502(Lcom/samsung/wakeupsetting/Wave;I)I

    .line 669
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$300(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 670
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextState:Lcom/samsung/wakeupsetting/Wave$State;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$300(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$State;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->onEnterState(Lcom/samsung/wakeupsetting/Wave$State;)V

    .line 673
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$400(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v2

    if-eq v2, v1, :cond_1

    .line 674
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mNextTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/Wave;->access$400(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->onEnterTrainState(Lcom/samsung/wakeupsetting/Wave$TrainState;)V

    .line 678
    :cond_1
    return-void
.end method

.method private updateTrainAnimation(J)V
    .locals 2
    .param p1, "delta"    # J

    .prologue
    .line 833
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$1;->$SwitchMap$com$samsung$wakeupsetting$Wave$TrainState:[I

    iget-object v1, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mTrainState:Lcom/samsung/wakeupsetting/Wave$TrainState;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/Wave;->access$200(Lcom/samsung/wakeupsetting/Wave;)Lcom/samsung/wakeupsetting/Wave$TrainState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/Wave$TrainState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 863
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 837
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mFadeOutAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    .line 838
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tock(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 841
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # operator-- for: Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$1710(Lcom/samsung/wakeupsetting/Wave;)I

    .line 842
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/Wave;->access$1700(Lcom/samsung/wakeupsetting/Wave;)I

    move-result v0

    if-gez v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    const/16 v1, 0x1e

    # += operator for: Lcom/samsung/wakeupsetting/Wave;->mScrollIndex:I
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->access$1712(Lcom/samsung/wakeupsetting/Wave;I)I

    goto :goto_0

    .line 849
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mSweepAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 854
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    iget-object v0, v0, Lcom/samsung/wakeupsetting/Wave;->mFadeInAnim:Lcom/samsung/wakeupsetting/Wave$Anim;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/wakeupsetting/Wave$Anim;->tick(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/Wave;->changeState(Lcom/samsung/wakeupsetting/Wave$Request;)V

    goto :goto_0

    .line 833
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getRunState()Lcom/samsung/wakeupsetting/Wave$RunState;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    return-object v0
.end method

.method public run()V
    .locals 8

    .prologue
    .line 583
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mTimeStamp:J

    .line 584
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$RunState;->EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

    if-eq v4, v5, :cond_6

    .line 585
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$RunState;->RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;

    if-ne v4, v5, :cond_5

    .line 586
    const-wide/16 v2, 0x0

    .line 587
    .local v2, "ts":J
    const/4 v0, 0x0

    .line 589
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$000(Lcom/samsung/wakeupsetting/Wave;)Landroid/view/TextureView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/TextureView;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 590
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$000(Lcom/samsung/wakeupsetting/Wave;)Landroid/view/TextureView;

    move-result-object v5

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 591
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 592
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateState()V

    .line 593
    iget-wide v6, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mTimeStamp:J

    sub-long v6, v2, v6

    invoke-direct {p0, v6, v7}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->updateAnimation(J)V

    .line 594
    if-eqz v0, :cond_3

    .line 595
    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/Wave$DrawThread;->draw(Landroid/graphics/Canvas;)V

    .line 599
    :goto_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601
    if-eqz v0, :cond_1

    .line 602
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;
    invoke-static {v4}, Lcom/samsung/wakeupsetting/Wave;->access$000(Lcom/samsung/wakeupsetting/Wave;)Landroid/view/TextureView;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/TextureView;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 604
    :cond_1
    iput-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mTimeStamp:J

    .line 607
    const-wide/16 v4, 0x5

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 622
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "ts":J
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mNextRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    if-eq v4, v5, :cond_0

    .line 623
    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 624
    :try_start_3
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mNextRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 625
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 626
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 627
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mTimeStamp:J

    goto :goto_0

    .line 597
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    .restart local v2    # "ts":J
    :cond_3
    :try_start_4
    const-string/jumbo v4, "Wave"

    const-string/jumbo v6, "Canvas is null!"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 599
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 601
    :catchall_1
    move-exception v4

    if-eqz v0, :cond_4

    .line 602
    iget-object v5, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->this$0:Lcom/samsung/wakeupsetting/Wave;

    # getter for: Lcom/samsung/wakeupsetting/Wave;->mView:Landroid/view/TextureView;
    invoke-static {v5}, Lcom/samsung/wakeupsetting/Wave;->access$000(Lcom/samsung/wakeupsetting/Wave;)Landroid/view/TextureView;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/TextureView;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_4
    throw v4

    .line 608
    :catch_0
    move-exception v1

    .line 610
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 613
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "ts":J
    :cond_5
    iget-object v4, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v5, Lcom/samsung/wakeupsetting/Wave$RunState;->IDLE:Lcom/samsung/wakeupsetting/Wave$RunState;

    if-ne v4, v5, :cond_2

    .line 615
    const-wide/16 v4, 0x64

    :try_start_6
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 616
    :catch_1
    move-exception v1

    .line 618
    .restart local v1    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 626
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_2
    move-exception v4

    :try_start_7
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v4

    .line 630
    :cond_6
    return-void
.end method

.method public updateRunState(Lcom/samsung/wakeupsetting/Wave$RunState;)V
    .locals 6
    .param p1, "next"    # Lcom/samsung/wakeupsetting/Wave$RunState;

    .prologue
    .line 634
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v3, Lcom/samsung/wakeupsetting/Wave$RunState;->EXIT:Lcom/samsung/wakeupsetting/Wave$RunState;

    if-eq v2, v3, :cond_1

    .line 635
    iget-object v3, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 636
    :try_start_0
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mNextRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    .line 638
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mRunState:Lcom/samsung/wakeupsetting/Wave$RunState;

    sget-object v4, Lcom/samsung/wakeupsetting/Wave$RunState;->RUNNING:Lcom/samsung/wakeupsetting/Wave$RunState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v4, :cond_0

    .line 639
    const/4 v0, 0x1

    .line 640
    .local v0, "again":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 642
    :try_start_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$DrawThread;->mLock:Ljava/lang/Object;

    const-wide/16 v4, 0xfa0

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 643
    const/4 v0, 0x0

    goto :goto_0

    .line 644
    :catch_0
    move-exception v1

    .line 646
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 650
    .end local v0    # "again":Z
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 655
    :cond_1
    return-void
.end method
