.class public Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;
.super Landroid/preference/PreferenceFragment;
.source "VoiceWakeupSecuredLockFragment.java"

# interfaces
.implements Lcom/vlingo/midas/settings/twopane/SettingsDataFragment$OnBackPressedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation


# instance fields
.field private actionBarSwitch:Landroid/widget/Switch;

.field private content:Landroid/view/View;

.field private isFromMainApp:Z

.field private mDensity:F

.field private mFragmentLoadListener:Lcom/vlingo/midas/settings/twopane/OnFragmentLoadListener;

.field private mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

.field title:Ljava/lang/String;

.field private wakeupContent:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mDensity:F

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->isFromMainApp:Z

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->title:Ljava/lang/String;

    return-void
.end method

.method private getCurrentSystemLanguage()Ljava/util/Locale;
    .locals 9

    .prologue
    .line 115
    :try_start_0
    const-string/jumbo v7, "android.app.ActivityManagerNative"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 116
    .local v1, "amnClass":Ljava/lang/Class;
    const/4 v0, 0x0

    .line 117
    .local v0, "amn":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 119
    .local v2, "config":Landroid/content/res/Configuration;
    const-string/jumbo v7, "getDefault"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 120
    .local v6, "methodGetDefault":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 121
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 123
    const-string/jumbo v7, "getConfiguration"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 124
    .local v5, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 125
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "config":Landroid/content/res/Configuration;
    check-cast v2, Landroid/content/res/Configuration;

    .line 126
    .restart local v2    # "config":Landroid/content/res/Configuration;
    iget-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    .end local v0    # "amn":Ljava/lang/Object;
    .end local v1    # "amnClass":Ljava/lang/Class;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetDefault":Ljava/lang/reflect/Method;
    .local v3, "currentLocale":Ljava/util/Locale;
    :goto_0
    return-object v3

    .line 127
    .end local v3    # "currentLocale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 128
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 129
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .restart local v3    # "currentLocale":Ljava/util/Locale;
    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->content:Landroid/view/View;

    sget v1, Lcom/vlingo/midas/R$id;->wakeup_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->wakeupContent:Landroid/widget/TextView;

    .line 108
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, -0x2

    const/4 v3, 0x0

    .line 76
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 77
    new-instance v1, Landroid/widget/Switch;

    invoke-direct {v1, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->actionBarSwitch:Landroid/widget/Switch;

    .line 78
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 79
    .local v0, "padding":I
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 80
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->requestFocus()Z

    .line 81
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 84
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->voice_wake_up_in_sercured_lock_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->actionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 86
    new-instance v1, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->actionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v1, p1, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

    .line 87
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

    invoke-virtual {v1, p0}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->setVoiceWakeUpUIChangeListener(Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;)V

    .line 88
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "desiredState"    # Z

    .prologue
    .line 145
    invoke-virtual {p0, p2}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->updateUIVoiceWakeupSettings(Z)V

    .line 146
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 140
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 142
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 45
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 47
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v3, Landroid/util/DisplayMetrics;->density:F

    iput v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mDensity:F

    .line 48
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale(Landroid/content/res/Resources;)V

    .line 49
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v8, "svoice"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->isFromMainApp:Z

    .line 50
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "voicetalk_language"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    .local v1, "currentLocale":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v2

    .line 52
    .local v2, "currentSystem":Ljava/util/Locale;
    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    .line 54
    .local v6, "stringLanguage":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    .line 55
    .local v5, "stringCountry":Ljava/lang/String;
    if-nez v1, :cond_1

    const-string/jumbo v7, "pt"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 57
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 58
    .local v0, "conf":Landroid/content/res/Configuration;
    iput-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 59
    invoke-virtual {v4, v0, v10}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 71
    .end local v0    # "conf":Landroid/content/res/Configuration;
    .end local v4    # "res":Landroid/content/res/Resources;
    .end local v5    # "stringCountry":Ljava/lang/String;
    .end local v6    # "stringLanguage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 60
    .restart local v5    # "stringCountry":Ljava/lang/String;
    .restart local v6    # "stringLanguage":Ljava/lang/String;
    :cond_1
    const-string/jumbo v7, "Brazil"

    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v8

    const-string/jumbo v9, "ro.csc.country_code"

    invoke-virtual {v8, v9}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v1, :cond_0

    const-string/jumbo v7, "en-US"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string/jumbo v7, "pt"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string/jumbo v7, "BR"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 66
    .restart local v4    # "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 67
    .restart local v0    # "conf":Landroid/content/res/Configuration;
    iput-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 68
    invoke-virtual {v4, v0, v10}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    sget v0, Lcom/vlingo/midas/R$layout;->wakeup_secured:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->content:Landroid/view/View;

    .line 102
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->init()V

    .line 103
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->content:Landroid/view/View;

    return-object v0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 97
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 170
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 174
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 172
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 165
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pause()V

    .line 166
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 158
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 159
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->mVoiceWakeupSettingsEnabler:Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->resume()V

    .line 160
    return-void
.end method

.method public updateUIVoiceWakeupSettings(Z)V
    .locals 2
    .param p1, "turnOn"    # Z

    .prologue
    .line 149
    if-eqz p1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->wakeupContent:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 153
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->wakeupContent:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
