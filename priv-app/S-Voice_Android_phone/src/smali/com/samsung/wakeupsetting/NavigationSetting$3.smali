.class Lcom/samsung/wakeupsetting/NavigationSetting$3;
.super Ljava/lang/Object;
.source "NavigationSetting.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/NavigationSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/NavigationSetting;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/wakeupsetting/NavigationSetting$3;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 96
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 101
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$3;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mEditAddress:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$000(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$3;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mEditShortcutName:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$100(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$3;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$200(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$3;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$200(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
