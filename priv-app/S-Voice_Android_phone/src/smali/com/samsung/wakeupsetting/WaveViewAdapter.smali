.class public abstract Lcom/samsung/wakeupsetting/WaveViewAdapter;
.super Ljava/lang/Object;
.source "WaveViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;,
        Lcom/samsung/wakeupsetting/WaveViewAdapter$State;,
        Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;
    }
.end annotation


# static fields
.field public static final NUM_TRIALS:I = 0x4


# instance fields
.field protected mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

.field protected mContext:Landroid/content/Context;

.field protected mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/samsung/wakeupsetting/WaveViewAdapter;->mContext:Landroid/content/Context;

    .line 99
    return-void
.end method


# virtual methods
.method public abstract getAudioSnapshot(I)[F
.end method

.method public abstract getCurrentAudio()[F
.end method

.method public abstract getCurrentTrial()I
.end method

.method public getState()Lcom/samsung/wakeupsetting/WaveViewAdapter$State;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/wakeupsetting/WaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    return-object v0
.end method

.method public abstract nextEnrollment()V
.end method

.method public abstract reset()V
.end method

.method public setCallback(Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;)V
    .locals 0
    .param p1, "cb"    # Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/wakeupsetting/WaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    .line 107
    return-void
.end method
