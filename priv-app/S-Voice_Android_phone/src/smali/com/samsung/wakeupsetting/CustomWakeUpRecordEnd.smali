.class public Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;
.super Landroid/widget/RelativeLayout;
.source "CustomWakeUpRecordEnd.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;
    }
.end annotation


# static fields
.field private static listener:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;


# instance fields
.field private context:Landroid/content/Context;

.field private doneBtn:Lcom/vlingo/midas/gui/customviews/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->context:Landroid/content/Context;

    .line 43
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 54
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->done_button:I

    if-ne v0, v1, :cond_0

    .line 55
    sget-object v1, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->listener:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;

    if-eqz v1, :cond_0

    .line 57
    sget-object v1, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->listener:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;

    invoke-interface {v1}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;->OnCustomWakeUpRecordEndDoneButtonClicked()V

    .line 61
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/vlingo/midas/R$id;->done_button:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->doneBtn:Lcom/vlingo/midas/gui/customviews/Button;

    .line 48
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->doneBtn:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method

.method public setOnOnCustonWakeUpRecordEndListener(Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;

    .prologue
    .line 37
    sput-object p1, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->listener:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$OnCustonWakeUpRecordEndListener;

    .line 38
    return-void
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 66
    if-nez p1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->context:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$id;->recordScrollView:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 69
    .local v0, "sv":Landroid/widget/ScrollView;
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd$1;-><init>(Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;Landroid/widget/ScrollView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 76
    .end local v0    # "sv":Landroid/widget/ScrollView;
    :cond_0
    return-void
.end method
