.class Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;
.super Landroid/widget/ArrayAdapter;
.source "CustomWakeupSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSetting;->showCustomCommandlist()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSetting;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSetting;Landroid/content/Context;ILjava/util/List;Ljava/util/List;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 92
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;>;"
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSetting;

    iput-object p5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;->val$list:Ljava/util/List;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 98
    if-nez p2, :cond_0

    .line 100
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSetting;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 103
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x1090004

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TwoLineListItem;

    .line 112
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .local v2, "row":Landroid/widget/TwoLineListItem;
    :goto_0
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$3;->val$list:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;

    .line 114
    .local v0, "data":Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;
    invoke-virtual {v2}, Landroid/widget/TwoLineListItem;->getText1()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v2}, Landroid/widget/TwoLineListItem;->getText2()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-object v2

    .end local v0    # "data":Lcom/samsung/wakeupsetting/CustomWakeupSetting$WakeupCommand;
    .end local v2    # "row":Landroid/widget/TwoLineListItem;
    :cond_0
    move-object v2, p2

    .line 108
    check-cast v2, Landroid/widget/TwoLineListItem;

    .restart local v2    # "row":Landroid/widget/TwoLineListItem;
    goto :goto_0
.end method
