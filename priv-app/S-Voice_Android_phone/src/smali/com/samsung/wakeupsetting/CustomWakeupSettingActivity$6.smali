.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchWakeupDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

.field final synthetic val$dialog:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iput-object p2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x0

    .line 809
    # setter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$202(Z)Z

    .line 811
    packed-switch p3, :pswitch_data_0

    .line 847
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 848
    :goto_1
    return-void

    .line 814
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 815
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 816
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->val$dialog:Landroid/app/Dialog;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 817
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 820
    .end local v0    # "message":Landroid/os/Message;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$800(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    goto :goto_0

    .line 830
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 831
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    sget v3, Lcom/vlingo/midas/R$string;->title_change_voice_function:I

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 832
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 833
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 834
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    goto :goto_0

    .line 839
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->checkPreferenceValue()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 840
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 841
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchResetAlertDialog()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    goto :goto_0

    .line 811
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
