.class public final Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;
.super Ljava/lang/Object;
.source "VoiceWakeupSettingsEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$VoiceWakeUpUIChangeListener;
    }
.end annotation


# static fields
.field public static LOG_TAG:Ljava/lang/String;


# instance fields
.field private final KEY_VOICE_WAKEUP_SETTINGS:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mDensity:F

.field private mOldX:F

.field private mOldY:F

.field private mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private mSwitch:Landroid/widget/Switch;

.field private mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

.field mWakeupDialog:Landroid/app/AlertDialog;

.field pref:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string/jumbo v0, "VoiceWakeupSettingsEnabler"

    sput-object v0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switch_"    # Landroid/widget/Switch;

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string/jumbo v0, "voice_wakeup_settings"

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->KEY_VOICE_WAKEUP_SETTINGS:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    .line 93
    iput v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mOldX:F

    .line 94
    iput v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mOldY:F

    .line 95
    iput v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mDensity:F

    .line 48
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    .line 50
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public dismissDialog()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "turnOn"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 230
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 233
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "preferences_voice_wake_up"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 235
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "VOICE_WAKE_UP_POP"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->showWakeupLockScreenDialog()Z

    .line 271
    :goto_0
    return-void

    .line 240
    :cond_0
    if-eqz p2, :cond_2

    .line 241
    const-string/jumbo v1, "car_word_spotter_enabled"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 242
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 243
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 244
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 245
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    if-eqz v1, :cond_1

    .line 246
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    sget v2, Lcom/vlingo/midas/R$string;->on:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 258
    :cond_1
    :goto_1
    if-eqz p2, :cond_4

    .line 259
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 260
    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->updateUIVoiceWakeupSettings(Z)V

    goto :goto_0

    .line 248
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 249
    const-string/jumbo v1, "car_word_spotter_enabled"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 253
    :goto_2
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 254
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 255
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    if-eqz v1, :cond_1

    .line 256
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    sget v2, Lcom/vlingo/midas/R$string;->off:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1

    .line 251
    :cond_3
    const-string/jumbo v1, "car_word_spotter_enabled"

    invoke-static {v1, v4}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    .line 262
    :cond_4
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 263
    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->updateUIVoiceWakeupSettings(Z)V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    const-string/jumbo v0, "voice_wake_up"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 84
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 85
    return-void

    .line 78
    :cond_1
    const-string/jumbo v0, "voice_wake_up"

    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public resume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    sget-object v0, Lcom/vlingo/midas/settings/twopane/WakeupFragment;->mWakeupLockScreen:Lcom/vlingo/midas/settings/TwoLinePreference;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    .line 59
    :goto_0
    const-string/jumbo v0, "preferences_voice_wake_up_time"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 67
    :goto_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->updateUIVoiceWakeupSettings(Z)V

    .line 68
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 69
    return-void

    .line 57
    :cond_1
    sget-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->WakeupSummary:Landroid/preference/Preference;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    goto :goto_0

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_1
.end method

.method setVoiceWakeUpUIChangeListener(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0
    .param p1, "mlistener"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .line 103
    return-void
.end method

.method public showWakeupLockScreenDialog()Z
    .locals 10

    .prologue
    .line 111
    iget-object v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 112
    .local v2, "inflater":Landroid/view/LayoutInflater;
    sget v7, Lcom/vlingo/midas/R$layout;->custom_alert_dialog:I

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 113
    .local v1, "checkboxLayout":Landroid/view/View;
    sget v7, Lcom/vlingo/midas/R$id;->checkBox:I

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 114
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const-string/jumbo v7, "system/fonts/SECRobotoLight-Regular.ttf"

    invoke-static {v7}, Lcom/vlingo/midas/util/Typefaces;->getTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    .line 117
    .local v5, "samsungSans_text":Landroid/graphics/Typeface;
    iget-object v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    iput v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mDensity:F

    .line 119
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v3

    .line 123
    .local v3, "leftPadding":I
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v7

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v8

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v9

    invoke-virtual {v0, v3, v7, v8, v9}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 126
    new-instance v7, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$1;

    invoke-direct {v7, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$1;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    new-instance v7, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$2;

    invoke-direct {v7, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$2;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 138
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    invoke-direct {v4, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 139
    .local v4, "mAlertDialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 140
    sget v7, Lcom/vlingo/midas/R$id;->custom_dialog_warning_textView:I

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 141
    .local v6, "text":Landroid/widget/TextView;
    if-eqz v6, :cond_0

    .line 142
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 144
    :cond_0
    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 147
    :cond_1
    sget v7, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 148
    sget v7, Lcom/vlingo/midas/R$string;->wakeup_screen_off_dialog_message:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 159
    const v7, 0x104000a

    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;

    invoke-direct {v8, p0, v0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    const/high16 v7, 0x1040000

    new-instance v8, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;

    invoke-direct {v8, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$4;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V

    invoke-virtual {v4, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 201
    new-instance v7, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$5;

    invoke-direct {v7, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$5;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 212
    new-instance v7, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$6;

    invoke-direct {v7, p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$6;-><init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)V

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 224
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mWakeupDialog:Landroid/app/AlertDialog;

    .line 226
    const/4 v7, 0x0

    return v7
.end method

.method public updateUIVoiceWakeupSettings(Z)V
    .locals 3
    .param p1, "turnOn"    # Z

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v0, p1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->updateUIVoiceWakeupSettings(Z)V

    .line 107
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 108
    return-void
.end method
