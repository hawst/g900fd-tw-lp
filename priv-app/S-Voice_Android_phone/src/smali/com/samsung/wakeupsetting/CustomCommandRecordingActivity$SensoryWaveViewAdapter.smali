.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;
.super Lcom/samsung/wakeupsetting/WaveViewAdapter;
.source "CustomCommandRecordingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SensoryWaveViewAdapter"
.end annotation


# static fields
.field static final MAX_RMS:F = 120.0f

.field static final NUM_BLOCKS:I = 0x12

.field static final NUM_SAMPLES:I = 0x1e

.field static final NUM_SINE_CYCLES:I = 0x4

.field static final TAG:Ljava/lang/String; = "WaveAdapter"

.field static final THRESH:F = 0.1f

.field static final USE_SINE:Z = true

.field static final USE_SPECTRUM:Z = true


# instance fields
.field blockIndex:I

.field delta:F

.field input:[[I

.field lastStatus:I

.field max:F

.field recIndex:I

.field recording:[F

.field roll:[F

.field sine:[F

.field snapIndex:I

.field snapshot:[[F

.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

.field trial:I


# direct methods
.method public constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2292
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .line 2293
    invoke-direct {p0, p2}, Lcom/samsung/wakeupsetting/WaveViewAdapter;-><init>(Landroid/content/Context;)V

    .line 2276
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->delta:F

    .line 2277
    const/16 v0, 0x12

    new-array v0, v0, [[I

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->input:[[I

    .line 2281
    const/4 v0, 0x4

    new-array v0, v0, [[F

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    .line 2294
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->init()V

    .line 2295
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->READY:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2296
    return-void
.end method

.method private clearInput()V
    .locals 0

    .prologue
    .line 2590
    return-void
.end method

.method private clearSnapshot()V
    .locals 3

    .prologue
    .line 2593
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 2594
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 2593
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2595
    :cond_0
    return-void
.end method

.method private init()V
    .locals 8

    .prologue
    const/16 v7, 0x12

    const/16 v6, 0x1e

    .line 2567
    new-array v1, v6, [F

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recording:[F

    .line 2568
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 2569
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    new-array v2, v6, [F

    aput-object v2, v1, v0

    .line 2568
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2572
    :cond_0
    new-array v1, v6, [F

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->sine:[F

    .line 2573
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v6, :cond_1

    .line 2574
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->sine:[F

    const-wide v2, 0x403921fb54442d18L    # 25.132741228718345

    int-to-double v4, v0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    double-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    aput v2, v1, v0

    .line 2573
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2577
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v7, :cond_2

    .line 2578
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->input:[[I

    new-array v2, v6, [I

    aput-object v2, v1, v0

    .line 2577
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2581
    :cond_2
    new-array v1, v7, [F

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->roll:[F

    .line 2582
    return-void
.end method

.method private normalizeSnapshot(I)V
    .locals 7
    .param p1, "trial"    # I

    .prologue
    const/4 v6, 0x0

    .line 2598
    iget v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->max:F

    cmpl-float v2, v2, v6

    if-lez v2, :cond_2

    if-lez p1, :cond_2

    const/4 v2, 0x4

    if-gt p1, v2, :cond_2

    .line 2599
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x1e

    if-ge v0, v2, :cond_1

    .line 2600
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v3, p1, -0x1

    aget-object v2, v2, v3

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    const v3, 0x3dcccccd    # 0.1f

    mul-float/2addr v2, v3

    const v3, 0x3f666666    # 0.9f

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    aget v4, v4, v0

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v1, v2, v3

    .line 2601
    .local v1, "v":F
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v3, p1, -0x1

    aget-object v2, v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    aput v3, v2, v0

    .line 2602
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 2603
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v3, p1, -0x1

    aget-object v2, v2, v3

    add-int/lit8 v3, v0, -0x1

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    aget v4, v4, v0

    aput v4, v2, v3

    .line 2599
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2606
    .end local v1    # "v":F
    :cond_1
    iput v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->max:F

    .line 2608
    .end local v0    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method public addRmsValue(I)V
    .locals 4
    .param p1, "rms"    # I

    .prologue
    .line 2382
    const/4 v0, 0x0

    .line 2383
    .local v0, "val":F
    if-ltz p1, :cond_2

    sget-object v1, Lcom/samsung/wakeupsetting/LevelMapper;->MAP:[F

    array-length v1, v1

    if-gt p1, v1, :cond_2

    .line 2384
    sget-object v1, Lcom/samsung/wakeupsetting/LevelMapper;->MAP:[F

    aget v0, v1, p1

    .line 2392
    :goto_0
    const-string/jumbo v1, "WaveAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "RMS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", mapped = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2400
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    sget-object v2, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->RECORDING:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    if-ne v1, v2, :cond_1

    .line 2407
    iget v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->max:F

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    .line 2408
    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->max:F

    .line 2410
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    iget v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->trial:I

    aget-object v1, v1, v2

    iget v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    aput v0, v1, v2

    .line 2411
    iget v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x1e

    iput v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    .line 2414
    :cond_1
    return-void

    .line 2386
    :cond_2
    int-to-float v1, p1

    const/high16 v2, 0x42f00000    # 120.0f

    div-float v0, v1, v2

    goto :goto_0
.end method

.method public addSpectrum([I)V
    .locals 8
    .param p1, "spectrum"    # [I

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 2311
    if-eqz p1, :cond_3

    .line 2314
    const/4 v0, 0x0

    .local v0, "avg":F
    const/4 v1, 0x0

    .line 2315
    .local v1, "avg2":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, p1

    div-int/lit8 v4, v4, 0x2

    if-ge v3, v4, :cond_0

    .line 2316
    aget v4, p1, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v4}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v4

    const/high16 v5, 0x43480000    # 200.0f

    div-float/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    add-float/2addr v0, v4

    .line 2315
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2319
    :cond_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->roll:[F

    iget v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->blockIndex:I

    array-length v6, p1

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    div-float v6, v0, v6

    aput v6, v4, v5

    .line 2322
    const/4 v3, 0x0

    :goto_1
    const/16 v4, 0x12

    if-ge v3, v4, :cond_1

    .line 2323
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->roll:[F

    aget v4, v4, v3

    add-float/2addr v1, v4

    .line 2322
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2325
    :cond_1
    const/4 v3, 0x0

    :goto_2
    const/16 v4, 0x1e

    if-ge v3, v4, :cond_2

    .line 2326
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recording:[F

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->sine:[F

    aget v5, v5, v3

    mul-float/2addr v5, v1

    const/high16 v6, 0x41900000    # 18.0f

    div-float/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    aput v5, v4, v3

    .line 2325
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2329
    :cond_2
    iget v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->blockIndex:I

    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v4, v4, 0x12

    iput v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->blockIndex:I

    .line 2332
    const-wide/16 v4, 0x1

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2376
    .end local v0    # "avg":F
    .end local v1    # "avg2":F
    .end local v3    # "i":I
    :cond_3
    :goto_3
    return-void

    .line 2333
    .restart local v0    # "avg":F
    .restart local v1    # "avg2":F
    .restart local v3    # "i":I
    :catch_0
    move-exception v2

    .line 2335
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3
.end method

.method public getAudioSnapshot(I)[F
    .locals 2
    .param p1, "trial"    # I

    .prologue
    .line 2563
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapshot:[[F

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getCurrentAudio()[F
    .locals 1

    .prologue
    .line 2558
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recording:[F

    return-object v0
.end method

.method public getCurrentTrial()I
    .locals 1

    .prologue
    .line 2526
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->trial:I

    return v0
.end method

.method public getLastStatus()I
    .locals 1

    .prologue
    .line 2417
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->lastStatus:I

    return v0
.end method

.method public nextEnrollment()V
    .locals 2

    .prologue
    .line 2518
    const-string/jumbo v0, "WaveAdapter"

    const-string/jumbo v1, "nextEnrollment"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2519
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetNextEnroll(Z)V

    .line 2520
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->RECORDING:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2521
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->max:F

    .line 2522
    return-void
.end method

.method public notifyEnrollComplete(II)V
    .locals 7
    .param p1, "trial"    # I
    .param p2, "status"    # I

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2437
    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->READY:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2438
    const-string/jumbo v1, "WaveAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "notifyEnrollComplete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2440
    const/4 v1, 0x4

    if-ge p1, v1, :cond_0

    .line 2441
    iput p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->trial:I

    .line 2443
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v1, :cond_2

    .line 2444
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->SUCCESS:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .line 2445
    .local v0, "result":Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;
    if-ne p2, v6, :cond_1

    .line 2446
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RETRY:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .line 2447
    :cond_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    invoke-interface {v1, p1, v0}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onTrainingComplete(ILcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    .line 2452
    .end local v0    # "result":Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;
    :cond_2
    if-eq p2, v6, :cond_3

    .line 2453
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->normalizeSnapshot(I)V

    .line 2455
    :cond_3
    iput v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    .line 2456
    iput v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recIndex:I

    .line 2459
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recording:[F

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([FF)V

    .line 2460
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->roll:[F

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([FF)V

    .line 2461
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->clearInput()V

    .line 2466
    return-void
.end method

.method public notifyEnrollStarted(I)V
    .locals 2
    .param p1, "trial"    # I

    .prologue
    const/4 v1, 0x0

    .line 2421
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->RECORDING:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2422
    iput v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->lastStatus:I

    .line 2423
    const/4 p1, 0x0

    .line 2424
    iput v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    .line 2428
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v0, :cond_0

    .line 2429
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    invoke-interface {v0, p1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onTrainingStarted(I)V

    .line 2434
    :cond_0
    return-void
.end method

.method public notifyError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 2512
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v0, :cond_0

    .line 2513
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-interface {v0, v1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    .line 2514
    :cond_0
    return-void
.end method

.method public notifyProcessingCompleted(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 2496
    const-string/jumbo v0, "WaveAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "notifyProcessingCompleted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2497
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->PROCESSING:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    if-ne v0, v1, :cond_4

    .line 2498
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 2499
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->SUCCESS:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-interface {v0, v1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    .line 2507
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->READY:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2508
    iput p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->lastStatus:I

    .line 2509
    return-void

    .line 2500
    :cond_1
    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    .line 2501
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RETRY:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-interface {v0, v1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    goto :goto_0

    .line 2502
    :cond_2
    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    const/4 v0, 0x7

    if-eq p1, v0, :cond_3

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 2503
    :cond_3
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-interface {v0, v1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onProcessingComplete(Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    goto :goto_0

    .line 2505
    :cond_4
    invoke-virtual {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyError(I)V

    goto :goto_0
.end method

.method public notifyProcessingStarted()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2469
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->PROCESSING:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2471
    const-string/jumbo v0, "WaveAdapter"

    const-string/jumbo v1, "notifyProcessingStarted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2474
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->trial:I

    if-nez v0, :cond_2

    .line 2475
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v0, :cond_0

    .line 2476
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-interface {v0, v3, v1}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onTrainingComplete(ILcom/samsung/wakeupsetting/WaveViewAdapter$Result;)V

    .line 2477
    :cond_0
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$State;->READY:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mState:Lcom/samsung/wakeupsetting/WaveViewAdapter$State;

    .line 2484
    :cond_1
    :goto_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->normalizeSnapshot(I)V

    .line 2486
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    .line 2488
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recording:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 2490
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->roll:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 2491
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->clearInput()V

    .line 2492
    return-void

    .line 2479
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    if-eqz v0, :cond_1

    .line 2480
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->mCb:Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;

    invoke-interface {v0}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Callback;->onProcessingStarted()V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2531
    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->recIndex:I

    .line 2532
    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->snapIndex:I

    .line 2533
    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->trial:I

    .line 2534
    return-void
.end method
