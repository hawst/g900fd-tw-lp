.class public final enum Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;
.super Ljava/lang/Enum;
.source "WaveViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/WaveViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

.field public static final enum FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

.field public static final enum FAILED_RETRY:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

.field public static final enum SUCCESS:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    const-string/jumbo v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->SUCCESS:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .line 85
    new-instance v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    const-string/jumbo v1, "FAILED_RETRY"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RETRY:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .line 89
    new-instance v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    const-string/jumbo v1, "FAILED_RESTART"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->SUCCESS:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RETRY:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->FAILED_RESTART:Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->$VALUES:[Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->$VALUES:[Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/WaveViewAdapter$Result;

    return-object v0
.end method
