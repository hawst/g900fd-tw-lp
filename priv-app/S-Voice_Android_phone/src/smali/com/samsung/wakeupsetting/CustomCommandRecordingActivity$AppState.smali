.class final enum Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
.super Ljava/lang/Enum;
.source "CustomCommandRecordingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AppState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum ADAPTING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum ADAPTING_IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum ERROR:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum MIC_BUSY:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field public static final enum THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 172
    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "LISTENING"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "THINKING"

    invoke-direct {v0, v1, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "ADAPTING_IDLE"

    invoke-direct {v0, v1, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING_IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "ADAPTING"

    invoke-direct {v0, v1, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ERROR:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const-string/jumbo v1, "MIC_BUSY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->MIC_BUSY:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 171
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING_IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ERROR:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->MIC_BUSY:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->$VALUES:[Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 171
    const-class v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->$VALUES:[Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    return-object v0
.end method
