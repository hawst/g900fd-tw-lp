.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;
.super Landroid/widget/ArrayAdapter;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 343
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;>;"
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 349
    if-nez p2, :cond_0

    .line 350
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 353
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v3, Lcom/vlingo/midas/R$layout;->wakeup_twoline_list_item:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TwoLineListItem;

    .line 358
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .local v1, "row":Landroid/widget/TwoLineListItem;
    :goto_0
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    .line 360
    .local v2, "wakeupListItem":Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;
    invoke-virtual {v1}, Landroid/widget/TwoLineListItem;->getText1()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->getCommand()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    invoke-virtual {v1}, Landroid/widget/TwoLineListItem;->getText1()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 362
    invoke-virtual {v1}, Landroid/widget/TwoLineListItem;->getText2()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->getFunction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->getFunction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 365
    invoke-virtual {v1}, Landroid/widget/TwoLineListItem;->getText2()Landroid/widget/TextView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 370
    :goto_1
    return-object v1

    .end local v1    # "row":Landroid/widget/TwoLineListItem;
    .end local v2    # "wakeupListItem":Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;
    :cond_0
    move-object v1, p2

    .line 356
    check-cast v1, Landroid/widget/TwoLineListItem;

    .restart local v1    # "row":Landroid/widget/TwoLineListItem;
    goto :goto_0

    .line 367
    .restart local v2    # "wakeupListItem":Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;
    :cond_1
    invoke-virtual {v1}, Landroid/widget/TwoLineListItem;->getText2()Landroid/widget/TextView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
