.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;
.super Landroid/os/Handler;
.source "VoiceWakeUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestAudioFocusHandler"
.end annotation


# instance fields
.field private final mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .prologue
    .line 270
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 271
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;->mActivity:Ljava/lang/ref/WeakReference;

    .line 272
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 275
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$RequestAudioFocusHandler;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    .line 276
    .local v0, "mainActivity":Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;
    if-eqz v0, :cond_0

    .line 278
    :try_start_0
    iget-boolean v1, v0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_1

    .line 279
    const-string/jumbo v1, "/data/data/com.vlingo.midas/ListenToMyVoice.pcm"

    # invokes: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$000(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 286
    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setStreamVolumeNormal(Landroid/content/Context;)V

    .line 288
    :cond_0
    return-void

    .line 281
    :cond_1
    :try_start_1
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_1.pcm"

    # invokes: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$000(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v1

    goto :goto_0
.end method
