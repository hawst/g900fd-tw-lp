.class Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;
.super Landroid/os/Handler;
.source "WaveView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/WaveView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ResponseHandler"
.end annotation


# instance fields
.field viewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/wakeupsetting/WaveView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/wakeupsetting/WaveView;)V
    .locals 1
    .param p1, "view"    # Lcom/samsung/wakeupsetting/WaveView;

    .prologue
    .line 246
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 247
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;->viewRef:Ljava/lang/ref/WeakReference;

    .line 248
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 252
    iget-object v1, p0, Lcom/samsung/wakeupsetting/WaveView$ResponseHandler;->viewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/WaveView;

    .line 253
    .local v0, "view":Lcom/samsung/wakeupsetting/WaveView;
    if-eqz v0, :cond_0

    .line 254
    # invokes: Lcom/samsung/wakeupsetting/WaveView;->onWaveResponse(Landroid/os/Message;)V
    invoke-static {v0, p1}, Lcom/samsung/wakeupsetting/WaveView;->access$000(Lcom/samsung/wakeupsetting/WaveView;Landroid/os/Message;)V

    .line 261
    :cond_0
    return-void
.end method
