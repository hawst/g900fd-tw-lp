.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "functionPositionSelected"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 461
    sput p3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    .line 465
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v3

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionAvailable(I)Z
    invoke-static {v4, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$400(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 467
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$500(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 575
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$200()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v3

    const/16 v4, 0x9

    if-ge v3, v4, :cond_2

    .line 472
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$600(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 479
    :cond_2
    const/4 v3, 0x4

    new-array v0, v3, [I

    .line 480
    .local v0, "assignCommandArray":[I
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v2

    .line 483
    .local v2, "realAssignCommand":I
    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    .line 484
    add-int/lit8 v2, v2, 0x1

    .line 487
    :cond_3
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$700(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    sget-object v4, Lcom/samsung/voiceshell/VoiceEngine;->typeDefine:Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v5}, Lcom/samsung/voiceshell/VoiceEngine;->functionAssignment(Ljava/lang/String;[II)I

    move-result v3

    if-ne v3, v6, :cond_4

    .line 489
    aput v6, v0, v5

    .line 490
    aput v6, v0, v7

    .line 491
    aput v6, v0, v8

    .line 492
    aput v6, v0, v9

    .line 495
    :cond_4
    sget v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    packed-switch v3, :pswitch_data_0

    .line 513
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 522
    :goto_1
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$700(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v3

    sget-object v4, Lcom/samsung/voiceshell/VoiceEngine;->typeDefine:Ljava/lang/String;

    invoke-virtual {v3, v4, v0, v7}, Lcom/samsung/voiceshell/VoiceEngine;->functionAssignment(Ljava/lang/String;[II)I

    .line 524
    const/4 v1, 0x0

    .line 525
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v3, v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    .line 570
    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$200()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 571
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$800(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    goto :goto_0

    .line 497
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_0
    const-string/jumbo v3, "kew_wake_up_and_auto_function1"

    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 498
    aput v2, v0, v5

    goto :goto_1

    .line 501
    :pswitch_1
    const-string/jumbo v3, "kew_wake_up_and_auto_function2"

    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 502
    aput v2, v0, v7

    goto :goto_1

    .line 505
    :pswitch_2
    const-string/jumbo v3, "kew_wake_up_and_auto_function3"

    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 506
    aput v2, v0, v8

    goto :goto_1

    .line 509
    :pswitch_3
    const-string/jumbo v3, "kew_wake_up_and_auto_function4"

    invoke-static {v3, v2}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 510
    aput v2, v0, v9

    goto :goto_1

    .line 528
    .restart local v1    # "intent":Landroid/content/Intent;
    :pswitch_4
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "android.intent.action.PICK"

    const-string/jumbo v4, "content://contacts"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 529
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v3, v1, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 535
    :pswitch_5
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "android.intent.action.PICK"

    const-string/jumbo v4, "content://contacts"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 536
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v3, v1, v9}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 553
    :pswitch_6
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/wakeupsetting/NavigationSetting;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 554
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const/4 v4, 0x4

    invoke-virtual {v3, v1, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 560
    :pswitch_7
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 561
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string/jumbo v3, "com.android.settings"

    const-string/jumbo v4, "com.android.settings.lockscreenshortcut.AppList"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 562
    const-string/jumbo v3, "clicked_view_index"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 563
    const-string/jumbo v3, "shortcut_app_list"

    const-string/jumbo v4, ""

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 564
    const-string/jumbo v3, "num_of_shortcut"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 565
    const-string/jumbo v3, "max_num_of_shortcut"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 566
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    const/4 v4, 0x5

    invoke-virtual {v3, v1, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 495
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 525
    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
