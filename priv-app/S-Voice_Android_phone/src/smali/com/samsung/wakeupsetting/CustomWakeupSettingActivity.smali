.class public Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "CustomWakeupSettingActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;,
        Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;,
        Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;,
        Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$TabletType;
    }
.end annotation


# static fields
.field static final REQUEST_WAKEUP_COMMAND_RECORD:I = 0x1

.field static final SET_BOOKMARK_REQUEST:I = 0x6f

.field static final SET_DIRECT_DIAL_REQUEST:I = 0x2

.field static final SET_DIRECT_MESSAGE_REQUEST:I = 0x3

.field static final SET_NAVIGATION_REQUEST:I = 0x4

.field static final SET_OPEN_APPLICATION_REQUEST:I = 0x5

.field public static enrolled:Z

.field private static mFirstInit:Z

.field static mFunctionPositionSelected:I

.field private static mNeedToShowFunctionList:Z

.field private static mTheme:I

.field public static m_UDTSIDlastEnroll:[Ljava/lang/String;

.field public static m_lastEnroll:[Ljava/lang/String;

.field public static m_realLastEnroll:[Ljava/lang/String;

.field public static tts:Landroid/speech/tts/TextToSpeech;

.field static wakeupCommandPositonSelected:I

.field private static wakeupRecordSuccess:Ljava/lang/Boolean;


# instance fields
.field private currentTablet:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$TabletType;

.field private customTitleSupported:Z

.field private customWakeupBubbleTextView1:Landroid/widget/TextView;

.field private customWakeupBubbleTextView2:Landroid/widget/TextView;

.field private density:I

.field private functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

.field functionListAdapter:Landroid/widget/ArrayAdapter;

.field functionListView:Landroid/widget/ListView;

.field final functionNameArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;",
            ">;"
        }
    .end annotation
.end field

.field private inIUXMode:Z

.field private isFromHelpApp:Z

.field private isFromMainApp:Z

.field private isFunctionValueSelected:Ljava/lang/Boolean;

.field public isSensoryUDTSIDsoFileExist:Z

.field final mApplicationArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mContactNameArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDensity:F

.field private mListMainLayout:Landroid/widget/RelativeLayout;

.field final mMessageNameArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mNavigationAddressArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOldLX:F

.field private mOldLY:F

.field private mOldPX:F

.field private mOldPY:F

.field private mOldX:F

.field private mOldY:F

.field private mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

.field private mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

.field private my_orientation:I

.field preferenceNameArray:[Ljava/lang/String;

.field private resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

.field private final sensoryUDTSIDSoFilePath:Ljava/lang/String;

.field wakeupCommandArrayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;",
            ">;"
        }
    .end annotation
.end field

.field wakeupCommandListView:Landroid/widget/ListView;

.field final wakeupCommandNameArray:[Ljava/lang/String;

.field final wakeupDialogAutoFunctionArray:[Ljava/lang/String;

.field wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

.field wakeupDialogListView:Landroid/widget/ListView;

.field final wakeupDialogVoiceTalkArray:[Ljava/lang/String;

.field wakeupPreference:[Landroid/content/SharedPreferences;

.field wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    sput v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    .line 95
    sput v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    .line 127
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupRecordSuccess:Ljava/lang/Boolean;

    .line 130
    sput-boolean v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->enrolled:Z

    .line 132
    sput-boolean v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    .line 140
    sput-boolean v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNeedToShowFunctionList:Z

    .line 142
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mTheme:I

    .line 157
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_1.pcm"

    aput-object v1, v0, v2

    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_2.pcm"

    aput-object v1, v0, v3

    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_3.pcm"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_4.pcm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_5.pcm"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->m_lastEnroll:[Ljava/lang/String;

    .line 164
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_1.pcm"

    aput-object v1, v0, v2

    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_2.pcm"

    aput-object v1, v0, v3

    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_3.pcm"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_4.pcm"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_5.pcm"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->m_realLastEnroll:[Ljava/lang/String;

    .line 171
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_3.wav"

    aput-object v1, v0, v2

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_3.wav"

    aput-object v1, v0, v3

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_3.wav"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_3.wav"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_3.wav"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->m_UDTSIDlastEnroll:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 88
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    .line 107
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    .line 112
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    .line 113
    new-array v0, v5, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupPreference:[Landroid/content/SharedPreferences;

    .line 118
    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "WakeupCommandPreference2"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string/jumbo v2, "WakeupCommandPreference3"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "WakeupCommandPreference4"

    aput-object v2, v0, v1

    const-string/jumbo v1, "WakeupCommandPreference5"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    .line 125
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;

    .line 126
    invoke-static {}, Lcom/samsung/voiceshell/VoiceEngineWrapper;->getInstance()Lcom/samsung/voiceshell/VoiceEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    .line 128
    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromMainApp:Z

    .line 129
    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromHelpApp:Z

    .line 143
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mDensity:F

    .line 144
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldX:F

    .line 145
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldY:F

    .line 146
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldPX:F

    .line 147
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldPY:F

    .line 149
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldLX:F

    .line 150
    iput v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mOldLY:F

    .line 152
    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customTitleSupported:Z

    .line 153
    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->inIUXMode:Z

    .line 155
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

    .line 182
    iput-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    .line 183
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 185
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$TabletType;->OTHERS:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$TabletType;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->currentTablet:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$TabletType;

    .line 2053
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->checkPreferenceValue()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchResetAlertDialog()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->density:I

    return v0
.end method

.method static synthetic access$1200()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupRecordSuccess:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetPreferenceValue()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    return-void
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 88
    sget-boolean v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 88
    sput-boolean p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    return p0
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchWakeupDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionAvailable(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/voiceshell/VoiceEngine;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

    return-object v0
.end method

.method private addToPreference(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "wakeupCommandNameSelected"    # Ljava/lang/String;
    .param p2, "recordSuccess"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x0

    .line 1614
    const/4 v0, 0x0

    .line 1616
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    sget-boolean v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->enrolled:Z

    if-eqz v1, :cond_0

    .line 1618
    sput v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    .line 1620
    sput-boolean v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->enrolled:Z

    .line 1622
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupPreference:[Landroid/content/SharedPreferences;

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v1, v1, v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1623
    const-string/jumbo v1, "wakeupCommand"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1624
    const-string/jumbo v1, "recordSuccess"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1625
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1626
    return-void
.end method

.method private addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "wakeupCommandNameSelected"    # Ljava/lang/String;
    .param p2, "functionNameSelected"    # Ljava/lang/String;
    .param p3, "functionDetailSelected"    # Ljava/lang/String;
    .param p4, "functionPosition"    # I

    .prologue
    .line 1629
    const/4 v0, 0x0

    .line 1630
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupPreference:[Landroid/content/SharedPreferences;

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v1, v1, v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1631
    const-string/jumbo v1, "wakeupCommand"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1632
    const-string/jumbo v1, "Function"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1633
    const-string/jumbo v1, "FunctionDetail"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1634
    const-string/jumbo v1, "FunctionPosition"

    invoke-interface {v0, v1, p4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1635
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1636
    return-void
.end method

.method private checkPreferenceValue()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1707
    const/4 v1, 0x0

    .line 1709
    .local v1, "prefs":Landroid/content/SharedPreferences;
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1710
    const-string/jumbo v3, "wakeupCommand"

    const-string/jumbo v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1720
    .local v2, "wakeupCommandName":Ljava/lang/String;
    const-string/jumbo v3, "Function"

    const-string/jumbo v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1721
    .local v0, "functionName":Ljava/lang/String;
    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1722
    :cond_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;

    .line 1728
    :goto_0
    return-void

    .line 1725
    :cond_1
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionValueSelected:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private getDataValues()V
    .locals 4

    .prologue
    .line 1431
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1432
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1433
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1434
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1435
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1439
    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceIntValue(I)I

    move-result v0

    .line 1440
    .local v0, "funcPosition":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1448
    packed-switch v0, :pswitch_data_0

    .line 1435
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1450
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceTitleValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1453
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceTitleValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1456
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceTitleValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1459
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceTitleValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1469
    .end local v0    # "funcPosition":I
    :cond_1
    return-void

    .line 1448
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getFuncitonName(I)Ljava/lang/String;
    .locals 3
    .param p1, "functionPosition"    # I

    .prologue
    .line 1640
    const/4 v1, 0x0

    .line 1641
    .local v1, "listSize":I
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1642
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 1644
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 1646
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 1647
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 1651
    :goto_1
    return-object v2

    .line 1644
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1651
    :cond_2
    const-string/jumbo v2, ""

    goto :goto_1
.end method

.method private getFuncitonSubTitle(I)Ljava/lang/String;
    .locals 3
    .param p1, "functionPosition"    # I

    .prologue
    .line 1656
    const/4 v1, 0x0

    .line 1657
    .local v1, "listSize":I
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1658
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 1660
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 1662
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 1663
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v2

    .line 1667
    :goto_1
    return-object v2

    .line 1660
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1667
    :cond_2
    const-string/jumbo v2, ""

    goto :goto_1
.end method

.method public static getInstance()Landroid/speech/tts/TextToSpeech;
    .locals 1

    .prologue
    .line 608
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 609
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    .line 610
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPreferenceIntValue(I)I
    .locals 3
    .param p1, "wakeUpCommandIndex"    # I

    .prologue
    .line 1731
    const/4 v0, 0x0

    .line 1732
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1734
    const-string/jumbo v1, "FunctionPosition"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getPreferenceTitleValue(I)Ljava/lang/String;
    .locals 3
    .param p1, "wakeUpCommandIndex"    # I

    .prologue
    .line 1738
    const/4 v0, 0x0

    .line 1739
    .local v0, "prefs":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1742
    const-string/jumbo v1, "Function"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getRadioIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 710
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 711
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 712
    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.sec.android.app.fm"

    const-string/jumbo v3, "com.sec.android.app.fm.MainActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 714
    return-object v0
.end method

.method private initStrings()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 718
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->svoice_unlock:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->svoice_unlock:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 720
    invoke-static {}, Lcom/vlingo/core/internal/util/CorePackageInfoProvider;->hasDialing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 721
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_check_missed_call:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_check_missed_call:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 722
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_check_missed_message:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_check_missed_message:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v7}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 725
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_voice_camera:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_voice_camera:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 726
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->function_item_check_schedule:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->function_item_check_schedule:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v9}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 727
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_play_music:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_play_music:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 730
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getRadioIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isIntentLaunchable(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 731
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_play_radio:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_play_radio:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 734
    :cond_1
    const-string/jumbo v1, "com.sec.android.app.voicerecorder"

    invoke-static {v1, v6}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "com.sec.android.app.voicenote"

    invoke-static {v1, v6}, Lcom/vlingo/sdk/internal/util/PackageUtil;->isAppInstalled(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 736
    :cond_2
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    sget v3, Lcom/vlingo/midas/R$string;->title_record_voice:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->title_record_voice:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 739
    :cond_3
    sget v0, Lcom/vlingo/midas/R$string;->menu_driving_mode:I

    .line 741
    .local v0, "menu_driving_mode_id":I
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isDrivingModeSupported()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 742
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 760
    :cond_4
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_wake_up_auto_function1:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 761
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_wake_up_auto_function2:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 762
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_wake_up_auto_function3:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 763
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_wake_up_auto_function4:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    .line 765
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_listen_my_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 766
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 767
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_function:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 768
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_reset:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    .line 770
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_listen_my_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 771
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_command:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 772
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_reset:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    .line 774
    return-void
.end method

.method private initializeAutoFunctionAdapter()V
    .locals 3

    .prologue
    .line 903
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$8;

    const v1, 0x1090003

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogAutoFunctionArray:[Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$8;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

    .line 949
    return-void
.end method

.method private initializeVoiceTalkAdapter()V
    .locals 3

    .prologue
    .line 853
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;

    const v1, 0x1090003

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

    .line 900
    return-void
.end method

.method private isApplicationAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1473
    const/4 v1, 0x0

    .line 1474
    .local v1, "isFunctionInUse":Z
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getDataValues()V

    .line 1478
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1480
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1482
    const/4 v1, 0x1

    .line 1487
    :cond_0
    return v1

    .line 1478
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isContactAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1511
    const/4 v1, 0x0

    .line 1512
    .local v1, "isFunctionInUse":Z
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getDataValues()V

    .line 1516
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1518
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1520
    const/4 v1, 0x1

    .line 1525
    :cond_0
    return v1

    .line 1516
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isFunctionAvailable(I)Z
    .locals 4
    .param p1, "functionPositionSelected"    # I

    .prologue
    .line 684
    const/4 v1, 0x0

    .line 687
    .local v1, "isFunctionInUse":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 688
    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceIntValue(I)I

    move-result v2

    if-ne p1, v2, :cond_1

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceIntValue(I)I

    move-result v2

    const/16 v3, 0x9

    if-ge v2, v3, :cond_1

    .line 691
    const/4 v1, 0x1

    .line 695
    :cond_0
    if-nez v1, :cond_2

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 687
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 695
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isIntentLaunchable(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 699
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 700
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 702
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 703
    const/4 v2, 0x1

    .line 706
    :cond_0
    return v2
.end method

.method private isMessageAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1530
    const/4 v1, 0x0

    .line 1531
    .local v1, "isFunctionInUse":Z
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getDataValues()V

    .line 1535
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1537
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1539
    const/4 v1, 0x1

    .line 1544
    :cond_0
    return v1

    .line 1535
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isNavigationAddressAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1492
    const/4 v1, 0x0

    .line 1493
    .local v1, "isFunctionInUse":Z
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getDataValues()V

    .line 1497
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1499
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1501
    const/4 v1, 0x1

    .line 1506
    :cond_0
    return v1

    .line 1497
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private launchFunctionAlertDialog()V
    .locals 3

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    if-nez v0, :cond_0

    .line 1148
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 1149
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/vlingo/midas/R$string;->title_notification:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1150
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/vlingo/midas/R$string;->selected_function_already_exist:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1151
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$12;

    invoke-direct {v2, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$12;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1160
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1161
    return-void
.end method

.method private launchResetAlertDialog()V
    .locals 5

    .prologue
    .line 952
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->reset_dialog_message:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 953
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->alert_yes:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 954
    .local v2, "yes":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->alert_no:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 955
    .local v1, "no":Ljava/lang/String;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 956
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/vlingo/midas/R$string;->setting_wakeup_set_wakeup_command_reset:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 957
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 959
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$9;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual {v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1127
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$10;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$10;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual {v3, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1136
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$11;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$11;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1143
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetAlertDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1144
    return-void
.end method

.method private launchWakeupDialog()V
    .locals 3

    .prologue
    .line 778
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$5;

    invoke-direct {v0, p0, p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$5;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;)V

    .line 787
    .local v0, "dialog":Landroid/app/Dialog;
    sget v1, Lcom/vlingo/midas/R$layout;->wakeupdialog:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 788
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 789
    sget v1, Lcom/vlingo/midas/R$id;->wakeupDialogOptions:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListView:Landroid/widget/ListView;

    .line 795
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->initializeAutoFunctionAdapter()V

    .line 798
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 799
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->invalidate()V

    .line 800
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 801
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogListView:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;

    invoke-direct {v2, p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$6;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 850
    return-void
.end method

.method private readFromPreferenceAndUpdateList()V
    .locals 10

    .prologue
    const/4 v7, -0x1

    .line 1671
    const/4 v3, 0x0

    .line 1675
    .local v3, "prefs":Landroid/content/SharedPreferences;
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v5, v5, v6

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1676
    const-string/jumbo v5, "wakeupCommand"

    const-string/jumbo v6, ""

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1678
    .local v4, "wakeupCommandName":Ljava/lang/String;
    const-string/jumbo v5, "FunctionPosition"

    invoke-interface {v3, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1680
    .local v2, "functionPosition":I
    if-eq v2, v7, :cond_0

    .line 1681
    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getFuncitonName(I)Ljava/lang/String;

    move-result-object v1

    .line 1682
    .local v1, "functionName":Ljava/lang/String;
    const-string/jumbo v5, "FunctionDetail"

    const-string/jumbo v6, ""

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1688
    .local v0, "functionDetail":Ljava/lang/String;
    :goto_0
    if-eqz v4, :cond_2

    invoke-static {v4}, Lcom/vlingo/sdk/internal/util/StringUtils;->isNullOrWhiteSpace(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1690
    const/16 v5, 0x9

    if-lt v2, v5, :cond_1

    .line 1691
    move-object v1, v0

    .line 1692
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    new-instance v7, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    invoke-direct {v7, v4, v1, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1703
    :goto_1
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v5}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1704
    return-void

    .line 1684
    .end local v0    # "functionDetail":Ljava/lang/String;
    .end local v1    # "functionName":Ljava/lang/String;
    :cond_0
    const-string/jumbo v1, ""

    .line 1685
    .restart local v1    # "functionName":Ljava/lang/String;
    const-string/jumbo v0, ""

    .restart local v0    # "functionDetail":Ljava/lang/String;
    goto :goto_0

    .line 1696
    :cond_1
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    new-instance v7, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    invoke-direct {v7, v4, v1, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1701
    :cond_2
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    new-instance v7, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    iget-object v8, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v9, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v8, v8, v9

    invoke-direct {v7, v8, v1, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private resetPreferenceValue()V
    .locals 4

    .prologue
    .line 1834
    const/4 v0, 0x0

    .line 1835
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupPreference:[Landroid/content/SharedPreferences;

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v1, v1, v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1841
    const-string/jumbo v1, "wakeupCommand"

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1842
    const-string/jumbo v1, "Function"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1843
    const-string/jumbo v1, "FunctionDetail"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1844
    const-string/jumbo v1, "FunctionPosition"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1846
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1847
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1848
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1849
    return-void
.end method

.method private resetPreferenceValueChange()V
    .locals 1

    .prologue
    .line 2043
    const-string/jumbo v0, "/data/data/com.vlingo.midas/shared_prefs/WakeupCommandPreference1.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2044
    const-string/jumbo v0, "/data/data/com.vlingo.midas/shared_prefs/WakeupCommandPreference2.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2045
    const-string/jumbo v0, "/data/data/com.vlingo.midas/shared_prefs/WakeupCommandPreference3.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2046
    const-string/jumbo v0, "/data/data/com.vlingo.midas/shared_prefs/WakeupCommandPreference4.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2047
    const-string/jumbo v0, "/data/data/com.vlingo.midas/shared_prefs/WakeupCommandPreference5.xml"

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2049
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 2050
    return-void
.end method

.method private startCustomCommandRecordingActivity()V
    .locals 3

    .prologue
    .line 1425
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1426
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "trainType"

    sget v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1427
    const-string/jumbo v1, "svoice"

    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromMainApp:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1428
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1429
    return-void
.end method

.method private updateWakeupSettingUI()V
    .locals 5

    .prologue
    .line 1563
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 1564
    .local v1, "functionNameSelected":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v0

    .line 1566
    .local v0, "functionDetailSelected":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v2, v3, v4

    .line 1567
    .local v2, "wakeupCommandNameSelected":Ljava/lang/String;
    sget v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    const/16 v4, 0x9

    if-lt v3, v4, :cond_0

    .line 1568
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v3

    invoke-direct {p0, v2, v0, v0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1576
    :goto_0
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1577
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1578
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 1580
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    .line 1581
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1582
    return-void

    .line 1572
    :cond_0
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v3

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private updateWakeupSettingUILanguage()V
    .locals 7

    .prologue
    .line 1586
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    if-nez v2, :cond_1

    .line 1611
    :cond_0
    :goto_0
    return-void

    .line 1592
    :cond_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1593
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->initStrings()V

    .line 1594
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1596
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 1597
    invoke-direct {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceIntValue(I)I

    move-result v0

    .line 1598
    .local v0, "funcPosition":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 1599
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    new-instance v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getFuncitonName(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getFuncitonSubTitle(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1596
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1605
    :cond_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    new-instance v3, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    aget-object v4, v4, v1

    const-string/jumbo v5, " "

    const-string/jumbo v6, " "

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1608
    .end local v0    # "funcPosition":I
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1609
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1610
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WorldReadableFiles"
        }
    .end annotation

    .prologue
    .line 1551
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 1552
    .local v1, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 1553
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1559
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 1554
    :catch_0
    move-exception v0

    .line 1555
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1556
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 1557
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public PlayShortAudioFileViaAudioTrack(Ljava/lang/String;)V
    .locals 13
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1894
    if-nez p1, :cond_1

    .line 1945
    :cond_0
    :goto_0
    return-void

    .line 1899
    :cond_1
    const/4 v7, 0x0

    .line 1900
    .local v7, "byteData":[B
    const/4 v10, 0x0

    .line 1901
    .local v10, "file":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    .end local v10    # "file":Ljava/io/File;
    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1902
    .restart local v10    # "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v1

    long-to-int v1, v1

    new-array v7, v1, [B

    .line 1903
    const/4 v11, 0x0

    .line 1906
    .local v11, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1907
    .end local v11    # "in":Ljava/io/FileInputStream;
    .local v12, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v12, v7}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1913
    if-eqz v12, :cond_4

    .line 1915
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v11, v12

    .line 1922
    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    const/16 v1, 0x3e80

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    .line 1924
    .local v5, "intSize":I
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    const/16 v2, 0x3e80

    const/4 v3, 0x2

    const/4 v4, 0x2

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    .line 1928
    .local v0, "at":Landroid/media/AudioTrack;
    if-eqz v0, :cond_0

    .line 1931
    :try_start_3
    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 1933
    const/4 v1, 0x0

    array-length v2, v7

    invoke-virtual {v0, v7, v1, v2}, Landroid/media/AudioTrack;->write([BII)I

    .line 1934
    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1939
    :try_start_4
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 1940
    :catch_0
    move-exception v9

    .line 1941
    .local v9, "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1916
    .end local v0    # "at":Landroid/media/AudioTrack;
    .end local v5    # "intSize":I
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v8

    .line 1917
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    move-object v11, v12

    .line 1918
    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_1

    .line 1909
    .end local v8    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 1913
    :goto_2
    if-eqz v11, :cond_2

    .line 1915
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 1916
    :catch_3
    move-exception v8

    .line 1917
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1913
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    :goto_3
    if-eqz v11, :cond_3

    .line 1915
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1918
    :cond_3
    :goto_4
    throw v1

    .line 1916
    :catch_4
    move-exception v8

    .line 1917
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1935
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v0    # "at":Landroid/media/AudioTrack;
    .restart local v5    # "intSize":I
    :catch_5
    move-exception v8

    .line 1936
    .local v8, "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1939
    :try_start_8
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_0

    .line 1940
    :catch_6
    move-exception v9

    .line 1941
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1938
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    .line 1939
    :try_start_9
    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 1942
    :goto_5
    throw v1

    .line 1940
    :catch_7
    move-exception v9

    .line 1941
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 1913
    .end local v0    # "at":Landroid/media/AudioTrack;
    .end local v5    # "intSize":I
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v1

    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 1909
    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :catch_8
    move-exception v1

    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v11    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "in":Ljava/io/FileInputStream;
    :cond_4
    move-object v11, v12

    .end local v12    # "in":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public deleteData(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1875
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1877
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1879
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1881
    :cond_0
    return-void
.end method

.method public hideBubble()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 636
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 638
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 640
    return-void
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 1884
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1885
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1886
    const/4 v1, 0x1

    .line 1888
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 28
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1165
    invoke-super/range {p0 .. p3}, Lcom/vlingo/midas/ui/VLActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1168
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 1169
    sget v4, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1175
    if-nez p2, :cond_2

    .line 1176
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-nez v4, :cond_0

    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFunctionAvailable(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1419
    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    .line 1420
    const/4 v4, 0x0

    sput-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    .line 1422
    :cond_1
    :goto_1
    return-void

    .line 1182
    :cond_2
    const/4 v4, -0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_1b

    .line 1183
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1194
    :pswitch_0
    const/16 v22, 0x0

    .line 1197
    .local v22, "prefs":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v22

    .line 1198
    const-string/jumbo v4, "wakeupCommand"

    const-string/jumbo v6, ""

    move-object/from16 v0, v22

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 1199
    .local v26, "wakeupCommandName":Ljava/lang/String;
    const-string/jumbo v4, "Function"

    const-string/jumbo v6, ""

    move-object/from16 v0, v22

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1200
    .local v16, "functionName":Ljava/lang/String;
    const-string/jumbo v4, "FunctionPosition"

    const/4 v6, -0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v18

    .line 1202
    .local v18, "functionPosition":I
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_3

    .line 1206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1207
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getTitle()Ljava/lang/String;

    move-result-object v17

    .line 1210
    .local v17, "functionNameSelected":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v18

    .line 1211
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v15

    .line 1221
    .local v15, "functionDetailSelected":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v27, v4, v6

    .line 1222
    .local v27, "wakeupCommandNameSelected":Ljava/lang/String;
    sget v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    const/16 v6, 0x9

    if-lt v4, v6, :cond_5

    .line 1223
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v18

    invoke-direct {v0, v1, v15, v15, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1231
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    .line 1232
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNeedToShowFunctionList:Z

    if-eqz v4, :cond_0

    .line 1233
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1234
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1235
    sget v4, Lcom/vlingo/midas/R$string;->title_change_voice_function:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1236
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1237
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    goto/16 :goto_0

    .line 1214
    .end local v15    # "functionDetailSelected":Ljava/lang/String;
    .end local v17    # "functionNameSelected":Ljava/lang/String;
    .end local v27    # "wakeupCommandNameSelected":Ljava/lang/String;
    :cond_3
    move-object/from16 v17, v16

    .line 1215
    .restart local v17    # "functionNameSelected":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getIndex()I

    move-result v4

    const/16 v6, 0x9

    if-ge v4, v6, :cond_4

    .line 1216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "functionDetailSelected":Ljava/lang/String;
    goto :goto_2

    .line 1218
    .end local v15    # "functionDetailSelected":Ljava/lang/String;
    :cond_4
    const-string/jumbo v4, "FunctionDetail"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "functionDetailSelected":Ljava/lang/String;
    goto :goto_2

    .line 1227
    .restart local v27    # "wakeupCommandNameSelected":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v17

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v15, v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    .line 1242
    .end local v15    # "functionDetailSelected":Ljava/lang/String;
    .end local v16    # "functionName":Ljava/lang/String;
    .end local v17    # "functionNameSelected":Ljava/lang/String;
    .end local v18    # "functionPosition":I
    .end local v22    # "prefs":Landroid/content/SharedPreferences;
    .end local v26    # "wakeupCommandName":Ljava/lang/String;
    .end local v27    # "wakeupCommandNameSelected":Ljava/lang/String;
    :pswitch_1
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_6

    .line 1243
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1244
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_1

    .line 1248
    :cond_6
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1250
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1251
    .local v5, "Uri":Landroid/net/Uri;
    const/16 v19, 0x0

    .line 1252
    .local v19, "mCursor":Landroid/database/Cursor;
    const-string/jumbo v21, ""

    .line 1253
    .local v21, "phoneNumber":Ljava/lang/String;
    const-string/jumbo v13, ""

    .line 1255
    .local v13, "displayName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1256
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1257
    const-string/jumbo v4, "data1"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1258
    const-string/jumbo v4, "display_name"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1259
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Dial "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 1260
    .local v24, "subTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isContactAvailable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1261
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1263
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1270
    if-eqz v19, :cond_7

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1271
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 1266
    :cond_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mContactNameArray:Ljava/util/List;

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1270
    if-eqz v19, :cond_9

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1271
    :cond_9
    const/16 v19, 0x0

    .line 1275
    .end local v24    # "subTitle":Ljava/lang/String;
    :goto_4
    const-string/jumbo v4, "wakeupdata_DirectDial.txt"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Dial "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->setSubTitle(Ljava/lang/String;)V

    .line 1279
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_c

    .line 1280
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    goto/16 :goto_1

    .line 1268
    :catch_0
    move-exception v4

    .line 1270
    if-eqz v19, :cond_a

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1271
    :cond_a
    const/16 v19, 0x0

    .line 1272
    goto :goto_4

    .line 1270
    :catchall_0
    move-exception v4

    if-eqz v19, :cond_b

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1271
    :cond_b
    const/16 v19, 0x0

    throw v4

    .line 1282
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    goto/16 :goto_1

    .line 1287
    .end local v5    # "Uri":Landroid/net/Uri;
    .end local v13    # "displayName":Ljava/lang/String;
    .end local v19    # "mCursor":Landroid/database/Cursor;
    .end local v21    # "phoneNumber":Ljava/lang/String;
    :pswitch_2
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_d

    .line 1288
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1289
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_1

    .line 1292
    :cond_d
    invoke-static {}, Lcom/vlingo/core/internal/contacts/ContactDBUtilManager;->getContactDBUtil()Lcom/vlingo/core/internal/contacts/IContactDBUtil;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/vlingo/core/internal/contacts/IContactDBUtil;->isProviderStatusNormal(Landroid/content/ContentResolver;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1294
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1295
    .restart local v5    # "Uri":Landroid/net/Uri;
    const/16 v19, 0x0

    .line 1296
    .restart local v19    # "mCursor":Landroid/database/Cursor;
    const-string/jumbo v21, ""

    .line 1297
    .restart local v21    # "phoneNumber":Ljava/lang/String;
    const-string/jumbo v13, ""

    .line 1298
    .restart local v13    # "displayName":Ljava/lang/String;
    const-string/jumbo v24, ""

    .line 1300
    .restart local v24    # "subTitle":Ljava/lang/String;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 1301
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1302
    const-string/jumbo v4, "data1"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1303
    const-string/jumbo v4, "display_name"

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1304
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Message "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 1305
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isMessageAvailable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1306
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1307
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1308
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1315
    if-eqz v19, :cond_e

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1316
    :cond_e
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 1311
    :cond_f
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mMessageNameArray:Ljava/util/List;

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1315
    if-eqz v19, :cond_10

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1316
    :cond_10
    const/16 v19, 0x0

    .line 1319
    :goto_5
    const-string/jumbo v4, "wakeupdata_DirectMessage.txt"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Message "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->setSubTitle(Ljava/lang/String;)V

    .line 1324
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_13

    .line 1325
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    goto/16 :goto_1

    .line 1313
    :catch_1
    move-exception v4

    .line 1315
    if-eqz v19, :cond_11

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1316
    :cond_11
    const/16 v19, 0x0

    .line 1317
    goto :goto_5

    .line 1315
    :catchall_1
    move-exception v4

    if-eqz v19, :cond_12

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1316
    :cond_12
    const/16 v19, 0x0

    throw v4

    .line 1327
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    goto/16 :goto_1

    .line 1332
    .end local v5    # "Uri":Landroid/net/Uri;
    .end local v13    # "displayName":Ljava/lang/String;
    .end local v19    # "mCursor":Landroid/database/Cursor;
    .end local v21    # "phoneNumber":Ljava/lang/String;
    .end local v24    # "subTitle":Ljava/lang/String;
    :pswitch_3
    if-nez p3, :cond_14

    .line 1333
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1334
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_1

    .line 1336
    :cond_14
    const-string/jumbo v4, "address"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1337
    .local v10, "address":Ljava/lang/String;
    const-string/jumbo v4, "name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1338
    .local v23, "shortcutName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isNavigationAddressAvailable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1339
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1340
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1341
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V

    goto/16 :goto_1

    .line 1344
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mNavigationAddressArray:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1346
    const-string/jumbo v4, "wakeupdata_NavigationShortcut.txt"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v10}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->setSubTitle(Ljava/lang/String;)V

    .line 1348
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_16

    .line 1349
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    goto/16 :goto_1

    .line 1351
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    goto/16 :goto_1

    .line 1357
    .end local v10    # "address":Ljava/lang/String;
    .end local v23    # "shortcutName":Ljava/lang/String;
    :pswitch_4
    if-nez p3, :cond_17

    .line 1358
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1359
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_1

    .line 1361
    :cond_17
    const-string/jumbo v4, "selected_package"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1362
    .local v20, "packageName":Ljava/lang/String;
    const-string/jumbo v4, "selected_activity"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1363
    .local v12, "className":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1365
    .local v11, "applicationName":Ljava/lang/CharSequence;
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v6, v0, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v11

    .line 1369
    :goto_6
    if-eqz v11, :cond_19

    .line 1370
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isApplicationAvailable(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 1371
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1372
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/16 v6, 0x8

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1373
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->launchFunctionAlertDialog()V

    goto/16 :goto_1

    .line 1366
    :catch_2
    move-exception v14

    .line 1367
    .local v14, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v14}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_6

    .line 1376
    .end local v14    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mApplicationArray:Ljava/util/List;

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1378
    const-string/jumbo v4, "wakeupdata_OpenApplication.txt"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1379
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->setSubTitle(Ljava/lang/String;)V

    .line 1381
    :cond_19
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_1a

    .line 1382
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    goto/16 :goto_1

    .line 1384
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    goto/16 :goto_1

    .line 1392
    .end local v11    # "applicationName":Ljava/lang/CharSequence;
    .end local v12    # "className":Ljava/lang/String;
    .end local v20    # "packageName":Ljava/lang/String;
    :cond_1b
    const/16 v4, 0x6f

    move/from16 v0, p2

    if-ne v0, v4, :cond_1e

    .line 1393
    if-nez p3, :cond_1c

    .line 1394
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1395
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_1

    .line 1397
    :cond_1c
    const-string/jumbo v4, "URL"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1398
    .local v25, "url":Ljava/lang/String;
    const-string/jumbo v4, "TITLE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1399
    .restart local v23    # "shortcutName":Ljava/lang/String;
    const-string/jumbo v4, "wakeupdata_BookmarkShortcut.txt"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v4, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->writeWakeupDataFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFunctionPositionSelected:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$FunctionItem;->setSubTitle(Ljava/lang/String;)V

    .line 1401
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_1d

    .line 1402
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startCustomCommandRecordingActivity()V

    goto/16 :goto_1

    .line 1404
    :cond_1d
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUI()V

    goto/16 :goto_1

    .line 1414
    .end local v23    # "shortcutName":Ljava/lang/String;
    .end local v25    # "url":Ljava/lang/String;
    :cond_1e
    sget-boolean v4, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    if-eqz v4, :cond_0

    .line 1415
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    sget v6, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    aget-object v4, v4, v6

    const-string/jumbo v6, ""

    const-string/jumbo v7, ""

    const/4 v8, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6, v7, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1416
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->readFromPreferenceAndUpdateList()V

    goto/16 :goto_0

    .line 1183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1808
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1810
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1811
    sget v0, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1812
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1813
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 1817
    :goto_0
    return-void

    .line 1815
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 646
    if-eqz p1, :cond_0

    .line 647
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 649
    :cond_0
    sget v2, Lcom/vlingo/midas/R$id;->left_container:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 650
    .local v0, "leftContainer":Landroid/widget/RelativeLayout;
    sget v2, Lcom/vlingo/midas/R$id;->right_container:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 651
    .local v1, "rightContainer":Landroid/widget/RelativeLayout;
    if-eqz p1, :cond_1

    .line 652
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 653
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 654
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 655
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 662
    :cond_1
    :goto_0
    return-void

    .line 656
    :cond_2
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 657
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 658
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 22
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 242
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 243
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setRequestedOrientation(I)V

    .line 246
    :cond_0
    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    .line 247
    .local v11, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 248
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mDensity:F

    .line 250
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x3ff0000000000000L    # 1.0

    cmpl-double v17, v17, v19

    if-nez v17, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    iget v0, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    add-int v17, v17, v18

    const/16 v18, 0x7f0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v17, v0

    const v18, 0x4315d32c

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v17, v0

    const v18, 0x424a12f7

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-eqz v17, :cond_2

    :cond_1
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x3ff0000000000000L    # 1.0

    cmpl-double v17, v17, v19

    if-nez v17, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    iget v0, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    add-int v17, v17, v18

    const/16 v18, 0x820

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v17, v0

    const v18, 0x4315d2f2

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v17, v0

    const v18, 0x4316849c

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_7

    .line 255
    :cond_2
    sget v17, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mTheme:I

    .line 256
    sget v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mTheme:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTheme(I)V

    .line 263
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 264
    sget v17, Lcom/vlingo/midas/R$style;->actionBarStyleWakeup:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTheme(I)V

    .line 267
    :cond_3
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 268
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v17, v0

    move/from16 v0, v17

    div-int/lit16 v0, v0, 0xa0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->density:I

    .line 273
    const-string/jumbo v17, "/system/lib/libSensoryUDTSIDEngine.so"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    .line 275
    sget v17, Lcom/vlingo/midas/R$layout;->wakeupcommand:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setContentView(I)V

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    .line 280
    .local v14, "titlebar":Landroid/app/ActionBar;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 281
    if-eqz v14, :cond_4

    .line 282
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 289
    :cond_4
    :goto_1
    sget v17, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Display;->getRotation()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Display;->getRotation()I

    move-result v17

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 296
    :cond_5
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 301
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->initStrings()V

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string/jumbo v18, "svoice"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromMainApp:Z

    .line 305
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string/jumbo v18, "helpapp"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromHelpApp:Z

    .line 307
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_9

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupPreference:[Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->preferenceNameArray:[Ljava/lang/String;

    move-object/from16 v18, v0

    aget-object v18, v18, v9

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    aput-object v18, v17, v9

    .line 307
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 258
    .end local v9    # "i":I
    .end local v14    # "titlebar":Landroid/app/ActionBar;
    :cond_7
    sget v17, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mTheme:I

    goto/16 :goto_0

    .line 285
    .restart local v14    # "titlebar":Landroid/app/ActionBar;
    :cond_8
    if-eqz v14, :cond_4

    .line 286
    const/16 v17, 0xe

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_1

    .line 311
    .restart local v9    # "i":I
    :cond_9
    sget v17, Lcom/vlingo/midas/R$id;->functionList:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ListView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    .line 312
    sget v17, Lcom/vlingo/midas/R$id;->wakeupList:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ListView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    .line 313
    sget v17, Lcom/vlingo/midas/R$id;->custom_wakeup_setting_bubble_tv_1:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    .line 314
    sget v17, Lcom/vlingo/midas/R$id;->custom_wakeup_setting_bubble_tv_2:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 317
    sget v17, Lcom/vlingo/midas/R$id;->listMainLayout:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mListMainLayout:Landroid/widget/RelativeLayout;

    .line 319
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    .line 320
    const/4 v9, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_b

    .line 322
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceIntValue(I)I

    move-result v8

    .line 323
    .local v8, "funcPosition":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_a

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v18, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getPreferenceTitleValue(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getFuncitonSubTitle(I)Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v18 .. v21}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 333
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v18, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v9

    const-string/jumbo v20, " "

    const-string/jumbo v21, " "

    invoke-direct/range {v18 .. v21}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 338
    .end local v8    # "funcPosition":I
    :cond_b
    sget-boolean v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->enrolled:Z

    if-eqz v17, :cond_c

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandNameArray:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget-object v17, v17, v18

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->addToPreference(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 343
    :cond_c
    new-instance v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;

    sget v18, Lcom/vlingo/midas/R$layout;->wakeup_twoline_list_item:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandArrayList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move/from16 v3, v18

    move-object/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$1;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;ILjava/util/List;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    new-instance v18, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$2;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 415
    new-instance v17, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;

    sget v18, Lcom/vlingo/midas/R$layout;->function_list_item:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionNameArray:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move/from16 v3, v18

    move-object/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$3;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;ILjava/util/List;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    move-object/from16 v17, v0

    new-instance v18, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$4;-><init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 580
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v10

    .line 581
    .local v10, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "privateFlags"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v12

    .line 582
    .local v12, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 583
    .local v13, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 585
    .local v6, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    .local v5, "currentPrivateFlags":I
    const/16 v16, 0x0

    .local v16, "valueofFlagsEnableStatusBar":I
    const/4 v15, 0x0

    .line 586
    .local v15, "valueofFlagsDisableTray":I
    invoke-virtual {v12, v10}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    .line 587
    invoke-virtual {v13, v10}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v16

    .line 588
    invoke-virtual {v6, v10}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v15

    .line 590
    or-int v5, v5, v16

    .line 591
    or-int/2addr v5, v15

    .line 593
    invoke-virtual {v12, v10, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 594
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    .end local v5    # "currentPrivateFlags":I
    .end local v6    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v10    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v12    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v13    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v15    # "valueofFlagsDisableTray":I
    .end local v16    # "valueofFlagsEnableStatusBar":I
    :goto_5
    return-void

    .line 598
    :catch_0
    move-exception v7

    .line 601
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 595
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v17

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1820
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 1824
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mRequestAudioFocusHandler:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$RequestAudioFocusHandler;->removeMessages(I)V

    .line 1825
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1826
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 1827
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 1828
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 1831
    :cond_0
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 2092
    if-nez p1, :cond_0

    .line 2093
    sget-object v0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 2095
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 666
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 678
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 668
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 670
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 671
    sget v0, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 672
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 673
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    goto :goto_0

    .line 675
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->finish()V

    goto :goto_0

    .line 666
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1799
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 1802
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->inIUXMode:Z

    if-eqz v0, :cond_0

    .line 1803
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->finish()V

    .line 1804
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1747
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1748
    const-string/jumbo v2, "mFirstInit"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    .line 1749
    const-string/jumbo v2, "wakeupCommandPositonSelected"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    .line 1750
    const-string/jumbo v2, "isFuncListVisible"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1751
    .local v0, "isFuncListVisible":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1752
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1753
    sget v2, Lcom/vlingo/midas/R$string;->title_change_voice_function:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1758
    :goto_0
    const-string/jumbo v2, "isWakeupListVisible"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1759
    .local v1, "isWakeupListVisible":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1760
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1761
    sget v2, Lcom/vlingo/midas/R$string;->wakeup_setting_dialog_title:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1766
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->showBubble()V

    .line 1768
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupTwoLineListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1769
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1770
    return-void

    .line 1755
    .end local v1    # "isWakeupListVisible":Ljava/lang/Boolean;
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 1763
    .restart local v1    # "isWakeupListVisible":Ljava/lang/Boolean;
    :cond_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1773
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 1776
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1777
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->updateWakeupSettingUILanguage()V

    .line 1778
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "wycs.show.done"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->inIUXMode:Z

    .line 1779
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vlingo/core/internal/vlservice/VlingoApplicationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1780
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v1, "com.vlingo.client.app.action.ACTIVITY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1781
    const-string/jumbo v1, "com.vlingo.client.app.extra.STATE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1782
    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1784
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1785
    new-instance v1, Landroid/speech/tts/TextToSpeech;

    invoke-direct {v1, p0, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->tts:Landroid/speech/tts/TextToSpeech;

    .line 1787
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1791
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1792
    const-string/jumbo v0, "mFirstInit"

    sget-boolean v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mFirstInit:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1793
    const-string/jumbo v0, "isFuncListVisible"

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->isShown()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1794
    const-string/jumbo v0, "isWakeupListVisible"

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->isShown()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1795
    const-string/jumbo v0, "wakeupCommandPositonSelected"

    sget v1, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandPositonSelected:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1796
    return-void
.end method

.method public resetWakeup()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 1952
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->resetPreferenceValueChange()V

    .line 1953
    const-string/jumbo v1, "/system/lib/libSensoryUDTSIDEngine.so"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    .line 1955
    const/4 v1, 0x4

    new-array v0, v1, [I

    .line 1957
    .local v0, "assignCommandArray":[I
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->mVoiceEngine:Lcom/samsung/voiceshell/VoiceEngine;

    sget-object v2, Lcom/samsung/voiceshell/VoiceEngine;->typeDefine:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v4}, Lcom/samsung/voiceshell/VoiceEngine;->functionAssignment(Ljava/lang/String;[II)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1959
    aput v3, v0, v4

    .line 1960
    const/4 v1, 0x1

    aput v3, v0, v1

    .line 1961
    const/4 v1, 0x2

    aput v3, v0, v1

    .line 1962
    const/4 v1, 0x3

    aput v3, v0, v1

    .line 1965
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_1

    .line 1966
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_0.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1967
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_1.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1968
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_2.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1969
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_3.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1970
    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1971
    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1972
    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1973
    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_Deep_search.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1983
    :goto_0
    const-string/jumbo v1, "kew_wake_up_and_auto_function1"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1984
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_2

    .line 1985
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_0.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1986
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_1.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1987
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_2.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1988
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_3.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1997
    :goto_1
    const-string/jumbo v1, "kew_wake_up_and_auto_function2"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 1998
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_3

    .line 1999
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_0.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2000
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_1.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2001
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_2.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2002
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_3.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2011
    :goto_2
    const-string/jumbo v1, "kew_wake_up_and_auto_function3"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 2012
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_4

    .line 2013
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_0.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2014
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_1.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2015
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_2.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2016
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_3.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2026
    :goto_3
    const-string/jumbo v1, "kew_wake_up_and_auto_function4"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 2027
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isSensoryUDTSIDsoFileExist:Z

    if-eqz v1, :cond_5

    .line 2028
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_0.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2029
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_1.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2030
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_2.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2031
    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_3.wav"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2040
    :goto_4
    return-void

    .line 1975
    :cond_1
    const-string/jumbo v1, "/data/data/com.vlingo.midas/kwd_1.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1976
    const-string/jumbo v1, "/data/data/com.vlingo.midas/modelStatus_1.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1977
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR_1.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1978
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR4_1.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1979
    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_1.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1980
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_1.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1990
    :cond_2
    const-string/jumbo v1, "/data/data/com.vlingo.midas/kwd_2.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1991
    const-string/jumbo v1, "/data/data/com.vlingo.midas/modelStatus_2.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1992
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR_2.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1993
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR4_2.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1994
    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_2.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 1995
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_2.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2004
    :cond_3
    const-string/jumbo v1, "/data/data/com.vlingo.midas/kwd_3.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2005
    const-string/jumbo v1, "/data/data/com.vlingo.midas/modelStatus_3.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2006
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR_3.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2007
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR4_3.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2008
    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_3.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2009
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_3.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2018
    :cond_4
    const-string/jumbo v1, "/data/data/com.vlingo.midas/kwd_4.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2019
    const-string/jumbo v1, "/data/data/com.vlingo.midas/modelStatus_4.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2020
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR_4.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2021
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR4_4.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2022
    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_4.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2023
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_4.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2033
    :cond_5
    const-string/jumbo v1, "/data/data/com.vlingo.midas/kwd_5.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2034
    const-string/jumbo v1, "/data/data/com.vlingo.midas/modelStatus_5.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2035
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR_5.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2036
    const-string/jumbo v1, "/data/data/com.vlingo.midas/enSTR4_5.bin"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2037
    const-string/jumbo v1, "/data/data/com.vlingo.midas/lastEnrollUtt_5.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    .line 2038
    const-string/jumbo v1, "/data/data/com.vlingo.midas/realLastEnrollUtt_5.pcm"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->deleteData(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method public showBubble()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 614
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupCommandListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    if-nez v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 620
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->isFromHelpApp:Z

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->functionListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 622
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 623
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 624
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    goto :goto_0

    .line 627
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 628
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 629
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 630
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->customWakeupBubbleTextView2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    goto :goto_0
.end method
