.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setClickHandlers()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 719
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 722
    monitor-enter p1

    .line 723
    :try_start_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 725
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING_IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-ne v0, v1, :cond_1

    .line 726
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->startVerify(I)I

    .line 727
    const/4 v0, 0x0

    # setter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1302(I)I

    .line 728
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 745
    :cond_0
    :goto_0
    monitor-exit p1

    .line 746
    return-void

    .line 731
    :cond_1
    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7$1;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;)V

    const-wide/16 v1, 0x64

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 745
    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
