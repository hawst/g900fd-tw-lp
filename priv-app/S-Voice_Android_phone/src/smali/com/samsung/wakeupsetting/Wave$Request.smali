.class public final enum Lcom/samsung/wakeupsetting/Wave$Request;
.super Ljava/lang/Enum;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/wakeupsetting/Wave$Request;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum NONE:Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum REDO:Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

.field public static final enum START:Lcom/samsung/wakeupsetting/Wave$Request;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->NONE:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 58
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "START"

    invoke-direct {v0, v1, v4}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->START:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 59
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "NEXT"

    invoke-direct {v0, v1, v5}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 60
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "REDO"

    invoke-direct {v0, v1, v6}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 61
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "RESTART"

    invoke-direct {v0, v1, v7}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 62
    new-instance v0, Lcom/samsung/wakeupsetting/Wave$Request;

    const-string/jumbo v1, "INTERNAL_NEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/wakeupsetting/Wave$Request;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    .line 56
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/wakeupsetting/Wave$Request;

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NONE:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->START:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->REDO:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/wakeupsetting/Wave$Request;->RESTART:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/wakeupsetting/Wave$Request;->INTERNAL_NEXT:Lcom/samsung/wakeupsetting/Wave$Request;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$Request;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/wakeupsetting/Wave$Request;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/wakeupsetting/Wave$Request;

    return-object v0
.end method

.method public static values()[Lcom/samsung/wakeupsetting/Wave$Request;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/wakeupsetting/Wave$Request;->$VALUES:[Lcom/samsung/wakeupsetting/Wave$Request;

    invoke-virtual {v0}, [Lcom/samsung/wakeupsetting/Wave$Request;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/wakeupsetting/Wave$Request;

    return-object v0
.end method
