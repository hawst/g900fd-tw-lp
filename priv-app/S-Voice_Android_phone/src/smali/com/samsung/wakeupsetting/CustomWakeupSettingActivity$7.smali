.class Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;
.super Landroid/widget/ArrayAdapter;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->initializeVoiceTalkAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # [Ljava/lang/String;

    .prologue
    .line 853
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 857
    if-nez p2, :cond_1

    .line 858
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 859
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x1090003

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 863
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .local v1, "row":Landroid/widget/TextView;
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$color;->edit_text_color_dark_theme:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 864
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    iget-object v2, v2, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupDialogVoiceTalkArray:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 867
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 868
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->density:I
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x11

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->density:I
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1100(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x11

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 870
    if-eq p1, v6, :cond_0

    if-nez p1, :cond_3

    .line 871
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->checkPreferenceValue()V
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 872
    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupRecordSuccess:Ljava/lang/Boolean;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1200()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 873
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 882
    :goto_1
    return-object v1

    .end local v1    # "row":Landroid/widget/TextView;
    :cond_1
    move-object v1, p2

    .line 861
    check-cast v1, Landroid/widget/TextView;

    .restart local v1    # "row":Landroid/widget/TextView;
    goto :goto_0

    .line 876
    :cond_2
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 880
    :cond_3
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 887
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 888
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$7;->this$0:Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->checkPreferenceValue()V
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$000(Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;)V

    .line 889
    # getter for: Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->wakeupRecordSuccess:Ljava/lang/Boolean;
    invoke-static {}, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;->access$1200()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 890
    const/4 v0, 0x0

    .line 897
    :cond_1
    return v0
.end method
