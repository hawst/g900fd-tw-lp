.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;
.super Ljava/lang/Object;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->launchResetAlertDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x0

    .line 847
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/kwd_1.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 848
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/modelStatus_1.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 849
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR_1.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 850
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/enSTR4_1.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 851
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/lastEnrollUtt_1.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 852
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/realLastEnrollUtt_1.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 853
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 854
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 855
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 856
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_Deep_search.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 857
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user0_0.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 858
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user0_1.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 859
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user0_2.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 860
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/user0_3.wav"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 861
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/ListenToMyVoice.pcm"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 862
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/THRESHOLD.bin"

    invoke-virtual {v2, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->deleteData(Ljava/lang/String;)V

    .line 864
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const-string/jumbo v2, "WakeupCommandPreference1"

    aput-object v2, v1, v5

    .line 865
    .local v1, "preferenceNameArray":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 866
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    aget-object v3, v1, v5

    iget-object v4, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v4}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v2, v3, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 867
    const-string/jumbo v2, "isRecorded"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 868
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 869
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->hasCarModeApp()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 870
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$600(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Lcom/vlingo/midas/settings/TwoLinePreference;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body:I

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    .line 874
    :goto_0
    const-string/jumbo v2, "samsung_wakeup_engine_enable"

    invoke-static {v2, v5}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 875
    sget-object v2, Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;->PhraseSpotter:Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;

    invoke-static {v2}, Lcom/vlingo/core/internal/CoreAdapterRegistrar;->unregisterHandler(Lcom/vlingo/core/internal/CoreAdapterRegistrar$AdapterList;)V

    .line 876
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.samsung.CUSTOM_COMMAND_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 878
    return-void

    .line 872
    :cond_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$17;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mSetWakeup:Lcom/vlingo/midas/settings/TwoLinePreference;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$600(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Lcom/vlingo/midas/settings/TwoLinePreference;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->setting_voice_wakeup_screenoff_body_without_carmode:I

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/settings/TwoLinePreference;->setSummary(I)V

    goto :goto_0
.end method
