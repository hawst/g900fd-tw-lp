.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;
.super Landroid/os/Handler;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SystemTTS"
.end annotation


# static fields
.field private static final MSG_SPEAK:I = 0xa


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

.field tts:Landroid/speech/tts/TextToSpeech;

.field private ttsReady:Z


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 421
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .line 422
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 418
    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->ttsReady:Z

    .line 423
    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->ttsReady:Z

    .line 424
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v3, 0xa

    .line 429
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 430
    .local v1, "text":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->ttsReady:Z

    if-eqz v2, :cond_0

    .line 432
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 440
    :goto_0
    return-void

    .line 436
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->removeMessages(I)V

    .line 437
    invoke-virtual {p0, v3, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 438
    .local v0, "m":Landroid/os/Message;
    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 458
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    .line 459
    return-void
.end method

.method public onInit(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 476
    if-nez p1, :cond_0

    .line 477
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getCurrentSystemLanguage()Ljava/util/Locale;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->setLanguage(Ljava/util/Locale;)V

    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->ttsReady:Z

    .line 480
    :cond_0
    return-void
.end method

.method public setLanguage(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 462
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 466
    :cond_0
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 472
    :cond_0
    return-void
.end method

.method public speak(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xa

    .line 443
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_0

    .line 444
    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->removeMessages(I)V

    .line 445
    invoke-virtual {p0, v2, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 446
    .local v0, "m":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 448
    .end local v0    # "m":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 451
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->removeMessages(I)V

    .line 452
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 455
    :cond_0
    return-void
.end method
