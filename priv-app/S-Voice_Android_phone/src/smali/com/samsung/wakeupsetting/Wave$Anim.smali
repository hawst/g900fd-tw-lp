.class Lcom/samsung/wakeupsetting/Wave$Anim;
.super Ljava/lang/Object;
.source "Wave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/Wave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Anim"
.end annotation


# instance fields
.field delay:J

.field duration:J

.field elapsed:J

.field frac:F

.field interpolator:Landroid/view/animation/Interpolator;

.field isAnimating:Z

.field final synthetic this$0:Lcom/samsung/wakeupsetting/Wave;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/Wave;Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p2, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->this$0:Lcom/samsung/wakeupsetting/Wave;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->interpolator:Landroid/view/animation/Interpolator;

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 79
    return-void
.end method


# virtual methods
.method reset()V
    .locals 2

    .prologue
    .line 130
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 131
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    .line 132
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 134
    return-void
.end method

.method start(I)V
    .locals 4
    .param p1, "duration"    # I

    .prologue
    const-wide/16 v2, 0x0

    .line 82
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    .line 83
    iput-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 84
    iput-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->delay:J

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 87
    return-void
.end method

.method start(II)V
    .locals 2
    .param p1, "duration"    # I
    .param p2, "delay"    # I

    .prologue
    .line 90
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    .line 91
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 92
    int-to-long v0, p2

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->delay:J

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 95
    return-void
.end method

.method startEndless(I)V
    .locals 2
    .param p1, "interval"    # I

    .prologue
    .line 98
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    .line 99
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 102
    return-void
.end method

.method tick(J)Z
    .locals 8
    .param p1, "delta"    # J

    .prologue
    .line 106
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    if-eqz v2, :cond_0

    .line 107
    iget-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    add-long/2addr v2, p1

    iput-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 108
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    iget-wide v6, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->delay:J

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 109
    .local v0, "time":J
    iget-wide v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    iget-wide v4, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    iget-wide v6, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->delay:J

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 110
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    .line 114
    .end local v0    # "time":J
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    return v2

    .line 112
    .restart local v0    # "time":J
    :cond_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->interpolator:Landroid/view/animation/Interpolator;

    long-to-float v3, v0

    iget-wide v4, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    invoke-interface {v2, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    goto :goto_0
.end method

.method toDistance(I)I
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 154
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 155
    .local v0, "f":F
    :goto_0
    int-to-float v1, p1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    return v1

    .line 154
    .end local v0    # "f":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method toFadeInAlpha()I
    .locals 3

    .prologue
    .line 137
    const/16 v0, 0xff

    .line 138
    .local v0, "alpha":I
    iget v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 139
    const/high16 v1, 0x437f0000    # 255.0f

    iget v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 141
    :cond_0
    return v0
.end method

.method toFadeOutAlpha(F)I
    .locals 5
    .param p1, "limit"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 145
    const/16 v0, 0xff

    .line 146
    .local v0, "alpha":I
    sub-float v2, v4, p1

    iget v3, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 147
    .local v1, "f":F
    cmpg-float v2, v1, v4

    if-gtz v2, :cond_0

    .line 148
    const/high16 v2, 0x437f0000    # 255.0f

    sub-float v3, v4, v1

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 150
    :cond_0
    return v0
.end method

.method toReverseDistance(F)F
    .locals 3
    .param p1, "length"    # F

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 159
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    if-eqz v2, :cond_0

    iget v0, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->frac:F

    .line 160
    .local v0, "f":F
    :goto_0
    sub-float/2addr v1, v0

    mul-float/2addr v1, p1

    return v1

    .end local v0    # "f":F
    :cond_0
    move v0, v1

    .line 159
    goto :goto_0
.end method

.method tock(J)Z
    .locals 5
    .param p1, "delta"    # J

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "shift":Z
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->isAnimating:Z

    if-eqz v1, :cond_0

    .line 120
    iget-wide v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    add-long/2addr v1, p1

    iput-wide v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 121
    iget-wide v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    iget-wide v3, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->duration:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 123
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/samsung/wakeupsetting/Wave$Anim;->elapsed:J

    .line 126
    :cond_0
    return v0
.end method
