.class Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;
.super Ljava/lang/Object;
.source "VoiceWakeupSettingsEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->showWakeupLockScreenDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    iput-object p2, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 165
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    iget-object v1, v1, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->pref:Landroid/preference/Preference;

    sget v2, Lcom/vlingo/midas/R$string;->on:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$000(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 168
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->updateUIVoiceWakeupSettings(Z)V

    .line 170
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    const/4 v0, 0x0

    .line 172
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "preferences_voice_wake_up"

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 173
    const-string/jumbo v1, "VOICE_WAKE_UP_POP"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 174
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 176
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isH_Device()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 177
    :cond_2
    const-string/jumbo v1, "car_word_spotter_enabled"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 178
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 179
    const-string/jumbo v1, "preferences_voice_wake_up_time"

    invoke-static {v1, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 180
    const-string/jumbo v1, "voice_wake_up"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 186
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "com.vlingo.midas"

    const-string/jumbo v3, "WKON"

    invoke-static {v1, v2, v3}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void

    .line 182
    :cond_3
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler$3;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeupSettingsEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "wake_up_lock_screen"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method
