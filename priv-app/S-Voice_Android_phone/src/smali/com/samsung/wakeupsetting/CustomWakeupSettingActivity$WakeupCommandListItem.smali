.class public Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;
.super Ljava/lang/Object;
.source "CustomWakeupSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WakeupCommandListItem"
.end annotation


# instance fields
.field private detail:Ljava/lang/String;

.field private function:Ljava/lang/String;

.field private wakeupcommand:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "wakeupcommand"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;
    .param p3, "detail"    # Ljava/lang/String;

    .prologue
    .line 1854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->wakeupcommand:Ljava/lang/String;

    .line 1856
    iput-object p2, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->function:Ljava/lang/String;

    .line 1857
    iput-object p3, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->detail:Ljava/lang/String;

    .line 1858
    return-void
.end method


# virtual methods
.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1861
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->wakeupcommand:Ljava/lang/String;

    return-object v0
.end method

.method public getFunction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->function:Ljava/lang/String;

    return-object v0
.end method

.method public getFunctionDetail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1869
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeupSettingActivity$WakeupCommandListItem;->detail:Ljava/lang/String;

    return-object v0
.end method
