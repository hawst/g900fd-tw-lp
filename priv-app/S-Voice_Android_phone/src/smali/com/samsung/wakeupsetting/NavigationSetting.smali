.class public Lcom/samsung/wakeupsetting/NavigationSetting;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "NavigationSetting.java"


# static fields
.field private static log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private intent:Landroid/content/Intent;

.field private mButtonSave:Landroid/widget/Button;

.field private mEditAddress:Landroid/widget/EditText;

.field private mEditShortcutName:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/wakeupsetting/NavigationSetting;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/wakeupsetting/NavigationSetting;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/NavigationSetting;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditAddress:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/NavigationSetting;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditShortcutName:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/NavigationSetting;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/NavigationSetting;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    sget v12, Lcom/vlingo/midas/R$layout;->navigation_setting:I

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->setContentView(I)V

    .line 41
    const/4 v5, 0x0

    .line 44
    .local v5, "naviDrawable":Landroid/graphics/drawable/Drawable;
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 45
    .local v3, "i":Landroid/content/Intent;
    const-string/jumbo v12, "com.google.android.apps.maps"

    const-string/jumbo v13, "com.google.googlenav.appwidget.gohome.GoHomeCreateShortcutActivityAlias"

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    invoke-virtual {v12, v3}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 51
    .end local v3    # "i":Landroid/content/Intent;
    :goto_0
    sget v12, Lcom/vlingo/midas/R$string;->title_navigation_shortcut:I

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->getActionBar()Landroid/app/ActionBar;

    move-result-object v9

    .line 53
    .local v9, "titlebar":Landroid/app/ActionBar;
    const/16 v12, 0xe

    invoke-virtual {v9, v12}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 54
    invoke-virtual {v9, v5}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 56
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->getIntent()Landroid/content/Intent;

    move-result-object v12

    iput-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;

    .line 58
    sget v12, Lcom/vlingo/midas/R$id;->edit_address:I

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    iput-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditAddress:Landroid/widget/EditText;

    .line 59
    sget v12, Lcom/vlingo/midas/R$id;->edit_shortcutname:I

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    iput-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditShortcutName:Landroid/widget/EditText;

    .line 60
    sget v12, Lcom/vlingo/midas/R$id;->btn_save:I

    invoke-virtual {p0, v12}, Lcom/samsung/wakeupsetting/NavigationSetting;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    iput-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;

    .line 61
    iget-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    new-instance v8, Ljava/util/Timer;

    invoke-direct {v8}, Ljava/util/Timer;-><init>()V

    .line 64
    .local v8, "timer":Ljava/util/Timer;
    new-instance v12, Lcom/samsung/wakeupsetting/NavigationSetting$1;

    invoke-direct {v12, p0}, Lcom/samsung/wakeupsetting/NavigationSetting$1;-><init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V

    const-wide/16 v13, 0x12c

    invoke-virtual {v8, v12, v13, v14}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 72
    iget-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditAddress:Landroid/widget/EditText;

    new-instance v13, Lcom/samsung/wakeupsetting/NavigationSetting$2;

    invoke-direct {v13, p0}, Lcom/samsung/wakeupsetting/NavigationSetting$2;-><init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V

    invoke-virtual {v12, v13}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 93
    iget-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mEditShortcutName:Landroid/widget/EditText;

    new-instance v13, Lcom/samsung/wakeupsetting/NavigationSetting$3;

    invoke-direct {v13, p0}, Lcom/samsung/wakeupsetting/NavigationSetting$3;-><init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V

    invoke-virtual {v12, v13}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 114
    iget-object v12, p0, Lcom/samsung/wakeupsetting/NavigationSetting;->mButtonSave:Landroid/widget/Button;

    new-instance v13, Lcom/samsung/wakeupsetting/NavigationSetting$4;

    invoke-direct {v13, p0}, Lcom/samsung/wakeupsetting/NavigationSetting$4;-><init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->getWindow()Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 132
    .local v4, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    const-string/jumbo v13, "privateFlags"

    invoke-virtual {v12, v13}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 133
    .local v6, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    const-string/jumbo v13, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v12, v13}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v7

    .line 134
    .local v7, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    const-string/jumbo v13, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v12, v13}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 136
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v11, 0x0

    .local v11, "valueofFlagsEnableStatusBar":I
    const/4 v10, 0x0

    .line 137
    .local v10, "valueofFlagsDisableTray":I
    invoke-virtual {v6, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 138
    invoke-virtual {v7, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v11

    .line 139
    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v10

    .line 141
    or-int/2addr v0, v11

    .line 142
    or-int/2addr v0, v10

    .line 144
    invoke-virtual {v6, v4, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->getWindow()Landroid/view/Window;

    move-result-object v12

    invoke-virtual {v12, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 154
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v4    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v6    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v7    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v10    # "valueofFlagsDisableTray":I
    .end local v11    # "valueofFlagsEnableStatusBar":I
    :goto_1
    return-void

    .line 47
    .end local v8    # "timer":Ljava/util/Timer;
    .end local v9    # "titlebar":Landroid/app/ActionBar;
    :catch_0
    move-exception v2

    .line 48
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 149
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v8    # "timer":Ljava/util/Timer;
    .restart local v9    # "titlebar":Landroid/app/ActionBar;
    :catch_1
    move-exception v2

    .line 152
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 146
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v12

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 158
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 163
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 160
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->setResult(I)V

    .line 161
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/NavigationSetting;->finish()V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
