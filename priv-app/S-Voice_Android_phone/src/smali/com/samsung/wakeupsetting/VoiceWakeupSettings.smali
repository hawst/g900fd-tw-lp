.class public Lcom/samsung/wakeupsetting/VoiceWakeupSettings;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "VoiceWakeupSettings.java"


# static fields
.field static LOG_TAG:Ljava/lang/String;

.field private static mTheme:I


# instance fields
.field private flag:Z

.field private preference:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string/jumbo v0, "VoiceWakeupSettings"

    sput-object v0, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->LOG_TAG:Ljava/lang/String;

    .line 22
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->flag:Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    new-instance v17, Lcom/vlingo/midas/ui/PackageInfoProvider;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    .line 31
    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    .line 32
    .local v11, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 33
    iget v9, v11, Landroid/util/DisplayMetrics;->density:F

    .line 34
    .local v9, "mDensity":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 35
    .local v6, "i":Landroid/content/Intent;
    const/4 v7, 0x0

    .line 36
    .local v7, "launchmode":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 37
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v17

    const-string/jumbo v18, "extra_launch_from"

    const-string/jumbo v19, "none"

    invoke-virtual/range {v17 .. v19}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 40
    :cond_0
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x3ff0000000000000L    # 1.0

    cmpl-double v17, v17, v19

    if-nez v17, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    iget v0, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    add-int v17, v17, v18

    const/16 v18, 0x7f0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v17, v0

    const v18, 0x4315d32c

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_1

    iget v0, v11, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v17, v0

    const v18, 0x431684be

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-eqz v17, :cond_2

    :cond_1
    iget v0, v11, Landroid/util/DisplayMetrics;->density:F

    move/from16 v17, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x3ff0000000000000L    # 1.0

    cmpl-double v17, v17, v19

    if-nez v17, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    iget v0, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    add-int v17, v17, v18

    const/16 v18, 0x820

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->xdpi:F

    move/from16 v17, v0

    const v18, 0x4315d2f2

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_7

    iget v0, v11, Landroid/util/DisplayMetrics;->ydpi:F

    move/from16 v17, v0

    const v18, 0x4316849c

    invoke-static/range {v17 .. v18}, Ljava/lang/Float;->compare(FF)I

    move-result v17

    if-nez v17, :cond_7

    .line 46
    :cond_2
    sget v17, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    .line 56
    :cond_3
    :goto_0
    sget v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->setTheme(I)V

    .line 59
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getWindow()Landroid/view/Window;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 60
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "privateFlags"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v12

    .line 61
    .local v12, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v13

    .line 62
    .local v13, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v17

    const-string/jumbo v18, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 63
    .local v3, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .local v2, "currentPrivateFlags":I
    const/16 v16, 0x0

    .local v16, "valueofFlagsEnableStatusBar":I
    const/4 v15, 0x0

    .line 64
    .local v15, "valueofFlagsDisableTray":I
    invoke-virtual {v12, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 65
    invoke-virtual {v13, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v16

    .line 66
    invoke-virtual {v3, v8}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v15

    .line 67
    or-int v2, v2, v16

    .line 68
    or-int/2addr v2, v15

    .line 69
    invoke-virtual {v12, v8, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getWindow()Landroid/view/Window;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 77
    .end local v2    # "currentPrivateFlags":I
    .end local v3    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v8    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v12    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v13    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v15    # "valueofFlagsDisableTray":I
    .end local v16    # "valueofFlagsEnableStatusBar":I
    :goto_1
    sget v17, Lcom/vlingo/midas/R$layout;->fragment_container:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->setContentView(I)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v14

    .line 80
    .local v14, "titlebar":Landroid/app/ActionBar;
    if-eqz v14, :cond_4

    .line 81
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v17

    if-eqz v17, :cond_9

    .line 82
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 83
    sget v17, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    :cond_4
    :goto_2
    if-eqz v6, :cond_5

    .line 99
    const-string/jumbo v17, "preference_clicked"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->preference:Ljava/lang/String;

    .line 100
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v10

    .line 101
    .local v10, "mFragmentTransaction":Landroid/app/FragmentTransaction;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->preference:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->preference:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string/jumbo v18, "secure_voice_wake_up"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 102
    sget v17, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    new-instance v18, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;-><init>()V

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 111
    :cond_6
    :goto_3
    invoke-virtual {v10}, Landroid/app/FragmentTransaction;->commit()I

    .line 112
    return-void

    .line 47
    .end local v10    # "mFragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v14    # "titlebar":Landroid/app/ActionBar;
    :cond_7
    const-string/jumbo v17, "carmode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 48
    sget v17, Lcom/vlingo/midas/R$style;->MidasBlackTheme:I

    sput v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    .line 49
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 50
    sget v17, Lcom/vlingo/midas/R$style;->MidasTheme:I

    sput v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    goto/16 :goto_0

    .line 53
    :cond_8
    sget v17, Lcom/vlingo/midas/R$style;->MidasTheme:I

    sput v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->mTheme:I

    goto/16 :goto_0

    .line 71
    :catch_0
    move-exception v4

    .line 72
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    sget-object v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->LOG_TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "NoSuchFieldException "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", continuing"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 73
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v4

    .line 74
    .local v4, "e":Ljava/lang/Exception;
    sget-object v17, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->LOG_TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "Exception "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", continuing"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 84
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v14    # "titlebar":Landroid/app/ActionBar;
    :cond_9
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 85
    const-string/jumbo v17, "carmode"

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 86
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v17

    if-nez v17, :cond_a

    .line 87
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    sget v18, Lcom/vlingo/midas/R$drawable;->tw_ab_transparent_dark_holo:I

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    :cond_a
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 91
    sget v17, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 93
    :cond_b
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 94
    sget v17, Lcom/vlingo/midas/R$string;->setting_wakeup_screenoff_title:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 104
    .restart local v10    # "mFragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_c
    new-instance v5, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-direct {v5}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;-><init>()V

    .line 105
    .local v5, "fragment":Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;
    sget v17, Lcom/vlingo/midas/R$id;->fragment_placeholder:I

    move/from16 v0, v17

    invoke-virtual {v10, v0, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 106
    if-eqz v7, :cond_6

    const-string/jumbo v17, "carmode"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 107
    sget v17, Lcom/vlingo/midas/R$string;->custom_wakeup_command:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->setmode(Z)V

    goto/16 :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 120
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 118
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/VoiceWakeupSettings;->finish()V

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
