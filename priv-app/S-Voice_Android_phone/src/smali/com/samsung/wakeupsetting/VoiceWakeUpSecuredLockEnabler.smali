.class public final Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;
.super Ljava/lang/Object;
.source "VoiceWakeUpSecuredLockEnabler.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler$VoiceWakeUpUIChangeListener;
    }
.end annotation


# static fields
.field public static LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

.field private mSwitch:Landroid/widget/Switch;

.field private mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;

.field pref:Landroid/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string/jumbo v0, "VoiceWakeUpSecuredLockEnabler"

    sput-object v0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "switch_"    # Landroid/widget/Switch;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    .line 24
    new-instance v0, Lcom/vlingo/midas/ui/PackageInfoProvider;

    invoke-direct {v0, p1}, Lcom/vlingo/midas/ui/PackageInfoProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mPackageInfo:Lcom/vlingo/midas/ui/PackageInfoProvider;

    .line 25
    return-void
.end method


# virtual methods
.method public dismissDialog()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "turnOn"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 61
    if-eqz p2, :cond_1

    .line 62
    const-string/jumbo v0, "wakeupSecured"

    invoke-static {v0, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 63
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 64
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pref:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pref:Landroid/preference/Preference;

    sget v1, Lcom/vlingo/midas/R$string;->on:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 66
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->updateUIVoiceWakeupSettings(Z)V

    .line 74
    :goto_0
    return-void

    .line 68
    :cond_1
    const-string/jumbo v0, "wakeupSecured"

    invoke-static {v0, v2}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 69
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 70
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pref:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pref:Landroid/preference/Preference;

    sget v1, Lcom/vlingo/midas/R$string;->off:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 72
    :cond_2
    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->updateUIVoiceWakeupSettings(Z)V

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 40
    const-string/jumbo v0, "secure_voice_wake_up"

    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 41
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 42
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 28
    sget-object v0, Lcom/vlingo/midas/settings/SettingsScreen;->wakeupSecured:Landroid/preference/Preference;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->pref:Landroid/preference/Preference;

    .line 29
    const-string/jumbo v0, "wakeupSecured"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 31
    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->updateUIVoiceWakeupSettings(Z)V

    .line 36
    :goto_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 37
    return-void

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 34
    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->updateUIVoiceWakeupSettings(Z)V

    goto :goto_0
.end method

.method setVoiceWakeUpUIChangeListener(Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;)V
    .locals 0
    .param p1, "mlistener"    # Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;

    .line 53
    return-void
.end method

.method public updateUIVoiceWakeupSettings(Z)V
    .locals 1
    .param p1, "turnOn"    # Z

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpSecuredLockEnabler;->mUiChangeListener:Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;

    invoke-virtual {v0, p1}, Lcom/samsung/wakeupsetting/VoiceWakeupSecuredLockFragment;->updateUIVoiceWakeupSettings(Z)V

    .line 57
    return-void
.end method
