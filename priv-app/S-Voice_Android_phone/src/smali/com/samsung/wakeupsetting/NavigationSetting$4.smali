.class Lcom/samsung/wakeupsetting/NavigationSetting$4;
.super Ljava/lang/Object;
.source "NavigationSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/NavigationSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/NavigationSetting;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/NavigationSetting;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$300(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$300(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "address"

    iget-object v2, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mEditAddress:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$000(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$300(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "name"

    iget-object v2, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->mEditShortcutName:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$100(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    # getter for: Lcom/samsung/wakeupsetting/NavigationSetting;->intent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/wakeupsetting/NavigationSetting;->access$300(Lcom/samsung/wakeupsetting/NavigationSetting;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/wakeupsetting/NavigationSetting;->setResult(ILandroid/content/Intent;)V

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/NavigationSetting;->finish()V

    .line 126
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/NavigationSetting$4;->this$0:Lcom/samsung/wakeupsetting/NavigationSetting;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/NavigationSetting;->setResult(I)V

    goto :goto_0
.end method
