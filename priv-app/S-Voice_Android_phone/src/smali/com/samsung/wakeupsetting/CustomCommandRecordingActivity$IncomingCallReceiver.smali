.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CustomCommandRecordingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IncomingCallReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1873
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p2, "x1"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;

    .prologue
    .line 1873
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1876
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/vlingo/midas/gui/customviews/ListeningView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1877
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/vlingo/midas/gui/customviews/ListeningView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1879
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mBtNeeded:Z
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1880
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 1882
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-ne v0, v1, :cond_4

    .line 1883
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1884
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1885
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1887
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1895
    :cond_3
    :goto_0
    return-void

    .line 1888
    :cond_4
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    move-result-object v0

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v0, v1, :cond_3

    .line 1889
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1890
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1891
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1893
    :cond_5
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    goto :goto_0
.end method
