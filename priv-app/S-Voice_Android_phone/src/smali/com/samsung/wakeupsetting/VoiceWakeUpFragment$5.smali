.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$5;
.super Ljava/lang/Object;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->showWakeupInSecuredLockDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$5;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    .line 467
    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 468
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 469
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$5;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 470
    const/4 v0, 0x1

    .line 472
    :cond_0
    return v0
.end method
