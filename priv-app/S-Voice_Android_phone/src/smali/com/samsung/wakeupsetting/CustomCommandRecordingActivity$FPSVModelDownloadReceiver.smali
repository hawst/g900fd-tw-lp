.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CustomCommandRecordingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FPSVModelDownloadReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1899
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p2, "x1"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;

    .prologue
    .line 1899
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "paramIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1901
    const-string/jumbo v1, "result"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1903
    :try_start_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isLanguageUseFPSV()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1904
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v3, "path"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetResourceInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 1907
    :cond_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetWakeUp(IIS)V

    .line 1908
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1916
    :goto_1
    return-void

    .line 1904
    :cond_2
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2600(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1910
    :catch_0
    move-exception v0

    .line 1911
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    .line 1914
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_3
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v2

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v3

    invoke-virtual {v1, v2, v5, v3}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetWakeUp(IIS)V

    .line 1915
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setClickable(Z)V

    goto :goto_1
.end method
