.class public Lcom/samsung/wakeupsetting/CustomWaveLayout;
.super Landroid/widget/RelativeLayout;
.source "CustomWaveLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;
    }
.end annotation


# static fields
.field private static listener:Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;


# instance fields
.field private context:Landroid/content/Context;

.field private doneBtn:Lcom/vlingo/midas/gui/customviews/Button;

.field private mTopMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->context:Landroid/content/Context;

    .line 47
    const-string/jumbo v0, "com.vlingo.midas"

    const-string/jumbo v1, "WKCS"

    invoke-static {p1, v0, v1}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 98
    .local v0, "id":I
    sget v1, Lcom/vlingo/midas/R$id;->done_button:I

    if-ne v0, v1, :cond_0

    .line 99
    sget-object v1, Lcom/samsung/wakeupsetting/CustomWaveLayout;->listener:Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;

    if-eqz v1, :cond_0

    .line 101
    sget-object v1, Lcom/samsung/wakeupsetting/CustomWaveLayout;->listener:Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;

    invoke-interface {v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;->OnCustomWakeUpRecordEndDoneButtonClicked()V

    .line 105
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/vlingo/midas/R$id;->wake_up_count:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/vlingo/midas/R$id;->done_button:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vlingo/midas/gui/customviews/Button;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->doneBtn:Lcom/vlingo/midas/gui/customviews/Button;

    .line 55
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->doneBtn:Lcom/vlingo/midas/gui/customviews/Button;

    invoke-virtual {v0, p0}, Lcom/vlingo/midas/gui/customviews/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method public setErrorMessage(I)V
    .locals 3
    .param p1, "res"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 68
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 69
    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 62
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 63
    return-void
.end method

.method public setOnOnCustonWakeUpRecordEndListener(Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;

    .prologue
    .line 42
    sput-object p1, Lcom/samsung/wakeupsetting/CustomWaveLayout;->listener:Lcom/samsung/wakeupsetting/CustomWaveLayout$OnCustonWakeUpRecordEndListener;

    .line 43
    return-void
.end method

.method public setSuccessCount(IZ)V
    .locals 4
    .param p1, "count"    # I
    .param p2, "isFromGSA"    # Z

    .prologue
    .line 89
    if-eqz p2, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->custom_recording_current_status_for_gsa:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "format":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWaveLayout;->mTopMessage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    return-void

    .line 92
    .end local v0    # "format":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_speaknow:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "format":Ljava/lang/String;
    goto :goto_0
.end method
