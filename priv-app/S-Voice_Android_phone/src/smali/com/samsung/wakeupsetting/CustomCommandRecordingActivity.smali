.class public Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Lcom/samsung/voiceshell/VoiceEngineResultListener;
.implements Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$14;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;,
        Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    }
.end annotation


# static fields
.field public static final LOCKSCREEN_BIOMETRIC_WEAK_FALLBACK:Ljava/lang/String; = "lockscreen.biometric_weak_fallback"

.field private static final SUPPORTED_LANGUAGE_FOR_FPSV:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "CustomCommandRecordingActivity"

.field private static final WAKE_UP_TYPE_FACE_AND_VOICE:I = 0x0

.field private static final WAKE_UP_TYPE_FINGER_AND_VOICE:I = 0x3

.field private static final WAKE_UP_TYPE_GSA:I = 0x4

.field private static final WAKE_UP_TYPE_LOCKSCREEN:I = 0x1

.field private static final WAKE_UP_TYPE_SVOICE:I = 0x2

.field private static adaptationCount:I = 0x0

.field private static enroll_count:I = 0x0

.field private static mTheme:I = 0x0

.field public static m_UDTSIDlastEnroll:[Ljava/lang/String; = null

.field private static m_strWakeupData:[Ljava/lang/String; = null

.field private static final systemPath:Ljava/lang/String; = "/system/wakeupdata/sensory/"


# instance fields
.field private final ENROLL_CNT:I

.field private final ENROLL_CNT_NEW:I

.field TTSHandler:Landroid/os/Handler;

.field private adaptButton:Landroid/widget/Button;

.field private audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

.field private audioManager:Landroid/media/AudioManager;

.field private beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field private commandType:S

.field private controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

.field private count:I

.field private currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

.field private customCommandHintLayout:Landroid/widget/LinearLayout;

.field private customCommandHintTextView:Landroid/widget/TextView;

.field private doneButton:Landroid/widget/Button;

.field private doneButtonClicked:Z

.field private endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

.field private errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

.field private helpContainer:Landroid/view/View;

.field private helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

.field private helpLeftFragment:Lcom/vlingo/midas/gui/HelpFragment;

.field private help_layout:Landroid/widget/RelativeLayout;

.field private helpwakeup:Z

.field private inIUXMode:Z

.field private isAdaptVoice:Z

.field private isClickedDownload:Z

.field private isFromAlwaysOnSettings:Z

.field private isFromGSA:Z

.field private isFromSettings:Z

.field private isMicFocused:Z

.field private isPhoneInUse:Z

.field private isScreenOn:Z

.field public isSensoryUDTSIDsoFileExist:Z

.field public isShowingDialog:Z

.field private isTryAgain:Z

.field private landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

.field private lastEnrollCount:I

.field protected layoutMenuBtn:Landroid/widget/LinearLayout;

.field protected layoutMicBtnKitkat:Landroid/widget/LinearLayout;

.field private mBtNeeded:Z

.field private mContext:Landroid/content/Context;

.field private mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

.field private mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

.field private final mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

.field private mInitbody:Landroid/widget/ImageView;

.field private mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

.field private mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

.field private mRelativeLayoutMain:Landroid/widget/RelativeLayout;

.field private mRmsBefore:I

.field private mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

.field private mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

.field private mTts:Landroid/speech/tts/TextToSpeech;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeUpType:S

.field protected micStatusText:Landroid/widget/TextView;

.field private orientation:I

.field private recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

.field private reco_idleBtn:Landroid/widget/ImageView;

.field private reco_idleMic:Landroid/widget/ImageView;

.field private reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

.field private reco_listeningMic:Landroid/widget/ImageView;

.field private reco_thinkingBtn:Landroid/widget/ImageView;

.field private regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

.field private retryButton:Landroid/widget/Button;

.field private screenDensityDPI:F

.field private screenOnOffReceiver:Landroid/content/BroadcastReceiver;

.field private scrollView:Landroid/widget/ScrollView;

.field private final sensoryUDTSIDSoFilePath:Ljava/lang/String;

.field private soundPool:Landroid/media/SoundPool;

.field private soundStart:I

.field private timesView:Landroid/widget/TextView;

.field private titleView:Landroid/widget/TextView;

.field private waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

.field private waveView:Lcom/samsung/wakeupsetting/WaveView;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    sput v3, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 155
    sput v3, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 184
    sget v0, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTheme:I

    .line 196
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, ""

    aput-object v1, v0, v4

    const-string/jumbo v1, ""

    aput-object v1, v0, v5

    const-string/jumbo v1, ""

    aput-object v1, v0, v6

    const-string/jumbo v1, ""

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, ""

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;

    .line 198
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user0_3.wav"

    aput-object v1, v0, v3

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user1_3.wav"

    aput-object v1, v0, v4

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user2_3.wav"

    aput-object v1, v0, v5

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user3_3.wav"

    aput-object v1, v0, v6

    const-string/jumbo v1, "/data/data/com.vlingo.midas/user4_3.wav"

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_UDTSIDlastEnroll:[Ljava/lang/String;

    .line 205
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "en_gb"

    aput-object v1, v0, v3

    const-string/jumbo v1, "de_de"

    aput-object v1, v0, v4

    const-string/jumbo v1, "es_es"

    aput-object v1, v0, v5

    const-string/jumbo v1, "es_us"

    aput-object v1, v0, v6

    const-string/jumbo v1, "pt_br"

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SUPPORTED_LANGUAGE_FOR_FPSV:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 105
    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 137
    iput-short v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    .line 138
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->ENROLL_CNT:I

    .line 139
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->ENROLL_CNT_NEW:I

    .line 141
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mBtNeeded:Z

    .line 142
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->inIUXMode:Z

    .line 143
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    .line 144
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z

    .line 149
    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 150
    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 151
    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    .line 160
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isShowingDialog:Z

    .line 161
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    .line 162
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    .line 165
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isAdaptVoice:Z

    .line 166
    const/4 v0, 0x1

    iput-short v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    .line 175
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 176
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 179
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    .line 188
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isMicFocused:Z

    .line 189
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromSettings:Z

    .line 190
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromAlwaysOnSettings:Z

    .line 191
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    .line 192
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButtonClicked:Z

    .line 207
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isSensoryUDTSIDsoFileExist:Z

    .line 208
    const-string/jumbo v0, "/system/lib/libSensoryUDTSIDEngine.so"

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->sensoryUDTSIDSoFilePath:Ljava/lang/String;

    .line 215
    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    .line 401
    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$3;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->TTSHandler:Landroid/os/Handler;

    .line 1332
    iput v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->count:I

    .line 1364
    iput v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRmsBefore:I

    .line 2611
    return-void
.end method

.method private SetWakeupData()V
    .locals 9

    .prologue
    .line 1834
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1835
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-nez v3, :cond_1

    .line 1836
    const-string/jumbo v6, "CustomCommandRecordingActivity"

    const-string/jumbo v7, "SetWakeupData, PackaManger is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1853
    :cond_0
    :goto_0
    return-void

    .line 1841
    :cond_1
    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "com.vlingo.midas"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1842
    .local v4, "strPackagepath":Ljava/lang/String;
    sget-object v6, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;

    array-length v2, v6

    .line 1844
    .local v2, "nLen":I
    const/4 v6, 0x6

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "kwd"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "enSTR"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "enSTR4"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "modelStatus"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "lastEnrollUtt"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string/jumbo v7, "realLastEnrollUtt"

    aput-object v7, v5, v6

    .line 1846
    .local v5, "strWakeupData":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1847
    sget-object v6, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v5, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 1848
    const-string/jumbo v6, "CustomCommandRecordingActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "SetWakeupData, wakeup data : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;

    aget-object v8, v8, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1846
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1850
    .end local v1    # "i":I
    .end local v2    # "nLen":I
    .end local v4    # "strPackagepath":Ljava/lang/String;
    .end local v5    # "strWakeupData":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1851
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/speech/tts/TextToSpeech;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    .param p2, "x2"    # Z

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isMicFocused:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300()I
    .locals 1

    .prologue
    .line 89
    sget v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    return v0
.end method

.method static synthetic access$1302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 89
    sput p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    return p0
.end method

.method static synthetic access$1400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomWaveLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-short v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-short v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;SS)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # S
    .param p2, "x2"    # S

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startEnroll(SS)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    return-object v0
.end method

.method static synthetic access$2000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->m_strWakeupData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButtonClicked:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/vlingo/midas/gui/customviews/ListeningView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2702(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V

    return-void
.end method

.method static synthetic access$2900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mBtNeeded:Z

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 3

    .prologue
    .line 1806
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 1808
    const-string/jumbo v1, "power"

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1809
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string/jumbo v2, "CstmCmd_ScrnKpr"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1813
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1814
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1816
    :cond_1
    return-void
.end method

.method private checkVisibility()I
    .locals 2

    .prologue
    .line 394
    sget v1, Lcom/vlingo/midas/R$id;->wave:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 395
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 398
    :goto_0
    return v1

    :cond_0
    const/16 v1, 0xa

    goto :goto_0
.end method

.method private delegateViewForTalkBack(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 705
    if-eqz p1, :cond_0

    .line 706
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 708
    :cond_0
    return-void
.end method

.method private displayRecordingEndPopup()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1528
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    if-nez v1, :cond_1

    .line 1737
    :cond_0
    :goto_0
    return-void

    .line 1532
    :cond_1
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    invoke-virtual {v1, v3}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setVisibility(I)V

    .line 1533
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    invoke-virtual {v1, v4}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;->setVisibility(I)V

    .line 1534
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    invoke-virtual {v1, v4}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->setVisibility(I)V

    .line 1548
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->success_image:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1549
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->button_done:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1551
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->wake_up_count:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1581
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v1, :cond_2

    .line 1582
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->perfect_second:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1583
    .local v0, "t":Landroid/widget/TextView;
    sget v1, Lcom/vlingo/midas/R$string;->custom_recording_success_desc_for_gsa:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1586
    .end local v0    # "t":Landroid/widget/TextView;
    :cond_2
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->done_button:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButton:Landroid/widget/Button;

    .line 1587
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    .line 1591
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;

    invoke-direct {v2, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$9;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1715
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButton:Landroid/widget/Button;

    sget v2, Lcom/vlingo/midas/R$string;->done:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 1716
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v2, Lcom/vlingo/midas/R$id;->adapt_button:I

    invoke-virtual {v1, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptButton:Landroid/widget/Button;

    .line 1717
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;

    invoke-direct {v2, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$10;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732
    iget-short v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    if-nez v1, :cond_3

    sget v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_3

    .line 1733
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptButton:Landroid/widget/Button;

    sget v2, Lcom/vlingo/midas/R$string;->adapt_voice:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 1735
    :cond_3
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptButton:Landroid/widget/Button;

    sget v2, Lcom/vlingo/midas/R$string;->try_again:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0
.end method

.method private displayRecordingErrorPopup()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1488
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    if-nez v0, :cond_1

    .line 1510
    :cond_0
    :goto_0
    return-void

    .line 1492
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1493
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1495
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setVisibility(I)V

    .line 1496
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;->setVisibility(I)V

    .line 1497
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->setVisibility(I)V

    .line 1501
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    sget v1, Lcom/vlingo/midas/R$id;->retry_button:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->retryButton:Landroid/widget/Button;

    .line 1502
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->retryButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 1503
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->retryButton:Landroid/widget/Button;

    new-instance v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;

    invoke-direct {v1, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$8;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private displayRecordingPopup(Z)V
    .locals 4
    .param p1, "adaptButtonPressed"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1471
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isAdaptVoice:Z

    .line 1472
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    if-eqz v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    invoke-virtual {v0, v3}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setVisibility(I)V

    .line 1475
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$id;->wake_up_count:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1476
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$id;->success_image:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1477
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$id;->button_done:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1481
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    if-eqz v0, :cond_1

    .line 1482
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;->setVisibility(I)V

    .line 1483
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    if-eqz v0, :cond_2

    .line 1484
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    invoke-virtual {v0, v2}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->setVisibility(I)V

    .line 1485
    :cond_2
    return-void
.end method

.method private forceSystemLocale()V
    .locals 3

    .prologue
    .line 2117
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 2118
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v1

    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2119
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 2120
    return-void
.end method

.method private getCurrentSystemLanguage()Ljava/util/Locale;
    .locals 9

    .prologue
    .line 2131
    :try_start_0
    const-string/jumbo v7, "android.app.ActivityManagerNative"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 2132
    .local v1, "amnClass":Ljava/lang/Class;
    const/4 v0, 0x0

    .line 2133
    .local v0, "amn":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 2135
    .local v2, "config":Landroid/content/res/Configuration;
    const-string/jumbo v7, "getDefault"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 2136
    .local v6, "methodGetDefault":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2137
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2139
    const-string/jumbo v7, "getConfiguration"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v1, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 2140
    .local v5, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2141
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "config":Landroid/content/res/Configuration;
    check-cast v2, Landroid/content/res/Configuration;

    .line 2142
    .restart local v2    # "config":Landroid/content/res/Configuration;
    iget-object v3, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148
    .end local v0    # "amn":Ljava/lang/Object;
    .end local v1    # "amnClass":Ljava/lang/Class;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetDefault":Ljava/lang/reflect/Method;
    .local v3, "currentLocale":Ljava/util/Locale;
    :goto_0
    return-object v3

    .line 2143
    .end local v3    # "currentLocale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 2144
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 2145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .restart local v3    # "currentLocale":Ljava/util/Locale;
    goto :goto_0
.end method

.method private getGSALanguageString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2105
    const-string/jumbo v1, "current_gsa_language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2107
    .local v0, "languageString":Ljava/lang/String;
    const-string/jumbo v1, "-"

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2108
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 2110
    return-object v0
.end method

.method private getLanguageString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2087
    const-string/jumbo v1, "language"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2089
    .local v0, "languageString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2090
    const-string/jumbo v1, "CustomCommandRecordingActivity"

    const-string/jumbo v2, "Entering the null string"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2091
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->languageSupportListCheck()V

    .line 2092
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2095
    :cond_0
    const-string/jumbo v1, "-"

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2096
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 2098
    const-string/jumbo v1, "v_es_la"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2099
    const-string/jumbo v0, "es_us"

    .line 2101
    :cond_1
    return-object v0
.end method

.method private initialize()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x1

    .line 543
    iget-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v4, :cond_0

    .line 544
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->forceSystemLocale()V

    .line 547
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v4, v4

    iput v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenDensityDPI:F

    .line 548
    sget v4, Lcom/vlingo/midas/R$id;->titleCustomCommand:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    .line 550
    sget v4, Lcom/vlingo/midas/R$id;->wake_up_setting_tv:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    .line 551
    sget v4, Lcom/vlingo/midas/R$id;->CustomCommandHint:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintTextView:Landroid/widget/TextView;

    .line 552
    sget v4, Lcom/vlingo/midas/R$id;->customCommandHintLayout:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    .line 553
    sget v4, Lcom/vlingo/midas/R$id;->custom_wakeup_command:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRelativeLayoutMain:Landroid/widget/RelativeLayout;

    .line 554
    sget v4, Lcom/vlingo/midas/R$id;->initbody:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mInitbody:Landroid/widget/ImageView;

    .line 555
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->regular_control_view:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/RegularControlFragment;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    .line 557
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->land_control_view:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    .line 561
    new-instance v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;

    invoke-direct {v4, p0, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$IncomingCallReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 562
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 563
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 564
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 565
    const-string/jumbo v4, "CustomCommandRecordingActivity"

    const-string/jumbo v5, "registerIncomingCallRx"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    new-instance v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;

    invoke-direct {v4, p0, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$FPSVModelDownloadReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

    .line 568
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 569
    .local v2, "localIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v4, "com.sec.android.voiceservice.action.FPSVModelDownloadResult"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 570
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 572
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 573
    const-string/jumbo v4, "language"

    const-string/jumbo v5, "en-US"

    invoke-static {v4, v5}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 575
    .local v1, "language":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-nez v4, :cond_1

    const-string/jumbo v4, "en-US"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    iget-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getCurrentSystemLanguage()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 577
    :cond_2
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mInitbody:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$drawable;->voice_talk_setting_inches_02:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 580
    :cond_3
    sget v4, Lcom/vlingo/midas/R$id;->mic_btn_layout_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    .line 581
    sget v4, Lcom/vlingo/midas/R$id;->mic_status_txt:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->micStatusText:Landroid/widget/TextView;

    .line 582
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->micStatusText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 583
    sget v4, Lcom/vlingo/midas/R$id;->menu_layout_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMenuBtn:Landroid/widget/LinearLayout;

    .line 584
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMenuBtn:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 585
    sget v4, Lcom/vlingo/midas/R$id;->btn_toggle:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 586
    .local v3, "toggleButton":Landroid/widget/ImageButton;
    if-eqz v3, :cond_4

    .line 587
    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 592
    :cond_4
    iget v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->orientation:I

    if-ne v4, v7, :cond_a

    .line 593
    sget v4, Lcom/vlingo/midas/R$id;->btn_listening_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 594
    sget v4, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 595
    sget v4, Lcom/vlingo/midas/R$id;->btn_idle_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    .line 596
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 598
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    .line 609
    :goto_0
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-nez v4, :cond_5

    .line 610
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 611
    sget v5, Lcom/vlingo/midas/R$layout;->mic_animation_view:I

    sget v4, Lcom/vlingo/midas/R$id;->normal_layout_custom:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-static {p0, v5, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 615
    :goto_1
    sget v4, Lcom/vlingo/midas/R$id;->mic_animation_container:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/customviews/ListeningView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 616
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 617
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setItsPosition()V

    .line 620
    :cond_5
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRelativeLayoutMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setClipChildren(Z)V

    .line 621
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ControlFragment;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 622
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ControlFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->btn_listening_mic_kitkat:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningMic:Landroid/widget/ImageView;

    .line 623
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ControlFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/vlingo/midas/R$id;->mic_idle_kitkat:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleMic:Landroid/widget/ImageView;

    .line 624
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v4}, Lcom/vlingo/midas/gui/ControlFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    .line 627
    .end local v1    # "language":Ljava/lang/String;
    .end local v3    # "toggleButton":Landroid/widget/ImageButton;
    :cond_6
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 628
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setHelpFragmentTab()V

    .line 630
    :cond_7
    new-instance v4, Landroid/media/SoundPool;

    const/4 v5, 0x3

    invoke-direct {v4, v7, v5, v9}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    .line 631
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/vlingo/midas/R$raw;->start_tone:I

    invoke-virtual {v4, v5, v6, v7}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v4

    iput v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundStart:I

    .line 638
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    if-nez v4, :cond_8

    .line 639
    new-instance v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$4;

    invoke-direct {v4, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$4;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 657
    :cond_8
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    invoke-static {v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 659
    sget v4, Lcom/vlingo/midas/R$id;->wakeup_wave:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWaveLayout;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    .line 660
    sget v4, Lcom/vlingo/midas/R$id;->end_body:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    .line 661
    sget v4, Lcom/vlingo/midas/R$id;->error_body:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->errorBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordError;

    .line 664
    iget-boolean v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v4, :cond_9

    .line 665
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintTextView:Landroid/widget/TextView;

    sget v5, Lcom/vlingo/midas/R$string;->setting_wakeup_idle_hint_for_gsa:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 670
    :cond_9
    sget v4, Lcom/vlingo/midas/R$id;->recordScrollView:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->scrollView:Landroid/widget/ScrollView;

    .line 671
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    new-instance v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$5;

    invoke-direct {v5, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$5;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 679
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeMicTouchTalkBack()V

    .line 680
    return-void

    .line 600
    .restart local v1    # "language":Ljava/lang/String;
    .restart local v3    # "toggleButton":Landroid/widget/ImageButton;
    :cond_a
    sget v4, Lcom/vlingo/midas/R$id;->btn_listening_kitkat_land:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vlingo/midas/gui/customviews/MicViewKK;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    .line 601
    sget v4, Lcom/vlingo/midas/R$id;->btn_thinking_kitkat:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_thinkingBtn:Landroid/widget/ImageView;

    .line 602
    sget v4, Lcom/vlingo/midas/R$id;->btn_idle_kitkat_land:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    .line 603
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 605
    iget-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    iput-object v4, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    goto/16 :goto_0

    .line 613
    :cond_b
    sget v4, Lcom/vlingo/midas/R$layout;->mic_animation_view:I

    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRelativeLayoutMain:Landroid/widget/RelativeLayout;

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_1
.end method

.method private isThisNOTE10()Z
    .locals 3

    .prologue
    .line 798
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 799
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    const/16 v2, 0x500

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    const/16 v2, 0x2f0

    if-ne v1, v2, :cond_0

    .line 800
    const/4 v1, 0x1

    .line 802
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private playNotifySound()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 2162
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundStart:I

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 2165
    const-wide/16 v0, 0xd7

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 2167
    const-string/jumbo v0, "CustomCommandRecordingActivity"

    const-string/jumbo v1, "soundDuration 215"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2171
    :goto_0
    return-void

    .line 2168
    :catch_0
    move-exception v7

    .line 2169
    .local v7, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 1819
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1820
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1821
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1823
    :cond_0
    return-void
.end method

.method private removeDelegateViewForTalkBack(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 696
    if-eqz p1, :cond_0

    .line 697
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 698
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 699
    invoke-virtual {p1, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 701
    :cond_0
    return-void
.end method

.method private removeMicTouchTalkBack()V
    .locals 1

    .prologue
    .line 684
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 686
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 687
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 688
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 689
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 690
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 692
    :cond_0
    return-void
.end method

.method private setButtonIdle()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1401
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->setVisibility(I)V

    .line 1402
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_thinkingBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_thinkingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1405
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1406
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1407
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1409
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningBtn:Lcom/vlingo/midas/gui/customviews/MicViewBase;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/customviews/MicViewBase;->resetListeningAnimation()V

    .line 1410
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1411
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_2

    .line 1412
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setAlpha(F)V

    .line 1414
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_listeningMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1416
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleMic:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1418
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1419
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleMic:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->s_voice_mic:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1420
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->s_voice_standby_bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1423
    :cond_3
    return-void
.end method

.method private setClickHandlers()V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;

    invoke-direct {v1, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$7;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 748
    return-void
.end method

.method private setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 711
    new-instance v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$6;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    return-object v0
.end method

.method private setHelpFragmentTab()V
    .locals 12

    .prologue
    const/4 v11, -0x2

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 497
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->help_layout:Landroid/widget/RelativeLayout;

    if-nez v7, :cond_0

    .line 498
    sget v8, Lcom/vlingo/midas/R$layout;->help_list_view:I

    sget v7, Lcom/vlingo/midas/R$id;->custom_wakeup_command:I

    invoke-virtual {p0, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-static {p0, v8, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 499
    sget v7, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {p0, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->help_layout:Landroid/widget/RelativeLayout;

    .line 501
    :cond_0
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpContainer:Landroid/view/View;

    if-nez v7, :cond_1

    .line 502
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->help_layout:Landroid/widget/RelativeLayout;

    sget v8, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpContainer:Landroid/view/View;

    .line 504
    :cond_1
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    if-nez v7, :cond_2

    .line 507
    new-instance v7, Lcom/vlingo/midas/gui/HelpFragment;

    invoke-direct {v7}, Lcom/vlingo/midas/gui/HelpFragment;-><init>()V

    iput-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    .line 508
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    .line 509
    .local v5, "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v7, Lcom/vlingo/midas/R$id;->help_layout:I

    iget-object v8, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpFragment:Lcom/vlingo/midas/gui/HelpFragment;

    invoke-virtual {v5, v7, v8}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 510
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 512
    .end local v5    # "mTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$dimen;->helpfragment_width:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v3, v7

    .line 513
    .local v3, "helpFragmentWidth":I
    sget v7, Lcom/vlingo/midas/R$id;->custom_command_rl_ll:I

    invoke-virtual {p0, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 514
    .local v2, "customCommandView":Landroid/view/View;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v3, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 516
    .local v4, "helpParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xb

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 517
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpContainer:Landroid/view/View;

    invoke-virtual {v7, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 518
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v9, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 520
    .local v6, "normalLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 521
    sget v7, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v6, v10, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 522
    invoke-virtual {v2, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 527
    sget v7, Lcom/vlingo/midas/R$id;->normal_layout_custom:I

    invoke-virtual {p0, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 528
    .local v1, "controlViewContainer":Landroid/view/View;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v9, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 530
    .local v0, "controlParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 531
    const/16 v7, 0xe

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 532
    sget v7, Lcom/vlingo/midas/R$id;->help_layout:I

    invoke-virtual {v0, v10, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 533
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 534
    iget v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->orientation:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/vlingo/midas/R$bool;->help_fragment_supported:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 536
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->help_layout:Landroid/widget/RelativeLayout;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 539
    :goto_0
    return-void

    .line 538
    :cond_3
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->help_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setOrientationChangeUI()V
    .locals 2

    .prologue
    .line 788
    iget v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 789
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 795
    :goto_0
    return-void

    .line 792
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->landRegularControlFragment:Lcom/vlingo/midas/gui/LandRegularControlFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->regularControlFragment:Lcom/vlingo/midas/gui/RegularControlFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method private setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V
    .locals 9
    .param p1, "state"    # Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;
    .param p2, "orientationChange"    # Z

    .prologue
    const/4 v8, 0x3

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 807
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 808
    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$14;->$SwitchMap$com$samsung$wakeupsetting$CustomCommandRecordingActivity$AppState:[I

    invoke-virtual {p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1019
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 1020
    return-void

    .line 810
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-nez v2, :cond_1

    .line 811
    new-instance v2, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-direct {v2, p0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;-><init>(Lcom/samsung/voiceshell/VoiceEngineResultListener;)V

    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    .line 812
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->init()I

    .line 814
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 816
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 819
    :cond_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_3

    .line 820
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 822
    :cond_3
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopTTS()V

    .line 825
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 826
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 827
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 832
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v2, :cond_6

    .line 833
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->custom_recording_desc_before_tap_mic_for_gsa:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 839
    :goto_1
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 843
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/WaveView;->reset()V

    .line 846
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    if-eqz v2, :cond_4

    .line 847
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    invoke-virtual {v2, v6}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setVisibility(I)V

    .line 849
    :cond_4
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    if-eqz v2, :cond_5

    .line 850
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->endBody:Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;

    invoke-virtual {v2, v6}, Lcom/samsung/wakeupsetting/CustomWakeUpRecordEnd;->setVisibility(I)V

    .line 851
    :cond_5
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 852
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setButtonIdle()V

    goto :goto_0

    .line 835
    :cond_6
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_TAP_BUTTON_BELOW:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 856
    :pswitch_1
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v2, :cond_7

    .line 857
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->forceSystemLocale()V

    .line 867
    :cond_7
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_8

    .line 868
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 870
    :cond_8
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 871
    .local v1, "teleManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-eqz v2, :cond_a

    .line 872
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v2, :cond_9

    .line 873
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 874
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 876
    :cond_9
    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->MIC_BUSY:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-direct {p0, v2, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 905
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 906
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto/16 :goto_0

    .line 878
    :cond_a
    if-nez p2, :cond_b

    .line 879
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->acquireWakeLock()V

    .line 880
    iput-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    .line 881
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopTTS()V

    .line 882
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2, v8, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 885
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 890
    :cond_b
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    if-eqz v2, :cond_c

    .line 891
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "4 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_times:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 893
    :cond_c
    invoke-direct {p0, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingPopup(Z)V

    .line 894
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 895
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 902
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    invoke-virtual {v2}, Lcom/samsung/wakeupsetting/WaveView;->start()V

    goto :goto_2

    .line 911
    .end local v1    # "teleManager":Landroid/telephony/TelephonyManager;
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 912
    iget-short v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    if-nez v2, :cond_12

    .line 913
    sget v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_e

    .line 914
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_LOCK_ADAPT:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 915
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v2, v3, :cond_d

    .line 916
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_LOCK_ADAPT:I

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    .line 917
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_LOCK_ADAPT:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 959
    :cond_d
    :goto_3
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 962
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingEndPopup()V

    goto/16 :goto_0

    .line 920
    :cond_e
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_f

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT:I

    :goto_4
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 922
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v6}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 923
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 924
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v2, v3, :cond_d

    .line 925
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_10

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT:I

    :goto_5
    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    .line 927
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_11

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT:I

    :goto_6
    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_3

    .line 920
    :cond_f
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT_US:I

    goto :goto_4

    .line 925
    :cond_10
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT_US:I

    goto :goto_5

    .line 927
    :cond_11
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_PARTIAL_SUCCESS_LOCK_ADAPT_US:I

    goto :goto_6

    .line 932
    :cond_12
    iget-short v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    if-ne v2, v7, :cond_15

    .line 934
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v2, :cond_13

    .line 935
    sget v2, Lcom/vlingo/midas/R$string;->custom_recording_success_desc_for_gsa:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 939
    .local v0, "successStr":Ljava/lang/String;
    :goto_7
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 940
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v2, v3, :cond_d

    .line 941
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS:I

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    .line 942
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v2, :cond_14

    .line 943
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    invoke-virtual {v2, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->speak(Ljava/lang/String;)V

    goto :goto_3

    .line 937
    .end local v0    # "successStr":Ljava/lang/String;
    :cond_13
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "successStr":Ljava/lang/String;
    goto :goto_7

    .line 945
    :cond_14
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 949
    .end local v0    # "successStr":Ljava/lang/String;
    :cond_15
    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_16

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION:I

    :goto_8
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 951
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iget-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v2, v3, :cond_d

    if-nez p2, :cond_d

    .line 952
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_17

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION:I

    :goto_9
    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    .line 954
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v3

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isUSEnglishEnabled()Z

    move-result v2

    if-eqz v2, :cond_18

    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION:I

    :goto_a
    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 949
    :cond_16
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_US:I

    goto :goto_8

    .line 952
    :cond_17
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_US:I

    goto :goto_9

    .line 954
    :cond_18
    sget v2, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_RECOGNIZE_SUCCESS_FUNCTION_US:I

    goto :goto_a

    .line 966
    :pswitch_3
    if-nez p2, :cond_19

    .line 967
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->acquireWakeLock()V

    .line 968
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2, v8, v4}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 970
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopTTS()V

    .line 971
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 973
    :cond_19
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 974
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_speaknow:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 976
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "6 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_times:I

    invoke-virtual {p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 978
    invoke-direct {p0, v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingPopup(Z)V

    .line 979
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    if-eqz v2, :cond_0

    goto/16 :goto_0

    .line 985
    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 986
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_TAP_BUTTON_BELOW_UNLOCK_ADAPT:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 987
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 988
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 989
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setButtonIdle()V

    goto/16 :goto_0

    .line 993
    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 994
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 995
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 996
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->wake_up_word_already_assigned:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 997
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 998
    sget v2, Lcom/vlingo/midas/R$string;->wake_up_word_already_assigned:I

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->speakDefaultTTS(I)V

    .line 999
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->wake_up_word_already_assigned:I

    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    .line 1000
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1001
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setButtonIdle()V

    .line 1002
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->displayRecordingErrorPopup()V

    goto/16 :goto_0

    .line 1006
    :pswitch_6
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 1007
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 1008
    sput v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 1009
    iput-boolean v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isPhoneInUse:Z

    .line 1010
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    sget v3, Lcom/vlingo/midas/R$string;->phone_in_use:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1011
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 1012
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->customCommandHintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1013
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setButtonIdle()V

    goto/16 :goto_0

    .line 808
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private speakDefaultTTS(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 485
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-nez v1, :cond_0

    .line 486
    new-instance v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    invoke-direct {v1, p0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$1;)V

    iput-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    .line 487
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 488
    .local v0, "ttsfilter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 489
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTextToSpeechBroadcastReceiver:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$TextToSpeechBroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 490
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 491
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 493
    .end local v0    # "ttsfilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private startEnroll(SS)V
    .locals 2
    .param p1, "commandType"    # S
    .param p2, "wakeUpType"    # S

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v0, :cond_0

    .line 1788
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1789
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0, p1}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->startEnroll(S)I

    .line 1795
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->lastEnrollCount:I

    .line 1797
    :cond_0
    return-void

    .line 1792
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->startEnroll2(SS)I

    goto :goto_0
.end method

.method private startSettingsTTS()V
    .locals 4

    .prologue
    .line 381
    invoke-static {}, Lcom/vlingo/core/internal/util/ClientSuppliedValues;->isTalkbackOn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    sget v1, Lcom/vlingo/midas/R$string;->custom_recording_desc_before_tap_mic_for_gsa:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->speak(Ljava/lang/String;)V

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 385
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->TTSHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 387
    :cond_2
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->checkVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->setting_wakeup_tts_TAP_BUTTON_BELOW:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stopTTS()V
    .locals 1

    .prologue
    .line 1142
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 1144
    :try_start_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1148
    :cond_0
    :goto_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 1149
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTurn()V

    .line 1152
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_1

    .line 1153
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->stop()V

    .line 1154
    :cond_1
    return-void

    .line 1145
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private unregisterFPSVModelDownloadReceiver()V
    .locals 2

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 2080
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2081
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mFPSVModelDownloadReceiver:Landroid/content/BroadcastReceiver;

    .line 2082
    const-string/jumbo v0, "CustomCommandRecordingActivity"

    const-string/jumbo v1, "unregisterFPSVModelDownloadReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2084
    :cond_0
    return-void
.end method

.method private unregisterIncomingCallRx()V
    .locals 2

    .prologue
    .line 2071
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 2072
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mIncomingCallReceiver:Landroid/content/BroadcastReceiver;

    .line 2074
    const-string/jumbo v0, "CustomCommandRecordingActivity"

    const-string/jumbo v1, "unregisterIncomingCallRx"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2076
    :cond_0
    return-void
.end method


# virtual methods
.method public OnEnrollResult(III)V
    .locals 10
    .param p1, "job"    # I
    .param p2, "enroll_cnt"    # I
    .param p3, "checkResult"    # I

    .prologue
    .line 1186
    const/16 v0, 0x65

    .line 1187
    .local v0, "AL_Enroll":I
    const/16 v2, 0x66

    .line 1188
    .local v2, "CHECK_Enroll_START":I
    const/16 v1, 0x67

    .line 1189
    .local v1, "CHECK_Enroll_END":I
    const/16 v4, 0x68

    .line 1190
    .local v4, "Enroll_ConflictCommand":I
    const/16 v6, 0x69

    .line 1191
    .local v6, "Recording_Fail":I
    const/4 v3, 0x4

    .line 1192
    .local v3, "ENROLL_CNT":I
    const/4 v5, -0x1

    .line 1193
    .local v5, "RESET_ENROLL_CNT":I
    const-string/jumbo v7, "Wave"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "OnEnrollResult "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    packed-switch p1, :pswitch_data_0

    .line 1329
    :cond_0
    :goto_0
    return-void

    .line 1199
    :pswitch_0
    iget v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->lastEnrollCount:I

    if-eq v7, p2, :cond_3

    .line 1200
    if-nez p2, :cond_2

    .line 1202
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7, p2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyEnrollStarted(I)V

    .line 1203
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->playNotifySound()V

    .line 1204
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetNextEnroll(Z)V

    .line 1213
    :cond_1
    :goto_1
    iput p2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->lastEnrollCount:I

    .line 1216
    const/4 v7, 0x4

    if-ge p2, v7, :cond_0

    if-lez p2, :cond_0

    .line 1219
    sput p2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    goto :goto_0

    .line 1206
    :cond_2
    const/4 v7, 0x4

    if-ge p2, v7, :cond_1

    .line 1208
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7, p2, p3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyEnrollComplete(II)V

    goto :goto_1

    .line 1211
    :cond_3
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    const/4 v8, 0x5

    invoke-virtual {v7, p2, v8}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyEnrollComplete(II)V

    goto :goto_1

    .line 1237
    :pswitch_1
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyProcessingStarted()V

    goto :goto_0

    .line 1241
    :pswitch_2
    const/4 v7, 0x4

    if-ne p3, v7, :cond_4

    .line 1245
    :cond_4
    const/4 v7, 0x5

    if-ne p3, v7, :cond_5

    .line 1251
    :cond_5
    const/4 v7, 0x6

    if-ne p3, v7, :cond_7

    .line 1254
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v7, :cond_6

    .line 1255
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1256
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1258
    :cond_6
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    .line 1267
    :cond_7
    const/4 v7, 0x7

    if-ne p3, v7, :cond_9

    .line 1270
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v7, :cond_8

    .line 1271
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1272
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1274
    :cond_8
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    .line 1283
    :cond_9
    const/16 v7, 0x8

    if-ne p3, v7, :cond_b

    .line 1286
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v7, :cond_a

    .line 1287
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1288
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1290
    :cond_a
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    .line 1300
    :cond_b
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7, p3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyProcessingCompleted(I)V

    goto :goto_0

    .line 1304
    :pswitch_3
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v7, :cond_c

    .line 1305
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1306
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1308
    :cond_c
    sget-object v7, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ERROR:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const/4 v8, 0x0

    invoke-direct {p0, v7, v8}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1310
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7, p3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyError(I)V

    goto/16 :goto_0

    .line 1314
    :pswitch_4
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v7, :cond_d

    .line 1315
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1316
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1320
    :cond_d
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v7, p3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->notifyError(I)V

    .line 1321
    iget-object v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v8, Lcom/vlingo/midas/R$string;->phone_in_use:I

    invoke-virtual {v7, v8}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setErrorMessage(I)V

    .line 1322
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SpeakAgainForError()V

    goto/16 :goto_0

    .line 1195
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public OnRmsForWave(I)V
    .locals 3
    .param p1, "rmsValue"    # I

    .prologue
    .line 1336
    const-string/jumbo v0, "test"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "OnRmsForWave : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1338
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->addRmsValue(I)V

    .line 1362
    return-void
.end method

.method public OnSpectrumData([I)V
    .locals 1
    .param p1, "stats"    # [I

    .prologue
    .line 1800
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v0, p1}, Lcom/vlingo/midas/gui/ControlFragment;->showSpectrum([I)V

    .line 1802
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->addSpectrum([I)V

    .line 1803
    return-void
.end method

.method public OnVerifyResult(IS)V
    .locals 10
    .param p1, "verifyResult"    # I
    .param p2, "commandResult"    # S

    .prologue
    const/4 v9, 0x6

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 1158
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1159
    sget v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 1160
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    iget v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundStart:I

    move v3, v2

    move v5, v4

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 1161
    sget v0, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_speaknow:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1162
    .local v8, "format":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->timesView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "6 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->settings_custom_wakeup_times:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1169
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1175
    :goto_0
    sget v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    if-ge v0, v9, :cond_1

    .line 1176
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0, v4}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->startVerify(I)I

    .line 1182
    .end local v8    # "format":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1170
    .restart local v8    # "format":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1172
    .local v7, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1179
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :cond_1
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-direct {p0, v0, v4}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    goto :goto_1
.end method

.method SpeakAgainForError()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2247
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 2248
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setButtonIdle()V

    .line 2249
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 2250
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 2251
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->layoutMicBtnKitkat:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2253
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-nez v0, :cond_1

    .line 2254
    new-instance v0, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-direct {v0, p0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;-><init>(Lcom/samsung/voiceshell/VoiceEngineResultListener;)V

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    .line 2255
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->init()I

    .line 2257
    :cond_1
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 2258
    sput v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->adaptationCount:I

    .line 2259
    sput v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->enroll_count:I

    .line 2260
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->titleView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->removeDelegateViewForTalkBack(Landroid/view/View;)V

    .line 2261
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    if-eqz v0, :cond_2

    .line 2262
    iput-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    .line 2263
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->beforeState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 2264
    return-void
.end method

.method public deleteData(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1513
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1514
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1515
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1517
    :cond_0
    return-void
.end method

.method public fileRename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "beforeFilePath"    # Ljava/lang/String;
    .param p2, "afterFilePath"    # Ljava/lang/String;

    .prologue
    .line 1740
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1741
    .local v1, "mFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1742
    .local v0, "aFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1743
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1745
    :cond_0
    return-void
.end method

.method public isDowndloadedFPSVApk()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1932
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isExistFPSVLanguageModel()Z

    move-result v5

    if-nez v5, :cond_9

    .line 1935
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1936
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string/jumbo v6, "com.sec.android.voiceservice.fpsvmodel"

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1941
    .end local v0    # "context":Landroid/content/Context;
    .local v4, "mFPSVModel":Landroid/content/pm/ApplicationInfo;
    :goto_0
    if-eqz v4, :cond_2

    .line 1942
    iput-boolean v8, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    .line 1943
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1944
    .local v3, "localIntent":Landroid/content/Intent;
    const-string/jumbo v5, "com.sec.android.voiceservice.action.FPSVModelDownload"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1945
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v5, :cond_1

    .line 1946
    const-string/jumbo v5, "keyword"

    const-string/jumbo v6, "okgoogle"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1947
    const-string/jumbo v5, "language"

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1952
    :goto_1
    invoke-virtual {p0, v3}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1953
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V

    .line 2045
    .end local v3    # "localIntent":Landroid/content/Intent;
    .end local v4    # "mFPSVModel":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_2
    return-void

    .line 1938
    :catch_0
    move-exception v1

    .line 1939
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    .restart local v4    # "mFPSVModel":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 1949
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3    # "localIntent":Landroid/content/Intent;
    :cond_1
    const-string/jumbo v5, "keyword"

    const-string/jumbo v6, "higalaxy"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1950
    const-string/jumbo v5, "language"

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 1955
    .end local v3    # "localIntent":Landroid/content/Intent;
    :cond_2
    const/4 v2, 0x0

    .line 1956
    .local v2, "localBuilder":Landroid/app/AlertDialog$Builder;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1957
    new-instance v2, Landroid/app/AlertDialog$Builder;

    .end local v2    # "localBuilder":Landroid/app/AlertDialog$Builder;
    new-instance v5, Landroid/view/ContextThemeWrapper;

    const v6, 0x103012b

    invoke-direct {v5, p0, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1962
    .restart local v2    # "localBuilder":Landroid/app/AlertDialog$Builder;
    :goto_3
    sget v5, Lcom/vlingo/midas/R$string;->download_language_data_title:I

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1963
    sget v5, Lcom/vlingo/midas/R$string;->download_language_data_desc:I

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1965
    sget v5, Lcom/vlingo/midas/R$string;->download_btn:I

    new-instance v6, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;

    invoke-direct {v6, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$11;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1982
    sget v5, Lcom/vlingo/midas/R$string;->cancel_btn:I

    new-instance v6, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;

    invoke-direct {v6, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v2, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1996
    new-instance v5, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$13;

    invoke-direct {v5, p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$13;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2013
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isShowingDialog:Z

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFinishing()Z

    move-result v5

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    if-nez v5, :cond_3

    .line 2014
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2015
    iput-boolean v9, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isShowingDialog:Z

    .line 2017
    :cond_3
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    if-eqz v5, :cond_0

    .line 2018
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v5, :cond_4

    .line 2019
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 2021
    :cond_4
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V

    .line 2022
    iput-boolean v8, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isClickedDownload:Z

    .line 2023
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-nez v5, :cond_5

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isLanguageUseFPSV()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2024
    :cond_5
    iget-object v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    const-string/jumbo v7, "/system/wakeupdata/sensory/"

    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v5, :cond_8

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v5

    :goto_4
    invoke-virtual {v6, v7, v5}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetResourceInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 2028
    :cond_6
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    iget-short v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    iget-short v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    invoke-virtual {v5, v6, v8, v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetWakeUp(IIS)V

    goto/16 :goto_2

    .line 1959
    :cond_7
    new-instance v2, Landroid/app/AlertDialog$Builder;

    .end local v2    # "localBuilder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .restart local v2    # "localBuilder":Landroid/app/AlertDialog$Builder;
    goto :goto_3

    .line 2024
    :cond_8
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 2032
    .end local v2    # "localBuilder":Landroid/app/AlertDialog$Builder;
    .end local v4    # "mFPSVModel":Landroid/content/pm/ApplicationInfo;
    :cond_9
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-nez v5, :cond_a

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isLanguageUseFPSV()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2033
    :cond_a
    iget-object v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    const-string/jumbo v7, "/system/wakeupdata/sensory/"

    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v5, :cond_c

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v5

    :goto_5
    invoke-virtual {v6, v7, v5}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetResourceInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 2037
    :cond_b
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    iget-short v6, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    iget-short v7, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    invoke-virtual {v5, v6, v8, v7}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetWakeUp(IIS)V

    .line 2038
    iget-boolean v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    if-nez v5, :cond_d

    .line 2039
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V

    .line 2043
    :goto_6
    iget-object v5, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_2

    .line 2033
    :cond_c
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    .line 2041
    :cond_d
    iput-boolean v8, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    goto :goto_6
.end method

.method public isExistFPSVLanguageModel()Z
    .locals 3

    .prologue
    .line 2048
    const/4 v0, 0x0

    .line 2049
    .local v0, "isModelExist":Z
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v1, :cond_1

    .line 2051
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/okgoogle_am_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/okgoogle_search_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/okgoogle_ubm_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getGSALanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2054
    const/4 v0, 0x1

    .line 2067
    :goto_0
    return v0

    .line 2056
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2059
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/higalaxy_am_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/higalaxy_search_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/higalaxy_ubm_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2062
    const/4 v0, 0x1

    goto :goto_0

    .line 2064
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 1826
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1827
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1828
    const/4 v1, 0x1

    .line 1830
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isLanguageUseFPSV()Z
    .locals 2

    .prologue
    .line 1922
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getLanguageString()Ljava/lang/String;

    move-result-object v0

    .line 1923
    .local v0, "currentLanguage":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1924
    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SUPPORTED_LANGUAGE_FOR_FPSV:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1925
    const/4 v1, 0x1

    .line 1928
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1749
    iget-boolean v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mBtNeeded:Z

    if-eqz v2, :cond_0

    .line 1750
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->cancelTTS()V

    .line 1752
    :cond_0
    sget v2, Lcom/vlingo/midas/R$id;->custom_wake_up_layout:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1753
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v2, 0x1

    const/high16 v3, 0x43e60000    # 460.0f

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v0, v2

    .line 1754
    .local v0, "height1":I
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1755
    sget v2, Lcom/vlingo/midas/R$id;->custom_wake_up_layout:I

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1758
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    sget-object v3, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-ne v2, v3, :cond_4

    .line 1759
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v2, :cond_1

    .line 1760
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1761
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1763
    :cond_1
    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-direct {p0, v2, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1773
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1774
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1776
    :cond_2
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_3

    .line 1777
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1778
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 1781
    :cond_3
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterIncomingCallRx()V

    .line 1782
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterFPSVModelDownloadReceiver()V

    .line 1783
    return-void

    .line 1764
    :cond_4
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    sget-object v3, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-eq v2, v3, :cond_6

    .line 1765
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v2, :cond_5

    .line 1766
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1767
    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1769
    :cond_5
    sget-object v2, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-direct {p0, v2, v5}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    goto :goto_0

    .line 1771
    :cond_6
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1024
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1025
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 1026
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->orientation:I

    .line 1027
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->initialize()V

    .line 1028
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setClickHandlers()V

    .line 1029
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setOrientationChangeUI()V

    .line 1030
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 1031
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRelativeLayoutMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 1032
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isMicFocused:Z

    if-eqz v0, :cond_1

    .line 1033
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 1039
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_0

    .line 1040
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->setLanguage(Ljava/util/Locale;)V

    .line 1041
    :cond_0
    return-void

    .line 1035
    :cond_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearFocus()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 229
    invoke-static {}, Lcom/vlingo/midas/iux/IUXManager;->isTOSAccepted()Z

    move-result v14

    if-nez v14, :cond_0

    .line 230
    new-instance v14, Landroid/speech/tts/TextToSpeech;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mInitListener:Landroid/speech/tts/TextToSpeech$OnInitListener;

    move-object/from16 v16, v0

    invoke-direct/range {v14 .. v16}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    .line 234
    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 235
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRequestedOrientation(I)V

    .line 238
    :cond_1
    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    .line 239
    .local v8, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v14

    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v14

    invoke-virtual {v14, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 242
    iget v14, v8, Landroid/util/DisplayMetrics;->density:F

    float-to-double v14, v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v14, v14, v16

    if-nez v14, :cond_2

    iget v14, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v15, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v14, v15

    const/16 v15, 0x7f0

    if-ne v14, v15, :cond_2

    iget v14, v8, Landroid/util/DisplayMetrics;->xdpi:F

    const v15, 0x4315d32c

    sub-float/2addr v14, v15

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-nez v14, :cond_2

    iget v14, v8, Landroid/util/DisplayMetrics;->ydpi:F

    const v15, 0x431684be

    sub-float/2addr v14, v15

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-eqz v14, :cond_3

    :cond_2
    iget v14, v8, Landroid/util/DisplayMetrics;->density:F

    float-to-double v14, v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v14, v14, v16

    if-nez v14, :cond_b

    iget v14, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v15, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/2addr v14, v15

    const/16 v15, 0x820

    if-ne v14, v15, :cond_b

    iget v14, v8, Landroid/util/DisplayMetrics;->xdpi:F

    const v15, 0x4315d2f2

    sub-float/2addr v14, v15

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-nez v14, :cond_b

    iget v14, v8, Landroid/util/DisplayMetrics;->ydpi:F

    const v15, 0x4316849c

    sub-float/2addr v14, v15

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-nez v14, :cond_b

    .line 246
    :cond_3
    sget v14, Lcom/vlingo/midas/R$style;->DialogSlideAnim:I

    sput v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTheme:I

    .line 247
    sget v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTheme:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setTheme(I)V

    .line 253
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    const-string/jumbo v15, "fromSettings"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromSettings:Z

    .line 254
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    const-string/jumbo v15, "fromAlwaysOnSettings"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromAlwaysOnSettings:Z

    .line 256
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const-string/jumbo v15, "audio"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioManager:Landroid/media/AudioManager;

    .line 261
    invoke-super/range {p0 .. p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 263
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 265
    const-string/jumbo v14, "/system/lib/libSensoryUDTSIDEngine.so"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFileExist(Ljava/lang/String;)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isSensoryUDTSIDsoFileExist:Z

    .line 267
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mContext:Landroid/content/Context;

    .line 268
    sget-object v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 269
    const-string/jumbo v14, "samsung_wakeup_data_enable"

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_4

    .line 270
    const-string/jumbo v14, "samsung_wakeup_data_enable"

    const/4 v15, 0x1

    invoke-static {v14, v15}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 287
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 288
    .local v5, "i":Landroid/content/Intent;
    const-string/jumbo v14, "SETTING_VOICE_LOCK"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 291
    const-string/jumbo v14, "wakeUpType"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    int-to-short v14, v14

    move-object/from16 v0, p0

    iput-short v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    .line 293
    :cond_5
    const-string/jumbo v14, "trainType"

    const/4 v15, -0x1

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    int-to-short v14, v14

    move-object/from16 v0, p0

    iput-short v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    .line 295
    const-string/jumbo v14, "fromGSA"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 296
    const/4 v14, 0x4

    move-object/from16 v0, p0

    iput-short v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    .line 297
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-short v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    .line 299
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    .line 302
    :cond_6
    const-string/jumbo v14, "wycs.show.done"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->inIUXMode:Z

    .line 303
    const-string/jumbo v14, "help"

    const/4 v15, 0x0

    invoke-virtual {v5, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    .line 305
    const-string/jumbo v14, "CustomCommandRecordingActivity"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "wakeUpType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-short v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", commandType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-short v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", inIUXMode : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->inIUXMode:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ", helpwakeup : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v14

    if-nez v14, :cond_7

    .line 309
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->getInstance()Lcom/samsung/wakeupsetting/BluetoothManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/wakeupsetting/BluetoothManager;->init()V

    .line 310
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->getInstance()Lcom/samsung/wakeupsetting/BluetoothManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/wakeupsetting/BluetoothManager;->stopSCO()V

    .line 314
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 315
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->requestWindowFeature(I)Z

    .line 316
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    const/16 v15, 0x400

    const/16 v16, 0x400

    invoke-virtual/range {v14 .. v16}, Landroid/view/Window;->setFlags(II)V

    .line 318
    :cond_8
    sget v14, Lcom/vlingo/midas/R$layout;->custom_command_recording:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setContentView(I)V

    .line 319
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v11

    .line 320
    .local v11, "titlebar":Landroid/app/ActionBar;
    if-eqz v11, :cond_9

    .line 321
    const/16 v14, 0x1c

    invoke-virtual {v11, v14}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 322
    const/4 v14, 0x1

    invoke-virtual {v11, v14}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 323
    sget v14, Lcom/vlingo/midas/R$string;->menu_custom_wake_up:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    const/16 v15, 0x400

    const/16 v16, 0x400

    invoke-virtual/range {v14 .. v16}, Landroid/view/Window;->setFlags(II)V

    .line 329
    :cond_9
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 330
    .local v7, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-string/jumbo v15, "privateFlags"

    invoke-virtual {v14, v15}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    .line 331
    .local v9, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-string/jumbo v15, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v14, v15}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    .line 332
    .local v10, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-string/jumbo v15, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v14, v15}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 334
    .local v3, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v2, 0x0

    .local v2, "currentPrivateFlags":I
    const/4 v13, 0x0

    .local v13, "valueofFlagsEnableStatusBar":I
    const/4 v12, 0x0

    .line 335
    .local v12, "valueofFlagsDisableTray":I
    invoke-virtual {v9, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    .line 336
    invoke-virtual {v10, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v13

    .line 337
    invoke-virtual {v3, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v12

    .line 339
    or-int/2addr v2, v13

    .line 340
    or-int/2addr v2, v12

    .line 342
    invoke-virtual {v9, v7, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 343
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getWindow()Landroid/view/Window;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    .end local v2    # "currentPrivateFlags":I
    .end local v3    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v7    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v9    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v10    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v12    # "valueofFlagsDisableTray":I
    .end local v13    # "valueofFlagsEnableStatusBar":I
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SetWakeupData()V

    .line 356
    new-instance v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    .line 357
    sget v14, Lcom/vlingo/midas/R$id;->wave:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/samsung/wakeupsetting/WaveView;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    .line 358
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    invoke-virtual {v14}, Lcom/samsung/wakeupsetting/WaveView;->initialize()V

    .line 359
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v14, v15}, Lcom/samsung/wakeupsetting/WaveView;->setAdapter(Lcom/samsung/wakeupsetting/WaveViewAdapter;)V

    .line 360
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/samsung/wakeupsetting/WaveView;->setAnimationCompleteListener(Lcom/samsung/wakeupsetting/WaveView$AnimationCompleteListener;)V

    .line 363
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v14, :cond_a

    .line 364
    new-instance v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    .line 365
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    invoke-virtual {v14}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->initialize()V

    .line 367
    :cond_a
    new-instance v6, Landroid/content/IntentFilter;

    const-string/jumbo v14, "android.intent.action.SCREEN_ON"

    invoke-direct {v6, v14}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 368
    .local v6, "intentFilter":Landroid/content/IntentFilter;
    new-instance v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$2;-><init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 377
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v6}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 378
    return-void

    .line 249
    .end local v5    # "i":Landroid/content/Intent;
    .end local v6    # "intentFilter":Landroid/content/IntentFilter;
    .end local v11    # "titlebar":Landroid/app/ActionBar;
    :cond_b
    sget v14, Lcom/vlingo/midas/R$style;->CustomNoActionBar:I

    sput v14, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTheme:I

    goto/16 :goto_0

    .line 347
    .restart local v5    # "i":Landroid/content/Intent;
    .restart local v11    # "titlebar":Landroid/app/ActionBar;
    :catch_0
    move-exception v4

    .line 350
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 344
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v14

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1045
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 1049
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1050
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1051
    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->screenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 1055
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_1

    .line 1056
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1060
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    if-eqz v0, :cond_2

    .line 1061
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/WaveView;->reset()V

    .line 1062
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v0, :cond_3

    .line 1063
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->destroy()V

    .line 1064
    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    .line 1068
    :cond_3
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRelativeLayoutMain:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 1069
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_4

    .line 1070
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1071
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$color;->solid_black:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setBackgroundColor(I)V

    .line 1072
    iput-object v3, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    .line 1074
    :cond_4
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1075
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->getInstance()Lcom/samsung/wakeupsetting/BluetoothManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->destroy()V

    .line 1079
    :cond_5
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveView:Lcom/samsung/wakeupsetting/WaveView;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/WaveView;->shutdown()V

    .line 1080
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_6

    .line 1081
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->shutdown()V

    .line 1083
    :cond_6
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1084
    return-void

    .line 1058
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onEnrollAnimationComplete(I)V
    .locals 2
    .param p1, "trial"    # I

    .prologue
    .line 2178
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    invoke-virtual {v0, p1, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setSuccessCount(IZ)V

    .line 2180
    return-void
.end method

.method public onEnrollAnimationStarted()V
    .locals 0

    .prologue
    .line 2184
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->playNotifySound()V

    .line 2185
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 2153
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2157
    :goto_0
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 2155
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    goto :goto_0

    .line 2153
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 1089
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 1091
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    if-eqz v0, :cond_1

    .line 1092
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopVerify()I

    .line 1093
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    invoke-virtual {v0}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->stopEnroll()I

    .line 1095
    :cond_1
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 1098
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopTTS()V

    .line 1100
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->LISTENING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-ne v0, v1, :cond_2

    .line 1101
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->IDLE:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 1102
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1103
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v0, :cond_2

    .line 1104
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mListeningImg:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    .line 1109
    :cond_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    sget-object v1, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->ADAPTING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    if-ne v0, v1, :cond_3

    .line 1110
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    .line 1117
    :cond_3
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->releaseWakeLock()V

    .line 1119
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->doneButtonClicked:Z

    if-nez v0, :cond_4

    .line 1120
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startVoiceWakeup()V

    .line 1122
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromSettings:Z

    .line 1124
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->inIUXMode:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->helpwakeup:Z

    if-eqz v0, :cond_6

    .line 1125
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 1126
    :cond_6
    return-void
.end method

.method public onProcessingAnimationComplete()V
    .locals 4

    .prologue
    .line 2195
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->waveAdapter:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SensoryWaveViewAdapter;->getLastStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2244
    :goto_0
    return-void

    .line 2197
    :pswitch_0
    sget-object v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;->THINKING:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 2198
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->playNotifySound()V

    goto :goto_0

    .line 2201
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$string;->find_a_quieter_area_and_try_1_more_time:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setErrorMessage(I)V

    goto :goto_0

    .line 2206
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_2:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setErrorMessage(I)V

    .line 2209
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_0

    .line 2210
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_2:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->speak(Ljava/lang/String;)V

    .line 2213
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SpeakAgainForError()V

    goto :goto_0

    .line 2212
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_2:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\n"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_1

    .line 2218
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_1:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setErrorMessage(I)V

    .line 2221
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_1

    .line 2222
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_1:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->speak(Ljava/lang/String;)V

    .line 2225
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SpeakAgainForError()V

    goto :goto_0

    .line 2224
    :cond_1
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_1:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\n"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_2

    .line 2230
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->recBody:Lcom/samsung/wakeupsetting/CustomWaveLayout;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_3:I

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomWaveLayout;->setErrorMessage(I)V

    .line 2233
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z

    if-eqz v0, :cond_2

    .line 2234
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mTTSForGSA:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_3:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$SystemTTS;->speak(Ljava/lang/String;)V

    .line 2237
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->SpeakAgainForError()V

    goto/16 :goto_0

    .line 2236
    :cond_2
    invoke-static {}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->getInstance()Lcom/vlingo/core/internal/dialogmanager/DialogFlow;

    move-result-object v0

    sget v1, Lcom/vlingo/midas/R$string;->wakeup_error_case_3:I

    invoke-virtual {p0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\n"

    const-string/jumbo v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vlingo/core/internal/dialogmanager/DialogFlow;->tts(Ljava/lang/String;)V

    goto :goto_3

    .line 2195
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onProcessingAnimationStarted()V
    .locals 0

    .prologue
    .line 2191
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 752
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 754
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isOnlyBDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 755
    invoke-static {}, Lcom/samsung/wakeupsetting/BluetoothManager;->getInstance()Lcom/samsung/wakeupsetting/BluetoothManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/BluetoothManager;->stopSCO()V

    .line 760
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 761
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setVolumeControlStream(I)V

    .line 765
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->orientation:I

    .line 766
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->initialize()V

    .line 767
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setClickHandlers()V

    .line 768
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setOrientationChangeUI()V

    .line 769
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->currentState:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;

    invoke-direct {p0, v0, v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setRecordingStateUI(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$AppState;Z)V

    .line 770
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->updateCurrentLocale()V

    .line 771
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 772
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    if-nez v0, :cond_4

    .line 773
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isTryAgain:Z

    if-nez v0, :cond_1

    .line 774
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v0

    if-nez v0, :cond_3

    .line 776
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isDowndloadedFPSVApk()V

    .line 784
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopVoiceWakeup()V

    .line 785
    return-void

    .line 763
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->setVolumeControlStream(I)V

    goto :goto_0

    .line 778
    :cond_3
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V

    goto :goto_1

    .line 782
    :cond_4
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isScreenOn:Z

    goto :goto_1
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 1130
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStop()V

    .line 1132
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterIncomingCallRx()V

    .line 1133
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->unregisterFPSVModelDownloadReceiver()V

    .line 1134
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->stopTTS()V

    .line 1136
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->removeListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 1137
    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 1139
    return-void
.end method

.method public renameData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newpath"    # Ljava/lang/String;

    .prologue
    .line 1520
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1521
    .local v0, "mFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1522
    .local v1, "mFileNew":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1523
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1525
    :cond_0
    return-void
.end method

.method public startVoiceWakeup()V
    .locals 2

    .prologue
    .line 1856
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1859
    :cond_0
    return-void
.end method

.method public stopVoiceWakeup()V
    .locals 2

    .prologue
    .line 1862
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1863
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 1864
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1866
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 1868
    :cond_0
    return-void
.end method
