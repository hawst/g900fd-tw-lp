.class Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;
.super Ljava/lang/Object;
.source "CustomCommandRecordingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isDowndloadedFPSVApk()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V
    .locals 0

    .prologue
    .line 1982
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "paramDialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "paramInt"    # I

    .prologue
    const/4 v3, 0x0

    .line 1984
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mRecognizer:Lcom/samsung/voiceshell/WakeUpCmdRecognizer;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$900(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Lcom/samsung/voiceshell/WakeUpCmdRecognizer;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->mWakeUpType:S
    invoke-static {v1}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v1

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->commandType:S
    invoke-static {v2}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1700(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)S

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/voiceshell/WakeUpCmdRecognizer;->SetWakeUp(IIS)V

    .line 1986
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 1987
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    iput-boolean v3, v0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isShowingDialog:Z

    .line 1988
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->isFromGSA:Z
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1500(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1989
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    invoke-virtual {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->finish()V

    .line 1991
    :cond_0
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # invokes: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->startSettingsTTS()V
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$2800(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)V

    .line 1992
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity$12;->this$0:Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;

    # getter for: Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->reco_idleBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;->access$1200(Lcom/samsung/wakeupsetting/CustomCommandRecordingActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1993
    return-void
.end method
