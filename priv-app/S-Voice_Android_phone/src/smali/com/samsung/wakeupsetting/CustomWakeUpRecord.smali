.class public Lcom/samsung/wakeupsetting/CustomWakeUpRecord;
.super Landroid/widget/RelativeLayout;
.source "CustomWakeUpRecord.java"


# instance fields
.field private final MAX_SUCCESS_COUNT:I

.field private context:Landroid/content/Context;

.field private images:[Landroid/widget/ImageView;

.field private isAdaptButtonPressed:Z

.field private mProgressImage:Landroid/widget/ImageView;

.field private mSuccessCountText:Landroid/widget/TextView;

.field private prevCount:I

.field private recordcount_4:[I

.field private shouldPlaySuccessSound:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->MAX_SUCCESS_COUNT:I

    .line 31
    iput v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->prevCount:I

    .line 32
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->shouldPlaySuccessSound:Z

    .line 37
    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->isAdaptButtonPressed:Z

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    .line 43
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->initializeImages()V

    .line 44
    iput-object p1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->context:Landroid/content/Context;

    .line 45
    const-string/jumbo v0, "com.vlingo.midas"

    const-string/jumbo v1, "WKCS"

    invoke-static {p1, v0, v1}, Lcom/vlingo/midas/util/log/PreloadAppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method private delegateViewForTalkBack(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 90
    if-eqz p1, :cond_0

    .line 91
    invoke-direct {p0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 93
    :cond_0
    return-void
.end method

.method private initializeImages()V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    const/4 v1, 0x0

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_wake_up_command_4progress_1:I

    aput v2, v0, v1

    .line 105
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    const/4 v1, 0x1

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_wake_up_command_4progress_2:I

    aput v2, v0, v1

    .line 106
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    const/4 v1, 0x2

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_wake_up_command_4progress_3:I

    aput v2, v0, v1

    .line 107
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    const/4 v1, 0x3

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_talk_wake_up_command_4progress_4:I

    aput v2, v0, v1

    .line 108
    return-void
.end method

.method private setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord$1;

    invoke-direct {v0, p0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord$1;-><init>(Lcom/samsung/wakeupsetting/CustomWakeUpRecord;)V

    return-object v0
.end method


# virtual methods
.method public isSuccess()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->shouldPlaySuccessSound:Z

    return v0
.end method

.method public onFinishInflate()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    .line 66
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_1:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v3

    .line 67
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_2:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v4

    .line 68
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_3:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v5

    .line 69
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_4:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v6

    .line 70
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_5:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v7

    .line 71
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    const/4 v2, 0x5

    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting_6:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 73
    sget v0, Lcom/vlingo/midas/R$id;->wake_up_count:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mSuccessCountText:Landroid/widget/TextView;

    .line 74
    sget v0, Lcom/vlingo/midas/R$id;->wake_up_setting:I

    invoke-virtual {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mProgressImage:Landroid/widget/ImageView;

    .line 76
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mSuccessCountText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mProgressImage:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v0, v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v0, v0, v4

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v0, v0, v5

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v0, v0, v6

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 86
    :cond_0
    return-void
.end method

.method public setImageVisible(Z)V
    .locals 4
    .param p1, "adaptButtonPressed"    # Z

    .prologue
    const/16 v3, 0x8

    .line 52
    iput-boolean p1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->isAdaptButtonPressed:Z

    .line 53
    if-eqz p1, :cond_0

    .line 54
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 59
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->images:[Landroid/widget/ImageView;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    :cond_1
    return-void
.end method

.method public setSuccessCount(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    .line 114
    iget v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->prevCount:I

    if-eq p1, v1, :cond_1

    .line 115
    iput p1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->prevCount:I

    .line 116
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->shouldPlaySuccessSound:Z

    .line 121
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x6

    if-ge v0, v1, :cond_2

    .line 122
    if-ge v0, p1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mSuccessCountText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->isAdaptButtonPressed:Z

    if-nez v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->mProgressImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->recordcount_4:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 121
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/wakeupsetting/CustomWakeUpRecord;->shouldPlaySuccessSound:Z

    goto :goto_0

    .line 129
    .restart local v0    # "i":I
    :cond_2
    return-void
.end method
