.class Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;
.super Ljava/lang/Object;
.source "VoiceWakeUpFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->showWakeupInSecuredLockDialog()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;


# direct methods
.method constructor <init>(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    .line 457
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    # getter for: Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->mWakeup:Landroid/preference/CheckBoxPreference;
    invoke-static {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->access$100(Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 458
    const/4 v0, 0x0

    .line 459
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v1}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "preferences_voice_wake_up"

    iget-object v3, p0, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment$4;->this$0:Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;

    invoke-virtual {v3}, Lcom/samsung/wakeupsetting/VoiceWakeUpFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v1, v2, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 460
    const-string/jumbo v1, "VOICE_WAKE_UP_IN_SECURED_POP"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 461
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 462
    return-void
.end method
