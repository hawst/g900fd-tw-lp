.class public Lcom/samsung/samsungapps/UpdateManager;
.super Ljava/lang/Object;
.source "UpdateManager.java"


# static fields
.field static final MESSAGE_CMD:Ljava/lang/String; = "MESSAGE_CMD"

.field static final STATUS:Ljava/lang/String; = "DOWNLOAD_STATUS"

.field static final STATUS_ERROR_LATEST:I = 0x1

.field static final STATUS_ERROR_NETWORK:I = 0x9

.field static final STATUS_ERROR_NO_CONTENTS:I = 0x0

.field static final STATUS_UPDATE:I = 0x2

.field public static availableUpdate:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mThread:Lcom/samsung/samsungapps/UpdateCheckThread;

.field private threadCanceled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z

    .line 34
    new-instance v0, Lcom/samsung/samsungapps/UpdateManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/samsungapps/UpdateManager$1;-><init>(Lcom/samsung/samsungapps/UpdateManager;)V

    iput-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mHandler:Landroid/os/Handler;

    .line 93
    iput-object p1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/samsungapps/UpdateManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/samsungapps/UpdateManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/samsungapps/UpdateManager;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/samsungapps/UpdateManager;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/samsungapps/UpdateManager;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/samsungapps/UpdateManager;->setAlertDialog(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/samsungapps/UpdateManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/samsungapps/UpdateManager;->startSamsungApps()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/samsungapps/UpdateManager;)Lcom/samsung/samsungapps/UpdateCheckThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/samsungapps/UpdateManager;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;

    return-object v0
.end method

.method private setAlertDialog(Ljava/lang/String;)Landroid/app/Dialog;
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 124
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 126
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->software_update:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 127
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 128
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/samsungapps/UpdateManager$2;

    invoke-direct {v2, p0}, Lcom/samsung/samsungapps/UpdateManager$2;-><init>(Lcom/samsung/samsungapps/UpdateManager;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 137
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private setProgressDialog()Landroid/app/ProgressDialog;
    .locals 3

    .prologue
    .line 141
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 142
    .local v0, "progressDialog":Landroid/app/ProgressDialog;
    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$string;->checking_software_updates:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 144
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 145
    new-instance v1, Lcom/samsung/samsungapps/UpdateManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/samsungapps/UpdateManager$3;-><init>(Lcom/samsung/samsungapps/UpdateManager;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 154
    return-object v0
.end method

.method private startSamsungApps()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 112
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 113
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "com.sec.android.app.samsungapps"

    const-string/jumbo v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string/jumbo v1, "directcall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 116
    const-string/jumbo v1, "CallerType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 117
    const-string/jumbo v1, "GUID"

    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 120
    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 121
    return-void
.end method


# virtual methods
.method public startUpdateCheck()V
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z

    .line 99
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/vlingo/midas/settings/SettingsScreen;

    if-eqz v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/samsung/samsungapps/UpdateManager;->setProgressDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 101
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 106
    new-instance v0, Lcom/samsung/samsungapps/UpdateCheckThread;

    iget-object v1, p0, Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/samsung/samsungapps/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;

    .line 107
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/samsungapps/UpdateCheckThread;->start()V

    .line 109
    :cond_1
    return-void
.end method
