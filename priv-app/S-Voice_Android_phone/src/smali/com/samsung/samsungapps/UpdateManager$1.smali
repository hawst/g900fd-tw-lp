.class Lcom/samsung/samsungapps/UpdateManager$1;
.super Landroid/os/Handler;
.source "UpdateManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/samsungapps/UpdateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/samsungapps/UpdateManager;


# direct methods
.method constructor <init>(Lcom/samsung/samsungapps/UpdateManager;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 36
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "MESSAGE_CMD"

    const-string/jumbo v4, "DOWNLOAD_STATUS"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "cmd":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$100(Lcom/samsung/samsungapps/UpdateManager;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 42
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # setter for: Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z
    invoke-static {v2, v5}, Lcom/samsung/samsungapps/UpdateManager;->access$102(Lcom/samsung/samsungapps/UpdateManager;Z)Z

    goto :goto_0

    .line 46
    :cond_2
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Lcom/vlingo/midas/settings/SettingsScreen;

    if-eqz v2, :cond_4

    .line 47
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$200(Lcom/samsung/samsungapps/UpdateManager;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$200(Lcom/samsung/samsungapps/UpdateManager;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 48
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$200(Lcom/samsung/samsungapps/UpdateManager;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 49
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/samsungapps/UpdateManager;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2, v3}, Lcom/samsung/samsungapps/UpdateManager;->access$202(Lcom/samsung/samsungapps/UpdateManager;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 52
    :cond_3
    const-string/jumbo v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "DOWNLOAD_STATUS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 57
    :sswitch_0
    sput-boolean v5, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    .line 58
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    iget-object v3, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->latest_updates_have_already_been_installed:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/samsungapps/UpdateManager;->setAlertDialog(Ljava/lang/String;)Landroid/app/Dialog;
    invoke-static {v2, v3}, Lcom/samsung/samsungapps/UpdateManager;->access$300(Lcom/samsung/samsungapps/UpdateManager;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v1

    .line 60
    .local v1, "upgradeDialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 63
    .end local v1    # "upgradeDialog":Landroid/app/Dialog;
    :sswitch_1
    sput-boolean v6, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    .line 64
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # invokes: Lcom/samsung/samsungapps/UpdateManager;->startSamsungApps()V
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$400(Lcom/samsung/samsungapps/UpdateManager;)V

    goto :goto_0

    .line 67
    :sswitch_2
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    iget-object v3, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->unable_to_update_software:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/samsungapps/UpdateManager;->setAlertDialog(Ljava/lang/String;)Landroid/app/Dialog;
    invoke-static {v2, v3}, Lcom/samsung/samsungapps/UpdateManager;->access$300(Lcom/samsung/samsungapps/UpdateManager;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v1

    .line 69
    .restart local v1    # "upgradeDialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 75
    .end local v1    # "upgradeDialog":Landroid/app/Dialog;
    :cond_4
    const-string/jumbo v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "DOWNLOAD_STATUS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_0

    .line 79
    :pswitch_0
    sput-boolean v5, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    goto/16 :goto_0

    .line 82
    :pswitch_1
    sput-boolean v6, Lcom/samsung/samsungapps/UpdateManager;->availableUpdate:Z

    .line 83
    iget-object v2, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/samsungapps/UpdateManager$1;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/samsungapps/UpdateManager;->access$000(Lcom/samsung/samsungapps/UpdateManager;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/vlingo/midas/R$string;->software_updates_are_available:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 54
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
