.class Lcom/samsung/samsungapps/UpdateManager$3;
.super Ljava/lang/Object;
.source "UpdateManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/samsungapps/UpdateManager;->setProgressDialog()Landroid/app/ProgressDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/samsungapps/UpdateManager;


# direct methods
.method constructor <init>(Lcom/samsung/samsungapps/UpdateManager;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/samsungapps/UpdateManager$3;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager$3;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/samsungapps/UpdateManager;->threadCanceled:Z
    invoke-static {v0, v1}, Lcom/samsung/samsungapps/UpdateManager;->access$102(Lcom/samsung/samsungapps/UpdateManager;Z)Z

    .line 149
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager$3;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;
    invoke-static {v0}, Lcom/samsung/samsungapps/UpdateManager;->access$500(Lcom/samsung/samsungapps/UpdateManager;)Lcom/samsung/samsungapps/UpdateCheckThread;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager$3;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;
    invoke-static {v0}, Lcom/samsung/samsungapps/UpdateManager;->access$500(Lcom/samsung/samsungapps/UpdateManager;)Lcom/samsung/samsungapps/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/samsungapps/UpdateCheckThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/samsung/samsungapps/UpdateManager$3;->this$0:Lcom/samsung/samsungapps/UpdateManager;

    # getter for: Lcom/samsung/samsungapps/UpdateManager;->mThread:Lcom/samsung/samsungapps/UpdateCheckThread;
    invoke-static {v0}, Lcom/samsung/samsungapps/UpdateManager;->access$500(Lcom/samsung/samsungapps/UpdateManager;)Lcom/samsung/samsungapps/UpdateCheckThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/samsungapps/UpdateCheckThread;->cancel()V

    .line 152
    :cond_0
    return-void
.end method
