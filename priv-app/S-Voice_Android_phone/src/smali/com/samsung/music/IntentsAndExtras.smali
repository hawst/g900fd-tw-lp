.class public Lcom/samsung/music/IntentsAndExtras;
.super Ljava/lang/Object;
.source "IntentsAndExtras.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/music/IntentsAndExtras$MoodType;
    }
.end annotation


# static fields
.field public static final EXTRA_LAUNCH_MUSIC_PLAYER:Ljava/lang/String; = "launchMusicPlayer"

.field public static final EXTRA_LAUNCH_SQUARE:Ljava/lang/String; = "launchMusicSquare"

.field public static final EXTRA_NAME:Ljava/lang/String; = "extra_name"

.field public static final EXTRA_TYPE:Ljava/lang/String; = "extra_type"

.field public static final EXTRA_TYPE_ALBUM:Ljava/lang/String; = "album"

.field public static final EXTRA_TYPE_ARTIST:Ljava/lang/String; = "artist"

.field public static final EXTRA_TYPE_PLAYLIST:Ljava/lang/String; = "playlist"

.field public static final EXTRA_TYPE_TITLE:Ljava/lang/String; = "title"

.field public static final PLAYER_PAUSE_ACTION:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.pause"

.field public static final PLAYER_PLAY_ACTION:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.play"

.field public static final PLAYER_PLAY_NEXT_ACTION:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.playnext"

.field public static final PLAYER_PLAY_PREVIOUS_ACTION:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.playprevious"

.field public static final PLAYER_PLAY_VIA_ACTION:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_VIA"

.field public static final PLAY_BY_MOOD_ACTION:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

.field public static final QUICK_LIST_DEFAULT_VALUE:Ljava/lang/String; = "Quick list"

.field public static final QUICK_LIST_MUSIC_PLUS_VALUE:Ljava/lang/String; = "FavoriteList#328795!432@1341"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method
