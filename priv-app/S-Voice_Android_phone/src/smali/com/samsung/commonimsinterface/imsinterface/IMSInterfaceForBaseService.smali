.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.super Ljava/lang/Object;
.source "IMSInterfaceForBaseService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

.field protected mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

.field protected mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 19
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .line 20
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mContext:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;-><init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 29
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mContext:Landroid/content/Context;

    .line 30
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    .line 32
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->startService()V

    .line 33
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onServiceConnected()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method protected onServiceDisconnected()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method protected startService()V
    .locals 5

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "startService()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    const-string/jumbo v1, "com.samsung.commonimsinterface.imsstub.common.IMSService"

    .line 59
    const-string/jumbo v2, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    .line 60
    const/4 v3, 0x1

    .line 61
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionListener:Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 58
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->startIMSService(Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    const-string/jumbo v1, "ims"

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->getSystemService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 64
    return-void
.end method
