.class public Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;
.super Ljava/lang/Object;
.source "CommonIMSInterface.java"


# static fields
.field public static final BEARER_MOBILE:I = 0x1

.field public static final BEARER_WIFI:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final TOKEN_SERVICE_CALL:I = 0x0

.field public static final TOKEN_SERVICE_GENERAL:I = 0x7

.field public static final TOKEN_SERVICE_MEDIA:I = 0x4

.field public static final TOKEN_SERVICE_PRESENCE_EAB:I = 0x1

.field public static final TOKEN_SERVICE_PRESENCE_TMO:I = 0x3

.field public static final TOKEN_SERVICE_PRESENCE_UCE:I = 0x2

.field public static final TOKEN_SERVICE_PRES_EAB:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_PRES_TMO:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_PRES_UCE:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TOKEN_SERVICE_SMS:I = 0x5

.field public static final TOKEN_SERVICE_SSCONFIG:I = 0x6

.field private static mIMSInterface:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    .line 19
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    .line 13
    return-void
.end method

.method private constructor <init>(ILandroid/content/Context;)V
    .locals 0
    .param p1, "token"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static declared-synchronized getAvailableBearer(ILandroid/content/Context;)I
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 140
    const-class v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string/jumbo v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "VoLTE CSC feature disabled."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :goto_0
    monitor-exit v2

    return v0

    .line 144
    :cond_0
    if-nez p1, :cond_1

    .line 145
    :try_start_1
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Invalid Argument."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 149
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->initialize(Landroid/content/Context;)V

    .line 151
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getAvailableBearer(I)I

    move-result v0

    .line 153
    .local v0, "bearer":I
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getAvailableBearer. token:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$BEARER_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(ILandroid/content/Context;)Ljava/lang/Object;
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 47
    const-class v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string/jumbo v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 48
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "VoLTE CSC feature disabled."

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :goto_0
    monitor-exit v1

    return-object v0

    .line 51
    :cond_0
    if-nez p1, :cond_1

    .line 52
    :try_start_1
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Invalid Argument."

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 55
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->initialize(Landroid/content/Context;)V

    .line 57
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isServiceTokenEnabled(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 58
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "No permission for creating token "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " Id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_2
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    aget-object v2, v2, p0

    if-nez v2, :cond_9

    .line 63
    packed-switch p0, :pswitch_data_0

    .line 109
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "No permission for creating token "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " Id:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :pswitch_0
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    .line 113
    :goto_1
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Interface for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " has been Created!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :goto_2
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    aget-object v0, v0, p0

    goto/16 :goto_0

    .line 68
    :cond_3
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 72
    :pswitch_1
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 73
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 75
    :cond_4
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 81
    :pswitch_2
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 82
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 84
    :cond_5
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresenceService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 88
    :pswitch_3
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 89
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto :goto_1

    .line 91
    :cond_6
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto :goto_1

    .line 95
    :pswitch_4
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 96
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 98
    :cond_7
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSMSService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSMSService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 102
    :pswitch_5
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 103
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 105
    :cond_8
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->mIMSInterface:[Ljava/lang/Object;

    new-instance v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;

    invoke-direct {v2, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForSSConfigService;-><init>(Landroid/content/Context;)V

    aput-object v2, v0, p0

    goto/16 :goto_1

    .line 115
    :cond_9
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Interface for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Already Exists!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method public static declared-synchronized isServiceAvailable(ILandroid/content/Context;)Z
    .locals 5
    .param p0, "token"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 122
    const-class v2, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string/jumbo v3, "CscFeature_IMS_EnableVoLTE"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "VoLTE CSC feature disabled."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :goto_0
    monitor-exit v2

    return v0

    .line 126
    :cond_0
    if-nez p1, :cond_1

    .line 127
    :try_start_1
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Invalid Argument."

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 122
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 131
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->initialize(Landroid/content/Context;)V

    .line 132
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isServiceTokenEnabled(I)Z

    move-result v0

    .line 134
    .local v0, "available":Z
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "isServiceAvailable. token:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
