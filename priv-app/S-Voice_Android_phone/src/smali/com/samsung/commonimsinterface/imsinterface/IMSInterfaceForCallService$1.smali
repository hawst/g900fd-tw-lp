.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService$1;
.super Landroid/content/BroadcastReceiver;
.source "IMSInterfaceForCallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->setPSBarringEventReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    .line 929
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 932
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 933
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "cmd"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 935
    .local v2, "psBarred":Ljava/lang/String;
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Received PSBarringEvent => "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    const-string/jumbo v3, "com.android.intent.action.PSBARRED_FOR_VOLTE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 937
    const-string/jumbo v3, "1"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 939
    :try_start_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;

    iget-object v3, v3, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForCallService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->endDialingOrAlertingCall()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 945
    :cond_0
    :goto_0
    return-void

    .line 940
    :catch_0
    move-exception v1

    .line 941
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
