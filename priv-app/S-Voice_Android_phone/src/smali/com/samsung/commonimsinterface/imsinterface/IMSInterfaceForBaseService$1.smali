.class Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;
.super Ljava/lang/Object;
.source "IMSInterfaceForBaseService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;


# direct methods
.method constructor <init>(Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 3

    .prologue
    .line 38
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onConnected()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    iget-object v1, v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSServiceConnectionManager:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    const-string/jumbo v2, "ims"

    invoke-virtual {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->getSystemService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 42
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->onServiceConnected()V

    .line 43
    return-void
.end method

.method public onDisconnected()V
    .locals 2

    .prologue
    .line 47
    # getter for: Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onDisconnected()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    .line 51
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService$1;->this$0:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;->onServiceDisconnected()V

    .line 52
    return-void
.end method
