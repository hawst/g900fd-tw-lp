.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;
.super Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;
.source "IMSInterfaceForMediaService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMedia;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mCameraSurface:Landroid/view/Surface;

.field private mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mFarEndSurface:Landroid/view/Surface;

.field private mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForBaseService;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 25
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 26
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 27
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 28
    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 39
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-direct {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    .line 40
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->registerListeners()V

    .line 41
    return-void
.end method

.method private registerListeners()V
    .locals 3

    .prologue
    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public captureSurfaceImage(ZI)V
    .locals 4
    .param p1, "isNearEnd"    # Z
    .param p2, "onGoingCallType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 678
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "captureSurfaceImage isNearEnd:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " onGoingCallType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->captureSurfaceImage(ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 687
    return-void

    .line 682
    :catch_0
    move-exception v0

    .line 683
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 684
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 685
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public closeCamera()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 362
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "closeCamera"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->closeCamera()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 371
    return-void

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 368
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public deRegisterForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 74
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "deRegisterForMediaStateChange() with listener : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 77
    return-void
.end method

.method public deinitSurface(Z)V
    .locals 4
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 691
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "deinitSurface isNearEnd:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->deinitSurface(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 700
    return-void

    .line 695
    :catch_0
    move-exception v0

    .line 696
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 697
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getBackCameraId()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 403
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getBackCameraId"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getBackCameraId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 415
    .local v1, "iBackCameraId":I
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getBackCameraId BackCameraId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return v1

    .line 409
    .end local v1    # "iBackCameraId":I
    :catch_0
    move-exception v0

    .line 410
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 411
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 412
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCameraDirection()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 533
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getCameraDirection"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const/4 v1, -0x1

    .line 538
    .local v1, "iDirection":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCameraDirection()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 545
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getCameraDirection Direction:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    return v1

    .line 539
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 541
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 542
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCameraParameters()Landroid/hardware/Camera$Parameters;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 381
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "getCameraParameters"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCameraPreviewSize(IZ)Landroid/hardware/Camera$Size;
    .locals 3
    .param p1, "targetSize"    # I
    .param p2, "isHeight"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 518
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getCameraPreviewSize targetSize:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isHeight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCameraState()Lcom/samsung/commonimsinterface/imscommon/CameraState;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 451
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v5, "getCameraState"

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const/4 v1, 0x0

    .line 456
    .local v1, "result":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getCameraState()I

    move-result v2

    .line 458
    .local v2, "state":I
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/CameraState;->values()[Lcom/samsung/commonimsinterface/imscommon/CameraState;

    move-result-object v5

    array-length v6, v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_0

    .line 470
    :goto_1
    sget-object v4, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "getCameraState state:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    return-object v1

    .line 458
    :cond_0
    :try_start_1
    aget-object v3, v5, v4

    .line 459
    .local v3, "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    invoke-virtual {v3}, Lcom/samsung/commonimsinterface/imscommon/CameraState;->ordinal()I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-ne v7, v2, :cond_1

    .line 460
    move-object v1, v3

    .line 461
    goto :goto_1

    .line 458
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 464
    .end local v2    # "state":I
    .end local v3    # "value":Lcom/samsung/commonimsinterface/imscommon/CameraState;
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v4, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 466
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 467
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v4, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getFrontCameraId()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 427
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getFrontCameraId"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getFrontCameraId()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 439
    .local v1, "iFrontCameraId":I
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getBackCameraId BackCameraId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return v1

    .line 433
    .end local v1    # "iFrontCameraId":I
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 435
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getNegotiatedFPS()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 185
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getNegotiatedFPS"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getNegotiatedFPS()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 197
    .local v1, "iFps":I
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getNegotiatedFPS FPS:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return v1

    .line 191
    .end local v1    # "iFps":I
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 193
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getNegotiatedHeight()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 141
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getNegotiatedHeight"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x0

    .line 146
    .local v1, "iHeight":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getNegotiatedHeight()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 153
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getNegotiatedHeight Height:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return v1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 149
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getNegotiatedWidth()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 163
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getNegotiatedWidth"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/4 v1, 0x0

    .line 168
    .local v1, "iWidth":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getNegotiatedWidth()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 175
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getNegotiatedWidth Width:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    return v1

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 171
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getNumberOfCameras()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 245
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "getNumberOfCameras"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v1, 0x0

    .line 250
    .local v1, "iNumberOfCameras":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->getNumberOfCameras()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 257
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "getNumberOfCameras iNumberOfCameras:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    return v1

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 253
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 254
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public holdVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 704
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "holdVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->holdVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 712
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 708
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 709
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 710
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isMediaReadyToReceivePreview()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 207
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "isMediaReadyToReceivePreview"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v1, 0x0

    .line 212
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->isMediaReadyToReceivePreview()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 219
    return v1

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 215
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public mediaDeInit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 100
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "mediaDeInit"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->mediaDeInit()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public mediaInit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "mediaInit"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->mediaInit()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected onServiceConnected()V
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "onServiceConnected()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->registerListeners()V

    .line 48
    return-void
.end method

.method protected onServiceDisconnected()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public openCamera(I)Z
    .locals 5
    .param p1, "cameraId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 271
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "openCamera cameraId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const/4 v1, 0x0

    .line 276
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->openCamera(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 283
    return v1

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 279
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 280
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public openCamera(II)Z
    .locals 5
    .param p1, "cameraId"    # I
    .param p2, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 295
    sget-object v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "openCamera cameraId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const/4 v1, 0x0

    .line 300
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v2, p1, p2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->openCameraEx(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 307
    return v1

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 303
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v2, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "registerForMediaStateChange() with listener : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSRemoteListenerList:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    .line 70
    return-void
.end method

.method public resetCameraID()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 639
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "resetCameraID"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->resetCameraID()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 648
    return-void

    .line 643
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 645
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 646
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public resumeVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 716
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "resumeVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->resumeVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 724
    return-void

    .line 719
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 721
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 722
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendLiveVideo()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 665
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "sendLiveVideo"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendLiveVideo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 674
    return-void

    .line 669
    :catch_0
    move-exception v0

    .line 670
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 671
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 672
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public sendStillImage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "imageFormat"    # I
    .param p3, "frameSize"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 652
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "sendStillImage filePath:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " imageFormat:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " frameSize:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->sendStillImage(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 661
    return-void

    .line 656
    :catch_0
    move-exception v0

    .line 657
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 658
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 659
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraDisplayOrientation()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 556
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setCameraDisplayOrientation"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCameraDisplayOrientation()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 566
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setCameraDisplayOrientation is not implemented."

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    return-void

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 562
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 563
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraDisplayOrientation(I)V
    .locals 3
    .param p1, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 571
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setCameraDisplayOrientation"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setCameraDisplayOrientationEx(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 581
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setCameraDisplayOrientation is not implemented."

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    return-void

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 577
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 578
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCameraParameters(Landroid/hardware/Camera$Parameters;)V
    .locals 3
    .param p1, "parameters"    # Landroid/hardware/Camera$Parameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 393
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setCameraParameters parameters:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public setDisplay(Landroid/graphics/SurfaceTexture;)V
    .locals 5
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 482
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setDisplay(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 486
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 487
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 488
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 491
    :cond_1
    if-eqz p1, :cond_2

    .line 492
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 494
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 497
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setDisplay(Landroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 503
    return-void

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 500
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setFarEndSurface(Landroid/graphics/SurfaceTexture;)V
    .locals 5
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "setFarEndSurface(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 118
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 119
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 122
    :cond_1
    if-eqz p1, :cond_2

    .line 123
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    .line 125
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mFarEndSurface:Landroid/view/Surface;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setFarEndSurface(Landroid/view/Surface;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 134
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setIsMediaReadyToReceivePreview(Z)V
    .locals 4
    .param p1, "flag"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 227
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setIsMediaReadyToReceivePreview flag:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->setIsMediaReadyToReceivePreview(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 236
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 233
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startCameraPreview(Landroid/graphics/SurfaceTexture;)V
    .locals 3
    .param p1, "surfaceTexture"    # Landroid/graphics/SurfaceTexture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 317
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "startCameraPreview(SurfaceTexture)"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    if-eqz v1, :cond_1

    .line 322
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 323
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 326
    :cond_1
    if-eqz p1, :cond_2

    .line 327
    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    .line 329
    :cond_2
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 332
    :cond_3
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mCameraSurface:Landroid/view/Surface;

    invoke-interface {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startCameraPreview(Landroid/view/Surface;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 338
    return-void

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 335
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startRender(Z)V
    .locals 3
    .param p1, "isNearEnd"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 603
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "startRender"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->startRender(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 612
    return-void

    .line 607
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 609
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 610
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public stopCameraPreview()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 346
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "stopCameraPreview"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->stopCameraPreview()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 355
    return-void

    .line 350
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 352
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public swapSurface()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 621
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "swapSurface"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->swapVideoSurface()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 630
    return-void

    .line 625
    :catch_0
    move-exception v0

    .line 626
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 627
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 628
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public switchCamera()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 590
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "switchCamera"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    :try_start_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForMediaService;->mIMSService:Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    invoke-interface {v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;->switchCamera()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 599
    return-void

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 596
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 597
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/IMSException;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
