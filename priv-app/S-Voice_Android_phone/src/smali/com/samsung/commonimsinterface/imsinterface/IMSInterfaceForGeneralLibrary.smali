.class public Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;
.super Ljava/lang/Object;
.source "IMSInterfaceForGeneralLibrary.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneral;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 229
    return-void
.end method

.method public deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 213
    return-void
.end method

.method public deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 221
    return-void
.end method

.method public deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 205
    return-void
.end method

.method public disableWiFiCalling()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public enableWiFiCalling()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public forceRestart()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public getFeatureMask()I
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "getFeatureMask()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->isRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIMSSettingValues([I)Landroid/util/SparseArray;
    .locals 1
    .param p1, "fields"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    const/4 v0, 0x0

    return-object v0
.end method

.method public getServiceStatus()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public handleActionsOverEpdg()V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public isDeregisteredToMobile()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteredToWiFi()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToMobile()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public isDeregisteringToWiFi()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public isDisablingMobileData()Z
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public isEnablingMobileData()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public isLTEVideoCallEnabled()Z
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public isRegistered()Z
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "isRegistered()"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->isRegisteredToMobile()Z

    move-result v0

    return v0
.end method

.method public isRegisteredToMobile()Z
    .locals 4

    .prologue
    .line 40
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "isRegisteredToMobile()"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string/jumbo v1, "persist.radio.ims.reg"

    const-string/jumbo v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 63
    .local v0, "isRegistered":Z
    :goto_0
    sget-object v1, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForGeneralLibrary;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "isRegisteredToMobile(): read from system property : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return v0

    .line 61
    .end local v0    # "isRegistered":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRegisteredToWiFi()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToMobile()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public isRegisteringToWiFi()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public isVoLTEFeatureEnabled()Z
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x0

    return v0
.end method

.method public manualDeregister()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public manualRegister()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 225
    return-void
.end method

.method public registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 209
    return-void
.end method

.method public registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 217
    return-void
.end method

.method public registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .param p2, "reqFields"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/commonimsinterface/imscommon/IMSException;
        }
    .end annotation

    .prologue
    .line 201
    return-void
.end method

.method public sendDeregister()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public sendDeregister(I)Z
    .locals 1
    .param p1, "cause"    # I

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public sendInitialRegister(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 162
    return-void
.end method

.method public setLTEVideoCallDisable()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public setLTEVideoCallEnable()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceID"    # Ljava/lang/String;
    .param p2, "featureTags"    # [Ljava/lang/String;

    .prologue
    .line 158
    return-void
.end method

.method public setThirdPartyMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 186
    return-void
.end method

.method public setVoLTEFeatureDisable()V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public setVoLTEFeatureEnable()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public updateIMSSettingValues(Landroid/util/SparseArray;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 195
    .local p1, "updateMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v0, 0x0

    return v0
.end method
