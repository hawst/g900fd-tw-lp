.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigUsVzw.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 7
    return-void
.end method


# virtual methods
.method public change8BitAsciiTo4BitDTMF()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public e911DialingTimerMode()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x2

    return v0
.end method

.method public endAllVideoCallsWhenDataDisabled()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public getDefaultServiceTokenValue(II)Z
    .locals 4
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 31
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 13
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    :pswitch_2
    move v0, v1

    .line 28
    goto :goto_0

    .line 19
    :pswitch_3
    const-string/jumbo v2, "serranoltevzw"

    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;->getProduct()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "kltevzw"

    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;->getProduct()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    .line 20
    goto :goto_0

    .line 11
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 13
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getEpdnDisconnectMode()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x2

    return v0
.end method

.method public ignoreNotifyMTCallEvent2NativeCallApp()Z
    .locals 2

    .prologue
    .line 57
    const-string/jumbo v0, "net.sip.vzthirdpartyapi"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public is3gppSMSSIPtoMDN()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public notifyHDCallStatus()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;->isServiceTokenEnabled(I)Z

    move-result v0

    return v0
.end method

.method public removeUriPrefixPlus()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public supportEmergencyCallBackMode()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method

.method public supportNWAYConferenceCallReason()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public supportTTYonIMS()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public supportTimerVZW()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public supportVirtualRegiFeatureMask()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public vzwOmaDM()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method
