.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigEu.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public getDefaultServiceTokenValue(II)Z
    .locals 3
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 31
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 14
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    :pswitch_2
    move v0, v1

    .line 28
    goto :goto_0

    .line 20
    :pswitch_3
    const-string/jumbo v2, "NEE"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "COV"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "TMN"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 21
    const-string/jumbo v2, "SWC"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "VIT"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "TEL"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 22
    const-string/jumbo v2, "SER"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "MOB"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "TMH"

    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    .line 23
    goto :goto_0

    .line 12
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 14
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public scaNumberAlwaysStartPlus()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public supportFakeUSSDResponse()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
