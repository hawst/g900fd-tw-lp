.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.super Ljava/lang/Object;
.source "IMSConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$BEARER_TYPE;,
        Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$E911_TIMER_MODE;,
        Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$EPDN_DISCONNECT_MODE;,
        Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$SERVICE_TYPE;,
        Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;
    }
.end annotation


# static fields
.field private static final DB_SERVICE_LIST_URI:Landroid/net/Uri;

.field private static final FIELD_SERVICE_NAME:Ljava/lang/String; = "mServiceName"

.field private static final FIELD_SPEC_TYPE:Ljava/lang/String; = "mSpecType"

.field private static final FIELD_SPEC_TYPE_IMS:Ljava/lang/String; = "IMS_PDN"

.field private static final FIELD_SPEC_TYPE_INTERNET:Ljava/lang/String; = "INTERNET_PDN"

.field private static final FIELD_SPEC_TYPE_WIFI:Ljava/lang/String; = "WIFI"

.field public static final IMS_SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.commonimsservice"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final LOOPBACK_ADAPTER_CALL:I = 0x4

.field private static final LOOPBACK_ADAPTER_GENERAL:I = 0x1

.field private static final LOOPBACK_ADAPTER_PRESENCE:I = 0x8

.field private static final LOOPBACK_ADAPTER_SMS:I = 0x10

.field private static final LOOPBACK_ADAPTER_SSCONFIG:I = 0x20

.field private static mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;


# instance fields
.field private mInitialized:Z

.field protected mServiceAdaptor:[Ljava/lang/String;

.field private mServiceTokenStatus:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/SparseBooleanArray;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    .line 25
    const-string/jumbo v0, "content://com.example.HiddenMenuContentProvider/ServiceList"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->DB_SERVICE_LIST_URI:Landroid/net/Uri;

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    .line 19
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    .line 35
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInitialized:Z

    .line 149
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
    .locals 4

    .prologue
    .line 152
    const-class v2, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    if-nez v1, :cond_0

    .line 154
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "create new instance"

    invoke-static {v1, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "csc":Ljava/lang/String;
    const-string/jumbo v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsAtt;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsAtt;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    .line 192
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 159
    :cond_1
    :try_start_1
    const-string/jumbo v1, "TMB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsTmo;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsTmo;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 161
    :cond_2
    :try_start_2
    const-string/jumbo v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsVzw;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto :goto_0

    .line 163
    :cond_3
    const-string/jumbo v1, "USC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 164
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsUsc;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsUsc;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto :goto_0

    .line 165
    :cond_4
    const-string/jumbo v1, "LGT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "LUC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "LUO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 166
    :cond_5
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrLgt;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrLgt;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto :goto_0

    .line 167
    :cond_6
    const-string/jumbo v1, "DTM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "DTR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 168
    const-string/jumbo v1, "VD2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "VDP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 169
    const-string/jumbo v1, "VIA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "VID"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "VIT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 170
    const-string/jumbo v1, "FTM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "OFR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 171
    const-string/jumbo v1, "EVR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "OUK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "TMH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 172
    :cond_7
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 173
    :cond_8
    const-string/jumbo v1, "COV"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "NEE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "TMN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "SWC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "TEL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "SER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "MOB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 174
    :cond_9
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigEu;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 175
    :cond_a
    const-string/jumbo v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string/jumbo v1, "KTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string/jumbo v1, "KTO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 176
    :cond_b
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrKt;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrKt;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 177
    :cond_c
    const-string/jumbo v1, "SKT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string/jumbo v1, "SKC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string/jumbo v1, "SKO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 178
    :cond_d
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrSkt;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrSkt;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 179
    :cond_e
    const-string/jumbo v1, "SIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string/jumbo v1, "STH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 180
    :cond_f
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigSea;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigSea;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 181
    :cond_10
    const-string/jumbo v1, "KDI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 182
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigJpKdi;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigJpKdi;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 183
    :cond_11
    const-string/jumbo v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 184
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigChCHM;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigChCHM;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 185
    :cond_12
    const-string/jumbo v1, "ZZH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 186
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigHk;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigHk;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 187
    :cond_13
    const-string/jumbo v1, "ZVV"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 188
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigBrVivo;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigBrVivo;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    goto/16 :goto_0

    .line 190
    :cond_14
    new-instance v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigDefault;

    invoke-direct {v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfigDefault;-><init>()V

    sput-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInstance:Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private initializeWithDefaultValue()V
    .locals 12

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v11, 0x1

    .line 318
    new-array v1, v7, [I

    fill-array-data v1, :array_0

    .line 322
    .local v1, "bearerType":[I
    const/16 v5, 0x8

    new-array v4, v5, [I

    .line 324
    aput v9, v4, v11

    .line 325
    const/16 v5, 0x8

    aput v5, v4, v7

    .line 326
    const/4 v5, 0x5

    aput v5, v4, v8

    .line 327
    aput v11, v4, v9

    const/4 v5, 0x5

    .line 328
    aput v7, v4, v5

    const/4 v5, 0x6

    .line 329
    aput v8, v4, v5

    const/4 v5, 0x7

    .line 330
    const/4 v7, 0x6

    aput v7, v4, v5

    .line 333
    .local v4, "tokenType":[I
    array-length v8, v1

    move v7, v6

    :goto_0
    if-lt v7, v8, :cond_0

    .line 344
    return-void

    .line 333
    :cond_0
    aget v0, v1, v7

    .line 334
    .local v0, "bearer":I
    new-instance v2, Landroid/util/SparseBooleanArray;

    invoke-direct {v2}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 336
    .local v2, "serviceStatus":Landroid/util/SparseBooleanArray;
    array-length v9, v4

    move v5, v6

    :goto_1
    if-lt v5, v9, :cond_2

    .line 339
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 340
    const/4 v5, 0x7

    invoke-virtual {v2, v5, v11}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 341
    iget-object v5, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v5, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 333
    :cond_1
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_0

    .line 336
    :cond_2
    aget v3, v4, v5

    .line 337
    .local v3, "token":I
    invoke-virtual {p0, v0, v3}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getDefaultServiceTokenValue(II)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v2, v3, v11}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 336
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 318
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method public change8BitAsciiTo4BitDTMF()Z
    .locals 1

    .prologue
    .line 487
    const/4 v0, 0x0

    return v0
.end method

.method public conferenceCallNotifyByUser()Z
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x0

    return v0
.end method

.method public containCliNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "CLI"    # Ljava/lang/String;
    .param p2, "peerUri"    # Ljava/lang/String;

    .prologue
    .line 479
    const/4 v0, 0x0

    return v0
.end method

.method public disableDataOnInternationalRoamingOff()Z
    .locals 1

    .prologue
    .line 624
    const/4 v0, 0x0

    return v0
.end method

.method public e911DialingTimerMode()I
    .locals 1

    .prologue
    .line 572
    const/4 v0, 0x0

    return v0
.end method

.method public endAllVideoCallsWhenDataDisabled()Z
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x0

    return v0
.end method

.method public getAvailableBearer(I)I
    .locals 5
    .param p1, "serviceTokenType"    # I

    .prologue
    .line 393
    const/4 v1, 0x0

    .line 394
    .local v1, "avaialbleBearer":I
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 401
    return v1

    .line 395
    :cond_0
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseBooleanArray;

    .line 396
    .local v0, "array":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 397
    iget-object v4, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    or-int/2addr v1, v4

    .line 394
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getDefaultCameraResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 528
    const-string/jumbo v0, "VGA"

    return-object v0
.end method

.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    .line 405
    const/4 v0, 0x0

    return v0
.end method

.method public getEpdnDisconnectMode()I
    .locals 1

    .prologue
    .line 628
    const/4 v0, 0x0

    return v0
.end method

.method public getPresenceToken()I
    .locals 4

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 536
    invoke-virtual {p0, v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isServiceTokenEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 544
    :goto_0
    return v0

    .line 538
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isServiceTokenEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 539
    goto :goto_0

    .line 540
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isServiceTokenEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 541
    goto :goto_0

    .line 544
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getProduct()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447
    const-string/jumbo v0, "ro.build.product"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getServiceAdaptor(I)Ljava/lang/String;
    .locals 3
    .param p1, "serviceType"    # I

    .prologue
    .line 374
    monitor-enter p0

    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-lt p1, v0, :cond_1

    .line 375
    :cond_0
    :try_start_0
    sget-object v0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "unexpected serviceType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    const/4 v0, 0x0

    .line 378
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    aget-object v0, v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getServiceList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    .local v1, "serviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    const/4 v2, 0x7

    if-lt v0, v2, :cond_0

    .line 370
    monitor-exit p0

    return-object v1

    .line 366
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    .line 367
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 363
    .end local v0    # "index":I
    .end local v1    # "serviceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public ignoreNotifyMTCallEvent2NativeCallApp()Z
    .locals 1

    .prologue
    .line 524
    const/4 v0, 0x0

    return v0
.end method

.method public ignorePackageCheck()Z
    .locals 2

    .prologue
    .line 459
    const-string/jumbo v0, "persist.sys.ims.no_allow_check"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ignoreRegisterStateForMakeCall()Z
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized initialize(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "initialize :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInitialized:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-boolean v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 315
    :goto_0
    monitor-exit p0

    return-void

    .line 201
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mInitialized:Z

    .line 204
    invoke-static {}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->setLogLevel()V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isVoLTETypeHybridQCT_QIMS()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 207
    new-instance v11, Landroid/util/SparseBooleanArray;

    invoke-direct {v11}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 208
    .local v11, "serviceStatus":Landroid/util/SparseBooleanArray;
    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 209
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isCallServiceAvailableForQCT_QIMS()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 211
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isSMSServiceAvailableForQCT_QIMS()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 212
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 213
    :cond_2
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "put mServiceAdaptor"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "Qualcomm"

    aput-object v3, v1, v2

    .line 217
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x1

    const-string/jumbo v3, "Qualcomm"

    aput-object v3, v1, v2

    .line 219
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x3

    const-string/jumbo v3, "Qualcomm"

    aput-object v3, v1, v2

    .line 220
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x4

    const-string/jumbo v3, "Qualcomm"

    aput-object v3, v1, v2

    .line 221
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x5

    const-string/jumbo v3, "Qualcomm"

    aput-object v3, v1, v2

    .line 223
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "end - put mServiceAdaptor"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 196
    .end local v11    # "serviceStatus":Landroid/util/SparseBooleanArray;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 225
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 227
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->DB_SERVICE_LIST_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 229
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_b

    .line 230
    :cond_4
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "no service list. Use default value."

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->initializeWithDefaultValue()V

    .line 281
    :goto_1
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "end - get data from db."

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    if-eqz v7, :cond_5

    .line 283
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 285
    :cond_5
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "db - close"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "put mServiceAdaptor"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "Samsung"

    aput-object v3, v1, v2

    .line 289
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x1

    const-string/jumbo v3, "Samsung"

    aput-object v3, v1, v2

    .line 291
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x3

    const-string/jumbo v3, "Samsung"

    aput-object v3, v1, v2

    .line 292
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x4

    const-string/jumbo v3, "Samsung"

    aput-object v3, v1, v2

    .line 293
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x5

    const-string/jumbo v3, "Samsung"

    aput-object v3, v1, v2

    .line 296
    const-string/jumbo v1, "persist.sys.ims.cii_loopback"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 297
    .local v8, "loopbackAdapterFlag":I
    and-int/lit8 v1, v8, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 298
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "Loopback"

    aput-object v3, v1, v2

    .line 300
    :cond_6
    and-int/lit8 v1, v8, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_7

    .line 301
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x1

    const-string/jumbo v3, "Loopback"

    aput-object v3, v1, v2

    .line 303
    :cond_7
    and-int/lit8 v1, v8, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_8

    .line 304
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x3

    const-string/jumbo v3, "Loopback"

    aput-object v3, v1, v2

    .line 306
    :cond_8
    and-int/lit8 v1, v8, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_9

    .line 307
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x4

    const-string/jumbo v3, "Loopback"

    aput-object v3, v1, v2

    .line 309
    :cond_9
    and-int/lit8 v1, v8, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_a

    .line 310
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceAdaptor:[Ljava/lang/String;

    const/4 v2, 0x5

    const-string/jumbo v3, "Loopback"

    aput-object v3, v1, v2

    .line 313
    :cond_a
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "end - put mServiceAdaptor"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 233
    .end local v8    # "loopbackAdapterFlag":I
    :cond_b
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 235
    :cond_c
    const-string/jumbo v1, "mServiceName"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 236
    .local v12, "services":Ljava/lang/String;
    const-string/jumbo v1, "mSpecType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 237
    .local v9, "pdn":Ljava/lang/String;
    sget-object v1, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "pdn:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " services:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-static {v9}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$BEARER_TYPE;->mapToToken(Ljava/lang/String;)I

    move-result v6

    .line 239
    .local v6, "bearType":I
    if-nez v6, :cond_e

    .line 279
    :cond_d
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    .line 234
    if-nez v1, :cond_c

    goto/16 :goto_1

    .line 242
    :cond_e
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/util/SparseBooleanArray;

    .line 244
    .restart local v11    # "serviceStatus":Landroid/util/SparseBooleanArray;
    if-nez v11, :cond_f

    .line 245
    new-instance v11, Landroid/util/SparseBooleanArray;

    .end local v11    # "serviceStatus":Landroid/util/SparseBooleanArray;
    invoke-direct {v11}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 248
    .restart local v11    # "serviceStatus":Landroid/util/SparseBooleanArray;
    :cond_f
    const-string/jumbo v1, ","

    invoke-virtual {v12, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_3
    if-lt v1, v3, :cond_10

    .line 275
    invoke-virtual {v11}, Landroid/util/SparseBooleanArray;->size()I

    move-result v1

    if-lez v1, :cond_d

    .line 276
    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 277
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v1, v6, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 248
    :cond_10
    aget-object v10, v2, v1

    .line 249
    .local v10, "service":Ljava/lang/String;
    const-string/jumbo v4, "call"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 250
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 248
    :cond_11
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 252
    :cond_12
    const-string/jumbo v4, "media"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 253
    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 255
    :cond_13
    const-string/jumbo v4, "telephony"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 256
    const/16 v4, 0x8

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 258
    :cond_14
    const-string/jumbo v4, "sms"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 259
    const/4 v4, 0x5

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 261
    :cond_15
    const-string/jumbo v4, "presence"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 262
    const-string/jumbo v4, "VZW"

    invoke-virtual {p0, v4}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 263
    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 264
    :cond_16
    const-string/jumbo v4, "ATT"

    invoke-virtual {p0, v4}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 265
    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 266
    :cond_17
    const-string/jumbo v4, "TMB"

    invoke-virtual {p0, v4}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isSalesCodeEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 267
    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_4

    .line 270
    :cond_18
    const-string/jumbo v4, "ssconfig"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 271
    const/4 v4, 0x6

    const/4 v5, 0x1

    invoke-virtual {v11, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method public is3gppSMSSIPtoMDN()Z
    .locals 1

    .prologue
    .line 491
    const/4 v0, 0x0

    return v0
.end method

.method public isCallLoggingEnabled()Z
    .locals 2

    .prologue
    .line 616
    const-string/jumbo v0, "persist.sys.ims.enable_call_log"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isCallServiceAvailableForQCT_QIMS()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public isConferenceCall(ILcom/sec/ims/android/internal/IIMSParams;)Z
    .locals 1
    .param p1, "subType"    # I
    .param p2, "param"    # Lcom/sec/ims/android/internal/IIMSParams;

    .prologue
    .line 532
    const/4 v0, 0x0

    return v0
.end method

.method public isSMSServiceAvailableForQCT_QIMS()Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    return v0
.end method

.method public isSalesCodeEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 443
    const-string/jumbo v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isServiceTokenEnabled(I)Z
    .locals 4
    .param p1, "serviceTokenType"    # I

    .prologue
    .line 383
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 389
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 384
    :cond_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->mServiceTokenStatus:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseBooleanArray;

    .line 385
    .local v0, "array":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 386
    const/4 v3, 0x1

    goto :goto_1

    .line 383
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isTestBinding(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 451
    const-string/jumbo v1, "persist.sys.ims.stub"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "com.samsung.commonimsservice"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    const/4 v0, 0x1

    .line 455
    :cond_0
    return v0
.end method

.method public isVoLTETypeAPVTOnly()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 417
    const-string/jumbo v0, "IMS_AP_VTONLY"

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isVoLTETypeHybridCMC()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 421
    const-string/jumbo v2, "IMS_HYBRID_QCT_QIMS"

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 426
    :cond_0
    :goto_0
    return v1

    .line 424
    :cond_1
    const-string/jumbo v2, "ro.baseband"

    const-string/jumbo v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 426
    .local v0, "baseband":Ljava/lang/String;
    const-string/jumbo v2, "msm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "mdm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isVoLTETypeHybridQCT()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 430
    const-string/jumbo v2, "IMS_HYBRID_QCT_QIMS"

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 435
    :cond_0
    :goto_0
    return v1

    .line 433
    :cond_1
    const-string/jumbo v2, "ro.baseband"

    const-string/jumbo v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 435
    .local v0, "baseband":Ljava/lang/String;
    const-string/jumbo v2, "msm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "mdm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isVoLTETypeHybridQCT_QIMS()Z
    .locals 2

    .prologue
    .line 439
    const-string/jumbo v0, "IMS_HYBRID_QCT_QIMS"

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public lgtVtConferenceCall()Z
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x0

    return v0
.end method

.method public notifyHDCallStatus()Z
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x0

    return v0
.end method

.method public removeUriPrefixPlus()Z
    .locals 1

    .prologue
    .line 632
    const/4 v0, 0x0

    return v0
.end method

.method public scaNumberAlwaysStartPlus()Z
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public sktVtConferenceCall()Z
    .locals 1

    .prologue
    .line 512
    const/4 v0, 0x0

    return v0
.end method

.method public supportDocumentCacheTimeout()Z
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x0

    return v0
.end method

.method public supportEmergencyCallBackMode()Z
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    return v0
.end method

.method public supportFakeDeregisterResponse()Z
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    return v0
.end method

.method public supportFakeUSSDResponse()Z
    .locals 1

    .prologue
    .line 580
    const/4 v0, 0x0

    return v0
.end method

.method public supportIMSPDNChangeOnINETPDNChange()Z
    .locals 1

    .prologue
    .line 592
    const/4 v0, 0x0

    return v0
.end method

.method public supportNWAYConferenceCallReason()Z
    .locals 1

    .prologue
    .line 596
    const/4 v0, 0x0

    return v0
.end method

.method public supportNoHandleError()Z
    .locals 1

    .prologue
    .line 612
    const/4 v0, 0x0

    return v0
.end method

.method public supportPDNChangeOnForceRestart()Z
    .locals 1

    .prologue
    .line 604
    const/4 v0, 0x1

    return v0
.end method

.method public supportPSBarred()Z
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    return v0
.end method

.method public supportPassDeregisterRequestOnDeregisteredState()Z
    .locals 1

    .prologue
    .line 588
    const/4 v0, 0x0

    return v0
.end method

.method public supportPendingRequestListForMobile()Z
    .locals 1

    .prologue
    .line 560
    const/4 v0, 0x1

    return v0
.end method

.method public supportPendingRequestListForWiFi()Z
    .locals 1

    .prologue
    .line 564
    const/4 v0, 0x0

    return v0
.end method

.method public supportTTYonIMS()Z
    .locals 1

    .prologue
    .line 568
    const/4 v0, 0x0

    return v0
.end method

.method public supportTimerVZW()Z
    .locals 1

    .prologue
    .line 608
    const/4 v0, 0x0

    return v0
.end method

.method public supportVirtualRegiFeatureMask()Z
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x0

    return v0
.end method

.method public supportWFCSubscribe()Z
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x0

    return v0
.end method

.method public useAPCSService()Z
    .locals 1

    .prologue
    .line 548
    const/4 v0, 0x0

    return v0
.end method

.method public useEfPSISMSC()Z
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    return v0
.end method

.method public useHardCodedValueInLTEto3GTransition()Z
    .locals 1

    .prologue
    .line 576
    const/4 v0, 0x0

    return v0
.end method

.method public vzwOmaDM()Z
    .locals 1

    .prologue
    .line 471
    const/4 v0, 0x1

    return v0
.end method
