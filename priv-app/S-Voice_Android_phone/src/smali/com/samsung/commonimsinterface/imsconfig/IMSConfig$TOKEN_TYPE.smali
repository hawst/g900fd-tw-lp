.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$TOKEN_TYPE;
.super Ljava/lang/Object;
.source "IMSConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TOKEN_TYPE"
.end annotation


# static fields
.field public static final CALL:I = 0x0

.field public static final GENERAL:I = 0x7

.field public static final MAX:I = 0x9

.field public static final MEDIA:I = 0x4

.field public static final PRESENCE_EAB:I = 0x1

.field public static final PRESENCE_TMO:I = 0x3

.field public static final PRESENCE_UCE:I = 0x2

.field public static final SMS:I = 0x5

.field public static final SSCONFIG:I = 0x6

.field public static final TELEPHONY:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "token"    # I

    .prologue
    .line 83
    packed-switch p0, :pswitch_data_0

    .line 93
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 84
    :pswitch_0
    const-string/jumbo v0, "TOKEN_SERVICE_GENERAL"

    goto :goto_0

    .line 85
    :pswitch_1
    const-string/jumbo v0, "TOKEN_SERVICE_CALL"

    goto :goto_0

    .line 86
    :pswitch_2
    const-string/jumbo v0, "TOKEN_SERVICE_PRESENCE_EAB"

    goto :goto_0

    .line 87
    :pswitch_3
    const-string/jumbo v0, "TOKEN_SERVICE_PRESENCE_UCE"

    goto :goto_0

    .line 88
    :pswitch_4
    const-string/jumbo v0, "TOKEN_SERVICE_PRESENCE_TMO"

    goto :goto_0

    .line 89
    :pswitch_5
    const-string/jumbo v0, "TOKEN_SERVICE_MEDIA"

    goto :goto_0

    .line 90
    :pswitch_6
    const-string/jumbo v0, "TOKEN_SERVICE_SMS"

    goto :goto_0

    .line 91
    :pswitch_7
    const-string/jumbo v0, "TOKEN_SERVICE_SSCONFIG"

    goto :goto_0

    .line 92
    :pswitch_8
    const-string/jumbo v0, "TOKEN_SERVICE_TELEPHONY"

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
