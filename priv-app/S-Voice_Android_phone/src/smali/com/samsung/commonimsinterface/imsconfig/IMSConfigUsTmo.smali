.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsTmo;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigUsTmo.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public conferenceCallNotifyByUser()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public containCliNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "CLI"    # Ljava/lang/String;
    .param p2, "peerUri"    # Ljava/lang/String;

    .prologue
    .line 29
    const-string/jumbo v0, "*31#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "*82"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const-string/jumbo v0, "#31#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "*67"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    .line 29
    goto :goto_0
.end method

.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x0

    .line 12
    packed-switch p1, :pswitch_data_0

    .line 23
    :goto_0
    return v0

    .line 15
    :pswitch_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 18
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 12
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 15
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public supportFakeDeregisterResponse()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method public supportIMSPDNChangeOnINETPDNChange()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public supportNoHandleError()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public supportPassDeregisterRequestOnDeregisteredState()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public supportPendingRequestListForWiFi()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public supportWFCSubscribe()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method
