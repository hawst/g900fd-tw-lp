.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$E911_TIMER_MODE;
.super Ljava/lang/Object;
.source "IMSConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "E911_TIMER_MODE"
.end annotation


# static fields
.field public static final UNTIL_100TRYING:I = 0x1

.field public static final UNTIL_200OK:I = 0x2

.field public static final UNTIL_EPDNUP:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "timerMode"    # I

    .prologue
    .line 129
    packed-switch p0, :pswitch_data_0

    .line 133
    const-string/jumbo v0, "[E911 TimerMode]no E911 Timer Mode"

    :goto_0
    return-object v0

    .line 130
    :pswitch_0
    const-string/jumbo v0, "[E911 TimerMode]Until EPDN-UP"

    goto :goto_0

    .line 131
    :pswitch_1
    const-string/jumbo v0, "[E911 TimerMode]Until 100 Trying"

    goto :goto_0

    .line 132
    :pswitch_2
    const-string/jumbo v0, "[E911 TimerMode]Until 200 OK"

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
