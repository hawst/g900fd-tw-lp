.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfig$SERVICE_TYPE;
.super Ljava/lang/Object;
.source "IMSConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SERVICE_TYPE"
.end annotation


# static fields
.field public static final CALL:I = 0x1

.field public static final GENERAL:I = 0x0

.field public static final MAX:I = 0x7

.field public static final NOACCESS:I = 0x6

.field public static final PRESENCE:I = 0x3

.field public static final SMS:I = 0x4

.field public static final SSCONFIG:I = 0x5

.field public static final TELEPHONY:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "serviceType"    # I

    .prologue
    .line 110
    packed-switch p0, :pswitch_data_0

    .line 118
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 111
    :pswitch_0
    const-string/jumbo v0, "General"

    goto :goto_0

    .line 112
    :pswitch_1
    const-string/jumbo v0, "Call"

    goto :goto_0

    .line 113
    :pswitch_2
    const-string/jumbo v0, "Telephony"

    goto :goto_0

    .line 114
    :pswitch_3
    const-string/jumbo v0, "Presence"

    goto :goto_0

    .line 115
    :pswitch_4
    const-string/jumbo v0, "SMS"

    goto :goto_0

    .line 116
    :pswitch_5
    const-string/jumbo v0, "SSConfig"

    goto :goto_0

    .line 117
    :pswitch_6
    const-string/jumbo v0, "NoAccess"

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
