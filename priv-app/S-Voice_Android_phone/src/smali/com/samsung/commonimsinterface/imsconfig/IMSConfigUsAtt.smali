.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigUsAtt;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigUsAtt.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 7
    return-void
.end method


# virtual methods
.method public containCliNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "CLI"    # Ljava/lang/String;
    .param p2, "peerUri"    # Ljava/lang/String;

    .prologue
    .line 28
    const-string/jumbo v0, "*31#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "#31#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public disableDataOnInternationalRoamingOff()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public getDefaultCameraResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string/jumbo v0, "QVGA"

    return-object v0
.end method

.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x0

    .line 11
    packed-switch p1, :pswitch_data_0

    .line 22
    :goto_0
    return v0

    .line 13
    :pswitch_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 17
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 13
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method public isCallServiceAvailableForQCT_QIMS()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public isSMSServiceAvailableForQCT_QIMS()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public supportDocumentCacheTimeout()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public useEfPSISMSC()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method
