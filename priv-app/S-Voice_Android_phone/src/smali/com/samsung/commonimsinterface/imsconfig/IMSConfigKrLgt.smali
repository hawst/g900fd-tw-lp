.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigKrLgt;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigKrLgt.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public containCliNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "CLI"    # Ljava/lang/String;
    .param p2, "peerUri"    # Ljava/lang/String;

    .prologue
    .line 31
    const-string/jumbo v0, "#31#"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "*31#"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v0, "*23#"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x0

    .line 14
    packed-switch p1, :pswitch_data_0

    .line 25
    :goto_0
    return v0

    .line 16
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 20
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 14
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 16
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public ignoreRegisterStateForMakeCall()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public isConferenceCall(ILcom/sec/ims/android/internal/IIMSParams;)Z
    .locals 2
    .param p1, "subType"    # I
    .param p2, "param"    # Lcom/sec/ims/android/internal/IIMSParams;

    .prologue
    .line 50
    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    .line 51
    const-string/jumbo v0, "1"

    invoke-virtual {p2}, Lcom/sec/ims/android/internal/IIMSParams;->getIsFocus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    :cond_0
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lgtVtConferenceCall()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public supportPSBarred()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method public useHardCodedValueInLTEto3GTransition()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method
