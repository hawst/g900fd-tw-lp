.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigChCHM;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigChCHM.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 7
    return-void
.end method


# virtual methods
.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x0

    .line 11
    packed-switch p1, :pswitch_data_0

    .line 24
    :goto_0
    return v0

    .line 13
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 18
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 11
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 13
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public scaNumberAlwaysStartPlus()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method
