.class public Lcom/samsung/commonimsinterface/imsconfig/IMSConfigSea;
.super Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;
.source "IMSConfigSea.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;-><init>()V

    .line 9
    return-void
.end method


# virtual methods
.method public getDefaultServiceTokenValue(II)Z
    .locals 1
    .param p1, "bearerType"    # I
    .param p2, "serviceTokenType"    # I

    .prologue
    const/4 v0, 0x0

    .line 13
    packed-switch p1, :pswitch_data_0

    .line 24
    :goto_0
    return v0

    .line 15
    :pswitch_0
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 19
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 13
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 15
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method public scaNumberAlwaysStartPlus()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method
