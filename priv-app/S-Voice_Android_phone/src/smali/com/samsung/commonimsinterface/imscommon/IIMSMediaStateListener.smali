.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSMediaStateListener;
.super Ljava/lang/Object;
.source "IIMSMediaStateListener.java"


# static fields
.field public static final CAPTURE_TYPE_FAR_END:I = 0x1

.field public static final CAPTURE_TYPE_NEAR_END:I


# virtual methods
.method public abstract onCameraEvent(IZ)V
.end method

.method public abstract onCameraFirstFrameReady(I)V
.end method

.method public abstract onCameraStartFailure(I)V
.end method

.method public abstract onCaptureFailure(IZ)V
.end method

.method public abstract onCaptureSuccess(IZLjava/lang/String;)V
.end method

.method public abstract onVideoAvailable(II)V
.end method

.method public abstract onVideoHeld(I)V
.end method

.method public abstract onVideoHoldRequest(I)V
.end method

.method public abstract onVideoOrientation(II)V
.end method

.method public abstract onVideoResumeRequest(I)V
.end method

.method public abstract onVideoResumed(I)V
.end method
