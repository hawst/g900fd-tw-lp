.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$ERROR_CODE;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ERROR_CODE"
.end annotation


# static fields
.field public static final ADDRESS_INCOMPLETE:I = 0x1e4

.field public static final ALTERNATIVE_SERVICES:I = 0x17c

.field public static final AMBIGUOUS:I = 0x1e5

.field public static final ANONYMITY_DISALLOWED:I = 0x1b1

.field public static final BAD_EVENT:I = 0x1e9

.field public static final BAD_EXTENSION:I = 0x1a4

.field public static final BAD_GATEWAY:I = 0x1f6

.field public static final BAD_IDENTITY_INFO:I = 0x1b4

.field public static final BAD_LOCATION_INFORMATION:I = 0x1a8

.field public static final BAD_REQUEST:I = 0x190

.field public static final BUSY_EVERYWHERE:I = 0x258

.field public static final BUSY_HERE:I = 0x1e6

.field public static final CALL_5XX_RESPONSE:I = 0x899

.field public static final CALL_END_CALL_NW_HANDOVER:I = 0x453

.field public static final CALL_FAILED:I = 0x8fd

.field public static final CALL_FORBIDDEN_RSN_EXPIRED:I = 0x8fe

.field public static final CALL_FORBIDDEN_RSN_GROUP_CALL_SERVICE_UNAVAILABLE:I = 0x8ff

.field public static final CALL_FORBIDDEN_RSN_OUTGOING_CALLS_IMPOSSIBLE:I = 0x901

.field public static final CALL_FORBIDDEN_RSN_TEMPORARY_DISABILITY:I = 0x900

.field public static final CALL_HOLD_FAILED:I = 0x457

.field public static final CALL_INVITE_TIMEOUT:I = 0x45a

.field public static final CALL_NOT_ACCEPTABLE_DIVERT:I = 0x835

.field public static final CALL_REJECT_REASON_USR_BUSY_CS_CALL:I = 0x454

.field public static final CALL_RESUME_FAILED:I = 0x458

.field public static final CALL_SESSION_ABORT:I = 0x44d

.field public static final CALL_SESSION_TERMINATED:I = 0x44e

.field public static final CALL_SESSION_TIMEOUT:I = 0x44f

.field public static final CALL_STATUS_CONF_ADD_USER_TO_SESSION_FAILURE:I = 0x451

.field public static final CALL_STATUS_CONF_REMOVE_USER_FROM_SESSION_FAILURE:I = 0x452

.field public static final CALL_STATUS_CONF_START_SESSION_FAILURE:I = 0x450

.field public static final CALL_SWITCH_FAILURE:I = 0x455

.field public static final CALL_SWITCH_REJECTED:I = 0x456

.field public static final CALL_TEMP_UNAVAILABLE_415_CAUSE:I = 0x459

.field public static final CALL_TEMP_UNAVAILABLE_WITH_380_CAUSE:I = 0x89a

.field public static final CALL_TEMP_UNAVAILABLE_WITH_415_CAUSE:I = 0x89b

.field public static final CALL_TEMP_UNAVAILABLE_WITH_CAUSE:I = 0x2718

.field public static final CALL_TRANSACTION_DOES_NOT_EXIST:I = 0x1e1

.field public static final CANDIDATES_FOR_REMOVAL:I = 0x2710

.field public static final CLIENT_ERROR:I = 0x3e9

.field public static final CONDITIONAL_REQUEST_FAILED:I = 0x19c

.field public static final CONFLICT:I = 0x199

.field public static final CONGESTION:I = 0x2712

.field public static final CONSENT_NEEDED:I = 0x1d6

.field public static final CROSS_DOMAIN_AUTHENTICATION:I = 0x70a

.field public static final DATA_CONNECTION_LOST:I = 0x6a5

.field public static final DECLINE:I = 0x25b

.field public static final DEREG_SUCCEEDED:I = 0x644

.field public static final DNS_FAILURE_HOST:I = 0x5df

.field public static final DNS_FAILURE_NAPTR:I = 0x5e0

.field public static final DNS_FAILURE_SVC:I = 0x5e1

.field public static final DNS_QUERY_RETRY_FAILED:I = 0x963

.field public static final DNS_QUERY_RETRY_START:I = 0x962

.field public static final DOES_NOT_EXIST_ANYWHERE:I = 0x25c

.field public static final ERROR:I = 0x2716

.field public static final EXTENSION_REQUIRED:I = 0x1a5

.field public static final FAILED_TO_GO_READY:I = 0x709

.field public static final FIRST_HOP_LACKS_OUTBOUND_SUPPORT:I = 0x1b7

.field public static final FLOW_FAILED:I = 0x1ae

.field public static final FORBIDDEN:I = 0x193

.field public static final GONE:I = 0x19a

.field public static final INTERVAL_TOO_BRIEF:I = 0x1a7

.field public static final INVALID_CREDENTIALS:I = 0x2711

.field public static final INVALID_IDENTITY_HEADER:I = 0x1b6

.field public static final IN_PROGRESS:I = 0x2715

.field public static final LENTH_REQUIRED:I = 0x19b

.field public static final LOOP_DETECTED:I = 0x1e2

.field public static final MESSAGE_TOO_LARGE:I = 0x201

.field public static final METHOD_NOT_ALLOWED:I = 0x195

.field public static final MOVED_PERMANENTLY:I = 0x12d

.field public static final MOVED_TEMPORARILY:I = 0x12e

.field public static final MULTIPLE_CHOICES:I = 0x17c

.field public static final NETWORK_UNREACHABLE:I = 0x836

.field public static final NON_STANDARD_ERROR_CODE_BASE:I = 0x3e8

.field public static final NON_STANDARD_ERROR_CODE_BASE_CALL:I = 0x44c

.field public static final NON_STANDARD_ERROR_CODE_BASE_ETC:I = 0x708

.field public static final NON_STANDARD_ERROR_CODE_BASE_KT:I = 0x898

.field public static final NON_STANDARD_ERROR_CODE_BASE_LGU:I = 0x8fc

.field public static final NON_STANDARD_ERROR_CODE_BASE_MEDIA:I = 0x578

.field public static final NON_STANDARD_ERROR_CODE_BASE_NETWORK:I = 0x6a4

.field public static final NON_STANDARD_ERROR_CODE_BASE_PPP:I = 0x514

.field public static final NON_STANDARD_ERROR_CODE_BASE_QOS:I = 0x4b0

.field public static final NON_STANDARD_ERROR_CODE_BASE_REGI:I = 0x640

.field public static final NON_STANDARD_ERROR_CODE_BASE_SEVER:I = 0x5dc

.field public static final NON_STANDARD_ERROR_CODE_BASE_SKT:I = 0x834

.field public static final NON_STANDARD_ERROR_CODE_BASE_TMO:I = 0x960

.field public static final NON_STANDARD_ERROR_CODE_BASE_VZW:I = 0x9c4

.field public static final NOTACCEPTABLE_AUTO_DIVERT:I = 0x2717

.field public static final NOT_ACCEPTABLE:I = 0x196

.field public static final NOT_ACCEPTABLE2:I = 0x25e

.field public static final NOT_ACCEPTABLE_HERE:I = 0x1e8

.field public static final NOT_FOUND:I = 0x194

.field public static final NOT_IMPLEMENTED:I = 0x1f5

.field public static final NO_ERROR:I = 0x3e8

.field public static final OK:I = 0xc8

.field public static final PAYMENT_REQUIRED:I = 0x192

.field public static final PPP_OPEN_FAILURE:I = 0x516

.field public static final PPP_STATUS_CLOSE_EVENT:I = 0x515

.field public static final PRECONDITION_FAILURE:I = 0x244

.field public static final PROVIDE_REFERRER_IDENTITY:I = 0x1ad

.field public static final PROXY_AUTHENTICATION_REQUIRED:I = 0x197

.field public static final QOS_FAILURE:I = 0x4b1

.field public static final QOS_INCALL_SUSPEND:I = 0x4b3

.field public static final QOS_INCALL_UNAWARE:I = 0x4b4

.field public static final QOS_NW_UNAWARE:I = 0x4b2

.field public static final REG_NOT_SUBSCRIBED:I = 0x969

.field public static final REG_NOT_SUBSCRIBED_NON_403:I = 0x96a

.field public static final REG_NOT_SUBSCRIBED_REASON:I = 0x96b

.field public static final REG_REQ_FAILED:I = 0x642

.field public static final REG_RETRY_FAILED:I = 0x965

.field public static final REG_RETRY_START:I = 0x964

.field public static final REG_SSL_CERTIFICATE_FAILURE:I = 0x961

.field public static final REG_SUBSCRIBED:I = 0x968

.field public static final REG_TIMEOUT:I = 0x643

.field public static final REQUEST_ENTITY_TOO_LARGE:I = 0x19d

.field public static final REQUEST_PENDING:I = 0x1eb

.field public static final REQUEST_TERMINATED:I = 0x1e7

.field public static final REQUEST_TIMEOUT:I = 0x198

.field public static final REQUEST_URI_TOO_LONG:I = 0x19e

.field public static final RTP_TIME_OUT:I = 0x579

.field public static final SECURITY_AGREEMENT_REQUIRED:I = 0x1ee

.field public static final SERVER_ERROR:I = 0x5de

.field public static final SERVER_INTERNAL_ERROR:I = 0x1f4

.field public static final SERVER_INTERNAL_ERROR_WTH_FC_CAUSE:I = 0x89c
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SERVER_INTERNAL_ERROR_WTH_RESTORATION:I = 0x89d

.field public static final SERVER_TIME_OUT:I = 0x1f8

.field public static final SERVER_UNREACHABLE:I = 0x5dd

.field public static final SERVICE_UNAVAILABLE:I = 0x1f7

.field public static final SESSION_INTERVAL_TOO_SMALL:I = 0x1a6

.field public static final SIP_REG_FAILURE:I = 0x641

.field public static final SOCKET_ERROR:I = 0x2713

.field public static final SUBSCRIBE_RETRY_FAILED:I = 0x967

.field public static final SUBSCRIBE_RETRY_START:I = 0x966

.field public static final TEMPORARILY_UNAVAILABLE:I = 0x1e0

.field public static final TIMER_VZW_EXPIRED:I = 0x9c5

.field public static final TOO_MANY_HOPS:I = 0x1e3

.field public static final TRANSACTION_TERMINTED:I = 0x2714

.field public static final UNAUTHORIZED:I = 0x191

.field public static final UNDECIPHERABLE:I = 0x1ed

.field public static final UNKNOWN_RESOURCE_PRIORITY:I = 0x1a1

.field public static final UNSUPPORTED_CERTIFICATE:I = 0x1b5

.field public static final UNSUPPORTED_MEDIA_TYPE:I = 0x19f

.field public static final UNSUPPORTED_URI_SCHEME:I = 0x1a0

.field public static final USE_IDENTITY_HEADER:I = 0x1ac

.field public static final USE_PROXY:I = 0x131

.field public static final VERSION_NOT_SUPPORTED:I = 0x1f9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "errorCode"    # I

    .prologue
    .line 406
    sparse-switch p0, :sswitch_data_0

    .line 476
    const-string/jumbo v0, "Unknown"

    :goto_0
    return-object v0

    .line 407
    :sswitch_0
    const-string/jumbo v0, "No Error"

    goto :goto_0

    .line 408
    :sswitch_1
    const-string/jumbo v0, "Socket failure"

    goto :goto_0

    .line 409
    :sswitch_2
    const-string/jumbo v0, "Server error"

    goto :goto_0

    .line 410
    :sswitch_3
    const-string/jumbo v0, "Transaction terminated"

    goto :goto_0

    .line 411
    :sswitch_4
    const-string/jumbo v0, "Client error"

    goto :goto_0

    .line 412
    :sswitch_5
    const-string/jumbo v0, "Invalid remote address"

    goto :goto_0

    .line 413
    :sswitch_6
    const-string/jumbo v0, "Unreachable"

    goto :goto_0

    .line 414
    :sswitch_7
    const-string/jumbo v0, "Authentication failed"

    goto :goto_0

    .line 415
    :sswitch_8
    const-string/jumbo v0, "In progress"

    goto :goto_0

    .line 416
    :sswitch_9
    const-string/jumbo v0, "Network disconnected"

    goto :goto_0

    .line 417
    :sswitch_a
    const-string/jumbo v0, "Invalid domain"

    goto :goto_0

    .line 418
    :sswitch_b
    const-string/jumbo v0, "Server not rechable"

    goto :goto_0

    .line 419
    :sswitch_c
    const-string/jumbo v0, "Error"

    goto :goto_0

    .line 420
    :sswitch_d
    const-string/jumbo v0, "Call not acceptable and auto divert"

    goto :goto_0

    .line 421
    :sswitch_e
    const-string/jumbo v0, "Network busy"

    goto :goto_0

    .line 422
    :sswitch_f
    const-string/jumbo v0, "Call request failed"

    goto :goto_0

    .line 423
    :sswitch_10
    const-string/jumbo v0, "Registration failed"

    goto :goto_0

    .line 424
    :sswitch_11
    const-string/jumbo v0, "Call rejected"

    goto :goto_0

    .line 425
    :sswitch_12
    const-string/jumbo v0, "Call failed"

    goto :goto_0

    .line 426
    :sswitch_13
    const-string/jumbo v0, "Request terminated"

    goto :goto_0

    .line 427
    :sswitch_14
    const-string/jumbo v0, "Session aborted"

    goto :goto_0

    .line 428
    :sswitch_15
    const-string/jumbo v0, "Invalid address"

    goto :goto_0

    .line 429
    :sswitch_16
    const-string/jumbo v0, "call not allowed"

    goto :goto_0

    .line 430
    :sswitch_17
    const-string/jumbo v0, "Request type not allowed"

    goto :goto_0

    .line 431
    :sswitch_18
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 432
    :sswitch_19
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 433
    :sswitch_1a
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 434
    :sswitch_1b
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 435
    :sswitch_1c
    const-string/jumbo v0, "Call not allowed(Invite Failure)"

    goto :goto_0

    .line 436
    :sswitch_1d
    const-string/jumbo v0, "QOS failed"

    goto :goto_0

    .line 437
    :sswitch_1e
    const-string/jumbo v0, "QOS network unaware"

    goto :goto_0

    .line 438
    :sswitch_1f
    const-string/jumbo v0, "QOS suspended"

    goto :goto_0

    .line 439
    :sswitch_20
    const-string/jumbo v0, "QOS incall unaware"

    goto/16 :goto_0

    .line 440
    :sswitch_21
    const-string/jumbo v0, "PPP Closed"

    goto/16 :goto_0

    .line 441
    :sswitch_22
    const-string/jumbo v0, "Call failed"

    goto/16 :goto_0

    .line 442
    :sswitch_23
    const-string/jumbo v0, "Internal server error"

    goto/16 :goto_0

    .line 443
    :sswitch_24
    const-string/jumbo v0, "Service unavailable"

    goto/16 :goto_0

    .line 444
    :sswitch_25
    const-string/jumbo v0, "Session time out"

    goto/16 :goto_0

    .line 445
    :sswitch_26
    const-string/jumbo v0, "SIP Registration time out"

    goto/16 :goto_0

    .line 446
    :sswitch_27
    const-string/jumbo v0, "Registration request failed"

    goto/16 :goto_0

    .line 447
    :sswitch_28
    const-string/jumbo v0, "Call not acceptable divert"

    goto/16 :goto_0

    .line 448
    :sswitch_29
    const-string/jumbo v0, "PPP Open Failed"

    goto/16 :goto_0

    .line 449
    :sswitch_2a
    const-string/jumbo v0, "RTP Timeout"

    goto/16 :goto_0

    .line 450
    :sswitch_2b
    const-string/jumbo v0, "Call alternative services"

    goto/16 :goto_0

    .line 451
    :sswitch_2c
    const-string/jumbo v0, "Start conference call failure"

    goto/16 :goto_0

    .line 452
    :sswitch_2d
    const-string/jumbo v0, "Add user to session failure"

    goto/16 :goto_0

    .line 453
    :sswitch_2e
    const-string/jumbo v0, "Remove user from session failure"

    goto/16 :goto_0

    .line 454
    :sswitch_2f
    const-string/jumbo v0, "End call NW handover"

    goto/16 :goto_0

    .line 455
    :sswitch_30
    const-string/jumbo v0, "Cannot connect HD call"

    goto/16 :goto_0

    .line 456
    :sswitch_31
    const-string/jumbo v0, "Invite Timeout"

    goto/16 :goto_0

    .line 457
    :sswitch_32
    const-string/jumbo v0, "ACK wait timer timeout"

    goto/16 :goto_0

    .line 458
    :sswitch_33
    const-string/jumbo v0, "ACK for 200 OK but call terminated"

    goto/16 :goto_0

    .line 459
    :sswitch_34
    const-string/jumbo v0, "403 response for registering"

    goto/16 :goto_0

    .line 460
    :sswitch_35
    const-string/jumbo v0, "Call rejected due to active CS Call"

    goto/16 :goto_0

    .line 461
    :sswitch_36
    const-string/jumbo v0, "Bad request"

    goto/16 :goto_0

    .line 462
    :sswitch_37
    const-string/jumbo v0, "Media not supported"

    goto/16 :goto_0

    .line 463
    :sswitch_38
    const-string/jumbo v0, "Call switch failure"

    goto/16 :goto_0

    .line 464
    :sswitch_39
    const-string/jumbo v0, "Call switch rejected"

    goto/16 :goto_0

    .line 465
    :sswitch_3a
    const-string/jumbo v0, "Call hold failed"

    goto/16 :goto_0

    .line 466
    :sswitch_3b
    const-string/jumbo v0, "Call resume failed"

    goto/16 :goto_0

    .line 467
    :sswitch_3c
    const-string/jumbo v0, "Busy here"

    goto/16 :goto_0

    .line 468
    :sswitch_3d
    const-string/jumbo v0, "Not Acceptable Here"

    goto/16 :goto_0

    .line 469
    :sswitch_3e
    const-string/jumbo v0, "Network Unreachable"

    goto/16 :goto_0

    .line 470
    :sswitch_3f
    const-string/jumbo v0, "Call 5xx error response"

    goto/16 :goto_0

    .line 471
    :sswitch_40
    const-string/jumbo v0, "Call temporal unavailable with 380 cause"

    goto/16 :goto_0

    .line 472
    :sswitch_41
    const-string/jumbo v0, "Call temporal unavailable with 415 cause"

    goto/16 :goto_0

    .line 473
    :sswitch_42
    const-string/jumbo v0, "Internal server error with FC value"

    goto/16 :goto_0

    .line 474
    :sswitch_43
    const-string/jumbo v0, "Internal server error with Restoration"

    goto/16 :goto_0

    .line 475
    :sswitch_44
    const-string/jumbo v0, "Timer_VZW expired"

    goto/16 :goto_0

    .line 406
    :sswitch_data_0
    .sparse-switch
        0x17c -> :sswitch_2b
        0x190 -> :sswitch_36
        0x193 -> :sswitch_18
        0x194 -> :sswitch_5
        0x195 -> :sswitch_17
        0x196 -> :sswitch_f
        0x198 -> :sswitch_32
        0x19f -> :sswitch_37
        0x1e0 -> :sswitch_6
        0x1e4 -> :sswitch_15
        0x1e6 -> :sswitch_3c
        0x1e7 -> :sswitch_13
        0x1e8 -> :sswitch_3d
        0x1f4 -> :sswitch_23
        0x1f7 -> :sswitch_24
        0x25b -> :sswitch_11
        0x25e -> :sswitch_16
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_4
        0x44d -> :sswitch_14
        0x44e -> :sswitch_33
        0x44f -> :sswitch_25
        0x450 -> :sswitch_2c
        0x451 -> :sswitch_2d
        0x452 -> :sswitch_2e
        0x453 -> :sswitch_2f
        0x454 -> :sswitch_35
        0x455 -> :sswitch_38
        0x456 -> :sswitch_39
        0x457 -> :sswitch_3a
        0x458 -> :sswitch_3b
        0x459 -> :sswitch_30
        0x45a -> :sswitch_31
        0x4b1 -> :sswitch_1d
        0x4b2 -> :sswitch_1e
        0x4b3 -> :sswitch_1f
        0x4b4 -> :sswitch_20
        0x515 -> :sswitch_21
        0x516 -> :sswitch_29
        0x579 -> :sswitch_2a
        0x5dd -> :sswitch_b
        0x5de -> :sswitch_2
        0x641 -> :sswitch_10
        0x642 -> :sswitch_27
        0x643 -> :sswitch_26
        0x6a5 -> :sswitch_9
        0x709 -> :sswitch_22
        0x70a -> :sswitch_a
        0x835 -> :sswitch_28
        0x836 -> :sswitch_3e
        0x899 -> :sswitch_3f
        0x89a -> :sswitch_40
        0x89b -> :sswitch_41
        0x89c -> :sswitch_42
        0x89d -> :sswitch_43
        0x8fd -> :sswitch_12
        0x8fe -> :sswitch_19
        0x8ff -> :sswitch_1a
        0x900 -> :sswitch_1b
        0x901 -> :sswitch_1c
        0x969 -> :sswitch_34
        0x9c5 -> :sswitch_44
        0x2711 -> :sswitch_7
        0x2712 -> :sswitch_e
        0x2713 -> :sswitch_1
        0x2714 -> :sswitch_3
        0x2715 -> :sswitch_8
        0x2716 -> :sswitch_c
        0x2717 -> :sswitch_d
    .end sparse-switch
.end method
