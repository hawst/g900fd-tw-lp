.class public Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;
.super Ljava/lang/Object;
.source "IMSUserProfile.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_PORT:I = 0x13c4

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final TCP:Ljava/lang/String; = "TCP"

.field private static final UDP:Ljava/lang/String; = "UDP"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public available:I

.field private mAuthUserName:Ljava/lang/String;

.field private mAutoRegistration:Z

.field private transient mCallingUid:I

.field public transient mCapabilities:Landroid/os/Bundle;

.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mPort:I

.field private mProfileName:Ljava/lang/String;

.field private mProtocol:Ljava/lang/String;

.field private mProxyAddress:Ljava/lang/String;

.field private mSendKeepAlive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->LOG_TAG:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 19
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string/jumbo v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 303
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string/jumbo v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    iput-boolean v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 307
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    .line 309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    .line 318
    return-void

    :cond_0
    move v0, v2

    .line 312
    goto :goto_0

    :cond_1
    move v1, v2

    .line 313
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 2
    .param p1, "profile"    # Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string/jumbo v0, "UDP"

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 32
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 33
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 35
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 75
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProxyAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSipDomain()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getProfileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    .line 80
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAuthUserName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getPort()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 82
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getSendKeepAlive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    .line 83
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAutoRegistration()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    .line 84
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCallingUid()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 85
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getCapabilities()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    .line 86
    invoke-virtual {p1}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getAvailability()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->available:I

    .line 87
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;-><init>()V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)V
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Z)V
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;I)V
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    return-void
.end method

.method static synthetic access$4()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 495
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    if-nez v0, :cond_0

    .line 496
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    .line 497
    :cond_0
    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoRegistration()Z
    .locals 1

    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    return v0
.end method

.method public getAvailability()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->available:I

    return v0
.end method

.method public getCallingUid()I
    .locals 1

    .prologue
    .line 490
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    return v0
.end method

.method public getCapabilities()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    return v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getSendKeepAlive()Z
    .locals 1

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    return v0
.end method

.method public getSipDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getUriString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "sip:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->getUserName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public setCallingUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .prologue
    .line 482
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    .line 483
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 323
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProtocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 328
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mSendKeepAlive:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAutoRegistration:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 330
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCallingUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mAuthUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;->mCapabilities:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 334
    return-void

    :cond_0
    move v0, v2

    .line 328
    goto :goto_0

    :cond_1
    move v1, v2

    .line 329
    goto :goto_1
.end method
