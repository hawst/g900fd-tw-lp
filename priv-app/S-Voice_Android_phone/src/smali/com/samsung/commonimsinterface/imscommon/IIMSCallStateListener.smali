.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSCallStateListener;
.super Ljava/lang/Object;
.source "IIMSCallStateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSCallStateListener$CheckCallType;
    }
.end annotation


# virtual methods
.method public abstract onCallACKReceived(I)V
.end method

.method public abstract onCallBusy(I)V
.end method

.method public abstract onCallEnded(I)V
.end method

.method public abstract onCallEnded(II)V
.end method

.method public abstract onCallEstablished(IILcom/sec/ims/android/internal/IIMSParams;Ljava/lang/String;)V
.end method

.method public abstract onCallForwarded(I)V
.end method

.method public abstract onCallHeld(I)V
.end method

.method public abstract onCallHeld(IZ)V
.end method

.method public abstract onCallResumed(I)V
.end method

.method public abstract onCallResumed(IZ)V
.end method

.method public abstract onCallSwitched(IILcom/sec/ims/android/internal/IIMSParams;)V
.end method

.method public abstract onCallTrying(I)V
.end method

.method public abstract onCalling(I)V
.end method

.method public abstract onChangeRequest(II[B)V
.end method

.method public abstract onCodecBitRateChange(II)V
.end method

.method public abstract onCodecDescriptionChanged(I)V
.end method

.method public abstract onConferenceCallEstablished(IILcom/sec/ims/android/internal/IIMSParams;)V
.end method

.method public abstract onConferenceCallParticipantAdded(IIILcom/sec/ims/android/internal/IIMSParams;)V
.end method

.method public abstract onConferenceCallParticipantRemoved(IILjava/lang/String;)V
.end method

.method public abstract onConferenceEstablished(IILcom/sec/ims/android/internal/IIMSParams;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onEarlyMediaStart(ILcom/sec/ims/android/internal/IIMSParams;)V
.end method

.method public abstract onError(IILjava/lang/String;)V
.end method

.method public abstract onNetworkTransition(II)V
.end method

.method public abstract onNotifyReceived(I[I[Ljava/lang/String;)V
.end method

.method public abstract onRinging(ILjava/lang/String;I)V
.end method

.method public abstract onRinging(ILjava/lang/String;Lcom/sec/ims/android/internal/IIMSParams;I)V
.end method

.method public abstract onRingingBack(I)V
.end method

.method public abstract onTtyTextRequest(II[B)V
.end method

.method public abstract onUSSDIndication(III[B)V
.end method

.method public abstract onUSSDResponse(IZ)V
.end method

.method public abstract stopAlertTone(I)V
.end method
