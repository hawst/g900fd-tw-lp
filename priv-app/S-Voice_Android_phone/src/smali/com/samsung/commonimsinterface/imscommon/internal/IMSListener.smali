.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;
.super Ljava/lang/Object;
.source "IMSListener.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field protected mListener:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->LOG_TAG:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public constructor <init>(Landroid/os/RemoteCallbackList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "listener":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    .line 26
    if-nez p1, :cond_0

    .line 27
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v1, "[IMS ERROR] IMSListener : listener is null"

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    .line 31
    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V
    .locals 6
    .param p1, "token"    # I
    .param p2, "action"    # I
    .param p3, "parameter"    # Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_0

    .line 36
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onReceive : token["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] action ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] parameter ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .line 40
    .local v2, "length":I
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Length : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 50
    :goto_1
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    .end local v1    # "index":I
    .end local v2    # "length":I
    :cond_0
    monitor-exit p0

    return-void

    .line 44
    .restart local v1    # "index":I
    .restart local v2    # "length":I
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSListener;->mListener:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    invoke-interface {v3, p1, p2, p3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;->onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 35
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "index":I
    .end local v2    # "length":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
