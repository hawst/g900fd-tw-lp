.class public Lcom/samsung/commonimsinterface/imscommon/IIMSListener$GENERAL;
.super Ljava/lang/Object;
.source "IIMSListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GENERAL"
.end annotation


# static fields
.field public static final APCS_INFO:I = 0x3e9

.field public static final BASE:I = 0x3e8

.field public static final CELL_LOCATION_CHANGED:I = 0x3f8

.field public static final CHANGE_DATA:I = 0x3f1

.field public static final DATA_CHANGE_STARTED:I = 0x3f7

.field public static final DATA_CONNECTION_STATE_CHANGED:I = 0x3f6

.field public static final DATA_DISABLED:I = 0x3f5

.field public static final DATA_DISABLING:I = 0x3f4

.field public static final DATA_ENABLED:I = 0x3f3

.field public static final DATA_ENABLING:I = 0x3f2

.field public static final EMERGENCY_REGISTRATION_DONE:I = 0x3ef

.field public static final EMERGENCY_REGISTRATION_FAILED:I = 0x3f0

.field public static final NETWORK_TRANSITION:I = 0x3ee

.field public static final REGISTERING:I = 0x3ea

.field public static final REGISTRATION_DONE:I = 0x3ec

.field public static final REGISTRATION_FAILED:I = 0x3ed


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
