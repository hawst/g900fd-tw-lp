.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSEPDGStateListener;
.super Ljava/lang/Object;
.source "IIMSEPDGStateListener.java"


# virtual methods
.method public abstract onEPDGAvailable()V
.end method

.method public abstract onEPDGDedicatedBearer(I)V
.end method

.method public abstract onEPDGHandoverLTEtoWLAN(Z)V
.end method

.method public abstract onEPDGHandoverWLANtoLTE(Z)V
.end method

.method public abstract onEPDGOnlyWLANConnected(Z)V
.end method

.method public abstract onEPDGUnavailable(I)V
.end method

.method public abstract onInetDataChangeStarted(Z)V
.end method
