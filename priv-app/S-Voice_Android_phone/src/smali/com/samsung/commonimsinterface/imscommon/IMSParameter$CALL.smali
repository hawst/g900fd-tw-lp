.class public Lcom/samsung/commonimsinterface/imscommon/IMSParameter$CALL;
.super Ljava/lang/Object;
.source "IMSParameter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CALL"
.end annotation


# static fields
.field public static final ADDED_SESSION_ID:Ljava/lang/String; = "addedsessionid"

.field public static final AUDIO_CODEC:Ljava/lang/String; = "audiocodec"

.field public static final CALL_TYPE:Ljava/lang/String; = "calltype"

.field public static final CODEC_BIT_RATE:Ljava/lang/String; = "codecbitrate"

.field public static final CONFERENCE_SUPPORTED:Ljava/lang/String; = "conferencesupported"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DCS:Ljava/lang/String; = "dcs"

.field public static final EPDG_UNAVAILABLE_REASON:Ljava/lang/String; = "epdgunavailablereason"

.field public static final ERROR_CODE:Ljava/lang/String; = "errorcode"

.field public static final ERROR_MESSAGE:Ljava/lang/String; = "errormessage"

.field public static final EVENT:Ljava/lang/String; = "event"

.field public static final HISTORY:Ljava/lang/String; = "history"

.field public static final HOLD_REQUEST:Ljava/lang/String; = "holdrequest"

.field public static final MODIFY_HEADER:Ljava/lang/String; = "modifyheader"

.field public static final NUMBER_PLUS:Ljava/lang/String; = "numberplus"

.field public static final PARTICIPANT_LIST:Ljava/lang/String; = "participantlist"

.field public static final PEER_URI:Ljava/lang/String; = "peeruri"

.field public static final PLETTERING:Ljava/lang/String; = "plettering"

.field public static final REASON:Ljava/lang/String; = "reason"

.field public static final REMOVED_SESSION_ID:Ljava/lang/String; = "removedsessionid"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final RESUME_REQUEST:Ljava/lang/String; = "resumerequest"

.field public static final SESSION_ID:Ljava/lang/String; = "sessionid"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final TTY_TYPE:Ljava/lang/String; = "ttytype"

.field public static final URI_LIST:Ljava/lang/String; = "urilist"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
