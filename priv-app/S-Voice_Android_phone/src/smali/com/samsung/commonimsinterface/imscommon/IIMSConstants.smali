.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$ERROR_CODE;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$PRESENCE;
    }
.end annotation


# static fields
.field public static final ACTION_INET_DATA_CHANGE_STARTED:Ljava/lang/String; = "com.samsung.commonimsinterface.action.MOBILE_DATA_CHANGE"

.field public static final EXTRA_DATA_ENABLED:Ljava/lang/String; = "dataEnabled"
