.class public abstract Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;
.super Landroid/os/Binder;
.source "IIMSService.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

.field static final TRANSACTION_acceptChangeRequest:I = 0x10

.field static final TRANSACTION_addParticipantToNWayConferenceCall:I = 0x2b

.field static final TRANSACTION_addUserForConferenceCall:I = 0x1e

.field static final TRANSACTION_answerCall:I = 0x5

.field static final TRANSACTION_answerCallAudioOnly:I = 0x7

.field static final TRANSACTION_answerCallWithCallType:I = 0x6

.field static final TRANSACTION_captureSurfaceImage:I = 0x7f

.field static final TRANSACTION_changeCall:I = 0xf

.field static final TRANSACTION_closeCamera:I = 0x72

.field static final TRANSACTION_contactSvcCallFunction:I = 0x8a

.field static final TRANSACTION_deRegisterForAPCSStateChange:I = 0x42

.field static final TRANSACTION_deRegisterForCallStateChange:I = 0x2

.field static final TRANSACTION_deRegisterForConnectivityStateChange:I = 0x3c

.field static final TRANSACTION_deRegisterForMediaStateChange:I = 0x63

.field static final TRANSACTION_deRegisterForPresenceStateChange:I = 0x85

.field static final TRANSACTION_deRegisterForRegisterStateChange:I = 0x3e

.field static final TRANSACTION_deRegisterForSMSStateChange:I = 0x8c

.field static final TRANSACTION_deRegisterForSSConfigStateChange:I = 0x91

.field static final TRANSACTION_deRegisterForSettingStateListener:I = 0x40

.field static final TRANSACTION_deinitSurface:I = 0x80

.field static final TRANSACTION_disableWiFiCalling:I = 0x55

.field static final TRANSACTION_downgradeCall:I = 0xe

.field static final TRANSACTION_emergencyDeregister:I = 0x29

.field static final TRANSACTION_emergencyRegister:I = 0x28

.field static final TRANSACTION_enableWiFiCalling:I = 0x54

.field static final TRANSACTION_endCall:I = 0x8

.field static final TRANSACTION_endCallWithReason:I = 0x9

.field static final TRANSACTION_endDialingOrAlertingCall:I = 0x30

.field static final TRANSACTION_forceRestart:I = 0x53

.field static final TRANSACTION_getBackCameraId:I = 0x73

.field static final TRANSACTION_getCallBarring:I = 0x97

.field static final TRANSACTION_getCallForwarding:I = 0x95

.field static final TRANSACTION_getCallType:I = 0x36

.field static final TRANSACTION_getCallWaiting:I = 0x93

.field static final TRANSACTION_getCameraDirection:I = 0x76

.field static final TRANSACTION_getCameraState:I = 0x75

.field static final TRANSACTION_getCurrentLatchedNetwork:I = 0x23

.field static final TRANSACTION_getFeatureMask:I = 0x4e

.field static final TRANSACTION_getFrontCameraId:I = 0x74

.field static final TRANSACTION_getIMSSettingValues:I = 0x60

.field static final TRANSACTION_getMaxVolume:I = 0x1d

.field static final TRANSACTION_getNegotiatedFPS:I = 0x6a

.field static final TRANSACTION_getNegotiatedHeight:I = 0x68

.field static final TRANSACTION_getNegotiatedWidth:I = 0x69

.field static final TRANSACTION_getNumberOfCameras:I = 0x6d

.field static final TRANSACTION_getParticipantListSizeForNWayConferenceCall:I = 0x2d

.field static final TRANSACTION_getParticipantPeerURIFromNWayConferenceCall:I = 0x2f

.field static final TRANSACTION_getParticipantURIListForNWayConferenceCall:I = 0x2e

.field static final TRANSACTION_getRemoteCapabilitiesAndAvailability:I = 0x88

.field static final TRANSACTION_getServiceStatus:I = 0x4f

.field static final TRANSACTION_getSessionID:I = 0x39

.field static final TRANSACTION_getState:I = 0x37

.field static final TRANSACTION_handleActionsOverEpdg:I = 0x58

.field static final TRANSACTION_hashCode:I = 0x38

.field static final TRANSACTION_holdCall:I = 0xb

.field static final TRANSACTION_holdVideo:I = 0x81

.field static final TRANSACTION_indicateSrvccHoStarted:I = 0x20

.field static final TRANSACTION_isDeregisteredToMobile:I = 0x46

.field static final TRANSACTION_isDeregisteredToWiFi:I = 0x4a

.field static final TRANSACTION_isDeregisteringToMobile:I = 0x47

.field static final TRANSACTION_isDeregisteringToWiFi:I = 0x4b

.field static final TRANSACTION_isDisablingMobileData:I = 0x4d

.field static final TRANSACTION_isEnablingMobileData:I = 0x4c

.field static final TRANSACTION_isIMSEnabledOnWifi:I = 0x24

.field static final TRANSACTION_isImsForbidden:I = 0x22

.field static final TRANSACTION_isInCall:I = 0x33

.field static final TRANSACTION_isInEPDGCall:I = 0x3a

.field static final TRANSACTION_isIncomingCallIntent:I = 0x32

.field static final TRANSACTION_isLTEVideoCallEnabled:I = 0x5e

.field static final TRANSACTION_isMediaReadyToReceivePreview:I = 0x6b

.field static final TRANSACTION_isMuted:I = 0x19

.field static final TRANSACTION_isOnEHRPD:I = 0x26

.field static final TRANSACTION_isOnHold:I = 0x34

.field static final TRANSACTION_isOnLTE:I = 0x25

.field static final TRANSACTION_isRegistered:I = 0x43

.field static final TRANSACTION_isRegisteredToMobile:I = 0x44

.field static final TRANSACTION_isRegisteredToWiFi:I = 0x48

.field static final TRANSACTION_isRegisteringToMobile:I = 0x45

.field static final TRANSACTION_isRegisteringToWiFi:I = 0x49

.field static final TRANSACTION_isVoLTEFeatureEnabled:I = 0x5b

.field static final TRANSACTION_makeCall:I = 0x3

.field static final TRANSACTION_manualDeregister:I = 0x51

.field static final TRANSACTION_manualDeregisterForCall:I = 0x18

.field static final TRANSACTION_manualRegister:I = 0x50

.field static final TRANSACTION_mediaDeInit:I = 0x65

.field static final TRANSACTION_mediaInit:I = 0x64

.field static final TRANSACTION_mmSS_Svc_Api:I = 0x98

.field static final TRANSACTION_openCamera:I = 0x6e

.field static final TRANSACTION_openCameraEx:I = 0x6f

.field static final TRANSACTION_publishCapabilitiesAndAvailability:I = 0x86

.field static final TRANSACTION_registerForAPCSStateChange:I = 0x41

.field static final TRANSACTION_registerForCallStateChange:I = 0x1

.field static final TRANSACTION_registerForConnectivityStateChange:I = 0x3b

.field static final TRANSACTION_registerForMediaStateChange:I = 0x62

.field static final TRANSACTION_registerForPresenceStateChange:I = 0x84

.field static final TRANSACTION_registerForRegisterStateChange:I = 0x3d

.field static final TRANSACTION_registerForSMSStateChange:I = 0x8b

.field static final TRANSACTION_registerForSSConfigStateChange:I = 0x90

.field static final TRANSACTION_registerForSettingStateListener:I = 0x3f

.field static final TRANSACTION_registerServiceCapabilityAvailabilityTemplate:I = 0x89

.field static final TRANSACTION_rejectCall:I = 0xa

.field static final TRANSACTION_rejectChangeRequest:I = 0x11

.field static final TRANSACTION_removeParticipantFromNWayConferenceCall:I = 0x2c

.field static final TRANSACTION_resetCameraID:I = 0x7c

.field static final TRANSACTION_resumeCall:I = 0xc

.field static final TRANSACTION_resumeVideo:I = 0x82

.field static final TRANSACTION_sendDeliverReport:I = 0x8f

.field static final TRANSACTION_sendDeregister:I = 0x52

.field static final TRANSACTION_sendDtmf:I = 0x12

.field static final TRANSACTION_sendInitialRegister:I = 0x57

.field static final TRANSACTION_sendLiveVideo:I = 0x7e

.field static final TRANSACTION_sendReInvite:I = 0x1f

.field static final TRANSACTION_sendSMSOverIMS:I = 0x8d

.field static final TRANSACTION_sendSMSResponse:I = 0x8e

.field static final TRANSACTION_sendStillImage:I = 0x7d

.field static final TRANSACTION_sendTtyData:I = 0x16

.field static final TRANSACTION_sendUSSD:I = 0x4

.field static final TRANSACTION_setAutoResponse:I = 0x21

.field static final TRANSACTION_setCallBarring:I = 0x96

.field static final TRANSACTION_setCallForwarding:I = 0x94

.field static final TRANSACTION_setCallWaiting:I = 0x92

.field static final TRANSACTION_setCameraDisplayOrientation:I = 0x77

.field static final TRANSACTION_setCameraDisplayOrientationEx:I = 0x78

.field static final TRANSACTION_setDisplay:I = 0x66

.field static final TRANSACTION_setEmergencyPdnInfo:I = 0x27

.field static final TRANSACTION_setFarEndSurface:I = 0x67

.field static final TRANSACTION_setIsMediaReadyToReceivePreview:I = 0x6c

.field static final TRANSACTION_setLTEVideoCallDisable:I = 0x5d

.field static final TRANSACTION_setLTEVideoCallEnable:I = 0x5c

.field static final TRANSACTION_setRegistrationFeatureTags:I = 0x56

.field static final TRANSACTION_setSpeakerMode:I = 0x1b

.field static final TRANSACTION_setThirdPartyMode:I = 0x5f

.field static final TRANSACTION_setTtyMode:I = 0x17

.field static final TRANSACTION_setVoLTEFeatureDisable:I = 0x5a

.field static final TRANSACTION_setVoLTEFeatureEnable:I = 0x59

.field static final TRANSACTION_setVolume:I = 0x1c

.field static final TRANSACTION_startAudio:I = 0x35

.field static final TRANSACTION_startCameraPreview:I = 0x70

.field static final TRANSACTION_startDtmf:I = 0x13

.field static final TRANSACTION_startNWayConferenceCall:I = 0x2a

.field static final TRANSACTION_startRender:I = 0x7a

.field static final TRANSACTION_startVideo:I = 0x83

.field static final TRANSACTION_stopCameraPreview:I = 0x71

.field static final TRANSACTION_stopDtmf:I = 0x14

.field static final TRANSACTION_swapVideoSurface:I = 0x7b

.field static final TRANSACTION_switchCamera:I = 0x79

.field static final TRANSACTION_takeCall:I = 0x31

.field static final TRANSACTION_toggleMute:I = 0x1a

.field static final TRANSACTION_unpublishCapabilitiesAndAvailability:I = 0x87

.field static final TRANSACTION_updateIMSSettingValues:I = 0x61

.field static final TRANSACTION_upgradeCall:I = 0xd

.field static final TRANSACTION_voiceRecord:I = 0x15


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string/jumbo v1, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 1541
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 42
    :sswitch_0
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 50
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 56
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_2
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 59
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForCallStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 65
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 73
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 75
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 76
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->makeCall(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)I

    move-result v8

    .line 77
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 83
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_4
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 87
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 89
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 91
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 93
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 94
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendUSSD(III[BI)I

    move-result v8

    .line 95
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 101
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":I
    .end local v8    # "_result":I
    :sswitch_5
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 104
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCall(I)V

    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 110
    .end local v1    # "_arg0":I
    :sswitch_6
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 114
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 115
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCallWithCallType(II)V

    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 121
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_7
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 124
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->answerCallAudioOnly(I)V

    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 130
    .end local v1    # "_arg0":I
    :sswitch_8
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 133
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endCall(I)V

    .line 134
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 139
    .end local v1    # "_arg0":I
    :sswitch_9
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 143
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 144
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endCallWithReason(II)V

    .line 145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 150
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 154
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 155
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->rejectCall(II)V

    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 161
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_b
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 164
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->holdCall(I)V

    .line 165
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 170
    .end local v1    # "_arg0":I
    :sswitch_c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 173
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resumeCall(I)V

    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 179
    .end local v1    # "_arg0":I
    :sswitch_d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 183
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 184
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->upgradeCall(II)V

    .line 185
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 190
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 194
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 195
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->downgradeCall(II)V

    .line 196
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 201
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_f
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 205
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 206
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->changeCall(II)V

    .line 207
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 212
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_10
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 215
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->acceptChangeRequest(I)V

    .line 216
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 221
    .end local v1    # "_arg0":I
    :sswitch_11
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 224
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->rejectChangeRequest(I)V

    .line 225
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 230
    .end local v1    # "_arg0":I
    :sswitch_12
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 234
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 235
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDtmf(II)Z

    move-result v8

    .line 236
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 237
    if-eqz v8, :cond_0

    move v0, v9

    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 242
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_13
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 246
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 247
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startDtmf(II)Z

    move-result v8

    .line 248
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 249
    if-eqz v8, :cond_1

    move v0, v9

    :cond_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 254
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_14
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 257
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->stopDtmf(I)Z

    move-result v8

    .line 258
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 259
    if-eqz v8, :cond_2

    move v0, v9

    :cond_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 264
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_15
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 268
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 270
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 271
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->voiceRecord(IILjava/lang/String;)V

    .line 272
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 277
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_16
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 279
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 281
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 282
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendTtyData(I[B)V

    .line 283
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 288
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":[B
    :sswitch_17
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 291
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setTtyMode(I)V

    .line 292
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 297
    .end local v1    # "_arg0":I
    :sswitch_18
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 298
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualDeregisterForCall()V

    .line 299
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 304
    :sswitch_19
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 307
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isMuted(I)Z

    move-result v8

    .line 308
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 309
    if-eqz v8, :cond_3

    move v0, v9

    :cond_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 314
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_1a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 316
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 317
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->toggleMute(I)V

    .line 318
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 323
    .end local v1    # "_arg0":I
    :sswitch_1b
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 327
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_4

    move v2, v9

    .line 328
    .local v2, "_arg1":Z
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setSpeakerMode(IZ)V

    .line 329
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_4
    move v2, v0

    .line 327
    goto :goto_1

    .line 334
    .end local v1    # "_arg0":I
    :sswitch_1c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 338
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 339
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVolume(II)V

    .line 340
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 345
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_1d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 347
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 348
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getMaxVolume(I)I

    move-result v8

    .line 349
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 350
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 355
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_1e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 359
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 361
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 362
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->addUserForConferenceCall(ILjava/lang/String;I)I

    move-result v8

    .line 363
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 369
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":I
    :sswitch_1f
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 372
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendReInvite(I)V

    .line 373
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 378
    .end local v1    # "_arg0":I
    :sswitch_20
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 381
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->indicateSrvccHoStarted(I)V

    .line 382
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 387
    .end local v1    # "_arg0":I
    :sswitch_21
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 389
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 391
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 392
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setAutoResponse(II)V

    .line 393
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 398
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_22
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 399
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isImsForbidden()Z

    move-result v8

    .line 400
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 401
    if-eqz v8, :cond_5

    move v0, v9

    :cond_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 406
    .end local v8    # "_result":Z
    :sswitch_23
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 407
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCurrentLatchedNetwork()Ljava/lang/String;

    move-result-object v8

    .line 408
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 409
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_24
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isIMSEnabledOnWifi()Z

    move-result v8

    .line 416
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 417
    if-eqz v8, :cond_6

    move v0, v9

    :cond_6
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 422
    .end local v8    # "_result":Z
    :sswitch_25
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnLTE()Z

    move-result v8

    .line 424
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 425
    if-eqz v8, :cond_7

    move v0, v9

    :cond_7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 430
    .end local v8    # "_result":Z
    :sswitch_26
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnEHRPD()Z

    move-result v8

    .line 432
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 433
    if-eqz v8, :cond_8

    move v0, v9

    :cond_8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 438
    .end local v8    # "_result":Z
    :sswitch_27
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 440
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 442
    .local v1, "_arg0":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 444
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 445
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setEmergencyPdnInfo([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 451
    .end local v1    # "_arg0":[Ljava/lang/String;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_28
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 452
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->emergencyRegister()V

    .line 453
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 458
    :sswitch_29
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 459
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->emergencyDeregister()V

    .line 460
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 465
    :sswitch_2a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 467
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 469
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 471
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 472
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startNWayConferenceCall(III)I

    move-result v8

    .line 473
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 474
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 479
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v8    # "_result":I
    :sswitch_2b
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 483
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 484
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->addParticipantToNWayConferenceCall(II)V

    .line 485
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 490
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_2c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 494
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 495
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->removeParticipantFromNWayConferenceCall(II)V

    .line 496
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 501
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_2d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 503
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 504
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantListSizeForNWayConferenceCall(I)I

    move-result v8

    .line 505
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 506
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 511
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_2e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 513
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 514
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantURIListForNWayConferenceCall(I)[Ljava/lang/String;

    move-result-object v8

    .line 515
    .local v8, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 516
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    .end local v1    # "_arg0":I
    .end local v8    # "_result":[Ljava/lang/String;
    :sswitch_2f
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 523
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 525
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 526
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getParticipantPeerURIFromNWayConferenceCall(II)Ljava/lang/String;

    move-result-object v8

    .line 527
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 528
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 533
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_30
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 534
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->endDialingOrAlertingCall()V

    .line 535
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 540
    :sswitch_31
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 543
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 548
    .local v1, "_arg0":Landroid/content/Intent;
    :goto_2
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->takeCall(Landroid/content/Intent;)V

    .line 549
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 546
    .end local v1    # "_arg0":Landroid/content/Intent;
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/content/Intent;
    goto :goto_2

    .line 554
    .end local v1    # "_arg0":Landroid/content/Intent;
    :sswitch_32
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 556
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_b

    .line 557
    sget-object v10, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 562
    .restart local v1    # "_arg0":Landroid/content/Intent;
    :goto_3
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isIncomingCallIntent(Landroid/content/Intent;)Z

    move-result v8

    .line 563
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 564
    if-eqz v8, :cond_a

    move v0, v9

    :cond_a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 560
    .end local v1    # "_arg0":Landroid/content/Intent;
    .end local v8    # "_result":Z
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/content/Intent;
    goto :goto_3

    .line 569
    .end local v1    # "_arg0":Landroid/content/Intent;
    :sswitch_33
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 571
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 572
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isInCall(I)Z

    move-result v8

    .line 573
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 574
    if-eqz v8, :cond_c

    move v0, v9

    :cond_c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 579
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_34
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 581
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 582
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isOnHold(I)Z

    move-result v8

    .line 583
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 584
    if-eqz v8, :cond_d

    move v0, v9

    :cond_d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 589
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_35
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 591
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 592
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startAudio(I)V

    .line 593
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 598
    .end local v1    # "_arg0":I
    :sswitch_36
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 600
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 601
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallType(I)I

    move-result v8

    .line 602
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 603
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 608
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_37
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 610
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 611
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getState(I)I

    move-result v8

    .line 612
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 613
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 618
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_38
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 620
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 621
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->hashCode(I)I

    move-result v8

    .line 622
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 623
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 628
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_39
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 630
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 631
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getSessionID(I)I

    move-result v8

    .line 632
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 633
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 638
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_3a
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isInEPDGCall()Z

    move-result v8

    .line 640
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 641
    if-eqz v8, :cond_e

    move v0, v9

    :cond_e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 646
    .end local v8    # "_result":Z
    :sswitch_3b
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 648
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 649
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 650
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 655
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 657
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 658
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForConnectivityStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 659
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 664
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 666
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 667
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 668
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 673
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 676
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForRegisterStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 677
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 682
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_3f
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 684
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 685
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 686
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 691
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_40
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 693
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 694
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSettingStateListener(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 695
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 700
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_41
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 702
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 703
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 704
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 709
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_42
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 711
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 712
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForAPCSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 713
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 718
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_43
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 719
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegistered()Z

    move-result v8

    .line 720
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 721
    if-eqz v8, :cond_f

    move v0, v9

    :cond_f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 726
    .end local v8    # "_result":Z
    :sswitch_44
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 727
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteredToMobile()Z

    move-result v8

    .line 728
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 729
    if-eqz v8, :cond_10

    move v0, v9

    :cond_10
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 734
    .end local v8    # "_result":Z
    :sswitch_45
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 735
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteringToMobile()Z

    move-result v8

    .line 736
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 737
    if-eqz v8, :cond_11

    move v0, v9

    :cond_11
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 742
    .end local v8    # "_result":Z
    :sswitch_46
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 743
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteredToMobile()Z

    move-result v8

    .line 744
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 745
    if-eqz v8, :cond_12

    move v0, v9

    :cond_12
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 750
    .end local v8    # "_result":Z
    :sswitch_47
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 751
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteringToMobile()Z

    move-result v8

    .line 752
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 753
    if-eqz v8, :cond_13

    move v0, v9

    :cond_13
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 758
    .end local v8    # "_result":Z
    :sswitch_48
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 759
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteredToWiFi()Z

    move-result v8

    .line 760
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 761
    if-eqz v8, :cond_14

    move v0, v9

    :cond_14
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 766
    .end local v8    # "_result":Z
    :sswitch_49
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 767
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isRegisteringToWiFi()Z

    move-result v8

    .line 768
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    if-eqz v8, :cond_15

    move v0, v9

    :cond_15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 774
    .end local v8    # "_result":Z
    :sswitch_4a
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 775
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteredToWiFi()Z

    move-result v8

    .line 776
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 777
    if-eqz v8, :cond_16

    move v0, v9

    :cond_16
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 782
    .end local v8    # "_result":Z
    :sswitch_4b
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 783
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDeregisteringToWiFi()Z

    move-result v8

    .line 784
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 785
    if-eqz v8, :cond_17

    move v0, v9

    :cond_17
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 790
    .end local v8    # "_result":Z
    :sswitch_4c
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 791
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isEnablingMobileData()Z

    move-result v8

    .line 792
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 793
    if-eqz v8, :cond_18

    move v0, v9

    :cond_18
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 798
    .end local v8    # "_result":Z
    :sswitch_4d
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isDisablingMobileData()Z

    move-result v8

    .line 800
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 801
    if-eqz v8, :cond_19

    move v0, v9

    :cond_19
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 806
    .end local v8    # "_result":Z
    :sswitch_4e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 807
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getFeatureMask()I

    move-result v8

    .line 808
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 809
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 814
    .end local v8    # "_result":I
    :sswitch_4f
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 815
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getServiceStatus()Z

    move-result v8

    .line 816
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 817
    if-eqz v8, :cond_1a

    move v0, v9

    :cond_1a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 822
    .end local v8    # "_result":Z
    :sswitch_50
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 823
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualRegister()V

    .line 824
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 829
    :sswitch_51
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 830
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->manualDeregister()V

    .line 831
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 836
    :sswitch_52
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 838
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 839
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDeregister(I)Z

    move-result v8

    .line 840
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 841
    if-eqz v8, :cond_1b

    move v0, v9

    :cond_1b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 846
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_53
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 847
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->forceRestart()Z

    move-result v8

    .line 848
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 849
    if-eqz v8, :cond_1c

    move v0, v9

    :cond_1c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 854
    .end local v8    # "_result":Z
    :sswitch_54
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 855
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->enableWiFiCalling()V

    .line 856
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 861
    :sswitch_55
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 862
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->disableWiFiCalling()V

    .line 863
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 868
    :sswitch_56
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 870
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 872
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 873
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setRegistrationFeatureTags(Ljava/lang/String;[Ljava/lang/String;)V

    .line 874
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 879
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[Ljava/lang/String;
    :sswitch_57
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 881
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 882
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendInitialRegister(I)V

    .line 883
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 888
    .end local v1    # "_arg0":I
    :sswitch_58
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 889
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->handleActionsOverEpdg()V

    .line 890
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 895
    :sswitch_59
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 896
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVoLTEFeatureEnable()V

    .line 897
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 902
    :sswitch_5a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 903
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setVoLTEFeatureDisable()V

    .line 904
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 909
    :sswitch_5b
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 910
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isVoLTEFeatureEnabled()Z

    move-result v8

    .line 911
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 912
    if-eqz v8, :cond_1d

    move v0, v9

    :cond_1d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 917
    .end local v8    # "_result":Z
    :sswitch_5c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 918
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setLTEVideoCallEnable()V

    .line 919
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 924
    :sswitch_5d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 925
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setLTEVideoCallDisable()V

    .line 926
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 931
    :sswitch_5e
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 932
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isLTEVideoCallEnabled()Z

    move-result v8

    .line 933
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 934
    if-eqz v8, :cond_1e

    move v0, v9

    :cond_1e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 939
    .end local v8    # "_result":Z
    :sswitch_5f
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 941
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1f

    move v1, v9

    .line 942
    .local v1, "_arg0":Z
    :goto_4
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setThirdPartyMode(Z)V

    .line 943
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_1f
    move v1, v0

    .line 941
    goto :goto_4

    .line 948
    :sswitch_60
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 950
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 951
    .local v1, "_arg0":[I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getIMSSettingValues([I)Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    move-result-object v8

    .line 952
    .local v8, "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 953
    if-eqz v8, :cond_20

    .line 954
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 955
    invoke-virtual {v8, p3, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 958
    :cond_20
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 964
    .end local v1    # "_arg0":[I
    .end local v8    # "_result":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :sswitch_61
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 966
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_22

    .line 967
    sget-object v10, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 972
    .local v1, "_arg0":Landroid/os/Bundle;
    :goto_5
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->updateIMSSettingValues(Landroid/os/Bundle;)Z

    move-result v8

    .line 973
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 974
    if-eqz v8, :cond_21

    move v0, v9

    :cond_21
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 970
    .end local v1    # "_arg0":Landroid/os/Bundle;
    .end local v8    # "_result":Z
    :cond_22
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/os/Bundle;
    goto :goto_5

    .line 979
    .end local v1    # "_arg0":Landroid/os/Bundle;
    :sswitch_62
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 981
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 982
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 983
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 988
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_63
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 990
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 991
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForMediaStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 992
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 997
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_64
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 998
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mediaInit()V

    .line 999
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1004
    :sswitch_65
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1005
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mediaDeInit()V

    .line 1006
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1011
    :sswitch_66
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1013
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    .line 1014
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1020
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1022
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1023
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setDisplay(Landroid/view/Surface;II)V

    .line 1024
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1017
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    :cond_23
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_6

    .line 1029
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_67
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1031
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_24

    .line 1032
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1038
    .restart local v1    # "_arg0":Landroid/view/Surface;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1040
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1041
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setFarEndSurface(Landroid/view/Surface;II)V

    .line 1042
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1035
    .end local v1    # "_arg0":Landroid/view/Surface;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    :cond_24
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_7

    .line 1047
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_68
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1048
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedHeight()I

    move-result v8

    .line 1049
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1050
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1055
    .end local v8    # "_result":I
    :sswitch_69
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1056
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedWidth()I

    move-result v8

    .line 1057
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1058
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1063
    .end local v8    # "_result":I
    :sswitch_6a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1064
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNegotiatedFPS()I

    move-result v8

    .line 1065
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1066
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1071
    .end local v8    # "_result":I
    :sswitch_6b
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1072
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->isMediaReadyToReceivePreview()Z

    move-result v8

    .line 1073
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1074
    if-eqz v8, :cond_25

    move v0, v9

    :cond_25
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1079
    .end local v8    # "_result":Z
    :sswitch_6c
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1081
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_26

    move v1, v9

    .line 1082
    .local v1, "_arg0":Z
    :goto_8
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setIsMediaReadyToReceivePreview(Z)V

    .line 1083
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_26
    move v1, v0

    .line 1081
    goto :goto_8

    .line 1088
    :sswitch_6d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1089
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getNumberOfCameras()I

    move-result v8

    .line 1090
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1091
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1096
    .end local v8    # "_result":I
    :sswitch_6e
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1098
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1099
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->openCamera(I)Z

    move-result v8

    .line 1100
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1101
    if-eqz v8, :cond_27

    move v0, v9

    :cond_27
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1106
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_6f
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1110
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1111
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->openCameraEx(II)Z

    move-result v8

    .line 1112
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1113
    if-eqz v8, :cond_28

    move v0, v9

    :cond_28
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1118
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Z
    :sswitch_70
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_29

    .line 1121
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 1126
    .local v1, "_arg0":Landroid/view/Surface;
    :goto_9
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startCameraPreview(Landroid/view/Surface;)V

    .line 1127
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1124
    .end local v1    # "_arg0":Landroid/view/Surface;
    :cond_29
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/view/Surface;
    goto :goto_9

    .line 1132
    .end local v1    # "_arg0":Landroid/view/Surface;
    :sswitch_71
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1133
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->stopCameraPreview()V

    .line 1134
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1139
    :sswitch_72
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1140
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->closeCamera()V

    .line 1141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1146
    :sswitch_73
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getBackCameraId()I

    move-result v8

    .line 1148
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1149
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1154
    .end local v8    # "_result":I
    :sswitch_74
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1155
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getFrontCameraId()I

    move-result v8

    .line 1156
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1157
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1162
    .end local v8    # "_result":I
    :sswitch_75
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1163
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCameraState()I

    move-result v8

    .line 1164
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1165
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1170
    .end local v8    # "_result":I
    :sswitch_76
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1171
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCameraDirection()I

    move-result v8

    .line 1172
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1173
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1178
    .end local v8    # "_result":I
    :sswitch_77
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1179
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCameraDisplayOrientation()V

    .line 1180
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1185
    :sswitch_78
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1188
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCameraDisplayOrientationEx(I)V

    .line 1189
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1194
    .end local v1    # "_arg0":I
    :sswitch_79
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1195
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->switchCamera()V

    .line 1196
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1201
    :sswitch_7a
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2a

    move v1, v9

    .line 1204
    .local v1, "_arg0":Z
    :goto_a
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startRender(Z)V

    .line 1205
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_2a
    move v1, v0

    .line 1203
    goto :goto_a

    .line 1210
    :sswitch_7b
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1211
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->swapVideoSurface()V

    .line 1212
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1217
    :sswitch_7c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1218
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resetCameraID()V

    .line 1219
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1224
    :sswitch_7d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1226
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1228
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1230
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1231
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendStillImage(Ljava/lang/String;ILjava/lang/String;)V

    .line 1232
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1237
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_7e
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1238
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendLiveVideo()V

    .line 1239
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1244
    :sswitch_7f
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1246
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2b

    move v1, v9

    .line 1248
    .local v1, "_arg0":Z
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1249
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->captureSurfaceImage(ZI)V

    .line 1250
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":I
    :cond_2b
    move v1, v0

    .line 1246
    goto :goto_b

    .line 1255
    :sswitch_80
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1257
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2c

    move v1, v9

    .line 1258
    .restart local v1    # "_arg0":Z
    :goto_c
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deinitSurface(Z)V

    .line 1259
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_2c
    move v1, v0

    .line 1257
    goto :goto_c

    .line 1264
    :sswitch_81
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1265
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->holdVideo()V

    .line 1266
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1271
    :sswitch_82
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1272
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->resumeVideo()V

    .line 1273
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1278
    :sswitch_83
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1281
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->startVideo(I)V

    .line 1282
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1287
    .end local v1    # "_arg0":I
    :sswitch_84
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1289
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1290
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1291
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1296
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_85
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1298
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1299
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForPresenceStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1300
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1305
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_86
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1307
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1309
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2e

    .line 1310
    sget-object v10, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 1315
    .local v2, "_arg1":Landroid/os/Bundle;
    :goto_d
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->publishCapabilitiesAndAvailability(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v8

    .line 1316
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1317
    if-eqz v8, :cond_2d

    move v0, v9

    :cond_2d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1313
    .end local v2    # "_arg1":Landroid/os/Bundle;
    .end local v8    # "_result":Z
    :cond_2e
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/Bundle;
    goto :goto_d

    .line 1322
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Landroid/os/Bundle;
    :sswitch_87
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1323
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->unpublishCapabilitiesAndAvailability()Z

    move-result v8

    .line 1324
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1325
    if-eqz v8, :cond_2f

    move v0, v9

    :cond_2f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1330
    .end local v8    # "_result":Z
    :sswitch_88
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1332
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 1334
    .local v1, "_arg0":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1335
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getRemoteCapabilitiesAndAvailability([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 1336
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1337
    if-eqz v8, :cond_30

    move v0, v9

    :cond_30
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1342
    .end local v1    # "_arg0":[Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_89
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1344
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1346
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1347
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerServiceCapabilityAvailabilityTemplate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 1348
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1349
    if-eqz v8, :cond_31

    move v0, v9

    :cond_31
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1354
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_8a
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1356
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1358
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1360
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1362
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1363
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->contactSvcCallFunction(IIILjava/lang/String;)V

    .line 1364
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1369
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    :sswitch_8b
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1371
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1372
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1373
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1378
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_8c
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1380
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1381
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSMSStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1382
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1387
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_8d
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1389
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 1391
    .local v1, "_arg0":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1393
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1395
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1396
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendSMSOverIMS([BLjava/lang/String;Ljava/lang/String;I)V

    .line 1397
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1402
    .end local v1    # "_arg0":[B
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    :sswitch_8e
    const-string/jumbo v10, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1404
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_32

    move v1, v9

    .line 1406
    .local v1, "_arg0":Z
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1407
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendSMSResponse(ZI)V

    .line 1408
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    .end local v2    # "_arg1":I
    :cond_32
    move v1, v0

    .line 1404
    goto :goto_e

    .line 1413
    :sswitch_8f
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1415
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 1416
    .local v1, "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->sendDeliverReport([B)V

    .line 1417
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1422
    .end local v1    # "_arg0":[B
    :sswitch_90
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1424
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1425
    .local v1, "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->registerForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1426
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1431
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_91
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1433
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;

    move-result-object v1

    .line 1434
    .restart local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->deRegisterForSSConfigStateChange(Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;)V

    .line 1435
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 1440
    .end local v1    # "_arg0":Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener;
    :sswitch_92
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1442
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1444
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1445
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallWaiting(II)I

    move-result v8

    .line 1446
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1447
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1452
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_93
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1454
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1455
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallWaiting(I)I

    move-result v8

    .line 1456
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1457
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1462
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_94
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1464
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1466
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1468
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1470
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1472
    .restart local v4    # "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 1474
    .restart local v5    # "_arg4":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 1476
    .local v6, "_arg5":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .local v7, "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 1477
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallForwarding(IIIIIILjava/lang/String;)I

    move-result v8

    .line 1478
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1479
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1484
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v6    # "_arg5":I
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_95
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1486
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1488
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1489
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallForwarding(II)I

    move-result v8

    .line 1490
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1491
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1496
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_96
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1498
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1500
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1502
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1504
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1505
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->setCallBarring(IIILjava/lang/String;)I

    move-result v8

    .line 1506
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1507
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1512
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v8    # "_result":I
    :sswitch_97
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1514
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1516
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1517
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->getCallBarring(II)I

    move-result v8

    .line 1518
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1519
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1524
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_98
    const-string/jumbo v0, "com.samsung.commonimsinterface.imscommon.internal.IIMSService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1526
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1528
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1530
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1532
    .restart local v3    # "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1534
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 1535
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSService$Stub;->mmSS_Svc_Api(IIILjava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 1536
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1537
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5c -> :sswitch_5c
        0x5d -> :sswitch_5d
        0x5e -> :sswitch_5e
        0x5f -> :sswitch_5f
        0x60 -> :sswitch_60
        0x61 -> :sswitch_61
        0x62 -> :sswitch_62
        0x63 -> :sswitch_63
        0x64 -> :sswitch_64
        0x65 -> :sswitch_65
        0x66 -> :sswitch_66
        0x67 -> :sswitch_67
        0x68 -> :sswitch_68
        0x69 -> :sswitch_69
        0x6a -> :sswitch_6a
        0x6b -> :sswitch_6b
        0x6c -> :sswitch_6c
        0x6d -> :sswitch_6d
        0x6e -> :sswitch_6e
        0x6f -> :sswitch_6f
        0x70 -> :sswitch_70
        0x71 -> :sswitch_71
        0x72 -> :sswitch_72
        0x73 -> :sswitch_73
        0x74 -> :sswitch_74
        0x75 -> :sswitch_75
        0x76 -> :sswitch_76
        0x77 -> :sswitch_77
        0x78 -> :sswitch_78
        0x79 -> :sswitch_79
        0x7a -> :sswitch_7a
        0x7b -> :sswitch_7b
        0x7c -> :sswitch_7c
        0x7d -> :sswitch_7d
        0x7e -> :sswitch_7e
        0x7f -> :sswitch_7f
        0x80 -> :sswitch_80
        0x81 -> :sswitch_81
        0x82 -> :sswitch_82
        0x83 -> :sswitch_83
        0x84 -> :sswitch_84
        0x85 -> :sswitch_85
        0x86 -> :sswitch_86
        0x87 -> :sswitch_87
        0x88 -> :sswitch_88
        0x89 -> :sswitch_89
        0x8a -> :sswitch_8a
        0x8b -> :sswitch_8b
        0x8c -> :sswitch_8c
        0x8d -> :sswitch_8d
        0x8e -> :sswitch_8e
        0x8f -> :sswitch_8f
        0x90 -> :sswitch_90
        0x91 -> :sswitch_91
        0x92 -> :sswitch_92
        0x93 -> :sswitch_93
        0x94 -> :sswitch_94
        0x95 -> :sswitch_95
        0x96 -> :sswitch_96
        0x97 -> :sswitch_97
        0x98 -> :sswitch_98
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
