.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener;
.super Ljava/lang/Object;
.source "IIMSSSConfigStateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$CBType;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$CFType;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$CFURIType;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$SSClass;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$SSMode;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSSSConfigStateListener$SSStatus;
    }
.end annotation


# virtual methods
.method public abstract onGetCallBarringResponse(Z[II[I[I[Ljava/lang/String;)V
.end method

.method public abstract onGetCallForwardingResponse(Z[II[I[I[I[I[I[Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method public abstract onGetCallWaitingResponse(ZI[I[ILjava/lang/String;)V
.end method

.method public abstract onSetCallBarringResponse(ZLjava/lang/String;)V
.end method

.method public abstract onSetCallForwardingResponse(ZLjava/lang/String;)V
.end method

.method public abstract onSetCallWaitingResponse(ZLjava/lang/String;)V
.end method
