.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GENERAL"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL$FIELD;
    }
.end annotation


# static fields
.field public static final NETWORK_TYPE_EMERGENCY:I = 0x4

.field public static final NETWORK_TYPE_IMS:I = 0x3

.field public static final NETWORK_TYPE_WIFI:I = 0x2

.field public static final RCS_REGISTERED:I = 0x4

.field public static final SMS_FORMAT_3GPP:Ljava/lang/String; = "3GPP"

.field public static final SMS_FORMAT_3GPP2:Ljava/lang/String; = "3GPP2"

.field public static final SMS_OVER_IMS_REGISTERED:I = 0x2

.field public static final VOLTE_REGISTERED:I = 0x1

.field public static final VT_REGISTERED:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
