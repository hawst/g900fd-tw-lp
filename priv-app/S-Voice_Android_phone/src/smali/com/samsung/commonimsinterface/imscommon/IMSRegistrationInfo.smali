.class public Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;
.super Ljava/lang/Object;
.source "IMSRegistrationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo$ECMP_MODE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mECMPMode:I

.field private mErrorCode:I

.field private mErrorMessage:Ljava/lang/String;

.field private mExpiryTime:I

.field private mFeatureMask:I

.field private mIMSCkIk:Ljava/lang/String;

.field private mLimitedMode:I

.field private mLocalProfileUri:Ljava/lang/String;

.field private mNetworkType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 192
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->initialize()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIILjava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1, "localProfileUri"    # Ljava/lang/String;
    .param p2, "expiryTime"    # I
    .param p3, "featureMask"    # I
    .param p4, "networkType"    # I
    .param p5, "ecmpmode"    # I
    .param p6, "imsckik"    # Ljava/lang/String;
    .param p7, "limitedMode"    # I
    .param p8, "errorCode"    # I
    .param p9, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    .line 49
    iput p2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    .line 50
    iput p3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    .line 51
    iput p4, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    .line 52
    iput p5, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    .line 53
    iput-object p6, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    .line 54
    iput p7, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    .line 55
    iput p8, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    .line 56
    iput-object p9, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private final initialize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 72
    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    .line 73
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    .line 74
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    .line 75
    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    .line 76
    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    .line 77
    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    .line 78
    iput v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    .line 79
    iput v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    .line 80
    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    .line 81
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 157
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 159
    .local v0, "clone":Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 160
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 164
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 168
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    .line 171
    :cond_2
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method

.method public getECMPMode()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    return v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiryTime()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    return v0
.end method

.method public getFeatureMask()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    return v0
.end method

.method public getIMSCkIk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    return-object v0
.end method

.method public getLimitedMode()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    return v0
.end method

.method public getLocalProfileUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkType()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    return v0
.end method

.method public setECMPMode(I)V
    .locals 0
    .param p1, "ecmpmode"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    .line 117
    return-void
.end method

.method public setErrorCode(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    .line 141
    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setExpiryTime(I)V
    .locals 0
    .param p1, "expiryTime"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    .line 93
    return-void
.end method

.method public setFeatureMask(I)V
    .locals 0
    .param p1, "featureMask"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    .line 101
    return-void
.end method

.method public setIMSCkIk(Ljava/lang/String;)V
    .locals 0
    .param p1, "ckik"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setLimitedMode(I)V
    .locals 0
    .param p1, "limitedMode"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    .line 133
    return-void
.end method

.method public setLocalProfileUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "localProfileUri"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setNetworkType(I)V
    .locals 0
    .param p1, "networkType"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    .line 109
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLocalProfileUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mExpiryTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 183
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mFeatureMask:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mNetworkType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mECMPMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mIMSCkIk:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mLimitedMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    iget v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 190
    return-void
.end method
