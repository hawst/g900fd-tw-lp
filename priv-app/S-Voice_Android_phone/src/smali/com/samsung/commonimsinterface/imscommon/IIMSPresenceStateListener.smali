.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSPresenceStateListener;
.super Ljava/lang/Object;
.source "IIMSPresenceStateListener.java"


# static fields
.field public static final ERROR:I = -0xd

.field public static final NO_ERROR:I


# virtual methods
.method public abstract onCapabilityAndAvailabilityPublished(ILjava/lang/String;)V
.end method

.method public abstract onCapabilityAndAvailabilityReceived(I[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;)V
.end method

.method public abstract onCapabilityAndAvailabilityReceived(I[Lcom/samsung/commonimsinterface/imscommon/IMSUserProfile;Ljava/lang/String;)V
.end method

.method public abstract onPresenceAppRegSuccess(Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;)V
.end method

.method public abstract onServiceCapabilityAndAvailabilityReceived([Ljava/lang/String;)V
.end method
