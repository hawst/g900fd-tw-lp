.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;
.super Ljava/lang/Object;
.source "IMSServiceConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;
    }
.end annotation


# static fields
.field private static final IMS_SERVICE_KEY_BIND:Ljava/lang/String; = "BIND"

.field private static final IMS_SERVICE_KEY_INSTANCE:Ljava/lang/String; = "INSTANCE"

.field private static final IMS_SERVICE_KEY_INTERFACE:Ljava/lang/String; = "INTERFACE"

.field private static final IMS_SERVICE_KEY_LISTENER:Ljava/lang/String; = "LISTENER"

.field private static final IMS_SERVICE_KEY_NAME:Ljava/lang/String; = "NAME"

.field private static final IMS_SERVICE_RECONNECT_COUNT:I = 0x8

.field private static final IMS_SERVICE_RECONNECT_INTERNAL:I = 0x1f4

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

.field private mIMSServiceInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    .line 37
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 46
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->unbindService()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->bindService()V

    return-void
.end method

.method private bindService()V
    .locals 4

    .prologue
    .line 126
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bindService to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v3, "NAME"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    invoke-direct {v0, p0, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 130
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "bindService : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v3, "NAME"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 134
    const/4 v3, 0x1

    .line 132
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 135
    return-void
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 106
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "startService"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "INSTANCE"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    const/4 v0, 0x0

    .line 113
    .local v0, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "LISTENER"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .line 114
    .restart local v0    # "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;
    invoke-interface {v0}, Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;->onConnected()V

    .line 115
    return-void
.end method

.method private unbindService()V
    .locals 3

    .prologue
    .line 118
    sget-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "unbindService : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceConnection:Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager$IMSServiceConnection;

    .line 123
    return-void
.end method


# virtual methods
.method public getServiceInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v1, "INSTANCE"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Landroid/os/IBinder;
    .locals 6
    .param p1, "serviceName"    # Ljava/lang/String;

    .prologue
    .line 73
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "getSystemService : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v2, 0x0

    .line 77
    .local v2, "service":Landroid/os/IBinder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v3, 0x8

    if-lt v1, v3, :cond_0

    .line 101
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Can\'t connect to IMS Service ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 78
    :cond_0
    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 80
    if-eqz v2, :cond_1

    .line 81
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Connected to IMS Service ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :try_start_0
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "System Services : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/ServiceManager;->listServices()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object v3, v2

    .line 89
    goto :goto_1

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 93
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    sget-object v3, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "Waiting for IMS Service Connection..."

    invoke-static {v3, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-wide/16 v3, 0x1f4

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 77
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :catch_1
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3
.end method

.method public startIMSService(Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;)V
    .locals 5
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "serviceInterface"    # Ljava/lang/String;
    .param p3, "serviceBind"    # Z
    .param p4, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSServiceConnectionListener;

    .prologue
    .line 53
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "startIMSService : [NAME] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 54
    const-string/jumbo v3, ", ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "INTERFACE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 55
    const-string/jumbo v3, ", ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "BIND"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 56
    const-string/jumbo v3, ", ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "LISTENER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->isTestBinding(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "Test"

    .line 60
    .local v0, "testBinding":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "NAME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "INTERFACE"

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "BIND"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->mIMSServiceInfo:Ljava/util/HashMap;

    const-string/jumbo v2, "LISTENER"

    invoke-virtual {v1, v2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    if-eqz p3, :cond_1

    .line 66
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->bindService()V

    .line 70
    :goto_1
    return-void

    .line 58
    .end local v0    # "testBinding":Ljava/lang/String;
    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    .line 68
    .restart local v0    # "testBinding":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSServiceConnectionManager;->startService()V

    goto :goto_1
.end method
