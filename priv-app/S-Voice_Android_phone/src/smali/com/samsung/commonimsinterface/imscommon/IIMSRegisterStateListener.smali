.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSRegisterStateListener;
.super Ljava/lang/Object;
.source "IIMSRegisterStateListener.java"


# virtual methods
.method public abstract onEmergencyRegistrationDone(II)V
.end method

.method public abstract onEmergencyRegistrationFailed()V
.end method

.method public abstract onRegistering(Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;)V
.end method

.method public abstract onRegistrationDone(Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;)V
.end method

.method public abstract onRegistrationFailed(Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;)V
.end method
