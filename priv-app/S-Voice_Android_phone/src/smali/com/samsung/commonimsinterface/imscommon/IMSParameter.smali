.class public Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
.super Ljava/lang/Object;
.source "IMSParameter.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$CALL;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$GENERAL;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$MEDIA;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$PRESENCE;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$SMS;,
        Lcom/samsung/commonimsinterface/imscommon/IMSParameter$SSCONFIG;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSParameter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

.field private mPrimitiveMap:Landroid/os/Bundle;

.field private mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

.field private mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 13
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 14
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 15
    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 13
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 14
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 15
    iput-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 111
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .line 115
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 116
    .local v1, "size":I
    if-lez v1, :cond_2

    .line 117
    new-array v2, v1, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_3

    .line 123
    .end local v0    # "i":I
    :cond_2
    return-void

    .line 120
    .restart local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    const-class v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    aput-object v2, v3, v0

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 257
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 259
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 265
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":Z
    :goto_0
    return p2

    .line 263
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":Z
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getByteArray(Ljava/lang/String;)[B
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 219
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 221
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 227
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 225
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [B
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 227
    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 134
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 136
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 142
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":I
    :goto_0
    return p2

    .line 140
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":I
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getIntArray(Ljava/lang/String;)[I
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 236
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 238
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 244
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 242
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 244
    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 151
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # J

    .prologue
    .line 155
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 157
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 163
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":J
    :goto_0
    return-wide p2

    .line 161
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":J
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/Long;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getParcelable(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 288
    const-string/jumbo v1, "apcsinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 300
    :cond_0
    :goto_0
    return-object v0

    .line 290
    :cond_1
    const-string/jumbo v1, "registrationinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 291
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    goto :goto_0

    .line 294
    :cond_2
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 296
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 297
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParcelableArray(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 322
    const-string/jumbo v3, "profileparams"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 333
    :goto_0
    return-object v1

    .line 325
    :cond_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 327
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_1

    move-object v1, v2

    .line 328
    goto :goto_0

    .line 331
    :cond_1
    :try_start_0
    check-cast v1, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 333
    goto :goto_0
.end method

.method public getSparseStringArray(Ljava/lang/String;)Landroid/util/SparseArray;
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 346
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getParcelable(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 347
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    move-object v3, v4

    .line 360
    :cond_0
    :goto_0
    return-object v3

    .line 351
    :cond_1
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 352
    .local v3, "value":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 354
    .local v1, "bundleKey":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 355
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/NumberFormatException;
    move-object v3, v4

    .line 356
    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 172
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 174
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 180
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 178
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 180
    goto :goto_0
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 185
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 187
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 193
    .end local v1    # "o":Ljava/lang/Object;
    .end local p2    # "defaultValue":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 191
    .restart local v1    # "o":Ljava/lang/Object;
    .restart local p2    # "defaultValue":Ljava/lang/String;
    :cond_0
    :try_start_0
    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "o":Ljava/lang/Object;
    move-object p2, v1

    goto :goto_0

    .line 192
    .restart local v1    # "o":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public getStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 202
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v3, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 204
    .local v1, "o":Ljava/lang/Object;
    if-nez v1, :cond_0

    move-object v1, v2

    .line 210
    .end local v1    # "o":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 208
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_0
    :try_start_0
    check-cast v1, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/ClassCastException;
    move-object v1, v2

    .line 210
    goto :goto_0
.end method

.method public putBoolean(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 250
    return-void
.end method

.method public putByteArray(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 216
    return-void
.end method

.method public putInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 127
    return-void
.end method

.method public putIntArray(Ljava/lang/String;[I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [I

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 233
    return-void
.end method

.method public putLong(Ljava/lang/String;J)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 148
    return-void
.end method

.method public putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Landroid/os/Parcelable;

    .prologue
    .line 270
    const-string/jumbo v1, "apcsinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    if-eqz v1, :cond_0

    .line 272
    :try_start_0
    check-cast p2, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .end local p2    # "value":Landroid/os/Parcelable;
    invoke-virtual {p2}, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-void

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 276
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local p2    # "value":Landroid/os/Parcelable;
    :cond_0
    const-string/jumbo v1, "registrationinfo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    if-eqz v1, :cond_1

    .line 278
    :try_start_1
    check-cast p2, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    .end local p2    # "value":Landroid/os/Parcelable;
    invoke-virtual {p2}, Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 279
    :catch_1
    move-exception v0

    .line 280
    .restart local v0    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 283
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local p2    # "value":Landroid/os/Parcelable;
    :cond_1
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Landroid/os/Parcelable;

    .prologue
    .line 307
    const-string/jumbo v2, "profileparams"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    instance-of v2, p2, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    if-eqz v2, :cond_1

    .line 309
    :try_start_0
    array-length v2, p2

    new-array v2, v2, [Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    iput-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    .line 310
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-lt v1, v2, :cond_0

    .line 319
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 311
    .restart local v1    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    aget-object v2, p2, v1

    check-cast v2, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    invoke-virtual {v2}, Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    move-result-object v2

    aput-object v2, v3, v1
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 313
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_1

    .line 317
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_1
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v2, p1, p2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public putSparseStringArray(Ljava/lang/String;Landroid/util/SparseArray;)V
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p2, "value":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 339
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 342
    invoke-virtual {p0, p1, v0}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 343
    return-void

    .line 340
    :cond_0
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public putString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 370
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mPrimitiveMap:Landroid/os/Bundle;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 372
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    if-nez v2, :cond_1

    .line 373
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    :goto_0
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    if-nez v2, :cond_2

    .line 380
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 386
    :goto_1
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    if-nez v2, :cond_3

    .line 387
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 394
    :cond_0
    return-void

    .line 375
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 376
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mAPCSInfo:Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 383
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mRegistrationInfo:Lcom/samsung/commonimsinterface/imscommon/IMSRegistrationInfo;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 389
    :cond_3
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    array-length v2, v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 390
    iget-object v2, p0, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->mProfileParams:[Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 391
    .local v0, "profile":Lcom/samsung/commonimsinterface/imscommon/IMSProfileParams;
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 390
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
