.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;
.super Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;
.source "IMSRemoteListenerStub.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

.field private mIntArrayList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IIMSListener;",
            "[I>;"
        }
    .end annotation
.end field

.field private mListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IIMSListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IIMSRemoteListener$Stub;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    .line 28
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    invoke-direct {v0, p0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;)V

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private blockCallEventToNativeApp(I)Z
    .locals 2
    .param p1, "action"    # I

    .prologue
    .line 154
    invoke-static {}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->getInstance()Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/commonimsinterface/imsconfig/IMSConfig;->ignoreNotifyMTCallEvent2NativeCallApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.sec.ims"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-direct {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->isCallAction(I)Z

    move-result v0

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCallAction(I)Z
    .locals 1
    .param p1, "action"    # I

    .prologue
    .line 163
    const/16 v0, 0x7d0

    if-le p1, v0, :cond_0

    const/16 v0, 0xbb8

    if-ge p1, v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 67
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;[I)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    .param p2, "requiredFields"    # [I

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->addListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected handleMessage(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 64
    return-void
.end method

.method public onReceive(IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V
    .locals 15
    .param p1, "token"    # I
    .param p2, "action"    # I
    .param p3, "parameter"    # Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 101
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onReceive : token ["

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "] action ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "] parameter ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    :goto_1
    return-void

    .line 103
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .line 104
    .local v3, "listener":Lcom/samsung/commonimsinterface/imscommon/IIMSListener;
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 105
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->blockCallEventToNativeApp(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 106
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Ignore notifying to native call app : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, " , action : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_2
    iget-object v14, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$1;

    move-object v2, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$1;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;Lcom/samsung/commonimsinterface/imscommon/IIMSListener;IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    invoke-virtual {v14, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 116
    :cond_3
    const/16 v1, 0x3f1

    move/from16 v0, p2

    if-ne v0, v1, :cond_0

    .line 117
    const-string/jumbo v1, "settingsvalue"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->getSparseStringArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v12

    .line 119
    .local v12, "values":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-nez v12, :cond_4

    .line 120
    sget-object v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v2, "getSparseStringArray() value is null"

    invoke-static {v1, v2}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :cond_4
    iget-object v1, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [I

    .line 125
    .local v7, "fields":[I
    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    .line 127
    .local v10, "result":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .local v8, "index":I
    invoke-virtual {v12}, Landroid/util/SparseArray;->size()I

    move-result v11

    .local v11, "size":I
    :goto_2
    if-lt v8, v11, :cond_5

    .line 136
    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 137
    new-instance v6, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;

    invoke-direct {v6}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;-><init>()V

    .line 139
    .local v6, "newParamter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    const-string/jumbo v1, "settingsvalue"

    invoke-virtual {v6, v1, v10}, Lcom/samsung/commonimsinterface/imscommon/IMSParameter;->putSparseStringArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 141
    iget-object v14, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mHandler:Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;

    new-instance v1, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$2;

    move-object v2, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$2;-><init>(Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;Lcom/samsung/commonimsinterface/imscommon/IIMSListener;IILcom/samsung/commonimsinterface/imscommon/IMSParameter;)V

    invoke-virtual {v14, v1}, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub$InnerHandler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 128
    .end local v6    # "newParamter":Lcom/samsung/commonimsinterface/imscommon/IMSParameter;
    :cond_5
    array-length v2, v7

    const/4 v1, 0x0

    :goto_3
    if-lt v1, v2, :cond_6

    .line 127
    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 128
    :cond_6
    aget v9, v7, v1

    .line 129
    .local v9, "keyValue":I
    invoke-virtual {v12, v8}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    if-ne v9, v4, :cond_7

    .line 130
    invoke-virtual {v12, v8}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v12, v8}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v10, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_4

    .line 128
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public removeListener(Lcom/samsung/commonimsinterface/imscommon/IIMSListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/commonimsinterface/imscommon/IIMSListener;

    .prologue
    .line 86
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/commonimsinterface/imscommon/internal/IMSRemoteListenerStub;->mIntArrayList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_0
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
