.class public Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;
.super Ljava/lang/Object;
.source "IMSAPCSInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mServiceStatus:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo$1;

    invoke-direct {v0}, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 19
    invoke-direct {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->initialize()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean v1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 28
    return-void

    :cond_0
    move v0, v1

    .line 27
    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "serviceStatus"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 23
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 24
    return-void
.end method

.method private final initialize()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 32
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    .line 46
    .local v0, "clone":Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->clone()Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public getServiceStatus()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    return v0
.end method

.method public setServiceStatus(Z)V
    .locals 0
    .param p1, "serviceStatus"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    .line 36
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/commonimsinterface/imscommon/IMSAPCSInfo;->mServiceStatus:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
