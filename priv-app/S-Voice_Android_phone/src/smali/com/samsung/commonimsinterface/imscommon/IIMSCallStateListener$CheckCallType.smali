.class public Lcom/samsung/commonimsinterface/imscommon/IIMSCallStateListener$CheckCallType;
.super Ljava/lang/Object;
.source "IIMSCallStateListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSCallStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CheckCallType"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasCallType(I)Z
    .locals 1
    .param p0, "callType"    # I

    .prologue
    .line 24
    const/4 v0, 0x2

    if-eq v0, p0, :cond_0

    .line 25
    const/16 v0, 0x15

    if-eq v0, p0, :cond_0

    .line 26
    const/4 v0, 0x3

    if-eq v0, p0, :cond_0

    .line 27
    const/4 v0, 0x4

    if-eq v0, p0, :cond_0

    .line 28
    const/4 v0, 0x5

    if-eq v0, p0, :cond_0

    .line 29
    const/4 v0, 0x6

    if-eq v0, p0, :cond_0

    .line 30
    const/16 v0, 0xf

    if-eq v0, p0, :cond_0

    .line 31
    const/16 v0, 0xa

    if-eq v0, p0, :cond_0

    .line 32
    const/4 v0, 0x7

    if-eq v0, p0, :cond_0

    .line 33
    const/16 v0, 0x9

    if-eq v0, p0, :cond_0

    .line 34
    const/16 v0, 0xb

    if-eq v0, p0, :cond_0

    .line 35
    const/16 v0, 0xc

    if-eq v0, p0, :cond_0

    .line 36
    const/16 v0, 0xd

    if-eq v0, p0, :cond_0

    .line 37
    const/16 v0, 0x14

    if-eq v0, p0, :cond_0

    .line 38
    const/16 v0, 0xe

    if-eq v0, p0, :cond_0

    .line 39
    const/16 v0, 0x16

    if-eq v0, p0, :cond_0

    .line 40
    const/16 v0, 0x17

    if-eq v0, p0, :cond_0

    .line 41
    const/16 v0, 0x18

    if-eq v0, p0, :cond_0

    .line 42
    const/16 v0, 0x19

    if-eq v0, p0, :cond_0

    .line 43
    const/16 v0, 0x1a

    if-ne v0, p0, :cond_1

    .line 44
    :cond_0
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasMOCallType(I)Z
    .locals 1
    .param p0, "callType"    # I

    .prologue
    .line 50
    const/4 v0, 0x2

    if-eq v0, p0, :cond_0

    .line 51
    const/16 v0, 0x15

    if-eq v0, p0, :cond_0

    .line 52
    const/4 v0, 0x6

    if-eq v0, p0, :cond_0

    .line 53
    const/16 v0, 0xa

    if-eq v0, p0, :cond_0

    .line 54
    const/4 v0, 0x7

    if-eq v0, p0, :cond_0

    .line 55
    const/16 v0, 0x14

    if-eq v0, p0, :cond_0

    .line 56
    const/16 v0, 0xe

    if-eq v0, p0, :cond_0

    .line 57
    const/16 v0, 0x16

    if-eq v0, p0, :cond_0

    .line 58
    const/16 v0, 0x17

    if-eq v0, p0, :cond_0

    .line 59
    const/16 v0, 0x18

    if-eq v0, p0, :cond_0

    .line 60
    const/16 v0, 0x19

    if-eq v0, p0, :cond_0

    .line 61
    const/16 v0, 0x1a

    if-ne v0, p0, :cond_1

    .line 62
    :cond_0
    const/4 v0, 0x1

    .line 64
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasRejectOrEndCallType(I)Z
    .locals 1
    .param p0, "callType"    # I

    .prologue
    .line 68
    const/4 v0, 0x2

    if-eq v0, p0, :cond_0

    .line 69
    const/4 v0, 0x6

    if-eq v0, p0, :cond_0

    .line 70
    const/16 v0, 0xf

    if-eq v0, p0, :cond_0

    .line 71
    const/16 v0, 0xa

    if-eq v0, p0, :cond_0

    .line 72
    const/16 v0, 0x9

    if-eq v0, p0, :cond_0

    .line 73
    const/16 v0, 0x14

    if-eq v0, p0, :cond_0

    .line 74
    const/16 v0, 0x16

    if-eq v0, p0, :cond_0

    .line 75
    const/16 v0, 0x17

    if-eq v0, p0, :cond_0

    .line 76
    const/16 v0, 0x18

    if-eq v0, p0, :cond_0

    .line 77
    const/16 v0, 0x19

    if-eq v0, p0, :cond_0

    .line 78
    const/16 v0, 0x1a

    if-ne v0, p0, :cond_1

    .line 79
    :cond_0
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVideoCall(I)Z
    .locals 1
    .param p0, "callType"    # I

    .prologue
    .line 85
    const/4 v0, 0x3

    if-eq v0, p0, :cond_0

    .line 86
    const/4 v0, 0x4

    if-eq v0, p0, :cond_0

    .line 87
    const/4 v0, 0x6

    if-eq v0, p0, :cond_0

    .line 88
    const/16 v0, 0xf

    if-eq v0, p0, :cond_0

    .line 89
    const/16 v0, 0xa

    if-eq v0, p0, :cond_0

    .line 90
    const/4 v0, 0x7

    if-eq v0, p0, :cond_0

    .line 91
    const/16 v0, 0x9

    if-eq v0, p0, :cond_0

    .line 92
    const/16 v0, 0x16

    if-eq v0, p0, :cond_0

    .line 93
    const/16 v0, 0x17

    if-eq v0, p0, :cond_0

    .line 94
    const/16 v0, 0x18

    if-eq v0, p0, :cond_0

    .line 95
    const/16 v0, 0x19

    if-eq v0, p0, :cond_0

    .line 96
    const/16 v0, 0x1a

    if-ne v0, p0, :cond_1

    .line 97
    :cond_0
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
