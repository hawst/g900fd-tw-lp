.class public abstract Lcom/samsung/commonimsinterface/imscommon/internal/IMSSMSStateAdapter;
.super Ljava/lang/Object;
.source "IMSSMSStateAdapter.java"

# interfaces
.implements Lcom/samsung/commonimsinterface/imscommon/IIMSSMSStateListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveDeviceStorage(Z)V
    .locals 0
    .param p1, "isStorageFull"    # Z

    .prologue
    .line 13
    return-void
.end method

.method public onReceiveIncomingSMS(ILjava/lang/String;[B)V
    .locals 0
    .param p1, "msgID"    # I
    .param p2, "contentType"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    .line 8
    return-void
.end method

.method public onReceiveNotiInfo(IILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "msgID"    # I
    .param p2, "reasonCode"    # I
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "data"    # [B
    .param p5, "callId"    # Ljava/lang/String;
    .param p6, "remoteAddr"    # Ljava/lang/String;

    .prologue
    .line 14
    return-void
.end method

.method public onReceiveSMSAck(IILjava/lang/String;[B)V
    .locals 0
    .param p1, "msgID"    # I
    .param p2, "reasonCode"    # I
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "data"    # [B

    .prologue
    .line 10
    return-void
.end method

.method public onReceiveSMSMessage(IILjava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "msgID"    # I
    .param p2, "reasonCode"    # I
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "data"    # [B
    .param p5, "callId"    # Ljava/lang/String;
    .param p6, "remoteAddr"    # Ljava/lang/String;

    .prologue
    .line 12
    return-void
.end method
