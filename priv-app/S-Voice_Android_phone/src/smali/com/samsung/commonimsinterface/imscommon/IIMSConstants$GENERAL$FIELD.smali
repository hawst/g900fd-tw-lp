.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL$FIELD;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$GENERAL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FIELD"
.end annotation


# static fields
.field public static final AMR_AUDIO_BITRATE:I = 0x1

.field public static final AMR_AUDIO_BITRATE_WB:I = 0x2

.field public static final AVAIL_CACHE_EXP:I = 0x10

.field public static final CAP_CACHE_EXP:I = 0xa

.field public static final CAP_POLL_INTERVAL:I = 0xb

.field public static final DCN_NUMBER:I = 0x1b

.field public static final EAB_SETTING:I = 0xf

.field public static final FQDN_FOR_PCSCF:I = 0x12

.field public static final GZIP_FLAG:I = 0x16

.field public static final IMS_TEST_MODE:I = 0x19

.field public static final LVC_BETA_SETTING:I = 0xe

.field public static final MAX:I = 0x1f

.field public static final MIN_SE:I = 0x1a

.field public static final PCSCF_DOMAIN:I = 0x0

.field public static final POLL_LIST_SUB_EXP:I = 0x13

.field public static final PREF_CSCF_PORT:I = 0x11

.field public static final PUBLISH_ERR_RETRY_TIMER:I = 0x1e

.field public static final PUBLISH_TIMER:I = 0x14

.field public static final PUBLISH_TIMER_EXTEND:I = 0x15

.field public static final SILENT_REDIAL_ENABLE:I = 0x1c

.field public static final SIP_SESSION_TIMER:I = 0x3

.field public static final SIP_T1_TIMER:I = 0x7

.field public static final SIP_T2_TIMER:I = 0x8

.field public static final SIP_TF_TIMER:I = 0x9

.field public static final SMS_FORMAT:I = 0x4

.field public static final SMS_OVER_IMS:I = 0x5

.field public static final SMS_WRITE_UICC:I = 0x6

.field public static final SRC_THROTTLE_PUBLISH:I = 0xc

.field public static final SUBSCRIBE_MAX_ENTRY:I = 0xd

.field public static final TIMER_VZW:I = 0x17

.field public static final T_DELAY:I = 0x18

.field public static final T_LTE_911_FAIL:I = 0x1d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static values()[I
    .locals 3

    .prologue
    const/16 v2, 0x1f

    .line 79
    new-array v1, v2, [I

    .line 80
    .local v1, "values":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 84
    return-object v1

    .line 81
    :cond_0
    aput v0, v1, v0

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
