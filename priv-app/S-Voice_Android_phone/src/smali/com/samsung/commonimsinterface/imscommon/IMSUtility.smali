.class public Lcom/samsung/commonimsinterface/imscommon/IMSUtility;
.super Ljava/lang/Object;
.source "IMSUtility.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MAX_USSD_LENGTH:I = 0xb6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertHashtableToBundle(Ljava/util/Hashtable;)Landroid/os/Bundle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<**>;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "map":Ljava/util/Hashtable;, "Ljava/util/Hashtable<**>;"
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 24
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    .line 25
    invoke-virtual {p0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    .line 27
    .local v1, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-nez v4, :cond_1

    .line 35
    .end local v1    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_0
    return-object v0

    .line 28
    .restart local v1    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<*>;"
    :cond_1
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 29
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static correctRightShift(BI)B
    .locals 1
    .param p0, "source"    # B
    .param p1, "size"    # I

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 85
    :goto_0
    return p0

    .line 75
    :pswitch_0
    and-int/lit8 v0, p0, 0x7f

    int-to-byte p0, v0

    goto :goto_0

    .line 76
    :pswitch_1
    and-int/lit8 v0, p0, 0x3f

    int-to-byte p0, v0

    goto :goto_0

    .line 77
    :pswitch_2
    and-int/lit8 v0, p0, 0x1f

    int-to-byte p0, v0

    goto :goto_0

    .line 78
    :pswitch_3
    and-int/lit8 v0, p0, 0xf

    int-to-byte p0, v0

    goto :goto_0

    .line 79
    :pswitch_4
    and-int/lit8 v0, p0, 0x7

    int-to-byte p0, v0

    goto :goto_0

    .line 80
    :pswitch_5
    and-int/lit8 v0, p0, 0x3

    int-to-byte p0, v0

    goto :goto_0

    .line 81
    :pswitch_6
    and-int/lit8 v0, p0, 0x1

    int-to-byte p0, v0

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getBitHex(B)Ljava/lang/String;
    .locals 5
    .param p0, "raw"    # B

    .prologue
    const/16 v4, 0x8

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 58
    .local v2, "result":Ljava/lang/StringBuilder;
    const/16 v0, 0x80

    .line 60
    .local v0, "compare":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-lt v1, v4, :cond_0

    .line 70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 61
    :cond_0
    and-int v3, p0, v0

    if-eqz v3, :cond_1

    .line 62
    const/16 v3, 0x31

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    :goto_1
    shr-int/lit8 v0, v0, 0x1

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_1
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static getCurrentTime()Ljava/lang/String;
    .locals 11

    .prologue
    .line 250
    const/4 v6, 0x0

    .line 251
    .local v6, "month":Ljava/lang/String;
    const/4 v2, 0x0

    .line 252
    .local v2, "day":Ljava/lang/String;
    const/4 v3, 0x0

    .line 253
    .local v3, "hour":Ljava/lang/String;
    const/4 v5, 0x0

    .line 254
    .local v5, "minute":Ljava/lang/String;
    const/4 v7, 0x0

    .line 255
    .local v7, "second":Ljava/lang/String;
    const/4 v4, 0x0

    .line 256
    .local v4, "milliSecond":Ljava/lang/String;
    const/4 v1, 0x0

    .line 257
    .local v1, "currentTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 259
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 260
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x5

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 261
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 262
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 263
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "00"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xd

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    .line 264
    new-instance v8, Ljava/text/DecimalFormat;

    const-string/jumbo v9, "000"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v9, 0xe

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 266
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 268
    return-object v1
.end method

.method public static getCurrentTimeShort()Ljava/lang/String;
    .locals 9

    .prologue
    .line 272
    const/4 v2, 0x0

    .line 273
    .local v2, "hour":Ljava/lang/String;
    const/4 v4, 0x0

    .line 274
    .local v4, "minute":Ljava/lang/String;
    const/4 v5, 0x0

    .line 275
    .local v5, "second":Ljava/lang/String;
    const/4 v3, 0x0

    .line 276
    .local v3, "milliSecond":Ljava/lang/String;
    const/4 v1, 0x0

    .line 277
    .local v1, "currentTime":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 279
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string/jumbo v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 280
    new-instance v6, Ljava/text/DecimalFormat;

    const-string/jumbo v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 281
    new-instance v6, Ljava/text/DecimalFormat;

    const-string/jumbo v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xd

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 282
    new-instance v6, Ljava/text/DecimalFormat;

    const-string/jumbo v7, "000"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xe

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 284
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 286
    return-object v1
.end method

.method public static getHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 39
    const-string/jumbo v2, "0123456789ABCDEF"

    .line 41
    .local v2, "reference":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 42
    const/4 v4, 0x0

    .line 53
    :goto_0
    return-object v4

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 48
    .local v0, "hex":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_1
    array-length v4, p0

    if-lt v1, v4, :cond_1

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 49
    :cond_1
    aget-byte v3, p0, v1

    .line 50
    .local v3, "token":B
    and-int/lit16 v4, v3, 0xf0

    shr-int/lit8 v4, v4, 0x4

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    and-int/lit8 v5, v3, 0xf

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 48
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getNumberFromURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 89
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Source : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    move-object v0, p0

    .line 93
    .local v0, "target":Ljava/lang/String;
    const-string/jumbo v2, "sip:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Source contains sip:"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string/jumbo v2, "sip:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v5

    .line 96
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "target : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 106
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "target : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 108
    .end local v0    # "target":Ljava/lang/String;
    .local v1, "target":Ljava/lang/String;
    :goto_1
    return-object v1

    .line 97
    .end local v1    # "target":Ljava/lang/String;
    .restart local v0    # "target":Ljava/lang/String;
    :cond_0
    const-string/jumbo v2, "tel:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "Source contains tel:"

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string/jumbo v2, "tel:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v5

    .line 100
    sget-object v2, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "target : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 102
    .end local v0    # "target":Ljava/lang/String;
    .restart local v1    # "target":Ljava/lang/String;
    goto :goto_1
.end method

.method public static packToGsm7bit([BI)[B
    .locals 12
    .param p0, "data"    # [B
    .param p1, "inLength"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x0

    .line 112
    const/4 v0, 0x0

    .line 113
    .local v0, "index":I
    const/4 v2, 0x0

    .line 114
    .local v2, "pos":I
    const/4 v4, 0x0

    .line 115
    .local v4, "shift":I
    const/4 v1, 0x0

    .line 116
    .local v1, "outLength":I
    move-object v5, p0

    .local v5, "source":[B
    move-object v6, v8

    .line 117
    check-cast v6, [B

    .local v6, "target":[B
    move-object v3, v8

    .line 118
    check-cast v3, [B

    .line 119
    .local v3, "result":[B
    const/4 v7, 0x0

    .line 121
    .local v7, "temp":B
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "packToGsm7bit Begin : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p1, :cond_2

    .line 132
    rem-int/lit8 v8, p1, 0x8

    if-nez v8, :cond_5

    .line 133
    mul-int/lit8 v8, p1, 0x7

    div-int/lit8 v1, v8, 0x8

    .line 138
    :goto_1
    new-array v6, v1, [B

    .line 140
    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_2
    if-lt v0, p1, :cond_6

    .line 163
    rem-int/lit8 v8, v1, 0x7

    if-nez v8, :cond_0

    .line 164
    add-int/lit8 v8, v1, -0x1

    aget-byte v8, v6, v8

    shr-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 166
    if-nez v7, :cond_0

    .line 167
    add-int/lit8 v8, v1, -0x1

    aget-byte v9, v6, v8

    or-int/lit8 v9, v9, 0x1a

    int-to-byte v9, v9

    aput-byte v9, v6, v8

    .line 171
    :cond_0
    new-array v3, v1, [B

    .line 173
    invoke-static {v6, v11, v3, v11, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "packToGsm7bit End : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v3

    .line 177
    .end local v6    # "target":[B
    :cond_1
    return-object v6

    .line 125
    .restart local v6    # "target":[B
    :cond_2
    aget-byte v8, v5, v0

    const/16 v9, 0x40

    if-ne v8, v9, :cond_4

    .line 126
    aput-byte v11, v5, v0

    .line 124
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_4
    aget-byte v8, v5, v0

    const/16 v9, 0x24

    if-ne v8, v9, :cond_3

    .line 128
    const/4 v8, 0x2

    aput-byte v8, v5, v0

    goto :goto_3

    .line 135
    :cond_5
    mul-int/lit8 v8, p1, 0x7

    div-int/lit8 v8, v8, 0x8

    add-int/lit8 v1, v8, 0x1

    goto :goto_1

    .line 141
    :cond_6
    const/16 v8, 0xb6

    if-ge v2, v8, :cond_1

    .line 146
    aget-byte v8, v5, v0

    shr-int/2addr v8, v4

    int-to-byte v8, v8

    invoke-static {v8, v4}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 148
    aput-byte v7, v6, v2

    .line 150
    add-int/lit8 v8, v0, 0x1

    if-ge v8, p1, :cond_7

    .line 152
    aget-byte v8, v6, v2

    add-int/lit8 v9, v0, 0x1

    aget-byte v9, v5, v9

    rsub-int/lit8 v10, v4, 0x7

    shl-int/2addr v9, v10

    int-to-byte v9, v9

    or-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v6, v2

    .line 154
    add-int/lit8 v4, v4, 0x1

    .line 156
    const/4 v8, 0x7

    if-ne v4, v8, :cond_7

    .line 157
    const/4 v4, 0x0

    .line 158
    add-int/lit8 v0, v0, 0x1

    .line 140
    :cond_7
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method

.method public static unPackToGsm7bit([BI)[B
    .locals 14
    .param p0, "data"    # [B
    .param p1, "inLength"    # I

    .prologue
    const/16 v13, 0x20

    const/4 v12, 0x0

    const/16 v11, 0xd

    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "index":I
    const/4 v2, 0x0

    .line 183
    .local v2, "pos":I
    const/4 v4, 0x0

    .line 184
    .local v4, "shift":I
    mul-int/lit8 v8, p1, 0x8

    div-int/lit8 v1, v8, 0x7

    .line 185
    .local v1, "outLength":I
    move-object v5, p0

    .line 186
    .local v5, "source":[B
    new-array v6, v1, [B

    .line 187
    .local v6, "target":[B
    const/4 v3, 0x0

    check-cast v3, [B

    .line 188
    .local v3, "result":[B
    const/4 v7, 0x0

    .line 190
    .local v7, "temp":B
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "unPackToGsm7bit Begin : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v0, 0x0

    :goto_0
    if-lt v2, p1, :cond_1

    .line 240
    :cond_0
    :goto_1
    new-array v3, v1, [B

    .line 242
    invoke-static {v6, v12, v3, v12, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "unPackToGsm7bit End : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->getHex([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    return-object v3

    .line 193
    :cond_1
    aget-byte v8, v5, v2

    shl-int/2addr v8, v4

    and-int/lit8 v8, v8, 0x7f

    int-to-byte v8, v8

    aput-byte v8, v6, v0

    .line 195
    if-eqz v2, :cond_2

    .line 199
    add-int/lit8 v8, v2, -0x1

    aget-byte v8, v5, v8

    rsub-int/lit8 v9, v4, 0x8

    shr-int/2addr v8, v9

    int-to-byte v8, v8

    rsub-int/lit8 v9, v4, 0x8

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 201
    aget-byte v8, v6, v0

    or-int/2addr v8, v7

    int-to-byte v8, v8

    aput-byte v8, v6, v0

    .line 204
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_2

    add-int/lit8 v8, v0, 0x1

    if-ge v8, v1, :cond_2

    .line 205
    aput-byte v13, v6, v0

    .line 210
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 212
    const/4 v8, 0x7

    if-ne v4, v8, :cond_4

    .line 213
    const/4 v4, 0x0

    .line 216
    add-int/lit8 v0, v0, 0x1

    .line 218
    aget-byte v8, v5, v2

    shr-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->correctRightShift(BI)B

    move-result v7

    .line 220
    aput-byte v7, v6, v0

    .line 226
    aget-byte v8, v6, v0

    if-nez v8, :cond_3

    add-int/lit8 v8, v0, 0x1

    if-eq v8, v1, :cond_0

    .line 229
    :cond_3
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_5

    add-int/lit8 v8, v0, 0x1

    if-ge v8, v1, :cond_5

    .line 230
    aput-byte v13, v6, v0

    .line 192
    :cond_4
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_5
    aget-byte v8, v6, v0

    if-ne v8, v11, :cond_4

    add-int/lit8 v8, v0, 0x1

    if-ne v8, v1, :cond_4

    .line 233
    add-int/lit8 v1, v1, -0x1

    .line 234
    sget-object v8, Lcom/samsung/commonimsinterface/imscommon/IMSUtility;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v9, "outLength has been cut by 1"

    invoke-static {v8, v9}, Lcom/samsung/commonimsinterface/imscommon/IMSLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
