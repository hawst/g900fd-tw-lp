.class public Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL;
.super Ljava/lang/Object;
.source "IIMSConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/commonimsinterface/imscommon/IIMSConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CALL"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL$USSDDCS;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL$USSDStatus;,
        Lcom/samsung/commonimsinterface/imscommon/IIMSConstants$CALL$USSDType;
    }
.end annotation


# static fields
.field public static final CALL_TYPE_AUDIO:I = 0x2

.field public static final CALL_TYPE_AUDIO_CONFERENCE:I = 0x15

.field public static final CALL_TYPE_E911:I = 0x14

.field public static final CALL_TYPE_E911_VIDEO:I = 0x16

.field public static final CALL_TYPE_E911_VIDEO_HD:I = 0x17

.field public static final CALL_TYPE_E911_VIDEO_HD_LAND:I = 0x18

.field public static final CALL_TYPE_E911_VIDEO_HD_QVGA_LAND:I = 0x1a

.field public static final CALL_TYPE_E911_VIDEO_LAND:I = 0x19

.field public static final CALL_TYPE_HDVIDEO:I = 0x6

.field public static final CALL_TYPE_HDVIDEO_LAND:I = 0xf

.field public static final CALL_TYPE_QCIFVIDEO:I = 0xa

.field public static final CALL_TYPE_QCIFVIDEO_CONFERENCE:I = 0x8

.field public static final CALL_TYPE_QVGAVIDEO:I = 0x9

.field public static final CALL_TYPE_TTY_FULL:I = 0xb

.field public static final CALL_TYPE_TTY_HCO:I = 0xc

.field public static final CALL_TYPE_TTY_VCO:I = 0xd

.field public static final CALL_TYPE_USSD:I = 0xe

.field public static final CALL_TYPE_VIDEO_CONFERENCE:I = 0x7

.field public static final CALL_TYPE_VIDEO_SHARE_RX:I = 0x4

.field public static final CALL_TYPE_VIDEO_SHARE_TX:I = 0x3

.field public static final INBOUND_ONLY_VIDEO_CALL:I = 0x5

.field public static final INVALID_DATA:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
