.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSNetworkStateListener;
.super Ljava/lang/Object;
.source "IIMSNetworkStateListener.java"


# virtual methods
.method public abstract onCellLocationChanged()V
.end method

.method public abstract onDataConnectionStateChanged(II)V
.end method

.method public abstract onInetDataChangeStarted(Z)V
.end method

.method public abstract onNetworkConnected(ILandroid/net/NetworkInfo;)V
.end method

.method public abstract onNetworkConnecting(ILandroid/net/NetworkInfo;)V
.end method

.method public abstract onNetworkDisconnected(ILandroid/net/NetworkInfo;)V
.end method

.method public abstract onNetworkDisconnecting(ILandroid/net/NetworkInfo;)V
.end method
