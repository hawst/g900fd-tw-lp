.class public Lcom/samsung/commonimsinterface/imscommon/internal/IMSCommonData;
.super Ljava/lang/Object;
.source "IMSCommonData.java"


# static fields
.field public static final CONNECTIVITY_TYPE_MAX:I = 0x3

.field public static final CONNECTIVITY_TYPE_MOBILE:I = 0x0

.field public static final CONNECTIVITY_TYPE_MOBILE_IMS:I = 0x1

.field public static final CONNECTIVITY_TYPE_WIFI:I = 0x2

.field public static final IMS_END_REASON_CALL_BUSY:I = 0xb

.field public static final IMS_END_REASON_LOW_BATTERY:I = 0x6

.field public static final IMS_END_REASON_NORMAL_DISCONNECT:I = 0x5

.field public static final IMS_END_REASON_NWAY_CONFERENCE:I = 0x7

.field public static final IMS_END_REASON_NW_HANDOVER:I = 0x4

.field public static final IMS_END_REASON_OUT_OF_BATTERY:I = 0xa

.field public static final IMS_END_REASON_PS_BARRING:I = 0x10

.field public static final IMS_END_REASON_REJECT_ONLY_WLAN_CONNECTED:I = 0xc

.field public static final IMS_END_REASON_REJECT_USER_BUSY:I = 0x2

.field public static final IMS_END_REASON_REJECT_USR_BUSY_CS_CALL:I = 0x7

.field public static final IMS_END_REASON_REJECT_USR_DECLINE:I = 0x3

.field public static final IMS_END_REASON_SRVCC_HANDOVER:I = 0x8

.field public static final IMS_END_REASON_TEMP_NOT_ACCEPTABLE:I = 0x9

.field public static final IMS_END_REASON_VOPS_DISABLED:I = 0x9

.field public static final IMS_SYSTEM_SERVICE_NAME:Ljava/lang/String; = "ims"

.field public static final NATIVE_CALL_PACKAGE_NAME:Ljava/lang/String; = "com.sec.ims"

.field public static final NETWORK_TYPE_MAX:I = 0x2

.field public static final NETWORK_TYPE_MOBILE:I = 0x0

.field public static final NETWORK_TYPE_WIFI:I = 0x1

.field public static final NUMBER_OF_REQUIRED_DEDICATED_BEARERS:I = 0x2

.field public static final TTY_FULL:I = 0x3

.field public static final TTY_HEAR:I = 0x2

.field public static final TTY_OFF:I = 0x0

.field public static final TTY_TALK:I = 0x1

.field public static final TTY_UNDEFINED:I = 0xff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
