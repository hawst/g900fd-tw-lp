.class public interface abstract Lcom/samsung/commonimsinterface/imscommon/IIMSDebugInfoListener;
.super Ljava/lang/Object;
.source "IIMSDebugInfoListener.java"


# virtual methods
.method public abstract onAPCSEvent(Ljava/lang/String;ZZZLjava/lang/String;I)V
.end method

.method public abstract onAPCSXml(Ljava/lang/String;ZZZLjava/lang/String;)V
.end method

.method public abstract onFailedEvent(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onIconEvent(Ljava/lang/String;ZZZI)V
.end method

.method public abstract onNetworkInfo(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onNetworkStatus(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPDNStatus(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onSIPNotify(Ljava/lang/String;ZZZLjava/lang/String;II)V
.end method

.method public abstract onServiceStart(Ljava/lang/String;ZZZ)V
.end method
