.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;
.super Ljava/lang/Object;
.source "SVoiceTutorialBase.java"

# interfaces
.implements Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;


# direct methods
.method constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChanged(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 296
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;

    iget-object v0, v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 298
    .local v0, "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    if-eqz v0, :cond_0

    .line 299
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isTutorialCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;

    iget-object v1, v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/vlingo/midas/gui/ConversationActivity;->finishTutorial(Landroid/content/Context;)V

    .line 301
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 302
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 308
    .end local v0    # "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_0
    :goto_0
    return-void

    .line 304
    .restart local v0    # "activity":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_1
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->finish()V

    goto :goto_0
.end method
