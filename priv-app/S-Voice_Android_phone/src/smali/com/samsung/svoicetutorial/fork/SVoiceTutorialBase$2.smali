.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$2;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "SVoiceTutorialBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;


# direct methods
.method constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$2;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 4
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x1f4

    .line 239
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 240
    const-string/jumbo v0, "tutorial_tts_to_activate"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const-string/jumbo v0, "tutorial_tts_proccessing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 245
    :cond_2
    const-string/jumbo v0, "tutorial_tts_lets_start_seamless"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 248
    const-string/jumbo v0, "all_notifications_accepted"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 236
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 3
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 220
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 221
    const-string/jumbo v1, "tutorial_tts_to_activate"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 225
    .local v0, "audioManager":Landroid/media/AudioManager;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 227
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "setParameters, voice_wakeup_mic=off"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 231
    :cond_0
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 234
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_1
    return-void
.end method
