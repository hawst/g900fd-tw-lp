.class public Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
.super Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
.source "SVoiceTutorialWidget.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$2;,
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;
    }
.end annotation


# static fields
.field private static final MSG_DISABLE_MIC_FOR_TOTORIAL:I = 0x22b

.field private static final MSG_ENABLE_MIC_AFTER_TUTORIAL:I = 0xe

.field private static singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;


# instance fields
.field private mDefaultSharedPrefs:Landroid/content/SharedPreferences;

.field public tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 372
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    .line 48
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setMicChangeInterface(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->startMicListeningAnim()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addUsrBubble()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addSystmBubble()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addDummyWidget()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->moveToThinkingState()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->showTutorialEndTextLayout()V

    return-void
.end method

.method private addDummyWidget()V
    .locals 9

    .prologue
    const-wide/16 v7, 0x258

    const/4 v6, 0x2

    .line 314
    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSystemBblLL:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    .line 315
    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 316
    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    const-string/jumbo v4, "alpha"

    new-array v5, v6, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 318
    .local v1, "fadeIn":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 319
    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSystemBblLL:Landroid/widget/LinearLayout;

    const-string/jumbo v4, "translationY"

    new-array v5, v6, [F

    fill-array-data v5, :array_1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 321
    .local v2, "move":Landroid/animation/ObjectAnimator;
    invoke-virtual {v2, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 322
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 323
    .local v0, "animatorset":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 324
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 330
    .end local v0    # "animatorset":Landroid/animation/AnimatorSet;
    .end local v1    # "fadeIn":Landroid/animation/ObjectAnimator;
    .end local v2    # "move":Landroid/animation/ObjectAnimator;
    :cond_0
    return-void

    .line 316
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 319
    :array_1
    .array-data 4
        0x44480000    # 800.0f
        0x0
    .end array-data
.end method

.method private addSystmBubble()V
    .locals 4

    .prologue
    .line 297
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 306
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 307
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 311
    return-void
.end method

.method private addUsrBubble()V
    .locals 3

    .prologue
    .line 281
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSystemBblLL:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mUserBubble:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 282
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mMainTextLL:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 288
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mContext:Landroid/content/Context;

    sget v2, Lcom/vlingo/midas/R$anim;->system_bubble_anim:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 289
    .local v0, "anim":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mUserBubble:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 290
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mUserBubble:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 292
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    invoke-direct {v0, p0, p1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .line 43
    :cond_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    return-object v0
.end method

.method private handlerInit()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    .line 178
    return-void
.end method

.method private moveToThinkingState()V
    .locals 2

    .prologue
    .line 249
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 251
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->setMicThinking()V

    .line 252
    return-void
.end method

.method private setMicThinking()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 255
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 256
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 259
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 260
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 261
    sget v0, Lcom/vlingo/midas/R$id;->speak:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 262
    sget v0, Lcom/vlingo/midas/R$id;->speak:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 264
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 265
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->startThinkingAnim()V

    .line 270
    return-void
.end method

.method private showTutorialEndTextLayout()V
    .locals 9

    .prologue
    const/16 v8, 0x1b

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 181
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mContext:Landroid/content/Context;

    sget v5, Lcom/vlingo/midas/R$anim;->fade_in:I

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 192
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mMainTextLL:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_4

    if-eqz v0, :cond_4

    .line 193
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 195
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 196
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 197
    iput v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 198
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mMainTextLL:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 200
    sget v4, Lcom/vlingo/midas/R$id;->speak:I

    invoke-virtual {p0, v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 201
    .local v3, "sayTxt":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 202
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setAlpha(F)V

    .line 206
    :cond_0
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 207
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setAlpha(F)V

    .line 211
    :cond_1
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 212
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setAlpha(F)V

    .line 216
    :cond_2
    sget v4, Lcom/vlingo/midas/R$id;->processing_text:I

    invoke-virtual {p0, v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 217
    .local v1, "mProcessingTxt":Landroid/widget/TextView;
    if-eqz v1, :cond_3

    .line 218
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    :cond_3
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mMainTextLL:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 222
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mMainTextLL:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 224
    .end local v1    # "mProcessingTxt":Landroid/widget/TextView;
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "sayTxt":Landroid/widget/TextView;
    :cond_4
    return-void
.end method

.method private startMicListeningAnim()V
    .locals 2

    .prologue
    .line 243
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->START_LISTENING_ANIM_TUTORIAL:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 244
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSystemBblLL:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 246
    return-void
.end method

.method private startThinkingAnim()V
    .locals 4

    .prologue
    .line 273
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 274
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 276
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 279
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 334
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 335
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->releaseWakeLock()V

    .line 336
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_3

    .line 337
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x12

    if-ge v0, v2, :cond_0

    .line 338
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :cond_0
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialCompleted()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    if-nez v2, :cond_1

    .line 343
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v2, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 345
    :cond_1
    new-instance v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/ConversationActivity;-><init>()V

    .line 347
    .local v1, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    sget-boolean v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isTutorialFromSettings:Z

    if-eqz v2, :cond_2

    .line 348
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->setDoLaunchTutorial(Z)V

    .line 349
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->setTutorialCompleted(Z)V

    .line 352
    :cond_2
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->destroy()V

    .line 354
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 356
    .end local v0    # "i":I
    .end local v1    # "o":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_3
    sput-object v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHandler:Landroid/os/Handler;

    .line 357
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v2, :cond_5

    .line 358
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->getEngines()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 359
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 360
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 362
    :cond_4
    sput-object v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->tts:Landroid/speech/tts/TextToSpeech;

    .line 364
    :cond_5
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mFadeInAnim:Landroid/view/animation/Animation;

    .line 365
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mFadeOutAnim:Landroid/view/animation/Animation;

    .line 366
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    .line 368
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    .line 369
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSystemBblLL:Landroid/widget/LinearLayout;

    .line 371
    return-void
.end method

.method init()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->init()V

    .line 68
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isEdgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isAmericanFeature()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_fahrenheit_edge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_edge:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->isAmericanFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_fahrenheit:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->invalidate()V

    .line 55
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->onFinishInflate()V

    .line 60
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->handlerInit()V

    .line 61
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->init()V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->setDimensionsIfSantos()V

    .line 63
    return-void
.end method
