.class public Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;
.super Landroid/os/Handler;
.source "SVoiceTutorialWidgetK.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyHandler"
.end annotation


# instance fields
.field private final mMainActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 123
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 124
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;->mMainActivity:Ljava/lang/ref/WeakReference;

    .line 125
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 129
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;->mMainActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .line 130
    .local v0, "activity":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    if-eqz v0, :cond_0

    .line 134
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 144
    :pswitch_1
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v1, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_0

    .line 147
    :pswitch_2
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->showWakeUpCommandText()V

    goto :goto_0

    .line 150
    :pswitch_3
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->showSimultaneousText()V

    goto :goto_0

    .line 153
    :pswitch_4
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->showTapMicText()V

    goto :goto_0

    .line 156
    :pswitch_5
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCallActive()Z

    move-result v1

    if-nez v1, :cond_2

    .line 157
    sget-object v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->HI_GALAXY:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    invoke-static {v1}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->play(Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;)V

    .line 158
    :cond_2
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x8fc

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 161
    :pswitch_6
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->startMicListeningAnim()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$000(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    .line 162
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x13

    const-wide/16 v3, 0x2bc

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 166
    :pswitch_7
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_ask_question:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    goto :goto_0

    .line 169
    :pswitch_8
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->START_LISTENING_ANIM_TUTORIAL:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v1, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_0

    .line 173
    :pswitch_9
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_proccessing:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    goto :goto_0

    .line 177
    :pswitch_a
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCallActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 178
    sget-object v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->USER_COMMAND:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    invoke-static {v1}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->play(Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;)V

    goto :goto_0

    .line 181
    :pswitch_b
    iget-object v1, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tutorial_display_processing:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 186
    :pswitch_c
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playMicSounds(I)V

    goto/16 :goto_0

    .line 189
    :pswitch_d
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setMicIdle()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$100(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    goto/16 :goto_0

    .line 193
    :pswitch_e
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_show_result:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 194
    iget-object v1, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 195
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    const-wide/16 v3, 0x258

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 198
    :pswitch_f
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addSystemBubble()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$200(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    .line 199
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xd

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 203
    :pswitch_10
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addUserBubble()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$300(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    goto/16 :goto_0

    .line 206
    :pswitch_11
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addDummyWidget()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$400(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    .line 207
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x10

    const-wide/16 v3, 0x14

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 211
    :pswitch_12
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_start:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 212
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x16

    const-wide/16 v3, 0x9c4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 215
    :pswitch_13
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V

    .line 216
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->destroy()V

    goto/16 :goto_0

    .line 219
    :pswitch_14
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->moveToThinkingState()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$500(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    goto/16 :goto_0

    .line 222
    :pswitch_15
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->showTutorialEndTextLayout()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->access$600(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    goto/16 :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_14
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_f
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_15
    .end packed-switch
.end method
