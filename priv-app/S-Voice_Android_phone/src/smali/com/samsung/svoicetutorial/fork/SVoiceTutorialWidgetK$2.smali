.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$2;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "SVoiceTutorialWidgetK.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;


# direct methods
.method constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0

    .prologue
    .line 441
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$2;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 5
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 458
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 459
    const-string/jumbo v0, "tutorial_tts_lets_see"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    const-string/jumbo v0, "tutorial_tts_proccessing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 462
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 463
    :cond_2
    const-string/jumbo v0, "tutorial_tts_lets_start"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 464
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 465
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$2;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-virtual {v0, v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setTutorialCompleted(Z)V

    .line 466
    const-string/jumbo v0, "all_notifications_accepted"

    invoke-static {v0, v4}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 467
    :cond_3
    const-string/jumbo v0, "tutorial_tts_ask_question"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 469
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x14

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onError(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 455
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 444
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 445
    const-string/jumbo v0, "tutorial_tts_active"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 447
    :cond_1
    const-string/jumbo v0, "tutorial_tts_or"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 448
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 449
    :cond_2
    const-string/jumbo v0, "tutorial_tts_tap_mic"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
