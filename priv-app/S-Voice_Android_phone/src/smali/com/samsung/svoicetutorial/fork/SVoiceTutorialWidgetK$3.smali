.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$3;
.super Ljava/lang/Object;
.source "SVoiceTutorialWidgetK.java"

# interfaces
.implements Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;


# direct methods
.method constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$3;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V
    .locals 5
    .param p1, "controlState"    # Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .prologue
    .line 499
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$3;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    iget-object v0, v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 500
    .local v0, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$4;->$SwitchMap$com$vlingo$midas$gui$ControlFragment$ControlState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 503
    :pswitch_0
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 504
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_0

    .line 509
    :pswitch_1
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_1

    .line 510
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 512
    :cond_1
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22c

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 513
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x14

    const-wide/16 v3, 0x23f0

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 514
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 515
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    goto :goto_0

    .line 519
    :pswitch_2
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 520
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    goto :goto_0

    .line 524
    :pswitch_3
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->tutorialMicListeningAnim()V

    goto :goto_0

    .line 527
    :pswitch_4
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_2

    .line 528
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 530
    :cond_2
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v1, :cond_0

    .line 532
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    goto :goto_0

    .line 500
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
