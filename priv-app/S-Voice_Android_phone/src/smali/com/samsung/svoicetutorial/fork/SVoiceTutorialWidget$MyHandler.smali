.class public Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;
.super Landroid/os/Handler;
.source "SVoiceTutorialWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyHandler"
.end annotation


# instance fields
.field private final mMainActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 1
    .param p1, "activity"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;->mMainActivity:Ljava/lang/ref/WeakReference;

    .line 84
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v2, 0x8

    .line 88
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$MyHandler;->mMainActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    .line 89
    .local v0, "activity":Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    if-eqz v0, :cond_0

    .line 93
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 106
    :pswitch_1
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->showSayText()V

    .line 107
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->showWakeUpCommandText()V

    .line 108
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/16 v3, 0x834

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 111
    :pswitch_2
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->showSimultaneousText()V

    goto :goto_0

    .line 114
    :pswitch_3
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCallActive()Z

    move-result v1

    if-nez v1, :cond_2

    .line 115
    sget-object v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->HI_GALAXY:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    invoke-static {v1}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->play(Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;)V

    .line 116
    :cond_2
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 119
    :pswitch_4
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->startMicListeningAnim()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$000(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    goto :goto_0

    .line 124
    :pswitch_5
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_proccessing:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->playTTS(I)V

    goto :goto_0

    .line 129
    :pswitch_6
    iget-object v1, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v1, v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    sget v1, Lcom/vlingo/midas/R$id;->speak:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 134
    sget v1, Lcom/vlingo/midas/R$id;->processing_text:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 137
    :pswitch_7
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v1

    sget-object v2, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_stop_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v1, v2}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->playMicSounds(I)V

    goto :goto_0

    .line 140
    :pswitch_8
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_show_result:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->playTTS(I)V

    .line 142
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xd

    const-wide/16 v3, 0x258

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 145
    :pswitch_9
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addUsrBubble()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$100(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    .line 146
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    const-wide/16 v3, 0xce4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 149
    :pswitch_a
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addSystmBubble()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$200(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    goto/16 :goto_0

    .line 152
    :pswitch_b
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->addDummyWidget()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$300(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    .line 153
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x10

    const-wide/16 v3, 0x190

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 156
    :pswitch_c
    sget v1, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_start_seamless:I

    invoke-virtual {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->playTTS(I)V

    .line 157
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x16

    const-wide/16 v3, 0x9c4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 160
    :pswitch_d
    invoke-virtual {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->destroy()V

    goto/16 :goto_0

    .line 163
    :pswitch_e
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->moveToThinkingState()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$400(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    goto/16 :goto_0

    .line 166
    :pswitch_f
    # invokes: Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->showTutorialEndTextLayout()V
    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->access$500(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V

    goto/16 :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_e
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
    .end packed-switch
.end method
