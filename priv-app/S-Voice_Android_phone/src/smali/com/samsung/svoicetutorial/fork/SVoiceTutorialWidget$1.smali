.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;
.super Ljava/lang/Object;
.source "SVoiceTutorialWidget.java"

# interfaces
.implements Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;


# direct methods
.method constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V
    .locals 6
    .param p1, "controlState"    # Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 377
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    iget-object v0, v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    .line 378
    .local v0, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$2;->$SwitchMap$com$vlingo$midas$gui$ControlFragment$ControlState:[I

    invoke-virtual {p1}, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 432
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 384
    :pswitch_1
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    const/16 v2, 0x190

    invoke-virtual {v1, v5, v2}, Lcom/vlingo/midas/gui/ControlFragment;->fadeInMicLayout(ZI)V

    goto :goto_0

    .line 387
    :pswitch_2
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_1

    .line 388
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 390
    :cond_1
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0x22b

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 391
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mhandler:Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;

    const/16 v2, 0xe

    const-wide/16 v3, 0x2008

    invoke-virtual {v1, v2, v3, v4}, Lcom/vlingo/midas/gui/ConversationActivity$ConversationActivityHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 392
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_0

    .line 393
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    goto :goto_0

    .line 400
    :pswitch_3
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_2

    .line 401
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->fullContainer:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget$1;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;

    invoke-virtual {v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->transparent:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 403
    :cond_2
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 404
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 405
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->micBgMiniModeLayout:Landroid/view/View;

    const-string/jumbo v2, "#008892"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 407
    :cond_3
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_4

    .line 408
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/ControlFragment;->setFakeSvoiceMiniForTutorial(Z)V

    .line 410
    :cond_4
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_5

    .line 411
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    invoke-virtual {v1, v4}, Lcom/vlingo/midas/gui/ControlFragment;->removeMicStatusTextForTutorial(Z)V

    .line 413
    :cond_5
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_6

    .line 414
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 416
    :cond_6
    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->tutorialMicListeningAnim()V

    goto :goto_0

    .line 419
    :pswitch_4
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    if-eqz v1, :cond_7

    .line 420
    sget-object v1, Lcom/vlingo/midas/gui/ConversationActivity;->controlFragment:Lcom/vlingo/midas/gui/ControlFragment;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/ControlFragment;->setState(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 422
    :cond_7
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 423
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    if-eqz v1, :cond_0

    .line 424
    iget-object v1, v0, Lcom/vlingo/midas/gui/ConversationActivity;->mListeningAnimationView:Lcom/vlingo/midas/gui/customviews/ListeningView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vlingo/midas/gui/customviews/ListeningView;->setVisibility(I)V

    goto/16 :goto_0

    .line 378
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
