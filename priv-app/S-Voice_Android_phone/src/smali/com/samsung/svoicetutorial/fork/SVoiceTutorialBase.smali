.class public Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
.super Lcom/vlingo/midas/gui/customviews/RelativeLayout;
.source "SVoiceTutorialBase.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;
    }
.end annotation


# static fields
.field public static AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType; = null

.field static final DESTROY:I = 0x11

.field static final PLAY_TTS_ASK_QUESTION:I = 0x13

.field static final PLAY_TTS_FOR_PROCESSING:I = 0x9

.field static final PLAY_TTS_LETS_START:I = 0x10

.field static final PLAY_TTS_SHOW_RESULT:I = 0xc

.field static final PLAY_WAVE_FILE_FOR_HI_GALAXY:I = 0x4

.field static final PLAY_WAVE_FILE_FOR_THINKING_OVER:I = 0xa

.field static final PLAY_WAVE_FILE_FOR_USER_COMMAND:I = 0x7

.field public static final PREFERENCE_NAME_TUTORIAL_COMPLETE:Ljava/lang/String; = "is_tutorial_completed"

.field static final SEND_MSG_TO_START_LISTENING_ANIMATION:I = 0x14

.field static final SET_MIC_IDLE:I = 0xb

.field static final SH0W_TUTORIAL_TEXT_END:I = 0x16

.field static final SHOW_DUMMY_WIDGET:I = 0xf

.field static final SHOW_HI_GALAXY_TEXT:I = 0x1

.field static final SHOW_PROCESSING_TEXT:I = 0x6

.field static final SHOW_SIMULTANEOUS_TEXT:I = 0x2

.field static final SHOW_SYSTEM_BUBBLE:I = 0xe

.field static final SHOW_TAP_MIC_TEXT:I = 0x3

.field static final SHOW_USER_BUBBLE:I = 0xd

.field static final START:I = 0x0

.field static final START_MIC_LISTENING_ANIM:I = 0x5

.field public static final START_THINKING_STATE_TUTORIAL:I = 0x8

.field public static isTutorialFromSettings:Z

.field private static isVolumeMuted:Z

.field public static mHandler:Landroid/os/Handler;

.field protected static micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

.field static tts:Landroid/speech/tts/TextToSpeech;

.field private static ttsVolumeStream:I


# instance fields
.field private audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

.field bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

.field locale:Ljava/util/Locale;

.field final mContext:Landroid/content/Context;

.field mDefaultSharedPrefs:Landroid/content/SharedPreferences;

.field mDummyWidget:Landroid/widget/ImageView;

.field mFadeInAnim:Landroid/view/animation/Animation;

.field mFadeOutAnim:Landroid/view/animation/Animation;

.field mHiGalaxyTxt:Landroid/widget/TextView;

.field mMainTextLL:Landroid/widget/LinearLayout;

.field mShadow:Landroid/view/View;

.field mSimultaneousTxt:Landroid/widget/TextView;

.field mSpeakText:Landroid/widget/TextView;

.field mSystemBblLL:Landroid/widget/LinearLayout;

.field mSystemBubble:Landroid/widget/TextView;

.field mUserBubble:Landroid/widget/TextView;

.field mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field screenDensityDPI:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    sput-boolean v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isTutorialFromSettings:Z

    .line 46
    sput-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHandler:Landroid/os/Handler;

    .line 48
    sput-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    .line 85
    sput v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->ttsVolumeStream:I

    .line 86
    sput-boolean v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isVolumeMuted:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    .line 72
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    .line 73
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSpeakText:Landroid/widget/TextView;

    .line 74
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    .line 75
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    .line 76
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    .line 77
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    .line 78
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    .line 79
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mMainTextLL:Landroid/widget/LinearLayout;

    .line 80
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBblLL:Landroid/widget/LinearLayout;

    .line 81
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeOutAnim:Landroid/view/animation/Animation;

    .line 82
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 84
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    .line 89
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 217
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$2;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$2;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    .line 385
    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->locale:Ljava/util/Locale;

    .line 97
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    .line 98
    return-void
.end method

.method private getAudioType()Lcom/vlingo/core/internal/audio/AudioType;
    .locals 3

    .prologue
    .line 205
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    if-nez v1, :cond_0

    .line 206
    const-string/jumbo v1, "custom_tone_encoding"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "encoding":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioType;->valueOf(Ljava/lang/String;)Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v1

    sput-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    .end local v0    # "encoding":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->AUDIO_TYPE:Lcom/vlingo/core/internal/audio/AudioType;

    return-object v1

    .line 209
    .restart local v0    # "encoding":Ljava/lang/String;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isTutorialCompleted()Z
    .locals 2

    .prologue
    .line 101
    const-string/jumbo v0, "is_tutorial_completed"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setMicChangeInterface(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;)V
    .locals 0
    .param p0, "micChangeModeNew"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    .prologue
    .line 92
    sput-object p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    .line 93
    return-void
.end method


# virtual methods
.method acquireWakeLock()V
    .locals 4

    .prologue
    .line 361
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 365
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    const-string/jumbo v2, "SvoiceTutorial_ScrnKpr"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 372
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 373
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x61a8

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 375
    :cond_1
    return-void
.end method

.method delegateAccessibilityForViews()V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 317
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSpeakText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 318
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 320
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBblLL:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 322
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mMainTextLL:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 323
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 324
    return-void
.end method

.method delegateViewForTalkBack(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 428
    if-eqz p1, :cond_0

    .line 429
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 431
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->removeListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 106
    return-void
.end method

.method init()V
    .locals 3

    .prologue
    .line 260
    sget v0, Lcom/vlingo/midas/R$id;->hi_galaxy_text:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    .line 261
    sget v0, Lcom/vlingo/midas/R$id;->speak:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSpeakText:Landroid/widget/TextView;

    .line 262
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    sget v1, Lcom/vlingo/midas/R$anim;->fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    .line 263
    sget v0, Lcom/vlingo/midas/R$id;->simultaneously_txt:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    .line 264
    sget v0, Lcom/vlingo/midas/R$id;->system_bubble:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    .line 265
    sget v0, Lcom/vlingo/midas/R$id;->user_bubble:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    .line 266
    sget v0, Lcom/vlingo/midas/R$id;->dummy_widget:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    .line 267
    sget v0, Lcom/vlingo/midas/R$id;->shadow:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    .line 268
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    sget v1, Lcom/vlingo/midas/R$anim;->fade_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeOutAnim:Landroid/view/animation/Animation;

    .line 269
    sget v0, Lcom/vlingo/midas/R$id;->mainTextLL:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mMainTextLL:Landroid/widget/LinearLayout;

    .line 270
    sget v0, Lcom/vlingo/midas/R$id;->result_content_layout:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBblLL:Landroid/widget/LinearLayout;

    .line 271
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->requestAudioFocus(II)V

    .line 274
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCallActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->muteTTS()V

    .line 277
    :cond_0
    const-string/jumbo v0, "language"

    const-string/jumbo v1, "en-US"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/core/internal/settings/Settings;->getISOLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->getLocaleForIsoLanguage(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->locale:Ljava/util/Locale;

    .line 279
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-virtual {v0}, Lcom/vlingo/midas/gui/ConversationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 280
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->acquireWakeLock()V

    .line 284
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vlingo/midas/settings/MidasSettings;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->delegateAccessibilityForViews()V

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    if-nez v0, :cond_2

    .line 293
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$3;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->audioFocusChangeListener:Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;

    invoke-static {v0}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->addListener(Lcom/vlingo/core/internal/audio/AudioFocusChangeListener;)V

    .line 312
    return-void
.end method

.method initTTS()V
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    .line 130
    :cond_0
    return-void
.end method

.method isAmericanFeature()Z
    .locals 3

    .prologue
    .line 418
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "en-US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 420
    .local v0, "languageCheck":Z
    invoke-static {}, Lcom/vlingo/midas/ClientConfiguration;->isAmericanPhone()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 421
    const/4 v1, 0x1

    .line 423
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method muteTTS()V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 403
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 404
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->ttsVolumeStream:I

    .line 405
    invoke-virtual {v0, v4, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 406
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isVolumeMuted:Z

    .line 407
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 255
    invoke-super {p0, p1}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 256
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setDimensionsIfSantos()V

    .line 257
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 452
    invoke-super {p0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->onDetachedFromWindow()V

    .line 453
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lcom/vlingo/midas/gui/customviews/RelativeLayout;->onFinishInflate()V

    .line 115
    const-string/jumbo v1, "SvoiceTutorialWidget"

    const-string/jumbo v2, "Tutorial inflated"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->init(Landroid/content/Context;)V

    .line 117
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$1;

    invoke-direct {v1, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$1;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 123
    .local v0, "ttsThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 124
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 389
    if-nez p1, :cond_1

    .line 390
    const-string/jumbo v0, "SvoiceTutorialWidget"

    const-string/jumbo v1, "TTS initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    .line 392
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 393
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 395
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_always_with_you:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->playTTS(I)V

    .line 396
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_to_activate:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->playTTS(I)V

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 398
    const-string/jumbo v0, "SvoiceTutorialWidget"

    const-string/jumbo v1, "TTS initialization failed!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method playMicSounds(I)V
    .locals 2
    .param p1, "startRecoTone"    # I

    .prologue
    .line 200
    new-instance v0, Lcom/vlingo/core/internal/audio/TonePlayer;

    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getAudioType()Lcom/vlingo/core/internal/audio/AudioType;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/vlingo/core/internal/audio/TonePlayer;-><init>(ILcom/vlingo/core/internal/audio/AudioType;)V

    invoke-virtual {v0}, Lcom/vlingo/core/internal/audio/TonePlayer;->play()V

    .line 201
    return-void
.end method

.method playTTS(I)V
    .locals 5
    .param p1, "resourceId"    # I

    .prologue
    .line 327
    const/4 v0, 0x0

    .line 331
    .local v0, "name":Ljava/lang/String;
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_always_with_you:I

    if-ne p1, v2, :cond_1

    .line 332
    const-string/jumbo v0, "tutorial_tts_always_with_you"

    .line 352
    :cond_0
    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 353
    .local v1, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v2, "utteranceId"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string/jumbo v2, "com.samsung.SMT.KEY_PARAM"

    const-string/jumbo v3, "DISABLE_NOTICE_POPUP"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->tts:Landroid/speech/tts/TextToSpeech;

    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4, v1}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 357
    return-void

    .line 333
    .end local v1    # "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_to_activate:I

    if-ne p1, v2, :cond_2

    .line 334
    const-string/jumbo v0, "tutorial_tts_to_activate"

    goto :goto_0

    .line 335
    :cond_2
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_proccessing:I

    if-ne p1, v2, :cond_3

    .line 336
    const-string/jumbo v0, "tutorial_tts_proccessing"

    goto :goto_0

    .line 337
    :cond_3
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_start_seamless:I

    if-ne p1, v2, :cond_4

    .line 338
    const-string/jumbo v0, "tutorial_tts_lets_start_seamless"

    goto :goto_0

    .line 339
    :cond_4
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_ask_question:I

    if-ne p1, v2, :cond_5

    .line 340
    const-string/jumbo v0, "tutorial_tts_ask_question"

    goto :goto_0

    .line 341
    :cond_5
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_active:I

    if-ne p1, v2, :cond_6

    .line 342
    const-string/jumbo v0, "tutorial_tts_active"

    goto :goto_0

    .line 343
    :cond_6
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_or:I

    if-ne p1, v2, :cond_7

    .line 344
    const-string/jumbo v0, "tutorial_tts_or"

    goto :goto_0

    .line 345
    :cond_7
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_tap_mic:I

    if-ne p1, v2, :cond_8

    .line 346
    const-string/jumbo v0, "tutorial_tts_tap_mic"

    goto :goto_0

    .line 347
    :cond_8
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_see:I

    if-ne p1, v2, :cond_9

    .line 348
    const-string/jumbo v0, "tutorial_tts_lets_see"

    goto :goto_0

    .line 349
    :cond_9
    sget v2, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_start:I

    if-ne p1, v2, :cond_0

    .line 350
    const-string/jumbo v0, "tutorial_tts_lets_start"

    goto :goto_0
.end method

.method releaseWakeLock()V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    const-string/jumbo v0, "SvoiceTutorialWidget"

    const-string/jumbo v1, "releaseWakeLock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 383
    :cond_0
    return-void
.end method

.method setCustomAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;
    .locals 1

    .prologue
    .line 472
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$4;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$4;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;)V

    return-object v0
.end method

.method setDimensionsIfSantos()V
    .locals 8

    .prologue
    const/high16 v7, 0x41880000    # 17.0f

    const/16 v6, 0x13

    const/4 v5, 0x2

    const/high16 v4, 0x41d80000    # 27.0f

    const/high16 v3, 0x43200000    # 160.0f

    .line 134
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->getDeviceType()Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    move-result-object v0

    sget-object v1, Lcom/vlingo/midas/settings/MidasSettings$DeviceType;->TABLET_HIGH_RESOLUTION:Lcom/vlingo/midas/settings/MidasSettings$DeviceType;

    if-ne v0, v1, :cond_3

    .line 136
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 138
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 139
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 140
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_4

    .line 141
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 145
    :goto_0
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mUserBubble:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 150
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 151
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 152
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_5

    .line 153
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x41a00000    # 20.0f

    iget v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 157
    :goto_1
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSystemBubble:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 162
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_7

    .line 163
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 164
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 165
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 166
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isAmericanFeature()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 167
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_fahrenheit:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 179
    :goto_2
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 185
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_9

    .line 186
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 187
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 188
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 194
    :goto_3
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mShadow:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    :cond_3
    return-void

    .line 143
    :cond_4
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v7

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto/16 :goto_0

    .line 155
    :cond_5
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v7

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_1

    .line 169
    :cond_6
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 171
    :cond_7
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 172
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 173
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 174
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isAmericanFeature()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 175
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_fahrenheit_port:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 177
    :cond_8
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_port:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 190
    :cond_9
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 191
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 192
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->screenDensityDPI:F

    div-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_3
.end method

.method public setTutorialCompleted(Z)V
    .locals 4
    .param p1, "isCompleted"    # Z

    .prologue
    .line 456
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    .line 457
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mDefaultSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 458
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "is_tutorial_completed"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 459
    const-string/jumbo v1, "iux_complete"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 460
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 461
    const-string/jumbo v1, "SvoiceTutorialWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Tutorial completed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    return-void
.end method

.method showSayText()V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSpeakText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSpeakText:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 468
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 470
    :cond_0
    return-void
.end method

.method protected showSimultaneousText()V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 436
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mSimultaneousTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 438
    :cond_0
    return-void
.end method

.method protected showWakeUpCommandText()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 444
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mHiGalaxyTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 447
    :cond_0
    return-void
.end method

.method public unMuteTTS()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 410
    sget-boolean v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isVolumeMuted:Z

    if-eqz v1, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 412
    .local v0, "am":Landroid/media/AudioManager;
    const/16 v1, 0x9

    sget v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->ttsVolumeStream:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 413
    sput-boolean v3, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->isVolumeMuted:Z

    .line 415
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    return-void
.end method
