.class public Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;
.super Ljava/lang/Object;
.source "RegisterTutorialSoundPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;
    }
.end annotation


# static fields
.field private static mTutorialSoundPool:Landroid/media/SoundPool;

.field private static mTutorialSoundPoolHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static declared-synchronized destroy()V
    .locals 2

    .prologue
    .line 39
    const-class v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_0

    .line 40
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 41
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    .line 43
    :cond_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 44
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_1
    monitor-exit v1

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 19
    const-class v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Landroid/media/SoundPool;

    const/4 v2, 0x3

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Landroid/media/SoundPool;-><init>(III)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    .line 22
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->HI_GALAXY:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    sget-object v3, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    sget v4, Lcom/vlingo/midas/R$raw;->hi_galaxy:I

    const/4 v5, 0x1

    invoke-virtual {v3, p0, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    :goto_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->USER_COMMAND:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    sget-object v3, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    sget v4, Lcom/vlingo/midas/R$raw;->user_command:I

    const/4 v5, 0x1

    invoke-virtual {v3, p0, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_0
    monitor-exit v1

    return-void

    .line 25
    :cond_1
    :try_start_1
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;->HI_GALAXY:Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    sget-object v3, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    sget v4, Lcom/vlingo/midas/R$raw;->hi_galaxy_k:I

    const/4 v5, 0x1

    invoke-virtual {v3, p0, v4, v5}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static play(Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;)V
    .locals 7
    .param p0, "type"    # Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool$TutorialSoundType;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 32
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    if-nez v0, :cond_0

    .line 36
    :goto_0
    return-void

    .line 35
    :cond_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPool:Landroid/media/SoundPool;

    sget-object v1, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->mTutorialSoundPoolHashMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_0
.end method
