.class public Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
.super Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;
.source "SVoiceTutorialWidgetK.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$4;,
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;,
        Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;
    }
.end annotation


# static fields
.field private static final MSG_HIDE_MIC_FOR_TUTORIAL:I = 0x22c

.field private static final MSG_SHOW_MIC_LAYOUT:I = 0x14

.field public static isTutorialFromSettings:Z

.field private static singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;


# instance fields
.field private mDummyWidgetLL:Landroid/widget/LinearLayout;

.field private mTapMicTxt:Landroid/widget/TextView;

.field private mTutorialEndText:Landroid/widget/TextView;

.field private mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

.field private mWidgetNBblLL:Landroid/widget/LinearLayout;

.field public tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .line 475
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isTutorialFromSettings:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    .line 52
    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    .line 53
    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    .line 54
    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    .line 441
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$2;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$2;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    .line 494
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$3;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$3;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    .line 67
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tutorialListener:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    invoke-static {v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->setMicChangeInterface(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->startMicListeningAnim()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setMicIdle()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addSystemBubble()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addUserBubble()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->addDummyWidget()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->moveToThinkingState()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->showTutorialEndTextLayout()V

    return-void
.end method

.method private addDummyWidget()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 360
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    sget v3, Lcom/vlingo/midas/R$anim;->user_bubble_anim:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 361
    .local v1, "transNfadeY":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidget:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 362
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 363
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 364
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mShadow:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 365
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mShadow:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setAlpha(F)V

    .line 366
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    sget v2, Lcom/vlingo/midas/R$id;->dummy_widget_ll:I

    invoke-virtual {p0, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    .line 368
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 369
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 370
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 371
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidgetLL:Landroid/widget/LinearLayout;

    sget v3, Lcom/vlingo/midas/R$drawable;->voice_talk_weather_bg:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 377
    :cond_0
    new-instance v0, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v0}, Lcom/vlingo/midas/gui/ConversationActivity;-><init>()V

    .line 378
    .local v0, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/vlingo/midas/gui/ConversationActivity;->setDoLaunchTutorial(Z)V

    .line 381
    return-void
.end method

.method private addSystemBubble()V
    .locals 3

    .prologue
    .line 329
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBubble:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 332
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0x1e

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 333
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBubble:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->bubbleParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 337
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mMainTextLL:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 338
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 340
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    sget v2, Lcom/vlingo/midas/R$anim;->system_bubble_anim:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 341
    .local v0, "anim":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 343
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_1
    return-void
.end method

.method private addUserBubble()V
    .locals 4

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mUserBubble:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 350
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mUserBubble:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    :cond_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 355
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xf

    const-wide/16 v2, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 357
    :cond_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-direct {v0, p0, p1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    .line 62
    :cond_0
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->singleInstance:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    return-object v0
.end method

.method private handlerInit()V
    .locals 4

    .prologue
    .line 232
    new-instance v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;

    invoke-direct {v0, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$MyHandler;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    sput-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    .line 233
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 234
    return-void
.end method

.method private moveToThinkingState()V
    .locals 2

    .prologue
    .line 284
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_THINKING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 285
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 286
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setMicThinking()V

    .line 287
    return-void
.end method

.method private setMicIdle()V
    .locals 5

    .prologue
    .line 289
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 290
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    sget v2, Lcom/vlingo/midas/R$anim;->fade_out:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 293
    .local v0, "fadeOut":Landroid/view/animation/Animation;
    new-instance v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$1;

    invoke-direct {v1, p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$1;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 303
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 304
    sget-object v1, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v2, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v1, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 305
    return-void
.end method

.method private setMicThinking()V
    .locals 4

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 309
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 310
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 311
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 314
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->startThinkingAnim()V

    .line 315
    return-void
.end method

.method private showTutorialEndTextLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 237
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    sget v3, Lcom/vlingo/midas/R$anim;->fade_in:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 240
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tutorial_say_higalaxy:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "htmltext":Ljava/lang/String;
    const-string/jumbo v2, "\"\'"

    const-string/jumbo v3, "<img src=\'voice_help_font_qmarksleft.png\'/> "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "\ufffd\ufffd\'"

    const-string/jumbo v4, "<img src=\'voice_help_font_qmarksleft_jp.png\'/>"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "\'\""

    const-string/jumbo v4, " <img src=\'voice_help_font_qmarksright.png\'/>"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "\'\ufffd\ufffd"

    const-string/jumbo v4, "<img src=\'voice_help_font_qmarksright_jp.png\'/>"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "\n"

    const-string/jumbo v4, "<br/>"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 248
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 249
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 250
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    new-instance v3, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;

    invoke-direct {v3, p0, v5}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$1;)V

    invoke-static {v1, v3, v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 254
    :cond_0
    return-void
.end method

.method private startMicListeningAnim()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 263
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 265
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 266
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 267
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 268
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 269
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 270
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$string;->tutorial_display_speak:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 271
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 278
    invoke-static {}, Lcom/vlingo/core/internal/VlingoAndroidCore;->getResourceProvider()Lcom/vlingo/core/internal/ResourceIdProvider;

    move-result-object v0

    sget-object v1, Lcom/vlingo/core/internal/ResourceIdProvider$raw;->core_start_tone:Lcom/vlingo/core/internal/ResourceIdProvider$raw;

    invoke-interface {v0, v1}, Lcom/vlingo/core/internal/ResourceIdProvider;->getResourceId(Lcom/vlingo/core/internal/ResourceIdProvider$raw;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playMicSounds(I)V

    .line 279
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v1, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->ASR_LISTENING:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v0, v1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 281
    :cond_1
    return-void
.end method

.method private startThinkingAnim()V
    .locals 4

    .prologue
    .line 318
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 319
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 321
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 324
    :cond_0
    return-void
.end method


# virtual methods
.method delegateAccessibilityForViews()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 87
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mUserBubble:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBubble:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mMainTextLL:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mShadow:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->delegateViewForTalkBack(Landroid/view/View;)V

    .line 96
    return-void
.end method

.method public destroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 384
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->destroy()V

    .line 386
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->releaseWakeLock()V

    .line 388
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_3

    .line 389
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x12

    if-ge v0, v2, :cond_0

    .line 390
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 389
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 393
    :cond_0
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isTutorialCompleted()Z

    move-result v2

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isTutorialFromSettings:Z

    if-nez v2, :cond_1

    .line 394
    invoke-static {v5}, Lcom/vlingo/midas/settings/MidasSettings;->setAllNotificationsAccepted(Z)V

    .line 395
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->micChangeMode:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;

    sget-object v3, Lcom/vlingo/midas/gui/ControlFragment$ControlState;->TUTORIAL_IDLE:Lcom/vlingo/midas/gui/ControlFragment$ControlState;

    invoke-interface {v2, v3}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase$MicChangeMode;->setStateMic(Lcom/vlingo/midas/gui/ControlFragment$ControlState;)V

    .line 397
    :cond_1
    new-instance v1, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/ConversationActivity;-><init>()V

    .line 398
    .local v1, "o":Lcom/vlingo/midas/gui/ConversationActivity;
    sget-boolean v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isTutorialFromSettings:Z

    if-eqz v2, :cond_2

    .line 399
    invoke-virtual {v1, v5}, Lcom/vlingo/midas/gui/ConversationActivity;->setDoLaunchTutorial(Z)V

    .line 400
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setTutorialCompleted(Z)V

    .line 401
    sput-boolean v5, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isTutorialFromSettings:Z

    .line 403
    :cond_2
    invoke-static {}, Lcom/samsung/svoicetutorial/fork/RegisterTutorialSoundPool;->destroy()V

    .line 405
    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->getInstance(Landroid/content/Context;)Lcom/vlingo/core/internal/audio/AudioFocusManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vlingo/core/internal/audio/AudioFocusManager;->abandonAudioFocus()V

    .line 407
    .end local v0    # "i":I
    .end local v1    # "o":Lcom/vlingo/midas/gui/ConversationActivity;
    :cond_3
    sput-object v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHandler:Landroid/os/Handler;

    .line 408
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    if-eqz v2, :cond_5

    .line 409
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->getEngines()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 410
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 411
    sget-object v2, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 413
    :cond_4
    sput-object v4, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    .line 415
    :cond_5
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeInAnim:Landroid/view/animation/Animation;

    .line 416
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeOutAnim:Landroid/view/animation/Animation;

    .line 417
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    .line 418
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    .line 419
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    .line 420
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSystemBblLL:Landroid/widget/LinearLayout;

    .line 421
    iput-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    .line 422
    return-void
.end method

.method init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 99
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->init()V

    .line 100
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$string;->tutorial_display_higalaxy:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    sget v0, Lcom/vlingo/midas/R$id;->or:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mSimultaneousTxt:Landroid/widget/TextView;

    .line 102
    sget v0, Lcom/vlingo/midas/R$id;->tap_mic:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    .line 103
    sget v0, Lcom/vlingo/midas/R$id;->widget_n_bubble_layout:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mWidgetNBblLL:Landroid/widget/LinearLayout;

    .line 104
    sget v0, Lcom/vlingo/midas/R$id;->tutorial_end_text:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTutorialEndText:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ja_JP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mHiGalaxyTxt:Landroid/widget/TextView;

    sget v1, Lcom/vlingo/midas/R$drawable;->voice_help_font_qmarksleft_jp:I

    sget v2, Lcom/vlingo/midas/R$drawable;->voice_help_font_qmarksright_jp:I

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->isAmericanFeature()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mDummyWidget:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/vlingo/midas/R$drawable;->weather_widget_sample_fahrenheit:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 117
    :cond_1
    return-void
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->invalidate()V

    .line 74
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialBase;->onFinishInflate()V

    .line 79
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->handlerInit()V

    .line 80
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->init()V

    .line 81
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->setDimensionsIfSantos()V

    .line 82
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 427
    if-nez p1, :cond_1

    .line 428
    const-string/jumbo v0, "SvoiceTutorialWidget"

    const-string/jumbo v1, "TTS initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setSpeechRate(F)I

    .line 430
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 431
    sget-object v0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->tts:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mUtteranceProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 432
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_active:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 433
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_or:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 434
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_tap_mic:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 435
    sget v0, Lcom/vlingo/midas/R$string;->tutorial_tts_lets_see:I

    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->playTTS(I)V

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 437
    const-string/jumbo v0, "SvoiceTutorialWidget"

    const-string/jumbo v1, "TTS initialization failed!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected showTapMicText()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeInAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mFadeInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 259
    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mTapMicTxt:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 261
    :cond_0
    return-void
.end method
