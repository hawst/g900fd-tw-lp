.class Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;
.super Ljava/lang/Object;
.source "SVoiceTutorialWidgetK.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageGetter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;


# direct methods
.method private constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;
    .param p2, "x1"    # Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$1;

    .prologue
    .line 479
    invoke-direct {p0, p1}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;-><init>(Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;)V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 483
    const-string/jumbo v4, ".png"

    const-string/jumbo v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 484
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-virtual {v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string/jumbo v5, "drawable"

    iget-object v6, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    iget-object v6, v6, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 485
    .local v2, "id":I
    iget-object v4, p0, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK$ImageGetter;->this$0:Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;

    invoke-virtual {v4}, Lcom/samsung/svoicetutorial/fork/SVoiceTutorialWidgetK;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 486
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 487
    .local v3, "w":I
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 488
    .local v1, "h":I
    invoke-virtual {v0, v7, v7, v3, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 489
    return-object v0
.end method
