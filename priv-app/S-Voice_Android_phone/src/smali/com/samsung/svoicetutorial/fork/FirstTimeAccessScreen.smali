.class public Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;
.super Lcom/vlingo/midas/ui/VLActivity;
.source "FirstTimeAccessScreen.java"

# interfaces
.implements Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment$AlwaysOnSettingListener;
.implements Lcom/vlingo/midas/gui/DisclaimerFragment$TOSAcceptListener;


# static fields
.field public static final ALWAYS_ON_SETTINGS:Ljava/lang/String; = "always_on_settings"

.field public static final IS_DONE:Ljava/lang/String; = "is_done"

.field public static final IS_FIRST_LAUNCH:Ljava/lang/String; = "IS_FIRST_LAUNCH"

.field public static final LANGUAGE_CHANGED:Ljava/lang/String; = "com.vlingo.LANGUAGE_CHANGED"

.field private static final log:Lcom/vlingo/core/internal/logging/Logger;


# instance fields
.field private LOG_TAG:Ljava/lang/String;

.field private mCurrentFragment:Landroid/support/v4/app/Fragment;

.field private mTheme:I

.field private mTitleBar:Landroid/app/ActionBar;

.field prefForWishesEventAlert:Landroid/content/SharedPreferences;

.field sharedpreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;

    invoke-static {v0}, Lcom/vlingo/core/internal/logging/Logger;->getLogger(Ljava/lang/Class;)Lcom/vlingo/core/internal/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->log:Lcom/vlingo/core/internal/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/vlingo/midas/ui/VLActivity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTitleBar:Landroid/app/ActionBar;

    .line 196
    const-string/jumbo v0, "FirstTimeAccessScreen"

    iput-object v0, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private showAlwaysOnSettingsScreen()V
    .locals 3

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 215
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    new-instance v1, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 216
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    sget v1, Lcom/vlingo/midas/R$id;->introduction_main:I

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 220
    :goto_0
    return-void

    .line 219
    :cond_0
    sget v1, Lcom/vlingo/midas/R$id;->introduction_content:I

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private showTOSDisclaimerScreen()V
    .locals 4

    .prologue
    .line 109
    sget v2, Lcom/vlingo/midas/R$id;->main_layout_k:I

    invoke-virtual {p0, v2}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 110
    .local v1, "v":Landroid/view/View;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->welcome_page_img_v:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 114
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 115
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    new-instance v2, Lcom/vlingo/midas/gui/DisclaimerFragment;

    invoke-direct {v2}, Lcom/vlingo/midas/gui/DisclaimerFragment;-><init>()V

    iput-object v2, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 116
    sget v2, Lcom/vlingo/midas/R$id;->introduction_content:I

    iget-object v3, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 117
    return-void

    .line 113
    .end local v0    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/vlingo/midas/R$drawable;->welcome_page_img:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private showTutorialScreen()V
    .locals 3

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 200
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    new-instance v1, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;-><init>()V

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    .line 205
    :goto_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    sget v1, Lcom/vlingo/midas/R$id;->introduction_main:I

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 210
    :goto_1
    return-void

    .line 203
    :cond_0
    new-instance v1, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-direct {v1}, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;-><init>()V

    iput-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 208
    :cond_1
    sget v1, Lcom/vlingo/midas/R$id;->introduction_content:I

    iget-object v2, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_1
.end method


# virtual methods
.method public AlwaysOnSetting()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTutorialScreen()V

    .line 232
    return-void
.end method

.method public TOSAccepted()V
    .locals 3

    .prologue
    .line 224
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->sharedpreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "is_done"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showAlwaysOnSettingsScreen()V

    .line 228
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTutorialScreen()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showAlwaysOnSettingsScreen()V

    .line 146
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/SVoiceTutorialScreenFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 138
    :cond_2
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTOSDisclaimerScreen()V

    goto :goto_0

    .line 140
    :cond_3
    const-string/jumbo v1, "all_notifications_accepted"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 142
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/WelcomePage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->startActivity(Landroid/content/Intent;)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 123
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    const-string/jumbo v9, "always_on_settings"

    const/4 v10, 0x0

    invoke-virtual {p0, v9, v10}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->sharedpreferences:Landroid/content/SharedPreferences;

    .line 46
    const/16 v9, 0x8

    invoke-virtual {p0, v9}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->requestWindowFeature(I)Z

    .line 47
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    const v10, 0x1000400

    invoke-virtual {v9, v10}, Landroid/view/Window;->addFlags(I)V

    .line 50
    sget v9, Lcom/vlingo/midas/R$style;->MidasBlackMainTheme:I

    iput v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTheme:I

    .line 51
    iget v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTheme:I

    invoke-virtual {p0, v9}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->setTheme(I)V

    .line 52
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatTabletGUI()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 53
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->requestWindowFeature(I)Z

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getActionBar()Landroid/app/ActionBar;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTitleBar:Landroid/app/ActionBar;

    .line 55
    iget-object v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTitleBar:Landroid/app/ActionBar;

    if-eqz v9, :cond_1

    .line 56
    iget-object v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTitleBar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->app_name:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v9, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mTitleBar:Landroid/app/ActionBar;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 59
    :cond_1
    invoke-super {p0, p1}, Lcom/vlingo/midas/ui/VLActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isKitkatPhoneGUI()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 61
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x400

    const/16 v11, 0x400

    invoke-virtual {v9, v10, v11}, Landroid/view/Window;->setFlags(II)V

    .line 62
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->setRequestedOrientation(I)V

    .line 64
    :cond_2
    sget v9, Lcom/vlingo/midas/R$layout;->introduction_screen_k:I

    invoke-virtual {p0, v9}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->setContentView(I)V

    .line 68
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 69
    .local v4, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "privateFlags"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 70
    .local v5, "privateFlags":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 71
    .local v6, "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string/jumbo v10, "PRIVATE_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 73
    .local v1, "disableMultiTray":Ljava/lang/reflect/Field;
    const/4 v0, 0x0

    .local v0, "currentPrivateFlags":I
    const/4 v8, 0x0

    .local v8, "valueofFlagsEnableStatusBar":I
    const/4 v7, 0x0

    .line 74
    .local v7, "valueofFlagsDisableTray":I
    invoke-virtual {v5, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 75
    invoke-virtual {v6, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 76
    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v7

    .line 78
    or-int/2addr v0, v8

    .line 79
    or-int/2addr v0, v7

    .line 81
    invoke-virtual {v5, v4, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 82
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v0    # "currentPrivateFlags":I
    .end local v1    # "disableMultiTray":Ljava/lang/reflect/Field;
    .end local v4    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "privateFlags":Ljava/lang/reflect/Field;
    .end local v6    # "statusBarOpenByNoti":Ljava/lang/reflect/Field;
    .end local v7    # "valueofFlagsDisableTray":I
    .end local v8    # "valueofFlagsEnableStatusBar":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 99
    .local v3, "i":Landroid/content/Intent;
    const-string/jumbo v9, "OnBack"

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 100
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTutorialScreen()V

    .line 103
    :goto_1
    return-void

    .line 86
    .end local v3    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 89
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 102
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "i":Landroid/content/Intent;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTOSDisclaimerScreen()V

    goto :goto_1

    .line 83
    .end local v3    # "i":Landroid/content/Intent;
    :catch_1
    move-exception v9

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 128
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onDestroy()V

    .line 129
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 186
    invoke-super {p0, p1, p2}, Lcom/vlingo/midas/ui/VLActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 165
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    invoke-static {v3}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/SVoiceTutorialFragmentT;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showAlwaysOnSettingsScreen()V

    .line 179
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->mCurrentFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/AlwaysOnSettingsFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    invoke-direct {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->showTOSDisclaimerScreen()V

    goto :goto_0

    .line 173
    :cond_2
    const-string/jumbo v1, "all_notifications_accepted"

    invoke-static {v1, v3}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 175
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vlingo/midas/gui/WelcomePage;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->startActivity(Landroid/content/Intent;)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/svoicetutorial/fork/FirstTimeAccessScreen;->finish()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 150
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onPause()V

    .line 151
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onResume()V

    .line 107
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 155
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTOSAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    invoke-static {v1}, Lcom/vlingo/midas/settings/MidasSettings;->setSamsungDisclaimerAccepted(Z)V

    .line 157
    const-string/jumbo v0, "all_notifications_accepted"

    invoke-static {v0, v1}, Lcom/vlingo/midas/settings/MidasSettings;->setBoolean(Ljava/lang/String;Z)V

    .line 159
    :cond_0
    invoke-super {p0}, Lcom/vlingo/midas/ui/VLActivity;->onStop()V

    .line 160
    return-void
.end method
