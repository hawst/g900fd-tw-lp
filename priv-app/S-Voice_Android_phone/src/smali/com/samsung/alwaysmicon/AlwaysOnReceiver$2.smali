.class Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;
.super Landroid/os/Handler;
.source "AlwaysOnReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/alwaysmicon/AlwaysOnReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v4, 0x7d0

    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 393
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    .line 394
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_wakeup_mic=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 395
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=on"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_2

    .line 397
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$200(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    sput-boolean v1, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    .line 399
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "S Voice is waiting to do the \'voice_wakeup_mic=on\', until Call is finished"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 401
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 402
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 403
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    iget-object v0, v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 406
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    iget-object v0, v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 407
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_0

    .line 408
    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$300()Z

    move-result v0

    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z
    invoke-static {v1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$400(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Z

    move-result v1

    if-eq v0, v1, :cond_4

    .line 409
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$400(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Z

    move-result v0

    # setter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$302(Z)Z

    .line 410
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V
    invoke-static {v0, v2}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$500(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;I)V

    .line 412
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    iget-object v0, v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 414
    :cond_4
    # setter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->GSAPreferenceCheckStart:Z
    invoke-static {v2}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$602(Z)Z

    .line 415
    # setter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z
    invoke-static {v2}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$302(Z)Z

    goto :goto_0
.end method
