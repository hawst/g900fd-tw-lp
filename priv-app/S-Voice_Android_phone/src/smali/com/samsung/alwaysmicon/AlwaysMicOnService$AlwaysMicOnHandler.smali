.class Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;
.super Landroid/os/Handler;
.source "AlwaysMicOnService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/alwaysmicon/AlwaysMicOnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlwaysMicOnHandler"
.end annotation


# instance fields
.field private final outer:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/alwaysmicon/AlwaysMicOnService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .prologue
    .line 251
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 252
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->outer:Ljava/lang/ref/WeakReference;

    .line 253
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 256
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->outer:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .line 258
    .local v1, "o":Lcom/samsung/alwaysmicon/AlwaysMicOnService;
    if-eqz v1, :cond_0

    .line 259
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_0

    .line 260
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v3

    const-string/jumbo v4, "sys.hmt.connected"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 262
    .local v0, "hmtConnected":Z
    # getter for: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->audioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$300(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    .line 266
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/midas/VlingoApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 268
    .local v2, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-static {v2}, Lcom/vlingo/midas/phrasespotter/PhraseSpotterUtil;->getPhraseSpotterParameters(Landroid/content/res/Resources;)Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->init(Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterParameters;Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;)V

    .line 271
    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startSpotting()V
    invoke-static {v1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$400(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    .line 275
    .end local v0    # "hmtConnected":Z
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method
