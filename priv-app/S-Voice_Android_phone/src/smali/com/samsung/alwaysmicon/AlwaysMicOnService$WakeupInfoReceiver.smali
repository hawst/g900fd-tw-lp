.class Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlwaysMicOnService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/alwaysmicon/AlwaysMicOnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WakeupInfoReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;


# direct methods
.method private constructor <init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;Lcom/samsung/alwaysmicon/AlwaysMicOnService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;
    .param p2, "x1"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService$1;

    .prologue
    .line 194
    invoke-direct {p0, p1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;-><init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 197
    if-eqz p2, :cond_0

    .line 198
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "action":Ljava/lang/String;
    const-string/jumbo v3, "android.intent.action.CONTEXT_AWARE_MUSIC_INFO"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCarModeEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    if-eqz v3, :cond_2

    .line 204
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "TYPE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, "musicType":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "URI"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "URIValue":Ljava/lang/String;
    const-string/jumbo v3, "start"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    const-string/jumbo v3, "com.vlingo.midas"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 209
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$100(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    .line 245
    .end local v0    # "URIValue":Ljava/lang/String;
    .end local v1    # "action":Ljava/lang/String;
    .end local v2    # "musicType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 211
    .restart local v0    # "URIValue":Ljava/lang/String;
    .restart local v1    # "action":Ljava/lang/String;
    .restart local v2    # "musicType":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto :goto_0

    .line 213
    .end local v0    # "URIValue":Ljava/lang/String;
    .end local v2    # "musicType":Ljava/lang/String;
    :cond_2
    const-string/jumbo v3, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 214
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 216
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCarModeEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 218
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$100(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto :goto_0

    .line 220
    :cond_3
    const-string/jumbo v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 222
    :cond_4
    sget-boolean v3, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->audioManager:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$300(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 225
    const-string/jumbo v3, "Always"

    const-string/jumbo v4, "Music was stopped, but PhraseSpotter is working"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto :goto_0

    .line 228
    :cond_5
    const-string/jumbo v3, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 229
    const-string/jumbo v3, "state"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 230
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto :goto_0

    .line 232
    :cond_6
    const-string/jumbo v3, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 233
    const-string/jumbo v3, "android.bluetooth.adapter.extra.CONNECTION_STATE"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 236
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto/16 :goto_0

    .line 238
    :cond_7
    const-string/jumbo v3, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 239
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 241
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;->this$0:Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    # invokes: Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V
    invoke-static {v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    goto/16 :goto_0
.end method
