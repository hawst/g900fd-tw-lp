.class public Lcom/samsung/alwaysmicon/AlwaysMicOnService;
.super Landroid/app/Service;
.source "AlwaysMicOnService.java"

# interfaces
.implements Lcom/vlingo/core/internal/phrasespotter/PhraseSpotterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/alwaysmicon/AlwaysMicOnService$1;,
        Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;,
        Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;
    }
.end annotation


# static fields
.field private static final EVENT_ID_FOR_VOICE_WAKEUP_WORD:Ljava/lang/String; = "VOICE_WAKEUP_WORD_ID"

.field public static final EXTRA_FROM_ALWAYS_MIC_ON:Ljava/lang/String; = "from_always_mic_on"

.field private static final MSG_DELAYED_REGISTER:I = 0x0

.field private static final PROPERTY_KEY_VRMODE:Ljava/lang/String; = "sys.hmt.connected"

.field private static final TAG:Ljava/lang/String; = "Always"

.field public static alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

.field public static startAlwaysPhraseSpotting:Z


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private mAlwaysUeventObserver:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

.field private mWakeupInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting:Z

    .line 124
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    new-instance v0, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    invoke-direct {v0, p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;-><init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    .line 43
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->audioManager:Landroid/media/AudioManager;

    .line 29
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/alwaysmicon/AlwaysMicOnService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startSpotting()V

    return-void
.end method

.method private launchSVoice()V
    .locals 6

    .prologue
    .line 141
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 143
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v3, "power"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 145
    .local v2, "pm":Landroid/os/PowerManager;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v1, "i":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 147
    const-string/jumbo v3, "from_always_mic_on"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 150
    const v3, 0x3000001a

    const-string/jumbo v4, "VoiceTalkTemp"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 154
    return-void
.end method

.method private startAlwaysPhraseSpotting()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 190
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    invoke-virtual {v0, v3}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->removeMessages(I)V

    .line 191
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 192
    return-void
.end method

.method private startSpotting()V
    .locals 3

    .prologue
    .line 157
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 160
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isBluetoothAudioOn()Z

    move-result v1

    if-nez v1, :cond_2

    .line 167
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "startPhraseSpotting() in AlwaysMicOnService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->startPhraseSpotting()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    :try_start_1
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "startSpotting(): BT Audio is ON, NOT starting PhraseSpotter"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private stopSpotting()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->removeMessages(I)V

    .line 182
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->isListening()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "stopPhraseSpotting() in AlwaysMicOnService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->getInstance()Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->stopPhraseSpotting()V

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 54
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 56
    new-instance v1, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    invoke-direct {v1}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;-><init>()V

    iput-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mAlwaysUeventObserver:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    .line 57
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mAlwaysUeventObserver:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    const-string/jumbo v2, "VOICE_WAKEUP_WORD_ID"

    invoke-virtual {v1, v2}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startObserving(Ljava/lang/String;)V

    .line 60
    :cond_0
    const-string/jumbo v1, "is_from_vlingoapplication"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->sendBroadcast(Landroid/content/Intent;)V

    .line 64
    const-string/jumbo v1, "is_from_vlingoapplication"

    invoke-static {v1, v3}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 67
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.CONTEXT_AWARE_MUSIC_INFO"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string/jumbo v1, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 69
    const-string/jumbo v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 70
    const-string/jumbo v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 72
    const-string/jumbo v1, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 73
    const-string/jumbo v1, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    new-instance v1, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$WakeupInfoReceiver;-><init>(Lcom/samsung/alwaysmicon/AlwaysMicOnService;Lcom/samsung/alwaysmicon/AlwaysMicOnService$1;)V

    iput-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mWakeupInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 76
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-nez v1, :cond_2

    .line 77
    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mWakeupInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 79
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->startAlwaysPhraseSpotting()V

    .line 84
    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 95
    const-string/jumbo v0, "is_from_vlingoapplication"

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 96
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mhandler:Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/alwaysmicon/AlwaysMicOnService$AlwaysMicOnHandler;->removeMessages(I)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mAlwaysUeventObserver:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mAlwaysUeventObserver:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    invoke-virtual {v0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->stopObserving()V

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->mWakeupInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 104
    invoke-static {}, Lcom/vlingo/core/internal/phrasespotter/PhraseSpotter;->destroy()V

    .line 105
    return-void
.end method

.method public onPhraseDetected(Ljava/lang/String;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 109
    return-void
.end method

.method public onPhraseSpotted(Ljava/lang/String;)V
    .locals 0
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    .line 113
    return-void
.end method

.method public onPhraseSpotterStarted()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public onPhraseSpotterStopped()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public onSeamlessPhraseSpotted(Ljava/lang/String;Lcom/vlingo/core/internal/audio/MicrophoneStream;)V
    .locals 4
    .param p1, "phrase"    # Ljava/lang/String;
    .param p2, "micStream"    # Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->stopSpotting()V

    .line 129
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v1

    const-string/jumbo v2, "sys.hmt.connected"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 131
    .local v0, "hmtConnected":Z
    if-nez v0, :cond_0

    .line 132
    sput-object p2, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->alwaysMicrophoneStream:Lcom/vlingo/core/internal/audio/MicrophoneStream;

    .line 134
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysMicOnService;->launchSVoice()V

    .line 138
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startid"    # I

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method
