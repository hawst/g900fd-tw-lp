.class public Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;
.super Landroid/os/UEventObserver;
.source "AlwaysOnAppLauncher.java"


# static fields
.field private static final DSP_PACKAGE_NAME:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field private static final EVENT_FOR_OKGOOGLE:Ljava/lang/String; = "2"

.field private static final EVENT_FOR_SVOICE:Ljava/lang/String; = "1"

.field private static final EVENT_ID_FOR_VOICE_WAKEUP_WORD:Ljava/lang/String; = "VOICE_WAKEUP_WORD_ID"

.field public static final EXTRA_FROM_ALWAYS_MIC_ON:Ljava/lang/String; = "from_always_mic_on"

.field private static final LAUNCH_DSP_ACTION_NAME:Ljava/lang/String; = "com.google.android.googlequicksearchbox.VOICE_SEARCH_DSP_HOTWORD"

.field private static final MSG_CHECK_FOR_SEAMLESS_WAKEUP:I = 0x0

.field private static final PROPERTY_KEY_VRMODE:Ljava/lang/String; = "sys.hmt.connected"

.field public static final TAG:Ljava/lang/String; = "Always"

.field public static micShouldNotOpened:Z

.field public static startCheckingListeningState:Z

.field public static wasScreenOff:Z


# instance fields
.field final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    .line 177
    new-instance v0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;

    invoke-direct {v0, p0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;-><init>(Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;)V

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private launchGSA()V
    .locals 6

    .prologue
    const/high16 v5, 0x10000000

    .line 162
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 164
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startLaunchBooster()V

    .line 166
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v4, "com.google.android.googlequicksearchbox.VOICE_SEARCH_DSP_HOTWORD"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v2, "launchDSPintent":Landroid/content/Intent;
    const-string/jumbo v4, "com.google.android.googlequicksearchbox"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 169
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .end local v2    # "launchDSPintent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "android.speech.action.WEB_SEARCH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 172
    .local v3, "launchDSPintentBackup":Landroid/content/Intent;
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 173
    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private launchSVoice()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 62
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 64
    .local v1, "context":Landroid/content/Context;
    const-string/jumbo v6, "audio"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 65
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v6, "power"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    .line 67
    .local v4, "pm":Landroid/os/PowerManager;
    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v6

    if-nez v6, :cond_0

    .line 68
    sput-boolean v10, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->wasScreenOff:Z

    .line 71
    :cond_0
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->bluetoothManagerInit()Z

    .line 73
    invoke-static {}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getInstance()Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;

    move-result-object v6

    const-string/jumbo v7, "sys.hmt.connected"

    invoke-virtual {v6, v7, v9}, Lcom/samsung/wrapper/reflect/SystemPropertiesWrapper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 75
    .local v2, "hmtConnected":Z
    invoke-static {}, Lcom/vlingo/core/internal/bluetooth/BluetoothManager;->isHeadsetConnected()Z

    move-result v6

    if-nez v6, :cond_1

    if-eqz v2, :cond_2

    .line 76
    :cond_1
    const-string/jumbo v6, "Always"

    const-string/jumbo v7, "Wake up word is Triggered. But S Voice should not be launched"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const-string/jumbo v6, "voice_wakeup_mic=off"

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 79
    const-string/jumbo v6, "Always"

    const-string/jumbo v7, "setParameters, voice_wakeup_mic=off"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    :goto_0
    return-void

    .line 83
    :cond_2
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startLaunchBooster()V

    .line 85
    const-string/jumbo v5, ""

    .line 86
    .local v5, "wakeUpWord":Ljava/lang/String;
    const-string/jumbo v6, "samsung_wakeup_engine_enable"

    invoke-static {v6, v9}, Lcom/vlingo/midas/settings/MidasSettings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 87
    const-string/jumbo v5, ""

    .line 91
    :goto_1
    invoke-direct {p0, v1}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldnotOpened(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v6

    if-nez v6, :cond_3

    .line 94
    const-string/jumbo v6, "seamless_voice=on"

    invoke-virtual {v0, v6}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 95
    const-string/jumbo v6, "always"

    const-string/jumbo v7, "setParameters, seamless_voice=on"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    sput-boolean v10, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startCheckingListeningState:Z

    .line 98
    invoke-static {v10}, Lcom/vlingo/midas/settings/MidasSettings;->setSeamlessWakeupStarted(Z)V

    .line 99
    iget-object v6, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    iget-object v6, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->mHandler:Landroid/os/Handler;

    const-wide/16 v7, 0xfa0

    invoke-virtual {v6, v9, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 103
    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/vlingo/midas/gui/ConversationActivity;

    invoke-direct {v3, v1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    .local v3, "i":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 105
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v6

    if-nez v6, :cond_4

    .line 106
    const v6, 0x8000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    :cond_4
    const-string/jumbo v6, "from_always_mic_on"

    invoke-virtual {v3, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 109
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 110
    const-string/jumbo v6, "wake_up_phrase_as_string"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    :cond_5
    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 114
    const v6, 0x3000001a

    const-string/jumbo v7, "VoiceTalkTemp"

    invoke-virtual {v4, v6, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    const-wide/16 v7, 0x3e8

    invoke-virtual {v6, v7, v8}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto/16 :goto_0

    .line 89
    .end local v3    # "i":Landroid/content/Intent;
    :cond_6
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/vlingo/midas/R$string;->wake_up_phrase_hi_galaxy:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private micShouldnotOpened(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 123
    const-string/jumbo v2, "keyguard"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 126
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCoverOpened()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "secure_voice_wake_up"

    invoke-static {v2, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    sput-boolean v1, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 130
    sget-boolean v1, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    return v1
.end method

.method private startLaunchBooster()V
    .locals 12

    .prologue
    const/16 v11, 0x7d0

    const/4 v10, 0x0

    .line 134
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 138
    .local v1, "context":Landroid/content/Context;
    new-instance v4, Landroid/os/DVFSHelper;

    const/16 v7, 0xc

    invoke-direct {v4, v1, v7}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 139
    .local v4, "mCpuHelper":Landroid/os/DVFSHelper;
    invoke-virtual {v4}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v6

    .line 140
    .local v6, "supportedCpuFreqTable":[I
    if-eqz v6, :cond_0

    .line 141
    const-string/jumbo v7, "CPU"

    aget v8, v6, v10

    int-to-long v8, v8

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 144
    :cond_0
    new-instance v3, Landroid/os/DVFSHelper;

    const/16 v7, 0xe

    invoke-direct {v3, v1, v7}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 145
    .local v3, "mCpuCoreNumHelper":Landroid/os/DVFSHelper;
    invoke-virtual {v3}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v5

    .line 146
    .local v5, "supportedCpuCoreNum":[I
    if-eqz v5, :cond_1

    .line 147
    const-string/jumbo v7, "CORE_NUM"

    aget v8, v5, v10

    int-to-long v8, v8

    invoke-virtual {v3, v7, v8, v9}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 150
    :cond_1
    new-instance v2, Landroid/os/DVFSHelper;

    const/16 v7, 0x13

    invoke-direct {v2, v1, v7}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 151
    .local v2, "mBusHelper":Landroid/os/DVFSHelper;
    invoke-virtual {v2}, Landroid/os/DVFSHelper;->getSupportedBUSFrequency()[I

    move-result-object v0

    .line 152
    .local v0, "SupportedBUSFreqTable":[I
    if-eqz v0, :cond_2

    .line 153
    const-string/jumbo v7, "BUS"

    aget v8, v0, v10

    int-to-long v8, v8

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 156
    :cond_2
    invoke-virtual {v4, v11}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 157
    invoke-virtual {v3, v11}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 158
    invoke-virtual {v2, v11}, Landroid/os/DVFSHelper;->acquire(I)V

    .line 159
    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/os/UEventObserver$UEvent;

    .prologue
    .line 43
    const-string/jumbo v1, "VOICE_WAKEUP_WORD_ID"

    invoke-virtual {p1, v1}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 46
    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 47
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCarModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    const-string/jumbo v1, "always"

    const-string/jumbo v2, "Uevent received = EVENT_FOR_SVOICE, but CarMode Setting is enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    const-string/jumbo v1, "always"

    const-string/jumbo v2, "Uevent received = EVENT_FOR_SVOICE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->launchSVoice()V

    goto :goto_0

    .line 54
    :cond_2
    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const-string/jumbo v1, "always"

    const-string/jumbo v2, "Uevent received = EVENT_FOR_OKGOOGLE"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->launchGSA()V

    goto :goto_0
.end method
