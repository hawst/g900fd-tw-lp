.class public Lcom/samsung/alwaysmicon/AlwaysOnReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AlwaysOnReceiver.java"


# static fields
.field private static final ACTION_CHANGE_GSA_LANGUAGE:Ljava/lang/String; = "com.google.android.googlequicksearchbox.CHANGE_VOICESEARCH_LANGUAGE"

.field private static final ACTION_GSA_ALWAYS_ON_PREFERENCE_CHANGED:Ljava/lang/String; = "com.google.android.googlequicksearchbox.CHANGE_ALWAYS_ON_PREFERENCE"

.field private static final ACTION_PHONE_STATE:Ljava/lang/String; = "android.intent.action.PHONE_STATE"

.field private static final EXTRA_GSA_LANGUAGE:Ljava/lang/String; = "language"

.field private static GSAPreferenceCheckStart:Z = false

.field private static final MSG_CALLSTATE_CHECK_START:I = 0x1

.field private static final MSG_DELAYED_VOICEWAKEUP_ON:I = 0x0

.field private static final MSG_DELAYED_VOICEWAKEUP_ON_FOR_CALL:I = 0x2

.field private static final MSG_GSA_PREFERENCE_CHECK_START:I = 0x3

.field private static final NORMAL_TYPE:I = 0x0

.field private static final SAMSUNG_CHANGE_ALWAYS_ON_PREFERENCE_VALUE:Ljava/lang/String; = "enabled"

.field private static final SAMSUNG_DELETE_VOICE_MODEL_INTENT:Ljava/lang/String; = "com.sec.action.GOOGLE_HOTWORD_DELETE"

.field private static final SAMSUNG_DSP_HOTWORD_SUPPORTED:Ljava/lang/String; = "dspSupported"

.field private static final SAMSUNG_DSP_LANGUAGE_SUPPORTED:Ljava/lang/String; = "languageSupported"

.field private static final SAMSUNG_DSP_PREFERENCE_ENABLED:Ljava/lang/String; = "preferenceEnabled"

.field public static final SAMSUNG_DSP_WAKEUP_SETTING_CHANGED:Ljava/lang/String; = "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

.field private static final SAMSUNG_HOTWORD_DELETED_BOOLEAN_EXTRA:Ljava/lang/String; = "hotwordDeleted"

.field private static final SAMSUNG_KEYWORD_EXTRA:Ljava/lang/String; = "keyword"

.field private static final SAMSUNG_QUERY_DSP_INFO_REQUEST:Ljava/lang/String; = "com.sec.action.QUERY_DSP_INFO"

.field private static final SAMSUNG_SPEAKER_MODEL_PRESENT:Ljava/lang/String; = "speakerModelPresent"

.field private static final SAMSUNG_VOICESEARCH_EXTRA_VOICESEARCH_LANGUAGE:Ljava/lang/String; = "language"

.field private static final SETTING_OFF_TYPE:I = 0x1

.field private static final SUPPORTED_LANGUAGE_FOR_GSA:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "Always"

.field public static callFromSVoice:Z = false

.field private static final localPath:Ljava/lang/String; = "/data/data/com.vlingo.midas/"

.field private static final systemPath:Ljava/lang/String; = "/system/wakeupdata/sensory/"

.field private static wasGSADSPSettingEnabled:Z


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field final mHandler:Landroid/os/Handler;

.field private telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "en-US"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "en-GB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "fr-FR"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "de-DE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "ru-RU"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->SUPPORTED_LANGUAGE_FOR_GSA:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 63
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 67
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    .line 390
    new-instance v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;

    invoke-direct {v0, p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$2;-><init>(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)V

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysOnReceiver;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 26
    sget-boolean v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 26
    sput-boolean p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z

    return p0
.end method

.method static synthetic access$400(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/alwaysmicon/AlwaysOnReceiver;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    return-void
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 26
    sput-boolean p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->GSAPreferenceCheckStart:Z

    return p0
.end method

.method private deleteOkGoogleModels()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 166
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isOkGoogleModelPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string/jumbo v1, "/data/data/com.vlingo.midas/GoogleNow_AP_recog.raw"

    invoke-virtual {p0, v1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->deleteData(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "/data/data/com.vlingo.midas/GoogleNow_AP_search.raw"

    invoke-virtual {p0, v1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->deleteData(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 170
    :cond_0
    return v0
.end method

.method private getLanguageString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 304
    const-string/jumbo v1, "language"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "languageString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 307
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "Entering the null string"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->languageSupportListCheck()V

    .line 309
    const-string/jumbo v1, "language"

    const-string/jumbo v2, "en-US"

    invoke-static {v1, v2}, Lcom/vlingo/core/internal/settings/Settings;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 312
    :cond_0
    const-string/jumbo v1, "-"

    const-string/jumbo v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 315
    const-string/jumbo v1, "v_es_la"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 316
    const-string/jumbo v0, "es_la"

    .line 318
    :cond_1
    return-object v0
.end method

.method public static isAlwayOnServiceRunning()Z
    .locals 6

    .prologue
    .line 285
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "activity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 289
    .local v2, "manager":Landroid/app/ActivityManager;
    const v4, 0x7fffffff

    invoke-virtual {v2, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 292
    .local v1, "listServices":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v1, :cond_1

    .line 293
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 294
    .local v3, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    const-class v4, Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 296
    const/4 v4, 0x1

    .line 300
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isDSPSupported()Z
    .locals 1

    .prologue
    .line 202
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isTPhoneGUI()Z

    move-result v0

    return v0
.end method

.method private isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "mFilePath"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 194
    .local v0, "mFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const/4 v1, 0x1

    .line 198
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isGSADSPSettingEnabled()Z
    .locals 2

    .prologue
    .line 251
    const-string/jumbo v0, "gsa_alwayson_preference_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isGSALanguageSupportable()Z
    .locals 2

    .prologue
    .line 269
    const-string/jumbo v0, "gsa_alwayson_language_support"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isOkGoogleModelPresent()Z
    .locals 1

    .prologue
    .line 183
    const-string/jumbo v0, "/data/data/com.vlingo.midas/GoogleNow_AP_recog.raw"

    invoke-direct {p0, v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "/data/data/com.vlingo.midas/GoogleNow_AP_search.raw"

    invoke-direct {p0, v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setGSALanguageSupportable(Ljava/lang/String;)V
    .locals 2
    .param p1, "voiceSearchLanguage"    # Ljava/lang/String;

    .prologue
    .line 255
    const-string/jumbo v0, "current_gsa_language"

    invoke-static {v0, p1}, Lcom/vlingo/core/internal/settings/Settings;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    sget-object v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->SUPPORTED_LANGUAGE_FOR_GSA:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 261
    :cond_0
    const-string/jumbo v0, "gsa_alwayson_language_support"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 265
    :goto_0
    return-void

    .line 263
    :cond_1
    const-string/jumbo v0, "gsa_alwayson_language_support"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private setGSAPreference(Z)V
    .locals 14
    .param p1, "enabled"    # Z

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 206
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSoundDetectorSettingOn()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 207
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 208
    .local v3, "context":Landroid/content/Context;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    .line 212
    .local v8, "systemLocale":Ljava/util/Locale;
    :try_start_0
    const-string/jumbo v10, "android.app.ActivityManagerNative"

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 213
    .local v1, "amnClass":Ljava/lang/Class;
    const/4 v0, 0x0

    .line 214
    .local v0, "amn":Ljava/lang/Object;
    const/4 v2, 0x0

    .line 217
    .local v2, "config":Landroid/content/res/Configuration;
    const-string/jumbo v10, "getDefault"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v1, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 218
    .local v6, "methodGetDefault":Ljava/lang/reflect/Method;
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 219
    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v6, v1, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 222
    const-string/jumbo v10, "getConfiguration"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v1, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 223
    .local v5, "methodGetConfiguration":Ljava/lang/reflect/Method;
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 224
    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v5, v0, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "config":Landroid/content/res/Configuration;
    check-cast v2, Landroid/content/res/Configuration;

    .line 225
    .restart local v2    # "config":Landroid/content/res/Configuration;
    iget-object v8, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v0    # "amn":Ljava/lang/Object;
    .end local v1    # "amnClass":Ljava/lang/Class;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "methodGetConfiguration":Ljava/lang/reflect/Method;
    .end local v6    # "methodGetDefault":Ljava/lang/reflect/Method;
    :goto_0
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    .line 233
    .local v7, "svoiceConfig":Landroid/content/res/Configuration;
    iput-object v8, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 234
    invoke-virtual {v3, v7}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v9

    .line 236
    .local v9, "systemLocaleContext":Landroid/content/Context;
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/vlingo/midas/R$string;->toast_while_running_sound_detector_for_gsa:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 240
    const-string/jumbo v10, "gsa_alwayson_preference_enabled"

    invoke-static {v10, v12}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    .line 247
    .end local v3    # "context":Landroid/content/Context;
    .end local v7    # "svoiceConfig":Landroid/content/res/Configuration;
    .end local v8    # "systemLocale":Ljava/util/Locale;
    .end local v9    # "systemLocaleContext":Landroid/content/Context;
    :goto_1
    return-void

    .line 227
    .restart local v3    # "context":Landroid/content/Context;
    .restart local v8    # "systemLocale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 229
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 242
    .end local v3    # "context":Landroid/content/Context;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "systemLocale":Ljava/util/Locale;
    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSALanguageSupportable()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 243
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->startAlwaysMicOnserviceIfNeeded()V

    .line 245
    :cond_1
    const-string/jumbo v10, "gsa_alwayson_preference_enabled"

    invoke-static {v10, p1}, Lcom/vlingo/core/internal/settings/Settings;->setBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method private setLanguageModelForWakeUp(I)V
    .locals 8
    .param p1, "setType"    # I

    .prologue
    .line 322
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSamsungDSPWakeupEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isCarModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 323
    .local v2, "isSamsungDSPWakeupEnabled":Ljava/lang/Boolean;
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isGoogleDSPWakeupEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 325
    .local v4, "isGoogleDSPWakeupEnabled":Ljava/lang/Boolean;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 327
    .local v6, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_1

    .line 328
    const-string/jumbo v0, "audio"

    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 332
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 335
    :cond_2
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->getLanguageString()Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "voicetalkLanguage":Ljava/lang/String;
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;-><init>(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;I)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 388
    .end local v3    # "voicetalkLanguage":Ljava/lang/String;
    :cond_3
    return-void

    .line 322
    .end local v2    # "isSamsungDSPWakeupEnabled":Ljava/lang/Boolean;
    .end local v4    # "isGoogleDSPWakeupEnabled":Ljava/lang/Boolean;
    .end local v6    # "context":Landroid/content/Context;
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startAlwaysMicOnserviceIfNeeded()V
    .locals 3

    .prologue
    .line 273
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isAlwayOnServiceRunning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/alwaysmicon/AlwaysMicOnService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 276
    .local v0, "startIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 278
    const-string/jumbo v1, "Always"

    const-string/jumbo v2, "Always service started in AlwaysOnReceiver. Because There was no Always On Settings which was turned on."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_0
    return-void
.end method


# virtual methods
.method public deleteData(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 174
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .local v1, "mFile":Ljava/io/File;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 176
    .local v0, "deleted":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 179
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    return v2
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x2

    const-wide/16 v8, 0x7d0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 73
    const/4 v0, 0x0

    .line 74
    .local v0, "action":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 75
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_0
    if-nez v0, :cond_2

    .line 163
    :cond_1
    :goto_0
    return-void

    .line 81
    :cond_2
    const-string/jumbo v4, "com.samsung.CUSTOM_COMMAND_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "com.vlingo.LANGUAGE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 83
    :cond_3
    invoke-direct {p0, v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    goto :goto_0

    .line 84
    :cond_4
    const-string/jumbo v4, "com.samsung.wakeupsettings.SVOICE_DSP_SETTING_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 85
    invoke-direct {p0, v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    .line 87
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysMicOnProvider;->notifyChange()V

    goto :goto_0

    .line 88
    :cond_5
    const-string/jumbo v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 89
    invoke-static {}, Lcom/samsung/alwaysmicon/AlwaysMicOnProvider;->notifyChange()V

    goto :goto_0

    .line 90
    :cond_6
    const-string/jumbo v4, "android.settings.CAR_MODE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 91
    invoke-direct {p0, v7}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    goto :goto_0

    .line 92
    :cond_7
    const-string/jumbo v4, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 93
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v5, "voice_wakeup_mic=on"

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 94
    const-string/jumbo v4, "Always"

    const-string/jumbo v5, "setParameters, voice_wakeup_mic=on"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 97
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 98
    :cond_8
    const-string/jumbo v4, "com.sec.android.automotive.drivelink.CAR_MODE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 99
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 100
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 101
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 104
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 105
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    const-wide/16 v5, 0xfa0

    invoke-virtual {v4, v7, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 107
    :cond_9
    const-string/jumbo v4, "com.sec.action.GOOGLE_HOTWORD_DELETE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 108
    invoke-virtual {p0, v7}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 109
    .local v1, "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "hotwordDeleted"

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->deleteOkGoogleModels()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 111
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_a
    const-string/jumbo v4, "com.sec.action.QUERY_DSP_INFO"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 112
    const-string/jumbo v4, "language"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "voiceSearchLanguage":Ljava/lang/String;
    const-string/jumbo v4, "keyword"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "voiceSearchKeyword":Ljava/lang/String;
    const-string/jumbo v4, "okgoogle"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 118
    invoke-direct {p0, v3}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setGSALanguageSupportable(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v7}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v1

    .line 121
    .restart local v1    # "bundle":Landroid/os/Bundle;
    const-string/jumbo v4, "dspSupported"

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isDSPSupported()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 122
    const-string/jumbo v4, "languageSupported"

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSALanguageSupportable()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 123
    const-string/jumbo v4, "speakerModelPresent"

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isOkGoogleModelPresent()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    const-string/jumbo v4, "preferenceEnabled"

    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 126
    const-string/jumbo v4, "Always"

    const-string/jumbo v5, "Feedback Intent has sent"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 128
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "voiceSearchKeyword":Ljava/lang/String;
    .end local v3    # "voiceSearchLanguage":Ljava/lang/String;
    :cond_b
    const-string/jumbo v4, "com.google.android.googlequicksearchbox.CHANGE_ALWAYS_ON_PREFERENCE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 129
    const-string/jumbo v4, "enabled"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setGSAPreference(Z)V

    .line 131
    sget-boolean v4, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->GSAPreferenceCheckStart:Z

    if-nez v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSoundDetectorSettingOn()Z

    move-result v4

    if-nez v4, :cond_1

    .line 132
    sput-boolean v7, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->GSAPreferenceCheckStart:Z

    .line 133
    invoke-direct {p0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isGSADSPSettingEnabled()Z

    move-result v4

    sput-boolean v4, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->wasGSADSPSettingEnabled:Z

    .line 135
    invoke-direct {p0, v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    .line 136
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 138
    :cond_c
    const-string/jumbo v4, "com.google.android.googlequicksearchbox.CHANGE_VOICESEARCH_LANGUAGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 139
    const-string/jumbo v4, "language"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    .restart local v3    # "voiceSearchLanguage":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setGSALanguageSupportable(Ljava/lang/String;)V

    .line 142
    invoke-direct {p0, v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V

    goto/16 :goto_0

    .line 143
    .end local v3    # "voiceSearchLanguage":Ljava/lang/String;
    :cond_d
    const-string/jumbo v4, "android.intent.action.PHONE_STATE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 144
    sget-boolean v4, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isAnyDSPWakeupEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    if-nez v4, :cond_1

    .line 148
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 149
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 151
    sput-boolean v5, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->callFromSVoice:Z

    goto/16 :goto_0

    .line 153
    :cond_e
    const-string/jumbo v4, "com.samsung.alwaysmicon.VOICEWAKEUP_SETTING_FOR_LPSD"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 154
    const-string/jumbo v4, "turnoff"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 155
    const-string/jumbo v4, "preferences_voice_wake_up_time"

    invoke-static {v4, v7}, Lcom/vlingo/midas/settings/MidasSettings;->setInt(Ljava/lang/String;I)V

    .line 156
    invoke-direct {p0, v5}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setGSAPreference(Z)V

    .line 158
    iget-object v4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v5, "voice_wakeup_mic=off"

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 159
    const-string/jumbo v4, "Always"

    const-string/jumbo v5, "Sound Detector is started, setParameters, voice_wakeup_mic=off"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
