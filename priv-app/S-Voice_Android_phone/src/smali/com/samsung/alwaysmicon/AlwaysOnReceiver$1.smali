.class Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;
.super Ljava/lang/Object;
.source "AlwaysOnReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->setLanguageModelForWakeUp(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

.field final synthetic val$isGoogleDSPWakeupEnabled:Ljava/lang/Boolean;

.field final synthetic val$isSamsungDSPWakeupEnabled:Ljava/lang/Boolean;

.field final synthetic val$setType:I

.field final synthetic val$voicetalkLanguage:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;I)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    iput-object p2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isSamsungDSPWakeupEnabled:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$voicetalkLanguage:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isGoogleDSPWakeupEnabled:Ljava/lang/Boolean;

    iput p5, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$setType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 339
    const/4 v7, 0x0

    .line 341
    .local v7, "result":I
    :try_start_0
    invoke-static {}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngineWrapper;->getInstance()Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isSamsungDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    const-string/jumbo v2, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    :goto_0
    iget-object v2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isSamsungDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string/jumbo v2, "/data/data/com.vlingo.midas/UDT_Always_AP_search.raw"

    :goto_1
    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isGoogleDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_8

    const-string/jumbo v3, "/data/data/com.vlingo.midas/GoogleNow_AP_recog.raw"

    :goto_2
    iget-object v5, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isGoogleDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string/jumbo v4, "/data/data/com.vlingo.midas/GoogleNow_AP_search.raw"

    :cond_0
    const-string/jumbo v5, "/data/data/com.vlingo.midas/"

    invoke-virtual/range {v0 .. v5}, Lcom/sensoryinc/fluentsoftsdk/SensoryUDTSIDEngine;->doCombineModel(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 358
    :goto_3
    if-ne v7, v8, :cond_b

    .line 359
    const-string/jumbo v1, "Always"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "doCombineModel Success samsung("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isSamsungDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/UDT_Always_AP_recog.raw"

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z
    invoke-static {v0, v3}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "UDT"

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "))"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " && Google("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$isGoogleDSPWakeupEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_keyword_path=%s"

    new-array v2, v8, [Ljava/lang/Object;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/COMBINED_Deep_recog.bin"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "keyword_grammar_path=%s"

    new-array v2, v8, [Ljava/lang/Object;

    const-string/jumbo v3, "/data/data/com.vlingo.midas/COMBINED_Deep_search.bin"

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 365
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_keyword_path=/data/data/com.vlingo.midas/COMBINED_Deep_recog.bin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, keyword_grammar_path=/data/data/com.vlingo.midas/COMBINED_Deep_search.bin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_Deep_recog.bin"

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    const-string/jumbo v1, "/data/data/com.vlingo.midas/UDT_Always_Deep_search.bin"

    # invokes: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->isFileExist(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$000(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isZeroDevice()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 372
    :cond_2
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_trig=150"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 373
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_trig=150"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :goto_5
    iget v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$setType:I

    if-nez v0, :cond_3

    .line 379
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_wakeup_mic=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 380
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=on"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_3
    :goto_6
    return-void

    .line 341
    :cond_4
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "/system/wakeupdata/sensory/samsung_wakeup_am_quiet_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$voicetalkLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_v2.raw"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_5
    move-object v1, v4

    goto/16 :goto_0

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "/system/wakeupdata/sensory/samsung_wakeup_grammar_quiet_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$voicetalkLanguage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_v2.raw"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto/16 :goto_1

    :cond_7
    move-object v2, v4

    goto/16 :goto_1

    :cond_8
    move-object v3, v4

    goto/16 :goto_2

    .line 354
    :catch_0
    move-exception v6

    .line 355
    .local v6, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v6}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto/16 :goto_3

    .line 359
    .end local v6    # "e":Ljava/lang/NoSuchMethodError;
    :cond_9
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->val$voicetalkLanguage:Ljava/lang/String;

    goto/16 :goto_4

    .line 375
    :cond_a
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnReceiver$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnReceiver;

    # getter for: Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/alwaysmicon/AlwaysOnReceiver;->access$100(Lcom/samsung/alwaysmicon/AlwaysOnReceiver;)Landroid/media/AudioManager;

    move-result-object v0

    const-string/jumbo v1, "voice_trig=1200"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 376
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_trig=1200"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 383
    :cond_b
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "doCombineModel failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method
