.class Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;
.super Landroid/os/Handler;
.source "AlwaysOnAppLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field audioManager:Landroid/media/AudioManager;

.field context:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;


# direct methods
.method constructor <init>(Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;)V
    .locals 2

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->this$0:Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 178
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->audioManager:Landroid/media/AudioManager;

    .line 181
    invoke-static {}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getInstance()Lcom/vlingo/core/internal/util/ApplicationAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/core/internal/util/ApplicationAdapter;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 184
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_1

    .line 185
    invoke-static {}, Lcom/vlingo/midas/VlingoApplication;->getInstance()Lcom/vlingo/midas/VlingoApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vlingo/midas/VlingoApplication;->isAppInForeground()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startCheckingListeningState:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/vlingo/midas/settings/MidasSettings;->isSeamlessWakeupStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "Wake up Triggered. But Failed to start Recording"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "seamless_voice=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 189
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, seamless_voice=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-static {v2}, Lcom/vlingo/midas/settings/MidasSettings;->setSeamlessWakeupStarted(Z)V

    .line 192
    sput-boolean v2, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->micShouldNotOpened:Z

    .line 193
    sput-boolean v2, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher;->startCheckingListeningState:Z

    .line 195
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->audioManager:Landroid/media/AudioManager;

    const-string/jumbo v1, "voice_wakeup_mic=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 196
    const-string/jumbo v0, "Always"

    const-string/jumbo v1, "setParameters, voice_wakeup_mic=off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/samsung/alwaysmicon/AlwaysOnAppLauncher$1;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.vlingo.midas.ACTION_VOICEWAKEUP_START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 201
    :cond_1
    return-void
.end method
