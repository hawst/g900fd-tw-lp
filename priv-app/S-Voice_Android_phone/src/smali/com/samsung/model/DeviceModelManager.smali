.class public Lcom/samsung/model/DeviceModelManager;
.super Ljava/lang/Object;
.source "DeviceModelManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isHDevice()Z
    .locals 1

    .prologue
    .line 26
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isHDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    .line 15
    if-eqz p0, :cond_1

    const-string/jumbo v0, "SM-N900"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "ASH"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Madrid"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "JS01"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SC-01F"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SCL22"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SC-02F"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SM-G910"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    :cond_0
    const/4 v0, 0x1

    .line 22
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJDevice()Z
    .locals 1

    .prologue
    .line 39
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isJDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isJDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    .line 31
    if-eqz p0, :cond_1

    const-string/jumbo v0, "L720T"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "GT-I9505"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNote4Device()Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/model/DeviceModelManager;->isNote4Device(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isNote4Device(Ljava/lang/String;)Z
    .locals 1
    .param p0, "build_model"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method
