.class Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;
.super Ljava/lang/Object;
.source "FileAuditLogListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bzbyte/util/audit/FileAuditLogListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Worker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;


# direct methods
.method constructor <init>(Lcom/bzbyte/util/audit/FileAuditLogListener;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 69
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    # getter for: Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$000(Lcom/bzbyte/util/audit/FileAuditLogListener;)Ljava/io/FileOutputStream;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    # getter for: Lcom/bzbyte/util/audit/FileAuditLogListener;->ivLastAccess:J
    invoke-static {v3}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$100(Lcom/bzbyte/util/audit/FileAuditLogListener;)J

    move-result-wide v3

    sub-long/2addr v1, v3

    iget-object v3, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    # getter for: Lcom/bzbyte/util/audit/FileAuditLogListener;->ivTimeout:J
    invoke-static {v3}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$200(Lcom/bzbyte/util/audit/FileAuditLogListener;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    # invokes: Lcom/bzbyte/util/audit/FileAuditLogListener;->closeStream()V
    invoke-static {v1}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$300(Lcom/bzbyte/util/audit/FileAuditLogListener;)V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    # getter for: Lcom/bzbyte/util/audit/FileAuditLogListener;->ivTimeout:J
    invoke-static {v1}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$200(Lcom/bzbyte/util/audit/FileAuditLogListener;)J

    move-result-wide v1

    const-wide/16 v3, 0x2

    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V

    .line 82
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;->this$0:Lcom/bzbyte/util/audit/FileAuditLogListener;

    const/4 v2, 0x0

    # setter for: Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;
    invoke-static {v1, v2}, Lcom/bzbyte/util/audit/FileAuditLogListener;->access$402(Lcom/bzbyte/util/audit/FileAuditLogListener;Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;)Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    .line 83
    return-void
.end method
