.class public Lcom/bzbyte/util/audit/OutputAuditLogListener;
.super Ljava/lang/Object;
.source "OutputAuditLogListener.java"

# interfaces
.implements Lcom/bzbyte/util/audit/AuditLogListener;


# instance fields
.field private ivPout:Ljava/io/PrintWriter;

.field private ivXMLEncode:Z


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/bzbyte/util/audit/OutputAuditLogListener;->ivXMLEncode:Z

    .line 25
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, p1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/bzbyte/util/audit/OutputAuditLogListener;->ivPout:Ljava/io/PrintWriter;

    .line 26
    return-void
.end method


# virtual methods
.method public processAuditLogEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    .locals 2
    .param p1, "ev"    # Lcom/bzbyte/util/audit/AuditLogEvent;

    .prologue
    .line 41
    invoke-static {p1}, Lcom/bzbyte/util/audit/AuditLogUtil;->toString(Lcom/bzbyte/util/audit/AuditLogEvent;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "str":Ljava/lang/String;
    iget-boolean v1, p0, Lcom/bzbyte/util/audit/OutputAuditLogListener;->ivXMLEncode:Z

    if-eqz v1, :cond_0

    .line 43
    invoke-static {v0}, Lcom/bzbyte/util/xml/XMLUtil;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/bzbyte/util/audit/OutputAuditLogListener;->ivPout:Ljava/io/PrintWriter;

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 51
    return-void
.end method
