.class public Lcom/bzbyte/util/audit/FileAuditLogListener;
.super Ljava/lang/Object;
.source "FileAuditLogListener.java"

# interfaces
.implements Lcom/bzbyte/util/audit/AuditLogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;
    }
.end annotation


# instance fields
.field private ivFile:Ljava/io/File;

.field private ivFout:Ljava/io/FileOutputStream;

.field private ivLastAccess:J

.field private ivPout:Ljava/io/PrintStream;

.field private ivTimeout:J

.field private ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivLastAccess:J

    .line 48
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivTimeout:J

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    .line 58
    iput-object p1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFile:Ljava/io/File;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/bzbyte/util/audit/FileAuditLogListener;)Ljava/io/FileOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/bzbyte/util/audit/FileAuditLogListener;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    return-object v0
.end method

.method static synthetic access$100(Lcom/bzbyte/util/audit/FileAuditLogListener;)J
    .locals 2
    .param p0, "x0"    # Lcom/bzbyte/util/audit/FileAuditLogListener;

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivLastAccess:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/bzbyte/util/audit/FileAuditLogListener;)J
    .locals 2
    .param p0, "x0"    # Lcom/bzbyte/util/audit/FileAuditLogListener;

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivTimeout:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/bzbyte/util/audit/FileAuditLogListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/bzbyte/util/audit/FileAuditLogListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/bzbyte/util/audit/FileAuditLogListener;->closeStream()V

    return-void
.end method

.method static synthetic access$402(Lcom/bzbyte/util/audit/FileAuditLogListener;Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;)Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;
    .locals 0
    .param p0, "x0"    # Lcom/bzbyte/util/audit/FileAuditLogListener;
    .param p1, "x1"    # Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    return-object p1
.end method

.method private declared-synchronized closeStream()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivPout:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->flush()V

    .line 92
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivPout:Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :cond_0
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized openStream()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    if-nez v0, :cond_0

    .line 102
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFile:Ljava/io/File;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    .line 103
    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivFout:Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivPout:Ljava/io/PrintStream;

    .line 104
    iget-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    invoke-direct {v0, p0}, Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;-><init>(Lcom/bzbyte/util/audit/FileAuditLogListener;)V

    iput-object v0, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    .line 107
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivWorker:Lcom/bzbyte/util/audit/FileAuditLogListener$Worker;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit p0

    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public processAuditLogEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    .locals 10
    .param p1, "ev"    # Lcom/bzbyte/util/audit/AuditLogEvent;

    .prologue
    .line 117
    :try_start_0
    invoke-direct {p0}, Lcom/bzbyte/util/audit/FileAuditLogListener;->openStream()V

    .line 118
    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getSourceClassName()Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "sourceStr":Ljava/lang/String;
    const-string/jumbo v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "shortName":Ljava/lang/String;
    iget-object v3, p0, Lcom/bzbyte/util/audit/FileAuditLogListener;->ivPout:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "<Audit "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "type"

    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getTypeName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bzbyte/util/WebUtil;->genAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "message"

    invoke-virtual {p1}, Lcom/bzbyte/util/audit/AuditLogEvent;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/bzbyte/util/xml/XMLUtil;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bzbyte/util/WebUtil;->genAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "source"

    invoke-static {v5, v0}, Lcom/bzbyte/util/WebUtil;->genAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "time"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/sql/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/bzbyte/util/WebUtil;->genAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "/>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    .end local v0    # "shortName":Ljava/lang/String;
    .end local v1    # "sourceStr":Ljava/lang/String;
    :goto_0
    return-void

    .line 127
    :catch_0
    move-exception v2

    .line 129
    .local v2, "th":Ljava/lang/Throwable;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method
