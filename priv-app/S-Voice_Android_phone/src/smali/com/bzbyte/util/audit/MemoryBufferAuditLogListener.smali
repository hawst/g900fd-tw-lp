.class public Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;
.super Ljava/lang/Object;
.source "MemoryBufferAuditLogListener.java"

# interfaces
.implements Lcom/bzbyte/util/audit/AuditLogListener;


# instance fields
.field private ivBuffer:Ljava/lang/String;

.field private ivMaxSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    .line 39
    const/16 v0, 0x2710

    iput v0, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivMaxSize:I

    return-void
.end method


# virtual methods
.method public processAuditLogEvent(Lcom/bzbyte/util/audit/AuditLogEvent;)V
    .locals 4
    .param p1, "ev"    # Lcom/bzbyte/util/audit/AuditLogEvent;

    .prologue
    .line 43
    invoke-static {p1}, Lcom/bzbyte/util/audit/AuditLogUtil;->toString(Lcom/bzbyte/util/audit/AuditLogEvent;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "stuff":Ljava/lang/String;
    const-string/jumbo v2, "<"

    const-string/jumbo v3, "["

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    const-string/jumbo v2, ">"

    const-string/jumbo v3, "]"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    .line 48
    iget-object v2, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivMaxSize:I

    if-le v2, v3, :cond_0

    .line 50
    iget-object v2, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivMaxSize:I

    sub-int v0, v2, v3

    .line 51
    .local v0, "flushSize":I
    iget-object v2, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/bzbyte/util/audit/MemoryBufferAuditLogListener;->ivBuffer:Ljava/lang/String;

    .line 53
    .end local v0    # "flushSize":I
    :cond_0
    return-void
.end method
