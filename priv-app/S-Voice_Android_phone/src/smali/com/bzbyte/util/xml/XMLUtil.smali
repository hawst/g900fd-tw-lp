.class public Lcom/bzbyte/util/xml/XMLUtil;
.super Ljava/lang/Object;
.source "XMLUtil.java"


# static fields
.field private static ivInst:Lcom/bzbyte/util/xml/XMLUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/bzbyte/util/xml/XMLUtil;

    invoke-direct {v0}, Lcom/bzbyte/util/xml/XMLUtil;-><init>()V

    sput-object v0, Lcom/bzbyte/util/xml/XMLUtil;->ivInst:Lcom/bzbyte/util/xml/XMLUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "val"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 193
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 194
    .local v2, "pout":Ljava/io/PrintWriter;
    new-instance v0, Lcom/bzbyte/util/DefaultSimplePrintWriter;

    invoke-direct {v0, v2}, Lcom/bzbyte/util/DefaultSimplePrintWriter;-><init>(Ljava/io/PrintWriter;)V

    .line 195
    .local v0, "dspw":Lcom/bzbyte/util/DefaultSimplePrintWriter;
    invoke-static {p0, v0}, Lcom/bzbyte/util/xml/XMLUtil;->encode(Ljava/lang/String;Lcom/bzbyte/util/SimplePrintWriter;)V

    .line 196
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 197
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static encode(Ljava/lang/String;Lcom/bzbyte/util/SimplePrintWriter;)V
    .locals 6
    .param p0, "val"    # Ljava/lang/String;
    .param p1, "pout"    # Lcom/bzbyte/util/SimplePrintWriter;

    .prologue
    const/16 v5, 0x20

    .line 157
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 158
    .local v0, "chars":[C
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_6

    .line 160
    aget-char v2, v0, v1

    .line 161
    .local v2, "it":C
    const/16 v3, 0x26

    if-ne v2, v3, :cond_0

    .line 162
    const-string/jumbo v3, "&amp;"

    invoke-interface {p1, v3}, Lcom/bzbyte/util/SimplePrintWriter;->print(Ljava/lang/String;)V

    .line 158
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    :cond_0
    const/16 v3, 0x3c

    if-ne v2, v3, :cond_1

    .line 164
    const-string/jumbo v3, "&lt;"

    invoke-interface {p1, v3}, Lcom/bzbyte/util/SimplePrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 165
    :cond_1
    const/16 v3, 0x3e

    if-ne v2, v3, :cond_2

    .line 166
    const-string/jumbo v3, "&gt;"

    invoke-interface {p1, v3}, Lcom/bzbyte/util/SimplePrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 167
    :cond_2
    const/16 v3, 0x22

    if-ne v2, v3, :cond_3

    .line 168
    const-string/jumbo v3, "&quot;"

    invoke-interface {p1, v3}, Lcom/bzbyte/util/SimplePrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 171
    :cond_3
    if-ge v2, v5, :cond_4

    .line 173
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "&#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/bzbyte/util/SimplePrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    .line 175
    :cond_4
    if-lt v2, v5, :cond_5

    .line 177
    invoke-interface {p1, v2}, Lcom/bzbyte/util/SimplePrintWriter;->print(C)V

    goto :goto_1

    .line 181
    :cond_5
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Dont know how to encode char:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " dec:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 186
    .end local v2    # "it":C
    :cond_6
    return-void
.end method

.method public static genAtr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/bzbyte/util/xml/XMLUtil;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getBooleanAtr(Lorg/w3c/dom/Node;Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "node"    # Lorg/w3c/dom/Node;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "valueIfNull"    # Z

    .prologue
    .line 71
    invoke-static {p0, p1}, Lcom/bzbyte/util/xml/XMLUtil;->getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 75
    .end local p2    # "valueIfNull":Z
    :goto_0
    return p2

    .restart local p2    # "valueIfNull":Z
    :cond_0
    const-string/jumbo v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    goto :goto_0
.end method

.method public static getStringAtr(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "node"    # Lorg/w3c/dom/Node;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 49
    if-nez p0, :cond_0

    .line 50
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Node may not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    if-nez v2, :cond_2

    .line 57
    :cond_1
    :goto_0
    return-object v1

    .line 53
    :cond_2
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    invoke-interface {v2, p1}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 54
    .local v0, "nd":Lorg/w3c/dom/Node;
    if-eqz v0, :cond_1

    .line 55
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static isFiller(Lorg/w3c/dom/Node;)Z
    .locals 2
    .param p0, "nd"    # Lorg/w3c/dom/Node;

    .prologue
    .line 132
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x1

    .line 135
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDocument(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;

    .prologue
    .line 105
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 106
    .local v2, "fact":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 108
    .local v0, "db":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v0, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 112
    .end local v0    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "fact":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string/jumbo v4, "Failed to parse XML"

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static parseDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 2
    .param p0, "resourceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 144
    .local v0, "in":Ljava/io/InputStream;
    :try_start_0
    const-class v1, Lcom/bzbyte/util/xml/XMLUtil;

    invoke-virtual {v1, p0}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 145
    invoke-static {v0}, Lcom/bzbyte/util/xml/XMLUtil;->parseDocument(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v1

    .line 149
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v1
.end method
