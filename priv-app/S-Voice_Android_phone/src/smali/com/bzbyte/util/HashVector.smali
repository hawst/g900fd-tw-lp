.class public Lcom/bzbyte/util/HashVector;
.super Ljava/lang/Object;
.source "HashVector.java"


# instance fields
.field private ivKeys:Ljava/util/ArrayList;

.field private ivUnique:Z

.field private ivValues:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/HashVector;->ivValues:Ljava/util/Map;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/bzbyte/util/HashVector;->ivUnique:Z

    .line 47
    return-void
.end method

.method private checkPrev(Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 67
    iget-object v1, p0, Lcom/bzbyte/util/HashVector;->ivValues:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 68
    .local v0, "prev":Ljava/lang/Object;
    iget-boolean v1, p0, Lcom/bzbyte/util/HashVector;->ivUnique:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 70
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Key alrady in collection and unique mode. Try removing first:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_0
    if-eqz v0, :cond_1

    .line 75
    iget-object v1, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 77
    :cond_1
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivValues:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getKey(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/bzbyte/util/HashVector;->checkPrev(Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivValues:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 107
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivValues:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/bzbyte/util/HashVector;->ivKeys:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
