.class Lcom/bzbyte/util/Cache$CacheItem;
.super Ljava/lang/Object;
.source "Cache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/bzbyte/util/Cache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CacheItem"
.end annotation


# instance fields
.field private ivLastAccess:J

.field private ivObj:Ljava/lang/Object;


# virtual methods
.method public getLastAccessTime()J
    .locals 2

    .prologue
    .line 212
    iget-wide v0, p0, Lcom/bzbyte/util/Cache$CacheItem;->ivLastAccess:J

    return-wide v0
.end method

.method public getObject()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 206
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/bzbyte/util/Cache$CacheItem;->ivLastAccess:J

    .line 207
    iget-object v0, p0, Lcom/bzbyte/util/Cache$CacheItem;->ivObj:Ljava/lang/Object;

    return-object v0
.end method
