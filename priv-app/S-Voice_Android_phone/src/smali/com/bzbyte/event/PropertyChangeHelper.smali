.class public Lcom/bzbyte/event/PropertyChangeHelper;
.super Ljava/lang/Object;
.source "PropertyChangeHelper.java"


# instance fields
.field private ivListeners:Lcom/bzbyte/util/HashVector;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/bzbyte/util/HashVector;

    invoke-direct {v0}, Lcom/bzbyte/util/HashVector;-><init>()V

    iput-object v0, p0, Lcom/bzbyte/event/PropertyChangeHelper;->ivListeners:Lcom/bzbyte/util/HashVector;

    .line 21
    return-void
.end method


# virtual methods
.method public addPropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/bzbyte/event/PropertyChangeListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/bzbyte/event/PropertyChangeHelper;->ivListeners:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v0, p1, p1}, Lcom/bzbyte/util/HashVector;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public removePropertyChangeListener(Lcom/bzbyte/event/PropertyChangeListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/bzbyte/event/PropertyChangeListener;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/bzbyte/event/PropertyChangeHelper;->ivListeners:Lcom/bzbyte/util/HashVector;

    invoke-virtual {v0, p1}, Lcom/bzbyte/util/HashVector;->remove(Ljava/lang/Object;)V

    .line 48
    return-void
.end method
