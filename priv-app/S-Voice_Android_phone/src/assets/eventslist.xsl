<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
    <xsl:template name="contact" match="FieldGroup[@n='contact']">
        <Contact>
            <xsl:attribute name="Name">
                <xsl:value-of select="Field[@n='name']/@v"/>
            </xsl:attribute>
            <xsl:attribute name="Id">
                <xsl:value-of select="Field[@n='id']/@v"/>
            </xsl:attribute>
            <xsl:attribute name="Score">
                <xsl:value-of select="Field[@n='score']/@v"/>
            </xsl:attribute>
            <Addresses>
                <xsl:for-each select="FieldGroup/FieldGroup[@n='address']">
                    <Address>
                        <xsl:attribute name="Detail">
                            <xsl:value-of select="Field[@n='detail']/@v"/>
                        </xsl:attribute>
                        <xsl:attribute name="Type">
                            <xsl:value-of select="Field[@n='type']/@v"/>
                        </xsl:attribute>
                        <xsl:attribute name="Id">
                            <xsl:value-of select="Field[@n='id']/@v"/>
                        </xsl:attribute>
                    </Address>
                </xsl:for-each>
            </Addresses>
        </Contact>
    </xsl:template>
    <xsl:template match="/">
        <EventList>
            <Events>
                <xsl:for-each select="Events/Event">
                    <xsl:choose>
                        <xsl:when test="@n='message-resolved'">
                            <MessageResolvedEvent>
                                <xsl:attribute name="NumMatches">
                                    <xsl:value-of select="Field[@n='matchcount']/@v"/>
                                </xsl:attribute>
                                <Messages>
                                    <xsl:for-each select="FieldGroup/FieldGroup[@n='message']">
                                        <Message>
                                            <xsl:attribute name="Id">
                                                <xsl:value-of select="Field[@n='id']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="SenderName">
                                                <xsl:value-of select="Field[@n='sender.name']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="SenderPhone">
                                                <xsl:value-of select="Field[@n='sender.phone']/@v"/>
                                            </xsl:attribute>
                                        </Message>
                                    </xsl:for-each>
                                </Messages>
                            </MessageResolvedEvent>
                        </xsl:when>
                        <xsl:when test="@n='alarm-resolved'">
                            <AlarmResolvedEvent>
                                <xsl:attribute name="NumMatches">
                                    <xsl:value-of select="Field[@n='matchcount']/@v"/>
                                </xsl:attribute>
                                <Alarms>
                                    <xsl:for-each select="FieldGroup/FieldGroup[@n='alarm']">
                                        <Alarm>
                                            <xsl:for-each select="Field[@n]">
                                                <xsl:choose>
                                                    <xsl:when test="@n='id'">
                                                        <xsl:attribute name="Id">
                                                            <xsl:value-of select="@v"/>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="@n='time'">
                                                        <xsl:attribute name="Time">
                                                            <xsl:value-of select="@v"/>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="@n='set'">
                                                        <xsl:attribute name="Set">
                                                            <xsl:value-of select="@v"/>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="@n='days'">
                                                        <xsl:attribute name="Days">
                                                            <xsl:value-of select="@v"/>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                    <xsl:when test="@n='repeat'">
                                                        <xsl:attribute name="Repeat">
                                                            <xsl:value-of select="@v"/>
                                                        </xsl:attribute>
                                                    </xsl:when>
                                                </xsl:choose>

                                            </xsl:for-each>
                                        </Alarm>
                                    </xsl:for-each>
                                </Alarms>
                            </AlarmResolvedEvent>
                        </xsl:when>
                        <xsl:when test="@n='task-resolved'">
                            <TaskResolvedEvent>
                                <xsl:attribute name="NumMatches">
                                    <xsl:value-of select="Field[@n='matchcount']/@v"/>
                                </xsl:attribute>
                                <Tasks>
                                    <xsl:for-each select="FieldGroup/FieldGroup[@n='task']">
                                        <Task>
                                            <xsl:attribute name="Id">
                                                <xsl:value-of select="Field[@n='id']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Title">
                                                <xsl:value-of select="Field[@n='title']/@v"/>
                                            </xsl:attribute>
                                            <xsl:choose>
                                                <xsl:when test="Field[@n='reminder.set']">
                                                    <xsl:attribute name="ReminderSet">
                                                        <xsl:value-of select="Field[@n='reminder.set']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="Field[@n='date']">
                                                    <xsl:attribute name="Date">
                                                        <xsl:value-of select="Field[@n='date']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="Field[@n='reminder.date']">
                                                    <xsl:attribute name="ReminderDate">
                                                        <xsl:value-of select="Field[@n='reminder.date']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="Field[@n='reminder.time']">
                                                    <xsl:attribute name="ReminderTime">
                                                        <xsl:value-of select="Field[@n='reminder.time']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                                <xsl:when test="Field[@n='complete']">
                                                    <xsl:attribute name="Complete">
                                                        <xsl:value-of select="Field[@n='complete']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                            </xsl:choose>
                                        </Task>
                                    </xsl:for-each>
                                </Tasks>
                            </TaskResolvedEvent>
                        </xsl:when>
                        <xsl:when test="@n='appointment-resolved'">
                            <AppointmentResolvedEvent>
                                <xsl:attribute name="NumMatches">
                                    <xsl:value-of select="Field[@n='matchcount']/@v"/>
                                </xsl:attribute>
                                <Appointments>
                                    <xsl:for-each select="FieldGroup/FieldGroup[@n='appointment']">
                                        <Appointment>
                                            <xsl:attribute name="Id">
                                                <xsl:value-of select="Field[@n='id']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Title">
                                                <xsl:value-of select="Field[@n='title']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Time">
                                                <xsl:value-of select="Field[@n='time']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Date">
                                                <xsl:value-of select="Field[@n='date']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Location">
                                                <xsl:value-of select="Field[@n='location']/@v"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="Duration">
                                                <xsl:value-of select="Field[@n='duration']/@v"/>
                                            </xsl:attribute>
                                            <xsl:choose>
                                                <xsl:when test="Field[@n='all.day']">
                                                    <xsl:attribute name="AllDay">
                                                        <xsl:value-of select="Field[@n='all.day']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                            </xsl:choose>
                                            <xsl:choose>
                                                <xsl:when test="Field[@n='readonly']">
                                                    <xsl:attribute name="ReadOnly">
                                                        <xsl:value-of select="Field[@n='readonly']/@v"/>
                                                    </xsl:attribute>
                                                </xsl:when>
                                            </xsl:choose>
                                            <Invitees>
                                                <xsl:for-each select="FieldGroup/FieldGroup[@n='invitee']">
                                                    <Contact>
                                                        <xsl:attribute name="Name">
                                                            <xsl:value-of select="Field[@n='name']/@v"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="Id">
                                                            <xsl:value-of select="Field[@n='id']/@v"/>
                                                        </xsl:attribute>
                                                    </Contact>
                                                </xsl:for-each>
                                                <xsl:apply-templates select="FieldGroup[@n='invitees']"/>
                                            </Invitees>
                                        </Appointment>
                                    </xsl:for-each>
                                </Appointments>
                            </AppointmentResolvedEvent>
                        </xsl:when>
                        <xsl:when test="@n='contact-resolved'">
                            <ContactResolvedEvent>
                                <xsl:attribute name="NumMatches">
                                    <xsl:value-of select="Field[@n='matchcount']/@v"/>
                                </xsl:attribute>
                                <Matches>
                                    <xsl:apply-templates select="FieldGroup[@n='contacts']"/>
                                </Matches>
                            </ContactResolvedEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-confirmed'">
                            <ActionConfirmedEvent>
                            </ActionConfirmedEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-cancelled'">
                            <ActionCancelledEvent>
                            </ActionCancelledEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-completed'">
                            <ActionCompletedEvent>
                            </ActionCompletedEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-failed'">
                            <ActionFailedEvent>
                                <xsl:attribute name="Reason">
                                    <xsl:value-of select="Field[@n='reason']/@v"/>
                                </xsl:attribute>
                            </ActionFailedEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-call'">
                            <ActionCallEvent>
                                <xsl:attribute name="Id">
                                    <xsl:value-of select="Field[@n='id']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderName">
                                    <xsl:value-of select="Field[@n='sender.name']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderPhone">
                                    <xsl:value-of select="Field[@n='sender.phone']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="FieldId">
                                    <xsl:value-of select="Field[@n='fieldid']/@v"/>
                                </xsl:attribute>
                            </ActionCallEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-forward'">
                            <ActionForwardEvent>
                                <xsl:attribute name="Id">
                                    <xsl:value-of select="Field[@n='id']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderName">
                                    <xsl:value-of select="Field[@n='sender.name']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderPhone">
                                    <xsl:value-of select="Field[@n='sender.phone']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="FieldId">
                                    <xsl:value-of select="Field[@n='fieldid']/@v"/>
                                </xsl:attribute>
                            </ActionForwardEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-reply'">
                            <ActionReplyEvent>
                                <xsl:attribute name="Id">
                                    <xsl:value-of select="Field[@n='id']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderName">
                                    <xsl:value-of select="Field[@n='sender.name']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="SenderPhone">
                                    <xsl:value-of select="Field[@n='sender.phone']/@v"/>
                                </xsl:attribute>
                                <xsl:attribute name="FieldId">
                                    <xsl:value-of select="Field[@n='fieldid']/@v"/>
                                </xsl:attribute>
                            </ActionReplyEvent>
                        </xsl:when>
                        <xsl:when test="@n='action-nlu'">
                            <ActionNluEvent>
                                <xsl:attribute name="ParseType">
                                    <xsl:value-of select="Field[@n='parsetype']/@v"/>
                                </xsl:attribute>
                            </ActionNluEvent>
                        </xsl:when>
                        <xsl:when test="@n='choice-selected'">
                            <ChoiceSelectedEvent>
                                <xsl:attribute name="ChoiceUid">
                                    <xsl:value-of select="Field[@n='choice-uid']/@v"/>
                                </xsl:attribute>
                            </ChoiceSelectedEvent>
                        </xsl:when>
                        <xsl:when test="@n='content-changed'">
                            <ContentChangedEvent>
                                <xsl:attribute name="Key">
                                    <xsl:value-of select="Field/@n"/>
                                </xsl:attribute>
                                <xsl:attribute name="Value">
                                    <xsl:value-of select="Field/@v"/>
                                </xsl:attribute>
                            </ContentChangedEvent>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </Events>
        </EventList>
    </xsl:template>
</xsl:stylesheet>