.class Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;
.super Ljava/lang/Object;
.source "PrivacyManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/privacy/PrivacyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-static {p2}, Lcom/samsung/android/providers/context/privacy/IPrivacyManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$0(Lcom/samsung/android/providers/context/privacy/PrivacyManager;Lcom/samsung/android/providers/context/privacy/IPrivacyManager;)V

    .line 400
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    # getter for: Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$1(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    # getter for: Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$1(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;->onServiceConnected()V

    .line 403
    :cond_0
    const-string v0, "ContextFramework:PrivacyManager"

    const-string v1, "Service for PrivacyManager is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v2, 0x0

    .line 408
    const-string v0, "ContextFramework:PrivacyManager"

    const-string v1, "Service for PrivacyManager is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-static {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$0(Lcom/samsung/android/providers/context/privacy/PrivacyManager;Lcom/samsung/android/providers/context/privacy/IPrivacyManager;)V

    .line 411
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    # getter for: Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$1(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    # getter for: Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$1(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;->onServiceDisconnected()V

    .line 413
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;->this$0:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-static {v0, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->access$2(Lcom/samsung/android/providers/context/privacy/PrivacyManager;Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;)V

    .line 415
    :cond_0
    return-void
.end method
