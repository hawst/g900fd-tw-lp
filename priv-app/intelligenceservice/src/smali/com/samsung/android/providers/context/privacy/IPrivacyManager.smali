.class public interface abstract Lcom/samsung/android/providers/context/privacy/IPrivacyManager;
.super Ljava/lang/Object;
.source "IPrivacyManager.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/privacy/IPrivacyManager$Stub;
    }
.end annotation


# virtual methods
.method public abstract isPrivacyConsentAcquired(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPrivacyConsent([I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unsetPrivacyConsent([I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
