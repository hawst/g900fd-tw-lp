.class public Lcom/samsung/android/providers/context/privacy/PrivacyManager;
.super Ljava/lang/Object;
.source "PrivacyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ContextFramework:PrivacyManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

.field private mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

.field private mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "conn"    # Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    .prologue
    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$1;-><init>(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)V

    iput-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 368
    iput-object p1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    .line 369
    iput-object p2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    .line 370
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/providers/context/privacy/PrivacyManager;Lcom/samsung/android/providers/context/privacy/IPrivacyManager;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/providers/context/privacy/PrivacyManager;)Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/providers/context/privacy/PrivacyManager;Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    return-void
.end method


# virtual methods
.method public bindService()Z
    .locals 4

    .prologue
    .line 381
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.providers.context.privacy.IPrivacyManager"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 382
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    return v1
.end method

.method public isPrivacyConsentAcquired(Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/SecurityException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 505
    const/4 v0, 0x0

    .line 507
    .local v0, "result":Z
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    if-eqz v1, :cond_1

    .line 508
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    invoke-virtual {p1}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->getValue()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/android/providers/context/privacy/IPrivacyManager;->isPrivacyConsentAcquired(I)Z

    move-result v0

    .line 513
    return v0

    .line 510
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The instance is not initialized or initialization is in progress"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setPrivacyConsent([Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;)V
    .locals 4
    .param p1, "items"    # [Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/SecurityException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 438
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    if-eqz v2, :cond_2

    .line 439
    :cond_0
    array-length v2, p1

    new-array v1, v2, [I

    .line 441
    .local v1, "itemArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 445
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    invoke-interface {v2, v1}, Lcom/samsung/android/providers/context/privacy/IPrivacyManager;->setPrivacyConsent([I)V

    .line 449
    return-void

    .line 442
    :cond_1
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->getValue()I

    move-result v2

    aput v2, v1, v0

    .line 441
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 447
    .end local v0    # "i":I
    .end local v1    # "itemArray":[I
    :cond_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The instance is not initialized or initialization is in progress"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 392
    return-void
.end method

.method public unsetPrivacyConsent([Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;)V
    .locals 4
    .param p1, "items"    # [Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/SecurityException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 471
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mPrivacyConnection:Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;

    if-eqz v2, :cond_2

    .line 472
    :cond_0
    array-length v2, p1

    new-array v1, v2, [I

    .line 474
    .local v1, "itemArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 478
    iget-object v2, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->mInterface:Lcom/samsung/android/providers/context/privacy/IPrivacyManager;

    invoke-interface {v2, v1}, Lcom/samsung/android/providers/context/privacy/IPrivacyManager;->unsetPrivacyConsent([I)V

    .line 482
    return-void

    .line 475
    :cond_1
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->getValue()I

    move-result v2

    aput v2, v1, v0

    .line 474
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 480
    .end local v0    # "i":I
    .end local v1    # "itemArray":[I
    :cond_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The instance is not initialized or initialization is in progress"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
