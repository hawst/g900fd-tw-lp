.class public final enum Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
.super Ljava/lang/Enum;
.source "PrivacyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/providers/context/privacy/PrivacyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrivacyItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

.field public static final enum WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 94
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "USE_APP"

    const/4 v2, 0x0

    .line 102
    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 104
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "MANAGE_APP"

    .line 112
    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 114
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "TAKE_PHOTO"

    .line 122
    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 124
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "RECORD_VIDEO"

    .line 132
    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 134
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "PLAY_MUSIC"

    .line 142
    invoke-direct {v0, v1, v7, v8}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 144
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "PLAY_VIDEO"

    .line 152
    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 154
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "EXCHANGE_CALL"

    const/4 v2, 0x6

    .line 162
    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 164
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "EXCHANGE_MESSAGE"

    const/4 v2, 0x7

    .line 172
    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 174
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "EXCHANGE_EMAIL"

    const/16 v2, 0x8

    .line 182
    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 184
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "BROWSE_WEB"

    const/16 v2, 0x9

    .line 192
    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 194
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "SEARCH_KEYWORD"

    const/16 v2, 0xa

    .line 202
    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 204
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "TRACK_ROAMING"

    const/16 v2, 0xb

    .line 212
    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 214
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "CHANGE_DEVICE_STATUS"

    const/16 v2, 0xc

    .line 222
    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 224
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "USE_WIFI"

    const/16 v2, 0xd

    .line 232
    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 234
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "MOVE_PLACE"

    const/16 v2, 0xe

    .line 243
    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 245
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "RECORD_AUDIO"

    const/16 v2, 0xf

    .line 253
    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 255
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "WRITE_DOCUMENT"

    const/16 v2, 0x10

    .line 263
    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 265
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "CAPTURE_CONTENT"

    const/16 v2, 0x11

    .line 273
    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 275
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "MOVE_LOCATION"

    const/16 v2, 0x12

    .line 284
    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 286
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "CHANGE_ACTIVITY"

    const/16 v2, 0x13

    .line 294
    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 296
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "MOVE_AREA"

    const/16 v2, 0x14

    .line 304
    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 306
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "STATUS_PLACE"

    const/16 v2, 0x15

    .line 315
    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 317
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "UPLOAD_APP_FEATURE_SURVEY"

    const/16 v2, 0x16

    .line 322
    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 324
    new-instance v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const-string v1, "UPLOAD_ALWAYS_SERVICE"

    const/16 v2, 0x17

    .line 332
    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    .line 92
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MANAGE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->TAKE_PHOTO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->RECORD_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->PLAY_MUSIC:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->PLAY_VIDEO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_CALL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_MESSAGE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->EXCHANGE_EMAIL:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->BROWSE_WEB:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->SEARCH_KEYWORD:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->TRACK_ROAMING:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CHANGE_DEVICE_STATUS:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->USE_WIFI:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->RECORD_AUDIO:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->WRITE_DOCUMENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CAPTURE_CONTENT:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_AREA:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->STATUS_PLACE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->UPLOAD_APP_FEATURE_SURVEY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->UPLOAD_ALWAYS_SERVICE:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->ENUM$VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I

    .prologue
    .line 336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 337
    iput p3, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->mValue:I

    .line 338
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->ENUM$VALUES:[Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->mValue:I

    return v0
.end method
