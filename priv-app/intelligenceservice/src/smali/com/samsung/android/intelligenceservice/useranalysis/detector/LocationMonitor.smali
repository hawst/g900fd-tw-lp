.class public Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;
.super Ljava/lang/Object;
.source "LocationMonitor.java"


# static fields
.field private static final SEC_LOCATION_SERVICE:Ljava/lang/String; = "sec_location"

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.LocationMonitor"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLocManager:Lcom/samsung/location/SLocationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    .line 27
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v1, "sec_location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/SLocationManager;

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    if-nez v0, :cond_0

    .line 29
    const-string v0, "UserAnalysis.LocationMonitor"

    const-string v1, "Unable to get the location service."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public addBluetoothLocation(Ljava/lang/String;)I
    .locals 7
    .param p1, "bluetoothAddress"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v1, Lcom/samsung/location/SLocationParameter;

    const/4 v3, 0x3

    invoke-direct {v1, v3, p1}, Lcom/samsung/location/SLocationParameter;-><init>(ILjava/lang/String;)V

    .line 86
    .local v1, "parameter":Lcom/samsung/location/SLocationParameter;
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v3, v1}, Lcom/samsung/location/SLocationManager;->addGeofence(Lcom/samsung/location/SLocationParameter;)I

    move-result v2

    .line 87
    .local v2, "ret":I
    if-gez v2, :cond_1

    .line 88
    const-string v3, "UserAnalysis.LocationMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to register a fence - code("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v4, "LM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ab "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 103
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    move v0, v2

    .line 95
    .local v0, "monitorId":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->start(I)I

    move-result v2

    .line 96
    if-gez v2, :cond_0

    .line 97
    const-string v3, "UserAnalysis.LocationMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to start monitoring - code("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v3, v0}, Lcom/samsung/location/SLocationManager;->removeGeofence(I)I

    .line 99
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v4, "LM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sb "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 100
    goto :goto_0
.end method

.method public addGeoLocation(DDI)I
    .locals 9
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "radius"    # I

    .prologue
    .line 35
    new-instance v0, Lcom/samsung/location/SLocationParameter;

    const/4 v1, 0x1

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/location/SLocationParameter;-><init>(IDDI)V

    .line 38
    .local v0, "parameter":Lcom/samsung/location/SLocationParameter;
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v1, v0}, Lcom/samsung/location/SLocationManager;->addGeofence(Lcom/samsung/location/SLocationParameter;)I

    move-result v8

    .line 39
    .local v8, "ret":I
    if-gez v8, :cond_1

    .line 40
    const-string v1, "UserAnalysis.LocationMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to register a fence - code("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v2, "LM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ag "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 55
    :cond_0
    :goto_0
    return v7

    .line 45
    :cond_1
    move v7, v8

    .line 47
    .local v7, "monitorId":I
    invoke-virtual {p0, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->start(I)I

    move-result v8

    .line 48
    if-gez v8, :cond_0

    .line 49
    const-string v1, "UserAnalysis.LocationMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start monitoring - code("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v1, v7}, Lcom/samsung/location/SLocationManager;->removeGeofence(I)I

    .line 51
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v2, "LM"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sg "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 52
    goto :goto_0
.end method

.method public addWifiLocation(Ljava/lang/String;)I
    .locals 7
    .param p1, "wifiBssId"    # Ljava/lang/String;

    .prologue
    .line 60
    new-instance v1, Lcom/samsung/location/SLocationParameter;

    const/4 v3, 0x2

    invoke-direct {v1, v3, p1}, Lcom/samsung/location/SLocationParameter;-><init>(ILjava/lang/String;)V

    .line 62
    .local v1, "parameter":Lcom/samsung/location/SLocationParameter;
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v3, v1}, Lcom/samsung/location/SLocationManager;->addGeofence(Lcom/samsung/location/SLocationParameter;)I

    move-result v2

    .line 63
    .local v2, "ret":I
    if-gez v2, :cond_1

    .line 64
    const-string v3, "UserAnalysis.LocationMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to register a fence - code("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v4, "LM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "aw "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    move v0, v2

    .line 71
    .local v0, "monitorId":I
    invoke-virtual {p0, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->start(I)I

    move-result v2

    .line 72
    if-gez v2, :cond_0

    .line 73
    const-string v3, "UserAnalysis.LocationMonitor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to start monitoring - code("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v3, v0}, Lcom/samsung/location/SLocationManager;->removeGeofence(I)I

    .line 75
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-string v4, "LM"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sw "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 76
    goto :goto_0
.end method

.method public removeLocation(I)V
    .locals 4
    .param p1, "monitorId"    # I

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->stop(I)I

    move-result v0

    .line 109
    .local v0, "ret":I
    if-gez v0, :cond_0

    .line 110
    const-string v1, "UserAnalysis.LocationMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to stop monitoring a fence - code("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v1, p1}, Lcom/samsung/location/SLocationManager;->removeGeofence(I)I

    move-result v0

    .line 114
    if-gez v0, :cond_1

    .line 115
    const-string v1, "UserAnalysis.LocationMonitor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to unregister a fence - code("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_1
    return-void
.end method

.method public start(I)I
    .locals 4
    .param p1, "monitorId"    # I

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$SLocationBroadcastReceiver;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.location.action.LOCATION_FENCE_DETECTED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v2, "monitor_id"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, p1, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 126
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/location/SLocationManager;->startGeofence(ILandroid/app/PendingIntent;)I

    move-result v2

    return v2
.end method

.method public stop(I)I
    .locals 4
    .param p1, "monitorId"    # I

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$SLocationBroadcastReceiver;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.location.action.LOCATION_FENCE_DETECTED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    const-string v2, "monitor_id"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 134
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mContext:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, p1, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 136
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v2, p1, v1}, Lcom/samsung/location/SLocationManager;->stopGeofence(ILandroid/app/PendingIntent;)I

    move-result v2

    return v2
.end method

.method syncGeofenceAfterBoot(Ljava/util/LinkedList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "monitorIdList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->mLocManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/SLocationManager;->syncGeofence(Ljava/util/List;)I

    .line 141
    return-void
.end method
