.class Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DumpLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DumpLogDbHelper"
.end annotation


# static fields
.field private static final AUTO_DELETE_TRIGGER_NAME:Ljava/lang/String; = "auto_delete"

.field private static final CREATE_DUMP_LOG_TRIGGER:Ljava/lang/String; = "CREATE TRIGGER IF NOT EXISTS auto_delete AFTER INSERT ON dump_log WHEN (select count() FROM dump_log) > 50 BEGIN DELETE FROM dump_log WHERE _id IN (SELECT _id FROM dump_log ORDER BY _id ASC LIMIT 10); END"

.field private static final CREATE_TABLE_DUMP_LOG:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS dump_log (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp DATETIME,message TEXT);"

.field private static final DATABASE_NAME:Ljava/lang/String; = "dump.db"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final DUMP_LOG_COLUMN_NAME_ID:Ljava/lang/String; = "_id"

.field public static final DUMP_LOG_COLUMN_NAME_MESSAGE:Ljava/lang/String; = "message"

.field public static final DUMP_LOG_COLUMN_NAME_TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final DUMP_LOG_TABLE_NAME:Ljava/lang/String; = "dump_log"

.field private static final MAX_DUMP_LOG_COUNT:I = 0x32

.field private static sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    const-string v0, "dump.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 143
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    const-class v1, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    invoke-direct {v0, p0}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    .line 138
    :cond_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 147
    const-string v0, "CREATE TABLE IF NOT EXISTS dump_log (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp DATETIME,message TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 148
    const-string v0, "CREATE TRIGGER IF NOT EXISTS auto_delete AFTER INSERT ON dump_log WHEN (select count() FROM dump_log) > 50 BEGIN DELETE FROM dump_log WHERE _id IN (SELECT _id FROM dump_log ORDER BY _id ASC LIMIT 10); END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 154
    return-void
.end method
