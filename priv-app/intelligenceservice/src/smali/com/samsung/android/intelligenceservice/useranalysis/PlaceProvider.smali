.class public Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;
.super Landroid/content/ContentProvider;
.source "PlaceProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;,
        Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    }
.end annotation


# static fields
.field static final BACKUP_DB_NAME:Ljava/lang/String; = "Place.db.back"

.field public static final CHANGED_BIT_SET:Ljava/lang/String; = "changed_bit_set"

.field private static final DEFAULT_PLACE_CNT:I = 0x5

.field static final INTENT_PERMISSION:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

.field static final METHOD_GET_USER_CONSENT:Ljava/lang/String; = "getUserConsent"

.field static final METHOD_SET_USER_CONSENT:Ljava/lang/String; = "setUserConsent"

.field public static final OLD_LOCATION_TYPE:Ljava/lang/String; = "old_location_type"

.field static final PLACE:I = 0x1

.field static final PLACE_ID:I = 0x2

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.PlaceProvider"

.field private static mContext:Landroid/content/Context;

.field private static mNeedToSurvey:Z

.field static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

.field private mPwd:[B

.field private mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    .line 66
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mNeedToSurvey:Z

    .line 70
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 71
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.internal.intelligence.useranalysis"

    const-string v2, "place"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.internal.intelligence.useranalysis"

    const-string v2, "place/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 58
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    .line 60
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    .line 1121
    return-void
.end method

.method private static calculateChangedBitSet(Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;)J
    .locals 6
    .param p0, "cur"    # Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    .param p1, "old"    # Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    .prologue
    .line 808
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 809
    .local v0, "bitset":Ljava/util/BitSet;
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MAX:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 810
    iget v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    iget v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    if-eq v1, v2, :cond_0

    .line 811
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 813
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 814
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 816
    :cond_1
    iget v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->category:I

    iget v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->category:I

    if-eq v1, v2, :cond_2

    .line 817
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->CATEGORY:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 819
    :cond_2
    iget v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    iget v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    if-eq v1, v2, :cond_3

    .line 820
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LOCATION_TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 822
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->address:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->address:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 823
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 825
    :cond_4
    iget-wide v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->latitude:D

    iget-wide v4, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->latitude:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_5

    .line 826
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LATITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 828
    :cond_5
    iget-wide v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->longitude:D

    iget-wide v4, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->longitude:D

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_6

    .line 829
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LONGITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 831
    :cond_6
    iget v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->radius:I

    iget v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->radius:I

    if-eq v1, v2, :cond_7

    .line 832
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->RADIUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 834
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiName:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 835
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 837
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiBssId:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiBssId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 838
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_BSSID:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 840
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btName:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 841
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 843
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btMacAddress:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btMacAddress:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 844
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_MAC_ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 846
    :cond_b
    iget v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->monitoringStatus:I

    iget v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->monitoringStatus:I

    if-eq v1, v2, :cond_c

    .line 847
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 849
    :cond_c
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->provider:Ljava/lang/String;

    iget-object v2, p1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->provider:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 850
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->PROVIDER:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 852
    :cond_d
    invoke-virtual {v0}, Ljava/util/BitSet;->toLongArray()[J

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v1, v2

    return-wide v2
.end method

.method private copyFromCursorToContentValues(Landroid/content/ContentValues;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1192
    invoke-interface {p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    .line 1193
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1194
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getType(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1193
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1196
    :pswitch_0
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_1

    .line 1199
    :pswitch_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    goto :goto_1

    .line 1202
    :pswitch_2
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1205
    :pswitch_3
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 1208
    :pswitch_4
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1214
    :cond_0
    return-void

    .line 1194
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private dumpPlaces(Ljava/io/PrintWriter;)V
    .locals 12
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 1040
    const-string v1, "[P]"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1041
    const/4 v9, 0x0

    .line 1042
    .local v9, "c":Landroid/database/Cursor;
    monitor-enter p0

    .line 1045
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v1, :cond_0

    .line 1046
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1050
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    const-string v1, "place"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 1056
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058
    if-nez v9, :cond_1

    .line 1059
    const-string v1, "null"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1072
    :goto_2
    return-void

    .line 1048
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto :goto_0

    .line 1052
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v10

    .line 1053
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 1054
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "delete - query() SQLiteException!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1056
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 1063
    :cond_1
    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1064
    new-instance v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    invoke-direct {v11, v9}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;-><init>(Landroid/database/Cursor;)V

    .line 1065
    .local v11, "info":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    const-string v1, "%d, %d, %d, %d, %s, %d, %s, %s, %s, %s, %d, %s%n"

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->category:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->address:Ljava/lang/String;

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->radius:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiName:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiBssId:Ljava/lang/String;

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btName:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btMacAddress:Ljava/lang/String;

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->monitoringStatus:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, v11, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->provider:Ljava/lang/String;

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto/16 :goto_3

    .line 1071
    .end local v11    # "info":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2
.end method

.method private getPrivacyGuardedFilter(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "selection"    # Ljava/lang/String;

    .prologue
    .line 989
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    .line 990
    .local v2, "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(Ljava/lang/String;I)Z

    move-result v0

    .line 992
    .local v0, "consented":Z
    if-nez v0, :cond_1

    .line 993
    const-string v1, ""

    .line 995
    .local v1, "guardedSelection":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 996
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 997
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1000
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1001
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1002
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1010
    :goto_0
    return-object v1

    .line 1007
    .end local v1    # "guardedSelection":Ljava/lang/String;
    :cond_1
    move-object v1, p1

    .restart local v1    # "guardedSelection":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getSafeSubstring(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "in"    # Ljava/lang/String;
    .param p1, "count"    # I

    .prologue
    .line 1075
    if-nez p0, :cond_0

    .line 1076
    const-string v0, "(null)"

    .line 1081
    :goto_0
    return-object v0

    .line 1078
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 1079
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    .line 1081
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private notifyUpdates(Ljava/util/List;Landroid/util/SparseArray;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 856
    .local p1, "placeInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    .local p2, "oldPlaceInfoList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v14

    if-gtz v14, :cond_1

    .line 943
    :cond_0
    :goto_0
    return-void

    .line 860
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 861
    .local v4, "List_user_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 862
    .local v3, "List_system_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 865
    .local v2, "List_app_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    .line 866
    .local v13, "placeInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    iget v14, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    packed-switch v14, :pswitch_data_0

    .line 883
    const-string v14, "UserAnalysis.PlaceProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Skipping unknown type: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 868
    :pswitch_0
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 869
    .local v5, "extras":Landroid/os/Bundle;
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 870
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "type : user_defined"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :goto_2
    const-wide/16 v6, 0x0

    .line 888
    .local v6, "changedBitSet":J
    iget v14, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    .line 889
    .local v12, "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    if-eqz v12, :cond_2

    .line 890
    invoke-static {v13, v12}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->calculateChangedBitSet(Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;)J

    move-result-wide v6

    .line 891
    const-string v14, "old_location_type"

    iget v15, v12, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    invoke-virtual {v5, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 896
    :goto_3
    const-string v14, "_id"

    iget v15, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    invoke-virtual {v5, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 897
    const-string v14, "name"

    iget-object v15, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    const-string v14, "category"

    iget v15, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->category:I

    invoke-virtual {v5, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 900
    const-string v14, "changed_bit_set"

    invoke-virtual {v5, v14, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 901
    const-string v14, "location_type"

    iget v15, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    invoke-virtual {v5, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 903
    const-string v14, "UserAnalysis.PlaceProvider"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "putExtra : operation=update, _id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v13, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", changedBitSet="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 873
    .end local v5    # "extras":Landroid/os/Bundle;
    .end local v6    # "changedBitSet":J
    .end local v12    # "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :pswitch_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 874
    .restart local v5    # "extras":Landroid/os/Bundle;
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "type : system_predicted"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 878
    .end local v5    # "extras":Landroid/os/Bundle;
    :pswitch_2
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 879
    .restart local v5    # "extras":Landroid/os/Bundle;
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 880
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "type : app_provided"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 893
    .restart local v6    # "changedBitSet":J
    .restart local v12    # "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :cond_2
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "oldPlaceInfo is null!"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 907
    .end local v5    # "extras":Landroid/os/Bundle;
    .end local v6    # "changedBitSet":J
    .end local v12    # "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    .end local v13    # "placeInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_4

    .line 908
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 909
    .local v11, "iUser":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.intelligence.useranalysis.action.USER_DEFINED_PLACE_CHANGED"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 910
    const/16 v14, 0x20

    invoke-virtual {v11, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 911
    const-string v14, "operation"

    const-string v15, "update"

    invoke-virtual {v11, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 913
    const-string v14, "data"

    invoke-virtual {v11, v14, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 914
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    invoke-virtual {v14, v11, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 915
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v14

    invoke-virtual {v14, v11}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 916
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "sendBroadcast : i_user"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    .end local v11    # "iUser":Landroid/content/Intent;
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_5

    .line 920
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 921
    .local v10, "iSystem":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.intelligence.useranalysis.action.SYSTEM_PREDICTED_PLACE_CHANGED"

    invoke-virtual {v10, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 922
    const/16 v14, 0x20

    invoke-virtual {v10, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 923
    const-string v14, "operation"

    const-string v15, "update"

    invoke-virtual {v10, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 925
    const-string v14, "data"

    invoke-virtual {v10, v14, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 926
    const-string v14, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v14, v15}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendPrivacyGuardedUnicast(Landroid/content/Intent;Ljava/lang/String;I)V

    .line 928
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v14

    invoke-virtual {v14, v10}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 929
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "sendBroadcast : i_system"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    .end local v10    # "iSystem":Landroid/content/Intent;
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_0

    .line 933
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 934
    .local v9, "iApp":Landroid/content/Intent;
    const-string v14, "com.samsung.android.internal.intelligence.useranalysis.action.APP_PROVIDED_PLACE_CHANGED"

    invoke-virtual {v9, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 935
    const/16 v14, 0x20

    invoke-virtual {v9, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 936
    const-string v14, "operation"

    const-string v15, "update"

    invoke-virtual {v9, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 938
    const-string v14, "data"

    invoke-virtual {v9, v14, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 939
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    invoke-virtual {v14, v9, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 940
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v14

    invoke-virtual {v14, v9}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 941
    const-string v14, "UserAnalysis.PlaceProvider"

    const-string v15, "sendBroadcast : i_app"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 866
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private notifyUpdatesToSurvey(Ljava/util/List;Landroid/util/SparseArray;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 946
    .local p1, "placeInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    .local p2, "oldPlaceInfoList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    .line 947
    .local v5, "placeInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    iget v6, v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    invoke-virtual {p2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    .line 948
    .local v4, "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    if-eqz v4, :cond_0

    .line 951
    iget v3, v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    .line 952
    .local v3, "oldLocationType":I
    iget v0, v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    .line 953
    .local v0, "curLocationType":I
    const-string v2, "Others"

    .line 954
    .local v2, "name":Ljava/lang/String;
    iget v6, v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    const/4 v7, 0x5

    if-ge v6, v7, :cond_1

    .line 955
    iget-object v2, v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->name:Ljava/lang/String;

    .line 958
    :cond_1
    and-int/lit8 v6, v3, 0x1

    if-nez v6, :cond_2

    and-int/lit8 v6, v0, 0x1

    if-lez v6, :cond_2

    .line 960
    const-string v6, "ADMP"

    const-string v7, "GPS"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    :cond_2
    and-int/lit8 v6, v3, 0x2

    if-nez v6, :cond_3

    and-int/lit8 v6, v0, 0x2

    if-lez v6, :cond_3

    .line 964
    const-string v6, "ADMP"

    const-string v7, "WIFI"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    :cond_3
    and-int/lit8 v6, v3, 0x4

    if-nez v6, :cond_4

    and-int/lit8 v6, v0, 0x4

    if-lez v6, :cond_4

    .line 968
    const-string v6, "ADMP"

    const-string v7, "BT"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    :cond_4
    and-int/lit8 v6, v3, 0x1

    if-lez v6, :cond_5

    and-int/lit8 v6, v0, 0x1

    if-nez v6, :cond_5

    .line 973
    const-string v6, "RMMP"

    const-string v7, "GPS"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :cond_5
    and-int/lit8 v6, v3, 0x2

    if-lez v6, :cond_6

    and-int/lit8 v6, v0, 0x2

    if-nez v6, :cond_6

    .line 977
    const-string v6, "RMMP"

    const-string v7, "WIFI"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    :cond_6
    and-int/lit8 v6, v3, 0x4

    if-lez v6, :cond_0

    and-int/lit8 v6, v0, 0x4

    if-nez v6, :cond_0

    .line 981
    const-string v6, "RMMP"

    const-string v7, "BT"

    invoke-direct {p0, v6, v2, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 984
    .end local v0    # "curLocationType":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "oldLocationType":I
    .end local v4    # "oldPlaceInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    .end local v5    # "placeInfo":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :cond_7
    return-void
.end method

.method private recoverDataFromBackupDb()Z
    .locals 18

    .prologue
    .line 1143
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "recover data from the old database"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-nez v3, :cond_1

    .line 1145
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "secure database is not supported"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    const/4 v3, 0x0

    .line 1188
    :cond_0
    :goto_0
    return v3

    .line 1149
    :cond_1
    sget-object v3, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->getOldPassword(Landroid/content/Context;)[B

    move-result-object v16

    .line 1150
    .local v16, "oldPwd":[B
    if-nez v16, :cond_2

    .line 1151
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "it failed to initalize database"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1152
    const/4 v3, 0x0

    goto :goto_0

    .line 1155
    :cond_2
    new-instance v13, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    sget-object v3, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    const-string v4, "Place.db.back"

    invoke-direct {v13, v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1156
    .local v13, "dbHelper":Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;
    const/4 v11, 0x0

    .line 1157
    .local v11, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1159
    .local v2, "oldDb":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    .line 1160
    .local v15, "newDb":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1161
    const-string v3, "place"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1163
    if-eqz v11, :cond_5

    .line 1164
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 1165
    .local v17, "values":Landroid/content/ContentValues;
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1166
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentValues;->clear()V

    .line 1167
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v11}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->copyFromCursorToContentValues(Landroid/content/ContentValues;Landroid/database/Cursor;)V

    .line 1168
    const-string v3, "place"

    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v4, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1172
    .end local v15    # "newDb":Landroid/database/sqlite/SQLiteDatabase;
    .end local v17    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v14

    .line 1173
    .local v14, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "It failed to recover"

    invoke-static {v3, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1174
    const/4 v3, 0x0

    .line 1176
    if-eqz v11, :cond_3

    .line 1177
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1179
    :cond_3
    if-eqz v2, :cond_4

    .line 1180
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1182
    :cond_4
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    const-string v5, "Place.db.back"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 1183
    .local v12, "dbFile":Ljava/io/File;
    if-eqz v12, :cond_0

    .line 1184
    invoke-static {v12}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    goto :goto_0

    .line 1176
    .end local v12    # "dbFile":Ljava/io/File;
    .end local v14    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v15    # "newDb":Landroid/database/sqlite/SQLiteDatabase;
    :cond_5
    if-eqz v11, :cond_6

    .line 1177
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1179
    :cond_6
    if-eqz v2, :cond_7

    .line 1180
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1182
    :cond_7
    sget-object v3, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    const-string v4, "Place.db.back"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 1183
    .restart local v12    # "dbFile":Ljava/io/File;
    if-eqz v12, :cond_8

    .line 1184
    invoke-static {v12}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    .line 1187
    :cond_8
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "database has been recovered"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1176
    .end local v12    # "dbFile":Ljava/io/File;
    .end local v15    # "newDb":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    if-eqz v11, :cond_9

    .line 1177
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1179
    :cond_9
    if-eqz v2, :cond_a

    .line 1180
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1182
    :cond_a
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    const-string v5, "Place.db.back"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 1183
    .restart local v12    # "dbFile":Ljava/io/File;
    if-eqz v12, :cond_b

    .line 1184
    invoke-static {v12}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    .line 1186
    :cond_b
    throw v3
.end method

.method private static safeEquals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "l"    # Ljava/lang/String;
    .param p1, "r"    # Ljava/lang/String;

    .prologue
    .line 805
    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendPrivacyGuardedUnicast(Landroid/content/Intent;Ljava/lang/String;I)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "permission"    # Ljava/lang/String;
    .param p3, "consentCode"    # I

    .prologue
    .line 1015
    new-instance v5, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    .line 1016
    .local v5, "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-virtual {v5, p3}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->getConsentedPackages(I)[Ljava/lang/String;

    move-result-object v1

    .line 1018
    .local v1, "consentedPackages":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1019
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 1020
    .local v4, "packageName":Ljava/lang/String;
    invoke-virtual {p1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1021
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1022
    const-string v6, "UserAnalysis.PlaceProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Privacy-guarded event is sent to ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1025
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "place"    # Ljava/lang/String;
    .param p3, "method"    # Ljava/lang/String;

    .prologue
    .line 102
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 103
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "com.android.settings"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v2, "feature"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v2, "extra"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 108
    .local v1, "it":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 110
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 112
    return-void
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const-string v5, "setUserConsent"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "requester":Ljava/lang/String;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 81
    .local v0, "consentCode":I
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    .line 82
    .local v1, "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->setUserConsent(Ljava/lang/String;I)I

    move-result v3

    .line 84
    .local v3, "ret":I
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 85
    .local v4, "rv":Landroid/os/Bundle;
    const-string v5, "result"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    .end local v0    # "consentCode":I
    .end local v1    # "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    .end local v2    # "requester":Ljava/lang/String;
    .end local v3    # "ret":I
    .end local v4    # "rv":Landroid/os/Bundle;
    :goto_0
    return-object v4

    .line 87
    :cond_0
    const-string v5, "getUserConsent"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    .line 90
    .restart local v2    # "requester":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    .line 91
    .restart local v1    # "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-virtual {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->getUserConsent(Ljava/lang/String;)I

    move-result v0

    .line 93
    .restart local v0    # "consentCode":I
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 94
    .restart local v4    # "rv":Landroid/os/Bundle;
    const-string v5, "result"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 98
    .end local v0    # "consentCode":I
    .end local v1    # "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    .end local v2    # "requester":Ljava/lang/String;
    .end local v4    # "rv":Landroid/os/Bundle;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 36
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 116
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete(Uri, String, String[]) - uri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", selection: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", args: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/16 v19, 0x0

    .line 120
    .local v19, "affectedRows":I
    const/16 v31, 0x0

    .line 121
    .local v31, "iUser":Landroid/content/Intent;
    const/16 v30, 0x0

    .line 122
    .local v30, "iSystem":Landroid/content/Intent;
    const/16 v29, 0x0

    .line 124
    .local v29, "iApp":Landroid/content/Intent;
    const/16 v20, 0x0

    .line 126
    .local v20, "c":Landroid/database/Cursor;
    sget-object v3, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 138
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Operation not supported for uri:"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 130
    :pswitch_0
    if-nez p2, :cond_1

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 142
    :goto_0
    :pswitch_1
    monitor-enter p0

    .line 145
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v3, :cond_2

    .line 146
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 150
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    const-string v3, "place"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v20

    .line 169
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    if-nez v20, :cond_3

    .line 172
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "cursor == null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v3, 0x0

    .line 348
    :goto_3
    return v3

    .line 133
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 135
    goto :goto_0

    .line 148
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto :goto_1

    .line 152
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v25

    .line 153
    .local v25, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    :try_start_3
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "delete - query()"

    move-object/from16 v0, v25

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->recoverDataFromBackupDb()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 159
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "place"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v20

    goto :goto_2

    .line 161
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v26

    .line 162
    .local v26, "e1":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "delete - query() SQLiteException again"

    move-object/from16 v0, v26

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 169
    .end local v25    # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    .end local v26    # "e1":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v3

    .line 165
    :catch_2
    move-exception v25

    .line 166
    .local v25, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    invoke-virtual/range {v25 .. v25}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 167
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "delete - query() SQLiteException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 176
    .end local v25    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_3
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v24

    .line 177
    .local v24, "cnt":I
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cursor : cnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    new-instance v13, Ljava/util/ArrayList;

    move/from16 v0, v24

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    .local v13, "List_id":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 181
    .local v17, "List_type":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v15, Ljava/util/ArrayList;

    move/from16 v0, v24

    invoke-direct {v15, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 182
    .local v15, "List_name":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, v24

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 183
    .local v12, "List_category":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v14, Ljava/util/ArrayList;

    move/from16 v0, v24

    invoke-direct {v14, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 185
    .local v14, "List_location_type":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 186
    :goto_4
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_4

    .line 187
    const-string v3, "_id"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    const-string v3, "type"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    const-string v3, "name"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    const-string v3, "category"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    const-string v3, "location_type"

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_4

    .line 197
    :cond_4
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 199
    monitor-enter p0

    .line 202
    :try_start_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v3, :cond_a

    .line 203
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 207
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_5
    const-string v3, "place"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v19

    .line 212
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_6
    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 213
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete(), dbTableName: place, affectedRows: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v3, 0x1

    move/from16 v0, v19

    if-ne v0, v3, :cond_b

    .line 218
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v6, v3

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v35

    .line 221
    .local v35, "notify_uri":Landroid/net/Uri;
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify_uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 231
    .end local v35    # "notify_uri":Landroid/net/Uri;
    :cond_5
    :goto_7
    sget-boolean v3, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mNeedToSurvey:Z

    if-eqz v3, :cond_c

    if-lez v19, :cond_c

    .line 232
    const/16 v28, 0x0

    .local v28, "i":I
    :goto_8
    move/from16 v0, v28

    move/from16 v1, v19

    if-ge v0, v1, :cond_c

    .line 233
    move/from16 v0, v28

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v33

    .line 234
    .local v33, "location_type":I
    const-string v34, "Others"

    .line 235
    .local v34, "name":Ljava/lang/String;
    move/from16 v0, v28

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_6

    .line 236
    move/from16 v0, v28

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    .end local v34    # "name":Ljava/lang/String;
    check-cast v34, Ljava/lang/String;

    .line 238
    .restart local v34    # "name":Ljava/lang/String;
    :cond_6
    and-int/lit8 v3, v33, 0x1

    if-lez v3, :cond_7

    .line 239
    const-string v3, "RMMP"

    const-string v4, "GPS"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_7
    and-int/lit8 v3, v33, 0x2

    if-lez v3, :cond_8

    .line 242
    const-string v3, "RMMP"

    const-string v4, "WIFI"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_8
    and-int/lit8 v3, v33, 0x4

    if-lez v3, :cond_9

    .line 245
    const-string v3, "RMMP"

    const-string v4, "BT"

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_9
    add-int/lit8 v28, v28, 0x1

    goto :goto_8

    .line 205
    .end local v28    # "i":I
    .end local v33    # "location_type":I
    .end local v34    # "name":Ljava/lang/String;
    :cond_a
    :try_start_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v2

    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto/16 :goto_5

    .line 208
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_3
    move-exception v25

    .line 209
    .restart local v25    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_a
    invoke-virtual/range {v25 .. v25}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 210
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "delete - delete() SQLiteException!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 212
    .end local v25    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v3

    .line 224
    :cond_b
    const/4 v3, 0x1

    move/from16 v0, v19

    if-le v0, v3, :cond_5

    .line 225
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify_uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_7

    .line 250
    :cond_c
    const/16 v23, 0x0

    .line 251
    .local v23, "check_user_type":Z
    const/16 v22, 0x0

    .line 252
    .local v22, "check_system_type":Z
    const/16 v21, 0x0

    .line 254
    .local v21, "check_app_type":Z
    if-lez v19, :cond_16

    .line 256
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v18, "List_user_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v16, "List_system_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v11, "List_app_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/16 v32, 0x0

    .local v32, "j":I
    :goto_9
    move/from16 v0, v32

    move/from16 v1, v19

    if-ge v0, v1, :cond_13

    .line 262
    move-object/from16 v0, v17

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 263
    if-nez v23, :cond_d

    .line 264
    new-instance v31, Landroid/content/Intent;

    .end local v31    # "iUser":Landroid/content/Intent;
    invoke-direct/range {v31 .. v31}, Landroid/content/Intent;-><init>()V

    .line 265
    .restart local v31    # "iUser":Landroid/content/Intent;
    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.action.USER_DEFINED_PLACE_CHANGED"

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const/16 v3, 0x20

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 267
    const-string v3, "operation"

    const-string v4, "delete"

    move-object/from16 v0, v31

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    :cond_d
    new-instance v27, Landroid/os/Bundle;

    invoke-direct/range {v27 .. v27}, Landroid/os/Bundle;-><init>()V

    .line 272
    .local v27, "extras":Landroid/os/Bundle;
    const-string v4, "_id"

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 273
    const-string v4, "name"

    move/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v4, "category"

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    const-string v3, "UserAnalysis.PlaceProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type : user_defined, putExtra : operation = delete, _id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const/16 v23, 0x1

    .line 260
    .end local v27    # "extras":Landroid/os/Bundle;
    :cond_e
    :goto_a
    add-int/lit8 v32, v32, 0x1

    goto/16 :goto_9

    .line 281
    :cond_f
    move-object/from16 v0, v17

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 282
    if-nez v22, :cond_10

    .line 283
    new-instance v30, Landroid/content/Intent;

    .end local v30    # "iSystem":Landroid/content/Intent;
    invoke-direct/range {v30 .. v30}, Landroid/content/Intent;-><init>()V

    .line 284
    .restart local v30    # "iSystem":Landroid/content/Intent;
    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.action.SYSTEM_PREDICTED_PLACE_CHANGED"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const/16 v3, 0x20

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 286
    const-string v3, "operation"

    const-string v4, "delete"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    :cond_10
    new-instance v27, Landroid/os/Bundle;

    invoke-direct/range {v27 .. v27}, Landroid/os/Bundle;-><init>()V

    .line 291
    .restart local v27    # "extras":Landroid/os/Bundle;
    const-string v4, "_id"

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 292
    const-string v4, "name"

    move/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v4, "category"

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 294
    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    const-string v4, "UserAnalysis.PlaceProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type : system_predicted, putExtra : operation = delete, _id = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", name = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", category = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const/16 v22, 0x1

    .line 301
    goto/16 :goto_a

    .end local v27    # "extras":Landroid/os/Bundle;
    :cond_11
    move-object/from16 v0, v17

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 302
    if-nez v21, :cond_12

    .line 303
    new-instance v29, Landroid/content/Intent;

    .end local v29    # "iApp":Landroid/content/Intent;
    invoke-direct/range {v29 .. v29}, Landroid/content/Intent;-><init>()V

    .line 304
    .restart local v29    # "iApp":Landroid/content/Intent;
    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.action.APP_PROVIDED_PLACE_CHANGED"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const/16 v3, 0x20

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 306
    const-string v3, "operation"

    const-string v4, "delete"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    :cond_12
    new-instance v27, Landroid/os/Bundle;

    invoke-direct/range {v27 .. v27}, Landroid/os/Bundle;-><init>()V

    .line 310
    .restart local v27    # "extras":Landroid/os/Bundle;
    const-string v4, "_id"

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 311
    const-string v4, "name"

    move/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v4, "category"

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 313
    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    const-string v4, "UserAnalysis.PlaceProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "type : app_provided, putExtra : operation = delete, _id = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", name = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v32

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", category = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/16 v21, 0x1

    goto/16 :goto_a

    .line 324
    .end local v27    # "extras":Landroid/os/Bundle;
    :cond_13
    if-eqz v23, :cond_14

    .line 325
    const-string v3, "data"

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 326
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    move-object/from16 v0, v31

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 327
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 328
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "sendBroadcast : i_user"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_14
    if-eqz v22, :cond_15

    .line 332
    const-string v3, "data"

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 333
    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendPrivacyGuardedUnicast(Landroid/content/Intent;Ljava/lang/String;I)V

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 336
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "sendBroadcast : i_system"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_15
    if-eqz v21, :cond_16

    .line 340
    const-string v3, "data"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    move-object/from16 v0, v29

    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 343
    const-string v3, "UserAnalysis.PlaceProvider"

    const-string v4, "sendBroadcast : i_app"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v11    # "List_app_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v16    # "List_system_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v18    # "List_user_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v32    # "j":I
    :cond_16
    move/from16 v3, v19

    .line 348
    goto/16 :goto_3

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 1029
    if-eqz p2, :cond_0

    .line 1030
    invoke-direct {p0, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->dumpPlaces(Ljava/io/PrintWriter;)V

    .line 1031
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 1032
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1033
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-direct {v1, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->dump(Ljava/io/PrintWriter;)V

    .line 1034
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 1035
    invoke-static {v0, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->dump(Landroid/content/Context;Ljava/io/PrintWriter;)V

    .line 1037
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    return-void
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 354
    const-string v0, "UserAnalysis.PlaceProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getType(Uri) - uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 363
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Operation not supported for uri:"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/place"

    .line 360
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.item/place"

    goto :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 26
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 371
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "insert(Uri, ContentValues) - uri: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const-wide/16 v22, -0x1

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    .line 374
    .local v19, "rowId":Ljava/lang/Long;
    const/16 v17, 0x0

    .line 379
    .local v17, "packageName":Ljava/lang/String;
    const-string v22, "type"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v21

    .line 380
    .local v21, "type":Ljava/lang/Integer;
    const-string v22, "name"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 381
    .local v14, "name":Ljava/lang/String;
    const-string v22, "category"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 382
    .local v6, "category":Ljava/lang/Integer;
    const-string v22, "provider"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 383
    .local v18, "provider":Ljava/lang/String;
    const-string v22, "location_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    .line 384
    .local v12, "location_type":Ljava/lang/Integer;
    const-string v22, "monitoring_status"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v13

    .line 387
    .local v13, "monitoring_status":Ljava/lang/Integer;
    if-eqz v21, :cond_0

    if-eqz v14, :cond_0

    if-eqz v6, :cond_0

    if-eqz v12, :cond_0

    if-nez v13, :cond_1

    .line 389
    :cond_0
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "Please insert type, name, category, location_type, monitoring_status columns!!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const/4 v5, 0x0

    .line 516
    :goto_0
    return-object v5

    .line 395
    :cond_1
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v17

    .line 396
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "packageName : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    :goto_1
    if-eqz v17, :cond_3

    const-string v22, "com.samsung.android.intelligenceservice"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 403
    const-string v22, "com.sec.android.app.launcher"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_3

    .line 404
    if-eqz v18, :cond_2

    .line 405
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "Please don\'t input provider column !!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const/4 v5, 0x0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v8

    .line 398
    .local v8, "e":Ljava/lang/SecurityException;
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "getCallingPackage SecurityException!!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-virtual {v8}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 408
    .end local v8    # "e":Ljava/lang/SecurityException;
    :cond_2
    const-string v22, "provider"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    move-object/from16 v18, v17

    .line 414
    :cond_3
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "provider : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 417
    .local v20, "timestamp_utc":Ljava/lang/Long;
    const-string v22, "timestamp_utc"

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 419
    monitor-enter p0

    .line 422
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v22, v0

    if-eqz v22, :cond_a

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 427
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_2
    const-string v22, "place"

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, p2

    invoke-virtual {v7, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v19

    .line 444
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_4
    :goto_3
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 445
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "insert(), dbTableName: place, rowId: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " inserted"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    sget-boolean v22, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mNeedToSurvey:Z

    if-eqz v22, :cond_8

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const-wide/16 v24, -0x1

    cmp-long v22, v22, v24

    if-lez v22, :cond_8

    .line 450
    const-string v15, "Others"

    .line 451
    .local v15, "nameForSurvey":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const-wide/16 v24, 0x5

    cmp-long v22, v22, v24

    if-gez v22, :cond_5

    .line 452
    move-object v15, v14

    .line 454
    :cond_5
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v22

    and-int/lit8 v22, v22, 0x1

    if-lez v22, :cond_6

    .line 455
    const-string v22, "ADMP"

    const-string v23, "GPS"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v15, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_6
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v22

    and-int/lit8 v22, v22, 0x2

    if-lez v22, :cond_7

    .line 458
    const-string v22, "ADMP"

    const-string v23, "WIFI"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v15, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_7
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v22

    and-int/lit8 v22, v22, 0x4

    if-lez v22, :cond_8

    .line 461
    const-string v22, "ADMP"

    const-string v23, "BT"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v15, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendSurveyBroadcast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    .end local v15    # "nameForSurvey":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const-wide/16 v24, -0x1

    cmp-long v22, v22, v24

    if-lez v22, :cond_e

    .line 466
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    .line 468
    .local v16, "notify_uri":Landroid/net/Uri;
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "notify_uri : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 473
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 475
    .local v5, "broadcast_uri":Landroid/net/Uri;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 477
    .local v4, "List_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 479
    .local v11, "i":Landroid/content/Intent;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_b

    .line 480
    const-string v22, "com.samsung.android.internal.intelligence.useranalysis.action.USER_DEFINED_PLACE_CHANGED"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "type : user_defined"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_9
    :goto_4
    const/16 v22, 0x20

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 491
    const-string v22, "operation"

    const-string v23, "insert"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 495
    .local v10, "extras":Landroid/os/Bundle;
    const-string v22, "_id"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->intValue()I

    move-result v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 496
    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const-string v22, "category"

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 498
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "putExtra : operation = insert, _id = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    const-string v22, "data"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 503
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v22

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_d

    .line 504
    const-string v22, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v11, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sendPrivacyGuardedUnicast(Landroid/content/Intent;Ljava/lang/String;I)V

    .line 509
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 511
    const-string v22, "UserAnalysis.PlaceProvider"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "broadcast_uri : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 425
    .end local v4    # "List_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v5    # "broadcast_uri":Landroid/net/Uri;
    .end local v10    # "extras":Landroid/os/Bundle;
    .end local v11    # "i":Landroid/content/Intent;
    .end local v16    # "notify_uri":Landroid/net/Uri;
    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto/16 :goto_2

    .line 428
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v8

    .line 429
    .local v8, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    :try_start_4
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "insert - insert()"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 432
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->recoverDataFromBackupDb()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v22

    if-eqz v22, :cond_4

    .line 434
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 435
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v22, "place"

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, p2

    invoke-virtual {v7, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v19

    goto/16 :goto_3

    .line 436
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_2
    move-exception v9

    .line 437
    .local v9, "e1":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "insert - insert() SQLiteException again"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 444
    .end local v8    # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    .end local v9    # "e1":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v22

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v22

    .line 440
    :catch_3
    move-exception v8

    .line 441
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_7
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 442
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "insert - insert() SQLiteException!!!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 482
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v4    # "List_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .restart local v5    # "broadcast_uri":Landroid/net/Uri;
    .restart local v11    # "i":Landroid/content/Intent;
    .restart local v16    # "notify_uri":Landroid/net/Uri;
    :cond_b
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v22

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    .line 483
    const-string v22, "com.samsung.android.internal.intelligence.useranalysis.action.SYSTEM_PREDICTED_PLACE_CHANGED"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "type : system_predicted"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 485
    :cond_c
    const/16 v22, 0x2

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 486
    const-string v22, "com.samsung.android.internal.intelligence.useranalysis.action.APP_PROVIDED_PLACE_CHANGED"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string v22, "UserAnalysis.PlaceProvider"

    const-string v23, "type : app_provided"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 507
    .restart local v10    # "extras":Landroid/os/Bundle;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v22

    const-string v23, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v11, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 516
    .end local v4    # "List_bundle":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v5    # "broadcast_uri":Landroid/net/Uri;
    .end local v10    # "extras":Landroid/os/Bundle;
    .end local v11    # "i":Landroid/content/Intent;
    .end local v16    # "notify_uri":Landroid/net/Uri;
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 522
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "PlaceProvider onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    .line 525
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v1

    const-string v2, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v1, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mNeedToSurvey:Z

    .line 528
    monitor-enter p0

    .line 530
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->isSupportedFeature(S)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 531
    invoke-static {}, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->getPassword()[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    .line 532
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-nez v1, :cond_0

    .line 533
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    .line 534
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "Create SecureDbHelper"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    const-string v2, "Place.db.back"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 537
    .local v0, "dbFile":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "backup file exists"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->recoverDataFromBackupDb()Z

    .line 548
    .end local v0    # "dbFile":Ljava/io/File;
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 551
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .line 552
    const/4 v1, 0x1

    return v1

    .line 543
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    if-nez v1, :cond_0

    .line 544
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    .line 545
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "Create DbHelper"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 548
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 564
    const/4 v9, 0x0

    .line 566
    .local v9, "c":Landroid/database/Cursor;
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 578
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Operation not supported for uri:"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 570
    :pswitch_0
    if-nez p3, :cond_2

    .line 571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 582
    :goto_0
    :pswitch_1
    invoke-direct {p0, p3}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getPrivacyGuardedFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 584
    .local v3, "guardedSelection":Ljava/lang/String;
    monitor-enter p0

    .line 587
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v1, :cond_3

    .line 588
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 592
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    const-string v1, "place"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p2

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 612
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 614
    if-eqz v9, :cond_1

    .line 620
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v9, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 623
    :cond_1
    return-object v9

    .line 573
    .end local v3    # "guardedSelection":Ljava/lang/String;
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 575
    goto :goto_0

    .line 590
    .restart local v3    # "guardedSelection":Ljava/lang/String;
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto :goto_1

    .line 594
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_0
    move-exception v10

    .line 595
    .local v10, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    :try_start_3
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "query - query()"

    invoke-static {v1, v2, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 598
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->recoverDataFromBackupDb()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 600
    :try_start_4
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "retry one more time"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 602
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "place"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p2

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v9

    goto :goto_2

    .line 604
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v11

    .line 605
    .local v11, "e1":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "query - query() SQLiteException again"

    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 612
    .end local v10    # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    .end local v11    # "e1":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v1

    .line 608
    :catch_2
    move-exception v10

    .line 609
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 610
    const-string v1, "UserAnalysis.PlaceProvider"

    const-string v2, "query - query() SQLiteException!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 566
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 26
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 629
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 630
    .local v20, "l":Ljava/lang/StringBuilder;
    const-string v5, "update(Uri, ContentValues, String, String[]) - uri: "

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", selection: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", args: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {p4 .. p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    const-string v5, "UserAnalysis.PlaceProvider"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    const/4 v13, 0x0

    .line 637
    .local v13, "affectedRows":I
    const/4 v14, 0x0

    .line 638
    .local v14, "c":Landroid/database/Cursor;
    const/16 v23, 0x0

    .line 641
    .local v23, "packageName":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v23

    .line 642
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "packageName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :goto_0
    const-string v5, "provider"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 649
    .local v25, "provider":Ljava/lang/String;
    if-eqz v23, :cond_1

    const-string v5, "com.samsung.android.intelligenceservice"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 650
    const-string v5, "com.sec.android.app.launcher"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 651
    if-eqz v25, :cond_0

    .line 652
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "Please don\'t input provider column !!!"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    const/4 v5, 0x0

    .line 801
    :goto_1
    return v5

    .line 643
    .end local v25    # "provider":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 644
    .local v16, "e":Ljava/lang/SecurityException;
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "getCallingPackage SecurityException!!!"

    move-object/from16 v0, v16

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 655
    .end local v16    # "e":Ljava/lang/SecurityException;
    .restart local v25    # "provider":Ljava/lang/String;
    :cond_0
    const-string v5, "provider"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    move-object/from16 v25, v23

    .line 661
    :cond_1
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "provider : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    sget-object v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->sUriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 675
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string v6, "Operation not supported for uri:"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 667
    :pswitch_0
    if-nez p3, :cond_3

    .line 668
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 679
    :goto_2
    :pswitch_1
    monitor-enter p0

    .line 682
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v5, :cond_4

    .line 683
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 687
    .local v4, "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_3
    const-string v5, "place"

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    .line 706
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    :goto_4
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 708
    if-nez v14, :cond_5

    .line 709
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "cursor == null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 670
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 672
    goto :goto_2

    .line 685
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteDatabaseCorruptException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto :goto_3

    .line 689
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_1
    move-exception v16

    .line 690
    .local v16, "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    :try_start_4
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "update - query()"

    move-object/from16 v0, v16

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 693
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->recoverDataFromBackupDb()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    .line 695
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 696
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "place"

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v14

    goto :goto_4

    .line 698
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_2
    move-exception v17

    .line 699
    .local v17, "e1":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "update - query() SQLiteException again"

    move-object/from16 v0, v17

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 706
    .end local v16    # "e":Landroid/database/sqlite/SQLiteDatabaseCorruptException;
    .end local v17    # "e1":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v5

    .line 702
    :catch_3
    move-exception v16

    .line 703
    .local v16, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 704
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "update - query() SQLiteException!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 713
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_5
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 714
    .local v15, "cnt":I
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cursor : cnt = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    new-instance v24, Ljava/util/ArrayList;

    move-object/from16 v0, v24

    invoke-direct {v0, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 717
    .local v24, "placeInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    new-instance v22, Landroid/util/SparseArray;

    move-object/from16 v0, v22

    invoke-direct {v0, v15}, Landroid/util/SparseArray;-><init>(I)V

    .line 719
    .local v22, "oldPlaceInfoList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;>;"
    :goto_5
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 720
    new-instance v19, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    move-object/from16 v0, v19

    invoke-direct {v0, v14}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;-><init>(Landroid/database/Cursor;)V

    .line 721
    .local v19, "info":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    move-object/from16 v0, v19

    iget v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_5

    .line 723
    .end local v19    # "info":Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
    :cond_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 725
    monitor-enter p0

    .line 728
    :try_start_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v5, :cond_7

    .line 729
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 733
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_6
    const-string v5, "place"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result v13

    .line 739
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_7
    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 740
    new-instance v20, Ljava/lang/StringBuilder;

    .end local v20    # "l":Ljava/lang/StringBuilder;
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 741
    .restart local v20    # "l":Ljava/lang/StringBuilder;
    const-string v5, "update(), dbTableName : "

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "place"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", affectedRows = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " updated"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    const-string v5, "UserAnalysis.PlaceProvider"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    monitor-enter p0

    .line 748
    :try_start_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-eqz v5, :cond_8

    .line 749
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mPwd:[B

    invoke-virtual {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->getReadableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 754
    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_8
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_9
    invoke-virtual/range {v22 .. v22}, Landroid/util/SparseArray;->size()I

    move-result v5

    move/from16 v0, v18

    if-ge v0, v5, :cond_b

    .line 756
    const-string v5, "place"

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 760
    if-nez v14, :cond_9

    .line 761
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "cursor == null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 762
    const/4 v5, 0x0

    :try_start_b
    monitor-exit p0

    goto/16 :goto_1

    .line 778
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "i":I
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v5

    .line 731
    :cond_7
    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result-object v4

    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto/16 :goto_6

    .line 735
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catch_4
    move-exception v16

    .line 736
    .restart local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_d
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 737
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "update - update() SQLiteException!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 739
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_2
    move-exception v5

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    throw v5

    .line 751
    :cond_8
    :try_start_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .restart local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    goto :goto_8

    .line 765
    .restart local v18    # "i":I
    :cond_9
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 766
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cursor : cnt = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 769
    new-instance v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    invoke-direct {v5, v14}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;-><init>(Landroid/database/Cursor;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 771
    :cond_a
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 754
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_9

    .line 774
    .end local v4    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "i":I
    :catch_5
    move-exception v16

    .line 775
    .restart local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_f
    invoke-virtual/range {v16 .. v16}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 776
    const-string v5, "UserAnalysis.PlaceProvider"

    const-string v6, "update - query() SQLiteException!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    .end local v16    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_b
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 781
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_e

    .line 782
    sget-object v6, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;

    iget v5, v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    int-to-long v8, v5

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v21

    .line 785
    .local v21, "notify_uri":Landroid/net/Uri;
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notify_uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 795
    .end local v21    # "notify_uri":Landroid/net/Uri;
    :cond_c
    :goto_a
    sget-boolean v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->mNeedToSurvey:Z

    if-eqz v5, :cond_d

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_d

    .line 796
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->notifyUpdatesToSurvey(Ljava/util/List;Landroid/util/SparseArray;)V

    .line 800
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->notifyUpdates(Ljava/util/List;Landroid/util/SparseArray;)V

    move v5, v13

    .line 801
    goto/16 :goto_1

    .line 788
    :cond_e
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_c

    .line 789
    const-string v5, "UserAnalysis.PlaceProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notify_uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_a

    .line 663
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
