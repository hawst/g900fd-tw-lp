.class Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;
.super Ljava/lang/Object;
.source "PrivacyManager.java"

# interfaces
.implements Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContextPrivacyServiceConnection"
.end annotation


# instance fields
.field public mConsentPrediction:Z

.field public mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mConsentPrediction:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$1;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected()V
    .locals 5

    .prologue
    .line 100
    const-string v2, "UserAnalysis.PrivacyManager"

    const-string v3, "Context privacy service is connected."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v2, 0x3

    new-array v1, v2, [Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    const/4 v2, 0x0

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->MOVE_LOCATION:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->CHANGE_ACTIVITY:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;->USE_APP:Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;

    aput-object v3, v1, v2

    .line 105
    .local v1, "items":[Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    if-eqz v2, :cond_0

    .line 108
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mConsentPrediction:Z

    if-eqz v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->setPrivacyConsent([Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;)V

    .line 114
    :goto_0
    const-string v2, "UserAnalysis.PrivacyManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Context privacy has been configured(consent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mConsentPrediction:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-virtual {v2}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->unbindService()V

    .line 123
    :cond_0
    return-void

    .line 111
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->unsetPrivacyConsent([Lcom/samsung/android/providers/context/privacy/PrivacyManager$PrivacyItem;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "UserAnalysis.PrivacyManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to set context privacy(code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onServiceDisconnected()V
    .locals 2

    .prologue
    .line 127
    const-string v0, "UserAnalysis.PrivacyManager"

    const-string v1, "Context privacy service is disconnected."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    .line 130
    return-void
.end method
