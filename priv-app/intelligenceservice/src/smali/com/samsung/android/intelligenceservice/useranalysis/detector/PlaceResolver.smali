.class public Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;
.super Ljava/lang/Object;
.source "PlaceResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;,
        Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    }
.end annotation


# static fields
.field private static final DATABASE_CREATE:Ljava/lang/String; = "CREATE TABLE id_map (monitor_id INTEGER PRIMARY KEY NOT NULL, place_id INTEGER NOT NULL, location_method INTEGER NOT NULL, sticky INTEGER DEFAULT 0);"

.field private static final DATABASE_NAME:Ljava/lang/String; = "PlaceDetection.db"

.field private static final DATABASE_SELECT_BY_MONITOR:Ljava/lang/String; = "monitor_id = ?"

.field private static final DATABASE_SELECT_BY_PLACE:Ljava/lang/String; = "place_id = ? AND location_method = ?"

.field private static final DATABASE_VERSION:I = 0x2

.field private static final KEY_LOCATION_METHOD:Ljava/lang/String; = "location_method"

.field private static final KEY_MONITOR_ID:Ljava/lang/String; = "monitor_id"

.field private static final KEY_PLACE_ID:Ljava/lang/String; = "place_id"

.field private static final KEY_STICKY:Ljava/lang/String; = "sticky"

.field private static final TABLE_NAME:Ljava/lang/String; = "id_map"

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.PlaceResolver"


# instance fields
.field private mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    .line 81
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    .line 82
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 17
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 453
    const-string v2, "[resolver]"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 456
    monitor-enter p0

    .line 458
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 459
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_0

    .line 460
    const-string v2, "null db"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 461
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-void

    .line 463
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v2, 0x1

    :try_start_2
    const-string v3, "id_map"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    .line 469
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 471
    if-eqz v11, :cond_2

    .line 472
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 473
    const-string v2, "monitor_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 474
    .local v14, "monitorId":I
    const-string v2, "place_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 475
    .local v15, "placeId":I
    const-string v2, "location_method"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 476
    .local v13, "locationMethod":I
    const-string v2, "sticky"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 477
    .local v16, "sticky":I
    const-string v2, "%d, %d, %d, %d%n"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto :goto_1

    .line 465
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v13    # "locationMethod":I
    .end local v14    # "monitorId":I
    .end local v15    # "placeId":I
    .end local v16    # "sticky":I
    :catch_0
    move-exception v12

    .line 466
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    const-string v2, "exception"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 467
    monitor-exit p0

    goto :goto_0

    .line 469
    .end local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 479
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 481
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0
.end method

.method public getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .locals 19

    .prologue
    .line 290
    const/16 v16, 0x0

    .line 292
    .local v16, "monirotInfos":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    monitor-enter p0

    .line 294
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 295
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_0

    .line 296
    const-string v3, "UserAnalysis.PlaceResolver"

    const-string v4, "db == null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    const/4 v3, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-object v3

    .line 299
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v3, 0x1

    :try_start_2
    const-string v4, "id_map"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "monitor_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "place_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "location_method"

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v12

    .line 307
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 309
    if-eqz v12, :cond_2

    .line 310
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v0, v3, [Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-object/from16 v16, v0

    .line 312
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 314
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v14, v3, :cond_1

    .line 315
    const-string v3, "monitor_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 316
    .local v17, "monitorId":I
    const-string v3, "place_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 317
    .local v18, "placeId":I
    const-string v3, "location_method"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 319
    .local v15, "locationMethod":I
    new-instance v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v3, v0, v1, v15}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;-><init>(III)V

    aput-object v3, v16, v14

    .line 320
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 314
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 302
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v12    # "cursor":Landroid/database/Cursor;
    .end local v14    # "i":I
    .end local v15    # "locationMethod":I
    .end local v17    # "monitorId":I
    .end local v18    # "placeId":I
    :catch_0
    move-exception v13

    .line 303
    .local v13, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 304
    const-string v3, "UserAnalysis.PlaceResolver"

    const-string v4, "SQLiteException"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    const/4 v3, 0x0

    monitor-exit p0

    goto :goto_0

    .line 307
    .end local v13    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 323
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "i":I
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 325
    .end local v14    # "i":I
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    move-object/from16 v3, v16

    .line 327
    goto :goto_0
.end method

.method public getMonitorId(II)I
    .locals 13
    .param p1, "placeId"    # I
    .param p2, "locationMethod"    # I

    .prologue
    const/4 v12, -0x1

    .line 420
    monitor-enter p0

    .line 422
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 423
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 424
    const-string v1, "UserAnalysis.PlaceResolver"

    const-string v2, "db == null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return v12

    .line 427
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v1, 0x1

    :try_start_2
    const-string v2, "id_map"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "monitor_id"

    aput-object v5, v3, v4

    const-string v4, "place_id = ? AND location_method = ?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 436
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 438
    const/4 v12, -0x1

    .line 440
    .local v12, "monitorId":I
    if-eqz v10, :cond_2

    .line 441
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 442
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 443
    const-string v1, "monitor_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 445
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 447
    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 432
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v12    # "monitorId":I
    :catch_0
    move-exception v11

    .line 433
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    const-string v1, "UserAnalysis.PlaceResolver"

    const-string v2, "It failed to get monitor id"

    invoke-static {v1, v2, v11}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 434
    monitor-exit p0

    goto :goto_0

    .line 436
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method public getMonitorInfo(I)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .locals 16
    .param p1, "monitorId"    # I

    .prologue
    .line 249
    monitor-enter p0

    .line 251
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 252
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_0

    .line 253
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "db == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    const/4 v14, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-object v14

    .line 256
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v2, 0x1

    :try_start_2
    const-string v3, "id_map"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "place_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "location_method"

    aput-object v6, v4, v5

    const-string v5, "monitor_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    .line 266
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 268
    const/4 v14, 0x0

    .line 269
    .local v14, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-eqz v11, :cond_2

    .line 270
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 271
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 273
    const-string v2, "place_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 274
    .local v15, "placeId":I
    const-string v2, "location_method"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 276
    .local v13, "locationMethod":I
    new-instance v14, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    .end local v14    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    move/from16 v0, p1

    invoke-direct {v14, v0, v15, v13}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;-><init>(III)V

    .line 278
    .end local v13    # "locationMethod":I
    .end local v15    # "placeId":I
    .restart local v14    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 280
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 261
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v14    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :catch_0
    move-exception v12

    .line 262
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 263
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "SQLiteException"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const/4 v14, 0x0

    monitor-exit p0

    goto :goto_0

    .line 266
    .end local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2
.end method

.method public register(III)V
    .locals 8
    .param p1, "monitorId"    # I
    .param p2, "placeId"    # I
    .param p3, "locationMethod"    # I

    .prologue
    .line 86
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 87
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "monitor_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    const-string v5, "place_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    const-string v5, "location_method"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 90
    const-string v5, "sticky"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 93
    monitor-enter p0

    .line 95
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 96
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 97
    const-string v5, "UserAnalysis.PlaceResolver"

    const-string v6, "db == null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-void

    .line 100
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    const-string v5, "id_map"

    const/4 v6, 0x0

    const/4 v7, 0x5

    invoke-virtual {v0, v5, v6, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 101
    .local v2, "ret":J
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 109
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gez v5, :cond_1

    .line 110
    const-string v5, "UserAnalysis.PlaceResolver"

    const-string v6, "Failed to register a monitor"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 102
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "ret":J
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 104
    const-string v5, "UserAnalysis.PlaceResolver"

    const-string v6, "SQLiteException"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    monitor-exit p0

    goto :goto_0

    .line 107
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5

    .line 114
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2    # "ret":J
    :cond_1
    const-string v5, "UserAnalysis.PlaceResolver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Monitor(mid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", m="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") is registered."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public reset()V
    .locals 5

    .prologue
    .line 400
    monitor-enter p0

    .line 402
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 403
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 404
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "db == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-void

    .line 407
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    const-string v2, "id_map"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 408
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 413
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_1
    :try_start_3
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 409
    :catch_0
    move-exception v1

    .line 410
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 411
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "SQLiteException"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public setSticky(II)V
    .locals 10
    .param p1, "monitorId"    # I
    .param p2, "sticky"    # I

    .prologue
    const/4 v9, 0x1

    .line 332
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 333
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "sticky"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 337
    monitor-enter p0

    .line 339
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 340
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 341
    const-string v4, "UserAnalysis.PlaceResolver"

    const-string v5, "db == null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_0
    return-void

    .line 344
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :try_start_2
    const-string v4, "id_map"

    const-string v5, "monitor_id = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 347
    .local v2, "ret":I
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 353
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 355
    if-eq v2, v9, :cond_0

    .line 356
    const-string v4, "UserAnalysis.PlaceResolver"

    const-string v5, "Something wrong in id map database."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 348
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "ret":I
    :catch_0
    move-exception v1

    .line 349
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 350
    const-string v4, "UserAnalysis.PlaceResolver"

    const-string v5, "SQLiteException"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    monitor-exit p0

    goto :goto_0

    .line 353
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4
.end method

.method public unregisterByPlaceId(II)I
    .locals 15
    .param p1, "placeId"    # I
    .param p2, "locationMethod"    # I

    .prologue
    .line 123
    monitor-enter p0

    .line 125
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 126
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_0

    .line 127
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "db == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 128
    const/4 v14, -0x1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 179
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return v14

    .line 130
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    const/4 v2, 0x1

    :try_start_2
    const-string v3, "id_map"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "sticky"

    aput-object v6, v4, v5

    const-string v5, "place_id = ? AND location_method = ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v11

    .line 140
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 142
    const/4 v14, 0x0

    .line 144
    .local v14, "sticky":I
    if-eqz v11, :cond_2

    .line 145
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 146
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 147
    const-string v2, "sticky"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 149
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 151
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 154
    monitor-enter p0

    .line 156
    :try_start_4
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 157
    if-nez v1, :cond_3

    .line 158
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "db == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 159
    const/4 v14, -0x1

    .end local v14    # "sticky":I
    :try_start_5
    monitor-exit p0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 135
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 136
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_6
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 137
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "SQLiteException"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const/4 v14, -0x1

    monitor-exit p0

    goto :goto_0

    .line 140
    .end local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v2

    .line 161
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "sticky":I
    :cond_3
    :try_start_7
    const-string v2, "id_map"

    const-string v3, "place_id = ? AND location_method = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 164
    .local v13, "ret":I
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 170
    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 172
    const/4 v2, 0x1

    if-eq v13, v2, :cond_4

    .line 173
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "Something wrong in id map database."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_4
    const-string v2, "UserAnalysis.PlaceResolver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Monitor(pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", m="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is unregistered."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 165
    .end local v13    # "ret":I
    :catch_1
    move-exception v12

    .line 166
    .restart local v12    # "e":Landroid/database/sqlite/SQLiteException;
    :try_start_9
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 167
    const-string v2, "UserAnalysis.PlaceResolver"

    const-string v3, "SQLiteException"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v14, -0x1

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method
