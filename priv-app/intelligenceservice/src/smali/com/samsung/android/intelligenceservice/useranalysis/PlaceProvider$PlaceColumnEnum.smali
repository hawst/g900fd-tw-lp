.class public final enum Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;
.super Ljava/lang/Enum;
.source "PlaceProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaceColumnEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum BLUETOOTH_MAC_ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum BLUETOOTH_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum CATEGORY:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum LATITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum LOCATION_TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum LONGITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum MAX:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum PROVIDER:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum RADIUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum WIFI_BSSID:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

.field public static final enum WIFI_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1122
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "TYPE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1123
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1124
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->CATEGORY:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1125
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "LOCATION_TYPE"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LOCATION_TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1126
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "ADDRESS"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1127
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "LATITUDE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LATITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1128
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "LONGITUDE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LONGITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1129
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "RADIUS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->RADIUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1130
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "WIFI_NAME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1131
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "WIFI_BSSID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_BSSID:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1132
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "BLUETOOTH_NAME"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1133
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "BLUETOOTH_MAC_ADDRESS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_MAC_ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1134
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "MONITORING_STATUS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1135
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "PROVIDER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->PROVIDER:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1137
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    const-string v1, "MAX"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MAX:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    .line 1121
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->CATEGORY:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LOCATION_TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LATITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LONGITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->RADIUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_BSSID:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_NAME:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_MAC_ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->PROVIDER:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MAX:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->$VALUES:[Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1121
    const-class v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;
    .locals 1

    .prologue
    .line 1121
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->$VALUES:[Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v0}, [Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    return-object v0
.end method
