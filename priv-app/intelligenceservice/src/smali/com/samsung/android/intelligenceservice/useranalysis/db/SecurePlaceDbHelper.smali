.class public Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;
.super Landroid/database/sqlite/SQLiteSecureOpenHelper;
.source "SecurePlaceDbHelper.java"


# static fields
.field private static final LOCATION_WIDGET_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.widgetapp.locationwidget"

.field private static final PLACE_DB_BLOB_TYPE:Ljava/lang/String; = "BLOB"

.field private static final PLACE_DB_DEFAULT_EMPTY_STRING:Ljava/lang/String; = "DEFAULT \'\'"

.field private static final PLACE_DB_DEFAULT_INT_ZERO:Ljava/lang/String; = "DEFAULT 0"

.field private static final PLACE_DB_INTEGER_TYPE:Ljava/lang/String; = "INTEGER"

.field private static final PLACE_DB_NOT_NULL:Ljava/lang/String; = "NOT NULL"

.field private static final PLACE_DB_PRIMARY_KEY_TYPE:Ljava/lang/String; = "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

.field private static final PLACE_DB_REAL_TYPE:Ljava/lang/String; = "REAL"

.field private static final PLACE_DB_TEXT_TYPE:Ljava/lang/String; = "TEXT"

.field private static final PLACE_DB_TIME_TYPE:Ljava/lang/String; = "TIMESTAMP"

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.SecurePlaceDbHelper"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v0, "Place.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    const-string v0, "UserAnalysis.SecurePlaceDbHelper"

    const-string v1, "SecurePlaceDbHelper() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dbName"    # Ljava/lang/String;

    .prologue
    .line 37
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    const-string v0, "UserAnalysis.SecurePlaceDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SecurePlaceDbHelper: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    return-void
.end method

.method private static checkLocationWidget(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    if-nez p0, :cond_0

    .line 130
    const/4 v1, 0x0

    .line 141
    :goto_0
    return v1

    .line 133
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 136
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.sec.android.widgetapp.locationwidget"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    const/4 v1, 0x1

    .local v1, "installed":Z
    goto :goto_0

    .line 138
    .end local v1    # "installed":Z
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .restart local v1    # "installed":Z
    goto :goto_0
.end method

.method static createTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 55
    const-string v1, "UserAnalysis.SecurePlaceDbHelper"

    const-string v2, "createTables() "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 59
    :try_start_0
    const-string v1, "CREATE TABLE IF NOT EXISTS place (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp_utc TIMESTAMP,type INTEGER,name TEXT NOT NULL DEFAULT \'\',category INTEGER,location_type INTEGER,address TEXT NOT NULL DEFAULT \'\',latitude REAL,longitude REAL,radius INTEGER NOT NULL DEFAULT 0,wifi_name TEXT,wifi_bssid TEXT,bluetooth_name TEXT,bluetooth_mac_address TEXT,monitoring_status INTEGER,provider TEXT,extra_data BLOB NOT NULL DEFAULT \'\');"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 79
    const-wide/16 v2, 0x0

    const-string v1, "Home"

    const/4 v4, 0x1

    invoke-static {p1, v2, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V

    .line 80
    const-wide/16 v2, 0x1

    const-string v1, "Work"

    const/4 v4, 0x2

    invoke-static {p1, v2, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V

    .line 81
    const-wide/16 v2, 0x2

    const-string v1, "Car"

    const/4 v4, 0x3

    invoke-static {p1, v2, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V

    .line 83
    invoke-static {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->checkLocationWidget(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const-wide/16 v2, 0x3

    const-string v1, "School"

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V

    .line 85
    const-wide/16 v2, 0x4

    const-string v1, "Gym"

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 93
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 94
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "UserAnalysis.SecurePlaceDbHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static insertDefaultPlace(Landroid/database/sqlite/SQLiteDatabase;JLjava/lang/String;I)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "_id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "category"    # I

    .prologue
    const/4 v4, 0x0

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO place (_id,timestamp_utc,type,name,category,location_type,monitoring_status,provider) VALUES ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'com.android.settings\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method static upgradeTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 98
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS place"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    invoke-static {p0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->createTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 105
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "UserAnalysis.SecurePlaceDbHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error executing SQL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 43
    const-string v0, "UserAnalysis.SecurePlaceDbHelper"

    const-string v1, "onCreate() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->createTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 50
    const-string v0, "UserAnalysis.SecurePlaceDbHelper"

    const-string v1, "onUpgrade() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->upgradeTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 52
    return-void
.end method
