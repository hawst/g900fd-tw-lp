.class public Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;
.super Ljava/lang/Object;
.source "AesEncryption.java"


# static fields
.field private static final KEY_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final PBE_ITERATION_COUNT:I = 0x64

.field private static final PBE_KEY_LENGTH:I = 0x400

.field private static final SECRET_KEY_ALGORITHM:Ljava/lang/String; = "PBKDF2WithHmacSHA1"


# instance fields
.field private final salt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "_cf_salt"

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;->salt:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 9
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 53
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    iget-object v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;->salt:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    const/16 v7, 0x64

    const/16 v8, 0x400

    invoke-direct {v2, v4, v6, v7, v8}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 55
    .local v2, "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    const-string v4, "PBKDF2WithHmacSHA1"

    invoke-static {v4}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v1

    .line 56
    .local v1, "factory":Ljavax/crypto/SecretKeyFactory;
    invoke-virtual {v1, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 57
    .local v3, "tmp":Ljavax/crypto/SecretKey;
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v3}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v6

    const-string v7, "AES"

    invoke-direct {v4, v6, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    .line 66
    .end local v1    # "factory":Ljavax/crypto/SecretKeyFactory;
    .end local v2    # "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    .end local v3    # "tmp":Ljavax/crypto/SecretKey;
    :goto_0
    return-object v4

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/security/spec/InvalidKeySpecException;
    invoke-virtual {v0}, Ljava/security/spec/InvalidKeySpecException;->printStackTrace()V

    move-object v4, v5

    .line 60
    goto :goto_0

    .line 61
    .end local v0    # "e":Ljava/security/spec/InvalidKeySpecException;
    :catch_1
    move-exception v0

    .line 62
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    move-object v4, v5

    .line 63
    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v0

    .line 65
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v4, v5

    .line 66
    goto :goto_0
.end method
