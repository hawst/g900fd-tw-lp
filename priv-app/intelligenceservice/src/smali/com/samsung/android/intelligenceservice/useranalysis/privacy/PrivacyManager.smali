.class public Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
.super Ljava/lang/Object;
.source "PrivacyManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$1;,
        Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;,
        Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;,
        Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ConsentChangePropagator;
    }
.end annotation


# static fields
.field public static final ACTION_CONSENT_DETECTION_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.CONSENT_DETECTION_CHANGED"

.field public static final ACTION_CONSENT_PREDICTION_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.CONSENT_PREDICTION_CHANGED"

.field private static final DATABASE_CREATE:Ljava/lang/String; = "CREATE TABLE place_privacy (package_name STRING PRIMARY KEY NOT NULL, consent_code INTEGER NOT NULL);"

.field private static final DATABASE_NAME:Ljava/lang/String; = "privacy"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final ERROR_CODE_ILLEGAL_ARGUMENT:I = -0x1

.field private static final ERROR_CODE_SECURITY:I = -0x2

.field private static final ERROR_CODE_SUCCESS:I = 0x0

.field private static final ERROR_CODE_UNKNOWN:I = -0x3

.field public static final EXTRA_KEY_CONSENT:Ljava/lang/String; = "consent"

.field private static final KEY_CONSENT_CODE:Ljava/lang/String; = "consent_code"

.field private static final KEY_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final PERMISSION_REQUIRED_FOR_PRIVACY:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

.field public static final PRIVACY_DETECT_PLACE:I = 0x2

.field public static final PRIVACY_PREDICT_PLACE:I = 0x1

.field private static final TABLE_NAME:Ljava/lang/String; = "place_privacy"

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.PrivacyManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    .line 58
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    .line 155
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    .line 156
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    .line 157
    return-void
.end method

.method private checkPermission(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permission"    # Ljava/lang/String;

    .prologue
    .line 423
    const/4 v2, 0x0

    .line 425
    .local v2, "granted":Z
    iget-object v9, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 426
    .local v6, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v6, :cond_0

    .line 428
    const/16 v9, 0x1000

    :try_start_0
    invoke-virtual {v6, p1, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 431
    .local v5, "pacakgeInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v5, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 432
    .local v8, "requestedPermissions":[Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 434
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v7, v0, v3

    .line 436
    .local v7, "requestedPermission":Ljava/lang/String;
    invoke-virtual {v7, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-eqz v9, :cond_1

    .line 437
    const/4 v2, 0x1

    .line 448
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "pacakgeInfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "requestedPermission":Ljava/lang/String;
    .end local v8    # "requestedPermissions":[Ljava/lang/String;
    :cond_0
    :goto_1
    return v2

    .line 434
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "pacakgeInfo":Landroid/content/pm/PackageInfo;
    .restart local v7    # "requestedPermission":Ljava/lang/String;
    .restart local v8    # "requestedPermissions":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 443
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "pacakgeInfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "requestedPermission":Ljava/lang/String;
    .end local v8    # "requestedPermissions":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 444
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "UserAnalysis.PrivacyManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid package name - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private checkPrivacyByPass(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 454
    if-eqz p1, :cond_0

    .line 455
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    .line 460
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private propagateDetectionConsentChange(Z)V
    .locals 5
    .param p1, "consentNew"    # Z

    .prologue
    .line 465
    const-string v3, "UserAnalysis.PrivacyManager"

    const-string v4, "Consent status for detection has been changed."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const-string v3, "UserAnalysis.PrivacyManager"

    const-string v4, "Inform detecter about the change."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    move-result-object v1

    .line 469
    .local v1, "detector":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->registerPrivacyConsentChangedListener()V

    .line 470
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 471
    .local v0, "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    if-eqz v0, :cond_0

    .line 473
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.action.CONSENT_DETECTION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 474
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "consent"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 475
    invoke-virtual {v0, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 477
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->unregisterPrivacyConsentChangedListener()V

    .line 478
    return-void
.end method


# virtual methods
.method public cleanUserConsent(Ljava/lang/String;)V
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 336
    invoke-virtual {p0, v10}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v1

    .line 339
    .local v1, "consentDetectionLast":Z
    monitor-enter p0

    .line 341
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 342
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_1

    .line 343
    const-string v5, "UserAnalysis.PrivacyManager"

    const-string v6, "db == null"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :goto_0
    return-void

    .line 346
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_1
    :try_start_2
    const-string v5, "place_privacy"

    const-string v6, "package_name = ? "

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-virtual {v2, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 347
    .local v4, "ret":I
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 353
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 355
    if-ge v4, v9, :cond_2

    .line 356
    const-string v5, "UserAnalysis.PrivacyManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' has not been registered. return without effects."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 348
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "ret":I
    :catch_0
    move-exception v3

    .line 349
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 350
    const-string v5, "UserAnalysis.PrivacyManager"

    const-string v6, "SQLiteException"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    monitor-exit p0

    goto :goto_0

    .line 353
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5

    .line 360
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4    # "ret":I
    :cond_2
    invoke-virtual {p0, v10}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v0

    .line 362
    .local v0, "consentDetectionCurrent":Z
    if-eq v1, v0, :cond_0

    .line 363
    invoke-direct {p0, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->propagateDetectionConsentChange(Z)V

    goto :goto_0
.end method

.method public getConsentedPackages(I)[Ljava/lang/String;
    .locals 13
    .param p1, "consentCode"    # I

    .prologue
    .line 369
    const/4 v8, 0x0

    .line 370
    .local v8, "consentedPackages":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "consent_code & "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 376
    .local v3, "select":Ljava/lang/String;
    monitor-enter p0

    .line 378
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 379
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 380
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "db == null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    const/4 v1, 0x0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return-object v1

    .line 383
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    const-string v1, "place_privacy"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "package_name"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    .line 391
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 393
    if-eqz v9, :cond_3

    .line 394
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 396
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v8, v1, [Ljava/lang/String;

    .line 398
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 401
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v11, v1, :cond_1

    .line 402
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 404
    .local v12, "packageName":Ljava/lang/String;
    aput-object v12, v8, v11

    .line 405
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 401
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 386
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "i":I
    .end local v12    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 387
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 388
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "SQLiteException"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/4 v1, 0x0

    monitor-exit p0

    goto :goto_0

    .line 391
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 409
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v8, v11

    .line 411
    .end local v11    # "i":I
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 413
    :cond_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 415
    const-string v2, "UserAnalysis.PrivacyManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v8, :cond_4

    array-length v1, v8

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " packages are consented for ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v8

    .line 418
    goto/16 :goto_0

    .line 415
    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public getUserConsent(Ljava/lang/String;)I
    .locals 13
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 296
    const-string v1, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    invoke-direct {p0, p1, v1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "Required permission is not requested."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const/4 v10, -0x2

    .line 331
    :goto_0
    return v10

    .line 301
    :cond_0
    const/4 v10, -0x1

    .line 305
    .local v10, "consentCode":I
    monitor-enter p0

    .line 307
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 308
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 309
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "db == null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    const/4 v10, -0x1

    .end local v10    # "consentCode":I
    :try_start_1
    monitor-exit p0

    goto :goto_0

    .line 319
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 312
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v10    # "consentCode":I
    :cond_1
    const/4 v1, 0x1

    :try_start_2
    const-string v2, "place_privacy"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "consent_code"

    aput-object v5, v3, v4

    const-string v4, "package_name = ? "

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    .line 319
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 321
    if-eqz v11, :cond_3

    .line 322
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 323
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 324
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 326
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 328
    :cond_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 330
    const-string v1, "UserAnalysis.PrivacyManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' has been consented for code("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 315
    .local v12, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 316
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "SQLiteException"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v10, -0x1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public isUserConsented(I)Z
    .locals 14
    .param p1, "consentCode"    # I

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 161
    const/4 v9, 0x0

    .line 165
    .local v9, "consented":Z
    monitor-enter p0

    .line 167
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 168
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_0

    .line 169
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "db == null"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return v13

    .line 172
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_0
    :try_start_2
    const-string v1, "place_privacy"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "consent_code"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 178
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 180
    if-eqz v10, :cond_3

    .line 181
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 182
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 185
    :cond_1
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 186
    .local v8, "code":I
    and-int v1, p1, v8

    if-ne v1, p1, :cond_4

    move v9, v12

    .line 188
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v9, :cond_1

    .line 190
    .end local v8    # "code":I
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 192
    :cond_3
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    move v13, v9

    .line 194
    goto :goto_0

    .line 173
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 174
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 175
    const-string v1, "UserAnalysis.PrivacyManager"

    const-string v2, "SQLiteException"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    monitor-exit p0

    goto :goto_0

    .line 178
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v8    # "code":I
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_4
    move v9, v13

    .line 186
    goto :goto_1
.end method

.method public isUserConsented(Ljava/lang/String;I)Z
    .locals 15
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "consentCode"    # I

    .prologue
    .line 199
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->checkPrivacyByPass(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    const/4 v12, 0x1

    .line 238
    :goto_0
    return v12

    .line 204
    :cond_0
    const/4 v12, 0x0

    .line 208
    .local v12, "consented":Z
    monitor-enter p0

    .line 210
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 211
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_1

    .line 212
    const-string v2, "UserAnalysis.PrivacyManager"

    const-string v3, "db == null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    const/4 v12, 0x0

    .end local v12    # "consented":Z
    :try_start_1
    monitor-exit p0

    goto :goto_0

    .line 222
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 215
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v12    # "consented":Z
    :cond_1
    const/4 v2, 0x1

    :try_start_2
    const-string v3, "place_privacy"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "consent_code"

    aput-object v6, v4, v5

    const-string v5, "package_name = ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v13

    .line 222
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 224
    if-eqz v13, :cond_3

    .line 225
    const/4 v11, 0x0

    .line 227
    .local v11, "code":I
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 228
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 229
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 231
    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 233
    and-int v2, p2, v11

    move/from16 v0, p2

    if-ne v2, v0, :cond_4

    const/4 v12, 0x1

    .line 235
    .end local v11    # "code":I
    :cond_3
    :goto_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    .line 217
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v13    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v14

    .line 218
    .local v14, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 219
    const-string v2, "UserAnalysis.PrivacyManager"

    const-string v3, "SQLiteException"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v12, 0x0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 233
    .end local v14    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v11    # "code":I
    .restart local v13    # "cursor":Landroid/database/Cursor;
    :cond_4
    const/4 v12, 0x0

    goto :goto_1
.end method

.method public setUserConsent(Ljava/lang/String;I)I
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "consentCode"    # I

    .prologue
    const/4 v11, 0x2

    const/4 v7, -0x1

    .line 243
    shr-int/lit8 v8, p2, 0x2

    if-eqz v8, :cond_0

    .line 245
    const-string v8, "UserAnalysis.PrivacyManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid consent code("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") encountered"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :goto_0
    return v7

    .line 249
    :cond_0
    const-string v8, "com.samsung.android.internal.intelligence.useranalysis.permission.READ_PLACE"

    invoke-direct {p0, p1, v8}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 250
    const-string v7, "UserAnalysis.PrivacyManager"

    const-string v8, "Required permission is not requested."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const/4 v7, -0x2

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {p0, v11}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v1

    .line 256
    .local v1, "consentDetectionLast":Z
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 257
    .local v6, "values":Landroid/content/ContentValues;
    const-string v8, "package_name"

    invoke-virtual {v6, v8, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v8, "consent_code"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 262
    monitor-enter p0

    .line 264
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;

    invoke-virtual {v8}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 265
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v2, :cond_2

    .line 266
    const-string v8, "UserAnalysis.PrivacyManager"

    const-string v9, "db == null"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :try_start_1
    monitor-exit p0

    goto :goto_0

    .line 276
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 269
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_2
    :try_start_2
    const-string v8, "place_privacy"

    const/4 v9, 0x0

    const/4 v10, 0x5

    invoke-virtual {v2, v8, v9, v6, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v4

    .line 270
    .local v4, "ret":J
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 278
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-gez v7, :cond_3

    .line 279
    const-string v7, "UserAnalysis.PrivacyManager"

    const-string v8, "Failed to register an id pair."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v7, -0x3

    goto :goto_0

    .line 271
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "ret":J
    :catch_0
    move-exception v3

    .line 272
    .local v3, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 273
    const-string v8, "UserAnalysis.PrivacyManager"

    const-string v9, "SQLiteException"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 284
    .end local v3    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4    # "ret":J
    :cond_3
    invoke-virtual {p0, v11}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v0

    .line 286
    .local v0, "consentDetectionCurrent":Z
    if-eq v1, v0, :cond_4

    .line 287
    invoke-direct {p0, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->propagateDetectionConsentChange(Z)V

    .line 290
    :cond_4
    const-string v7, "UserAnalysis.PrivacyManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' has been consented for code("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const/4 v7, 0x0

    goto/16 :goto_0
.end method
