.class Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;
.super Ljava/lang/Object;
.source "PlaceProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlaceInfo"
.end annotation


# instance fields
.field public address:Ljava/lang/String;

.field public btMacAddress:Ljava/lang/String;

.field public btName:Ljava/lang/String;

.field public category:I

.field public id:I

.field public latitude:D

.field public locationType:I

.field public longitude:D

.field public monitoringStatus:I

.field public name:Ljava/lang/String;

.field public provider:Ljava/lang/String;

.field public radius:I

.field public type:I

.field public wifiBssId:Ljava/lang/String;

.field public wifiName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->id:I

    .line 1104
    const-string v0, "type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->type:I

    .line 1105
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->name:Ljava/lang/String;

    .line 1106
    const-string v0, "category"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->category:I

    .line 1107
    const-string v0, "location_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->locationType:I

    .line 1108
    const-string v0, "address"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->address:Ljava/lang/String;

    .line 1109
    const-string v0, "latitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->latitude:D

    .line 1110
    const-string v0, "longitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->longitude:D

    .line 1111
    const-string v0, "radius"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->radius:I

    .line 1112
    const-string v0, "wifi_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiName:Ljava/lang/String;

    .line 1113
    const-string v0, "wifi_bssid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->wifiBssId:Ljava/lang/String;

    .line 1114
    const-string v0, "bluetooth_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btName:Ljava/lang/String;

    .line 1115
    const-string v0, "bluetooth_mac_address"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->btMacAddress:Ljava/lang/String;

    .line 1116
    const-string v0, "monitoring_status"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->monitoringStatus:I

    .line 1117
    const-string v0, "provider"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceInfo;->provider:Ljava/lang/String;

    .line 1118
    return-void
.end method
