.class public Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;
.super Ljava/lang/Object;
.source "MyPlaceDbPreference.java"


# static fields
.field private static final PREF_NAME:Ljava/lang/String; = "myplace_pref"

.field public static final PREF_RESTORE_STATUS:Ljava/lang/String; = "restore_status"

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.MyPlaceDbPreference"


# instance fields
.field private final pref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, "UserAnalysis.MyPlaceDbPreference"

    const-string v1, "Constructor Preference"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    const-string v0, "myplace_pref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;->pref:Landroid/content/SharedPreferences;

    .line 18
    return-void
.end method


# virtual methods
.method public getValue(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;->pref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public put(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 21
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;->pref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 23
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 24
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 25
    return-void
.end method
