.class public Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ConsentChangePropagator;
.super Landroid/app/IntentService;
.source "PrivacyManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConsentChangePropagator"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "ConsentChangePropagator"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 66
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "action":Ljava/lang/String;
    const-string v4, "UserAnalysis.PrivacyManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Intent(action"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") is received."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v4, "com.samsung.android.internal.intelligence.useranalysis.action.CONSENT_PREDICTION_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    const-string v4, "consent"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 78
    .local v2, "consent":Z
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;-><init>(Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$1;)V

    .line 79
    .local v1, "connection":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;
    new-instance v3, Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ConsentChangePropagator;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;-><init>(Landroid/content/Context;Lcom/samsung/android/providers/context/privacy/PrivacyServiceConnection;)V

    .line 82
    .local v3, "contextPrivacyManager":Lcom/samsung/android/providers/context/privacy/PrivacyManager;
    iput-boolean v2, v1, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mConsentPrediction:Z

    .line 83
    iput-object v3, v1, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;->mContextPrivacyManager:Lcom/samsung/android/providers/context/privacy/PrivacyManager;

    .line 85
    invoke-virtual {v3}, Lcom/samsung/android/providers/context/privacy/PrivacyManager;->bindService()Z

    .line 87
    const-string v4, "UserAnalysis.PrivacyManager"

    const-string v5, "Bind to context privacy manager."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    .end local v1    # "connection":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager$ContextPrivacyServiceConnection;
    .end local v2    # "consent":Z
    .end local v3    # "contextPrivacyManager":Lcom/samsung/android/providers/context/privacy/PrivacyManager;
    :cond_0
    return-void
.end method
