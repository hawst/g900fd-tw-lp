.class public final Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Intent;
.super Ljava/lang/Object;
.source "PlaceContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Intent"
.end annotation


# static fields
.field public static final APP_PROVIDED_PLACE_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.APP_PROVIDED_PLACE_CHANGED"

.field public static final EXTRA_DATA:Ljava/lang/String; = "data"

.field public static final EXTRA_OPERATION:Ljava/lang/String; = "operation"

.field public static final OPERATION_TYPE_DELETE:Ljava/lang/String; = "delete"

.field public static final OPERATION_TYPE_INSERT:Ljava/lang/String; = "insert"

.field public static final OPERATION_TYPE_UPDATE:Ljava/lang/String; = "update"

.field public static final SYSTEM_PREDICTED_PLACE_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.SYSTEM_PREDICTED_PLACE_CHANGED"

.field public static final USER_DEFINED_PLACE_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.USER_DEFINED_PLACE_CHANGED"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    return-void
.end method
