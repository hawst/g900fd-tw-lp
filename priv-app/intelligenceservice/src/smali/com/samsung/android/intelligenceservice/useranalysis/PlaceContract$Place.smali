.class public final Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;
.super Ljava/lang/Object;
.source "PlaceContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$PlaceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Place"
.end annotation


# static fields
.field public static final CATEGORY_CAR:I = 0x3

.field public static final CATEGORY_HOME:I = 0x1

.field public static final CATEGORY_OTHERS:I = 0x0

.field public static final CATEGORY_WORK:I = 0x2

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/place"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/place"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final LOCATION_TYPE_BLUETOOTH_DEVICE:I = 0x4

.field public static final LOCATION_TYPE_GEO_LOCATION:I = 0x1

.field public static final LOCATION_TYPE_WIFI_AP:I = 0x2

.field public static final MONITORING_STATUS_OFF:I = 0x0

.field public static final MONITORING_STATUS_ON:I = 0x1

.field public static final TYPE_APP_PROVIDED:I = 0x2

.field public static final TYPE_SYSTEM_PREDICTED:I = 0x3

.field public static final TYPE_USER_DEFINED:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "content://com.samsung.android.internal.intelligence.useranalysis/place"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method
