.class public Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PlaceDbHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UserAnalysis.PlaceDbHelper"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const-string v0, "Place.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 42
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->mContext:Landroid/content/Context;

    .line 43
    const-string v0, "UserAnalysis.PlaceDbHelper"

    const-string v1, "PlaceDbHelper() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 48
    const-string v0, "UserAnalysis.PlaceDbHelper"

    const-string v1, "onCreate() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->createTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 50
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 54
    const-string v0, "UserAnalysis.PlaceDbHelper"

    const-string v1, "onUpgrade() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;->upgradeTables(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 56
    return-void
.end method
