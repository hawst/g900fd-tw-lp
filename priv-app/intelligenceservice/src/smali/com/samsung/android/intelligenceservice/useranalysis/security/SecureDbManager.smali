.class public Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;
.super Ljava/lang/Object;
.source "SecureDbManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UserAnalysis.SecureDbManager"

.field private static final mDbKey:Ljava/lang/String; = "ua.secure.db.key"

.field private static mPassword:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->mPassword:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createNewKey()[B
    .locals 4

    .prologue
    .line 43
    const-string v1, "ua.secure.db.key"

    .line 46
    .local v1, "uniqueKey":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;

    invoke-direct {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;-><init>()V

    const-string v3, "ua.secure.db.key"

    invoke-virtual {v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;->getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 47
    .local v0, "secretKey":Ljavax/crypto/SecretKey;
    if-nez v0, :cond_0

    .line 48
    const-string v2, "UserAnalysis.SecureDbManager"

    const-string v3, "secretKey == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v2, 0x0

    .line 52
    :goto_0
    return-object v2

    .line 51
    :cond_0
    const-string v2, "UserAnalysis.SecureDbManager"

    const-string v3, "Key for secure DB is newly created"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    goto :goto_0
.end method

.method public static declared-synchronized getOldPassword(Landroid/content/Context;)[B
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const-class v6, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;

    monitor-enter v6

    :try_start_0
    const-string v5, "UserAnalysis.SecureDbManager"

    const-string v7, "getting old password"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    const/4 v2, 0x0

    .line 20
    .local v2, "newKey":[B
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 21
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "deviceId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 23
    const-string v5, "UserAnalysis.SecureDbManager"

    const-string v7, "device id is null"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    const/4 v5, 0x0

    .line 30
    :goto_0
    monitor-exit v6

    return-object v5

    .line 26
    :cond_0
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "ua.secure.db.key"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 27
    .local v4, "uniqueKey":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;

    invoke-direct {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;-><init>()V

    .line 29
    .local v1, "enc":Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;->getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v5

    invoke-interface {v5}, Ljavax/crypto/SecretKey;->getEncoded()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    move-object v5, v2

    .line 30
    goto :goto_0

    .line 18
    .end local v0    # "deviceId":Ljava/lang/String;
    .end local v1    # "enc":Lcom/samsung/android/intelligenceservice/useranalysis/security/AesEncryption;
    .end local v2    # "newKey":[B
    .end local v3    # "tm":Landroid/telephony/TelephonyManager;
    .end local v4    # "uniqueKey":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static declared-synchronized getPassword()[B
    .locals 2

    .prologue
    .line 34
    const-class v1, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->mPassword:[B

    if-nez v0, :cond_0

    .line 35
    invoke-static {}, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->createNewKey()[B

    move-result-object v0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->mPassword:[B

    .line 37
    :cond_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/security/SecureDbManager;->mPassword:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
