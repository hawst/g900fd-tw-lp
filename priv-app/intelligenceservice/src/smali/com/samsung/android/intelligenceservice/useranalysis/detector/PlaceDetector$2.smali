.class Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;
.super Landroid/content/BroadcastReceiver;
.source "PlaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->registerPrivacyConsentChangedListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;


# direct methods
.method constructor <init>(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    const-string v1, "consent"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 151
    .local v0, "consent":Z
    if-eqz v0, :cond_0

    .line 152
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addAllPlaces(Landroid/content/Context;)V
    invoke-static {v1, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$400(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;)V

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removeAllPlaces()V
    invoke-static {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$500(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V

    goto :goto_0
.end method
