.class public Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$SLocationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PlaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SLocationBroadcastReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 683
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 686
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "action":Ljava/lang/String;
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    const-string v1, "com.samsung.location.SERVICE_READY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 691
    invoke-static {p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    move-result-object v1

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->restoreMonitors()V
    invoke-static {v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$600(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V

    .line 695
    :goto_0
    return-void

    .line 693
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    move-result-object v1

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->fenceDetected(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v1, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$700(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
