.class Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;
.super Landroid/content/BroadcastReceiver;
.source "PlaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;


# direct methods
.method constructor <init>(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 94
    const-string v2, "UserAnalysis.PlaceDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # getter for: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-static {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$000(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 97
    const-string v2, "UserAnalysis.PlaceDetector"

    const-string v3, "Detection is not consented. Return without effect."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const-string v2, "operation"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "operation":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 103
    const-string v2, "UserAnalysis.PlaceDetector"

    const-string v3, "operation == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 106
    :cond_2
    const-string v2, "data"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 107
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    if-nez v0, :cond_3

    .line 108
    const-string v2, "UserAnalysis.PlaceDetector"

    const-string v3, "data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 111
    :cond_3
    const-string v2, "UserAnalysis.PlaceDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "operation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string v2, "UserAnalysis.PlaceDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " places changes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v2, "insert"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 115
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addPlaces(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v2, p1, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$100(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 116
    :cond_4
    const-string v2, "update"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 117
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->updatePlaces(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v2, p1, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$200(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 118
    :cond_5
    const-string v2, "delete"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;->this$0:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removePlacesByPlaceId(Ljava/util/ArrayList;)V
    invoke-static {v2, v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$300(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method
