.class public Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;
.super Ljava/lang/Object;
.source "DumpLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;,
        Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;
    }
.end annotation


# static fields
.field public static final LOG_TIME_FORMAT:Ljava/lang/String; = "MM-dd HH:mm:ss.SSS"

.field public static final TAG:Ljava/lang/String; = "UserAnalysis"

.field private static final sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;

    invoke-direct {v0}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;-><init>()V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->mHandler:Landroid/os/Handler;

    .line 44
    return-void
.end method

.method public static dump(Landroid/content/Context;Ljava/io/PrintWriter;)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 57
    const-string v3, "[Recent Events]"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 59
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 60
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "dump_log"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 62
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 63
    new-instance v12, Ljava/text/SimpleDateFormat;

    const-string v3, "MM-dd HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v12, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 64
    .local v12, "dateFormat":Ljava/text/DateFormat;
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    .line 65
    .local v11, "date":Ljava/util/Date;
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    const-string v3, "timestamp"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 68
    .local v16, "timestamp":J
    const-string v3, "message"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 70
    .local v14, "message":Ljava/lang/String;
    move-wide/from16 v0, v16

    invoke-virtual {v11, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 71
    const-string v3, "%s %s%n"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v12, v11}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v14, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "date":Ljava/util/Date;
    .end local v12    # "dateFormat":Ljava/text/DateFormat;
    .end local v14    # "message":Ljava/lang/String;
    .end local v16    # "timestamp":J
    :catch_0
    move-exception v13

    .line 76
    .local v13, "e":Landroid/database/sqlite/SQLiteException;
    const-string v3, "SQLiteException"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 77
    const-string v3, "UserAnalysis"

    const-string v4, "It failed to get the database for dump_log (read)"

    invoke-static {v3, v4, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 79
    .end local v13    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    :goto_1
    return-void

    .line 73
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v11    # "date":Ljava/util/Date;
    .restart local v12    # "dateFormat":Ljava/text/DateFormat;
    :cond_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 50
    :try_start_0
    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;

    iget-object v1, v1, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UserAnalysis"

    const-string v2, "It failed to get application instance"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
