.class Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;
.super Ljava/lang/Object;
.source "DumpLog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DumpMessage"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMessage:Ljava/lang/String;

.field private final mTag:Ljava/lang/String;

.field private mTimestamp:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mContext:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mTag:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mMessage:Ljava/lang/String;

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mTimestamp:J

    .line 92
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 97
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpLogDbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 98
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 99
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "timestamp"

    iget-wide v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mTimestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 100
    const-string v5, "message"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mTag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog$DumpMessage;->mMessage:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v5, "dump_log"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 102
    .local v2, "id":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-gez v5, :cond_0

    .line 103
    const-string v5, "UserAnalysis"

    const-string v6, "It failed to insert to dump_log table"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "id":J
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    return-void

    .line 105
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    const-string v5, "UserAnalysis"

    const-string v6, "It failed to get the database for dump_log"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
