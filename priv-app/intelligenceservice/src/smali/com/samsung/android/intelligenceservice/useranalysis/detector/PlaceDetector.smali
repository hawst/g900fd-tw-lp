.class public Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
.super Ljava/lang/Object;
.source "PlaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$EmergencyModeChangeReceiver;,
        Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$SLocationBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_LOCATION_FENCE_DETECTED:Ljava/lang/String; = "com.samsung.location.action.LOCATION_FENCE_DETECTED"

.field public static final ACTION_LOCATION_SERVICE_READY:Ljava/lang/String; = "com.samsung.location.SERVICE_READY"

.field private static final ACTION_PLACE_PROXIMITY_CHANGED:Ljava/lang/String; = "com.samsung.android.internal.intelligence.useranalysis.action.PLACE_PROXIMITY_CHANGED"

.field private static final DEDICATED_PLACE_PROXIMITY_RECEIVER:Ljava/lang/String; = "com.samsung.android.providers.context"

.field private static final EXTRA_CATEGORY:Ljava/lang/String; = "category"

.field private static final EXTRA_LOCATION_METHOD:Ljava/lang/String; = "location_method"

.field public static final EXTRA_MONITOR_ID:Ljava/lang/String; = "monitor_id"

.field private static final EXTRA_NAME:Ljava/lang/String; = "name"

.field private static final EXTRA_PROXIMITY_TYPE:Ljava/lang/String; = "proximity_type"

.field private static final EXTRA_TRANSITION:Ljava/lang/String; = "transition"

.field private static final EXTRA_URI:Ljava/lang/String; = "uri"

.field private static final GEOFENCE_RADIUS_DEFAULT:I = 0xc8

.field private static final GEOFENCE_RADIUS_MAX:I = 0x1f4

.field private static final GEOFENCE_RADIUS_MIN:I = 0x64

.field private static final LOCATION_METHOD_BLUETOOTH_DEVICE:I = 0x4

.field private static final LOCATION_METHOD_GEO_POINT:I = 0x1

.field private static final LOCATION_METHOD_WIFI_AP:I = 0x2

.field private static final PLACE_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "UserAnalysis.PlaceDetector"

.field private static sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

.field private final mPlaceChangedListener:Landroid/content/BroadcastReceiver;

.field private mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

.field private mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

.field private mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "monitoring_status"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "location_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "radius"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "wifi_bssid"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "bluetooth_mac_address"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->PLACE_PROJECTION:[Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$1;-><init>(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V

    iput-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPlaceChangedListener:Landroid/content/BroadcastReceiver;

    .line 124
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    .line 127
    iput-object p1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    .line 128
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-direct {v2, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    .line 129
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-direct {v2, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    .line 130
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-direct {v2, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    .line 132
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 133
    .local v0, "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.internal.intelligence.useranalysis.action.USER_DEFINED_PLACE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 134
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.internal.intelligence.useranalysis.action.APP_PROVIDED_PLACE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v2, "com.samsung.android.internal.intelligence.useranalysis.action.SYSTEM_PREDICTED_PLACE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPlaceChangedListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addPlaces(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->updatePlaces(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removePlacesByPlaceId(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addAllPlaces(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removeAllPlaces()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->restoreMonitors()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->fenceDetected(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->restartMonitors()V

    return-void
.end method

.method private addAllPlaces(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 343
    const-string v3, "monitoring_status = ?"

    .line 344
    .local v3, "selectClause":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 346
    .local v7, "selectionArg":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->PLACE_PROJECTION:[Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 348
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 349
    invoke-direct {p0, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addPlaces(Landroid/database/Cursor;)V

    .line 350
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 352
    :cond_0
    return-void
.end method

.method private addBluetoothLocationPlace(ILjava/lang/String;)V
    .locals 4
    .param p1, "placeId"    # I
    .param p2, "bluetoothAddress"    # Ljava/lang/String;

    .prologue
    .line 388
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-virtual {v1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->addBluetoothLocation(Ljava/lang/String;)I

    move-result v0

    .line 389
    .local v0, "monitorId":I
    if-ltz v0, :cond_0

    .line 390
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    const/4 v2, 0x4

    invoke-virtual {v1, v0, p1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->register(III)V

    .line 392
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Place("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is added for monitoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_0
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to monitor a place("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private addGeoLocationPlace(IDDI)V
    .locals 8
    .param p1, "placeId"    # I
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .param p6, "radius"    # I

    .prologue
    .line 356
    if-nez p6, :cond_1

    .line 357
    const/16 p6, 0xc8

    .line 364
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->addGeoLocation(DDI)I

    move-result v0

    .line 365
    .local v0, "monitorId":I
    if-ltz v0, :cond_3

    .line 366
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->register(III)V

    .line 368
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Place(lat, lon, radius="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is added for monitoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :goto_1
    return-void

    .line 358
    .end local v0    # "monitorId":I
    :cond_1
    const/16 v1, 0x64

    if-ge p6, v1, :cond_2

    .line 359
    const/16 p6, 0x64

    goto :goto_0

    .line 360
    :cond_2
    const/16 v1, 0x1f4

    if-le p6, v1, :cond_0

    .line 361
    const/16 p6, 0x1f4

    goto :goto_0

    .line 372
    .restart local v0    # "monitorId":I
    :cond_3
    const-string v1, "UserAnalysis.PlaceDetector"

    const-string v2, "Failed to monitor a place with lat/lon"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private addPlaces(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "placeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v5, 0x0

    .line 325
    const-string v3, "_id IN ( ? )"

    .line 326
    .local v3, "selectClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .local v6, "buf":Ljava/lang/StringBuilder;
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 329
    const-string v0, ", "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    invoke-virtual {p2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 332
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 334
    .local v9, "selectionArg":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->PLACE_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 336
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 337
    invoke-direct {p0, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addPlaces(Landroid/database/Cursor;)V

    .line 338
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 340
    :cond_1
    return-void
.end method

.method private addPlaces(Landroid/database/Cursor;)V
    .locals 21
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v19

    .line 402
    .local v19, "registeredMonitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 403
    const-string v2, "monitoring_status"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 404
    .local v18, "monitoringStatus":I
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_0

    .line 408
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 409
    .local v3, "placeId":I
    const/4 v10, 0x0

    .line 410
    .local v10, "alreadyRegisteredGeolocation":Z
    const/4 v11, 0x0

    .line 411
    .local v11, "alreadyRegisteredWifi":Z
    const/4 v9, 0x0

    .line 412
    .local v9, "alreadyRegisteredBt":Z
    if-eqz v19, :cond_1

    .line 413
    move-object/from16 v12, v19

    .local v12, "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    array-length v0, v12

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_1

    aget-object v15, v12, v14

    .line 414
    .local v15, "info":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget v2, v15, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    if-ne v2, v3, :cond_4

    .line 415
    iget v2, v15, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    packed-switch v2, :pswitch_data_0

    .line 428
    :goto_2
    :pswitch_0
    if-eqz v10, :cond_4

    if-eqz v11, :cond_4

    if-eqz v9, :cond_4

    .line 436
    .end local v12    # "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v14    # "i$":I
    .end local v15    # "info":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v16    # "len$":I
    :cond_1
    const-string v2, "location_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 437
    .local v17, "locationType":I
    if-nez v10, :cond_2

    and-int/lit8 v2, v17, 0x1

    if-eqz v2, :cond_2

    .line 438
    const-string v2, "latitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 439
    .local v4, "latitude":D
    const-string v2, "longitude"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    .line 440
    .local v6, "longitude":D
    const-string v2, "radius"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .local v8, "radius":I
    move-object/from16 v2, p0

    .line 441
    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addGeoLocationPlace(IDDI)V

    .line 444
    .end local v4    # "latitude":D
    .end local v6    # "longitude":D
    .end local v8    # "radius":I
    :cond_2
    if-nez v11, :cond_3

    and-int/lit8 v2, v17, 0x2

    if-eqz v2, :cond_3

    .line 445
    const-string v2, "wifi_bssid"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 446
    .local v20, "wifiBssid":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addWifiLocationPlace(ILjava/lang/String;)V

    .line 450
    .end local v20    # "wifiBssid":Ljava/lang/String;
    :cond_3
    if-nez v9, :cond_0

    and-int/lit8 v2, v17, 0x4

    if-eqz v2, :cond_0

    .line 451
    const-string v2, "bluetooth_mac_address"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 452
    .local v13, "bluetoothAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v13}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addBluetoothLocationPlace(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 417
    .end local v13    # "bluetoothAddress":Ljava/lang/String;
    .end local v17    # "locationType":I
    .restart local v12    # "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .restart local v14    # "i$":I
    .restart local v15    # "info":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .restart local v16    # "len$":I
    :pswitch_1
    const/4 v10, 0x1

    .line 418
    goto :goto_2

    .line 420
    :pswitch_2
    const/4 v11, 0x1

    .line 421
    goto/16 :goto_2

    .line 423
    :pswitch_3
    const/4 v9, 0x1

    .line 424
    goto/16 :goto_2

    .line 413
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    .line 455
    .end local v3    # "placeId":I
    .end local v9    # "alreadyRegisteredBt":Z
    .end local v10    # "alreadyRegisteredGeolocation":Z
    .end local v11    # "alreadyRegisteredWifi":Z
    .end local v12    # "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v14    # "i$":I
    .end local v15    # "info":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v16    # "len$":I
    .end local v18    # "monitoringStatus":I
    :cond_5
    return-void

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private addWifiLocationPlace(ILjava/lang/String;)V
    .locals 4
    .param p1, "placeId"    # I
    .param p2, "wifiBssid"    # Ljava/lang/String;

    .prologue
    .line 377
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-virtual {v1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->addWifiLocation(Ljava/lang/String;)I

    move-result v0

    .line 378
    .local v0, "monitorId":I
    if-ltz v0, :cond_0

    .line 379
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, p1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->register(III)V

    .line 381
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Place("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") is added for monitoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :goto_0
    return-void

    .line 383
    :cond_0
    const-string v1, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to monitor a place("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    invoke-virtual {v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->onDestroy()V

    .line 84
    :cond_0
    return-void
.end method

.method private fenceDetected(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 173
    const-string v3, "monitor_id"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 174
    .local v1, "monitorId":I
    const-string v3, "transition"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 175
    .local v2, "transition":I
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v0

    .line 176
    .local v0, "isConsented":Z
    const-string v3, "PD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "detected "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    if-nez v0, :cond_1

    .line 179
    const-string v3, "UserAnalysis.PlaceDetector"

    const-string v4, "Detection is not consented. Return without effect."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    if-lez v2, :cond_0

    .line 184
    invoke-direct {p0, p1, v2, v1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->handlePlaceProximity(Landroid/content/Context;II)V

    .line 185
    if-ne v2, v7, :cond_2

    .line 186
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v3, v1, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->setSticky(II)V

    goto :goto_0

    .line 188
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v3, v1, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->setSticky(II)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const-class v1, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    invoke-direct {v0, p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    .line 77
    :cond_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->sInstance:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handlePlaceProximity(Landroid/content/Context;II)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "transition"    # I
    .param p3, "monitorId"    # I

    .prologue
    const/4 v3, 0x0

    .line 619
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v0, p3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getMonitorInfo(I)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v9

    .line 620
    .local v9, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-nez v9, :cond_0

    .line 621
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get monitor info from  monitor id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    :goto_0
    return-void

    .line 625
    :cond_0
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    iget v2, v9, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    int-to-long v6, v2

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 626
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "name"

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string v7, "location_type"

    aput-object v7, v2, v6

    const/4 v6, 0x2

    const-string v7, "category"

    aput-object v7, v2, v6

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 629
    .local v8, "cursor":Landroid/database/Cursor;
    if-nez v8, :cond_1

    .line 630
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get place information for id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 634
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 635
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get place information for id("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v9, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 640
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 641
    const-string v0, "name"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 642
    .local v4, "placeName":Ljava/lang/String;
    const-string v0, "category"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 643
    .local v5, "placeCategory":I
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 645
    iget v3, v9, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    iget v6, v9, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    move-object v2, p0

    move v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->notifyPlaceProximity(ILjava/lang/String;III)V

    goto/16 :goto_0
.end method

.method private notifyPlaceProximity(ILjava/lang/String;III)V
    .locals 6
    .param p1, "placeId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "category"    # I
    .param p4, "locationMethod"    # I
    .param p5, "proximity"    # I

    .prologue
    .line 652
    sget-object v2, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 654
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 655
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.internal.intelligence.useranalysis.action.PLACE_PROXIMITY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 656
    const-string v2, "name"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    const-string v2, "category"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 658
    const-string v2, "uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 659
    const-string v2, "proximity_type"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 660
    const-string v2, "location_method"

    invoke-virtual {v0, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 662
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 663
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 665
    const-string v2, "UserAnalysis.PlaceDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "proximity change id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", m="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v2, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    const-string v3, "PD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    return-void
.end method

.method private removeAllPlaces()V
    .locals 8

    .prologue
    .line 550
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v4

    .line 551
    .local v4, "monitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-nez v4, :cond_0

    .line 552
    const-string v5, "UserAnalysis.PlaceDetector"

    const-string v6, "No places to remove from monitoring. Return without effects."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :goto_0
    return-void

    .line 556
    :cond_0
    move-object v0, v4

    .local v0, "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 557
    .local v3, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    iget v6, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v5, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->removeLocation(I)V

    .line 556
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 559
    .end local v3    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :cond_1
    const-string v5, "UserAnalysis.PlaceDetector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " places are removed from monitoring."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    iget-object v5, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v5}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->reset()V

    goto :goto_0
.end method

.method private removePlace(IILjava/lang/String;I)V
    .locals 8
    .param p1, "placeId"    # I
    .param p2, "locationMethod"    # I
    .param p3, "placeName"    # Ljava/lang/String;
    .param p4, "placeCategory"    # I

    .prologue
    .line 506
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getMonitorId(II)I

    move-result v6

    .line 507
    .local v6, "monitorId":I
    if-ltz v6, :cond_1

    .line 508
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-virtual {v0, v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->removeLocation(I)V

    .line 510
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->unregisterByPlaceId(II)I

    move-result v7

    .line 511
    .local v7, "sticky":I
    if-lez v7, :cond_0

    .line 512
    const/4 v5, 0x2

    move-object v0, p0

    move v1, p1

    move-object v2, p3

    move v3, p4

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->notifyPlaceProximity(ILjava/lang/String;III)V

    .line 519
    .end local v7    # "sticky":I
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Place(id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", m="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has not been added for monitoring."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Return without effect."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private removePlacesByPlaceId(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 522
    .local p1, "placeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/Bundle;

    .line 523
    .local v12, "place":Landroid/os/Bundle;
    const-string v0, "_id"

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 524
    .local v1, "placeId":I
    const-string v0, "name"

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 525
    .local v2, "placeName":Ljava/lang/String;
    const-string v0, "category"

    invoke-virtual {v12, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 526
    .local v3, "placeCategory":I
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v11

    .line 527
    .local v11, "monitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-eqz v11, :cond_2

    .line 528
    move-object v6, v11

    .local v6, "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    array-length v9, v6

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v9, :cond_1

    aget-object v10, v6, v8

    .line 529
    .local v10, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget v0, v10, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    if-ne v0, v1, :cond_0

    .line 530
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    iget v4, v10, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v0, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->removeLocation(I)V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    iget v4, v10, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->unregisterByPlaceId(II)I

    move-result v13

    .line 533
    .local v13, "sticky":I
    if-lez v13, :cond_0

    .line 534
    iget v4, v10, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->notifyPlaceProximity(ILjava/lang/String;III)V

    .line 528
    .end local v13    # "sticky":I
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 540
    .end local v10    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :cond_1
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Place(id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") is removed from monitoring."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 542
    .end local v6    # "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :cond_2
    const-string v0, "UserAnalysis.PlaceDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Place(id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") has not been added for monitoring."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const-string v0, "UserAnalysis.PlaceDetector"

    const-string v4, "Return without effect."

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 546
    .end local v1    # "placeId":I
    .end local v2    # "placeName":Ljava/lang/String;
    .end local v3    # "placeCategory":I
    .end local v11    # "monitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    .end local v12    # "place":Landroid/os/Bundle;
    :cond_3
    return-void
.end method

.method private restartMonitors()V
    .locals 9

    .prologue
    .line 458
    iget-object v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 459
    const-string v6, "UserAnalysis.PlaceDetector"

    const-string v7, "Detection is not consented. Return without effect."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v6}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v4

    .line 464
    .local v4, "monitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-nez v4, :cond_2

    .line 465
    const-string v6, "UserAnalysis.PlaceDetector"

    const-string v7, "No monitors to restore. Return without effects."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 469
    :cond_2
    move-object v0, v4

    .local v0, "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 470
    .local v3, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget-object v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    iget v7, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v6, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->stop(I)I

    move-result v5

    .line 471
    .local v5, "result":I
    const-string v6, "UserAnalysis.PlaceDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "stop monitor: result="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", pid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", m="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iget-object v6, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    iget v7, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v6, v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->start(I)I

    move-result v5

    .line 474
    const-string v6, "UserAnalysis.PlaceDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "start monitor: result="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", pid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", m="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method private restoreMonitors()V
    .locals 10

    .prologue
    .line 480
    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacy:Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->isUserConsented(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 481
    const-string v7, "UserAnalysis.PlaceDetector"

    const-string v8, "Detection is not consented. Return without effect."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 486
    .local v4, "monitorIdList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mResolver:Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;

    invoke-virtual {v7}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver;->getAllMonitorInfos()[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;

    move-result-object v5

    .line 487
    .local v5, "monitors":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    if-nez v5, :cond_2

    .line 488
    const-string v7, "UserAnalysis.PlaceDetector"

    const-string v8, "No monitors to restore. Return without effects."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-virtual {v7, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->syncGeofenceAfterBoot(Ljava/util/LinkedList;)V

    goto :goto_0

    .line 493
    :cond_2
    move-object v0, v5

    .local v0, "arr$":[Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 494
    .local v3, "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget v7, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 493
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 496
    .end local v3    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    invoke-virtual {v7, v4}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->syncGeofenceAfterBoot(Ljava/util/LinkedList;)V

    .line 498
    move-object v0, v5

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 499
    .restart local v3    # "monitor":Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;
    iget-object v7, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mMonitor:Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;

    iget v8, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/LocationMonitor;->start(I)I

    move-result v6

    .line 500
    .local v6, "result":I
    const-string v7, "UserAnalysis.PlaceDetector"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "restore monitor: result="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->monitorId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", pid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->placeId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", m="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceResolver$MonitorInfo;->locationMethod:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private updatePlaces(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 36
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p2, "placeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1d

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/os/Bundle;

    .line 195
    .local v16, "bundle":Landroid/os/Bundle;
    const-string v4, "_id"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v33

    .line 196
    .local v33, "placeId":I
    const-string v4, "changed_bit_set"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 197
    .local v18, "changed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v18, v4

    if-nez v4, :cond_1

    .line 198
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No change, place id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 201
    :cond_1
    const/4 v4, 0x1

    new-array v0, v4, [J

    move-object/from16 v24, v0

    const/4 v4, 0x0

    aput-wide v18, v24, v4

    .line 202
    .local v24, "longs":[J
    invoke-static/range {v24 .. v24}, Ljava/util/BitSet;->valueOf([J)Ljava/util/BitSet;

    move-result-object v17

    .line 204
    .local v17, "changedBitSet":Ljava/util/BitSet;
    const/16 v28, 0x0

    .line 205
    .local v28, "needToUpdateGeoLocation":Z
    const/16 v29, 0x0

    .line 206
    .local v29, "needToUpdateWifiLocation":Z
    const/16 v27, 0x0

    .line 207
    .local v27, "needToUpdateBlueToothLocation":Z
    const-string v4, "old_location_type"

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v30

    .line 208
    .local v30, "oldLocationType":I
    const-string v4, "location_type"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    .line 209
    .local v21, "curLocationType":I
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 210
    const/16 v28, 0x1

    .line 211
    const/16 v29, 0x1

    .line 212
    const/16 v27, 0x1

    .line 232
    :goto_1
    const/16 v22, 0x0

    .line 233
    .local v22, "cursor":Landroid/database/Cursor;
    if-nez v28, :cond_2

    if-nez v29, :cond_2

    if-eqz v27, :cond_3

    .line 234
    :cond_2
    const-string v7, "_id=?"

    .line 235
    .local v7, "selectClause":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceContract$Place;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->PLACE_PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v35

    aput-object v35, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 237
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "need to update: Geo("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") Wifi("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") BT("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    .end local v7    # "selectClause":Ljava/lang/String;
    :cond_3
    const/16 v26, 0x0

    .line 242
    .local v26, "monitoringStatus":I
    if-eqz v22, :cond_17

    .line 243
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_16

    .line 244
    const-string v4, "monitoring_status"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 254
    :goto_2
    const-string v5, "UserAnalysis.PlaceDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "placeId("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ") monitoringStatus("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ") cursor is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v22, :cond_18

    const-string v4, "null"

    :goto_3
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v4, "name"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 258
    .local v34, "placeName":Ljava/lang/String;
    const-string v4, "category"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v32

    .line 259
    .local v32, "placeCategory":I
    move/from16 v31, v26

    .line 260
    .local v31, "oldMonitoringStatus":I
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->MONITORING_STATUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 261
    if-nez v26, :cond_19

    const/16 v31, 0x1

    .line 265
    :cond_4
    :goto_4
    if-eqz v28, :cond_6

    .line 266
    and-int/lit8 v4, v30, 0x1

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    move/from16 v0, v31

    if-ne v0, v4, :cond_5

    .line 268
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove GeoLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v33

    move-object/from16 v2, v34

    move/from16 v3, v32

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removePlace(IILjava/lang/String;I)V

    .line 271
    :cond_5
    and-int/lit8 v4, v21, 0x1

    if-eqz v4, :cond_6

    .line 272
    if-eqz v22, :cond_1a

    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_1a

    .line 273
    const-string v4, "latitude"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 274
    .local v10, "lat":D
    const-string v4, "longitude"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 275
    .local v12, "lon":D
    const-string v4, "radius"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 276
    .local v14, "radius":I
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add GeoLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v8, p0

    move/from16 v9, v33

    .line 277
    invoke-direct/range {v8 .. v14}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addGeoLocationPlace(IDDI)V

    .line 284
    .end local v10    # "lat":D
    .end local v12    # "lon":D
    .end local v14    # "radius":I
    :cond_6
    :goto_5
    if-eqz v29, :cond_8

    .line 285
    and-int/lit8 v4, v30, 0x2

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    move/from16 v0, v31

    if-ne v0, v4, :cond_7

    .line 287
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove WifiLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v4, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v33

    move-object/from16 v2, v34

    move/from16 v3, v32

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removePlace(IILjava/lang/String;I)V

    .line 290
    :cond_7
    and-int/lit8 v4, v21, 0x2

    if-eqz v4, :cond_8

    .line 291
    if-eqz v22, :cond_1b

    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_1b

    .line 292
    const-string v4, "wifi_bssid"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 293
    .local v15, "bssId":Ljava/lang/String;
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add WifiLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addWifiLocationPlace(ILjava/lang/String;)V

    .line 301
    .end local v15    # "bssId":Ljava/lang/String;
    :cond_8
    :goto_6
    if-eqz v27, :cond_a

    .line 302
    and-int/lit8 v4, v30, 0x4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    move/from16 v0, v31

    if-ne v0, v4, :cond_9

    .line 304
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remove BtLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    const/4 v4, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v33

    move-object/from16 v2, v34

    move/from16 v3, v32

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->removePlace(IILjava/lang/String;I)V

    .line 307
    :cond_9
    and-int/lit8 v4, v21, 0x4

    if-eqz v4, :cond_a

    .line 308
    if-eqz v22, :cond_1c

    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_1c

    .line 309
    const-string v4, "bluetooth_mac_address"

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 310
    .local v25, "macAddress":Ljava/lang/String;
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add BtLocation of PlaceId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    move-object/from16 v0, p0

    move/from16 v1, v33

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->addBluetoothLocationPlace(ILjava/lang/String;)V

    .line 318
    .end local v25    # "macAddress":Ljava/lang/String;
    :cond_a
    :goto_7
    if-eqz v22, :cond_0

    .line 319
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 214
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v26    # "monitoringStatus":I
    .end local v31    # "oldMonitoringStatus":I
    .end local v32    # "placeCategory":I
    .end local v34    # "placeName":Ljava/lang/String;
    :cond_b
    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LOCATION_TYPE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 215
    xor-int v20, v30, v21

    .line 216
    .local v20, "changedLocationType":I
    and-int/lit8 v4, v20, 0x1

    if-eqz v4, :cond_10

    const/16 v28, 0x1

    .line 217
    :goto_8
    and-int/lit8 v4, v20, 0x2

    if-eqz v4, :cond_11

    const/16 v29, 0x1

    .line 218
    :goto_9
    and-int/lit8 v4, v20, 0x4

    if-eqz v4, :cond_12

    const/16 v27, 0x1

    .line 220
    .end local v20    # "changedLocationType":I
    :cond_c
    :goto_a
    if-nez v28, :cond_d

    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LATITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_d

    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->LONGITUDE:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_d

    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->RADIUS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_13

    :cond_d
    const/16 v28, 0x1

    .line 225
    :goto_b
    if-nez v29, :cond_e

    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->WIFI_BSSID:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_14

    :cond_e
    const/16 v29, 0x1

    .line 228
    :goto_c
    if-nez v27, :cond_f

    sget-object v4, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->BLUETOOTH_MAC_ADDRESS:Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;

    invoke-virtual {v4}, Lcom/samsung/android/intelligenceservice/useranalysis/PlaceProvider$PlaceColumnEnum;->ordinal()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_15

    :cond_f
    const/16 v27, 0x1

    :goto_d
    goto/16 :goto_1

    .line 216
    .restart local v20    # "changedLocationType":I
    :cond_10
    const/16 v28, 0x0

    goto :goto_8

    .line 217
    :cond_11
    const/16 v29, 0x0

    goto :goto_9

    .line 218
    :cond_12
    const/16 v27, 0x0

    goto :goto_a

    .line 220
    .end local v20    # "changedLocationType":I
    :cond_13
    const/16 v28, 0x0

    goto :goto_b

    .line 225
    :cond_14
    const/16 v29, 0x0

    goto :goto_c

    .line 228
    :cond_15
    const/16 v27, 0x0

    goto :goto_d

    .line 246
    .restart local v22    # "cursor":Landroid/database/Cursor;
    .restart local v26    # "monitoringStatus":I
    :cond_16
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "no record is found. placeId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 248
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 251
    :cond_17
    const-string v4, "UserAnalysis.PlaceDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cursor is null. placeId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 254
    :cond_18
    const-string v4, "not null"

    goto/16 :goto_3

    .line 261
    .restart local v31    # "oldMonitoringStatus":I
    .restart local v32    # "placeCategory":I
    .restart local v34    # "placeName":Ljava/lang/String;
    :cond_19
    const/16 v31, 0x0

    goto/16 :goto_4

    .line 279
    :cond_1a
    const-string v4, "UserAnalysis.PlaceDetector"

    const-string v5, "cursor is null or monitoring status is off"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 296
    :cond_1b
    const-string v4, "UserAnalysis.PlaceDetector"

    const-string v5, "cursor is null or monitoring status is off"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 313
    :cond_1c
    const-string v4, "UserAnalysis.PlaceDetector"

    const-string v5, "cursor is null or monitoring status is off"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 322
    .end local v16    # "bundle":Landroid/os/Bundle;
    .end local v17    # "changedBitSet":Ljava/util/BitSet;
    .end local v18    # "changed":J
    .end local v21    # "curLocationType":I
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v24    # "longs":[J
    .end local v26    # "monitoringStatus":I
    .end local v27    # "needToUpdateBlueToothLocation":Z
    .end local v28    # "needToUpdateGeoLocation":Z
    .end local v29    # "needToUpdateWifiLocation":Z
    .end local v30    # "oldLocationType":I
    .end local v31    # "oldMonitoringStatus":I
    .end local v32    # "placeCategory":I
    .end local v33    # "placeId":I
    .end local v34    # "placeName":Ljava/lang/String;
    :cond_1d
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->unregisterPrivacyConsentChangedListener()V

    .line 141
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 142
    .local v0, "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPlaceChangedListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 143
    return-void
.end method

.method public registerPrivacyConsentChangedListener()V
    .locals 4

    .prologue
    .line 146
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 147
    new-instance v1, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$2;-><init>(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V

    iput-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    .line 158
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 159
    .local v0, "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.android.internal.intelligence.useranalysis.action.CONSENT_DETECTION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 162
    .end local v0    # "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    :cond_0
    return-void
.end method

.method public unregisterPrivacyConsentChangedListener()V
    .locals 2

    .prologue
    .line 165
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    .line 167
    .local v0, "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    iget-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 168
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->mPrivacyConsentChangedListener:Landroid/content/BroadcastReceiver;

    .line 170
    .end local v0    # "broadcastManager":Landroid/support/v4/content/LocalBroadcastManager;
    :cond_0
    return-void
.end method
