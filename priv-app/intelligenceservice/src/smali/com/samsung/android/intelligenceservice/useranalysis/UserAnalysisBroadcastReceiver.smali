.class public Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UserAnalysisBroadcastReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UserAnalysis.UserAnalysisBroadcastReceiver"


# instance fields
.field private mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

.field private mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    .line 39
    iput-object v0, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 44
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "action":Ljava/lang/String;
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Intent(action: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is received."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-static {v11}, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->isSupportedFeature(S)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 50
    iget-object v8, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    if-nez v8, :cond_0

    .line 51
    new-instance v8, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    invoke-direct {v8, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mSecureDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/SecurePlaceDbHelper;

    .line 52
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    const-string v9, "Create SecureDbHelper"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    :goto_0
    const-string v8, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 62
    new-instance v5, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;

    invoke-direct {v5, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;-><init>(Landroid/content/Context;)V

    .line 64
    .local v5, "pref":Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;
    const-string v8, "restore_status"

    invoke-virtual {v5, v8, v11}, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;->getValue(Ljava/lang/String;Z)Z

    move-result v6

    .line 66
    .local v6, "prefValue":Z
    if-nez v6, :cond_1

    .line 67
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    const-string v9, "Restore at first"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v2, Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;

    invoke-direct {v2, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;-><init>(Landroid/content/Context;)V

    .line 70
    .local v2, "mydb":Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;
    invoke-virtual {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;->Restore()Z

    move-result v1

    .line 72
    .local v1, "flag":Z
    if-eqz v1, :cond_3

    .line 73
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    const-string v9, "MyPlaceDb does exist."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :goto_1
    const-string v8, "restore_status"

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;->put(Ljava/lang/String;Z)V

    .line 90
    .end local v1    # "flag":Z
    .end local v2    # "mydb":Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;
    .end local v5    # "pref":Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;
    .end local v6    # "prefValue":Z
    :cond_1
    :goto_2
    return-void

    .line 55
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    if-nez v8, :cond_0

    .line 56
    new-instance v8, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    invoke-direct {v8, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/samsung/android/intelligenceservice/useranalysis/UserAnalysisBroadcastReceiver;->mDbHelper:Lcom/samsung/android/intelligenceservice/useranalysis/db/PlaceDbHelper;

    .line 57
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    const-string v9, "Create DbHelper"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    .restart local v1    # "flag":Z
    .restart local v2    # "mydb":Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;
    .restart local v5    # "pref":Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;
    .restart local v6    # "prefValue":Z
    :cond_3
    const-string v8, "UserAnalysis.UserAnalysisBroadcastReceiver"

    const-string v9, "MyPlaceDb does not exist."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 79
    .end local v1    # "flag":Z
    .end local v2    # "mydb":Lcom/samsung/android/intelligenceservice/useranalysis/db/RestoreMyPlaceDb;
    .end local v5    # "pref":Lcom/samsung/android/intelligenceservice/useranalysis/db/MyPlaceDbPreference;
    .end local v6    # "prefValue":Z
    :cond_4
    const-string v8, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 80
    const-string v8, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v8, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    .line 81
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 82
    .local v4, "packageUri":Landroid/net/Uri;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "packageName":Ljava/lang/String;
    :goto_3
    if-eqz v3, :cond_1

    .line 85
    new-instance v7, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;

    invoke-direct {v7, p1}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;-><init>(Landroid/content/Context;)V

    .line 86
    .local v7, "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    invoke-virtual {v7, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;->cleanUserConsent(Ljava/lang/String;)V

    goto :goto_2

    .line 82
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v7    # "privacyManager":Lcom/samsung/android/intelligenceservice/useranalysis/privacy/PrivacyManager;
    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method
