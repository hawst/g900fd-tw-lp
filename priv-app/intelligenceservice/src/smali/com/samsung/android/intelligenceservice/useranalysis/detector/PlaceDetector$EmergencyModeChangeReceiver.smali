.class public Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector$EmergencyModeChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PlaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmergencyModeChangeReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 698
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 701
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 702
    .local v0, "action":Ljava/lang/String;
    const-string v2, "reason"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 703
    .local v1, "mode":I
    const-string v2, "UserAnalysis.PlaceDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 705
    invoke-static {p1}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->getInstance(Landroid/content/Context;)Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;

    move-result-object v2

    # invokes: Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->restartMonitors()V
    invoke-static {v2}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->access$800(Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;)V

    .line 706
    const-string v2, "PM"

    const-string v3, "EM"

    invoke-static {p1, v2, v3}, Lcom/samsung/android/intelligenceservice/useranalysis/util/DumpLog;->put(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_0
    return-void
.end method
