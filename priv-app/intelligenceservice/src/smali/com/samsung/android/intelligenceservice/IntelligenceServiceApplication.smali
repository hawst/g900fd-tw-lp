.class public Lcom/samsung/android/intelligenceservice/IntelligenceServiceApplication;
.super Landroid/app/Application;
.source "IntelligenceServiceApplication.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "IntelligenceServiceApplication"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 36
    const-string v0, "IntelligenceServiceApplication"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/android/intelligenceservice/useranalysis/detector/PlaceDetector;->destroy()V

    .line 50
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 51
    return-void
.end method
