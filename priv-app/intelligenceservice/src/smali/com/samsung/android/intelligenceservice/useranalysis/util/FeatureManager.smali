.class public Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;
.super Ljava/lang/Object;
.source "FeatureManager.java"


# static fields
.field public static final FEATURE_SECURE_DB:S = 0x0s

.field private static final MAX_FEATURE:S = 0x1s

.field private static final sFeatureList:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 13
    new-array v0, v2, [Z

    sput-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->sFeatureList:[Z

    .line 16
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->sFeatureList:[Z

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([ZZ)V

    .line 19
    sget-object v0, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->sFeatureList:[Z

    const/4 v1, 0x0

    aput-boolean v2, v0, v1

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isSupportedFeature(S)Z
    .locals 2
    .param p0, "feature"    # S

    .prologue
    const/4 v0, 0x1

    .line 23
    if-ltz p0, :cond_0

    if-ge p0, v0, :cond_0

    sget-object v1, Lcom/samsung/android/intelligenceservice/useranalysis/util/FeatureManager;->sFeatureList:[Z

    aget-boolean v1, v1, p0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
