.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/billing/b/f;->a:Lcom/sec/android/app/billing/b/f;

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/b/e;->a(Landroid/content/Context;Lcom/sec/android/app/billing/b/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->d(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    return-void

    :cond_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:scbStatus()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
