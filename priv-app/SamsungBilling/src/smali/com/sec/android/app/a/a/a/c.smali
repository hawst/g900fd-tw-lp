.class public final enum Lcom/sec/android/app/a/a/a/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/a/a/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/android/app/a/a/a/c;

.field public static final enum b:Lcom/sec/android/app/a/a/a/c;

.field public static final enum c:Lcom/sec/android/app/a/a/a/c;

.field public static final enum d:Lcom/sec/android/app/a/a/a/c;

.field public static final enum e:Lcom/sec/android/app/a/a/a/c;

.field private static final synthetic f:[Lcom/sec/android/app/a/a/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/app/a/a/a/c;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/a/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->a:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Lcom/sec/android/app/a/a/a/c;

    const-string v1, "BINDING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/a/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Lcom/sec/android/app/a/a/a/c;

    const-string v1, "BIND_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/a/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->c:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Lcom/sec/android/app/a/a/a/c;

    const-string v1, "BINDED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/a/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->d:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Lcom/sec/android/app/a/a/a/c;

    const-string v1, "UNBINDED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/a/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->e:Lcom/sec/android/app/a/a/a/c;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/a/a/a/c;

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->a:Lcom/sec/android/app/a/a/a/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->c:Lcom/sec/android/app/a/a/a/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->d:Lcom/sec/android/app/a/a/a/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->e:Lcom/sec/android/app/a/a/a/c;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/a/a/a/c;->f:[Lcom/sec/android/app/a/a/a/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/a/a/a/c;
    .locals 1

    const-class v0, Lcom/sec/android/app/a/a/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/a/a/a/c;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/a/a/a/c;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/app/a/a/a/c;->f:[Lcom/sec/android/app/a/a/a/c;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/a/a/a/c;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
