.class public Lcom/sec/android/app/billing/u;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/lang/String; = null

.field public static final b:Ljava/lang/String; = "UnifiedPayment"

.field public static c:Ljava/lang/String;

.field public static d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "2.01.24"

    sput-object v0, Lcom/sec/android/app/billing/u;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[UP_VERSION : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/billing/u;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/billing/u;->c:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "CREDIT_CARD_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "CREDIT_CARD"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Lcom/sec/android/app/billing/requestparam/GiftCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/GiftCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/GiftCardData;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "GIFT_CARD_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "GIFT_CARD"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "PAYMENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/app/billing/u;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    sput-object p0, Lcom/sec/android/app/billing/u;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[UP_VERSION : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/billing/u;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/billing/u;->c:Ljava/lang/String;

    return-void
.end method

.method public static b(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "CREDIT_CARD_REQUEST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "REGISTER_CREDIT_CARD"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object v1
.end method
