.class public Lcom/sec/android/app/billing/requestparam/RequestParamValidator;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static check(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/CreditCardData;
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-static {v0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/UserInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "_"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    const-string v0, "[Parameter Validator] language change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static check(Lcom/sec/android/app/billing/requestparam/DeviceInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/DeviceInfo;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    const-string v0, "[Parameter Validator] deviceID change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->language:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "_"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->language:Ljava/lang/String;

    const-string v0, "[Parameter Validator] language change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_1
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->usimType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    if-ne v1, v4, :cond_9

    const-string v1, "0"

    iput-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->usimType:Ljava/lang/String;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSimState() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v1, "[Parameter Validator] usimType change"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->msisdn:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->msisdn:Ljava/lang/String;

    const-string v1, "[Parameter Validator] msisdn change"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    const-string v1, "[Parameter Validator] osVersion change"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_4
    const-string v3, ""

    const-string v1, ""

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    if-eq v0, v4, :cond_a

    invoke-static {p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->getSimOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "getSimOperator() : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    if-eqz v2, :cond_a

    const/4 v0, 0x0

    const/4 v1, 0x3

    :try_start_0
    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x3

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    const-string v1, "[Parameter Validator] mcc change"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    const-string v0, "[Parameter Validator] mnc change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->getCscFromBuildProp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    const-string v0, "[Parameter Validator] csc change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->carrierYN:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "Y"

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->carrierYN:Ljava/lang/String;

    const-string v0, "[Parameter Validator] carrierYN change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_8
    return-object p0

    :cond_9
    const-string v1, "1"

    iput-object v1, p0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->usimType:Ljava/lang/String;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    move-object v1, v2

    goto :goto_1

    :cond_a
    move-object v0, v1

    move-object v1, v3

    goto :goto_1
.end method

.method public static check(Lcom/sec/android/app/billing/requestparam/GiftCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/GiftCardData;
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-static {v0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/UserInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->language:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "_"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->language:Ljava/lang/String;

    const-string v0, "[Parameter Validator] language change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static check(Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-static {v0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/DeviceInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-static {v0, p1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/UserInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    return-object p0
.end method

.method public static check(Lcom/sec/android/app/billing/requestparam/UserInfo;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "The value of userID is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "The value of userID is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/j;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "The value of userEmail is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "The value of userEmail is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/j;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "The value of accessToken is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "The value of accessToken is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/j;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;

    move-result-object v0

    throw v0

    :cond_2
    return-object p0
.end method

.method private static getCscFromBuildProp()Ljava/lang/String;
    .locals 2

    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ril.sales_code"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static getSimOperator(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const-string v1, ""

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static isNullOrEmpty(Ljava/lang/String;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
