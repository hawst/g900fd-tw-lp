.class Lcom/sec/android/app/billing/s;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/r;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/r;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PSMS : UNIFIEDPAYMENT_ACTION_MSG_SENT onReceive intent : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/billing/s;->getResultCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v0}, Lcom/sec/android/app/billing/r;->a(Lcom/sec/android/app/billing/r;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v0}, Lcom/sec/android/app/billing/r;->b(Lcom/sec/android/app/billing/r;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v1}, Lcom/sec/android/app/billing/r;->c(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/r;->a(Lcom/sec/android/app/billing/r;Lcom/sec/android/app/billing/s;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v0}, Lcom/sec/android/app/billing/r;->d(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/t;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/billing/s;->getResultCode()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v0}, Lcom/sec/android/app/billing/r;->d(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/billing/t;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/s;->a:Lcom/sec/android/app/billing/r;

    invoke-static {v0}, Lcom/sec/android/app/billing/r;->d(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/billing/t;->b()V

    goto :goto_0
.end method
