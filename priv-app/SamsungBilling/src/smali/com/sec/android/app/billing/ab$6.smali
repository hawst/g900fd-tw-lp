.class Lcom/sec/android/app/billing/ab$6;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/ab;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/ab;

.field private final synthetic b:Landroid/webkit/WebView;

.field private final synthetic c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/ab;Landroid/webkit/WebView;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/ab$6;->a:Lcom/sec/android/app/billing/ab;

    iput-object p2, p0, Lcom/sec/android/app/billing/ab$6;->b:Landroid/webkit/WebView;

    iput-object p3, p0, Lcom/sec/android/app/billing/ab$6;->c:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 v2, 0x3

    const-string v0, "SSLError AlertDialog HW back key press"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->a:Lcom/sec/android/app/billing/ab;

    invoke-static {v0}, Lcom/sec/android/app/billing/ab;->b(Lcom/sec/android/app/billing/ab;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->a:Lcom/sec/android/app/billing/ab;

    invoke-static {v0}, Lcom/sec/android/app/billing/ab;->b(Lcom/sec/android/app/billing/ab;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->a:Lcom/sec/android/app/billing/ab;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/ab;->a(Lcom/sec/android/app/billing/ab;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/ab$6;->a:Lcom/sec/android/app/billing/ab;

    invoke-static {v0}, Lcom/sec/android/app/billing/ab;->b(Lcom/sec/android/app/billing/ab;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/SslErrorHandler;

    invoke-virtual {v0}, Landroid/webkit/SslErrorHandler;->cancel()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
