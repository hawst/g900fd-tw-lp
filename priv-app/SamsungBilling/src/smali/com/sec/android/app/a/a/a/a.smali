.class public Lcom/sec/android/app/a/a/a/a;
.super Ljava/lang/Object;


# static fields
.field private static final d:Ljava/lang/String; = "DownloadServiceConnectionManager"

.field private static synthetic h:[I


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/sec/android/app/a/a/a/g;

.field private c:Lcom/sec/android/app/a/a/a/c;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/a/a/a/b;

.field private g:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/sec/android/app/a/a/a/c;->a:Lcom/sec/android/app/a/a/a/c;

    iput-object v0, p0, Lcom/sec/android/app/a/a/a/a;->c:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Lcom/sec/android/app/a/a/a/a$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/a/a/a/a$1;-><init>(Lcom/sec/android/app/a/a/a/a;)V

    iput-object v0, p0, Lcom/sec/android/app/a/a/a/a;->g:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/sec/android/app/a/a/a/a;->e:Landroid/content/Context;

    const-string v0, "com.sec.android.app.samsungapps.downloadservice.SamsungAppsDownloadService"

    iput-object v0, p0, Lcom/sec/android/app/a/a/a/a;->a:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/os/IBinder;)V
    .locals 2

    invoke-static {p1}, Lcom/sec/android/app/a/a/a/h;->a(Landroid/os/IBinder;)Lcom/sec/android/app/a/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/a/a/a/a;->b:Lcom/sec/android/app/a/a/a/g;

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->d:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/app/a/a/a/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->j()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/a/a/a/a;Landroid/os/IBinder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/a/a/a/a;->a(Landroid/os/IBinder;)V

    return-void
.end method

.method private final a(Lcom/sec/android/app/a/a/a/c;)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/a/a/a/a;->c()V

    iput-object p1, p0, Lcom/sec/android/app/a/a/a/a;->c:Lcom/sec/android/app/a/a/a/c;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DownloadServiceConnectionManager - mState : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/a/a/a/a;->d()V

    return-void
.end method

.method static synthetic h()[I
    .locals 3

    sget-object v0, Lcom/sec/android/app/a/a/a/a;->h:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/a/a/a/c;->values()[Lcom/sec/android/app/a/a/a/c;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/app/a/a/a/c;->d:Lcom/sec/android/app/a/a/a/c;

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/app/a/a/a/c;->c:Lcom/sec/android/app/a/a/a/c;

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/app/a/a/a/c;->a:Lcom/sec/android/app/a/a/a/c;

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/app/a/a/a/c;->e:Lcom/sec/android/app/a/a/a/c;

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/app/a/a/a/a;->h:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private i()Lcom/sec/android/app/a/a/a/c;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->c:Lcom/sec/android/app/a/a/a/c;

    return-object v0
.end method

.method private j()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/a/a/a/a;->b:Lcom/sec/android/app/a/a/a/g;

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->e:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->c:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private k()V
    .locals 4

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/a/a/a/a;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/a/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/a/a/a/a;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->l()V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->l()V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->c:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private m()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    invoke-interface {v0}, Lcom/sec/android/app/a/a/a/b;->a()V

    :cond_0
    return-void
.end method

.method private n()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    invoke-interface {v0}, Lcom/sec/android/app/a/a/a/b;->c()V

    :cond_0
    return-void
.end method

.method private o()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    invoke-interface {v0}, Lcom/sec/android/app/a/a/a/b;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/android/app/a/a/a/c;->b:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/sec/android/app/a/a/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/a/a/a/a;->f:Lcom/sec/android/app/a/a/a/b;

    return-void
.end method

.method public b()V
    .locals 2

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/a/a/a/a;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    sget-object v0, Lcom/sec/android/app/a/a/a/c;->e:Lcom/sec/android/app/a/a/a/c;

    invoke-direct {p0, v0}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/c;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected c()V
    .locals 2

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected d()V
    .locals 2

    invoke-static {}, Lcom/sec/android/app/a/a/a/a;->h()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/a/a/a/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->k()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->o()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->n()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->m()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected e()Lcom/sec/android/app/a/a/a/c;
    .locals 1

    sget-object v0, Lcom/sec/android/app/a/a/a/c;->a:Lcom/sec/android/app/a/a/a/c;

    return-object v0
.end method

.method public f()Lcom/sec/android/app/a/a/a/g;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/a/a/a/a;->b:Lcom/sec/android/app/a/a/a/g;

    return-object v0
.end method

.method public g()Z
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/a;->i()Lcom/sec/android/app/a/a/a/c;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/a/a/a/c;->d:Lcom/sec/android/app/a/a/a/c;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
