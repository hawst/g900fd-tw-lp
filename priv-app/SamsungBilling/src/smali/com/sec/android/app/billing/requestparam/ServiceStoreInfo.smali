.class public Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

.field public country:Ljava/lang/String;

.field public serviceURL:Ljava/lang/String;

.field public telNoForCS:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;
    .locals 2

    new-instance v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->telNoForCS:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->serviceURL:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->telNoForCS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->serviceURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
