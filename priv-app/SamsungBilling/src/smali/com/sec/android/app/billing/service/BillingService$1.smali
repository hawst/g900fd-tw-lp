.class Lcom/sec/android/app/billing/service/BillingService$1;
.super Lcom/sec/android/app/billing/service/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/service/BillingService;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/service/BillingService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/service/BillingService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-direct {p0}, Lcom/sec/android/app/billing/service/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BillingService : requestBilling : apiVersion = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestData = ***, etc = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;Landroid/os/Bundle;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    iget-object v1, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/billing/service/BillingService;->a(ILandroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    new-instance v1, Lcom/sec/android/app/billing/service/BillingVO;

    invoke-direct {v1}, Lcom/sec/android/app/billing/service/BillingVO;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;Lcom/sec/android/app/billing/service/BillingVO;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    iput p1, v0, Lcom/sec/android/app/billing/service/BillingVO;->a:I

    const-string v0, "PACKAGE_NAME"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    iput-object v1, v0, Lcom/sec/android/app/billing/service/BillingVO;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/billing/service/BillingService;->a(Landroid/os/Bundle;)I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    iput v2, v0, Lcom/sec/android/app/billing/service/BillingVO;->c:I

    const-string v0, "REQUEST_ACTION"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    iput-object v3, v0, Lcom/sec/android/app/billing/service/BillingVO;->d:Ljava/lang/String;

    const-string v0, "PAYMENT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iput-object v0, v4, Lcom/sec/android/app/billing/service/BillingVO;->g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    const-string v4, ""

    iput-object v4, v0, Lcom/sec/android/app/billing/service/BillingVO;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v0

    iput-object p3, v0, Lcom/sec/android/app/billing/service/BillingVO;->f:Ljava/lang/String;

    const-string v0, "SILENT_INSTALL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0, v3}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    const-string v0, "CREDIT_CARD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, v4, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BillingService : requestBilling() : [AIDL_SENDING_RESULT_ERROR] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v1}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_CODE"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v1}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    :try_start_1
    const-string v0, "GIFT_CARD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "GIFT_CARD_REQUEST"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iput-object v0, v4, Lcom/sec/android/app/billing/service/BillingVO;->i:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "GIFT_CARD_REQUEST"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    const-string v0, "REGISTER_CREDIT_CARD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, v4, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v0}, Lcom/sec/android/app/billing/service/BillingService;->a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService$1;->a:Lcom/sec/android/app/billing/service/BillingService;

    invoke-static {v2}, Lcom/sec/android/app/billing/service/BillingService;->b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method
