.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->telefonicaPayStart(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v5, 0x0

    const/16 v13, 0xc8

    const/4 v2, 0x0

    const/4 v6, 0x1

    :try_start_0
    new-instance v8, Ljava/net/URL;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->b:Ljava/lang/String;

    invoke-direct {v8, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "telefonicaPay proxyAddress : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "telefonicaPay proxyPort : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    move v1, v2

    :goto_0
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/net/Proxy;

    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, v9, v10}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v3, v4}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    invoke-virtual {v8, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    :goto_1
    if-eqz v0, :cond_e

    const-string v3, "Connection"

    const-string v4, "close"

    invoke-virtual {v0, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "[TELEFONICA] setFollowRedirects"

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    const/16 v3, 0x4e20

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const v3, 0xea60

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TELEFONICA] TELEFONICA_AUTHURL ===> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TELEFONICA] connection.getResponseCode() ===> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v3, "location"

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "[TELEFONICA] LOCATION_SERVICE ===> "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_2
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    if-ne v3, v13, :cond_8

    new-instance v7, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    const-string v11, "UTF-8"

    invoke-direct {v3, v4, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v7, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v3, v5

    :goto_3
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V

    move v7, v2

    move v4, v6

    :goto_4
    if-eqz v7, :cond_1

    const-string v7, "location"

    invoke-virtual {v0, v7}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "Set-Cookie"

    invoke-virtual {v0, v11}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const-string v12, "Cookie"

    invoke-virtual {v0, v12, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[TELEFONICA] Redirect to URL : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "[TELEFONICA] Redirect connection.getResponseCode() ===> "

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    if-ne v7, v13, :cond_0

    move v4, v6

    :cond_0
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    :goto_5
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_a

    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v12, "[TELEFONICA] Redirect URL Content : "

    invoke-direct {v7, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_1
    move v14, v4

    move-object v4, v0

    move v0, v14

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    move v4, v0

    :goto_6
    add-int/lit8 v0, v1, 0x1

    if-nez v3, :cond_c

    const-string v1, ""

    :goto_7
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "authUrl connection cnt : "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", result : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->e(Ljava/lang/String;)V

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    if-eqz v4, :cond_b

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/billing/b/f;->a:Lcom/sec/android/app/billing/b/f;

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/b/e;->a(Landroid/content/Context;Lcom/sec/android/app/billing/b/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_8
    return-void

    :cond_4
    :try_start_1
    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    goto/16 :goto_1

    :cond_5
    const-string v3, "[TELEFONICA] LOCATION_SERVICE ===> location is null"

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_8

    :cond_6
    if-nez v3, :cond_7

    move-object v3, v4

    goto/16 :goto_3

    :cond_7
    :try_start_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v11, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :cond_8
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v4, 0x12e

    if-eq v3, v4, :cond_9

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_9

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_d

    :cond_9
    move v7, v6

    move v4, v2

    move-object v3, v5

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_5

    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_8

    :cond_b
    const-wide/16 v3, 0x3e8

    :try_start_5
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v1, v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_8

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    throw v0

    :cond_c
    move-object v1, v3

    goto/16 :goto_7

    :cond_d
    move v7, v2

    move v4, v2

    move-object v3, v5

    goto/16 :goto_4

    :cond_e
    move v4, v2

    move-object v3, v5

    goto/16 :goto_6
.end method
