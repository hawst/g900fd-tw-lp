.class public Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;
.super Landroid/app/Activity;


# static fields
.field private static Z:Ljava/lang/String; = null

.field public static final a:I = 0x1

.field private static final ad:I = 0x5

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field public static e:Ljava/lang/String; = null

.field private static final o:Ljava/lang/String; = "UnifiedPaymentMainActivity"

.field private static final p:Ljava/lang/String; = "PACKAGE_NAME"

.field private static final q:Ljava/lang/String; = "REQUEST_CODE"

.field private static final r:Ljava/lang/String; = "com.sec.android.app.billing.action.ACCOUNT"

.field private static final s:Ljava/lang/String; = "APP"

.field private static final t:Ljava/lang/String; = "WEB"

.field private static final u:Ljava/lang/String; = "com.osp.app.signin"

.field private static final v:I = 0x32f4


# instance fields
.field private A:Landroid/app/AlertDialog;

.field private B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

.field private C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

.field private D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

.field private E:Landroid/content/BroadcastReceiver;

.field private F:Lcom/sec/android/app/billing/ab;

.field private G:Ljava/lang/String;

.field private H:Ljava/lang/String;

.field private I:Ljava/lang/String;

.field private J:Ljava/lang/String;

.field private K:Ljava/lang/String;

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Ljava/lang/String;

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private Y:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aa:I

.field private ab:I

.field private ac:I

.field private ae:Landroid/hardware/input/InputManager;

.field private af:Landroid/util/SparseBooleanArray;

.field private ag:Lcom/sec/android/app/billing/v;

.field private ah:Z

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field g:Landroid/os/Handler;

.field h:Ljava/lang/Runnable;

.field i:Ljava/lang/Runnable;

.field j:Ljava/lang/Runnable;

.field k:Ljava/lang/Runnable;

.field l:Ljava/lang/Runnable;

.field m:Ljava/lang/Runnable;

.field n:Ljava/lang/Runnable;

.field private w:Landroid/webkit/WebView;

.field private x:Landroid/app/ProgressDialog;

.field private y:Landroid/widget/LinearLayout;

.field private z:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->E:Landroid/content/BroadcastReceiver;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->H:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->K:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Q:Z

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->R:Z

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->S:Z

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->T:Z

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->U:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->V:Z

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->W:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    iput-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$11;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$12;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$13;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->j:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$14;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->l:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$16;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m:Ljava/lang/Runnable;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$17;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic A(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/hardware/input/InputManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    return-object v0
.end method

.method static synthetic B(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/util/SparseBooleanArray;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->af:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "makePGPaymentActivityIntent pgType : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pgUrl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pgRequest : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "UNIFIED_PAYMENT_PG_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "UNIFIED_PAYMENT_PG_URL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "UNIFIED_PAYMENT_PG_REQUEST"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "IS_UX_V2"

    iget-boolean v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->W:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "DISPLAY_TYPE"

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/billing/requestparam/InicisRequestData;
    .locals 1

    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/billing/requestparam/InicisRequestData;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/billing/requestparam/InicisRequestData;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/InicisRequestData;-><init>()V

    iput-object p1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_MID:Ljava/lang/String;

    iput-object p2, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_OID:Ljava/lang/String;

    iput-object p4, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_AMT:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/b/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_UNAME:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/b/b;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_EMAIL:Ljava/lang/String;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paymentID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",requestID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",paymentMethod=kpi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NOTI:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "up-web/kcc_cb.do"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NEXT_URL:Ljava/lang/String;

    invoke-static {p6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RETURN_URL:Ljava/lang/String;

    invoke-static {p5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NOTI_URL:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/sec/android/app/billing/b/b;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_GOODS:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_OFFER_PERIOD:Ljava/lang/String;

    :cond_1
    const-string v1, "1"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_MOBILE:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "KTF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45002"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45004"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45008"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    const-string v1, "KTF"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&hpp_noauth=Y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "4"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_3
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_EMAIL:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v3, "SKTelecom"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "45003"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "45005"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "45011"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    const-string v1, "SKT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&hpp_noauth=Y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "4"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v3, "LGT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "45006"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    const-string v1, "LGT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "1"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string v1, "nexturl=get"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "1"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_1

    :cond_9
    const-string v1, "nexturl=get"

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->y:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Q:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showSslAlertDialog baseUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000a

    new-instance v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$6;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$7;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic a(I)Z
    .locals 1

    invoke-static {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->b(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->U:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->R:Z

    return-void
.end method

.method private static b(I)Z
    .locals 1

    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    return-object v0
.end method

.method private c()V
    .locals 6

    const/4 v5, 0x3

    const/4 v3, 0x0

    const-string v0, "PAYMENT"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "REQUEST_CODE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : PAYMENT : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "REQUEST_CODE"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/service/BillingVO;

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/sec/android/app/billing/service/BillingVO;->g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/sec/android/app/billing/service/BillingVO;->g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : PAYMENT : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is valid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : PAYMENT : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "The json data is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    :cond_1
    const-string v0, "CREDIT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "REQUEST_CODE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : CREDIT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "REQUEST_CODE"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/service/BillingVO;

    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : CREDIT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is valid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : CREDIT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "The json data is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    const-string v0, "GIFT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "REQUEST_CODE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : GIFT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "REQUEST_CODE"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/service/BillingVO;

    if-eqz v0, :cond_4

    iget-object v3, v0, Lcom/sec/android/app/billing/service/BillingVO;->i:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    if-eqz v3, :cond_4

    iget-object v0, v0, Lcom/sec/android/app/billing/service/BillingVO;->i:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : GIFT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is valid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate() : GIFT_CARD : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "The json data is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto/16 :goto_0

    :cond_5
    const-string v0, "onCreate() : action is invalid. finish()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->L:Ljava/lang/String;

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->S:Z

    return-void
.end method

.method static synthetic c(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Z:Ljava/lang/String;

    return-void
.end method

.method private d()V
    .locals 4

    const v3, 0x7f030003

    const v2, 0x7f030002

    const-string v0, "PAYMENT"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "v1"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->W:Z

    invoke-virtual {p0, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto :goto_0

    :cond_1
    const-string v0, "CREDIT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "v1"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto :goto_0

    :cond_4
    const-string v0, "GIFT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, "v1"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setContentView(I)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "onCreate() : action is invalid. finish()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q()V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->M:Ljava/lang/String;

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->T:Z

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "install ok"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/a/b;

    invoke-direct {v0}, Lcom/sec/android/app/billing/a/b;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    const/16 v2, 0x7a

    invoke-virtual {v0, p1, v1, v2, p0}, Lcom/sec/android/app/billing/a/b;->a(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "not installed"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$18;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    const-string v0, "[Alipay] reInit()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "setUPJsonToRequestVO UP JSON does not set"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "PAYMENT"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "UPVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "PAYMENT : [LIB ver] UPJson Library version change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "CURRENCY"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "CURRENCY"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "CURRENCY"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->currency:Ljava/lang/String;

    const-string v0, "PAYMENT : UPJson COUNTRY change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v0, "PAYMENT : UPJson CURRENCY change !! "

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT : UnifiedPaymentData.appServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.serviceStoreInfo.country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.productInfo.currency : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.libraryVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MCC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    const-string v0, "PAYMENT : UPJson MCC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MNC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MNC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    const-string v0, "PAYMENT : UPJson MNC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT : [LIB ver] Library version change !! "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT : UnifiedPaymentData.appServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.serviceStoreInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.productInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.libraryVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    const-string v0, "CREDIT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "UPVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "CREDIT_CARD : [LIB ver] UPJson Library version change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    const-string v0, "CREDIT_CARD : UPJson COUNTRY change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREDIT_CARD : CreditCardData.appServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CreditCardData.country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CreditCardData.libraryVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MCC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    const-string v0, "CREDIT_CARD : UPJson MCC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MNC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MNC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    const-string v0, "CREDIT_CARD : UPJson MNC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    if-eqz v0, :cond_9

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREDIT_CARD : [LIB ver] Library version change !! "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    const-string v0, "GIFT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->country:Ljava/lang/String;

    const-string v0, "GIFT_CARD : UPJson COUNTRY change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GIFT_CARD : GiftCardData.appServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GiftCardData.country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MCC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    const-string v0, "GIFT_CARD : UPJson MCC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "MNC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v2, "MNC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    const-string v0, "GIFT_CARD : UPJson MNC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->N:Ljava/lang/String;

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->V:Z

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Q:Z

    return v0
.end method

.method private f()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    const v1, 0x7f070003

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->y:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->P:Ljava/lang/String;

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->R:Z

    return v0
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x1

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->c(Landroid/content/Context;)Z

    move-result v1

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "UP Android Version : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getAndroidVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/ab;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    invoke-direct {v0, v2, v1}, Lcom/sec/android/app/billing/ab;-><init>(Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->F:Lcom/sec/android/app/billing/ab;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->F:Lcom/sec/android/app/billing/ab;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/billing/aa;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/billing/aa;-><init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    const-wide/32 v1, 0x800000

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    sget-object v1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->H:Ljava/lang/String;

    return-void
.end method

.method static synthetic g(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->S:Z

    return v0
.end method

.method private h()V
    .locals 8

    const-wide/32 v0, 0x800000

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "http"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Local Cache DIR : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :try_start_0
    const-string v3, "android.net.http.HttpResponseCache"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "install"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/io/File;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->K:Ljava/lang/String;

    return-void
.end method

.method static synthetic h(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->T:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x156

    const v4, 0x3f733333    # 0.95f

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-static {}, Lcom/sec/android/app/billing/b/b;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    const-string v0, "[Parameter Validator] timeOffset change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    if-eqz v0, :cond_4

    const-string v0, "N"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    if-eqz v0, :cond_4

    const-string v0, "KOR"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "confirmPassword start"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    new-instance v1, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPServerURL"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "PAYMENT : UPJson UPServerURL change !!"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->J:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "payment.do"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PAYMENT : UnifiedPaymentData.DeviceInfo.displayType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, "v1"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "tablet_payment.do"

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Old UX] V1 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paymentPath : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/billing/w;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/app/billing/w;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    if-eqz v0, :cond_1

    const-string v0, "CHN"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    const-string v1, "PAYMENT : UnifiedPaymentData.DeviceInfo : null"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    const-string v0, "payment.do"

    goto/16 :goto_3

    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_a
    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] default V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    const-string v1, "UnifiedPaymentMainActivity callPaymentPage() : Configuration.ORIENTATION_PORTRAIT : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/16 v2, 0x189

    invoke-static {p0, v1, v5, v2, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_c
    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->y:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_a

    const-string v1, "UnifiedPaymentMainActivity callPaymentPage() : mLLProgressDialog != null : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->y:Landroid/widget/LinearLayout;

    invoke-static {p0, v1, v5, v6, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto :goto_5

    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_c

    const-string v1, "UnifiedPaymentMainActivity callPaymentPage() : Configuration.ORIENTATION_LANDSCAPE : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/16 v2, 0x14e

    invoke-static {p0, v1, v5, v2, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto :goto_6
.end method

.method static synthetic j(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    return-object v0
.end method

.method private j()V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {}, Lcom/sec/android/app/billing/b/b;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    const-string v0, "[Parameter Validator] timeOffset change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/billing/b/b;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CREDIT_CARD : storeRequestID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPServerURL"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "CREDIT_CARD : UPJson UPServerURL change !!"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->J:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "creditcard.do"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, "v1"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "tablet_creditcard.do"

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Old UX] V1 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "creditCardPath : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/billing/w;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/app/billing/w;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "CHN"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string v0, "creditcard.do"

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] default V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method static synthetic k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    return-object v0
.end method

.method private k()V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-static {}, Lcom/sec/android/app/billing/b/b;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->timeOffset:Ljava/lang/String;

    const-string v0, "[Parameter Validator] timeOffset change"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    const-string v1, "UPServerURL"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "GIFT_CARD : UPJson UPServerURL change !!"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->I:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->J:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "giftcard.do"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, "v1"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "tablet_giftcard.do"

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Old UX] V1 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "giftCardPath : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/billing/w;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/app/billing/w;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "CHN"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    const-string v0, "giftcard.do"

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] default V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method static synthetic l(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/CreditCardData;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    return-object v0
.end method

.method private l()V
    .locals 7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ag:Lcom/sec/android/app/billing/v;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    aget v3, v1, v0

    invoke-virtual {v2, v3}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getId()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getSources()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->b(I)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->af:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseBooleanArray;->append(IZ)V

    if-eqz v4, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity :  mIsJoystickAdded = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity :  ids["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v1, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", inputDeviceId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", isJoystick = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", inputDevice.getName() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v2, "javascript:addedGamePad()"

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic m(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic n(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->s()V

    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic o(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 5

    const-string v1, "WEB"

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.osp.app.signin"

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v2, 0x32f4

    if-lt v0, v2, :cond_2

    const-string v1, "APP"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "APP"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "APP"
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSamsungAccountType() : result : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-object v0

    :cond_1
    :try_start_2
    const-string v1, "WEB"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "WEB"
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "samsungAccountType error NameNotFoundException message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "samsungAccountType error message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto/16 :goto_0
.end method

.method static synthetic p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 4

    const-string v0, "UnifiedPaymentMainActivity : showUpdateAlertDialog()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f06001e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060010

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$8;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$8;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/update/e;->b(Landroid/content/Context;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "version : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    const-string v1, "needToUpdate true"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    move v1, v0

    :goto_1
    if-eqz v1, :cond_2

    const v0, 0x7f060011

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v3, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$9;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$9;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$10;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$10;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const-string v1, "needToUpdate false"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    move v1, v0

    goto :goto_1

    :cond_2
    const v0, 0x7f06001f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->C:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->D:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method static synthetic s(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    return v0
.end method

.method static synthetic t(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->E:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic v(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic w(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->L:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->M:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->P:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "https://mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v0, "PAYMENT"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "CREDIT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->j()V

    goto :goto_0

    :cond_1
    const-string v0, "GIFT_CARD"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k()V

    goto :goto_0

    :cond_2
    const-string v0, "onCreate() : action is invalid. finish()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdToPrt() : mUPServerUrl before: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    const-string v2, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v0, "PAYMENT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    const-string v2, "mop.samsungosp.com"

    const-string v3, "stg-mop.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdToPrt() : mUPServerUrl after: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdAndPrtToStg2() : mUPServerUrl before: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    const-string v2, "stg-api.samsungosp.com"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "stg-mop.samsungosp.com"

    const-string v2, "stg-api.samsungosp.com"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    const-string v1, "cn-mop.samsungosp.com"

    const-string v2, "stg-api.samsungosp.com"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdAndPrtToStg2() : mUPServerUrl after1: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdAndPrtToStg2() : action: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "PAYMENT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPrdAndPrtToStg2() : getGiftCardnCouponURL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    :cond_4
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    :cond_5
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdAndPrtToStg2() : getGiftCardnCouponURL result: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    :cond_8
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    :cond_9
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    :cond_b
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    :cond_c
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    :cond_e
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    :cond_f
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    :cond_11
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    :cond_12
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    :cond_13
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    const-string v1, "mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    const-string v3, "mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    :cond_14
    const-string v1, "stg-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    const-string v3, "stg-mop.samsungosp.com"

    const-string v4, "stg-api.samsungosp.com"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    :cond_15
    const-string v1, "cn-mop.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    const-string v2, "cn-mop.samsungosp.com"

    const-string v3, "stg-api.samsungosp.com"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/BillingInterfaceURL;->paramURL:Ljava/lang/String;

    :cond_16
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPrdToPrt() : mUPServerUrl after2: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_17
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : mIsJoystickAdded = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ah:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : e.getKeyCode() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x13

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x60

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x61

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x63

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6d

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6c

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->aa:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ab:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : isLongPress : dispatchKeyEventCheckCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    const-string v1, "@@@@@ dispatchKeyEvent() : isLongPress : return true"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I

    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->V:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "@@@@@ dispatchKeyEvent() : e.getAction() == KeyEvent.ACTION_DOWN"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:dispatchKeyEvent("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->aa:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ab:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ac:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public finish()V
    .locals 1

    const-string v0, "UnifiedPaymentMainActivity - finish"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->x:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->z:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->A:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_2
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentMainActivity onActivityResult : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/16 v1, 0x66

    if-ne p1, v1, :cond_1b

    if-ne p2, v5, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x3

    if-ne p2, v1, :cond_2

    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    if-ne p2, v1, :cond_3

    const-string v0, "ResultCode is ResultPending"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-ne p2, v6, :cond_4

    const-string v0, "payment result canceled"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-nez p2, :cond_6

    sget-object v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ""

    sget-object v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "ISP payment not reInit"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v0, "ISP payment ResultCode is ResultCanceled"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x5

    if-ne p2, v1, :cond_7

    const-string v0, "Japan Webmoney Status Checking....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:jwmComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const/16 v1, 0x10

    if-ne p2, v1, :cond_8

    const-string v0, "Japan danal docomo Status Checking....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:jwdComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    const/4 v1, 0x6

    if-ne p2, v1, :cond_b

    const-string v1, "WebMoney or NEW SINA payment is fail"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz p3, :cond_9

    const-string v0, "FAIL_PAYMENT_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_9
    if-eqz v0, :cond_a

    const-string v1, "GCB06"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$4;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:reInitOnFailPayment()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x7

    if-ne p2, v0, :cond_c

    const-string v0, "China NEW SINA Status Checking....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:csmComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const/16 v0, 0x8

    if-ne p2, v0, :cond_d

    const-string v0, "Russia Qiwi Status Checking....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:rqwComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const/16 v0, 0x9

    if-ne p2, v0, :cond_10

    const-string v0, "LiveGamer Complete call....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, ""

    if-eqz p3, :cond_e

    const-string v0, "SLG_PAYMENT_METHOD"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SLG paymentMethod : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v1, "TRM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:slgStatus()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:slgComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const/16 v0, 0xa

    if-ne p2, v0, :cond_11

    const-string v0, "UPoint retrieve call..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:checkUpointBalance()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const/16 v0, 0xb

    if-ne p2, v0, :cond_13

    const-string v0, "GPP Complete call..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, ""

    if-eqz p3, :cond_12

    const-string v0, "PAYER_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_12
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GPP payerID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callGppComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    const/16 v0, 0xc

    if-ne p2, v0, :cond_15

    const-string v0, "KCC Complete call..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, ""

    if-eqz p3, :cond_14

    const-string v0, "KCC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_14
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KCC QueryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callEncodeKccComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    const/16 v0, 0xd

    if-ne p2, v0, :cond_17

    const-string v0, "MRC Complete call..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, ""

    if-eqz p3, :cond_16

    const-string v0, "MRC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_16
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MRC queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callEncodeKccMrcComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    const/16 v0, 0xe

    if-ne p2, v0, :cond_19

    const-string v0, "ICC Register complete..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, ""

    if-eqz p3, :cond_18

    const-string v0, "ICC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_18
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MRC queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callIccRegister(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    const/16 v0, 0x11

    if-ne p2, v0, :cond_1a

    const-string v0, "WCB Complete....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:wcbComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    const/16 v0, 0x12

    if-ne p2, v0, :cond_0

    const-string v0, "GCB06 Status check....."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$5;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_1b
    const/16 v0, 0x64

    if-ne p1, v0, :cond_22

    if-ne p2, v3, :cond_1f

    if-eqz p3, :cond_1e

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    const-string v1, "userid"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "userid"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    :cond_1c
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->X:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    const-string v1, ""

    const-string v2, "Does not match Samsung Account information...."

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1d
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_1

    :cond_1e
    const-string v0, "data is null..."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_1f
    if-nez p2, :cond_20

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_1

    :cond_20
    if-eqz p3, :cond_21

    const-string v0, "SAC_0105"

    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SamsungAccount] error_code : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f(Ljava/lang/String;)V

    goto :goto_1

    :cond_21
    const-string v0, "[SamsungAccount] confirmPassword error"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_1

    :cond_22
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_25

    if-ne p2, v3, :cond_23

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onActivityResult() : requestCode == REQUEST_ID_ACCOUNT_VERIFY_WEB : resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_23
    if-nez p2, :cond_24

    const-string v0, "onActivityResult() : requestCode == REQUEST_ID_ACCOUNT_VERIFY_WEB : resultCode is RESULT_CANCELED"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_2

    :cond_24
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_2

    :cond_25
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    if-ne p2, v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:getCreditCard()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    const/16 v3, 0x156

    const v2, 0x3f733333    # 0.95f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity onConfigurationChanged() : newConfig.orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PAYMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "v1"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->B:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const-string v0, "UnifiedPaymentMainActivity onConfigurationChanged() : Configuration.ORIENTATION_PORTRAIT : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/16 v1, 0x189

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const-string v0, "UnifiedPaymentMainActivity onConfigurationChanged() : Configuration.ORIENTATION_LANDSCAPE : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const/16 v1, 0x14e

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->U:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e:Ljava/lang/String;

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UP Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    const-string v0, "UnifiedPaymentMainActivity - onCreate()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity - onCreate() : getResources().getConfiguration().orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->af:Landroid/util/SparseBooleanArray;

    new-instance v0, Lcom/sec/android/app/billing/v;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/v;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ag:Lcom/sec/android/app/billing/v;

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f()V

    invoke-static {}, Lcom/sec/android/app/billing/b/b;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->Y:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onCreate() : action = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->d()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g()V

    const-string v0, "up.updateNo.mode"

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/k;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Landroid/os/Handler;)V

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "UnifiedPaymentMainActivity - onDestroy"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->U:Z

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->E:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "java.lang.IllegalArgumentException: Receiver not registered."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity onKeyDown KEYCODE_BACK mBackKeyFlag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->V:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->V:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "UnifiedPaymentMainActivity WebView goBack()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "ISP_SUCCESS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UnifiedPaymentMainActivity onNewIntent() : ISP SUCCESS"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->w:Landroid/webkit/WebView;

    const-string v1, "javascript:ispComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "UnifiedPaymentMainActivity onNewIntent() : intent is null."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "UnifiedPaymentMainActivity - onPause"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ae:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->ag:Lcom/sec/android/app/billing/v;

    invoke-virtual {v0, v1}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "UnifiedPaymentMainActivity - onResume"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->E:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->l()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "UnifiedPaymentMainActivity - onStop"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method
