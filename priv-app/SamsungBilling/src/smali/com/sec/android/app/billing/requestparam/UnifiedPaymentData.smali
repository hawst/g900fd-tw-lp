.class public Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appServiceID:Ljava/lang/String;

.field public billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

.field public deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

.field public libraryVersion:Ljava/lang/String;

.field public paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

.field private platformCode:Ljava/lang/String;

.field public productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

.field public samsungAccountType:Ljava/lang/String;

.field public sandBoxData:Lcom/sec/android/app/billing/requestparam/SandBoxData;

.field public serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

.field public signatureInfo:Lcom/sec/android/app/billing/requestparam/SignatureInfo;

.field public storeRequestID:Ljava/lang/String;

.field public subscriptionInfo:Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

.field public timeOffset:Ljava/lang/String;

.field public userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

.field public uxVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "A"

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;
    .locals 2

    new-instance v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/ProductInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/UserInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    const-class v0, Lcom/sec/android/app/billing/requestparam/SignatureInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/SignatureInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/requestparam/SignatureInfo;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->subscriptionInfo:Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/SandBoxData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/SandBoxData;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->sandBoxData:Lcom/sec/android/app/billing/requestparam/SandBoxData;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/requestparam/SignatureInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->subscriptionInfo:Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->sandBoxData:Lcom/sec/android/app/billing/requestparam/SandBoxData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->samsungAccountType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
