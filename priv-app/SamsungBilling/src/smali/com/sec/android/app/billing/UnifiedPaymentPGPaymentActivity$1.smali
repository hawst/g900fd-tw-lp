.class Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity Handler what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/high16 v2, 0x7f080000

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;Landroid/widget/LinearLayout;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b:Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0xa

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_a
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_1

    const-string v1, "SLG_PAYMENT_METHOD"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_b
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "PAYER_ID"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_c
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "KCC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_d
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "MRC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xd

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_f
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_2

    const-string v1, "FAIL_PAYMENT_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_10
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ICC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :pswitch_12
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_12
    .end packed-switch
.end method
