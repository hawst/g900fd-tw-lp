.class Lcom/sec/android/app/billing/update/UpdateActivity$5;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/a/a/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/update/UpdateActivity;->e()V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/update/UpdateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    const-string v0, "UpdateActivity : onDisconnected"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 2

    const-string v0, "UpdateActivity : onConnectionFailed"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(I)V

    return-void
.end method

.method public c()V
    .locals 3

    const-string v0, "UpdateActivity : onConnected"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->e(Lcom/sec/android/app/billing/update/UpdateActivity;)Lcom/sec/android/app/a/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/a/a/a/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/billing/update/UpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "UpdateActivity : onStartDownloadByPackageName"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->f(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const-string v0, "UpdateActivity : startDownloadByPackageName exception"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "UpdateActivity : AIDL exception"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$5;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(I)V

    goto :goto_0
.end method
