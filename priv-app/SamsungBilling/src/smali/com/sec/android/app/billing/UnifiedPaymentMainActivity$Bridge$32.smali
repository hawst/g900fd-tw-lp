.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->iccRegisterCard(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f:Ljava/util/ArrayList;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Ljava/util/ArrayList;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[iccPay] checkPGURL is fasle : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    const-string v1, "1000"

    const-string v2, "Iran PG url validation error"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[iccPay] iccUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->b:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[iccPay] tokenUrl : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    const-string v3, "ICC"

    invoke-virtual {v1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1
.end method
