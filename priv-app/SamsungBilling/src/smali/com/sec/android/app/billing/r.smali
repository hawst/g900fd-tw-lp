.class public Lcom/sec/android/app/billing/r;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/telephony/SmsManager;

.field private e:Landroid/app/PendingIntent;

.field private final f:Ljava/lang/String;

.field private g:Lcom/sec/android/app/billing/s;

.field private h:Lcom/sec/android/app/billing/t;

.field private i:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/r;->d:Landroid/telephony/SmsManager;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/billing/r;->e:Landroid/app/PendingIntent;

    const-string v0, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/sec/android/app/billing/r;->f:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/android/app/billing/r;->i:Landroid/content/Context;

    iput-object p2, p0, Lcom/sec/android/app/billing/r;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/billing/r;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/billing/r;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/r;)Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/r;Lcom/sec/android/app/billing/s;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/r;->g:Lcom/sec/android/app/billing/s;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/r;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/s;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->g:Lcom/sec/android/app/billing/s;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/billing/r;)Lcom/sec/android/app/billing/t;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->h:Lcom/sec/android/app/billing/t;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->i:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/r;->e:Landroid/app/PendingIntent;

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->e:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->g:Lcom/sec/android/app/billing/s;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/billing/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/s;-><init>(Lcom/sec/android/app/billing/r;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/r;->g:Lcom/sec/android/app/billing/s;

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/billing/r;->g:Lcom/sec/android/app/billing/s;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    :try_start_0
    const-string v0, "SMSManager send : start smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->d:Landroid/telephony/SmsManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/r;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/r;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/billing/r;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/billing/r;->e:Landroid/app/PendingIntent;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, "SMSManager send : fail smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->h:Lcom/sec/android/app/billing/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/r;->h:Lcom/sec/android/app/billing/t;

    invoke-interface {v0}, Lcom/sec/android/app/billing/t;->b()V

    goto :goto_0
.end method

.method public a(Lcom/sec/android/app/billing/t;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/r;->h:Lcom/sec/android/app/billing/t;

    return-void
.end method
