.class public Lcom/sec/android/app/billing/z;
.super Lcom/sec/android/app/billing/y;


# static fields
.field private static final c:Ljava/lang/String; = "[Bridge Call for CreditCardActivity] "


# instance fields
.field a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

.field private b:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/sec/android/app/billing/CreditCardActivity$Bridge;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/y;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/z;->b:Landroid/webkit/WebView;

    iput-object p3, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    const-string v0, "** UnifiedPaymentWebChromeClientNoJSForCreditCardActivity **"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Bridge Call for CreditCardActivity] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Bridge Call for CreditCardActivity] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    const-string v0, "** Android native call(by javascript) for CreditCardActivity **"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " 1. view ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " 2. url ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " 3. message ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " 4. defaultValue ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " 5. result ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    if-eqz p3, :cond_10

    const-string v0, "{"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "}"

    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v2, v1

    :goto_0
    if-eqz v2, :cond_f

    :try_start_0
    invoke-static {p3}, Lcom/sec/android/app/billing/h;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/h;

    move-result-object v8

    if-nez v8, :cond_0

    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/billing/y;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-object v0, v8, Lcom/sec/android/app/billing/h;->a:Ljava/lang/String;

    const-string v4, "getAndroidVersion"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->getAndroidVersion()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move v6, v3

    :goto_2
    const-string v0, ""

    if-eqz v4, :cond_c

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v5, 0x2

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    if-eqz v6, :cond_b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0xa

    if-le v5, v6, :cond_b

    invoke-static {v4}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    const-string v4, "encodedReturnString(masked)"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {p5, v0}, Landroid/webkit/JsPromptResult;->confirm(Ljava/lang/String;)V

    :goto_4
    iget-object v4, v8, Lcom/sec/android/app/billing/h;->c:Ljava/lang/String;

    if-eqz v4, :cond_f

    const-string v4, ""

    iget-object v5, v8, Lcom/sec/android/app/billing/h;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "javascript:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v8, Lcom/sec/android/app/billing/h;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/billing/z;->b:Landroid/webkit/WebView;

    invoke-virtual {v4, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    :goto_5
    if-nez v0, :cond_e

    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/billing/y;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    move-result v0

    goto :goto_1

    :cond_1
    :try_start_1
    const-string v4, "blockBackKey"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->blockBackKey()V

    move-object v4, v5

    move v6, v3

    goto :goto_2

    :cond_2
    const-string v4, "releaseBackKey"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->releaseBackKey()V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_3
    const-string v4, "getUnifiedPaymentData"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->getUnifiedPaymentData()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move v6, v1

    goto/16 :goto_2

    :cond_4
    const-string v4, "loadComplete"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->loadComplete()V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_5
    const-string v4, "cancelPayment"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->cancelPayment()V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_6
    const-string v4, "receiveCreditCardResult"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    iget-object v0, v8, Lcom/sec/android/app/billing/h;->b:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->receiveCreditCardResult(Ljava/lang/String;)V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_7
    const-string v4, "showToast"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    iget-object v0, v8, Lcom/sec/android/app/billing/h;->b:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->showToast(Ljava/lang/String;)V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_8
    const-string v4, "hideLoadingAnimation"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->hideLoadingAnimation()V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_9
    const-string v4, "getVersion"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/billing/z;->a:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->getVersion()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move v6, v3

    goto/16 :goto_2

    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Call Undefined native API from Javascript : "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/z;->b(Ljava/lang/String;)V

    move-object v4, v5

    move v6, v3

    goto/16 :goto_2

    :cond_b
    const-string v4, "encodedReturnString"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Native Call Fail : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/z;->b(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v3

    goto/16 :goto_5

    :cond_c
    if-eqz v5, :cond_d

    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "returnBoolean : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/billing/z;->a(Ljava/lang/String;)V

    invoke-virtual {p5, v0}, Landroid/webkit/JsPromptResult;->confirm(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    invoke-virtual {p5}, Landroid/webkit/JsPromptResult;->confirm()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_4

    :cond_e
    move v0, v1

    goto/16 :goto_1

    :cond_f
    move v0, v2

    goto/16 :goto_5

    :cond_10
    move v2, v3

    goto/16 :goto_0
.end method
