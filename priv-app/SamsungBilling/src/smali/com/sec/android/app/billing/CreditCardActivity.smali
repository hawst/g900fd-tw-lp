.class public Lcom/sec/android/app/billing/CreditCardActivity;
.super Landroid/app/Activity;


# static fields
.field private static final B:I = 0x5

.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field private static final f:Ljava/lang/String; = "CreditCardActivity"

.field private static final t:Ljava/lang/String; = "PACKAGE_NAME"

.field private static final u:Ljava/lang/String; = "REQUEST_CODE"


# instance fields
.field private A:I

.field private C:Landroid/hardware/input/InputManager;

.field private D:Landroid/util/SparseBooleanArray;

.field private E:Lcom/sec/android/app/billing/d;

.field private F:Z

.field private G:Landroid/os/Handler;

.field e:Ljava/lang/Runnable;

.field private g:Lcom/sec/android/app/billing/ab;

.field private h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

.field private i:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

.field private j:Landroid/webkit/WebView;

.field private k:Landroid/app/ProgressDialog;

.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/app/AlertDialog;

.field private n:Landroid/app/AlertDialog;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;

.field private v:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z

.field private x:Landroid/app/AlertDialog;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->q:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->r:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->w:Z

    new-instance v0, Lcom/sec/android/app/billing/CreditCardActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/CreditCardActivity$1;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->G:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/CreditCardActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/CreditCardActivity$2;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->e:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/CreditCardActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->w:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showSslAlertDialog baseUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000a

    new-instance v2, Lcom/sec/android/app/billing/CreditCardActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/CreditCardActivity$3;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/CreditCardActivity$4;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/CreditCardActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    const v1, 0x7f070003

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->l:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/CreditCardActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->r:Z

    return-void
.end method

.method static synthetic b(I)Z
    .locals 1

    invoke-static {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->c(I)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 5

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CreditCardActivity - onCreate : PACKAGE_NAME = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "REQUEST_CODE"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "CreditCardActivity - onCreate : REQUEST_CODE = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/service/BillingVO;

    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "CreditCardActivity - onCreate : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is valid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "CreditCardActivity - onCreate : PACKAGE_NAME = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "REQUEST_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jsonData is invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "The json data is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/CreditCardActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->i()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/billing/CreditCardActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    return-void
.end method

.method private static c(I)Z
    .locals 1

    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->G:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "UPVersion"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "CreditCardActivity - onCreate : [LIB ver] UPJson Library version change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "UPVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    const-string v0, "CreditCardActivity - onCreate : UPJson COUNTRY change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity - onCreate : CreditCardData.appServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CreditCardData.country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CreditCardData.libraryVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "MCC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    const-string v0, "CreditCardActivity - onCreate : UPJson MCC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "MNC"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "MCC"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v2, "MNC"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    const-string v0, "CreditCardActivity - onCreate : UPJson MNC change !!"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity - onCreate : [LIB ver] Library version change !! "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/b/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/sec/android/app/billing/u;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    return-object v0
.end method

.method private e()V
    .locals 5

    const/4 v4, 0x1

    const v0, 0x7f080003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->c(Landroid/content/Context;)Z

    move-result v0

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->i:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UP Android Version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/billing/ab;

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->G:Landroid/os/Handler;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/billing/ab;-><init>(Landroid/os/Handler;Z)V

    iput-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->g:Lcom/sec/android/app/billing/ab;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->g:Lcom/sec/android/app/billing/ab;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/billing/z;

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/sec/android/app/billing/CreditCardActivity;->i:Lcom/sec/android/app/billing/CreditCardActivity$Bridge;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/billing/z;-><init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/sec/android/app/billing/CreditCardActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    const-wide/32 v1, 0x800000

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    sget-object v1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private f()V
    .locals 8

    const-wide/32 v0, 0x800000

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "http"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Local Cache DIR : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :try_start_0
    const-string v3, "android.net.http.HttpResponseCache"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "install"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/io/File;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lcom/sec/android/app/billing/CreditCardActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x156

    const v4, 0x3f733333    # 0.95f

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    const-string v1, "UPServerURL"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "CreditCardActivity - callRegisterCreditCardPage() : UPJson UPServerURL change !!"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/c;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->p:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->G:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->e:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "v2/register_creditcard.do"

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "v2/tablet/register_creditcard.do"

    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] default V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/billing/e;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/sec/android/app/billing/e;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v0, "CHN"

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    const-string v1, "CreditCardActivity callRegisterCreditCardPage() : Configuration.ORIENTATION_PORTRAIT : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    const/16 v2, 0x189

    invoke-static {p0, v1, v5, v2, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->l:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_2

    const-string v1, "CreditCardActivity callRegisterCreditCardPage() : mLLProgressDialog != null : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->l:Landroid/widget/LinearLayout;

    invoke-static {p0, v1, v5, v6, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    const-string v1, "CreditCardActivity callRegisterCreditCardPage() : Configuration.ORIENTATION_LANDSCAPE : call supportLowResolutionDevice()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    const/16 v2, 0x14e

    invoke-static {p0, v1, v5, v2, v4}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto :goto_2
.end method

.method private h()V
    .locals 7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->E:Lcom/sec/android/app/billing/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    aget v3, v1, v0

    invoke-virtual {v2, v3}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getId()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getSources()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/billing/CreditCardActivity;->c(I)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/billing/CreditCardActivity;->D:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseBooleanArray;->append(IZ)V

    if-eqz v4, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity : onResume() : mIsJoystickAdded = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity : onResume() : ids["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v1, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", inputDeviceId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", isJoystick = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", inputDevice.getName() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v2, "javascript:addedGamePad()"

    iget-object v3, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v3, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic h(Lcom/sec/android/app/billing/CreditCardActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    return v0
.end method

.method static synthetic i(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/hardware/input/InputManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    return-object v0
.end method

.method private i()V
    .locals 4

    const-string v0, "CreditCardActivity : showUpdateAlertDialog()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060013

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060014

    invoke-virtual {p0, v2}, Lcom/sec/android/app/billing/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060010

    new-instance v3, Lcom/sec/android/app/billing/CreditCardActivity$5;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/billing/CreditCardActivity$5;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f060011

    new-instance v2, Lcom/sec/android/app/billing/CreditCardActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/CreditCardActivity$6;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/sec/android/app/billing/CreditCardActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/CreditCardActivity$7;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->D:Landroid/util/SparseBooleanArray;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->l:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06000d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f06001b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060011

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060010

    new-instance v3, Lcom/sec/android/app/billing/CreditCardActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/billing/CreditCardActivity$8;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : mIsJoystickAdded = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->F:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : e.getKeyCode() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x13

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x60

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x61

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x63

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6d

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6c

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->y:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->z:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : isLongPress : dispatchKeyEventCheckCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    const-string v1, "@@@@@ dispatchKeyEvent() : isLongPress : return true"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I

    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->r:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "@@@@@ dispatchKeyEvent() : e.getAction() == KeyEvent.ACTION_DOWN"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:dispatchKeyEvent("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->y:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->z:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->A:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public finish()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    const-string v0, "CreditCardActivity - finish"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->x:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_3
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    const/16 v3, 0x156

    const v2, 0x3f733333    # 0.95f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity onConfigurationChanged() : newConfig.orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v0, v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "CreditCardActivity onConfigurationChanged() : Configuration.ORIENTATION_PORTRAIT : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    const/16 v1, 0x189

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "CreditCardActivity onConfigurationChanged() : Configuration.ORIENTATION_LANDSCAPE : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    const/16 v1, 0x14e

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iput-boolean v3, p0, Lcom/sec/android/app/billing/CreditCardActivity;->q:Z

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UP Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    const-string v0, "CreditCardActivity - onCreate"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity - onCreate : getResources().getConfiguration().orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->D:Landroid/util/SparseBooleanArray;

    new-instance v0, Lcom/sec/android/app/billing/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/d;-><init>(Lcom/sec/android/app/billing/CreditCardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->E:Lcom/sec/android/app/billing/d;

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->b()V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "CreditCardActivity - onCreate : The isOnline is false. finish()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "The ActiveNetwork is not Connected."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v1, 0x7f060009

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    :cond_1
    invoke-static {}, Lcom/sec/android/app/billing/b/b;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->v:Ljava/util/HashMap;

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->c()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->d()V

    const-string v0, "up.updateNo.mode"

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/k;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->G:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Landroid/os/Handler;)V

    :cond_2
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->e()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->f()V

    const-string v0, "https://mop.samsungosp.com"

    iput-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->s:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->g()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "CreditCardActivity - onDestroy"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->q:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity onKeyDown() KEYCODE_BACK mBackKeyFlag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->r:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->w:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CreditCardActivity WebView goBack()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->j:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity onKeyDown : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->a()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "CreditCardActivity - onPause"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity;->C:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity;->E:Lcom/sec/android/app/billing/d;

    invoke-virtual {v0, v1}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "CreditCardActivity - onResume"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/CreditCardActivity;->h()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "CreditCardActivity - onStop"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method
