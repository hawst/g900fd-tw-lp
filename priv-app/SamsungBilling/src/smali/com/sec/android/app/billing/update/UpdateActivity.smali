.class public Lcom/sec/android/app/billing/update/UpdateActivity;
.super Landroid/app/Activity;


# static fields
.field private static final b:Ljava/lang/String; = "UpdateActivity"

.field private static final c:I = 0x1

.field private static final d:I = 0x2

.field private static final e:I = 0x3

.field private static final f:I = 0x4


# instance fields
.field a:Landroid/os/Handler;

.field private g:Z

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/app/AlertDialog;

.field private j:Z

.field private k:Z

.field private l:Lcom/sec/android/app/a/a/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->k:Z

    new-instance v0, Lcom/sec/android/app/billing/update/UpdateActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$1;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 1

    const-string v0, "UpdateActivity : notifyAPKInstalled()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->g(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "UpdateActivity : installUPClient()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    return v0
.end method

.method private b()V
    .locals 1

    const-string v0, "UpdateActivity : deleteDownloadFile()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/billing/update/k;->b(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/update/UpdateActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->k:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/update/UpdateActivity;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    const-string v0, "UpdateActivity : startInstalledAppDetailsActivity()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "package:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private c()V
    .locals 1

    const-string v0, "UpdateActivity : invokeSamsungApps()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->e()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/update/UpdateActivity$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/update/UpdateActivity$6;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private d()V
    .locals 3

    const-string v0, "UpdateActivity : showEnableSamsungAppsAlertDialog()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f06000d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060016

    new-instance v2, Lcom/sec/android/app/billing/update/UpdateActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$2;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060011

    new-instance v2, Lcom/sec/android/app/billing/update/UpdateActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$3;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/sec/android/app/billing/update/UpdateActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$4;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->b()V

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/billing/update/UpdateActivity;)Lcom/sec/android/app/a/a/a/a;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->l:Lcom/sec/android/app/a/a/a/a;

    return-object v0
.end method

.method private e()V
    .locals 2

    new-instance v0, Lcom/sec/android/app/a/a/a/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/a/a/a/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->l:Lcom/sec/android/app/a/a/a/a;

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->l:Lcom/sec/android/app/a/a/a/a;

    new-instance v1, Lcom/sec/android/app/billing/update/UpdateActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$5;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/a/a/a/a;->a(Lcom/sec/android/app/a/a/a/b;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->l:Lcom/sec/android/app/a/a/a/a;

    invoke-virtual {v0}, Lcom/sec/android/app/a/a/a/a;->a()V

    return-void
.end method

.method private f()V
    .locals 3

    const-string v0, "UpdateActivity : startDownloadByPackageName()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->l:Lcom/sec/android/app/a/a/a/a;

    invoke-virtual {v0}, Lcom/sec/android/app/a/a/a/a;->f()Lcom/sec/android/app/a/a/a/g;

    move-result-object v0

    const-string v1, "com.sec.android.app.billing"

    new-instance v2, Lcom/sec/android/app/billing/update/UpdateActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/update/UpdateActivity$7;-><init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/a/a/a/g;->a(Ljava/lang/String;Lcom/sec/android/app/a/a/a/d;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->f()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "directcall"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "CallerType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "GUID"

    const-string v2, "com.sec.android.app.billing"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_0

    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const-string v0, "UpdateActivity : invokeSamsungApps() : deepLink startActivity"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateActivity : invokeSamsungApps() : ActivityNotFoundException : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->d()V

    goto :goto_0

    :cond_0
    const/high16 v1, 0x14000000

    :try_start_1
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public finish()V
    .locals 1

    const-string v0, "UpdateActivity : finish()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    const/16 v4, 0x131

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iput-boolean v3, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v0, "UpdateActivity : onCreate()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.billing"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/update/k;->a(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->g:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : onCreate() : mIsUserApp = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Landroid/app/ProgressDialog;

    const v1, 0x7f070001

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    const v1, 0x7f060013

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    const v1, 0x7f060015

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "UpdateActivity : onCreate() : mIsShowProgressDialog = false : invokeSamsungApps."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->k:Z

    invoke-direct {p0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateActivity : onCreate() : NameNotFoundException = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateActivity : onCreate() : Exception = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_0
    const-string v0, "UpdateActivity : onCreate() : mIsShowProgressDialog = true : downloadFromSamsungAppsServer."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->k:Z

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->a:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/e;->b(Landroid/content/Context;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const-string v0, "UpdateActivity : onDestroy()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->j:Z

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 1

    const-string v0, "UpdateActivity : onPause()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    const-string v0, "UpdateActivity : onRestart()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const-string v0, "UpdateActivity : onResume()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    const-string v0, "UpdateActivity : onStart()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const-string v0, "UpdateActivity : onStop()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
