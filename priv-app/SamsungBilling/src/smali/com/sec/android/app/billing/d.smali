.class public Lcom/sec/android/app/billing/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/input/InputManager$InputDeviceListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/CreditCardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/billing/CreditCardActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInputDeviceAdded(I)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->i(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/hardware/input/InputManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceAdded() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inputDevice = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getSources()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->b(I)Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/CreditCardActivity;->j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseBooleanArray;->append(IZ)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@@@@@ InputDeviceListener : onInputDeviceAdded() : deviceId = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isJoystick = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/CreditCardActivity;->c(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceAdded() : mIsJoystickAdded = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/CreditCardActivity;->h(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceAdded() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inputDevice.getName() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v0, "javascript:addedGamePad()"

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onInputDeviceChanged(I)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->i(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/hardware/input/InputManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceChanged() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inputDevice = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/InputDevice;->getSources()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->b(I)Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/CreditCardActivity;->j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@@@@@ InputDeviceListener : onInputDeviceChanged() : isJoystickOld = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isJoystickNew = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    if-eq v2, v1, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/CreditCardActivity;->j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/app/billing/CreditCardActivity;->c(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@@@@@ InputDeviceListener : onInputDeviceChanged() : mIsJoystickAdded = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/CreditCardActivity;->h(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@@@@@ InputDeviceListener : onInputDeviceChanged() : deviceId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Joystick is removed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v3, "javascript:removedGamePad()"

    iget-object v4, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/CreditCardActivity;->e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/CreditCardActivity;->c(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceChanged() : mIsJoystickAdded = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/CreditCardActivity;->h(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceChanged() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inputDevice.getName() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v0, "javascript:addedGamePad()"

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onInputDeviceRemoved(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->i(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/hardware/input/InputManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceRemoved() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", inputDevice = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ InputDeviceListener : onInputDeviceRemoved() : deviceId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isJoystick = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/CreditCardActivity;->c(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@@@@@ InputDeviceListener : onInputDeviceRemoved() : mIsJoystickAdded = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->h(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@@@@@ InputDeviceListener : onInputDeviceRemoved() : deviceId = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Joystick is removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v0, "javascript:removedGamePad()"

    iget-object v1, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/d;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->j(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    return-void
.end method
