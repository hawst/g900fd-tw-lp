.class public Lcom/sec/android/app/billing/b/j;
.super Ljava/lang/Exception;


# static fields
.field public static final a:Ljava/lang/String; = "UnifiedPayment checkout parameter exception"

.field public static final b:Ljava/lang/String; = "UnifiedPayment creditcard parameter exception"

.field public static final c:Ljava/lang/String; = "UnifiedPayment giftcard parameter exception"

.field private static final d:J = -0x7736b30130344aa6L


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/b/j;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/b/j;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
