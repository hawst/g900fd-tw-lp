.class public Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;
.super Landroid/app/Activity;


# static fields
.field private static final A:Ljava/lang/String; = "https://mobile.inicis.com/smart/upoint/"

.field private static final B:Ljava/lang/String; = "https://mobile.inicis.com/smart/mobile/"

.field private static final C:Ljava/lang/String; = "https://mobile.inicis.com/smart/culture/"

.field private static final D:Ljava/lang/String; = "https://inilite.inicis.com/inibill/inibill_card.jsp"

.field private static final E:Ljava/lang/String; = "https://inilite.inicis.com/inibill/inibill_hpp.jsp"

.field private static final L:I = 0x5

.field public static final c:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_URL"

.field public static final d:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_REQUEST"

.field public static final e:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_TYPE"

.field public static final f:Ljava/lang/String; = "WEBMONEY"

.field public static final g:Ljava/lang/String; = "WCB"

.field public static final h:Ljava/lang/String; = "DANAL_DOCOMO"

.field public static final i:Ljava/lang/String; = "PAYPAL"

.field public static final j:Ljava/lang/String; = "SINA"

.field public static final k:Ljava/lang/String; = "KCC"

.field public static final l:Ljava/lang/String; = "KUP"

.field public static final m:Ljava/lang/String; = "KPI"

.field public static final n:Ljava/lang/String; = "INICISMRC"

.field public static final o:Ljava/lang/String; = "KPIMRC"

.field public static final p:Ljava/lang/String; = "QIWI"

.field public static final q:Ljava/lang/String; = "LIVE_GAMER"

.field public static final r:Ljava/lang/String; = "ICC"

.field public static final s:Ljava/lang/String; = "KUP_RETRIEVE"

.field public static final t:Ljava/lang/String; = "KCT"

.field public static final u:Ljava/lang/String; = "GCB06"

.field private static final z:Ljava/lang/String; = "https://mobile.inicis.com/smart/wcard/"


# instance fields
.field private F:Z

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:I

.field private J:I

.field private K:I

.field private M:Landroid/hardware/input/InputManager;

.field private N:Landroid/util/SparseBooleanArray;

.field private O:Lcom/sec/android/app/billing/x;

.field private P:Z

.field private Q:Landroid/os/Handler;

.field public final a:Ljava/lang/String;

.field protected b:Z

.field private v:Landroid/webkit/WebView;

.field private w:Landroid/app/ProgressDialog;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "UnifiedPaymentPGPaymentActivity"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->a:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->F:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->H:Z

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->Q:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 4

    const/16 v3, 0x156

    const v2, 0x3f733333    # 0.95f

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->H:Z

    if-eqz v0, :cond_1

    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    const-string v1, "M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setRequestedOrientation(I)V

    const-string v0, "[New UX] DISPLAY_TYPE_MOBILE : setRequestedOrientation : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "UnifiedPaymentPGPaymentActivity onCreate : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    const/16 v1, 0x21c

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->x:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    const-string v0, "UnifiedPaymentPGPaymentActivity onCreate : mLLProgressDialog != null : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->x:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {p0, v0, v3, v1, v2}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->setContentView(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->x:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    return-void
.end method

.method static synthetic a(I)Z
    .locals 1

    invoke-static {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->F:Z

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private b()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/app/ProgressDialog;

    const v1, 0x7f070003

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->x:Landroid/widget/LinearLayout;

    return-void
.end method

.method private static b(I)Z
    .locals 1

    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/hardware/input/InputManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    return-object v0
.end method

.method private c()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f080004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    const-string v1, "#e6e6e6"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v1, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/billing/ab;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->Q:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->e()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/billing/ab;-><init>(Landroid/os/Handler;Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/billing/y;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Landroid/util/SparseBooleanArray;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->N:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method private d()V
    .locals 7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->O:Lcom/sec/android/app/billing/x;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    aget v3, v1, v0

    invoke-virtual {v2, v3}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getId()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getSources()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b(I)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->N:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseBooleanArray;->append(IZ)V

    if-eqz v4, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity : onResume() : mIsJoystickAdded = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "@@@@@ UnifiedPaymentMainActivity : onResume() : ids["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v1, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", inputDeviceId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", isJoystick = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", inputDevice.getName() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private e()Z
    .locals 7

    const/4 v2, 0x0

    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v4

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v1, v0

    :goto_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "MobileNetwork : "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "proxyAddress : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", proxyPort : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/billing/c;->aM:[Ljava/lang/String;

    array-length v3, v1

    move v0, v2

    :goto_1
    if-lt v2, v3, :cond_1

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isProxyDirectConnectMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return v0

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    aget-object v4, v1, v2

    if-eqz v4, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method static synthetic e(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    return v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/16 v3, 0x61

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : mIsJoystickAdded = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->P:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : e.getKeyCode() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-static {v2}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x13

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x60

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x63

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6d

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x6c

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x66

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_3

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->I:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->J:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@@@@@ dispatchKeyEvent() : isLongPress : dispatchKeyEventCheckCount = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_2

    const-string v1, "@@@@@ dispatchKeyEvent() : isLongPress : return true"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I

    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "@@@@@ dispatchKeyEvent() : e.getAction() == KeyEvent.ACTION_DOWN"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v1, v3, :cond_1

    const/4 v1, 0x4

    invoke-virtual {p0, v1, p1}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_4
    :try_start_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->I:I

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->J:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->K:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public finish()V
    .locals 1

    const-string v0, "UnifiedPaymentPGPaymentActivity finish"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->w:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity onConfigurationChanged() : newConfig.orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->H:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    const-string v1, "M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "UnifiedPaymentPGPaymentActivity onConfigurationChanged() : call supportLowResolutionDevice()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    const/16 v1, 0x156

    const/16 v2, 0x21c

    const v3, 0x3f733333    # 0.95f

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/billing/b/b;->a(Landroid/content/Context;Landroid/view/View;IIF)V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iput-boolean v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->F:Z

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UP Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity onCreate : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity onCreate : getResources().getConfiguration().orientation = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->N:Landroid/util/SparseBooleanArray;

    new-instance v0, Lcom/sec/android/app/billing/x;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/x;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->O:Lcom/sec/android/app/billing/x;

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b()V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const-string v0, "UNIFIED_PAYMENT_PG_TYPE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "UNIFIED_PAYMENT_PG_URL"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "UNIFIED_PAYMENT_PG_REQUEST"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "IS_UX_V2"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->H:Z

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UnifiedPaymentPGPaymentActivity onCreate : mIsNewUX = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->H:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v4, "DISPLAY_TYPE"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UnifiedPaymentPGPaymentActivity onCreate : mDisplayType = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->G:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->a()V

    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->c()V

    if-eqz v3, :cond_e

    const-string v1, "KCC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "https://mobile.inicis.com/smart/wcard/"

    :cond_2
    :goto_1
    const-string v1, "INICISMRC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "KPIMRC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    :goto_2
    const-string v0, "KCC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "KPI"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "KUP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "KCC"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity, params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/sec/android/app/billing/b/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v1, "INICISMRC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "https://inilite.inicis.com/inibill/inibill_card.jsp"

    goto :goto_1

    :cond_6
    const-string v1, "KPIMRC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v0, "https://inilite.inicis.com/inibill/inibill_hpp.jsp"

    goto :goto_1

    :cond_7
    const-string v1, "PAYPAL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "WEBMONEY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "DANAL_DOCOMO"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "SINA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KUP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v0, "https://mobile.inicis.com/smart/upoint/"

    goto/16 :goto_1

    :cond_8
    const-string v1, "QIWI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "LIVE_GAMER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ICC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KUP_RETRIEVE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "WCB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KCT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v0, "https://mobile.inicis.com/smart/culture/"

    goto/16 :goto_1

    :cond_9
    const-string v1, "GCB06"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "https://mobile.inicis.com/smart/mobile/"

    goto/16 :goto_1

    :cond_a
    const-string v1, "QIWI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "LIVE_GAMER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "ICC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "KUP_RETRIEVE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "params url : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "params params : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "params url : "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->v:Landroid/webkit/WebView;

    const-string v4, "EUC-KR"

    invoke-static {v3, v4}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    goto/16 :goto_2

    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity, params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {v1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UnifiedPaymentPGPaymentActivity, params is null"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-nez v0, :cond_f

    const-string v0, "UnifiedPaymentPGPaymentActivity, data is null"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentPGPaymentActivity, no data"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "UnifiedPaymentPGPaymentActivity onDestroy"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->F:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentPGPaymentActivity onKeyDown : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06000d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f06000f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060011

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f060010

    new-instance v3, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity$2;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->M:Landroid/hardware/input/InputManager;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->O:Lcom/sec/android/app/billing/x;

    invoke-virtual {v0, v1}, Landroid/hardware/input/InputManager;->unregisterInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->finish()V

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGPaymentActivity;->d()V

    return-void
.end method
