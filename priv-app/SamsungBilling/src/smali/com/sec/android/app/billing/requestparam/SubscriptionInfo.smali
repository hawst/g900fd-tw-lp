.class public Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public dayPeriodPayment:Ljava/lang/String;

.field public freeTrialPeriod:Ljava/lang/String;

.field public notificationURL:Ljava/lang/String;

.field public periodStartPaymentInfoList:[Ljava/lang/String;

.field public repeatCount:Ljava/lang/String;

.field public repeatNumber:Ljava/lang/String;

.field public retryCount:Ljava/lang/String;

.field public retryPeriod:Ljava/lang/String;

.field public statusNotificationURL:Ljava/lang/String;

.field public subscriptionID:Ljava/lang/String;

.field public subscriptionOrderURL:Ljava/lang/String;

.field public subscriptionTypeCode:Ljava/lang/String;

.field public timeZone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;
    .locals 2

    new-instance v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionTypeCode:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->freeTrialPeriod:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionOrderURL:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->timeZone:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->retryCount:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->retryPeriod:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->repeatCount:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->dayPeriodPayment:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->notificationURL:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionID:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->repeatNumber:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->statusNotificationURL:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionTypeCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->freeTrialPeriod:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionOrderURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->timeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->retryCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->retryPeriod:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->repeatCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->dayPeriodPayment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->notificationURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->subscriptionID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->repeatNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->statusNotificationURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method
