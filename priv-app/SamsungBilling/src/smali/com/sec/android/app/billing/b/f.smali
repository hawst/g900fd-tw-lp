.class public final enum Lcom/sec/android/app/billing/b/f;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/billing/b/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/sec/android/app/billing/b/f;

.field public static final enum b:Lcom/sec/android/app/billing/b/f;

.field public static final enum c:Lcom/sec/android/app/billing/b/f;

.field public static final enum d:Lcom/sec/android/app/billing/b/f;

.field public static final enum e:Lcom/sec/android/app/billing/b/f;

.field private static final synthetic f:[Lcom/sec/android/app/billing/b/f;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/app/billing/b/f;

    const-string v1, "Not_Change_NetWork"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/billing/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/billing/b/f;->a:Lcom/sec/android/app/billing/b/f;

    new-instance v0, Lcom/sec/android/app/billing/b/f;

    const-string v1, "DirectBillingAuthUrlCheck"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/billing/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/billing/b/f;->b:Lcom/sec/android/app/billing/b/f;

    new-instance v0, Lcom/sec/android/app/billing/b/f;

    const-string v1, "ConfirmDirectBilling"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/billing/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/billing/b/f;->c:Lcom/sec/android/app/billing/b/f;

    new-instance v0, Lcom/sec/android/app/billing/b/f;

    const-string v1, "Res_Data_NULL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/billing/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/billing/b/f;->d:Lcom/sec/android/app/billing/b/f;

    new-instance v0, Lcom/sec/android/app/billing/b/f;

    const-string v1, "Res_Result_NOK"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/billing/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/billing/b/f;->e:Lcom/sec/android/app/billing/b/f;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/billing/b/f;

    sget-object v1, Lcom/sec/android/app/billing/b/f;->a:Lcom/sec/android/app/billing/b/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/billing/b/f;->b:Lcom/sec/android/app/billing/b/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/billing/b/f;->c:Lcom/sec/android/app/billing/b/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/billing/b/f;->d:Lcom/sec/android/app/billing/b/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/billing/b/f;->e:Lcom/sec/android/app/billing/b/f;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/billing/b/f;->f:[Lcom/sec/android/app/billing/b/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/billing/b/f;
    .locals 1

    const-class v0, Lcom/sec/android/app/billing/b/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/b/f;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/billing/b/f;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/android/app/billing/b/f;->f:[Lcom/sec/android/app/billing/b/f;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/app/billing/b/f;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
