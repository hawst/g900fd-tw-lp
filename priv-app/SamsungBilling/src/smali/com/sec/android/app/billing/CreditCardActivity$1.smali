.class Lcom/sec/android/app/billing/CreditCardActivity$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/CreditCardActivity;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/CreditCardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/CreditCardActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/CreditCardActivity$1;)Lcom/sec/android/app/billing/CreditCardActivity;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    const v6, 0x7f06001c

    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity Handler what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/CreditCardActivity;->a(I)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->b(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    :sswitch_2
    const-string v0, "handler received PAGE_ERROR"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0001"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "WebView Page error"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    aget-object v2, v0, v3

    aget-object v0, v0, v4

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    aget-object v2, v0, v3

    aget-object v0, v0, v4

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "register card success"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "register card cancel"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "register card fail"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CreditCardActivity queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "@@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "errorId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", errorMessage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v3, "ERROR_ID"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity : handleMessage : UNKNOWN_ERROR : ERROR_ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity : handleMessage : UNKNOWN_ERROR : ERROR_MESSAGE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_MESSAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ERROR_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v1, v5, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "CreditCardActivity : handleMessage : NEED_NOT_UPDATE"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "CreditCardActivity : handleMessage : NEED_TO_UPDATE : showUpdateAlertDialog()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->c(Lcom/sec/android/app/billing/CreditCardActivity;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/CreditCardActivity$1$1;-><init>(Lcom/sec/android/app/billing/CreditCardActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0, v4}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/billing/CreditCardActivity;->a(I)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0, v3}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->f(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->f(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->f(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->a(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$1;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/billing/CreditCardActivity;->a(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_5
        0x11 -> :sswitch_6
        0x12 -> :sswitch_7
        0x15 -> :sswitch_b
        0x16 -> :sswitch_c
        0x25 -> :sswitch_3
        0x26 -> :sswitch_4
        0x12c -> :sswitch_a
        0x131 -> :sswitch_8
        0x136 -> :sswitch_9
    .end sparse-switch
.end method
