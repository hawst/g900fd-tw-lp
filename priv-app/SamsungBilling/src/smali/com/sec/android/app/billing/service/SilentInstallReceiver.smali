.class public Lcom/sec/android/app/billing/service/SilentInstallReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String; = "SilentInstallReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;)J

    move-result-wide v0

    const-string v2, "SilentInstallReceiver : onReceive() : android.intent.action.BOOT_COMPLETED : call setAlarm."

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/billing/update/e;->b(Landroid/content/Context;J)V

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "com.sec.android.app.billing.service.SilentInstallByAlarmManger"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "SilentInstallReceiver : onReceive() : INTENT_ACTION_SILENT_INSTALL_BY_ALARMMANGER : call SilentInstallService."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/service/SilentInstallService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SilentInstallReceiver : onReceive() : action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
