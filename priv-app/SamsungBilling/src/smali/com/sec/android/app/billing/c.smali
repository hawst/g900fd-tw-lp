.class public Lcom/sec/android/app/billing/c;
.super Ljava/lang/Object;


# static fields
.field public static final A:Ljava/lang/String; = "ERROR_ID"

.field public static final B:Ljava/lang/String; = "SIGNATURE"

.field public static final C:Ljava/lang/String; = "PAYMENT_RECEITE"

.field public static final D:Ljava/lang/String; = "0001"

.field public static final E:Ljava/lang/String; = "0002"

.field public static final F:Ljava/lang/String; = "1000"

.field public static final G:I = 0x64

.field public static final H:I = 0x65

.field public static final I:I = 0x66

.field public static final J:I = 0xc8

.field public static final K:Ljava/lang/String; = "https://mop.samsungosp.com"

.field public static final L:Ljava/lang/String; = "https://cn-mop.samsungosp.com"

.field public static final M:Ljava/lang/String; = "register_creditcard.do"

.field public static final N:Ljava/lang/String; = "payment.do"

.field public static final O:Ljava/lang/String; = "tablet_payment.do"

.field public static final P:Ljava/lang/String; = "gb_payment.do"

.field public static final Q:Ljava/lang/String; = "gp/payment.do"

.field public static final R:Ljava/lang/String; = "creditcard.do"

.field public static final S:Ljava/lang/String; = "tablet_creditcard.do"

.field public static final T:Ljava/lang/String; = "gb_creditcard.do"

.field public static final U:Ljava/lang/String; = "gp/creditcard.do"

.field public static final V:Ljava/lang/String; = "giftcard.do"

.field public static final W:Ljava/lang/String; = "tablet_giftcard.do"

.field public static final X:Ljava/lang/String; = "gb_giftcard.do"

.field public static final Y:Ljava/lang/String; = "gp/giftcard.do"

.field public static final Z:Ljava/lang/String; = "tablet"

.field public static final a:Ljava/lang/String; = "com.sec.android.app.billing"

.field public static final aA:Ljava/lang/String; = "www.wicore.in"

.field public static final aB:Ljava/lang/String; = "mop.samsungosp.com"

.field public static final aC:Ljava/lang/String; = "stg-api.samsungosp.com"

.field public static final aD:Ljava/lang/String; = "mop.samsungosp.com"

.field public static final aE:Ljava/lang/String; = "cn-mop.samsungosp.com"

.field public static final aF:Ljava/lang/String; = "stg-mop.samsungosp.com"

.field public static final aG:I = 0xea60

.field public static final aH:I = 0x12c

.field public static final aI:I = 0xea60

.field public static final aJ:Ljava/lang/String; = "com.sec.android.wallet"

.field public static final aK:Ljava/lang/String; = "A000"

.field public static final aL:Ljava/lang/String; = "com.sec.android.wallet.PAYMENT_COMPLETED_"

.field public static final aM:[Ljava/lang/String;

.field public static final aN:I = 0x5

.field public static final aO:I = 0x6

.field public static final aP:I = 0x7

.field public static final aQ:I = 0x8

.field public static final aR:I = 0x9

.field public static final aS:I = 0xa

.field public static final aT:I = 0xb

.field public static final aU:I = 0xc

.field public static final aV:I = 0xd

.field public static final aW:I = 0xe

.field public static final aX:I = 0xf

.field public static final aY:I = 0x10

.field public static final aZ:I = 0x11

.field public static final aa:Ljava/lang/String; = "up.json"

.field public static final ab:Ljava/lang/String; = "UPServerURL"

.field public static final ac:Ljava/lang/String; = "UPVersion"

.field public static final ad:Ljava/lang/String; = "MCC"

.field public static final ae:Ljava/lang/String; = "MNC"

.field public static final af:Ljava/lang/String; = "COUNTRY"

.field public static final ag:Ljava/lang/String; = "CURRENCY"

.field public static final ah:Ljava/lang/String; = "v1"

.field public static final ai:Ljava/lang/String; = "v2"

.field public static final aj:Ljava/lang/String; = "gp"

.field public static final ak:Ljava/lang/String; = "www.paypal.com"

.field public static final al:Ljava/lang/String; = "www.sandbox.paypal.com"

.field public static final am:Ljava/lang/String; = "ipn.teledit.com"

.field public static final an:Ljava/lang/String; = "ota.pay.mobile.sina.cn"

.field public static final ao:Ljava/lang/String; = "w.qiwi.com"

.field public static final ap:Ljava/lang/String; = "www.paysbuy.com"

.field public static final aq:Ljava/lang/String; = "demo.paysbuy.com"

.field public static final ar:Ljava/lang/String; = "oem.tfelements.com"

.field public static final as:Ljava/lang/String; = "samsung.samanepay.com"

.field public static final at:Ljava/lang/String; = "account.samsung.com"

.field public static final au:Ljava/lang/String; = "ipntest.danal.co.kr"

.field public static final av:Ljava/lang/String; = "ipn.danalpay.com"

.field public static final aw:Ljava/lang/String; = "ipntest.danal.co.kr"

.field public static final ax:Ljava/lang/String; = "ipnweb.danalpay.com"

.field public static final ay:Ljava/lang/String; = "secureacceptance.cybersource.com"

.field public static final az:Ljava/lang/String; = "testsecureacceptance.cybersource.com"

.field public static final b:I = 0x1

.field public static final bA:I = 0x24ab8

.field public static final bB:Ljava/lang/String; = "Y"

.field public static final bC:Ljava/lang/String; = "N"

.field public static final bD:Ljava/lang/String; = "y"

.field public static final bE:Ljava/lang/String; = "n"

.field public static final bF:Ljava/lang/String; = "/"

.field public static final bG:Ljava/lang/String; = "http://"

.field public static final bH:Ljava/lang/String; = "https://"

.field public static final bI:Ljava/lang/String; = "SAC_0105"

.field public static final bJ:Ljava/lang/String; = "recent_payment_key"

.field public static final bK:Ljava/lang/String; = "SKT"

.field public static final bL:Ljava/lang/String; = "KTF"

.field public static final bM:Ljava/lang/String; = "LGT"

.field public static final bN:Ljava/lang/String; = "KTF"

.field public static final bO:Ljava/lang/String; = "SKTelecom"

.field public static final bP:Ljava/lang/String; = "LGT"

.field public static final bQ:Ljava/lang/String; = "45002"

.field public static final bR:Ljava/lang/String; = "45004"

.field public static final bS:Ljava/lang/String; = "45008"

.field public static final bT:Ljava/lang/String; = "45003"

.field public static final bU:Ljava/lang/String; = "45005"

.field public static final bV:Ljava/lang/String; = "45011"

.field public static final bW:Ljava/lang/String; = "45006"

.field public static final bX:Ljava/lang/String; = "NULL"

.field public static final bY:Ljava/lang/String; = "PBA"

.field public static final bZ:Ljava/lang/String; = "PBB"

.field public static final ba:I = 0x12

.field public static final bb:I = 0x25

.field public static final bc:I = 0x26

.field public static final bd:I = 0x0

.field public static final be:I = 0x1

.field public static final bf:I = 0x2

.field public static final bg:I = 0x3

.field public static final bh:I = 0x4

.field public static final bi:I = 0x5

.field public static final bj:I = 0x6

.field public static final bk:I = 0x7

.field public static final bl:I = 0x8

.field public static final bm:I = 0x9

.field public static final bn:Ljava/lang/String; = "PAYER_ID"

.field public static final bo:Ljava/lang/String; = "KCC_QUERY_STRING"

.field public static final bp:Ljava/lang/String; = "MRC_QUERY_STRING"

.field public static final bq:Ljava/lang/String; = "ICC_QUERY_STRING"

.field public static final br:Ljava/lang/String; = "REGISTER_CARD_FAIL_STRING"

.field public static final bs:Ljava/lang/String; = "SLG_PAYMENT_METHOD"

.field public static final bt:Ljava/lang/String; = "FAIL_PAYMENT_QUERY_STRING"

.field public static final bu:Ljava/lang/String; = "CHN"

.field public static final bv:Ljava/lang/String; = "KOR"

.field public static final bw:Ljava/lang/String; = "T"

.field public static final bx:Ljava/lang/String; = "M"

.field public static final by:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_SERVICE"

.field public static final bz:I = 0x2244e

.field public static final c:Ljava/lang/String; = "UNIFIED_PAYMENT_REQUEST"

.field public static final ca:Ljava/lang/String; = "TRM"

.field public static final cb:Ljava/lang/String; = "BANGO"

.field public static final cc:Ljava/lang/String; = "asdf"

.field public static final cd:I = 0x156

.field public static final ce:I = 0x189

.field public static final cf:I = 0x14e

.field public static final cg:I = 0x156

.field public static final ch:I = 0x189

.field public static final ci:I = 0x14e

.field public static final cj:F = 0.95f

.field public static final ck:I = 0x156

.field public static final cl:I = 0x21c

.field public static final d:Ljava/lang/String; = "CREDIT_CARD_REQUEST"

.field public static final e:Ljava/lang/String; = "GIFT_CARD_REQUEST"

.field public static final f:Ljava/lang/String; = "IS_UX_V2"

.field public static final g:Ljava/lang/String; = "DISPLAY_TYPE"

.field public static final h:Ljava/lang/String; = "PAYMENT"

.field public static final i:Ljava/lang/String; = "CREDIT_CARD"

.field public static final j:Ljava/lang/String; = "GIFT_CARD"

.field public static final k:Ljava/lang/String; = "REGISTER_CREDIT_CARD"

.field public static final l:Ljava/lang/String; = "SILENT_INSTALL"

.field public static final m:Ljava/lang/String; = "com.sec.android.app.billing.service.SilentInstallByAlarmManger"

.field public static final n:Ljava/lang/String; = "ISP_SUCCESS"

.field public static final o:Ljava/lang/String; = "APK"

.field public static final p:Ljava/lang/String; = "APKUPReadersHub://"

.field public static final q:Ljava/lang/String; = "APKUPLearningHub://"

.field public static final r:Ljava/lang/String; = "APKUPMediaHub://"

.field public static final s:Ljava/lang/String; = "APKUPVideoHub://"

.field public static final t:Ljava/lang/String; = "APKUPMusicHub://"

.field public static final u:Ljava/lang/String; = "APKUPSamsungCloud://"

.field public static final v:Ljava/lang/String; = "APKUPSamsungApps://"

.field public static final w:Ljava/lang/String; = "mailto"

.field public static final x:Ljava/lang/String; = "javascript:"

.field public static final y:Ljava/lang/String; = "close=true"

.field public static final z:Ljava/lang/String; = "ERROR_MESSAGE"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "20809"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "20810"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "20811"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "20813"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "20814"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/billing/c;->aM:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
