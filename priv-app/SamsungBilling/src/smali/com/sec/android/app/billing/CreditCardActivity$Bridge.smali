.class Lcom/sec/android/app/billing/CreditCardActivity$Bridge;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/CreditCardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/CreditCardActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/CreditCardActivity$Bridge;)Lcom/sec/android/app/billing/CreditCardActivity;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    return-object v0
.end method


# virtual methods
.method public blockBackKey()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "CreditCardActivity : blockBackKey()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/CreditCardActivity;->b(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    return-void
.end method

.method public cancelPayment()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$2;-><init>(Lcom/sec/android/app/billing/CreditCardActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public getAndroidVersion()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CreditCardActivity : [Platform Version]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnifiedPaymentData()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->g(Lcom/sec/android/app/billing/CreditCardActivity;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getVersion() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public hideLoadingAnimation()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public loadComplete()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "CreditCardActivity : call loadComplete()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    iget-object v1, v1, Lcom/sec/android/app/billing/CreditCardActivity;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->h(Lcom/sec/android/app/billing/CreditCardActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$1;-><init>(Lcom/sec/android/app/billing/CreditCardActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public receiveCreditCardResult(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->d(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/CreditCardActivity$Bridge$3;-><init>(Lcom/sec/android/app/billing/CreditCardActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public releaseBackKey()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "CreditCardActivity : releaseBackKey()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/CreditCardActivity;->b(Lcom/sec/android/app/billing/CreditCardActivity;Z)V

    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "CreditCardActivity : showToast()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$Bridge;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
