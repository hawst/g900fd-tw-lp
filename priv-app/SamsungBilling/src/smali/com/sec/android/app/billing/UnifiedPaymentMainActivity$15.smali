.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "bangoPayRunnable"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[bangoPay] Status"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:bangoComplete("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$15;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
