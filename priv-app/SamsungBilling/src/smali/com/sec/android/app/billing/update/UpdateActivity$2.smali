.class Lcom/sec/android/app/billing/update/UpdateActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/update/UpdateActivity;->d()V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/update/UpdateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$2;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const-string v0, "com.sec.android.app.samsungapps"

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$2;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->b(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateActivity : startInstalledAppDetailsActivity() : result = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$2;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$2;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    return-void
.end method
