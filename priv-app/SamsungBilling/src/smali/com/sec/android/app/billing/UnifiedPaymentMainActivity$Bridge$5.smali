.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gcbPayStart(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v3, 0x0

    :try_start_0
    new-instance v5, Ljava/net/URL;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->b:Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gcbPay proxyAddress : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gcbPay proxyPort : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, v6, v7}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v2, v4}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    invoke-virtual {v5, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v4, v0

    :goto_1
    if-eqz v4, :cond_b

    const-string v0, "Connection"

    const-string v2, "close"

    invoke-virtual {v4, v0, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    const/16 v0, 0x4e20

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const v0, 0xea60

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[GCB] GCB_AUTHURL ===> "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[GCB] connection.getResponseCode() ===> "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_a

    new-instance v8, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    const-string v9, "UTF-8"

    invoke-direct {v0, v2, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    :goto_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V

    :goto_3
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    if-nez v2, :cond_0

    const-string v2, ""

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "[GCB] url connection result : "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_4
    add-int/lit8 v0, v1, 0x1

    if-nez v2, :cond_9

    const-string v1, ""

    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "authUrl connection cnt : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", result : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->e(Ljava/lang/String;)V

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    if-eqz v1, :cond_6

    const-string v2, "Status=Success"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/billing/b/f;->a:Lcom/sec/android/app/billing/b/f;

    invoke-static {v0, v2}, Lcom/sec/android/app/billing/b/e;->a(Landroid/content/Context;Lcom/sec/android/app/billing/b/f;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i:Ljava/lang/Runnable;

    const-wide/32 v3, 0xea60

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "gcbPay excute authURL result : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Authorization failed. result : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/j;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_6
    return-void

    :cond_3
    :try_start_1
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v4, v0

    goto/16 :goto_1

    :cond_4
    if-nez v2, :cond_5

    move-object v2, v0

    goto/16 :goto_2

    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_6
    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    move v1, v0

    goto/16 :goto_0

    :cond_7
    const-string v0, "Status=Success"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "gcbPay Successed. Call javascript:gcbComplete()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6

    :cond_8
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Authorization failed. result : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/j;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/b/j;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6

    :catch_2
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6

    :catch_3
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Lcom/sec/android/app/billing/b/j;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    throw v0

    :cond_9
    move-object v1, v2

    goto/16 :goto_5

    :cond_a
    move-object v2, v3

    goto/16 :goto_3

    :cond_b
    move-object v2, v3

    goto/16 :goto_4
.end method
