.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kccPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;

.field private final synthetic e:Ljava/lang/String;

.field private final synthetic f:Ljava/lang/String;

.field private final synthetic g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[kccPay] mId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", paymentId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", paymentAmount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/InicisRequestData;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_MID:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_OID:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_AMT:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/b/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_UNAME:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/billing/b/b;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_EMAIL:Ljava/lang/String;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paymentID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",requestID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",paymentMethod=kcc"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NOTI:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->m(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "up-web/kcc_cb.do"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NEXT_URL:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "APK"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_CANCEL_URL:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "APK"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_RETURN_URL:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->g:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_NOTI_URL:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    array-length v1, v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v2, v2, v4

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/sec/android/app/billing/b/b;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_GOODS:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_OFFER_PERIOD:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->n(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "after post : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    const-string v3, "KCC"

    invoke-virtual {v0}, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v5, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_2
    iput-object v5, v0, Lcom/sec/android/app/billing/requestparam/InicisRequestData;->P_EMAIL:Ljava/lang/String;

    goto/16 :goto_0
.end method
