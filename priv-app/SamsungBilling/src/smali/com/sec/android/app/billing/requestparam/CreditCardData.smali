.class public Lcom/sec/android/app/billing/requestparam/CreditCardData;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appServiceID:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

.field public isOnlyRegister:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public libraryVersion:Ljava/lang/String;

.field private platformCode:Ljava/lang/String;

.field public storeRequestID:Ljava/lang/String;

.field public timeOffset:Ljava/lang/String;

.field public upServerURL:Ljava/lang/String;

.field public userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

.field public uxVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/requestparam/CreditCardData$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/CreditCardData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "A"

    iput-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/CreditCardData;
    .locals 2

    new-instance v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/CreditCardData;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/UserInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->isOnlyRegister:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->isOnlyRegister:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
