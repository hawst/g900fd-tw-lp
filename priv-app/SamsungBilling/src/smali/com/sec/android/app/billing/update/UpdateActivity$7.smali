.class Lcom/sec/android/app/billing/update/UpdateActivity$7;
.super Lcom/sec/android/app/a/a/a/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/update/UpdateActivity;->f()V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/update/UpdateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-direct {p0}, Lcom/sec/android/app/a/a/a/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "UpdateActivity : onDownloadSuccess"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const-string v1, "onDownloadSuccess"

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    return-void
.end method

.method public a(JJ)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const-string v1, "%d %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    const-string v0, "UpdateActivity : onProgress"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onInstallFailed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateActivity : onInstallFailed"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    return-void
.end method

.method public b()V
    .locals 2

    const-string v0, "UpdateActivity : onDownloadFailed"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const-string v1, "onDownloadFailed"

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(I)V

    return-void
.end method

.method public c()V
    .locals 2

    const-string v0, "UpdateActivity : onDownloadCanceled"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const-string v1, "onDownloadCanceled"

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$7;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    return-void
.end method
