.class public Lcom/sec/android/app/billing/requestparam/ProductInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public currency:Ljava/lang/String;

.field public detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

.field public phonebillAmount:Ljava/lang/String;

.field public tax:Ljava/lang/String;

.field public taxIncluded:Ljava/lang/String;

.field public totalAmount:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/requestparam/ProductInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/ProductInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/ProductInfo;
    .locals 5

    new-instance v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;

    invoke-direct {v2}, Lcom/sec/android/app/billing/requestparam/ProductInfo;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->totalAmount:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->tax:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->taxIncluded:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->currency:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v0, v3

    new-array v0, v0, [Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-lt v1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->phonebillAmount:Ljava/lang/String;

    return-object v2

    :cond_1
    iget-object v4, v2, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aget-object v0, v3, v1

    check-cast v0, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->totalAmount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->tax:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->taxIncluded:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->currency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/requestparam/ProductInfo;->phonebillAmount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
