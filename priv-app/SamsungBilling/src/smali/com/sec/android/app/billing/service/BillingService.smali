.class public Lcom/sec/android/app/billing/service/BillingService;
.super Landroid/app/Service;


# static fields
.field public static a:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/billing/service/BillingVO;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String; = "BillingService"

.field private static final c:I = 0x4e20

.field private static final d:I = 0x4e33

.field private static final e:I = 0x1

.field private static final f:I = 0x2

.field private static final g:I = 0x3

.field private static final h:I = 0x4

.field private static final i:I = 0x5

.field private static final j:I = 0x6

.field private static final k:Ljava/lang/String; = "PACKAGE_NAME"

.field private static final l:Ljava/lang/String; = "REQUEST_CODE"

.field private static final m:Ljava/lang/String; = "REQUEST_ACTION"

.field private static final n:Ljava/lang/String; = "REQUEST_DATA"

.field private static final o:Ljava/lang/String; = "RESPONSE_CODE"

.field private static final p:Ljava/lang/String; = "RESPONSE_MESSAGE"

.field private static final q:Ljava/lang/String; = "BILLING_INTENT"

.field private static final r:Ljava/lang/String; = "BILLING_INTENT_EXIST"


# instance fields
.field private s:Landroid/content/Intent;

.field private t:Landroid/app/PendingIntent;

.field private u:Landroid/os/Bundle;

.field private v:Lcom/sec/android/app/billing/service/BillingVO;

.field private w:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

.field private x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

.field private y:Lcom/sec/android/app/billing/requestparam/GiftCardData;

.field private final z:Lcom/sec/android/app/billing/service/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/sec/android/app/billing/service/BillingService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/service/BillingService$1;-><init>(Lcom/sec/android/app/billing/service/BillingService;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->z:Lcom/sec/android/app/billing/service/b;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/BillingService;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->u:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/BillingService;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/service/BillingService;->u:Landroid/os/Bundle;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/BillingService;Lcom/sec/android/app/billing/service/BillingVO;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/service/BillingService;->v:Lcom/sec/android/app/billing/service/BillingVO;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/BillingService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/service/BillingService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BillingService : startSilentInstallService() : action = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/billing/service/SilentInstallService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/service/BillingService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/service/BillingService;)Lcom/sec/android/app/billing/service/BillingVO;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->v:Lcom/sec/android/app/billing/service/BillingVO;

    return-object v0
.end method


# virtual methods
.method a(Landroid/os/Bundle;)I
    .locals 4

    const/4 v1, 0x0

    const-string v0, "REQUEST_CODE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "BillingService : getRequestCodeFromBundle() : REQUEST_CODE is null."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    instance-of v2, v0, Ljava/lang/Long;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : getRequestCodeFromBundle() : REQUEST_CODE is not Integer or Long. ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method a(ILandroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/16 v3, 0x4e20

    if-ge p1, v3, :cond_1

    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The apiVersion value (request parameter) is invalid."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The apiVersion value (request parameter) is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p2, :cond_2

    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The requestData value (request parameter) is null."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The requestData value (request parameter) is null."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "PACKAGE_NAME"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BillingService : packageName = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The PACKAGE_NAME value in requestData (request parameter) is null or empty."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The PACKAGE_NAME value in requestData (request parameter) is null or empty."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, p2}, Lcom/sec/android/app/billing/service/BillingService;->a(Landroid/os/Bundle;)I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BillingService : requestCode = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-nez v3, :cond_5

    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The REQUEST_CODE value in requestData (request parameter) is null or 0 or invalid type."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The REQUEST_CODE value in requestData (request parameter) is null or 0 or invalid type."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v3, "REQUEST_ACTION"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BillingService : action = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The REQUEST_ACTION value in requestData (request parameter) is null or empty."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The REQUEST_ACTION value in requestData (request parameter) is null or empty."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v4, "PAYMENT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "CREDIT_CARD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "GIFT_CARD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "REGISTER_CREDIT_CARD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "SILENT_INSTALL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The REQUEST_ACTION value in requestData (request parameter) is null or empty."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The REQUEST_ACTION value in requestData (request parameter) is invalid."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v4, "SILENT_INSTALL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "REQUEST_DATA"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_9
    const-string v1, "BillingService : checkRequestParameter() : [AIDL_SENDING_RESULT_FAIL] The REQUEST_DATA value in requestData (request parameter) is null or empty."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "RESPONSE_MESSAGE"

    const-string v2, "The REQUEST_DATA value in requestData (request parameter) is null or empty."

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/billing/service/BillingService;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v1, :cond_0

    const-string v4, "REGISTER_CREDIT_CARD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v3, p0, Lcom/sec/android/app/billing/service/BillingService;->s:Landroid/content/Intent;

    const-string v3, "BILLING_INTENT_EXIST"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_2
    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/billing/service/BillingService;->s:Landroid/content/Intent;

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/billing/service/BillingService;->t:Landroid/app/PendingIntent;

    :cond_b
    const-string v1, "BILLING_INTENT"

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService;->t:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_0

    :cond_c
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "RESPONSE_MESSAGE"

    const-string v5, "The all values of the request parameter is valid."

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_d
    const-string v4, "SILENT_INSTALL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x4e33

    if-ge p1, v3, :cond_e

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/billing/update/BlankActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v3, p0, Lcom/sec/android/app/billing/service/BillingService;->s:Landroid/content/Intent;

    const-string v3, "BILLING_INTENT_EXIST"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_e
    const-string v1, "BILLING_INTENT_EXIST"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move v1, v2

    goto :goto_2

    :cond_f
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v3, p0, Lcom/sec/android/app/billing/service/BillingService;->s:Landroid/content/Intent;

    const-string v3, "BILLING_INTENT_EXIST"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    const/4 v4, 0x2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    new-instance v0, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v0}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    :try_start_0
    const-string v2, "PAYMENT"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-class v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-virtual {v0, p2, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->w:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->w:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-static {v0, p0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->w:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    const-string v0, "UNIFIED_PAYMENT_REQUEST"

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService;->w:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "BillingService : checkRequestParameterValidation() : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " : checkJsonDataToPrint = *****"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/codehaus/jackson/map/JsonMappingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    const-string v0, "RESPONSE_CODE"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "RESPONSE_MESSAGE"

    const-string v2, "The all values of the request parameter is valid."

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    const-string v2, "CREDIT_CARD"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-class v2, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {v0, p2, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {v0, p0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    const-string v0, "CREDIT_CARD_REQUEST"

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_1
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/codehaus/jackson/map/JsonMappingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][JsonParseException] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    :try_start_2
    const-string v2, "GIFT_CARD"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-class v2, Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-virtual {v0, p2, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->y:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->y:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-static {v0, p0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/GiftCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/GiftCardData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->y:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    const-string v0, "GIFT_CARD_REQUEST"

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService;->y:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_2
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/codehaus/jackson/map/JsonMappingException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/codehaus/jackson/map/JsonMappingException;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][JsonMappingException] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/codehaus/jackson/map/JsonMappingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Lorg/codehaus/jackson/map/JsonMappingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_3
    :try_start_3
    const-string v2, "REGISTER_CREDIT_CARD"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-class v2, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {v0, p2, v2}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-static {v0, p0}, Lcom/sec/android/app/billing/requestparam/RequestParamValidator;->check(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    const-string v0, "CREDIT_CARD_REQUEST"

    iget-object v2, p0, Lcom/sec/android/app/billing/service/BillingService;->x:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_3
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/codehaus/jackson/map/JsonMappingException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][IOException] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/b/j;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][UnifiedPaymentException] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/billing/b/j;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Lcom/sec/android/app/billing/b/j;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][NullPointerException] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BillingService : checkRequestParameterValidation() : [AIDL_SENDING_RESULT_FAIL][Exception] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "RESPONSE_MESSAGE"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const-string v0, "BillingService : onBind()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingService;->z:Lcom/sec/android/app/billing/service/b;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "BillingService : onCreate()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/billing/update/k;->b(Landroid/content/Context;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "BillingService : onDestroy()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BillingService : onDestroy() : activityList.size() = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    sget-object v0, Lcom/sec/android/app/billing/service/BillingService;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "BillingService : onUnbind()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method
