.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    return-object v0
.end method


# virtual methods
.method public alert(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$34;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$34;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bangoPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$10;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$10;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public bangoPayStart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$9;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$9;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public blockBackKey()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    return-void
.end method

.method public cacPayEncrypt(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "cacPayEncrypt called"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$4;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public callBrowser(Ljava/lang/String;)Z
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "callBrowser url : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivity(Landroid/content/Intent;)V

    const-string v0, "callBrowser startActivity"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "callBrowser url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelCreditCard()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$40;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$40;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public cancelPayment()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$21;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$21;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public checkSession()Ljava/lang/String;
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "N"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "KOR"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->o(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "APP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkSession(), isValid ? : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const-string v0, "false"

    goto :goto_1

    :cond_2
    const-string v0, "true"

    goto :goto_1
.end method

.method public gcb06Pay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$13;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$13;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public gcb06PayStart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$11;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$11;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public gcbPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$6;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$6;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public gcbPayStart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$5;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getAndroidVersion()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Platform Version]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateFormat()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Date format : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getMobileDataStatus()Ljava/lang/String;
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "getMobileDataStatus()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getMobileDataEnabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "getMobileDataStatus() data enable"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-string v0, "1"

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "getMobileDataStatus() data disable"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const-string v0, "0"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    const-string v0, "2"

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, "0"

    goto :goto_0
.end method

.method public getMsisdn()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRecentPayment(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[getRecentPayment] data : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "recent_payment_key"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v1, "recent_payment_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnifiedPaymentData()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->i(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    invoke-static {}, Lcom/sec/android/app/billing/u;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getVersion() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method public gppPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$17;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public hideLoadingAnimation()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public iccRegisterCard(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$32;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v7, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$14;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$14;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$15;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$15;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public installAlipay()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$30;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$30;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public installSwallet()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$31;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$31;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public isInstalledApplication()Z
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v0, 0x1

    const-string v1, "in isInstalledApplication()"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    const-string v2, "com.sec.android.wallet"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInstalledApplication() return availableWallet : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return v0

    :catch_0
    move-exception v1

    const-string v1, "isInstalledApplication() exception"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;-><init>(Landroid/content/Context;)V

    const-string v2, "isInstalledApplication() new CheckWalletAvaliable"

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->canLinkWallet()I

    move-result v1

    if-ne v1, v0, :cond_0

    const-string v1, "isInstalledApplication() wallet enable"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "isInstalledApplication() wallet disable"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMobileDataEnabled()Z
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const/4 v1, 0x0

    const-string v0, "isMobileDataEnabled()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getMobileDataEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0
.end method

.method public jwdPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$26;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$26;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public kcbSendSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "kcbSendSMS smsMO : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", smsMessage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPrevRequestID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->v(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->v(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->v(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "kcbSendSMS duplicate sms sending"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$35;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$35;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0, p3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/r;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/sec/android/app/billing/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$36;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$36;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Lcom/sec/android/app/billing/r;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v2, v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$37;

    invoke-direct {v3, p0, v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$37;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Lcom/sec/android/app/billing/r;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public kccPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$42;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public kctPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$20;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$20;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public kpiMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v11, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$16;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$16;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public kpiPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$3;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public kupPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$2;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public loadComplete()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "call loadComplete()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->s(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$22;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public loadPage(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$33;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$33;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public openOnNewActivity(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public openOnNewActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$39;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$39;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public psmsPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$18;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$18;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public receiveCreditCardResult(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$41;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$41;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$23;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$23;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public receivePaymentPendingResult()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$12;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public receivePaymentResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public registerCreditCard()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "registerCreditCard()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const-class v2, Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->l(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "UnifiedPaymentMainActivity registerCreditCard() : mCreditCardRequest is null."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/CreditCardData;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->language:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/requestparam/BillingServerInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    const-string v2, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/16 v2, 0x65

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const-string v1, "registerCreditCard() : mCreditCardRequest is not null."

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    const-string v1, "CREDIT_CARD_REQUEST"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->l(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/CreditCardData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public releaseBackKey()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Z)V

    return-void
.end method

.method public setMobileDataEnabled()V
    .locals 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    const-string v0, "setMobileDataEnabled()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mService"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "setMobileDataEnabled"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setPGURLs(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[setPGURLs] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f:Ljava/util/ArrayList;

    const-string v0, "\\|"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v2, v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f:Ljava/util/ArrayList;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PGURLList["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setRecentPayment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[setRecentPayment] payment : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "recent_payment_key"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public slgPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$19;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$19;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public startQiwi(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$29;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$29;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public startSINA(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$28;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$28;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public startWallet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    const-string v3, "com.sec.android.wallet"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    const-string v2, "[S Wallet] Wallet was installed"

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "com.sec.android.wallet.PAYMENT_COMPLETED_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SWALLET_BROADCAST_CODE_APPSERVICEID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->t(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->t(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->u(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v6, "wallet://com.sec.android.wallet?"

    const-string v2, "mobile.instore"

    const-string v3, "payment"

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    array-length v4, v4

    iget-object v5, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v5, v5, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v5, v5, v7

    iget-object v5, v5, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    const-string v13, ""

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/sec/android/app/billing/requestparam/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    iget-object v13, v4, Lcom/sec/android/app/billing/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, p10

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    move-object/from16 v0, p6

    move-object/from16 v1, p10

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    move-object/from16 v0, p10

    invoke-static {v5, v0}, Lcom/sec/android/app/billing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p10

    invoke-static {v13, v0}, Lcom/sec/android/app/billing/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    move-object v12, v5

    move-object/from16 v9, p6

    move-object/from16 v5, p2

    :goto_0
    :try_start_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v14, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-static/range {v2 .. v13}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[S Wallet] Wallet mPostUrl : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v12, v5

    move-object/from16 v9, p6

    move-object/from16 v5, p2

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v2, "[S Wallet] Wallet was not installed"

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    new-instance v3, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$24;

    invoke-direct {v3, p0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$24;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    const-string v2, "[S Wallet] reInit()"

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public telefonicaPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$8;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$8;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public telefonicaPayStart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$7;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public wcbPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$27;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$27;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public webMoneyPay(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->g:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$25;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$25;-><init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
