.class Lcom/sec/android/app/billing/requestparam/PaymentInfo$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/requestparam/PaymentInfo;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/PaymentInfo;
    .locals 1

    invoke-static {p1}, Lcom/sec/android/app/billing/requestparam/PaymentInfo;->readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/app/billing/requestparam/PaymentInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/billing/requestparam/PaymentInfo;
    .locals 1

    new-array v0, p1, [Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/sec/android/app/billing/requestparam/PaymentInfo$1;->newArray(I)[Lcom/sec/android/app/billing/requestparam/PaymentInfo;

    move-result-object v0

    return-object v0
.end method
