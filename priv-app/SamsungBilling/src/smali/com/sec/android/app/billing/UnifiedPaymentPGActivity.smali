.class public Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;
.super Landroid/app/Activity;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "UnifiedPaymentPGResultActivity"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->a:Ljava/lang/String;

    const-string v0, "ISP_OK"

    iput-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    const-string v0, "UnifiedPaymentPGResultActivity finish"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-object v0, Lcom/sec/android/app/billing/u;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGResultActivity onCreate : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "UnifiedPaymentPGResultActivity, data is null"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "APKUPReadersHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPLearningHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPMediaHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPVideoHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPMusicHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPSamsungCloud://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "APKUPSamsungApps://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "ISP_SUCCESS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UnifiedPaymentPGResultActivity, ISP_SUCCESS : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "ISP_OK"

    sput-object v0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/billing/UnifiedPaymentPGActivity;->finish()V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentPGResultActivity, no data"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "UnifiedPaymentPGResultActivity onDestroy"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "UnifiedPaymentPGResultActivity onResume"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    return-void
.end method
