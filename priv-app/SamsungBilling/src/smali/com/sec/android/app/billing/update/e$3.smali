.class Lcom/sec/android/app/billing/update/e$3;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/billing/update/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Landroid/os/Handler;I)V
.end annotation


# instance fields
.field private final synthetic a:Landroid/os/Handler;

.field private final synthetic b:Landroid/content/Context;

.field private final synthetic c:I


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/sec/android/app/billing/update/e$3;->b:Landroid/content/Context;

    iput p3, p0, Lcom/sec/android/app/billing/update/e$3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 5

    const/16 v4, 0x136

    const/16 v0, 0xc8

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK NOT : RESPONSE_CODE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK NOT : RESPONSE_MESSAGE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseMessage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    const/16 v1, 0x131

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "responseMessage"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK : RESPONSE_CODE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "STUB_UPDATE_CHECK"

    const-string v1, "responseMessage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/e;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "versionCode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK : responseVersionCode = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v0, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK : responseVersionCode is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->b:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;J)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->b:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/billing/update/e$3;->b:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;J)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$3;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;I)V

    iget v1, p0, Lcom/sec/android/app/billing/update/e$3;->c:I

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/update/e;->a(II)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateAdapter : checkVersionCodeFromSamsungAppsServer() : onRequestStubUpdateCheckFinished() : HttpStatus.SC_OK : needToUpdate = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$3;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
