.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resStr : *****, signature : *****"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    const/4 v3, 0x0

    const-string v1, ""

    const-string v2, ""

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->c:Ljava/lang/String;

    invoke-static {v0}, Lorg/a/a/f;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lorg/a/a/d;

    move-object v3, v0

    :cond_0
    if-eqz v3, :cond_4

    const-string v0, "userID"

    invoke-virtual {v3, v0}, Lorg/a/a/d;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "userID"

    invoke-virtual {v3, v0}, Lorg/a/a/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :cond_1
    const-string v0, "paymentMethod"

    invoke-virtual {v3, v0}, Lorg/a/a/d;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "paymentMethod"

    invoke-virtual {v3, v0}, Lorg/a/a/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    const-string v2, "FCD"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "GCC"

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[receivePaymentResult] paymentMethod : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_3

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "CPN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->j(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->setRecentPayment(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const-string v0, "PAYMENT_RECEITE"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->c:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "SIGNATURE"

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->d:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$1;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    return-void

    :cond_4
    move-object v0, v2

    goto :goto_0
.end method
