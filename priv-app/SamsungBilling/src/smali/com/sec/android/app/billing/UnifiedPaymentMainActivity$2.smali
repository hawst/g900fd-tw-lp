.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->e(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->o(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "confirmPassword() : samsungAccountType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v1, "APP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SamsungAccount] version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const v1, 0x2244e

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.CONFIRM_PASSWORD_POPUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    const-string v2, "asdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "more_info"

    const-string v2, "netflix"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "theme"

    const-string v2, "light"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_ secret"

    const-string v2, "asdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    const-string v2, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "more_info"

    const-string v2, "netflix"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.billing.action.ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "clientId"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "accessToken"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "isConfirmPassword"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/16 v2, 0xc8

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$2;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
