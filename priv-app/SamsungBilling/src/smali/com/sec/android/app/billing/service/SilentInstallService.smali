.class public Lcom/sec/android/app/billing/service/SilentInstallService;
.super Landroid/app/IntentService;


# static fields
.field private static final b:Ljava/lang/String; = "SilentInstallService"


# instance fields
.field a:Landroid/os/Handler;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SilentInstallService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/app/billing/service/SilentInstallService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/service/SilentInstallService$1;-><init>(Lcom/sec/android/app/billing/service/SilentInstallService;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    return-void
.end method

.method private a()V
    .locals 1

    const-string v0, "SilentInstallService : deleteDownloadFile()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/billing/update/k;->b(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/SilentInstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/service/SilentInstallService;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/service/SilentInstallService;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/service/SilentInstallService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    const-string v0, "SilentInstallService : installUPClient()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    return-void
.end method

.method private b()V
    .locals 1

    const-string v0, "SilentInstallService : downloadFromSamsungAppsServer()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/e;->b(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/billing/service/SilentInstallService;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/billing/service/SilentInstallService;->a()V

    return-void
.end method

.method private c()V
    .locals 1

    const-string v0, "SilentInstallService : notifyAPKInstalled()"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/billing/b/b;->g(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    const/16 v3, 0x131

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "com.sec.android.app.billing.service.SilentInstallByAlarmManger"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/billing/service/SilentInstallService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/b;->f(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SilentInstallService : onHandleIntent() : INTENT_ACTION_SILENT_INSTALL_BY_ALARMMANGER :  The isWifiConnected is false. return."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SilentInstallService : onHandleIntent() : INTENT_ACTION_SILENT_INSTALL_BY_ALARMMANGER :  The isWifiConnected is true. continue."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/billing/service/SilentInstallService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.billing"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/update/k;->a(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->c:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SilentInstallService : onHandleIntent() : mIsUserApp = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    iget-boolean v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->c:Z

    if-nez v0, :cond_3

    const-string v0, "up.updateNo.mode"

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/k;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-static {p0, v0}, Lcom/sec/android/app/billing/update/e;->a(Landroid/content/Context;Landroid/os/Handler;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SilentInstallService : onHandleIntent() : action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SilentInstallService : onHandleIntent() : NameNotFoundException = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-static {v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SilentInstallService : onHandleIntent() : Exception = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-static {v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/SilentInstallService;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_3
    const-string v0, "SilentInstallService : onHandleIntent() : mIsUserApp == true : Silent Install is skipped."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
