.class Lcom/sec/android/app/billing/ab$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/ab;->a(Landroid/webkit/WebView;Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/ab;

.field private final synthetic b:Ljava/util/Hashtable;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Landroid/webkit/WebView;

.field private final synthetic e:Ljava/util/Hashtable;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/ab;Ljava/util/Hashtable;Ljava/lang/String;Landroid/webkit/WebView;Ljava/util/Hashtable;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/ab$3;->a:Lcom/sec/android/app/billing/ab;

    iput-object p2, p0, Lcom/sec/android/app/billing/ab$3;->b:Ljava/util/Hashtable;

    iput-object p3, p0, Lcom/sec/android/app/billing/ab$3;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/billing/ab$3;->d:Landroid/webkit/WebView;

    iput-object p5, p0, Lcom/sec/android/app/billing/ab$3;->e:Ljava/util/Hashtable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$3;->b:Ljava/util/Hashtable;

    iget-object v1, p0, Lcom/sec/android/app/billing/ab$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "<INIPAYMOBILE>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Call : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/ab$3;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$3;->a:Lcom/sec/android/app/billing/ab;

    invoke-static {v0}, Lcom/sec/android/app/billing/ab;->a(Lcom/sec/android/app/billing/ab;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$3;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/billing/ab$3;->e:Ljava/util/Hashtable;

    iget-object v3, p0, Lcom/sec/android/app/billing/ab$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\uc124\uce58 url\uc774 \uc62c\ubc14\ub974\uc9c0 \uc54a\uc2b5\ub2c8\ub2e4"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
