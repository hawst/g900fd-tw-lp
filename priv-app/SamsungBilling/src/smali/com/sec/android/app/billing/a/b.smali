.class public Lcom/sec/android/app/billing/a/b;
.super Ljava/lang/Object;


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field b:Ljava/lang/Integer;

.field c:Lcom/alipay/android/app/IAlixPay;

.field d:Z

.field e:Landroid/app/Activity;

.field private f:Landroid/content/ServiceConnection;

.field private g:Lcom/alipay/android/app/IRemoteServiceCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "MobileSecurePayer"

    sput-object v0, Lcom/sec/android/app/billing/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/billing/a/b;->b:Ljava/lang/Integer;

    iput-object v2, p0, Lcom/sec/android/app/billing/a/b;->c:Lcom/alipay/android/app/IAlixPay;

    iput-boolean v1, p0, Lcom/sec/android/app/billing/a/b;->d:Z

    iput-object v2, p0, Lcom/sec/android/app/billing/a/b;->e:Landroid/app/Activity;

    new-instance v0, Lcom/sec/android/app/billing/a/b$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/a/b$1;-><init>(Lcom/sec/android/app/billing/a/b;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/a/b;->f:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/sec/android/app/billing/a/b$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/billing/a/b$2;-><init>(Lcom/sec/android/app/billing/a/b;)V

    iput-object v0, p0, Lcom/sec/android/app/billing/a/b;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/billing/a/b;)Lcom/alipay/android/app/IRemoteServiceCallback;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/a/b;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    :goto_1
    return v2

    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v4, "com.alipay.android.app"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/billing/a/b;)Landroid/content/ServiceConnection;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/billing/a/b;->f:Landroid/content/ServiceConnection;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/sec/android/app/billing/a/b;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/billing/a/b;->d:Z

    iput-object p4, p0, Lcom/sec/android/app/billing/a/b;->e:Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/android/app/billing/a/b;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/billing/a/b;->e:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/alipay/android/app/IAlixPay;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/billing/a/b;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/billing/a/b$3;

    invoke-direct {v2, p0, p1, p3, p2}, Lcom/sec/android/app/billing/a/b$3;-><init>(Lcom/sec/android/app/billing/a/b;Ljava/lang/String;ILandroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
