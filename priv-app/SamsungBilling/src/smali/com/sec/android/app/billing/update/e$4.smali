.class Lcom/sec/android/app/billing/update/e$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/billing/update/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/update/e;->b(Landroid/content/Context;Landroid/os/Handler;)V
.end annotation


# instance fields
.field private final synthetic a:Landroid/os/Handler;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/sec/android/app/billing/update/e$4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 7

    const/4 v3, 0x0

    const/16 v5, 0x131

    const/16 v0, 0xc8

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : HttpStatus.SC_OK NOT : RESPONSE_CODE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : HttpStatus.SC_OK NOT : RESPONSE_MESSAGE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseMessage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-static {v0, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "responseMessage"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : HttpStatus.SC_OK : RESPONSE_CODE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    const-string v0, "STUB_DOWNLOAD"

    const-string v1, "responseMessage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/update/e;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "contentSize"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : responseContentSize = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v0, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : responseContentSize is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-static {v0, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "onRequestStubDownloadFinished() : responseContentSize is null or empty."

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    const-string v0, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : contentSize is 0."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-static {v0, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "onRequestStubDownloadFinished() : contentSize is 0."

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_4
    const-string v1, "downloadURI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    const-string v0, "UpdateAdapter : downloadFromSamsungAppsServer() : onRequestStubDownloadFinished() : responseDownloadUri is null or empty."

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-static {v0, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "responseCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "onRequestStubDownloadFinished() : responseDownloadUri is null or empty."

    invoke-static {v1, v2}, Lcom/sec/android/app/billing/update/k;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    const/16 v4, 0x12d

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/billing/update/e$4;->b:Ljava/lang/String;

    new-instance v5, Lcom/sec/android/app/billing/update/f;

    const/4 v4, 0x0

    invoke-direct {v5, v4}, Lcom/sec/android/app/billing/update/f;-><init>(Lcom/sec/android/app/billing/update/f;)V

    iget-object v6, p0, Lcom/sec/android/app/billing/update/e$4;->a:Landroid/os/Handler;

    move v4, v3

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/billing/update/e;->a(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/Object;Landroid/os/Handler;)V

    goto/16 :goto_0
.end method
