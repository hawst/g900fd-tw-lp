.class Lcom/sec/android/app/billing/CreditCardActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/CreditCardActivity;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/CreditCardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/CreditCardActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-string v0, "over 1 minute oneMinuteCheckRunnable"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->c(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0001"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    const-string v2, "Waiting time has been exceeded."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    iget-object v2, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const v3, 0x7f060009

    invoke-virtual {v2, v3}, Lcom/sec/android/app/billing/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/billing/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->e(Lcom/sec/android/app/billing/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/CreditCardActivity$2;->a:Lcom/sec/android/app/billing/CreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/CreditCardActivity;->finish()V

    return-void
.end method
