.class public Lcom/sec/android/app/billing/service/BillingVO;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lorg/codehaus/jackson/annotate/JsonIgnoreProperties;
    ignoreUnknown = true
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

.field public h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

.field public i:Lcom/sec/android/app/billing/requestparam/GiftCardData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/billing/service/BillingVO$1;

    invoke-direct {v0}, Lcom/sec/android/app/billing/service/BillingVO$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/service/BillingVO;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Parcel;)Lcom/sec/android/app/billing/service/BillingVO;
    .locals 2

    new-instance v1, Lcom/sec/android/app/billing/service/BillingVO;

    invoke-direct {v1}, Lcom/sec/android/app/billing/service/BillingVO;-><init>()V

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->a:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->b:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->c:I

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->d:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->e:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->f:Ljava/lang/String;

    const-class v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    const-class v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    const-class v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/billing/requestparam/GiftCardData;

    iput-object v0, v1, Lcom/sec/android/app/billing/service/BillingVO;->i:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->g:Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->h:Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/service/BillingVO;->i:Lcom/sec/android/app/billing/requestparam/GiftCardData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
