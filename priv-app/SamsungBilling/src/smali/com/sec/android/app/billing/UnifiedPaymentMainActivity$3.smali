.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->f(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/a;->a(Landroid/content/Context;)Lcom/sec/android/app/billing/a;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->p(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->q(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->r(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/billing/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_ secret"

    const-string v2, "asdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_mode"

    const-string v2, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "more_info"

    const-string v2, "netflix"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->c(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$3;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0
.end method
