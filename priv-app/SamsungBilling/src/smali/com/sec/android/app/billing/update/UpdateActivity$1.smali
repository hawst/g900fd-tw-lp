.class Lcom/sec/android/app/billing/update/UpdateActivity$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/billing/update/UpdateActivity;
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/update/UpdateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/update/UpdateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : handleMessage : msg.what = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : default : msg.what = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->b(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpdateActivity : DOWNLOAD_STARTED : Progress = 0"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const v1, 0x7f080002

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/billing/update/UpdateActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->b(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : DOWNLOAD_PROGRESS : Progress = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "UpdateActivity : DOWNLOAD_FINISHED : installUPClient() : start"

    invoke-static {v1}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v1, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;Ljava/lang/String;)V

    const-string v0, "UpdateActivity : DOWNLOAD_FINISHED : installUPClient() : end"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_2
    const-string v0, "UpdateActivity : DOWNLOAD_ERROR : deleteDownloadFile() : start"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->d(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    const-string v0, "UpdateActivity : DOWNLOAD_ERROR : deleteDownloadFile() : end"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : DOWNLOAD_ERROR : ERROR_ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : DOWNLOAD_ERROR : ERROR_MESSAGE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_MESSAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ERROR_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v1, v4, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->a(Lcom/sec/android/app/billing/update/UpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->c(Lcom/sec/android/app/billing/update/UpdateActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_3
    const-string v0, "UpdateActivity : UNKNOWN_ERROR : deleteDownloadFile() : start"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-static {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->d(Lcom/sec/android/app/billing/update/UpdateActivity;)V

    const-string v0, "UpdateActivity : UNKNOWN_ERROR : deleteDownloadFile() : end"

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : UNKNOWN_ERROR : ERROR_ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : UNKNOWN_ERROR : ERROR_MESSAGE = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ERROR_MESSAGE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "ERROR_ID"

    const-string v2, "0002"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ERROR_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v1, v4, v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateActivity : INSTALL_APK : msg.what = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/billing/update/UpdateActivity;->setResult(I)V

    iget-object v0, p0, Lcom/sec/android/app/billing/update/UpdateActivity$1;->a:Lcom/sec/android/app/billing/update/UpdateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/billing/update/UpdateActivity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
