.class Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->openOnNewActivity(Ljava/lang/String;)V
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->b:Ljava/lang/String;

    const-string v1, "CREDIT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;

    invoke-direct {v0}, Lcom/sec/android/app/billing/requestparam/CreditCardData;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->country:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/requestparam/DeviceInfo;->language:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->language:Ljava/lang/String;

    new-instance v1, Lcom/sec/android/app/billing/requestparam/UserInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/requestparam/UserInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userID:Ljava/lang/String;

    iget-object v1, v0, Lcom/sec/android/app/billing/requestparam/CreditCardData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->k(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;)Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/requestparam/UserInfo;

    iget-object v2, v2, Lcom/sec/android/app/billing/requestparam/UserInfo;->userName:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/requestparam/UserInfo;->userName:Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/billing/u;->a(Lcom/sec/android/app/billing/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    :try_end_0
    .catch Lcom/sec/android/app/billing/b/j; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->a(Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/b/j;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge$38;->b:Ljava/lang/String;

    const-string v1, "GIFT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method
