.class public Lcom/sec/android/app/billing/aa;
.super Lcom/sec/android/app/billing/y;


# static fields
.field private static final c:Ljava/lang/String; = "[Bridge Call] "


# instance fields
.field a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

.field private b:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebView;Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/billing/y;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/sec/android/app/billing/aa;->b:Landroid/webkit/WebView;

    iput-object p3, p0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    const-string v0, "** UnifiedPaymentWebChromeClientNoJSForMainActivity **"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Bridge Call] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Bridge Call] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/b/i;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z
    .locals 18

    const/4 v15, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const-string v1, "** Android native call(by javascript) for MainActivity **"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 1. view ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 2. url ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 3. message ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 4. defaultValue ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 5. result ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    if-eqz p3, :cond_0

    const-string v1, "{"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "}"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v12, 0x1

    :cond_0
    if-eqz v12, :cond_41

    :try_start_0
    invoke-static/range {p3 .. p3}, Lcom/sec/android/app/billing/g;->a(Ljava/lang/String;)Lcom/sec/android/app/billing/g;

    move-result-object v16

    if-nez v16, :cond_1

    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/billing/y;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    move-result v1

    :goto_0
    return v1

    :cond_1
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->a:Ljava/lang/String;

    const-string v2, "receivePaymentResult"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v1, v2, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    :goto_1
    const-string v1, ""

    if-eqz v3, :cond_3e

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    if-eqz v4, :cond_3d

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0xa

    if-le v2, v4, :cond_3d

    invoke-static {v3}, Lcom/sec/android/app/billing/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    const-string v2, "encodedReturnString(masked)"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/webkit/JsPromptResult;->confirm(Ljava/lang/String;)V

    :goto_3
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_41

    const-string v2, ""

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_41

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->b:Landroid/webkit/WebView;

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v12

    :goto_4
    if-nez v1, :cond_40

    invoke-super/range {p0 .. p5}, Lcom/sec/android/app/billing/y;->onJsPrompt(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsPromptResult;)Z

    move-result v1

    goto/16 :goto_0

    :cond_2
    :try_start_1
    const-string v2, "receivePaymentPendingResult"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentPendingResult()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_3
    const-string v2, "receivePaymentErrorResult"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_4
    const-string v2, "alert"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->alert(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_5
    const-string v2, "openOnNewActivity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->openOnNewActivity(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->openOnNewActivity(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_7
    const-string v2, "registerCreditCard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->registerCreditCard()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_8
    const-string v2, "cancelCreditCard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->cancelCreditCard()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_9
    const-string v2, "receiveCreditCardResult"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->receiveCreditCardResult(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_a
    const-string v2, "kccPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kccPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_b
    const-string v2, "kupPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kupPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_c
    const-string v2, "kpiPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kpiPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_d
    const-string v2, "cacPayEncrypt"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->cacPayEncrypt(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_e
    const-string v2, "gcbPayStart"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gcbPayStart(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_f
    const-string v2, "gcbPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gcbPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_10
    const-string v2, "telefonicaPayStart"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->telefonicaPayStart(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_11
    const-string v2, "telefonicaPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->telefonicaPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_12
    const-string v2, "bangoPayStart"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->bangoPayStart(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_13
    const-string v2, "bangoPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->bangoPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_14
    const-string v2, "gcb06PayStart"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gcb06PayStart(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_15
    const-string v2, "gcb06Pay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gcb06Pay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_16
    const-string v2, "inicisMRCpay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_18
    const-string v2, "kpiMRCpay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/16 v11, 0x8

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/16 v17, 0x9

    move/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kpiMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_19
    const-string v2, "gppPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->gppPay(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_1a
    const-string v2, "psmsPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->psmsPay(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_1b
    const-string v2, "slgPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->slgPay(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_1c
    const-string v2, "hideLoadingAnimation"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->hideLoadingAnimation()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_1d
    const-string v2, "cancelPayment"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->cancelPayment()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_1e
    const-string v2, "getUnifiedPaymentData"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getUnifiedPaymentData()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object v3, v1

    move v4, v2

    move-object v2, v13

    goto/16 :goto_1

    :cond_1f
    const-string v2, "checkSession"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->checkSession()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_20
    const-string v2, "getVersion"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getVersion()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_21
    const-string v2, "getAndroidVersion"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getAndroidVersion()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_22
    const-string v2, "blockBackKey"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->blockBackKey()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_23
    const-string v2, "releaseBackKey"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->releaseBackKey()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_24
    const-string v2, "loadComplete"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->loadComplete()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_25
    const-string v2, "setPGURLs"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->setPGURLs(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_26
    const-string v2, "getDateFormat"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getDateFormat()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_27
    const-string v2, "startWallet"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/16 v11, 0x8

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/16 v17, 0x9

    move/from16 v0, v17

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual/range {v1 .. v11}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->startWallet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_28
    const-string v2, "webMoneyPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->webMoneyPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_29
    const-string v2, "isInstalledApplication"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->isInstalledApplication()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v2, v1

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2a
    const-string v2, "isMobileDataEnabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->isMobileDataEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v2, v1

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2b
    const-string v2, "setMobileDataEnabled"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->setMobileDataEnabled()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2c
    const-string v2, "startSINA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->startSINA(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2d
    const-string v2, "startQiwi"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->startQiwi(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2e
    const-string v2, "installAlipay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->installAlipay()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_2f
    const-string v2, "installSwallet"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->installSwallet()V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_30
    const-string v2, "iccRegisterCard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->iccRegisterCard(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_31
    const-string v2, "loadPage"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->loadPage(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_32
    const-string v2, "kcbSendSMS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v1, v2, v3}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kcbSendSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_33
    const-string v2, "showToast"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->showToast(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_34
    const-string v2, "getRecentPayment"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getRecentPayment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_35
    const-string v2, "setRecentPayment"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->setRecentPayment(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_36
    const-string v2, "getMobileDataStatus"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getMobileDataStatus()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_37
    const-string v2, "callBrowser"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_38

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->callBrowser(Ljava/lang/String;)Z

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_38
    const-string v2, "wcbPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_39

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->wcbPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_39
    const-string v2, "jwdPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->jwdPay(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_3a
    const-string v2, "kctPay"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v6, 0x3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v8, 0x5

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/sec/android/app/billing/g;->b:Ljava/util/List;

    const/4 v10, 0x7

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->kctPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_3b
    const-string v2, "getMsisdn"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/billing/aa;->a:Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v1}, Lcom/sec/android/app/billing/UnifiedPaymentMainActivity$Bridge;->getMsisdn()Ljava/lang/String;

    move-result-object v1

    move-object v2, v13

    move-object v3, v1

    move v4, v15

    goto/16 :goto_1

    :cond_3c
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Call Undefined native API from Javascript : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/billing/aa;->b(Ljava/lang/String;)V

    move-object v2, v13

    move-object v3, v14

    move v4, v15

    goto/16 :goto_1

    :cond_3d
    const-string v2, "encodedReturnString"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Native Call Fail : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/billing/aa;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_3e
    if-eqz v2, :cond_3f

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "returnBoolean : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/billing/aa;->a(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/webkit/JsPromptResult;->confirm(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_3f
    invoke-virtual/range {p5 .. p5}, Landroid/webkit/JsPromptResult;->confirm()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    :cond_40
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_41
    move v1, v12

    goto/16 :goto_4
.end method
