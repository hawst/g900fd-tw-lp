.class public Lcom/sec/android/wallet/confirm/http/ControlDataMgr;
.super Ljava/lang/Object;


# static fields
.field public static final WALLET_STATE_CAN_NOT_USE:I = 0x0

.field public static final WALLET_STATE_CAN_USE:I = 0x1

.field public static final WALLET_STATE_EXCEPTION_HTTP_ALL:I = -0x1

.field public static final WALLET_STATE_EXCEPTION_HTTP_CLIENT_PROTOCOL:I = -0x4

.field public static final WALLET_STATE_EXCEPTION_HTTP_CONNECTION_TIMEOUT:I = -0x3

.field public static final WALLET_STATE_EXCEPTION_HTTP_SOCKET_TIMEOUT:I = -0x2

.field public static final WALLET_STATE_EXCEPTION_PARSE:I = -0x6

.field public static final WALLET_STATE_EXCEPTION_PARSE_ALL:I = -0x5

.field private static mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;


# instance fields
.field private mCanUseWallet:I

.field private mIsPoling:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mIsPoling:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mCanUseWallet:I

    return-void
.end method

.method public static getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;
    .locals 1

    sget-object v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    invoke-direct {v0}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;-><init>()V

    :goto_0
    sput-object v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    sget-object v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mInst:Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    goto :goto_0
.end method


# virtual methods
.method public getPolingState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mIsPoling:Z

    return v0
.end method

.method public getUseWalletState()I
    .locals 1

    iget v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mCanUseWallet:I

    return v0
.end method

.method public setInitControlDatas()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mIsPoling:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mCanUseWallet:I

    return-void
.end method

.method public setPolingState(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mIsPoling:Z

    return-void
.end method

.method public setUseWalletState(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->mCanUseWallet:I

    return-void
.end method
