.class public Lcom/sec/android/wallet/confirm/http/HttpProtocol;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lorg/apache/http/HttpResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mCallback:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->mCallback:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->mCallback:Landroid/os/Handler;

    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->mCallback:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->doInBackground([Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const/16 v3, 0x2710

    invoke-static {v2, v3}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    aget-object v3, p1, v4

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_0
    const-string v1, ""

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v1

    :goto_1
    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    :cond_0
    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setPolingState(Z)V

    return-object v0

    :catch_0
    move-exception v1

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    const/4 v2, -0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    const/4 v2, -0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_0

    :catch_4
    move-exception v2

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v2

    const/4 v3, -0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_1

    :catch_5
    move-exception v2

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v2

    const/4 v3, -0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setUseWalletState(I)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lorg/apache/http/HttpResponse;

    invoke-virtual {p0, p1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->onPostExecute(Lorg/apache/http/HttpResponse;)V

    return-void
.end method

.method protected onPostExecute(Lorg/apache/http/HttpResponse;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->mCallback:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->mCallback:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method
