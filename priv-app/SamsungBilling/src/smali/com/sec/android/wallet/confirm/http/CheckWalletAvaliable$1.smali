.class Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->canLinkWallet(Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

.field private final synthetic val$callback:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;


# direct methods
.method constructor <init>(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;Landroid/os/Looper;Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    iput-object p3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->val$callback:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->val$callback:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;
    invoke-static {v0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$0(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$1(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$2(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$3(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$4(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$5(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$6(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->this$0:Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    # getter for: Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->access$7(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;->val$callback:Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;

    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    :cond_0
    return-void
.end method
