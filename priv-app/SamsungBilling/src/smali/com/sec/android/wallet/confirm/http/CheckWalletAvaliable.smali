.class public Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;
.super Ljava/lang/Object;


# instance fields
.field private final BASE_TARGET_STG_URL:Ljava/lang/String;

.field private final BASE_TARGET_URL:Ljava/lang/String;

.field private final mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

.field private mContext:Landroid/content/Context;

.field private mCsc:Ljava/lang/String;

.field private mDeviceId:Ljava/lang/String;

.field private mIsStg:Z

.field private mMcc:Ljava/lang/String;

.field private mMnc:Ljava/lang/String;

.field private mModuleId:Ljava/lang/String;

.field private mOpenApi:Ljava/lang/String;

.field private mTargetUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->BASE_TARGET_URL:Ljava/lang/String;

    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->BASE_TARGET_STG_URL:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    const-string v0, "PRD"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->BASE_TARGET_URL:Ljava/lang/String;

    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->BASE_TARGET_STG_URL:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "STG"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "STG"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "PRD"

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Lcom/sec/android/wallet/confirm/data/WalletStateCache;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    return-object v0
.end method

.method private getFullUrl()Ljava/lang/String;
    .locals 8

    new-instance v7, Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mIsStg:Z

    if-eqz v0, :cond_0

    const-string v0, "http://api.stg.wallet.samsung.com/cmn/isAvailable?"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->makeTargetParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "https://api.wallet.samsung.com/cmn/isAvailable?"

    goto :goto_0
.end method

.method private getParam()V
    .locals 4

    const/4 v3, 0x5

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    const-string v1, "06"

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v3, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    const-string v1, "450"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "kor"

    :goto_0
    iput-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private makeTargetParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "moduleId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&csc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&openApi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public canLinkWallet()I
    .locals 11

    const/4 v9, 0x0

    const/4 v8, 0x1

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->getParam()V

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v8, :cond_0

    move v0, v8

    :goto_0
    return v0

    :cond_0
    if-nez v0, :cond_1

    move v0, v9

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setInitControlDatas()V

    new-instance v0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;

    invoke-direct {v0}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;-><init>()V

    new-array v1, v8, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->getFullUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getPolingState()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v10}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getUseWalletState()I

    move-result v0

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public canLinkWallet(Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    new-instance v8, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v8, p0, v0, p1}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable$1;-><init>(Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;Landroid/os/Looper;Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;)V

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->getParam()V

    iget-object v0, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCache:Lcom/sec/android/wallet/confirm/data/WalletStateCache;

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mModuleId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMcc:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mMnc:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mCsc:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mOpenApi:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->mTargetUrl:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v9, :cond_0

    invoke-interface {p1, v9}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    :goto_0
    return-void

    :cond_0
    if-nez v0, :cond_1

    invoke-interface {p1, v10}, Lcom/sec/android/wallet/confirm/http/ICheckWalletCallback;->onResponseCheckWallet(I)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->getInst()Lcom/sec/android/wallet/confirm/http/ControlDataMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/wallet/confirm/http/ControlDataMgr;->setInitControlDatas()V

    new-instance v0, Lcom/sec/android/wallet/confirm/http/HttpProtocol;

    invoke-direct {v0, v8}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;-><init>(Landroid/os/Handler;)V

    new-array v1, v9, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->getFullUrl()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/http/HttpProtocol;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
