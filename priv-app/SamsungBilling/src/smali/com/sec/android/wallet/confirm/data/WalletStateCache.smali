.class public Lcom/sec/android/wallet/confirm/data/WalletStateCache;
.super Ljava/lang/Object;


# static fields
.field public static final INVALID_CACHED_WALLET_STATE:I = -0x1


# instance fields
.field private final ONE_DAY_MILLIS:J

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->ONE_DAY_MILLIS:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    return-void
.end method

.method private isSameCachedParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getModuleId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getMcc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getMnc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getCsc(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getOpenApi(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/wallet/confirm/data/Preference;->getTargetUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public checkCachedResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    const/4 v0, -0x1

    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->getUseWalletState(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/wallet/confirm/data/Preference;->getCachedTime(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/32 v4, 0x240c8400

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    invoke-direct/range {p0 .. p7}, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->isSameCachedParam(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    if-nez v1, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public saveCacheResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    invoke-static {}, Lcom/sec/android/wallet/confirm/data/Preference;->getInst()Lcom/sec/android/wallet/confirm/data/Preference;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->setUseWalletState(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/wallet/confirm/data/Preference;->setModuleId(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/wallet/confirm/data/Preference;->setMcc(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/wallet/confirm/data/Preference;->setMnc(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/wallet/confirm/data/Preference;->setCsc(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p5}, Lcom/sec/android/wallet/confirm/data/Preference;->setDeviceId(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p6}, Lcom/sec/android/wallet/confirm/data/Preference;->setOpenApi(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p7}, Lcom/sec/android/wallet/confirm/data/Preference;->setTargetUrl(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/sec/android/wallet/confirm/data/WalletStateCache;->mContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/wallet/confirm/data/Preference;->setCachedTime(Landroid/content/Context;Ljava/lang/String;)Z

    return-void
.end method
