.class public Lorg/a/a/a/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field public static final e:I = 0x4

.field public static final f:I = 0x5

.field public static final g:I = 0x6

.field public static final h:I = -0x1


# instance fields
.field private i:Ljava/util/LinkedList;

.field private j:Lorg/a/a/a/e;

.field private k:Lorg/a/a/a/f;

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lorg/a/a/a/e;

    move-object v0, v1

    check-cast v0, Ljava/io/Reader;

    invoke-direct {v2, v0}, Lorg/a/a/a/e;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lorg/a/a/a/c;->j:Lorg/a/a/a/e;

    iput-object v1, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    const/4 v0, 0x0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    return-void
.end method

.method private a(Ljava/util/LinkedList;)I
    .locals 1

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private a(Lorg/a/a/a/a;)Ljava/util/Map;
    .locals 1

    if-nez p1, :cond_1

    new-instance v0, Lorg/a/a/d;

    invoke-direct {v0}, Lorg/a/a/d;-><init>()V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lorg/a/a/a/a;->a()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/a/a/d;

    invoke-direct {v0}, Lorg/a/a/d;-><init>()V

    goto :goto_0
.end method

.method private b(Lorg/a/a/a/a;)Ljava/util/List;
    .locals 1

    if-nez p1, :cond_1

    new-instance v0, Lorg/a/a/b;

    invoke-direct {v0}, Lorg/a/a/b;-><init>()V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lorg/a/a/a/a;->b()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/a/a/b;

    invoke-direct {v0}, Lorg/a/a/b;-><init>()V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Lorg/a/a/a/c;->j:Lorg/a/a/a/e;

    invoke-virtual {v0}, Lorg/a/a/a/e;->f()Lorg/a/a/a/f;

    move-result-object v0

    iput-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    if-nez v0, :cond_0

    new-instance v0, Lorg/a/a/a/f;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/a/a/a/f;-><init>(ILjava/lang/Object;)V

    iput-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/io/Reader;Lorg/a/a/a/a;)Ljava/lang/Object;
    .locals 7

    const/4 v6, -0x1

    const/4 v5, 0x1

    invoke-virtual {p0, p1}, Lorg/a/a/a/c;->a(Ljava/io/Reader;)V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget v0, p0, Lorg/a/a/a/c;->l:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget v0, p0, Lorg/a/a/a/c;->l:I

    if-ne v0, v6, :cond_5

    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    throw v0

    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->a(Lorg/a/a/a/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->b(Lorg/a/a/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    if-ne v0, v6, :cond_1

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0

    :pswitch_7
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_2

    :pswitch_8
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    const/4 v0, 0x4

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_a
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v5, :cond_3

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v2}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_3

    :pswitch_c
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_d
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    iget-object v4, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v4, v4, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v2}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_e
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->b(Lorg/a/a/a/a;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->a(Lorg/a/a/a/a;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_4

    :pswitch_11
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_12
    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v1, v1, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_13
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v5, :cond_4

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v2}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_0

    :pswitch_14
    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->a(Lorg/a/a/a/a;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v4, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_15
    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, p2}, Lorg/a/a/a/c;->b(Lorg/a/a/a/a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v4, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto/16 :goto_0

    :pswitch_16
    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-ne v0, v6, :cond_0

    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    iget-object v2, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v5, v2}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_16
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_10
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_d
        :pswitch_f
        :pswitch_c
        :pswitch_e
        :pswitch_c
        :pswitch_c
        :pswitch_0
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_12
        :pswitch_14
        :pswitch_11
        :pswitch_15
        :pswitch_13
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, Lorg/a/a/a/a;

    invoke-virtual {p0, p1, v0}, Lorg/a/a/a/c;->a(Ljava/lang/String;Lorg/a/a/a/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lorg/a/a/a/a;)Ljava/lang/Object;
    .locals 4

    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0, v0, p2}, Lorg/a/a/a/c;->a(Ljava/io/Reader;Lorg/a/a/a/a;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/a/a/a/d;

    const/4 v2, -0x1

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v0}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v1
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    const/4 v0, 0x0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    iput-object v1, p0, Lorg/a/a/a/c;->i:Ljava/util/LinkedList;

    return-void
.end method

.method public a(Ljava/io/Reader;)V
    .locals 1

    iget-object v0, p0, Lorg/a/a/a/c;->j:Lorg/a/a/a/e;

    invoke-virtual {v0, p1}, Lorg/a/a/a/e;->a(Ljava/io/Reader;)V

    invoke-virtual {p0}, Lorg/a/a/a/c;->a()V

    return-void
.end method

.method public a(Ljava/io/Reader;Lorg/a/a/a/b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/a/a/a/c;->a(Ljava/io/Reader;Lorg/a/a/a/b;Z)V

    return-void
.end method

.method public a(Ljava/io/Reader;Lorg/a/a/a/b;Z)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, -0x1

    if-nez p3, :cond_3

    invoke-virtual {p0, p1}, Lorg/a/a/a/c;->a(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/a/a/a/c;->i:Ljava/util/LinkedList;

    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/a/a/a/c;->i:Ljava/util/LinkedList;

    :cond_1
    :try_start_0
    iget v0, p0, Lorg/a/a/a/c;->l:I

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    iget v0, p0, Lorg/a/a/a/c;->l:I

    if-ne v0, v4, :cond_9

    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/a/a/a/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_3

    :catch_0
    move-exception v0

    iput v4, p0, Lorg/a/a/a/c;->l:I

    throw v0

    :cond_3
    iget-object v0, p0, Lorg/a/a/a/c;->i:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/a/a/a/c;->a(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/a/a/a/c;->i:Ljava/util/LinkedList;

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-interface {p2}, Lorg/a/a/a/b;->a()V

    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/a/a/a/d; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :catch_1
    move-exception v0

    iput v4, p0, Lorg/a/a/a/c;->l:I

    throw v0

    :pswitch_3
    const/4 v0, 0x1

    :try_start_2
    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-interface {p2, v0}, Lorg/a/a/a/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_4
    :goto_2
    :pswitch_4
    return-void

    :pswitch_5
    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->c()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    :pswitch_6
    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->f()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    :pswitch_7
    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    if-ne v0, v4, :cond_5

    invoke-interface {p2}, Lorg/a/a/a/b;->b()V

    const/4 v0, 0x6

    iput v0, p0, Lorg/a/a/a/c;->l:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/a/a/a/d; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_2

    :catch_2
    move-exception v0

    iput v4, p0, Lorg/a/a/a/c;->l:I

    throw v0

    :cond_5
    const/4 v0, -0x1

    :try_start_3
    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/a/a/a/d; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v0

    iput v4, p0, Lorg/a/a/a/c;->l:I

    throw v0

    :pswitch_8
    :try_start_4
    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_2

    :pswitch_9
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_1

    :pswitch_a
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x4

    iput v2, p0, Lorg/a/a/a/c;->l:I

    new-instance v2, Ljava/lang/Integer;

    iget v3, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2, v0}, Lorg/a/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :cond_6
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_1

    :pswitch_b
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v5, :cond_7

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v1}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    :goto_3
    invoke-interface {p2}, Lorg/a/a/a/b;->d()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :cond_7
    const/4 v0, 0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto :goto_3

    :pswitch_c
    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_3

    :pswitch_d
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_1

    :pswitch_e
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v1}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-interface {p2, v0}, Lorg/a/a/a/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Lorg/a/a/a/b;->e()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_f
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    new-instance v0, Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->f()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_10
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    new-instance v0, Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->c()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_11
    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v1}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    invoke-interface {p2}, Lorg/a/a/a/b;->e()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_12
    invoke-direct {p0}, Lorg/a/a/a/c;->c()V

    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I

    packed-switch v0, :pswitch_data_4

    :pswitch_13
    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto/16 :goto_1

    :pswitch_14
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget-object v0, v0, Lorg/a/a/a/f;->j:Ljava/lang/Object;

    invoke-interface {p2, v0}, Lorg/a/a/a/b;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_15
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v5, :cond_8

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    invoke-direct {p0, v1}, Lorg/a/a/a/c;->a(Ljava/util/LinkedList;)I

    move-result v0

    iput v0, p0, Lorg/a/a/a/c;->l:I

    :goto_4
    invoke-interface {p2}, Lorg/a/a/a/b;->g()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :cond_8
    const/4 v0, 0x1

    iput v0, p0, Lorg/a/a/a/c;->l:I

    goto :goto_4

    :pswitch_16
    const/4 v0, 0x2

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->c()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_17
    const/4 v0, 0x3

    iput v0, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Ljava/lang/Integer;

    iget v2, p0, Lorg/a/a/a/c;->l:I

    invoke-direct {v0, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-interface {p2}, Lorg/a/a/a/b;->f()Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_2

    :pswitch_18
    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v2, v3}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0

    :cond_9
    iget-object v0, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    iget v0, v0, Lorg/a/a/a/f;->i:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/a/a/a/d; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_3

    if-ne v0, v4, :cond_1

    iput v4, p0, Lorg/a/a/a/c;->l:I

    new-instance v0, Lorg/a/a/a/d;

    invoke-virtual {p0}, Lorg/a/a/a/c;->b()I

    move-result v1

    iget-object v2, p0, Lorg/a/a/a/c;->k:Lorg/a/a/a/f;

    invoke-direct {v0, v1, v5, v2}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_18
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_12
        :pswitch_c
        :pswitch_11
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_b
        :pswitch_9
        :pswitch_9
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_e
        :pswitch_10
        :pswitch_d
        :pswitch_f
        :pswitch_d
        :pswitch_d
        :pswitch_0
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_14
        :pswitch_16
        :pswitch_13
        :pswitch_17
        :pswitch_15
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lorg/a/a/a/b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/a/a/a/c;->a(Ljava/lang/String;Lorg/a/a/a/b;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Lorg/a/a/a/b;Z)V
    .locals 4

    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0, v0, p2, p3}, Lorg/a/a/a/c;->a(Ljava/io/Reader;Lorg/a/a/a/b;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/a/a/a/d;

    const/4 v2, -0x1

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v0}, Lorg/a/a/a/d;-><init>(IILjava/lang/Object;)V

    throw v1
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lorg/a/a/a/c;->j:Lorg/a/a/a/e;

    invoke-virtual {v0}, Lorg/a/a/a/e;->a()I

    move-result v0

    return v0
.end method

.method public b(Ljava/io/Reader;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    check-cast v0, Lorg/a/a/a/a;

    invoke-virtual {p0, p1, v0}, Lorg/a/a/a/c;->a(Ljava/io/Reader;Lorg/a/a/a/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
