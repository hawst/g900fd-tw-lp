.class public Lcom/android/noisefield/ScriptC_noisefield;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_noisefield.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __BOOLEAN:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private __rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;

.field private mExportVar_densityDPI:F

.field private mExportVar_dotMesh:Landroid/renderscript/Mesh;

.field private mExportVar_dotParticles:Lcom/android/noisefield/ScriptField_Particle;

.field private mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragDots:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;

.field private mExportVar_textureDot:Landroid/renderscript/Allocation;

.field private mExportVar_touchDown:Z

.field private mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertDots:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertexColors:Lcom/android/noisefield/ScriptField_VertexColor_s;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1, "rs"    # Landroid/renderscript/RenderScript;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "id"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    .line 43
    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__ALLOCATION:Landroid/renderscript/Element;

    .line 44
    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    .line 45
    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    .line 46
    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__PROGRAM_STORE:Landroid/renderscript/Element;

    .line 47
    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__MESH:Landroid/renderscript/Element;

    .line 48
    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__F32:Landroid/renderscript/Element;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_touchDown:Z

    .line 50
    invoke-static {p1}, Landroid/renderscript/Element;->BOOLEAN(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__BOOLEAN:Landroid/renderscript/Element;

    .line 51
    return-void
.end method


# virtual methods
.method public bind_dotParticles(Lcom/android/noisefield/ScriptField_Particle;)V
    .locals 2
    .param p1, "v"    # Lcom/android/noisefield/ScriptField_Particle;

    .prologue
    const/16 v1, 0x9

    .line 202
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_dotParticles:Lcom/android/noisefield/ScriptField_Particle;

    .line 203
    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/noisefield/ScriptC_noisefield;->bindAllocation(Landroid/renderscript/Allocation;I)V

    .line 205
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-virtual {p1}, Lcom/android/noisefield/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/noisefield/ScriptC_noisefield;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_vertexColors(Lcom/android/noisefield/ScriptField_VertexColor_s;)V
    .locals 2
    .param p1, "v"    # Lcom/android/noisefield/ScriptField_VertexColor_s;

    .prologue
    const/16 v1, 0xa

    .line 214
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_vertexColors:Lcom/android/noisefield/ScriptField_VertexColor_s;

    .line 215
    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/noisefield/ScriptC_noisefield;->bindAllocation(Landroid/renderscript/Allocation;I)V

    .line 217
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-virtual {p1}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/noisefield/ScriptC_noisefield;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public invoke_positionParticles()V
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/noisefield/ScriptC_noisefield;->invoke(I)V

    .line 292
    return-void
.end method

.method public invoke_touch(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 296
    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    .line 297
    .local v0, "touch_fp":Landroid/renderscript/FieldPacker;
    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addF32(F)V

    .line 298
    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(F)V

    .line 299
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/noisefield/ScriptC_noisefield;->invoke(ILandroid/renderscript/FieldPacker;)V

    .line 300
    return-void
.end method

.method public declared-synchronized set_densityDPI(F)V
    .locals 1
    .param p1, "v"    # F

    .prologue
    .line 256
    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(IF)V

    .line 257
    iput p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_densityDPI:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    monitor-exit p0

    return-void

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_dotMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/Mesh;

    .prologue
    .line 226
    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 227
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_dotMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    monitor-exit p0

    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragBg(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/ProgramFragment;

    .prologue
    .line 115
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 116
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_fragBg:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragDots(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/ProgramFragment;

    .prologue
    .line 145
    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 146
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_fragDots:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gBackgroundMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/Mesh;

    .prologue
    .line 241
    monitor-enter p0

    const/16 v0, 0xc

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 242
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureDot(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/Allocation;

    .prologue
    .line 70
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 71
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_textureDot:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_touchDown(Z)V
    .locals 2
    .param p1, "v"    # Z

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;

    invoke-virtual {v0}, Landroid/renderscript/FieldPacker;->reset()V

    .line 276
    :goto_0
    iget-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;

    invoke-virtual {v0, p1}, Landroid/renderscript/FieldPacker;->addBoolean(Z)V

    .line 277
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/android/noisefield/ScriptC_noisefield;->__rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;

    invoke-virtual {p0, v0, v1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/FieldPacker;)V

    .line 278
    iput-boolean p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_touchDown:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    monitor-exit p0

    return-void

    .line 274
    :cond_0
    :try_start_1
    new-instance v0, Landroid/renderscript/FieldPacker;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/noisefield/ScriptC_noisefield;->__rs_fp_BOOLEAN:Landroid/renderscript/FieldPacker;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertBg(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/ProgramVertex;

    .prologue
    .line 100
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 101
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_vertBg:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit p0

    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertDots(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1, "v"    # Landroid/renderscript/ProgramVertex;

    .prologue
    .line 130
    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/noisefield/ScriptC_noisefield;->setVar(ILandroid/renderscript/BaseObj;)V

    .line 131
    iput-object p1, p0, Lcom/android/noisefield/ScriptC_noisefield;->mExportVar_vertDots:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
