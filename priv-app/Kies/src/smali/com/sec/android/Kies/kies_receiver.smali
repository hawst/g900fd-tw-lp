.class public Lcom/sec/android/Kies/kies_receiver;
.super Landroid/content/BroadcastReceiver;
.source "kies_receiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 213
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/emulated/0/_SamsungBnR_/Abackup/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 214
    new-instance v1, Ljava/io/File;

    const-string v2, "/storage/emulated/0/restore/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 215
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/log/kies/"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    new-instance v3, Ljava/io/File;

    const-string v4, "/storage/emulated/0/_SamsungBnR_/Abackup/temp/"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 219
    invoke-direct {p0, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Ljava/io/File;)V

    .line 222
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 224
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 226
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    invoke-direct {p0, v2}, Lcom/sec/android/Kies/kies_receiver;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :cond_1
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 232
    const-string v0, "KIES_RECEIVE"

    const-string v1, "BnR directory delete err"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 238
    const-string v1, "com.sec.android.Kies"

    const-string v2, "com.sec.android.Kies.kies_start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 240
    return-void
.end method

.method private a(Landroid/content/Context;[B[B)V
    .locals 3

    .prologue
    .line 242
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 243
    const-string v1, "head"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 244
    const-string v1, "body"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 245
    const-string v1, "com.sec.android.Kies"

    const-string v2, "com.sec.android.Kies.kies_start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 247
    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 196
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 198
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 199
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    invoke-virtual {v3}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Ljava/io/File;)V

    .line 198
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 206
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 207
    const-string v0, "KIES_RECEIVE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BnR directory delete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0x8

    const/4 v2, 0x6

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 68
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_GET_EAS_STATUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    sput v5, Lcom/sec/android/Kies/kies_start;->d:I

    .line 71
    const-string v0, "KIES_START"

    const-string v1, "KIES_ACTION_EVENT_NUM_GET_EAS_STATUS"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_MAKE_BACKUP_LIST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_MAKE_BACKUP_LIST Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 82
    new-array v0, v6, [B

    .line 84
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 85
    const/4 v1, 0x3

    aget-byte v1, v0, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 87
    const-string v1, "KIES_RECEIVE"

    const-string v2, "[APK BnR] Request apk detail "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v1, "body"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 90
    array-length v2, v1

    new-array v2, v2, [B

    .line 92
    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto :goto_0

    .line 96
    :cond_2
    const-string v1, "KIES_RECEIVE"

    const-string v2, "[APK BnR] Request all apk list"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v1, "body"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    .line 100
    invoke-direct {p0, p1, v0, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto :goto_0

    .line 103
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_REQUEST_BACKUP_SPACE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 105
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_REQUEST_BACKUP_SPACE Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    sput v6, Lcom/sec/android/Kies/kies_start;->d:I

    .line 109
    new-array v0, v5, [B

    .line 110
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 111
    const-string v1, "body"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 112
    array-length v2, v1

    new-array v2, v2, [B

    .line 113
    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 114
    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto/16 :goto_0

    .line 116
    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_MAKE_BACKUP_APK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 118
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_MAKE_BACKUP_APK Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/16 v0, 0xd

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 122
    new-array v0, v2, [B

    .line 123
    const/16 v0, 0x36

    new-array v0, v0, [B

    .line 125
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 126
    const-string v1, "body"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 127
    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto/16 :goto_0

    .line 131
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_SET_RESTORE_STATUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 133
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_SET_RESTORE_STATUS Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/16 v0, 0xe

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 136
    invoke-direct {p0, p1, v3, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto/16 :goto_0

    .line 138
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_START_RESTORE_APK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 140
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_START_RESTORE_APK Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/16 v0, 0xf

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 144
    new-array v0, v2, [B

    .line 147
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 148
    const-string v1, "body"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 150
    array-length v2, v1

    new-array v2, v2, [B

    .line 151
    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    invoke-direct {p0, p1, v0, v2}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto/16 :goto_0

    .line 155
    :cond_7
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_REQUEST_RESTORE_FINISH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 157
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_REQUEST_RESTORE_FINISH Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/16 v0, 0x10

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 160
    invoke-direct {p0, p1, v3, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    goto/16 :goto_0

    .line 162
    :cond_8
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 164
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_USB_DISCONNECT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 167
    const-string v1, "com.sec.android.Kies"

    const-string v2, "com.sec.android.Kies.kies_start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 169
    invoke-direct {p0}, Lcom/sec/android/Kies/kies_receiver;->a()V

    goto/16 :goto_0

    .line 172
    :cond_9
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.KIES_BNR_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const/16 v0, 0x11

    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    .line 175
    const-string v0, "KIES_RECEIVE"

    const-string v1, "[APK BnR] Receive KIES_BNR_CANCEL Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-direct {p0, p1, v3, v3}, Lcom/sec/android/Kies/kies_receiver;->a(Landroid/content/Context;[B[B)V

    .line 178
    invoke-direct {p0}, Lcom/sec/android/Kies/kies_receiver;->a()V

    goto/16 :goto_0
.end method
