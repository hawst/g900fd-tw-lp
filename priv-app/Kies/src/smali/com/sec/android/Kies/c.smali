.class Lcom/sec/android/Kies/c;
.super Ljava/lang/Object;
.source "kies_start.java"


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:J

.field private C:J

.field private D:Ljava/util/ArrayList;

.field private E:Ljava/util/List;

.field private F:Ljava/io/File;

.field private G:Ljava/util/ArrayList;

.field private H:Ljava/util/ArrayList;

.field private I:J

.field private J:J

.field private K:[B

.field private L:[B

.field public a:I

.field b:Landroid/os/Handler;

.field final synthetic c:Lcom/sec/android/Kies/kies_start;

.field private d:Landroid/content/Intent;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/io/File;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:[B

.field private u:[B

.field private v:[B

.field private w:[B

.field private x:[B

.field private y:[B

.field private z:Z


# direct methods
.method constructor <init>(Lcom/sec/android/Kies/kies_start;Landroid/content/Intent;ILandroid/os/Handler;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 1113
    iput-object p1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 911
    iput-object v6, p0, Lcom/sec/android/Kies/c;->d:Landroid/content/Intent;

    .line 913
    iput-object v6, p0, Lcom/sec/android/Kies/c;->e:Landroid/content/Context;

    .line 915
    const-string v0, "/data/app/"

    iput-object v0, p0, Lcom/sec/android/Kies/c;->f:Ljava/lang/String;

    .line 917
    const-string v0, "/system/priv-app/"

    iput-object v0, p0, Lcom/sec/android/Kies/c;->g:Ljava/lang/String;

    .line 919
    const-string v0, "/storage/emulated/0/_SamsungBnR_/Abackup/"

    iput-object v0, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    .line 921
    const-string v0, "/storage/emulated/0/restore/"

    iput-object v0, p0, Lcom/sec/android/Kies/c;->i:Ljava/lang/String;

    .line 923
    const-string v0, "/data/log/kies/"

    iput-object v0, p0, Lcom/sec/android/Kies/c;->j:Ljava/lang/String;

    .line 925
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->k:Ljava/io/File;

    .line 927
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/Kies/c;->l:I

    .line 929
    iput v3, p0, Lcom/sec/android/Kies/c;->m:I

    .line 931
    iput v2, p0, Lcom/sec/android/Kies/c;->n:I

    .line 933
    iput v3, p0, Lcom/sec/android/Kies/c;->o:I

    .line 935
    iput v2, p0, Lcom/sec/android/Kies/c;->p:I

    .line 937
    iput v1, p0, Lcom/sec/android/Kies/c;->q:I

    .line 939
    iput v3, p0, Lcom/sec/android/Kies/c;->r:I

    .line 941
    iput v2, p0, Lcom/sec/android/Kies/c;->s:I

    .line 944
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    .line 948
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    .line 952
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/Kies/c;->v:[B

    .line 956
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/Kies/c;->w:[B

    .line 960
    new-array v0, v1, [B

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/sec/android/Kies/c;->x:[B

    .line 964
    new-array v0, v1, [B

    fill-array-data v0, :array_5

    iput-object v0, p0, Lcom/sec/android/Kies/c;->y:[B

    .line 968
    iput-boolean v2, p0, Lcom/sec/android/Kies/c;->z:Z

    .line 970
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/Kies/c;->A:Ljava/util/ArrayList;

    .line 972
    iput-wide v4, p0, Lcom/sec/android/Kies/c;->B:J

    .line 974
    iput-wide v4, p0, Lcom/sec/android/Kies/c;->C:J

    .line 976
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/Kies/c;->D:Ljava/util/ArrayList;

    .line 978
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/Kies/c;->E:Ljava/util/List;

    .line 980
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->F:Ljava/io/File;

    .line 982
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/Kies/c;->G:Ljava/util/ArrayList;

    .line 988
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/Kies/c;->H:Ljava/util/ArrayList;

    .line 992
    iput-wide v4, p0, Lcom/sec/android/Kies/c;->I:J

    .line 996
    iput-wide v4, p0, Lcom/sec/android/Kies/c;->J:J

    .line 1002
    iput v3, p0, Lcom/sec/android/Kies/c;->a:I

    .line 1114
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] Init Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    iput-object p4, p0, Lcom/sec/android/Kies/c;->b:Landroid/os/Handler;

    .line 1117
    iput p3, p0, Lcom/sec/android/Kies/c;->a:I

    .line 1119
    const/16 v0, 0xb

    if-ne p3, v0, :cond_2

    .line 1120
    const/16 v0, 0xc

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1121
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1123
    iget-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    if-ne v0, v2, :cond_1

    .line 1124
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] apk detail request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 1127
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    .line 1128
    iget-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1154
    :cond_0
    :goto_0
    return-void

    .line 1130
    :cond_1
    iput-object v6, p0, Lcom/sec/android/Kies/c;->L:[B

    goto :goto_0

    .line 1132
    :cond_2
    const/16 v0, 0xc

    if-ne p3, v0, :cond_3

    .line 1133
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1134
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1136
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 1137
    array-length v1, v0

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    .line 1138
    iget-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1139
    :cond_3
    const/16 v0, 0xd

    if-ne p3, v0, :cond_4

    .line 1140
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1141
    const/16 v0, 0x34

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/Kies/c;->L:[B

    .line 1142
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1143
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->L:[B

    goto :goto_0

    .line 1144
    :cond_4
    iget v0, p0, Lcom/sec/android/Kies/c;->a:I

    const/16 v1, 0xe

    if-eq v0, v1, :cond_0

    .line 1146
    const/16 v0, 0xf

    if-ne p3, v0, :cond_0

    .line 1147
    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1148
    const-string v0, "head"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    .line 1150
    const-string v0, "body"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 1151
    array-length v1, v0

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    .line 1152
    iget-object v1, p0, Lcom/sec/android/Kies/c;->L:[B

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 944
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data

    .line 948
    nop

    :array_1
    .array-data 1
        0x0t
        0x1t
    .end array-data

    .line 952
    nop

    :array_2
    .array-data 1
        0x0t
        0x2t
    .end array-data

    .line 956
    nop

    :array_3
    .array-data 1
        0x0t
        0x6t
    .end array-data

    .line 960
    nop

    :array_4
    .array-data 1
        0x0t
        0x7t
    .end array-data

    .line 964
    nop

    :array_5
    .array-data 1
        0x0t
        0x5t
    .end array-data
.end method

.method static synthetic a(Lcom/sec/android/Kies/c;)I
    .locals 1

    .prologue
    .line 910
    iget v0, p0, Lcom/sec/android/Kies/c;->o:I

    return v0
.end method

.method private a([B)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x4

    .line 1024
    .line 1025
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    .line 1026
    new-array v2, v4, [B

    move v0, v1

    .line 1027
    :goto_0
    if-ge v0, v4, :cond_1

    .line 1028
    array-length v3, p1

    add-int/2addr v3, v0

    if-ge v3, v4, :cond_0

    .line 1029
    aput-byte v1, v2, v0

    .line 1027
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1031
    :cond_0
    array-length v3, p1

    add-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x4

    aget-byte v3, p1, v3

    aput-byte v3, v2, v0

    goto :goto_1

    .line 1034
    :cond_1
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1035
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1036
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/android/Kies/c;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 910
    invoke-direct {p0, p1, p2}, Lcom/sec/android/Kies/c;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1064
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v2

    .line 1109
    :goto_0
    return-object v0

    .line 1067
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    .line 1070
    goto :goto_0

    .line 1074
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 1077
    const-string v1, "."

    invoke-virtual {p2, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1079
    if-gez v1, :cond_3

    move-object v0, v2

    .line 1080
    goto :goto_0

    .line 1082
    :cond_3
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1086
    :goto_1
    array-length v1, v3

    if-ge v0, v1, :cond_6

    .line 1088
    aget-object v1, v3, v0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1090
    aget-object v1, v3, v0

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 1091
    aget-object v1, v3, v0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1092
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v0, v1

    .line 1094
    goto :goto_0

    .line 1096
    :cond_4
    invoke-virtual {v6, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1098
    new-instance v1, Ljava/io/File;

    const-string v6, "base.apk"

    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1099
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    move-object v0, v1

    .line 1100
    goto :goto_0

    .line 1086
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move-object v0, v2

    .line 1109
    goto :goto_0
.end method

.method private a(I)[B
    .locals 2

    .prologue
    .line 1041
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1042
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1043
    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1044
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/Kies/c;)Z
    .locals 1

    .prologue
    .line 910
    iget-boolean v0, p0, Lcom/sec/android/Kies/c;->z:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/android/Kies/c;)I
    .locals 1

    .prologue
    .line 910
    iget v0, p0, Lcom/sec/android/Kies/c;->p:I

    return v0
.end method

.method static synthetic d(Lcom/sec/android/Kies/c;)I
    .locals 1

    .prologue
    .line 910
    iget v0, p0, Lcom/sec/android/Kies/c;->q:I

    return v0
.end method

.method static synthetic e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->A:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->D:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/Kies/c;)Ljava/util/List;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->E:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/Kies/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/Kies/c;)I
    .locals 1

    .prologue
    .line 910
    iget v0, p0, Lcom/sec/android/Kies/c;->l:I

    return v0
.end method

.method static synthetic j(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->G:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->H:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/android/Kies/c;)[B
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    return-object v0
.end method

.method static synthetic m(Lcom/sec/android/Kies/c;)[B
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->x:[B

    return-object v0
.end method

.method static synthetic n(Lcom/sec/android/Kies/c;)[B
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->w:[B

    return-object v0
.end method

.method static synthetic o(Lcom/sec/android/Kies/c;)[B
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/File;)J
    .locals 4

    .prologue
    .line 2882
    const-wide/16 v0, -0x1

    .line 2883
    if-eqz p1, :cond_0

    .line 2884
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2885
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v2, v1

    .line 2886
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    .line 2887
    mul-long/2addr v0, v2

    .line 2889
    :cond_0
    return-wide v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sec/android/Kies/c;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1158
    iget-object v1, p0, Lcom/sec/android/Kies/c;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1159
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2416
    if-nez p1, :cond_1

    move v1, v2

    .line 2428
    :cond_0
    :goto_0
    return v1

    .line 2419
    :cond_1
    const-string v0, "sec_container"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    .line 2420
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2422
    :goto_1
    sget-object v3, Lcom/sec/android/Kies/a;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2423
    sget-object v3, Lcom/sec/android/Kies/a;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2424
    const-string v0, "KIES_START"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[APK BnR] except apk :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 2425
    goto :goto_0

    .line 2422
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1162
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] Thread run"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1163
    iget v0, p0, Lcom/sec/android/Kies/c;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1186
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->a()V

    .line 1187
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] Thread end"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    return-void

    .line 1165
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->c()I

    goto :goto_0

    .line 1168
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->d()I

    goto :goto_0

    .line 1171
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->e()I

    goto :goto_0

    .line 1174
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->f()I

    goto :goto_0

    .line 1177
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->g()I

    goto :goto_0

    .line 1180
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/android/Kies/c;->h()I

    goto :goto_0

    .line 1163
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public c()I
    .locals 15

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x4

    const/4 v14, 0x1

    const/4 v13, 0x2

    const/4 v12, 0x0

    .line 1191
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] process_action_event_make_backup_list"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1192
    new-array v7, v13, [B

    .line 1193
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v0, v12, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1196
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/android/Kies/c;->f:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1197
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1198
    new-instance v0, Lcom/sec/android/Kies/d;

    invoke-direct {v0, p0, v6}, Lcom/sec/android/Kies/d;-><init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V

    .line 1204
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 1206
    iget-object v3, p0, Lcom/sec/android/Kies/c;->K:[B

    if-eqz v3, :cond_3

    .line 1207
    new-array v3, v9, [B

    .line 1208
    iget-object v4, p0, Lcom/sec/android/Kies/c;->K:[B

    invoke-static {v4, v12, v3, v12, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1209
    invoke-direct {p0, v3}, Lcom/sec/android/Kies/c;->a([B)I

    move-result v4

    .line 1210
    const-string v3, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[APK BnR] make_backup_list: nCmdType = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    new-array v3, v9, [B

    .line 1213
    iget-object v5, p0, Lcom/sec/android/Kies/c;->K:[B

    invoke-static {v5, v9, v3, v12, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1214
    invoke-direct {p0, v3}, Lcom/sec/android/Kies/c;->a([B)I

    move-result v3

    .line 1215
    const-string v5, "KIES_START"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[APK BnR] make_backup_list: nNth = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget v5, p0, Lcom/sec/android/Kies/c;->n:I

    if-ne v5, v4, :cond_8

    .line 1234
    new-instance v5, Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/Kies/c;->L:[B

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>([B)V

    .line 1235
    const-string v8, "KIES_START"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[APK BnR] apk_detail ID : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1238
    :goto_0
    const-wide/16 v8, 0x0

    iput-wide v8, v0, Lcom/sec/android/Kies/d;->b:J

    .line 1241
    if-nez v4, :cond_4

    .line 1242
    invoke-static {v0, v1}, Lcom/sec/android/Kies/d;->a(Lcom/sec/android/Kies/d;Ljava/io/File;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/Kies/c;->C:J

    .line 1247
    :cond_0
    :goto_1
    iget-wide v8, p0, Lcom/sec/android/Kies/c;->C:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_7

    .line 1252
    :try_start_0
    new-instance v4, Lcom/sec/android/Kies/i;

    const/4 v1, 0x0

    invoke-direct {v4, p0, v1}, Lcom/sec/android/Kies/i;-><init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V

    .line 1253
    iget-object v1, p0, Lcom/sec/android/Kies/c;->E:Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/Kies/d;->a(Ljava/util/List;Ljava/io/File;ILcom/sec/android/Kies/i;Ljava/lang/String;)I

    move-result v1

    .line 1255
    iget-object v0, v4, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    .line 1256
    const-string v2, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[APK BnR] send body "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    if-eqz v3, :cond_1

    if-eq v1, v14, :cond_1

    .line 1259
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1260
    const/4 v3, 0x0

    const-string v4, "<Applicaion AppId"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1261
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1264
    :cond_1
    if-ne v1, v13, :cond_2

    .line 1265
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1266
    const-string v3, "</Applications>"

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1268
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1271
    :cond_2
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[APK BnR] send new body "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1273
    if-nez v1, :cond_5

    .line 1274
    const-string v1, "KIES_START"

    const-string v2, "[APK BnR] XML file has been created"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1275
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 1276
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1298
    :goto_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_READY_BACKUP_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1301
    const-string v1, "head"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1302
    const-string v1, "body"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1304
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1305
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] send intent : com.sec.android.intent.action.KIES_READY_BACKUP_LIST"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1310
    :goto_3
    return v14

    .line 1217
    :cond_3
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] make_backup_list: m_head null pointer exception"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    invoke-static {v0, v12, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1221
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_READY_BACKUP_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1223
    const-string v1, "head"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1224
    const-string v1, "body"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1225
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1226
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] send intent : com.sec.android.intent.action.KIES_READY_BACKUP_LIST"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1243
    :cond_4
    if-ne v4, v14, :cond_0

    .line 1244
    invoke-static {v0, v1, v5}, Lcom/sec/android/Kies/d;->a(Lcom/sec/android/Kies/d;Ljava/io/File;Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/Kies/c;->C:J

    goto/16 :goto_1

    .line 1277
    :cond_5
    if-ne v1, v14, :cond_6

    .line 1278
    :try_start_1
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] XML file make error"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1279
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1285
    :catch_0
    move-exception v0

    .line 1287
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1281
    :cond_6
    :try_start_2
    const-string v1, "KIES_START"

    const-string v2, "[APK BnR] More Data"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    .line 1283
    iget-object v0, p0, Lcom/sec/android/Kies/c;->v:[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1294
    :cond_7
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v0, v12, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1295
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] No Application"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_8
    move-object v5, v6

    goto/16 :goto_0
.end method

.method public d()I
    .locals 14

    .prologue
    .line 1314
    const/4 v0, 0x2

    new-array v7, v0, [B

    .line 1315
    const/4 v0, 0x0

    .line 1317
    iget-object v1, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v2, v7, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1321
    iget-object v1, p0, Lcom/sec/android/Kies/c;->K:[B

    if-eqz v1, :cond_0

    .line 1322
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 1323
    iget-object v1, p0, Lcom/sec/android/Kies/c;->K:[B

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1324
    invoke-direct {p0, v0}, Lcom/sec/android/Kies/c;->a([B)I

    move-result v8

    .line 1325
    const-string v0, "KIES_START"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[APK BnR] check_backup_space: nCmdType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    const/4 v3, 0x0

    .line 1343
    iget-object v0, p0, Lcom/sec/android/Kies/c;->L:[B

    array-length v0, v0

    mul-int/lit8 v1, v8, 0x6

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    new-array v9, v0, [B

    .line 1344
    invoke-direct {p0, v8}, Lcom/sec/android/Kies/c;->a(I)[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x4

    invoke-static {v0, v1, v9, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1346
    const/4 v2, 0x4

    .line 1349
    const-wide/16 v0, 0x0

    .line 1351
    const/4 v4, 0x0

    move v6, v4

    :goto_0
    if-ge v6, v8, :cond_4

    .line 1354
    const/4 v4, 0x2

    new-array v4, v4, [B

    .line 1355
    iget-object v5, p0, Lcom/sec/android/Kies/c;->L:[B

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-static {v5, v3, v4, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1356
    add-int/lit8 v3, v3, 0x2

    .line 1357
    invoke-direct {p0, v4}, Lcom/sec/android/Kies/c;->a([B)I

    move-result v10

    .line 1358
    const-string v5, "KIES_START"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[APK BnR] len = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    const/4 v5, 0x0

    const/4 v11, 0x2

    invoke-static {v4, v5, v9, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1362
    add-int/lit8 v2, v2, 0x2

    .line 1367
    new-array v4, v10, [B

    .line 1368
    iget-object v5, p0, Lcom/sec/android/Kies/c;->L:[B

    const/4 v11, 0x0

    invoke-static {v5, v3, v4, v11, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1369
    add-int v5, v3, v10

    .line 1370
    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v4}, Ljava/lang/String;-><init>([B)V

    .line 1371
    const-string v3, "KIES_START"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[APK BnR] App ID "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    const/4 v3, 0x0

    invoke-static {v4, v3, v9, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1375
    add-int v4, v2, v10

    .line 1377
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/Kies/c;->f:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2, v11}, Lcom/sec/android/Kies/c;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1379
    if-nez v2, :cond_1

    move v2, v4

    .line 1351
    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v5

    goto :goto_0

    .line 1327
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_READY_BACKUP_SPACE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1330
    iget-object v2, p0, Lcom/sec/android/Kies/c;->u:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v2, v3, v7, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1332
    const-string v2, "head"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1333
    const-string v2, "body"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1334
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1336
    const/4 v0, 0x1

    .line 1427
    :goto_2
    return v0

    .line 1382
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1384
    const-wide/16 v12, 0x0

    cmp-long v10, v0, v12

    if-nez v10, :cond_3

    move-wide v0, v2

    .line 1390
    :cond_2
    :goto_3
    const-string v10, "KIES_START"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "check : apk name is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", size is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1393
    iget-object v2, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v3, 0x0

    const/4 v10, 0x2

    invoke-static {v2, v3, v9, v4, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1394
    add-int/lit8 v2, v4, 0x2

    .line 1397
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/sec/android/Kies/c;->a(I)[B

    move-result-object v3

    const/4 v4, 0x0

    const/4 v10, 0x4

    invoke-static {v3, v4, v9, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1399
    add-int/lit8 v2, v2, 0x4

    goto :goto_1

    .line 1387
    :cond_3
    cmp-long v10, v2, v0

    if-gez v10, :cond_2

    move-wide v0, v2

    goto :goto_3

    .line 1403
    :cond_4
    new-array v3, v2, [B

    .line 1404
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v9, v4, v3, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1406
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 1409
    const-string v4, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[APK BnR] backup space STR : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1411
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.intent.action.KIES_READY_BACKUP_SPACE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1414
    iget-object v4, p0, Lcom/sec/android/Kies/c;->F:Ljava/io/File;

    invoke-virtual {p0, v4}, Lcom/sec/android/Kies/c;->a(Ljava/io/File;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/Kies/c;->J:J

    .line 1416
    const-string v4, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "check : minSize is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", memorysize is"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v8, p0, Lcom/sec/android/Kies/c;->J:J

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    iget-wide v4, p0, Lcom/sec/android/Kies/c;->J:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_5

    .line 1419
    iget-object v0, p0, Lcom/sec/android/Kies/c;->w:[B

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v0, v1, v7, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1423
    :goto_4
    const-string v0, "head"

    invoke-virtual {v2, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1424
    const-string v0, "body"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1425
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0, v2}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1427
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 1421
    :cond_5
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v0, v1, v7, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_4
.end method

.method public e()I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 1431
    iget v0, p0, Lcom/sec/android/Kies/c;->o:I

    .line 1432
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] kies make backup apk"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/Kies/c;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1440
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1441
    new-instance v2, Lcom/sec/android/Kies/d;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/Kies/d;-><init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V

    .line 1442
    new-instance v3, Ljava/io/File;

    const-string v4, "/storage/emulated/0/_SamsungBnR_/"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1443
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1444
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 1450
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1451
    const-string v4, "/storage/emulated/0/_SamsungBnR_/"

    .line 1452
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1453
    const-string v5, "Path"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1454
    iget-object v5, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v5, v3}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1455
    const-string v3, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[APK BnR] OBJECT_ADDED"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    new-array v3, v9, [B

    .line 1458
    iget-object v4, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v4, v8, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1461
    new-array v4, v9, [B

    .line 1462
    iget-object v5, p0, Lcom/sec/android/Kies/c;->L:[B

    invoke-static {v5, v8, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1463
    invoke-direct {p0, v4}, Lcom/sec/android/Kies/c;->a([B)I

    move-result v4

    .line 1465
    const-string v5, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[APK BnR] nLength:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1468
    new-array v5, v4, [B

    .line 1469
    iget-object v6, p0, Lcom/sec/android/Kies/c;->L:[B

    invoke-static {v6, v9, v5, v8, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1470
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    .line 1472
    const-string v5, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[APK BnR] strAppID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1476
    :try_start_0
    invoke-static {v2, v4, v0, v1}, Lcom/sec/android/Kies/d;->a(Lcom/sec/android/Kies/d;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1483
    :goto_1
    iget v1, p0, Lcom/sec/android/Kies/c;->o:I

    if-ne v0, v1, :cond_0

    .line 1484
    const-string v1, "KIES_START"

    const-string v2, "[APK BnR] KIES_READY_BACKUP_APK calls com.android.MTP.OBJECT_ADDED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1486
    const-string v2, "Path"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1487
    iget-object v2, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v2, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1494
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_READY_BACKUP_APK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1497
    const-string v2, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[APK BnR] KIES_READY_BACKUP_APK retErr = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    iget v2, p0, Lcom/sec/android/Kies/c;->p:I

    if-ne v0, v2, :cond_2

    .line 1500
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    invoke-static {v0, v8, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1505
    :goto_2
    const-string v0, "head"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1506
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1517
    const/4 v0, 0x1

    return v0

    .line 1447
    :cond_1
    const-string v3, "KIES_START"

    const-string v4, "[APK BnR] Root existed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1477
    :catch_0
    move-exception v0

    .line 1479
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1480
    iget v0, p0, Lcom/sec/android/Kies/c;->p:I

    goto :goto_1

    .line 1501
    :cond_2
    iget v2, p0, Lcom/sec/android/Kies/c;->q:I

    if-ne v0, v2, :cond_3

    .line 1502
    iget-object v0, p0, Lcom/sec/android/Kies/c;->w:[B

    invoke-static {v0, v8, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 1504
    :cond_3
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v0, v8, v3, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method public f()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 1522
    new-array v2, v6, [B

    .line 1527
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/Kies/c;->i:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1528
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1529
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 1531
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1532
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 1531
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.action.KIES_READY_RESTORE_STATUS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1540
    iget-object v3, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v3, v1, v2, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1541
    const-string v1, "head"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1543
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1545
    const/4 v0, 0x1

    return v0
.end method

.method public g()I
    .locals 15

    .prologue
    const/4 v12, 0x4

    const/4 v5, 0x0

    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v7, 0x0

    .line 1550
    new-array v2, v14, [B

    .line 1551
    new-array v4, v12, [B

    .line 1553
    new-array v6, v14, [B

    .line 1554
    const/16 v0, 0x62

    new-array v8, v0, [B

    .line 1556
    new-array v9, v14, [B

    .line 1557
    iget v1, p0, Lcom/sec/android/Kies/c;->o:I

    .line 1559
    const-string v0, "KIES_START"

    const-string v3, "[APK BnR] precess_action_event_start_resore_apk"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1563
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0}, Lcom/sec/android/Kies/kies_start;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "power"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 1565
    const-string v3, "kies_apk_bnr"

    invoke-virtual {v0, v13, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    .line 1566
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1568
    const-string v0, "KIES_START"

    const-string v10, "[APK BnR] WakeLock On"

    invoke-static {v0, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1570
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-static {v0}, Lcom/sec/android/Kies/kies_start;->a(Lcom/sec/android/Kies/kies_start;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1571
    const-string v0, "KIES_START"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[APK BnR] isVer: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-static {v11}, Lcom/sec/android/Kies/kies_start;->b(Lcom/sec/android/Kies/kies_start;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " isVapp: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-static {v11}, Lcom/sec/android/Kies/kies_start;->c(Lcom/sec/android/Kies/kies_start;)Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1575
    :cond_0
    iget-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    invoke-static {v0, v7, v2, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1576
    iget-object v0, p0, Lcom/sec/android/Kies/c;->K:[B

    invoke-static {v0, v14, v4, v7, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1578
    iget-object v0, p0, Lcom/sec/android/Kies/c;->L:[B

    invoke-static {v0, v7, v6, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1579
    iget-object v0, p0, Lcom/sec/android/Kies/c;->L:[B

    iget-object v2, p0, Lcom/sec/android/Kies/c;->L:[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v0, v14, v8, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1581
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>([B)V

    .line 1583
    const-string v2, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[APK BnR] apk name : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1585
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/Kies/c;->i:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1587
    new-instance v8, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/Kies/c;->f:Ljava/lang/String;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1592
    iget-boolean v2, p0, Lcom/sec/android/Kies/c;->z:Z

    if-eqz v2, :cond_11

    .line 1594
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/Kies/c;->j:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1595
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1596
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 1597
    array-length v10, v6

    move v4, v7

    :goto_0
    if-ge v4, v10, :cond_1

    aget-object v11, v6, v4

    .line 1598
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .line 1597
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1600
    :cond_1
    const-string v4, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[APK BnR] remove file : "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1614
    :cond_2
    :goto_1
    invoke-virtual {v2, v13, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 1615
    invoke-virtual {v2, v13, v7}, Ljava/io/File;->setReadable(ZZ)Z

    .line 1616
    invoke-virtual {v2, v13, v13}, Ljava/io/File;->setWritable(ZZ)Z

    .line 1618
    new-instance v4, Lcom/sec/android/Kies/g;

    invoke-direct {v4, p0}, Lcom/sec/android/Kies/g;-><init>(Lcom/sec/android/Kies/c;)V

    .line 1619
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v10

    .line 1621
    iget-object v6, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v6}, Lcom/sec/android/Kies/kies_start;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 1623
    if-eqz v6, :cond_e

    .line 1625
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v10, v7

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11, v13}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 1627
    if-nez v6, :cond_6

    .line 1628
    aget-object v1, v10, v7

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "/"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v11, v10, v7

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v11, "GAKIES"

    invoke-virtual {v4, v0, v1, v6, v11}, Lcom/sec/android/Kies/g;->b(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1669
    :cond_3
    :goto_2
    iget v1, p0, Lcom/sec/android/Kies/c;->o:I

    if-eq v0, v1, :cond_10

    .line 1670
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[APK BnR] errcode has error : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1671
    iget v1, p0, Lcom/sec/android/Kies/c;->p:I

    if-ne v0, v1, :cond_f

    .line 1672
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    invoke-static {v0, v7, v9, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1676
    :cond_4
    :goto_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1678
    const-string v1, "head"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1679
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1681
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1683
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] WakeLock Off "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1800
    :goto_4
    return v13

    .line 1602
    :cond_5
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1603
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 1604
    const-string v4, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[APK BnR] make name : "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1605
    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1606
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 1608
    const-string v10, "Path"

    invoke-virtual {v4, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1609
    iget-object v10, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v10, v4}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1610
    const-string v4, "KIES_START"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[APK BnR] OBJECT_ADDED"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1632
    :cond_6
    const-string v4, "KIES_START"

    const-string v6, "[APK BnR] This\'s apk, so copy to data"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v4, Ljava/io/File;

    const/4 v11, 0x0

    aget-object v11, v10, v11

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v4, v0, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1637
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    const/4 v11, 0x0

    aget-object v11, v10, v11

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v0, v2, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1639
    const/16 v0, 0x1000

    :try_start_2
    new-array v0, v0, [B

    .line 1641
    :goto_5
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_8

    .line 1642
    const/4 v12, 0x0

    invoke-virtual {v4, v0, v12, v11}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_5

    .line 1646
    :catch_0
    move-exception v0

    move-object v1, v4

    move-object v4, v6

    .line 1647
    :goto_6
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1648
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v6, "ENOSPC"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1649
    iget v0, p0, Lcom/sec/android/Kies/c;->q:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1654
    :goto_7
    if-eqz v4, :cond_7

    .line 1655
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 1656
    :cond_7
    if-eqz v1, :cond_3

    .line 1657
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 1658
    :catch_1
    move-exception v1

    goto/16 :goto_2

    .line 1644
    :cond_8
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 1645
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1654
    if-eqz v6, :cond_9

    .line 1655
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 1656
    :cond_9
    if-eqz v4, :cond_a

    .line 1657
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :cond_a
    move v0, v1

    .line 1660
    goto/16 :goto_2

    .line 1658
    :catch_2
    move-exception v0

    move v0, v1

    .line 1661
    goto/16 :goto_2

    .line 1651
    :cond_b
    :try_start_7
    iget v0, p0, Lcom/sec/android/Kies/c;->p:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_7

    .line 1653
    :catchall_0
    move-exception v0

    move-object v6, v5

    .line 1654
    :goto_8
    if-eqz v6, :cond_c

    .line 1655
    :try_start_8
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 1656
    :cond_c
    if-eqz v5, :cond_d

    .line 1657
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    .line 1660
    :cond_d
    :goto_9
    throw v0

    .line 1666
    :cond_e
    iget v0, p0, Lcom/sec/android/Kies/c;->p:I

    goto/16 :goto_2

    .line 1673
    :cond_f
    iget v1, p0, Lcom/sec/android/Kies/c;->q:I

    if-ne v0, v1, :cond_4

    .line 1674
    iget-object v0, p0, Lcom/sec/android/Kies/c;->w:[B

    invoke-static {v0, v7, v9, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_3

    .line 1687
    :cond_10
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v4, v10, v7

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1688
    invoke-virtual {v0, v13, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 1689
    invoke-virtual {v0, v13, v7}, Ljava/io/File;->setReadable(ZZ)Z

    .line 1690
    invoke-virtual {v0, v13, v7}, Ljava/io/File;->setWritable(ZZ)Z

    move-object v1, v2

    .line 1702
    :goto_a
    new-instance v4, Lcom/sec/android/Kies/f;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/Kies/f;-><init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V

    .line 1706
    :try_start_9
    invoke-virtual {v4, v1, v8}, Lcom/sec/android/Kies/f;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 1708
    iget-object v0, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x2

    invoke-static {v0, v2, v9, v6, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1710
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1712
    const-string v2, "head"

    invoke-virtual {v0, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1713
    iget-object v2, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v2, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1715
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_9
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    .line 1717
    :try_start_a
    const-string v0, "KIES_START"

    const-string v2, "[APK BnR] WakeLock Off "

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    goto/16 :goto_4

    .line 1721
    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v5

    .line 1723
    :goto_b
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v2, v0

    .line 1729
    :goto_c
    iget-object v0, p0, Lcom/sec/android/Kies/c;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-ne v0, v13, :cond_13

    .line 1731
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] restorefilelist is empty "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    iget-object v0, p0, Lcom/sec/android/Kies/c;->u:[B

    invoke-static {v0, v7, v9, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1734
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1736
    const-string v1, "head"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1737
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1739
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1741
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] WakeLock Off "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_11
    move-object v1, v0

    .line 1698
    goto :goto_a

    :cond_12
    move-object v2, v3

    .line 1727
    goto :goto_c

    .line 1724
    :catch_4
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    .line 1725
    :goto_d
    const-string v3, "KIES_START"

    const-string v5, "[APK BnR] packageNameCheck skip.."

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1726
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v0

    goto :goto_c

    .line 1749
    :cond_13
    const-string v0, "KIES_START"

    const-string v3, "[APK BnR] install apk"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/Kies/c;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/android/Kies/f;->a(Lcom/sec/android/Kies/f;Ljava/lang/String;)Lcom/sec/android/Kies/h;

    move-result-object v3

    move v0, v7

    .line 1757
    :cond_14
    const-wide/16 v10, 0x3e8

    :try_start_b
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V

    .line 1758
    const-string v5, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[APK BnR] sleep check"

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1761
    iget-boolean v5, v3, Lcom/sec/android/Kies/h;->a:Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    if-ne v5, v13, :cond_17

    .line 1786
    :cond_15
    :goto_e
    const/16 v1, 0xb4

    if-lt v0, v1, :cond_16

    iget-boolean v0, v3, Lcom/sec/android/Kies/h;->a:Z

    if-nez v0, :cond_16

    .line 1787
    iput-boolean v13, v3, Lcom/sec/android/Kies/h;->a:Z

    .line 1788
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1790
    iget-object v1, p0, Lcom/sec/android/Kies/c;->u:[B

    invoke-static {v1, v7, v9, v7, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1791
    const-string v1, "head"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1792
    iget-object v1, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v0}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1793
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] thread sent intent : FAIL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1796
    :cond_16
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1798
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] WakeLock Off "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1766
    :cond_17
    :try_start_c
    invoke-virtual {v4, v1, v8}, Lcom/sec/android/Kies/f;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 1768
    iget-boolean v5, v3, Lcom/sec/android/Kies/h;->a:Z

    if-nez v5, :cond_15

    .line 1769
    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/sec/android/Kies/h;->a:Z

    .line 1770
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1772
    iget-object v6, p0, Lcom/sec/android/Kies/c;->t:[B

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-static {v6, v10, v9, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1773
    const-string v6, "head"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1774
    iget-object v6, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v6, v5}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1775
    const-string v5, "KIES_START"

    const-string v6, "[APK BnR] thread sent intent : OK"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto :goto_e

    .line 1779
    :catch_5
    move-exception v5

    .line 1780
    const-string v5, "KIES_START"

    const-string v6, "[APK BnR] sleep err"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1782
    :cond_18
    add-int/lit8 v0, v0, 0x1

    .line 1784
    const/16 v5, 0xb4

    if-lt v0, v5, :cond_14

    goto :goto_e

    .line 1724
    :catch_6
    move-exception v0

    move-object v2, v0

    move-object v0, v5

    goto/16 :goto_d

    .line 1721
    :catch_7
    move-exception v0

    move-object v2, v0

    move-object v0, v3

    goto/16 :goto_b

    .line 1658
    :catch_8
    move-exception v1

    goto/16 :goto_9

    .line 1653
    :catchall_1
    move-exception v0

    goto/16 :goto_8

    :catchall_2
    move-exception v0

    move-object v5, v4

    goto/16 :goto_8

    :catchall_3
    move-exception v0

    move-object v5, v1

    move-object v6, v4

    goto/16 :goto_8

    .line 1646
    :catch_9
    move-exception v0

    move-object v1, v5

    move-object v4, v5

    goto/16 :goto_6

    :catch_a
    move-exception v0

    move-object v1, v5

    move-object v4, v6

    goto/16 :goto_6
.end method

.method public h()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1807
    new-array v0, v3, [B

    .line 1809
    iget-object v1, p0, Lcom/sec/android/Kies/c;->t:[B

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1811
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_REPONSE_RESTORE_FINISH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1814
    new-instance v2, Lcom/sec/android/Kies/d;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/Kies/d;-><init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V

    .line 1815
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/Kies/c;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1816
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1817
    invoke-static {v2, v3}, Lcom/sec/android/Kies/d;->b(Lcom/sec/android/Kies/d;Ljava/io/File;)V

    .line 1820
    :cond_0
    const-string v2, "head"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1821
    iget-object v0, p0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1823
    const/4 v0, 0x1

    return v0
.end method
