.class Lcom/sec/android/Kies/f;
.super Ljava/lang/Object;
.source "kies_start.java"


# instance fields
.field a:Landroid/content/pm/PackageManager;

.field final synthetic b:Lcom/sec/android/Kies/c;


# direct methods
.method private constructor <init>(Lcom/sec/android/Kies/c;)V
    .locals 1

    .prologue
    .line 2438
    iput-object p1, p0, Lcom/sec/android/Kies/f;->b:Lcom/sec/android/Kies/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2440
    iget-object v0, p0, Lcom/sec/android/Kies/f;->b:Lcom/sec/android/Kies/c;

    iget-object v0, v0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0}, Lcom/sec/android/Kies/kies_start;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V
    .locals 0

    .prologue
    .line 2438
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/f;-><init>(Lcom/sec/android/Kies/c;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/Kies/f;Ljava/lang/String;)Lcom/sec/android/Kies/h;
    .locals 1

    .prologue
    .line 2438
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/f;->a(Ljava/lang/String;)Lcom/sec/android/Kies/h;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/sec/android/Kies/h;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2534
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2535
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2537
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "install :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Uri:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2538
    new-instance v2, Lcom/sec/android/Kies/h;

    iget-object v3, p0, Lcom/sec/android/Kies/f;->b:Lcom/sec/android/Kies/c;

    invoke-direct {v2, v3}, Lcom/sec/android/Kies/h;-><init>(Lcom/sec/android/Kies/c;)V

    .line 2539
    iput-boolean v5, v2, Lcom/sec/android/Kies/h;->a:Z

    .line 2540
    iget-object v3, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v5, v4}, Landroid/content/pm/PackageManager;->verifyPendingInstall(II)V

    .line 2542
    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/f;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2543
    iget-object v0, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    const/4 v3, 0x2

    const-string v4, "app"

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 2548
    :goto_0
    return-object v2

    .line 2546
    :cond_0
    iget-object v0, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    const-string v3, "app"

    invoke-virtual {v0, v1, v2, v5, v3}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/File;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2555
    .line 2560
    const-string v2, "KIES_START"

    const-string v3, "[APK BnR] isReplaceApk :"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2562
    iget-object v2, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 2564
    if-nez v2, :cond_1

    .line 2609
    :cond_0
    :goto_0
    return v0

    .line 2567
    :cond_1
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2569
    if-eqz v2, :cond_3

    .line 2573
    :try_start_0
    const-string v3, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[APK BnR] isReplaceApk : sourcePackageName is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2575
    iget-object v3, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    const/16 v4, 0x200

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 2580
    if-eqz v3, :cond_2

    .line 2581
    const-string v4, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[APK BnR] isReplaceApk :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "is already installed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2582
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_0

    .line 2583
    const-string v3, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[APK BnR] isReplaceApk :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is system app"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2584
    goto :goto_0

    .line 2588
    :cond_2
    const-string v1, "KIES_START"

    const-string v3, "[APK BnR] isReplaceApk : there\'s no already existing app"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2590
    :catch_0
    move-exception v1

    .line 2592
    const-string v2, "KIES_START"

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2594
    :catch_1
    move-exception v1

    .line 2596
    const-string v1, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown package : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2598
    :catch_2
    move-exception v1

    .line 2600
    const-string v2, "KIES_START"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2606
    :cond_3
    const-string v1, "KIES_START"

    const-string v2, "[APK BnR] isReplaceApk : sourcePackageName is null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public a(Ljava/io/File;Ljava/io/File;)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2448
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 2449
    invoke-virtual {p2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    move v2, v1

    .line 2454
    :goto_0
    array-length v0, v3

    if-ge v2, v0, :cond_2

    .line 2458
    const-string v0, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "find apk = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 2460
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 2462
    aget-object v5, v4, v0

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 2463
    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 2467
    const-string v6, "apk"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2468
    iget-object v5, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    aget-object v6, v3, v2

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2470
    iget-object v6, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    aget-object v7, v4, v0

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2473
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2475
    const-string v0, "KIES_START"

    const-string v2, "Apk Name is same"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2507
    :goto_2
    return v1

    .line 2460
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2494
    :cond_1
    aget-object v0, v3, v2

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2495
    const-string v5, "KIES_START"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "add install apk :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2496
    iget-object v5, p0, Lcom/sec/android/Kies/f;->a:Landroid/content/pm/PackageManager;

    aget-object v6, v3, v2

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2498
    const-string v6, "KIES_START"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "add install packageName :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2499
    iget-object v6, p0, Lcom/sec/android/Kies/f;->b:Lcom/sec/android/Kies/c;

    invoke-static {v6}, Lcom/sec/android/Kies/c;->j(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2502
    iget-object v5, p0, Lcom/sec/android/Kies/f;->b:Lcom/sec/android/Kies/c;

    invoke-static {v5}, Lcom/sec/android/Kies/c;->k(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2454
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 2507
    :cond_2
    const/4 v1, 0x1

    goto :goto_2
.end method
