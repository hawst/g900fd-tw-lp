.class public Lcom/sec/android/Kies/g;
.super Ljava/lang/Object;
.source "kies_start.java"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:I

.field d:I

.field e:Ljava/lang/String;

.field f:[B

.field g:[B

.field h:Ljava/lang/String;

.field final synthetic i:Lcom/sec/android/Kies/c;


# direct methods
.method public constructor <init>(Lcom/sec/android/Kies/c;)V
    .locals 1

    .prologue
    .line 2672
    iput-object p1, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2674
    const-string v0, "PBEWithSHA256And256BitAES-CBC-BC"

    iput-object v0, p0, Lcom/sec/android/Kies/g;->a:Ljava/lang/String;

    .line 2676
    const-string v0, "AES/CBC/PKCS5Padding"

    iput-object v0, p0, Lcom/sec/android/Kies/g;->b:Ljava/lang/String;

    .line 2678
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/Kies/g;->c:I

    .line 2680
    const/16 v0, 0x100

    iput v0, p0, Lcom/sec/android/Kies/g;->d:I

    .line 2682
    const-string v0, "password"

    iput-object v0, p0, Lcom/sec/android/Kies/g;->e:Ljava/lang/String;

    .line 2684
    const-string v0, "ababababababababagababababa"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/g;->f:[B

    .line 2686
    const-string v0, "1234567890abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/g;->g:[B

    .line 2688
    const-string v0, "this will be encrypted"

    iput-object v0, p0, Lcom/sec/android/Kies/g;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2722
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    .line 2731
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual {p4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/Kies/g;->f:[B

    iget v4, p0, Lcom/sec/android/Kies/g;->c:I

    iget v5, p0, Lcom/sec/android/Kies/g;->d:I

    invoke-direct {v0, v1, v3, v4, v5}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 2733
    const-string v1, "PBEWithSHA256And256BitAES-CBC-BC"

    invoke-static {v1}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v1

    .line 2735
    invoke-virtual {v1, v0}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 2736
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    const-string v3, "AES"

    invoke-direct {v1, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 2738
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v3, p0, Lcom/sec/android/Kies/g;->g:[B

    invoke-direct {v0, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2740
    iget-object v3, p0, Lcom/sec/android/Kies/g;->b:Ljava/lang/String;

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    .line 2742
    const/4 v3, 0x1

    invoke-virtual {v5, v3, v1, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 2747
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2748
    :try_start_1
    const-string v0, "KIES_START"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "targetPath :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2750
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2751
    :try_start_2
    const-string v0, "KIES_START"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "outPath :"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2752
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, v3, v5}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2758
    const/16 v0, 0x1000

    :try_start_3
    new-array v0, v0, [B

    .line 2759
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_3

    .line 2760
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v2}, Ljavax/crypto/CipherOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 2770
    :catch_0
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    .line 2771
    :goto_1
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2772
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "ENOSPC"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2773
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->d(Lcom/sec/android/Kies/c;)I

    move-result v0

    .line 2777
    :goto_2
    const-string v4, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "retErr : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2781
    if-eqz v1, :cond_0

    .line 2782
    :try_start_5
    invoke-virtual {v1}, Ljavax/crypto/CipherOutputStream;->close()V

    .line 2783
    :cond_0
    if-eqz v3, :cond_1

    .line 2784
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 2785
    :cond_1
    if-eqz v2, :cond_2

    .line 2786
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 2791
    :cond_2
    :goto_3
    return v0

    .line 2764
    :cond_3
    :try_start_6
    invoke-virtual {v1}, Ljavax/crypto/CipherOutputStream;->close()V

    .line 2765
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 2766
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 2767
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    move-result v0

    .line 2768
    const-string v2, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "retErr : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 2781
    if-eqz v1, :cond_4

    .line 2782
    :try_start_7
    invoke-virtual {v1}, Ljavax/crypto/CipherOutputStream;->close()V

    .line 2783
    :cond_4
    if-eqz v4, :cond_5

    .line 2784
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 2785
    :cond_5
    if-eqz v3, :cond_2

    .line 2786
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 2787
    :catch_1
    move-exception v1

    goto :goto_3

    .line 2775
    :cond_6
    :try_start_8
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->c(Lcom/sec/android/Kies/c;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result v0

    goto :goto_2

    .line 2780
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    .line 2781
    :goto_4
    if-eqz v2, :cond_7

    .line 2782
    :try_start_9
    invoke-virtual {v2}, Ljavax/crypto/CipherOutputStream;->close()V

    .line 2783
    :cond_7
    if-eqz v4, :cond_8

    .line 2784
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 2785
    :cond_8
    if-eqz v3, :cond_9

    .line 2786
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2

    .line 2789
    :cond_9
    :goto_5
    throw v0

    .line 2787
    :catch_2
    move-exception v1

    goto :goto_5

    .line 2780
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_4
    move-exception v0

    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    goto :goto_4

    .line 2787
    :catch_3
    move-exception v1

    goto :goto_3

    .line 2770
    :catch_4
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1

    :catch_5
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto/16 :goto_1

    :catch_6
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_1
.end method

.method public b(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2796
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    .line 2801
    :try_start_0
    new-instance v0, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual {p4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/Kies/g;->f:[B

    iget v4, p0, Lcom/sec/android/Kies/g;->c:I

    iget v5, p0, Lcom/sec/android/Kies/g;->d:I

    invoke-direct {v0, v2, v3, v4, v5}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 2803
    const-string v2, "PBEWithSHA256And256BitAES-CBC-BC"

    invoke-static {v2}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v2

    .line 2805
    invoke-virtual {v2, v0}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 2806
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    const-string v3, "AES"

    invoke-direct {v2, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 2808
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    iget-object v3, p0, Lcom/sec/android/Kies/g;->g:[B

    invoke-direct {v0, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 2813
    iget-object v3, p0, Lcom/sec/android/Kies/g;->b:Ljava/lang/String;

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 2814
    const/4 v3, 0x2

    invoke-virtual {v4, v3, v2, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 2816
    const-string v0, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "target : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2818
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2820
    :try_start_1
    new-instance v0, Ljavax/crypto/CipherInputStream;

    invoke-direct {v0, v3, v4}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    .line 2821
    if-nez v0, :cond_0

    .line 2822
    const-string v2, "KIES_START"

    const-string v4, "decding err out == null"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2824
    :cond_0
    const-string v2, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "outPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2825
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2832
    const/16 v1, 0x1000

    :try_start_2
    new-array v1, v1, [B

    .line 2833
    :goto_0
    invoke-virtual {v0, v1}, Ljavax/crypto/CipherInputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_3

    .line 2834
    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 2843
    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    .line 2844
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2845
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ENOSPC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2846
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->d(Lcom/sec/android/Kies/c;)I

    move-result v0

    .line 2850
    :goto_2
    const-string v3, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retErr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2854
    if-eqz v1, :cond_1

    .line 2855
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2856
    :cond_1
    if-eqz v2, :cond_2

    .line 2857
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 2860
    :cond_2
    :goto_3
    return v0

    .line 2836
    :cond_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 2837
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 2838
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 2839
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    move-result v0

    .line 2840
    const-string v1, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "retErr : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2854
    if-eqz v2, :cond_4

    .line 2855
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 2856
    :cond_4
    if-eqz v3, :cond_2

    .line 2857
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    .line 2858
    :catch_1
    move-exception v1

    goto :goto_3

    .line 2848
    :cond_5
    :try_start_7
    iget-object v0, p0, Lcom/sec/android/Kies/g;->i:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->c(Lcom/sec/android/Kies/c;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result v0

    goto :goto_2

    .line 2853
    :catchall_0
    move-exception v0

    move-object v3, v1

    .line 2854
    :goto_4
    if-eqz v1, :cond_6

    .line 2855
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2856
    :cond_6
    if-eqz v3, :cond_7

    .line 2857
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 2860
    :cond_7
    :goto_5
    throw v0

    .line 2858
    :catch_2
    move-exception v1

    goto :goto_5

    .line 2853
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_4

    .line 2858
    :catch_3
    move-exception v1

    goto :goto_3

    .line 2843
    :catch_4
    move-exception v0

    move-object v2, v1

    goto/16 :goto_1

    :catch_5
    move-exception v0

    move-object v2, v3

    goto/16 :goto_1
.end method
