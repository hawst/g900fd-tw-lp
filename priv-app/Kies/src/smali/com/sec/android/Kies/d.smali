.class Lcom/sec/android/Kies/d;
.super Ljava/lang/Object;
.source "kies_start.java"


# instance fields
.field a:Landroid/content/pm/PackageManager;

.field public b:J

.field final synthetic c:Lcom/sec/android/Kies/c;


# direct methods
.method private constructor <init>(Lcom/sec/android/Kies/c;)V
    .locals 2

    .prologue
    .line 1827
    iput-object p1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    iget-object v0, v0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0}, Lcom/sec/android/Kies/kies_start;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/Kies/d;->a:Landroid/content/pm/PackageManager;

    .line 1831
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/Kies/d;->b:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/Kies/c;Lcom/sec/android/Kies/b;)V
    .locals 0

    .prologue
    .line 1827
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/d;-><init>(Lcom/sec/android/Kies/c;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/Kies/d;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)I
    .locals 1

    .prologue
    .line 1827
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/Kies/d;->a(Ljava/lang/String;Ljava/io/File;Ljava/io/File;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/io/File;Ljava/io/File;)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1884
    .line 1885
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    move-result v0

    .line 1887
    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1889
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1903
    :cond_0
    :goto_0
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "copy : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    iget-object v1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->b(Lcom/sec/android/Kies/c;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1905
    new-instance v0, Lcom/sec/android/Kies/g;

    iget-object v1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-direct {v0, v1}, Lcom/sec/android/Kies/g;-><init>(Lcom/sec/android/Kies/c;)V

    .line 1906
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/temp/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1907
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 1908
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1909
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1912
    const-string v3, "Path"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1913
    iget-object v1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    iget-object v1, v1, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v1, v2}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1915
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1917
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1918
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "temp/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GAKIES"

    invoke-virtual {v0, p2, p1, v1, v2}, Lcom/sec/android/Kies/g;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1939
    :goto_1
    iget-object v1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1944
    :try_start_0
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rename :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1945
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/temp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1949
    new-instance v2, Ljava/io/File;

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1998
    :cond_1
    :goto_2
    return v0

    .line 1891
    :cond_2
    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1892
    invoke-virtual {p3}, Ljava/io/File;->mkdir()Z

    .line 1893
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1894
    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1896
    const-string v3, "Path"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1897
    iget-object v3, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    iget-object v3, v3, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v3, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 1898
    invoke-virtual {p3, v6}, Ljava/io/File;->setWritable(Z)Z

    .line 1899
    const-string v1, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[APK BnR] OBJECT_ADDED"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1924
    :cond_3
    iget-object v1, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v1, p2, p1}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1926
    if-nez v1, :cond_4

    .line 1927
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->c(Lcom/sec/android/Kies/c;)I

    move-result v0

    goto :goto_2

    .line 1931
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "temp/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GAKIES"

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/sec/android/Kies/g;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 1951
    :catch_0
    move-exception v0

    .line 1952
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] temp copy err"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->c(Lcom/sec/android/Kies/c;)I

    move-result v0

    goto/16 :goto_2

    .line 1963
    :cond_5
    new-instance v1, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1965
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1968
    const/16 v3, 0x1000

    :try_start_1
    new-array v3, v3, [B

    .line 1970
    :goto_3
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_6

    .line 1971
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1976
    :catch_1
    move-exception v0

    .line 1977
    :try_start_2
    const-string v3, "KIES_START"

    const-string v4, "err"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1978
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "ENOSPC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1979
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->d(Lcom/sec/android/Kies/c;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 1984
    :goto_4
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1985
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 1990
    :goto_5
    invoke-virtual {p3}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1991
    invoke-virtual {p3, v6}, Ljava/io/File;->setWritable(Z)Z

    .line 1992
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : writable"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1974
    :cond_6
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1975
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1984
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1985
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_5

    .line 1986
    :catch_2
    move-exception v1

    goto :goto_5

    .line 1981
    :cond_7
    :try_start_6
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->c(Lcom/sec/android/Kies/c;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    goto :goto_4

    .line 1983
    :catchall_0
    move-exception v0

    .line 1984
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1985
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 1988
    :goto_6
    throw v0

    .line 1986
    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5
.end method

.method static synthetic a(Lcom/sec/android/Kies/d;Ljava/io/File;)J
    .locals 2

    .prologue
    .line 1827
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/d;->b(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/sec/android/Kies/d;Ljava/io/File;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1827
    invoke-direct {p0, p1, p2}, Lcom/sec/android/Kies/d;->a(Ljava/io/File;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 2091
    const-string v0, "KIES_START"

    const-string v1, "checkFile"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2095
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0, p1, p2}, Lcom/sec/android/Kies/c;->a(Lcom/sec/android/Kies/c;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 2097
    if-eqz v0, :cond_0

    .line 2098
    invoke-direct {p0, v0}, Lcom/sec/android/Kies/d;->c(Ljava/io/File;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/Kies/d;->b:J

    .line 2127
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/Kies/d;->b:J

    return-wide v0
.end method

.method private a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 2005
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 2007
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2008
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2009
    invoke-virtual {v3}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/Kies/d;->a(Ljava/io/File;)V

    .line 2007
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2011
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 2014
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/android/Kies/d;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1827
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/d;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2018
    if-nez p1, :cond_1

    .line 2031
    :cond_0
    :goto_0
    return v0

    .line 2021
    :cond_1
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 2023
    if-ltz v1, :cond_0

    .line 2026
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2028
    const-string v2, "apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2029
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/io/File;)J
    .locals 6

    .prologue
    .line 2040
    .line 2042
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 2044
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 2046
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2050
    aget-object v2, v1, v0

    new-instance v3, Lcom/sec/android/Kies/e;

    invoke-direct {v3, p0}, Lcom/sec/android/Kies/e;-><init>(Lcom/sec/android/Kies/d;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    .line 2044
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2075
    :cond_1
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2076
    invoke-direct {p0, v2}, Lcom/sec/android/Kies/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2077
    aget-object v2, v1, v0

    invoke-direct {p0, v2}, Lcom/sec/android/Kies/d;->c(Ljava/io/File;)J

    .line 2078
    iget-wide v2, p0, Lcom/sec/android/Kies/d;->b:J

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/Kies/d;->b:J

    goto :goto_1

    .line 2084
    :cond_2
    iget-wide v0, p0, Lcom/sec/android/Kies/d;->b:J

    return-wide v0
.end method

.method static synthetic b(Lcom/sec/android/Kies/d;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1827
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/d;->a(Ljava/io/File;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/Kies/d;Ljava/io/File;)J
    .locals 2

    .prologue
    .line 1827
    invoke-direct {p0, p1}, Lcom/sec/android/Kies/d;->c(Ljava/io/File;)J

    move-result-wide v0

    return-wide v0
.end method

.method private c(Ljava/io/File;)J
    .locals 7

    .prologue
    .line 2133
    const-wide/16 v2, 0x0

    .line 2135
    iget-object v0, p0, Lcom/sec/android/Kies/d;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2138
    if-nez v1, :cond_1

    .line 2188
    :cond_0
    :goto_0
    return-wide v2

    .line 2145
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/Kies/d;->a:Landroid/content/pm/PackageManager;

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v5, 0x80

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2153
    :goto_1
    if-eqz v0, :cond_0

    .line 2156
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2157
    const-string v0, "KIES_START"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "add packagename : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2160
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2162
    const-string v5, "base.apk"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ".apk"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2170
    :cond_2
    iget-object v4, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-virtual {v4, v0}, Lcom/sec/android/Kies/c;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2174
    iget-object v4, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v4}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2177
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->f(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2179
    iget-object v0, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v0}, Lcom/sec/android/Kies/c;->g(Lcom/sec/android/Kies/c;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2180
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    add-long/2addr v0, v2

    :goto_2
    move-wide v2, v0

    .line 2188
    goto :goto_0

    .line 2148
    :catch_0
    move-exception v0

    .line 2149
    const-string v0, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "not installed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2150
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/io/File;ILcom/sec/android/Kies/i;Ljava/lang/String;)I
    .locals 14

    .prologue
    .line 2208
    .line 2210
    const-string v2, "0000"

    .line 2211
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v4}, Lcom/sec/android/Kies/c;->h(Lcom/sec/android/Kies/c;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/AppList.xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2225
    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->i(Lcom/sec/android/Kies/c;)I

    .line 2227
    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->i(Lcom/sec/android/Kies/c;)I

    move-result v2

    mul-int v5, v2, p3

    .line 2228
    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->i(Lcom/sec/android/Kies/c;)I

    move-result v2

    mul-int v2, v2, p3

    iget-object v3, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v3}, Lcom/sec/android/Kies/c;->i(Lcom/sec/android/Kies/c;)I

    move-result v3

    add-int v6, v2, v3

    .line 2230
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    .line 2233
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v7

    .line 2234
    new-instance v8, Ljava/io/StringWriter;

    invoke-direct {v8}, Ljava/io/StringWriter;-><init>()V

    .line 2238
    :try_start_0
    invoke-interface {v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 2239
    const-string v2, "UTF-8"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2249
    const/4 v2, 0x0

    const-string v3, "Applications"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2250
    const/4 v2, 0x0

    const-string v3, "xmlns"

    const-string v4, "http://www.samsungmobile.com/applicationlist.xsd"

    invoke-interface {v7, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2253
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[APK BnR] app all count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2254
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_4

    .line 2255
    const-string v3, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[APK BnR] "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, " : "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2256
    if-eqz p5, :cond_1

    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2258
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[APK BnR]"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "continue"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2254
    :cond_0
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2262
    :cond_1
    if-ge v4, v5, :cond_2

    .line 2263
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[APK BnR] scan continue : "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2405
    :catch_0
    move-exception v2

    .line 2406
    const-string v2, "KIES_START"

    const-string v3, "[APK BnR] error occurred while creating xml file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2407
    const/4 v2, 0x1

    .line 2410
    :goto_2
    return v2

    .line 2265
    :cond_2
    if-lt v4, v6, :cond_3

    .line 2266
    :try_start_1
    const-string v2, "KIES_START"

    const-string v3, "[APK BnR] data more "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2268
    const/4 v2, 0x2

    .line 2276
    const/4 v3, 0x0

    const-string v4, "Applications"

    invoke-interface {v7, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2277
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 2278
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 2280
    invoke-virtual {v8}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    iput-object v3, v0, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    .line 2282
    const-string v3, "KIES_START"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[APK BnR] str "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2288
    :cond_3
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 2289
    iget-object v3, p0, Lcom/sec/android/Kies/d;->a:Landroid/content/pm/PackageManager;

    iget-object v9, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v10, 0x2200

    invoke-virtual {v3, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    .line 2293
    iget-object v10, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    iget-object v3, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v3}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v10, v3}, Lcom/sec/android/Kies/c;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2297
    iget-object v3, p0, Lcom/sec/android/Kies/d;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v9}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2298
    const-string v3, "KIES_START"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[APK BnR] BackupApplist i = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " appName : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2299
    iget-object v3, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v3}, Lcom/sec/android/Kies/c;->f(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 2300
    const-string v3, "KIES_START"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[APK BnR] BackupApplist "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " size = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2301
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 2302
    const-string v2, "KIES_START"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[APK BnR] BackupApplist "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " appVer = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2314
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2322
    const/4 v2, 0x0

    const-string v12, "Applicaion"

    invoke-interface {v7, v2, v12}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2323
    const/4 v12, 0x0

    const-string v13, "AppId"

    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v7, v12, v13, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2324
    const/4 v2, 0x0

    const-string v12, "Name"

    invoke-interface {v7, v2, v12, v9}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2325
    const-string v9, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[APK BnR] BackupApplist "

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " AppId = "

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2332
    const/4 v2, 0x0

    const-string v9, "Type"

    invoke-interface {v7, v2, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2333
    const-string v2, "download"

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2334
    const/4 v2, 0x0

    const-string v9, "Type"

    invoke-interface {v7, v2, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2336
    const/4 v2, 0x0

    const-string v9, "Version"

    invoke-interface {v7, v2, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2337
    if-eqz v3, :cond_5

    .line 2338
    invoke-interface {v7, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2341
    :goto_3
    const/4 v2, 0x0

    const-string v3, "Version"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2346
    const/4 v2, 0x0

    const-string v3, "Size"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2347
    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2348
    const/4 v2, 0x0

    const-string v3, "Size"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2363
    const/4 v2, 0x0

    const-string v3, "Path"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2365
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 2370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal:/_SamsungBnR_/Abackup/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/Kies/d;->c:Lcom/sec/android/Kies/c;

    invoke-static {v2}, Lcom/sec/android/Kies/c;->e(Lcom/sec/android/Kies/c;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2374
    const-string v3, "/"

    const-string v9, "\\"

    invoke-virtual {v2, v3, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 2378
    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2379
    const/4 v3, 0x0

    const-string v9, "Path"

    invoke-interface {v7, v3, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2381
    const/4 v3, 0x0

    const-string v9, "DataPath"

    invoke-interface {v7, v3, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2382
    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2383
    const/4 v2, 0x0

    const-string v3, "DataPath"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2385
    const/4 v2, 0x0

    const-string v3, "Applicaion"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2387
    if-eqz p5, :cond_0

    .line 2392
    :cond_4
    const-string v2, "KIES_START"

    const-string v3, "[APK BnR] end for "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2393
    const/4 v2, 0x0

    const-string v3, "Applications"

    invoke-interface {v7, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 2394
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 2396
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 2400
    invoke-virtual {v8}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    .line 2402
    const-string v2, "KIES_START"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[APK BnR] str "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/sec/android/Kies/i;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2340
    :cond_5
    const-string v2, ""

    invoke-interface {v7, v2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method
