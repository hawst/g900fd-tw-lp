.class public Lcom/sec/android/Kies/kies_start;
.super Landroid/app/Service;
.source "kies_start.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static a:Ljava/lang/String;

.field public static c:I

.field public static d:I


# instance fields
.field public b:Landroid/os/IBinder;

.field public e:Lcom/sec/android/Kies/c;

.field private f:Landroid/app/admin/DevicePolicyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137
    sput v0, Lcom/sec/android/Kies/kies_start;->c:I

    .line 138
    sput v0, Lcom/sec/android/Kies/kies_start;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 135
    new-instance v0, Lcom/sec/android/Kies/j;

    invoke-direct {v0, p0}, Lcom/sec/android/Kies/j;-><init>(Lcom/sec/android/Kies/kies_start;)V

    iput-object v0, p0, Lcom/sec/android/Kies/kies_start;->b:Landroid/os/IBinder;

    .line 910
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 895
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 897
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/Kies/kies_start;->b:Landroid/os/IBinder;

    if-eqz v1, :cond_0

    .line 898
    iget-object v1, p0, Lcom/sec/android/Kies/kies_start;->b:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    :cond_0
    :goto_0
    return-void

    .line 900
    :catch_0
    move-exception v0

    .line 901
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/Kies/kies_start;)Z
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/Kies/kies_start;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/sec/android/Kies/kies_start;)Z
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/Kies/kies_start;->d()Z

    move-result v0

    return v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/Kies/kies_start;)Z
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/Kies/kies_start;->c()Z

    move-result v0

    return v0
.end method

.method private d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 162
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    const-string v4, "application/vnd.android.package-archive"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 166
    if-nez v2, :cond_0

    .line 170
    :goto_0
    return v1

    .line 169
    :cond_0
    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 170
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 905
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 245
    sget v0, Lcom/sec/android/Kies/kies_start;->d:I

    packed-switch v0, :pswitch_data_0

    .line 306
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->stopSelf()V

    .line 308
    const/4 v0, 0x1

    return v0

    .line 274
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->b()I

    goto :goto_0

    .line 279
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->onDestroy()V

    goto :goto_0

    .line 288
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/Kies/kies_start;->e:Lcom/sec/android/Kies/c;

    invoke-virtual {v0}, Lcom/sec/android/Kies/c;->b()V

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public a()Landroid/app/admin/DevicePolicyManager;
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/Kies/kies_start;->f:Landroid/app/admin/DevicePolicyManager;

    if-nez v0, :cond_0

    .line 175
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/kies_start;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/sec/android/Kies/kies_start;->f:Landroid/app/admin/DevicePolicyManager;

    .line 176
    iget-object v0, p0, Lcom/sec/android/Kies/kies_start;->f:Landroid/app/admin/DevicePolicyManager;

    if-nez v0, :cond_0

    .line 177
    const-string v0, "KIES_START"

    const-string v1, "Can\'t get DevicePolicyManagerService: is it running?"

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Stack trace:"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/Kies/kies_start;->f:Landroid/app/admin/DevicePolicyManager;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 834
    .line 839
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    const-string v2, "/data/data/com.sec.android.Kies/kies_limit_status.dat"

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 840
    :try_start_1
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 841
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 850
    if-eqz v2, :cond_0

    .line 852
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 854
    :cond_0
    if-eqz v0, :cond_1

    .line 855
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 861
    :cond_1
    :goto_0
    return-void

    .line 857
    :catch_0
    move-exception v0

    .line 858
    const-string v0, "KIES_START"

    const-string v1, "IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 843
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 844
    :goto_1
    :try_start_4
    const-string v2, "KIES_START"

    const-string v3, "FileNotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 850
    if-eqz v1, :cond_2

    .line 852
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    .line 854
    :cond_2
    if-eqz v0, :cond_1

    .line 855
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 857
    :catch_2
    move-exception v0

    .line 858
    const-string v0, "KIES_START"

    const-string v1, "IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 845
    :catch_3
    move-exception v0

    move-object v0, v1

    .line 846
    :goto_2
    :try_start_6
    const-string v2, "KIES_START"

    const-string v3, "IOException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 850
    if-eqz v1, :cond_3

    .line 852
    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    .line 854
    :cond_3
    if-eqz v0, :cond_1

    .line 855
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 857
    :catch_4
    move-exception v0

    .line 858
    const-string v0, "KIES_START"

    const-string v1, "IOException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 849
    :catchall_0
    move-exception v0

    move-object v2, v1

    .line 850
    :goto_3
    if-eqz v2, :cond_4

    .line 852
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    .line 854
    :cond_4
    if-eqz v1, :cond_5

    .line 855
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 859
    :cond_5
    :goto_4
    throw v0

    .line 857
    :catch_5
    move-exception v1

    .line 858
    const-string v1, "KIES_START"

    const-string v2, "IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 849
    :catchall_1
    move-exception v2

    move-object v4, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catchall_3
    move-exception v2

    move-object v4, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    .line 845
    :catch_6
    move-exception v2

    goto :goto_2

    :catch_7
    move-exception v1

    move-object v1, v2

    goto :goto_2

    .line 843
    :catch_8
    move-exception v2

    goto :goto_1

    :catch_9
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method

.method public b()I
    .locals 2

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/sec/android/Kies/kies_start;->a()Landroid/app/admin/DevicePolicyManager;

    move-result-object v0

    .line 817
    if-eqz v0, :cond_1

    .line 818
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getAllowDesktopSync(Landroid/content/ComponentName;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 819
    const-string v0, "True"

    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/kies_start;->a(Ljava/lang/String;)V

    .line 827
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 821
    :cond_0
    const-string v0, "False"

    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/kies_start;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 824
    :cond_1
    const-string v0, "Invalid"

    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/kies_start;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/sec/android/Kies/kies_start;->b:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 201
    const-string v0, "KIES_START"

    const-string v1, "Service onCreate!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 204
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/Kies/kies_start;->a(Z)V

    .line 205
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 876
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 877
    const-string v0, "kies_start::onDestroy()"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/Kies/kies_start;->a:Ljava/lang/String;

    .line 878
    const-string v0, "KIES_START"

    const-string v1, "[APK BnR] apk detail request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    invoke-direct {p0, v2}, Lcom/sec/android/Kies/kies_start;->a(Z)V

    .line 880
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 210
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 221
    new-instance v0, Lcom/sec/android/Kies/b;

    invoke-direct {v0, p0}, Lcom/sec/android/Kies/b;-><init>(Lcom/sec/android/Kies/kies_start;)V

    .line 233
    new-instance v1, Lcom/sec/android/Kies/c;

    sget v2, Lcom/sec/android/Kies/kies_start;->d:I

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/sec/android/Kies/c;-><init>(Lcom/sec/android/Kies/kies_start;Landroid/content/Intent;ILandroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/Kies/kies_start;->e:Lcom/sec/android/Kies/c;

    .line 240
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 241
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 242
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 152
    sget v0, Lcom/sec/android/Kies/kies_start;->c:I

    invoke-virtual {p0, v0}, Lcom/sec/android/Kies/kies_start;->a(I)I

    .line 153
    return-void
.end method
