.class Lcom/sec/android/Kies/h;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "kies_start.java"


# instance fields
.field public a:Z

.field final synthetic b:Lcom/sec/android/Kies/c;


# direct methods
.method constructor <init>(Lcom/sec/android/Kies/c;)V
    .locals 1

    .prologue
    .line 2633
    iput-object p1, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    .line 2635
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/Kies/h;->a:Z

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 2640
    new-array v0, v5, [B

    .line 2643
    if-eq p2, v6, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 2645
    :cond_0
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was installed successfully"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2646
    iget-object v1, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->l(Lcom/sec/android/Kies/c;)[B

    move-result-object v1

    invoke-static {v1, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2659
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_READY_RESTORE_APK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2661
    const-string v2, "head"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2662
    iget-boolean v0, p0, Lcom/sec/android/Kies/h;->a:Z

    if-nez v0, :cond_5

    .line 2663
    iget-object v0, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    iget-object v0, v0, Lcom/sec/android/Kies/c;->c:Lcom/sec/android/Kies/kies_start;

    invoke-virtual {v0, v1}, Lcom/sec/android/Kies/kies_start;->sendBroadcast(Landroid/content/Intent;)V

    .line 2664
    iput-boolean v6, p0, Lcom/sec/android/Kies/h;->a:Z

    .line 2665
    const-string v0, "KIES_START"

    const-string v1, "Observer sent Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2669
    :goto_1
    return-void

    .line 2648
    :cond_1
    const-string v1, "KIES_START"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " wasn\'t installed. Returned error code is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2651
    const/4 v1, -0x2

    if-ne p2, v1, :cond_2

    .line 2652
    iget-object v1, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->m(Lcom/sec/android/Kies/c;)[B

    move-result-object v1

    invoke-static {v1, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 2653
    :cond_2
    const/4 v1, -0x4

    if-eq p2, v1, :cond_3

    const/16 v1, -0x14

    if-ne p2, v1, :cond_4

    .line 2654
    :cond_3
    iget-object v1, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->n(Lcom/sec/android/Kies/c;)[B

    move-result-object v1

    invoke-static {v1, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 2656
    :cond_4
    iget-object v1, p0, Lcom/sec/android/Kies/h;->b:Lcom/sec/android/Kies/c;

    invoke-static {v1}, Lcom/sec/android/Kies/c;->o(Lcom/sec/android/Kies/c;)[B

    move-result-object v1

    invoke-static {v1, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 2667
    :cond_5
    const-string v0, "KIES_START"

    const-string v1, "Observer doesn\'t sent Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
