.class Lcom/lifevibes/trimapp/Trim$9;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lifevibes/trimapp/Trim;->getEditTextFilter()[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$9;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8

    const v7, 0x7f07001f

    const/4 v2, 0x0

    const-string v0, ""

    invoke-static {p1}, Lcom/lifevibes/trimapp/util/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$9;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v0, v7, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    move v1, v2

    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v1, v3, :cond_4

    move v3, v2

    move v4, v2

    :goto_2
    sget-object v5, Lcom/lifevibes/trimapp/Trim;->mInvalidFileNameChar:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_2

    sget-object v5, Lcom/lifevibes/trimapp/Trim;->mInvalidFileNameChar:[Ljava/lang/String;

    aget-object v5, v5, v3

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    if-ne v5, v6, :cond_1

    const/4 v4, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    if-nez v4, :cond_3

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v1, v3, :cond_5

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$9;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v1, v7, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    goto :goto_0

    :cond_5
    move-object v0, p1

    goto :goto_0
.end method
