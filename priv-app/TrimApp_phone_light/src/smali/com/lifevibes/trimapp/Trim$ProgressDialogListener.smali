.class Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f07001c

    # invokes: Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$2400(Lcom/lifevibes/trimapp/Trim;I)V

    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2000(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v2, 0x7f070019

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v5}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v6}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v6

    aget-object v5, v5, v6

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/lifevibes/trimapp/Trim;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/lifevibes/trimapp/Trim;->showToast(Ljava/lang/CharSequence;I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$300(Lcom/lifevibes/trimapp/Trim;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2100(Lcom/lifevibes/trimapp/Trim;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$2100(Lcom/lifevibes/trimapp/Trim;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsConfigChanged:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2800(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/Trim;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method
