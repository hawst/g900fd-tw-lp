.class Lcom/lifevibes/trimapp/Trim_Share$14;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lifevibes/trimapp/Trim_Share;->onPrepareDialog(ILandroid/app/Dialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1000(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/Storage;

    move-result-object v1

    iget-object v1, v1, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$14;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
