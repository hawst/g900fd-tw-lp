.class Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
.super Landroid/os/AsyncTask;

# interfaces
.implements Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThumbnailTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;"
    }
.end annotation


# static fields
.field public static final RESULT_EXCEPTION:I = 0x1

.field public static final RESULT_INTERRUPTION:I = 0x2

.field public static final RESULT_SUCCESS:I = 0x3

.field public static final RESULT_UNSUPPORTED:I = 0x4


# instance fields
.field public mCurrentMask:I

.field private mEndTime:I

.field public mGetThumbnailsDone:Z

.field private mHeight:I

.field public mImageViewsReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<[",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public mIsReleased:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field public mNeedLoopRestart:Z

.field public mOpeningDone:Z

.field private mStartTime:I

.field public mTaskId:J

.field private mThumbnailNb:I

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWidth:I

.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mIsReleased:Z

    iput-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mOpeningDone:Z

    iput-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    iput-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPowerManager:Landroid/os/PowerManager;
    invoke-static {p1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3700(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/PowerManager;

    move-result-object v0

    const/4 v1, 0x6

    const-string v2, "TrimApp"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    # |= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I
    invoke-static {p1, v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$3576(Lcom/lifevibes/trimapp/Trim_Share;I)I

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I
    invoke-static {p1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3800(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I
    invoke-static {p1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3800(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    :goto_0
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->setTaskId()V

    return-void

    :cond_0
    iput-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method private getNextMask()I
    .locals 4

    const/16 v0, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$1700(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v0, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/16 v2, -0x9

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1772(Lcom/lifevibes/trimapp/Trim_Share;I)I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1700(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v2, -0x3

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1772(Lcom/lifevibes/trimapp/Trim_Share;I)I

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1700(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, -0x5

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1772(Lcom/lifevibes/trimapp/Trim_Share;I)I

    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getThumbnailInfo(I)Z
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mEndTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mThumbnailNb:I

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4600(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mImageViewsReference:Ljava/lang/ref/WeakReference;

    sget v1, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_WIDTH:I

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    sget v1, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_HEIGHT:I

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v1

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v1

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1800(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    if-gez v1, :cond_1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v1

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    add-int/lit16 v1, v1, -0xc8

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v0

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    add-int/lit16 v0, v0, -0xc8

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    goto :goto_1

    :sswitch_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1500(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mImageViewsReference:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$4500(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mThumbnailNb:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v0

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    div-int/2addr v0, v1

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$4500(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    move-result-object v0

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mEndTime:I

    goto :goto_1

    :sswitch_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1300(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mImageViewsReference:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mThumbnailNb:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getStartTime()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getEndTime()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mEndTime:I

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method private onTaskDestroyed(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1602(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$3202(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->updateButtonsEnabled()V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f07000f

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$300(Lcom/lifevibes/trimapp/Trim_Share;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsMultiWindowError:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$600(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f070025

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    :goto_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$300(Lcom/lifevibes/trimapp/Trim_Share;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f070013

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    goto :goto_1
.end method

.method private releaseThumbnailHandle()V
    .locals 2

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mIsReleased:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mIsReleased:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->releaseThumbnail()Z

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, -0x2

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3572(Lcom/lifevibes/trimapp/Trim_Share;I)I

    return-void
.end method

.method private setTaskId()V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget-wide v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v0, v2, v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$4402(Lcom/lifevibes/trimapp/Trim_Share;J)J

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 13

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    const/4 v11, 0x3

    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, -0x2

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3572(Lcom/lifevibes/trimapp/Trim_Share;I)I

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :goto_1
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mOpeningDone:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$100(Lcom/lifevibes/trimapp/Trim_Share;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v2

    aget-object v1, v1, v2

    sget v2, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_WIDTH:I

    sget v3, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_HEIGHT:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lifevibes/trimapp/util/MediaShare;->createThumbnail(Ljava/lang/String;II)Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mOpeningDone:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_1

    :cond_2
    :try_start_3
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->getThumbnailInfo(I)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x2

    :goto_4
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->releaseThumbnailHandle()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    :cond_5
    :try_start_4
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->getNextMask()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_4

    :cond_6
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    add-int/2addr v0, v1

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_7

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    :cond_7
    const/4 v0, 0x2

    goto :goto_4

    :cond_8
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    if-nez v0, :cond_9

    move v0, v11

    goto :goto_4

    :cond_9
    const/4 v12, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    :cond_a
    :try_start_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mThumbnailNb:I

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    int-to-long v2, v2

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mEndTime:I

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/trimapp/util/MediaShare;->isThumbnailSpeedPreferedOverAccuracy(IJJ)Z

    move-result v9

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v11

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v1

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    int-to-long v4, v0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mEndTime:I

    int-to-long v6, v0

    iget v8, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mThumbnailNb:I

    move-object v10, p0

    invoke-virtual/range {v1 .. v10}, Lcom/lifevibes/trimapp/util/MediaShare;->getThumbnailListCB(IIJJIZLcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v1, v12

    :goto_5
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_c

    iget-wide v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x2

    goto/16 :goto_4

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v0, 0x4

    goto/16 :goto_4

    :cond_d
    :try_start_7
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWidth:I

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mHeight:I

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mStartTime:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lifevibes/trimapp/util/MediaShare;->getThumbnail(III)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    const/4 v0, 0x1

    goto/16 :goto_4

    :catch_1
    move-exception v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    const/4 v0, 0x4

    goto/16 :goto_4

    :cond_e
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_11

    if-eqz v1, :cond_11

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mImageViewsReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/ImageView;

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {v2, v3}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    if-eqz v0, :cond_11

    new-instance v3, Ljava/lang/ref/WeakReference;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v2, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mImageViewReference:Ljava/lang/ref/WeakReference;

    iput-object v1, v2, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_f

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-eqz v0, :cond_10

    :cond_f
    const/4 v0, 0x2

    goto/16 :goto_4

    :cond_10
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->publishProgress([Ljava/lang/Object;)V

    :cond_11
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->setTaskId()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :cond_12
    move-object v1, v0

    goto/16 :goto_5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 5

    const/16 v4, 0x32

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mOpeningDone:Z

    if-eqz v2, :cond_3

    :cond_0
    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/util/MediaShare;->cancelThumbnail()V

    :cond_1
    :goto_1
    if-ge v0, v4, :cond_2

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    if-eqz v1, :cond_4

    :cond_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->releaseThumbnailHandle()V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->onTaskDestroyed(I)V

    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    return-void

    :cond_3
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->onTaskDestroyed(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->doPlayPauseAction()V

    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4000(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4200(Lcom/lifevibes/trimapp/Trim_Share;Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;)V
    .locals 9

    const/4 v1, 0x0

    aget-object v8, p1, v1

    if-eqz v8, :cond_2

    iget-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mImageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ImageView;

    if-eqz v7, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-object v5, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mMatrix:Landroid/graphics/Matrix;

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, v8, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    :cond_2
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;

    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->onProgressUpdate([Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;)V

    return-void
.end method

.method public onThumbnailProgress(IILandroid/graphics/Bitmap;)V
    .locals 6

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mImageViewsReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/ImageView;

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {v1, v2}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$4500(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v3

    add-int/2addr v3, p2

    aget-object v0, v0, v3

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mImageViewReference:Ljava/lang/ref/WeakReference;

    :goto_1
    iput-object p3, v1, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v2, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mTaskId:J

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/ref/WeakReference;

    aget-object v0, v0, p2

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;->mImageViewReference:Ljava/lang/ref/WeakReference;

    goto :goto_1
.end method
