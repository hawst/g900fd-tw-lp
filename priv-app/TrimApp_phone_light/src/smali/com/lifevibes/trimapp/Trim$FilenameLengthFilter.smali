.class Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;
.super Landroid/text/InputFilter$LengthFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FilenameLengthFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 1

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/16 v0, 0x32

    invoke-direct {p0, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 3

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v0

    sub-int v1, p3, p2

    add-int/2addr v0, v1

    sub-int v1, p6, p5

    sub-int/2addr v0, v1

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f070021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    :cond_0
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
