.class Lcom/lifevibes/trimapp/Trim$7;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/16 v1, 0x7d0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    new-instance v1, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {v1, v2}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    # setter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1202(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$ThumbnailTask;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z
    invoke-static {v0, v3}, Lcom/lifevibes/trimapp/Trim;->access$2902(Lcom/lifevibes/trimapp/Trim;Z)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mIsReleased:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$3000(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$3000(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$3000(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/lifevibes/trimapp/Trim;->doTrimmingAction(II)V
    invoke-static {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim;->access$3100(Lcom/lifevibes/trimapp/Trim;II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$7;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim;->doPlayPauseAction()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
