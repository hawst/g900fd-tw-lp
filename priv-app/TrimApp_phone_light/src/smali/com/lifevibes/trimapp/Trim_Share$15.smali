.class Lcom/lifevibes/trimapp/Trim_Share$15;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/16 v1, 0x7d0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {v1, v2}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1602(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z
    invoke-static {v0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$3202(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mIsReleased:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3200(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->doTrimmingAction(II)V
    invoke-static {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$3400(Lcom/lifevibes/trimapp/Trim_Share;II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$15;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->doPlayPauseAction()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
