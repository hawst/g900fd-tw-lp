.class Lcom/lifevibes/trimapp/Trim_Share$16;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->setProgressDialogValue(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3500(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$16;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$100(Lcom/lifevibes/trimapp/Trim_Share;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->refreshAllDisplay(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->setVideoProgress()I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2000(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x67

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share$16;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mSaveHandlePosition:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2900(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->moveUserHandleTo(IZ)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1802(Lcom/lifevibes/trimapp/Trim_Share;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->resolveUpdatePlayPauseMessage(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$3600(Lcom/lifevibes/trimapp/Trim_Share;I)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$16;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
