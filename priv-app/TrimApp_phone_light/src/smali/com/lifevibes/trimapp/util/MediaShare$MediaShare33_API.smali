.class Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;
.implements Lcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/util/MediaShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaShare33_API"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    }
.end annotation


# instance fields
.field private final mExpertCancelHandler:Landroid/os/Handler;

.field private mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

.field private mLimitAudioCodec:I

.field private mLimitBitrate:I

.field private mLimitHeight:I

.field private mLimitMillis:J

.field private mLimitRatio:I

.field private mLimitVideoCodec:I

.field private mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

.field private mOutput:Ljava/lang/String;

.field private mProjectPath:Ljava/lang/String;

.field private mTNListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;

.field private mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

.field public mTrimmingStatus:I

.field private mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

.field final synthetic this$0:Lcom/lifevibes/trimapp/util/MediaShare;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/util/MediaShare;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExpertCancelHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->onThumbnail(Landroid/graphics/Bitmap;II)V

    return-void
.end method

.method static synthetic access$000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExpertCancelHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)J
    .locals 2

    iget-wide v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitMillis:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    return v0
.end method

.method static synthetic access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    return-object v0
.end method

.method static synthetic access$402(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/videoeditor/VideoEditor;)Lcom/lifevibes/videoeditor/VideoEditor;
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    return-object p1
.end method

.method static synthetic access$500(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    return v0
.end method

.method static synthetic access$700(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitAudioCodec:I

    return v0
.end method

.method static synthetic access$800(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitVideoCodec:I

    return v0
.end method

.method static synthetic access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    return-object p1
.end method


# virtual methods
.method public cancelThumbnail()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    invoke-virtual {v0}, Lcom/lifevibes/videoeditor/MediaVideoItem;->cancelThumbnailList()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    :cond_0
    return-void
.end method

.method public createThumbnail(Ljava/lang/String;II)Z
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->loadLibrary()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    new-instance v1, Lcom/lifevibes/videoeditor/MediaVideoItem;

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    const-string v3, "ms3"

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I
    .locals 10

    const/16 v9, 0x60

    const/4 v8, 0x2

    const/4 v2, 0x0

    const v0, 0x40000008    # 2.000002f

    const v1, 0x4000000a    # 2.0000024f

    const/4 v3, 0x1

    iput v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/32 v6, 0x7fffffff

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    const v0, 0x40000006    # 2.0000014f

    :goto_0
    return v0

    :cond_0
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->loadLibrary()Z

    move-result v3

    if-nez v3, :cond_1

    const v0, -0x7ffffffc

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Lcom/lifevibes/videoeditor/MediaVideoItem;

    iget-object v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    const-string v6, "m1"

    const/4 v7, 0x0

    invoke-direct {v3, v5, v6, p1, v7}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFileType()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFileType:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAspectRatio()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAspectRatio:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFps:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoBitrate:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoProfile()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioType:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioBitrate:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v4

    iput v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioSamplingFrequency:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    :cond_2
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xa5

    if-le v3, v4, :cond_1c

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v3

    :try_start_1
    const-string v5, "TrimApp_MS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "new MediaVideoItem\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFileType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFileType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v6

    long-to-int v3, v6

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAspectRatio()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAspectRatio:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFps:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoProfile()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioSamplingFrequency:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    :cond_3
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xa5

    if-le v3, v4, :cond_f

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_4
    throw v3

    :cond_5
    const/high16 v0, 0x40000000    # 2.0f

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFileType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFileType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v6

    long-to-int v3, v6

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAspectRatio()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAspectRatio:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFps:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoProfile()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioSamplingFrequency:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xa5

    if-le v3, v4, :cond_8

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_7
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xff

    if-ne v3, v4, :cond_a

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x500

    if-le v0, v3, :cond_9

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x2d0

    if-le v0, v3, :cond_9

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    move v0, v1

    goto/16 :goto_0

    :cond_9
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x80

    if-ge v0, v3, :cond_b

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    if-ge v0, v9, :cond_b

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    move v0, v1

    goto/16 :goto_0

    :cond_a
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0xf00

    if-le v0, v3, :cond_9

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x870

    if-le v0, v3, :cond_9

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    move v0, v1

    goto/16 :goto_0

    :cond_b
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {p0, v0, v3, v4}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_c

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6
    move v0, v1

    goto/16 :goto_0

    :cond_c
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    if-le v0, v8, :cond_d

    const-string v0, "TrimApp_MS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported audio channels "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    const v0, 0x4000000c    # 2.0000029f

    goto/16 :goto_0

    :cond_d
    move v0, v2

    goto/16 :goto_0

    :cond_e
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xff

    if-ne v3, v4, :cond_11

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_f
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x500

    if-le v0, v3, :cond_10

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x2d0

    if-le v0, v3, :cond_10

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_10
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x80

    if-ge v0, v3, :cond_12

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    if-ge v0, v9, :cond_12

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_11
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0xf00

    if-le v0, v3, :cond_10

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x870

    if-le v0, v3, :cond_10

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_12
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {p0, v0, v3, v4}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_13

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_13
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    if-le v0, v8, :cond_d

    const-string v0, "TrimApp_MS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported audio channels "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :catch_1
    move-exception v3

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFileType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFileType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v6

    long-to-int v3, v6

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAspectRatio()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAspectRatio:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mFps:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoProfile()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioType:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioBitrate()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioBitrate:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioChannels()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioSamplingFrequency()I

    move-result v3

    iput v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioSamplingFrequency:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    :cond_14
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xa5

    if-le v3, v4, :cond_16

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_15
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xff

    if-ne v3, v4, :cond_18

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_16
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x500

    if-le v0, v3, :cond_17

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x2d0

    if-le v0, v3, :cond_17

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_17
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x80

    if-ge v0, v3, :cond_19

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    if-ge v0, v9, :cond_19

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_18
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0xf00

    if-le v0, v3, :cond_17

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x870

    if-le v0, v3, :cond_17

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_19
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {p0, v0, v3, v4}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_1a
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    if-le v0, v8, :cond_d

    const-string v0, "TrimApp_MS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported audio channels "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_1b
    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    const/16 v4, 0xff

    if-ne v3, v4, :cond_1e

    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported input profile or level ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoProfile:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_1c
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x500

    if-le v0, v3, :cond_1d

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x2d0

    if-le v0, v3, :cond_1d

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_1d
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0x80

    if-ge v0, v3, :cond_1f

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    if-ge v0, v9, :cond_1f

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_1e
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    const/16 v3, 0xf00

    if-le v0, v3, :cond_1d

    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    const/16 v3, 0x870

    if-le v0, v3, :cond_1d

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_1f
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget v4, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {p0, v0, v3, v4}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported resolution width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_20
    iget v0, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    if-le v0, v8, :cond_d

    const-string v0, "TrimApp_MS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported audio channels "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mAudioChannels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7
.end method

.method public getThumbnail(III)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    const-string v1, "TrimApp_MS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "**** getThumbnail() : Offset :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    int-to-long v2, p3

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getThumbnail(IIJ)Landroid/graphics/Bitmap;

    move-result-object v1

    const-string v2, "TrimApp_MS"

    const-string v3, "**** getThumbnail() : Offset Done"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public getThumbnailListCB(IIJJIZLcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;)V
    .locals 13

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTNListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;

    :try_start_0
    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "**** getThumbnailListCB() : Offset :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    move v4, p1

    move v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move/from16 v10, p7

    move/from16 v11, p8

    move-object v12, p0

    invoke-virtual/range {v3 .. v12}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getThumbnailList(IIJJIZLcom/lifevibes/videoeditor/MediaItem$GetThumbnailListCallback;)V

    const-string v2, "TrimApp_MS"

    const-string v3, "**** getThumbnailListCB() : Offset Done"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v2

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_2
    move-exception v2

    goto :goto_0

    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method public getTrimmingStatus()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I

    return v0
.end method

.method public isNullEncordingAvailable(III)Z
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0x438

    const/16 v4, 0x2d0

    const/16 v3, 0x1e0

    const/4 v0, 0x1

    const/16 v2, 0xf00

    if-gt p1, v2, :cond_0

    const/16 v2, 0x870

    if-le p2, v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/16 v2, 0xf00

    if-ne p1, v2, :cond_3

    const/16 v2, 0x870

    if-ne p2, v2, :cond_3

    const/4 v2, 0x4

    if-eq p3, v2, :cond_1

    const/4 v2, 0x5

    if-eq p3, v2, :cond_1

    const/4 v2, 0x6

    if-eq p3, v2, :cond_1

    :cond_3
    const/16 v2, 0xa00

    if-ne p1, v2, :cond_4

    const/16 v2, 0x5a0

    if-ne p2, v2, :cond_4

    const/4 v2, 0x4

    if-eq p3, v2, :cond_1

    const/4 v2, 0x5

    if-eq p3, v2, :cond_1

    const/4 v2, 0x6

    if-eq p3, v2, :cond_1

    :cond_4
    const/16 v2, 0x780

    if-ne p1, v2, :cond_5

    if-ne p2, v5, :cond_5

    if-ne p3, v0, :cond_1

    :cond_5
    const/16 v2, 0x5a0

    if-ne p1, v2, :cond_6

    if-ne p2, v5, :cond_6

    if-ne p3, v0, :cond_1

    :cond_6
    const/16 v2, 0x500

    if-ne p1, v2, :cond_7

    if-ne p2, v4, :cond_7

    if-ne p3, v0, :cond_1

    :cond_7
    if-ne p1, v5, :cond_8

    if-ne p2, v4, :cond_8

    if-ne p3, v0, :cond_1

    :cond_8
    const/16 v2, 0x3c0

    if-ne p1, v2, :cond_9

    if-ne p2, v4, :cond_9

    if-ne p3, v0, :cond_1

    :cond_9
    const/16 v2, 0x356

    if-ne p1, v2, :cond_a

    if-ne p2, v3, :cond_a

    if-ne p3, v0, :cond_1

    :cond_a
    const/16 v2, 0x320

    if-ne p1, v2, :cond_b

    if-ne p2, v3, :cond_b

    if-ne p3, v0, :cond_1

    :cond_b
    const/16 v2, 0x320

    if-ne p1, v2, :cond_c

    const/16 v2, 0x1c2

    if-ne p2, v2, :cond_c

    if-ne p3, v0, :cond_1

    :cond_c
    if-ne p1, v4, :cond_d

    if-ne p2, v3, :cond_d

    if-ne p3, v0, :cond_1

    :cond_d
    const/16 v2, 0x280

    if-ne p1, v2, :cond_e

    if-ne p2, v3, :cond_e

    if-ne p3, v0, :cond_1

    :cond_e
    const/16 v2, 0x280

    if-ne p1, v2, :cond_f

    const/16 v2, 0x168

    if-ne p2, v2, :cond_f

    if-ne p3, v0, :cond_1

    :cond_f
    if-ne p1, v3, :cond_10

    const/16 v2, 0x140

    if-ne p2, v2, :cond_10

    if-ne p3, v0, :cond_1

    :cond_10
    const/16 v2, 0x160

    if-ne p1, v2, :cond_11

    const/16 v2, 0x120

    if-eq p2, v2, :cond_1

    :cond_11
    const/16 v2, 0x140

    if-ne p1, v2, :cond_12

    const/16 v2, 0xf0

    if-ne p2, v2, :cond_12

    if-ne p3, v0, :cond_1

    :cond_12
    const/16 v2, 0xb0

    if-ne p1, v2, :cond_13

    const/16 v2, 0x90

    if-eq p2, v2, :cond_1

    :cond_13
    const/16 v2, 0xa0

    if-ne p1, v2, :cond_14

    const/16 v2, 0x78

    if-ne p2, v2, :cond_14

    if-ne p3, v0, :cond_1

    :cond_14
    const/16 v2, 0x80

    if-ne p1, v2, :cond_15

    const/16 v2, 0x60

    if-eq p2, v2, :cond_1

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public isThumbnailSpeedPreferedOverAccuracy(IJJ)Z
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaVideoItem;->isThumbnailSpeedPreferedOverAccuracy(IJJ)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v6

    goto :goto_0
.end method

.method public loadLibrary()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mProjectPath:Ljava/lang/String;

    :try_start_0
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v3, "/system/lib/liblifevibes_mediashare_sw_jni.so"

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    const-string v3, "TrimApp_MS"

    const-string v4, "ms sw loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-static {v2}, Lcom/lifevibes/videoeditor/VideoEditorFactory;->create(Ljava/lang/String;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v2

    iput-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    move v0, v1

    :goto_1
    return v0

    :cond_0
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_hw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/lib/liblifevibes_mediashare_hw_jni.so"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v3, "/system/lib/liblifevibes_mediashare_sw_jni.so"

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    const-string v3, "TrimApp_MS"

    const-string v4, "ms sw loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading VideoEditorImpl JNI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v3, "/system/lib/liblifevibes_mediashare_hw_jni.so"

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    const-string v3, "TrimApp_MS"

    const-string v4, "ms hw loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading VideoEditorImpl JNI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_sw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v3, "/system/lib/liblifevibes_mediashare_xneon_sw_jni.so"

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    const-string v3, "TrimApp_MS"

    const-string v4, "ms xneon sw loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_0

    :catch_2
    move-exception v1

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading VideoEditorImpl JNI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/ExceptionInInitializerError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_3
    :try_start_3
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    const-string v4, "ms_xneon_hw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v3, "/system/lib/liblifevibes_mediashare_xneon_hw_jni.so"

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V

    const-string v3, "TrimApp_MS"

    const-string v4, "ms xneon hw loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading VideoEditorImpl JNI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    :try_start_4
    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/lifevibes/trimapp/util/MediaShare;->SUPPORT_H263_QVGA:Z

    const-string v1, "TrimApp_MS"

    const-string v2, "No library is founded"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v1

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loading VideoEditorImpl JNI\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public loadLibrary(Ljava/lang/String;)Z
    .locals 1

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mProjectPath:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->loadLibrary()Z

    move-result v0

    return v0
.end method

.method public onThumbnail(Landroid/graphics/Bitmap;II)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTNListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTNListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2, p1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;->onThumbnailProgress(IILandroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public releaseThumbnail()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v0}, Lcom/lifevibes/videoeditor/VideoEditor;->release()V

    iput-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mMediaVideoItem:Lcom/lifevibes/videoeditor/MediaVideoItem;

    iput-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public releaseTrimming()Z
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->cancel()V

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v0}, Lcom/lifevibes/videoeditor/VideoEditor;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I
    .locals 10

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "**** Trimming start : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " end : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "TrimApp_MS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rotation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->loadLibrary()Z

    move-result v2

    if-nez v2, :cond_0

    const v2, -0x7ffffffc

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    if-nez v2, :cond_1

    const v2, -0x7ffffffc

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    const/4 v2, 0x1

    :try_start_0
    new-instance v3, Lcom/lifevibes/videoeditor/MediaVideoItem;

    iget-object v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    const-string v6, "ms3"

    const/4 v7, 0x0

    invoke-direct {v3, v5, v6, p1, v7}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    const/4 v2, 0x0

    move-object v4, v3

    :goto_1
    if-eqz v2, :cond_2

    const v2, -0x7ffffffc

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    float-to-double v6, p3

    const-wide/16 v8, 0x0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_3

    const/16 v3, 0x64

    :goto_2
    :try_start_1
    invoke-virtual {v4, v3}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setDisplayAngle(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_7

    const v2, -0x7ffffffc

    goto :goto_0

    :cond_3
    float-to-double v6, p3

    const-wide v8, 0x4056800000000000L    # 90.0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_4

    const/16 v3, 0x65

    goto :goto_2

    :cond_4
    float-to-double v6, p3

    const-wide v8, 0x4066800000000000L    # 180.0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_5

    const/16 v3, 0x66

    goto :goto_2

    :cond_5
    float-to-double v6, p3

    const-wide v8, 0x4070e00000000000L    # 270.0

    cmpl-double v3, v6, v8

    if-nez v3, :cond_6

    const/16 v3, 0x67

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    :catch_0
    move-exception v3

    const-string v3, "TrimApp_MS"

    const-string v5, "set DiaplayAngle error!!"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_7
    const/4 v2, 0x1

    move/from16 v0, p6

    int-to-long v6, v0

    move/from16 v0, p7

    int-to-long v8, v0

    :try_start_2
    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setExtractBoundaries(JJ)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    const/4 v2, 0x0

    :goto_4
    if-eqz v2, :cond_8

    const v2, -0x7ffffffc

    goto :goto_0

    :cond_8
    const/4 v2, 0x1

    :try_start_3
    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;

    invoke-interface {v3, v4}, Lcom/lifevibes/videoeditor/VideoEditor;->addMediaItem(Lcom/lifevibes/videoeditor/MediaItem;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2

    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_9

    const v2, -0x7ffffffc

    goto :goto_0

    :cond_9
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-lez v2, :cond_11

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getWidth()I

    move-result v2

    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getHeight()I

    move-result v3

    const/16 v5, 0x438

    if-ne v2, v5, :cond_a

    const/16 v5, 0x2d0

    if-eq v3, v5, :cond_b

    :cond_a
    const/16 v5, 0x2d0

    if-ne v2, v5, :cond_12

    const/16 v5, 0x1e0

    if-ne v3, v5, :cond_12

    :cond_b
    const/4 v5, 0x1

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    const/16 v5, 0x1e0

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    :goto_6
    invoke-virtual {v4}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoBitrate()I

    move-result v4

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const/16 v5, 0x6d60

    if-gt v4, v5, :cond_24

    const/16 v4, 0x6d60

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    :goto_7
    const/16 v4, 0x80

    if-ne v2, v4, :cond_c

    const/16 v4, 0x60

    if-eq v3, v4, :cond_f

    :cond_c
    const/16 v4, 0xa0

    if-ne v2, v4, :cond_d

    const/16 v4, 0x78

    if-eq v3, v4, :cond_f

    :cond_d
    const/16 v4, 0xb0

    if-ne v2, v4, :cond_e

    const/16 v4, 0x90

    if-eq v3, v4, :cond_f

    :cond_e
    const/16 v4, 0x140

    if-ne v2, v4, :cond_10

    const/16 v2, 0xf0

    if-ne v3, v2, :cond_10

    :cond_f
    iget v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v3, 0x7d000

    if-le v2, v3, :cond_10

    const v2, 0x7d000

    iput v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    :cond_10
    const/4 v2, 0x4

    iput v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitVideoCodec:I

    const/4 v2, 0x2

    iput v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitAudioCodec:I

    :cond_11
    iput-object p2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    iput-wide p4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitMillis:J

    new-instance v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;-><init>(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)V

    iput-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_12
    const/16 v5, 0xf00

    if-ne v2, v5, :cond_13

    const/16 v5, 0x870

    if-eq v3, v5, :cond_17

    :cond_13
    const/16 v5, 0x780

    if-ne v2, v5, :cond_14

    const/16 v5, 0x438

    if-eq v3, v5, :cond_17

    :cond_14
    const/16 v5, 0x500

    if-ne v2, v5, :cond_15

    const/16 v5, 0x2d0

    if-eq v3, v5, :cond_17

    :cond_15
    const/16 v5, 0x356

    if-ne v2, v5, :cond_16

    const/16 v5, 0x1e0

    if-eq v3, v5, :cond_17

    :cond_16
    const/16 v5, 0x280

    if-ne v2, v5, :cond_18

    const/16 v5, 0x168

    if-ne v3, v5, :cond_18

    :cond_17
    const/4 v5, 0x2

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    const/16 v5, 0x168

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_18
    const/16 v5, 0x3c0

    if-ne v2, v5, :cond_19

    const/16 v5, 0x2d0

    if-eq v3, v5, :cond_1d

    :cond_19
    const/16 v5, 0x280

    if-ne v2, v5, :cond_1a

    const/16 v5, 0x1e0

    if-eq v3, v5, :cond_1d

    :cond_1a
    const/16 v5, 0x140

    if-ne v2, v5, :cond_1b

    const/16 v5, 0xf0

    if-eq v3, v5, :cond_1d

    :cond_1b
    const/16 v5, 0xa0

    if-ne v2, v5, :cond_1c

    const/16 v5, 0x78

    if-eq v3, v5, :cond_1d

    :cond_1c
    const/16 v5, 0x80

    if-ne v2, v5, :cond_1f

    const/16 v5, 0x60

    if-ne v3, v5, :cond_1f

    :cond_1d
    const/4 v5, 0x3

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    const/16 v5, 0x1e0

    if-le v3, v5, :cond_1e

    const/16 v5, 0x1e0

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_1e
    iput v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_1f
    const/16 v5, 0x320

    if-ne v2, v5, :cond_20

    const/16 v5, 0x1e0

    if-ne v3, v5, :cond_20

    const/4 v5, 0x4

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    iput v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_20
    const/16 v5, 0x160

    if-ne v2, v5, :cond_21

    const/16 v5, 0x120

    if-eq v3, v5, :cond_22

    :cond_21
    const/16 v5, 0xb0

    if-ne v2, v5, :cond_23

    const/16 v5, 0x90

    if-ne v3, v5, :cond_23

    :cond_22
    const/4 v5, 0x5

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    iput v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_23
    const/4 v5, 0x0

    iput v5, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I

    iput v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I

    goto/16 :goto_6

    :cond_24
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x9c40

    if-gt v4, v5, :cond_25

    const v4, 0x9c40

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_25
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0xfa00

    if-gt v4, v5, :cond_26

    const v4, 0xfa00

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_26
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x17700

    if-gt v4, v5, :cond_27

    const v4, 0x17700

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_27
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x1f400

    if-gt v4, v5, :cond_28

    const v4, 0x1f400

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_28
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x2ee00

    if-gt v4, v5, :cond_29

    const v4, 0x2ee00

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_29
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x3e800

    if-gt v4, v5, :cond_2a

    const v4, 0x3e800

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_2a
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x5dc00

    if-gt v4, v5, :cond_2b

    const v4, 0x5dc00

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_2b
    iget v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    const v5, 0x7d000

    if-gt v4, v5, :cond_2c

    const v4, 0x7d000

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :cond_2c
    const v4, 0xc3500

    iput v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I

    goto/16 :goto_7

    :catch_1
    move-exception v3

    goto/16 :goto_4

    :catch_2
    move-exception v3

    goto/16 :goto_5

    :catch_3
    move-exception v3

    goto/16 :goto_5

    :catch_4
    move-exception v3

    goto/16 :goto_1

    :catch_5
    move-exception v3

    goto/16 :goto_1
.end method
