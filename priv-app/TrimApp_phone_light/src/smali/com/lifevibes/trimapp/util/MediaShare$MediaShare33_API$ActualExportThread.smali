.class Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
.super Landroid/os/AsyncTask;

# interfaces
.implements Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActualExportThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;"
    }
.end annotation


# instance fields
.field private mCanceled:Z

.field private mCompleted:Z

.field private mExportStarted:Z

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;

.field final synthetic this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)V
    .locals 3

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iget-object v0, p1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->this$0:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v0, v0, Lcom/lifevibes/trimapp/util/MediaShare;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x6

    const-string v2, "TrimApp_MS"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mCanceled:Z

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mExportStarted:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$100(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/lifevibes/videoeditor/VideoEditor;->cancelExport(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    invoke-interface {v0}, Lcom/lifevibes/videoeditor/VideoEditor;->release()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$402(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/videoeditor/VideoEditor;)Lcom/lifevibes/videoeditor/VideoEditor;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 12

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$100(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v9

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    const/4 v1, 0x2

    iput v1, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I

    :try_start_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitMillis:J
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$200(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitRatio:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$300(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/lifevibes/videoeditor/VideoEditor;->setAspectRatio(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$100(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitHeight:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$500(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I

    move-result v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitBitrate:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$600(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitMillis:J
    invoke-static {v4}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$200(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitAudioCodec:I
    invoke-static {v6}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$700(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I

    move-result v6

    iget-object v7, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mLimitVideoCodec:I
    invoke-static {v7}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$800(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)I

    move-result v7

    move-object v8, p0

    invoke-interface/range {v0 .. v8}, Lcom/lifevibes/videoeditor/VideoEditor;->export(Ljava/lang/String;IIJIILcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    const/4 v1, 0x3

    iput v1, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v11

    :goto_2
    if-eqz v0, :cond_6

    const/4 v0, -0x1

    :try_start_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v1, v9}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    goto :goto_0

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mVideoEditorImpl:Lcom/lifevibes/videoeditor/VideoEditor;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$400(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/videoeditor/VideoEditor;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mOutput:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$100(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/lifevibes/videoeditor/VideoEditor;->export(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    move v0, v10

    goto :goto_2

    :catch_1
    move-exception v0

    move v1, v10

    :goto_3
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    :try_start_5
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v0

    :goto_4
    const v2, 0x41100001    # 9.000001f

    if-eq v0, v2, :cond_3

    const v2, -0x7e71fff9

    if-ne v0, v2, :cond_4

    :cond_3
    const/16 v0, 0x88

    :try_start_6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v1, v9}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    goto/16 :goto_0

    :catch_2
    move-exception v0

    move v0, v11

    goto :goto_4

    :cond_4
    const v2, -0x7ffffff6

    if-ne v0, v2, :cond_5

    const v0, -0x7ffffffe

    :try_start_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v1, v9}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v0, v9}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mCompleted:Z

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mCanceled:Z

    if-nez v1, :cond_8

    :goto_5
    and-int/2addr v0, v10

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    move-result-object v0

    invoke-interface {v0, v11}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;->onTrimCompletion(I)V

    :cond_7
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v1, v9}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    throw v0

    :cond_8
    move v10, v11

    goto :goto_5

    :catch_3
    move-exception v0

    move v0, v10

    goto/16 :goto_2

    :catch_4
    move-exception v0

    move v0, v11

    goto/16 :goto_2

    :catch_5
    move-exception v0

    move v1, v11

    goto/16 :goto_3

    :catch_6
    move-exception v0

    move v0, v10

    goto/16 :goto_2

    :catch_7
    move-exception v0

    move v0, v11

    goto/16 :goto_2

    :catch_8
    move-exception v0

    move v0, v11

    goto/16 :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    iput v2, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimmingStatus:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExportThread:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$902(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;->onTrimError(II)V

    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method public onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mExportStarted:Z

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mCanceled:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread$1;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread$1;-><init>(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mExpertCancelHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$1000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->this$1:Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    # getter for: Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->mTrimListener:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->access$000(Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, p3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;->onTrimProgress(II)V

    const/16 v0, 0x64

    if-lt p3, v0, :cond_0

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API$ActualExportThread;->mCompleted:Z

    goto :goto_0
.end method
