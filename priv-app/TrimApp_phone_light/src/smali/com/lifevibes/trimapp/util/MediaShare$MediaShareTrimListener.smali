.class public interface abstract Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/util/MediaShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaShareTrimListener"
.end annotation


# virtual methods
.method public abstract onTrimCompletion(I)V
.end method

.method public abstract onTrimError(II)V
.end method

.method public abstract onTrimProgress(II)V
.end method

.method public abstract onTrimWarning(II)V
.end method
