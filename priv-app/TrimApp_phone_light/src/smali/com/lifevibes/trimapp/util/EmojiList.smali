.class public Lcom/lifevibes/trimapp/util/EmojiList;
.super Ljava/lang/Object;


# static fields
.field private static unicodeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x263a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f602"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f603"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f604"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f605"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f606"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f607"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f608"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f609"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f620"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f621"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f622"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f634"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f623"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f624"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f625"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f626"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f627"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f628"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f629"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f630"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f631"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f632"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f633"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f635"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f636"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f637"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f612"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f617"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f619"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f618"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f616"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f613"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f645"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f646"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f647"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f638"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f639"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f640"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f476"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f466"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f467"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f468"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f469"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f474"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f475"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f491"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f464"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f465"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f477"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f481"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f482"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f470"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f478"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f385"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f471"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f472"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f473"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f483"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f486"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f487"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f485"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f479"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f480"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f440"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f442"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f443"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f463"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f444"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f445"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2764"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f499"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f493"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f494"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f495"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f496"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f497"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f498"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x261d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f446"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f447"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f448"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f449"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f450"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f530"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f484"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f451"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f452"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f393"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f453"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f454"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f455"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f456"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f457"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f458"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f459"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f460"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f461"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f462"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f392"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f489"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f514"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f515"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f526"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4da"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4db"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f383"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f384"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f380"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f381"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f382"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f388"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f386"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f387"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f389"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4df"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x260e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4de"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2709"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ee"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ef"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2702"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f0cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x303d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f415"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f436"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f429"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f408"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f431"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f400"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f401"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f439"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f422"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f407"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f430"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f413"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f414"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f423"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f424"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f425"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f426"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f410"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f403"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f402"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f404"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f434"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f417"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f416"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f437"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f438"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f427"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f418"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f428"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f412"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f435"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f406"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f433"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f420"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f421"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f419"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f432"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f409"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f378"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f377"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f379"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f376"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f375"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f374"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f368"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f367"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f366"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f369"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f370"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f373"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f354"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f355"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f356"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f357"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f364"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f363"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f371"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f359"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f372"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f365"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f362"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f361"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f358"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f360"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f344"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f345"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f346"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f347"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f348"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f349"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f350"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f351"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f352"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f353"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f330"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f331"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f332"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f333"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f334"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f335"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f337"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f338"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f339"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f340"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f341"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f342"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f343"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f308"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f301"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f302"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f300"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2744"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f319"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f311"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f312"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f313"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f314"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f315"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f316"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f317"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f318"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f391"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f304"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f305"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f307"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f306"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f303"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f309"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f310"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5ff"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f488"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f527"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f528"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f529"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f682"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f683"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f684"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f685"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f686"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f687"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x24c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f688"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f690"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f691"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f692"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f694"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f695"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f696"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f697"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f698"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f699"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f681"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2708"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f689"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f680"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2668"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f490"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f492"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f519"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2650"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2651"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2652"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2653"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f531"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f170"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f171"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f18e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f520"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f521"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f522"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f523"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f524"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f201"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f192"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f193"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f19a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f232"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f233"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f234"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f235"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f236"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f237"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f238"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f239"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f202"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f23a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f250"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f251"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3299"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f21a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f22f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3297"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b55"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2139"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2705"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2714"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f517"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2734"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2733"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2795"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2796"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2716"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2797"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f525"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f567"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f550"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f551"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f552"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f553"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f554"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f560"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f555"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f561"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f556"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f562"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f557"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f563"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f558"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f564"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f559"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f565"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f566"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b06"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b07"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b05"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2934"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2935"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2747"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2728"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f534"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f535"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f533"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f532"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b50"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fe"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f538"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f539"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f536"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f537"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2754"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2753"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2755"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2757"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2049"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2660"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2663"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2666"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f511"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f191"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f512"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f513"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f510"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f518"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f516"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f503"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f500"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f501"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f502"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f504"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f505"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f506"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f507"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f508"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f509"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPattern()Ljava/util/regex/Pattern;
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lifevibes/trimapp/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v0, 0x28

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/lifevibes/trimapp/util/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x7c

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const-string v3, ")"

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    return-object v0
.end method

.method public static getUnicodeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/lifevibes/trimapp/util/EmojiList;->unicodeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static hasEmojiString(Ljava/lang/CharSequence;)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/lifevibes/trimapp/util/EmojiList;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    const/high16 v1, 0x1f0000

    and-int/2addr v1, v0

    shr-int/lit8 v1, v1, 0x10

    add-int/lit8 v1, v1, -0x1

    const v2, 0xdc00

    and-int/lit16 v3, v0, 0x3ff

    or-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0xd800

    const v4, 0xfc00

    and-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0xa

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x6

    or-int/2addr v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-char v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
