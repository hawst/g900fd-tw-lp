.class public Lcom/lifevibes/trimapp/util/MediaShare;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;,
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;,
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;,
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;,
        Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;
    }
.end annotation


# static fields
.field public static final ERROR_LOW_MEMORY:I = 0x88

.field public static final ID_MEDIASHARE_33:I = 0x21

.field public static final LOG_VIEW:Z = false

.field public static final MAX_FILESIZE_SUPPORTED:J = 0x7fffffffL

.field public static final MS_HW:Ljava/lang/String; = "ms_hw"

.field public static final MS_SW:Ljava/lang/String; = "ms_sw"

.field public static final MS_XNEON_HW:Ljava/lang/String; = "ms_xneon_hw"

.field public static final MS_XNEON_SW:Ljava/lang/String; = "ms_xneon_sw"

.field public static final RESULT_OK:I = 0x0

.field public static final RESULT_PROPERTIES_ERROR:I = 0x40000000

.field public static final RESULT_TRIMMING_ERROR:I = -0x80000000

.field public static final STATUS_TRIMMING_DONE:I = 0x3

.field public static final STATUS_TRIMMING_NOT_STARTED:I = 0x1

.field public static final STATUS_TRIMMING_RUNNING:I = 0x2

.field public static final STATUS_TRIMMING_UNKNOWN:I = 0x0

.field public static final TAG:Ljava/lang/String; = "TrimApp_MS"

.field public static final UNEXPECTED_ARGUMENT:I = -0x7ffffffc

.field public static final UNSUPPORTED_AUDIOCODEC:I = 0x4000000c

.field public static final UNSUPPORTED_FILESIZE:I = 0x40000006

.field public static final UNSUPPORTED_INPUT:I = -0x7ffffffe

.field public static final UNSUPPORTED_PROFILE_LEVEL:I = 0x40000008

.field public static final UNSUPPORTED_RESOLUTION:I = 0x4000000a


# instance fields
.field public SUPPORT_H263_QVGA:Z

.field private final mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

.field public final mContext:Landroid/content/Context;

.field public mLibraryInformation:Ljava/lang/String;

.field private mLoaded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mLoaded:Z

    iput-object p1, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mLibraryInformation:Ljava/lang/String;

    new-instance v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;-><init>(Lcom/lifevibes/trimapp/util/MediaShare;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    return-void
.end method


# virtual methods
.method public declared-synchronized cancelThumbnail()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->cancelThumbnail()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createThumbnail(Ljava/lang/String;II)Z
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0, p1, p2, p3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->createThumbnail(Ljava/lang/String;II)Z

    move-result v0

    goto :goto_0
.end method

.method public getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v0, 0x40000000    # 2.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0, p1, p2}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    goto :goto_0
.end method

.method public getThumbnail(III)Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0, p1, p2, p3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->getThumbnail(III)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getThumbnailListCB(IIJJIZLcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;)V
    .locals 11

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-interface/range {v1 .. v10}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->getThumbnailListCB(IIJJIZLcom/lifevibes/trimapp/util/MediaShare$MediaShareThumbnailListener;)V

    goto :goto_0
.end method

.method public getTrimmingStatus()I
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->getTrimmingStatus()I

    move-result v0

    goto :goto_0
.end method

.method public isNullEncordingAvailable(III)Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0, p1, p2, p3}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->isNullEncordingAvailable(III)Z

    move-result v0

    return v0
.end method

.method public isThumbnailSpeedPreferedOverAccuracy(IJJ)Z
    .locals 6

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->isThumbnailSpeedPreferedOverAccuracy(IJJ)Z

    move-result v0

    goto :goto_0
.end method

.method public loadLibrary(Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mLoaded:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    check-cast v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;

    invoke-virtual {v0, p1}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShare33_API;->loadLibrary(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "TrimApp_MS"

    const-string v2, "------------ loaded MS4.0"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iput-boolean v1, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mLoaded:Z

    move v0, v1

    goto :goto_0
.end method

.method public declared-synchronized releaseThumbnail()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->releaseThumbnail()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public releaseTrimming()Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->releaseTrimming()Z

    move-result v0

    return v0
.end method

.method public startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I
    .locals 10

    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    if-nez v0, :cond_0

    const-string v0, "TrimApp_MS"

    const-string v1, "library was not loaded!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v0, 0x40000000    # 2.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/util/MediaShare;->mApi:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareAPI;->startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I

    move-result v0

    goto :goto_0
.end method
