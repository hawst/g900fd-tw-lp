.class public Lcom/lifevibes/trimapp/Trim$TrimmingListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrimmingListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTrimCompletion(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mTrimmingDone:Z
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim;->access$402(Lcom/lifevibes/trimapp/Trim;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim;->access$2002(Lcom/lifevibes/trimapp/Trim;Z)Z

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$2100(Lcom/lifevibes/trimapp/Trim;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mOverwriteFilename:Z
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$2200(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/util/Support;->renameFile(Ljava/io/File;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # += operator for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim;->access$212(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v1

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-static {v1, v0}, Lcom/lifevibes/trimapp/util/Support;->registerFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v2, v1}, Lcom/lifevibes/trimapp/Trim;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.START_VIDEO_FROM_TRIM"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->releaseTrimming()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2300(Lcom/lifevibes/trimapp/Trim;)V

    :goto_1
    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z
    invoke-static {v0, v5}, Lcom/lifevibes/trimapp/Trim;->access$2002(Lcom/lifevibes/trimapp/Trim;Z)Z

    const/4 v0, -0x1

    invoke-virtual {p0, v5, v0}, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->onTrimError(II)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # += operator for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v1, v4}, Lcom/lifevibes/trimapp/Trim;->access$212(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$200(Lcom/lifevibes/trimapp/Trim;)I

    move-result v2

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-static {v1, v0}, Lcom/lifevibes/trimapp/util/Support;->registerFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    goto :goto_0
.end method

.method public onTrimError(II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim;->access$2400(Lcom/lifevibes/trimapp/Trim;I)V

    const/16 v0, 0x88

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f070017

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    goto :goto_0

    :cond_1
    const v0, -0x7ffffffe

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f070013

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$300(Lcom/lifevibes/trimapp/Trim;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    goto :goto_0
.end method

.method public onTrimProgress(II)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onTrimWarning(II)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1900(Lcom/lifevibes/trimapp/Trim;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const v1, 0x7f07001a

    # invokes: Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$2400(Lcom/lifevibes/trimapp/Trim;I)V

    goto :goto_0
.end method
