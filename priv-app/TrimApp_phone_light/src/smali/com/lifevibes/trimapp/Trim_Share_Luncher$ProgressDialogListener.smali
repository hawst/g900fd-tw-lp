.class Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share_Luncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;Lcom/lifevibes/trimapp/Trim_Share_Luncher$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$1200(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    const v1, 0x7f07001c

    # invokes: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->stopTrimming(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$1100(Lcom/lifevibes/trimapp/Trim_Share_Luncher;I)V

    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingSuccess:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$500(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TrimApp_Share_Luncher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trimming_success file path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$900(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$800(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$100(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$600(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share_Luncher;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->access$600(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
