.class public Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TrimmingListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method public constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTrimCompletion(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingDone:Z
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->access$402(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->access$2402(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2500(Lcom/lifevibes/trimapp/Trim_Share;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$1000(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/Storage;

    move-result-object v3

    iget-object v3, v3, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "photoring_temp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v2}, Lcom/lifevibes/trimapp/Trim_Share;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/lifevibes/trimapp/util/Support;->deleteFile(Landroid/content/Context;Ljava/io/File;)V

    :cond_0
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # += operator for: Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I
    invoke-static {v0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->access$212(Lcom/lifevibes/trimapp/Trim_Share;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$100(Lcom/lifevibes/trimapp/Trim_Share;)[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v2

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$100(Lcom/lifevibes/trimapp/Trim_Share;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-static {v1, v0}, Lcom/lifevibes/trimapp/util/Support;->registerFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v2, v1}, Lcom/lifevibes/trimapp/Trim_Share;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.RESULT_FROM_TRIM_SHARE"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->releaseTrimming()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2600(Lcom/lifevibes/trimapp/Trim_Share;)V

    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z
    invoke-static {v0, v5}, Lcom/lifevibes/trimapp/Trim_Share;->access$2402(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    const/4 v0, -0x1

    invoke-virtual {p0, v5, v0}, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->onTrimError(II)V

    goto :goto_0
.end method

.method public onTrimError(II)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$2700(Lcom/lifevibes/trimapp/Trim_Share;I)V

    const/16 v0, 0x88

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f070017

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    goto :goto_0

    :cond_1
    const v0, -0x7ffffffe

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f070013

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$300(Lcom/lifevibes/trimapp/Trim_Share;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f07001d

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    goto :goto_0
.end method

.method public onTrimProgress(II)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onTrimWarning(II)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f07001a

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2700(Lcom/lifevibes/trimapp/Trim_Share;I)V

    goto :goto_0
.end method
