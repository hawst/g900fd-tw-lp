.class public final Lcom/lifevibes/trimapp/R$styleable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final DoubleSeekbarStyleable:[I

.field public static final DoubleSeekbarStyleable_HANDLE_HEIGHT:I = 0xb

.field public static final DoubleSeekbarStyleable_HANDLE_MARGIN_T:I = 0xc

.field public static final DoubleSeekbarStyleable_HANDLE_PADDING:I = 0xd

.field public static final DoubleSeekbarStyleable_HANDLE_PADDING_SPACE:I = 0xe

.field public static final DoubleSeekbarStyleable_HANDLE_USER_HEIGHT:I = 0x10

.field public static final DoubleSeekbarStyleable_HANDLE_USER_MARGIN_T:I = 0x11

.field public static final DoubleSeekbarStyleable_HANDLE_USER_WIDTH:I = 0xf

.field public static final DoubleSeekbarStyleable_HANDLE_WIDTH:I = 0xa

.field public static final DoubleSeekbarStyleable_IMAGE_CONTAINER_HEIGHT:I = 0x6

.field public static final DoubleSeekbarStyleable_IMAGE_CONTAINER_MARGIN_LR:I = 0x7

.field public static final DoubleSeekbarStyleable_IMAGE_CONTAINER_MARGIN_T:I = 0x8

.field public static final DoubleSeekbarStyleable_IMAGE_CONTAINER_PADDING:I = 0x9

.field public static final DoubleSeekbarStyleable_IMAGE_HEIGHT:I = 0x5

.field public static final DoubleSeekbarStyleable_IMAGE_WIDTH:I = 0x4

.field public static final DoubleSeekbarStyleable_LAYOUT_HEIGHT:I = 0x12

.field public static final DoubleSeekbarStyleable_TIME_HEIGHT:I = 0x0

.field public static final DoubleSeekbarStyleable_TIME_MARGIN_LR:I = 0x2

.field public static final DoubleSeekbarStyleable_TIME_MARGIN_T:I = 0x3

.field public static final DoubleSeekbarStyleable_TIME_WIDTH:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/lifevibes/trimapp/R$styleable;->DoubleSeekbarStyleable:[I

    return-void

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
