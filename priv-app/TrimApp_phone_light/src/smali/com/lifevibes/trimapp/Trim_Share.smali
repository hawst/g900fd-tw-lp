.class public Lcom/lifevibes/trimapp/Trim_Share;
.super Landroid/app/Activity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/Trim_Share$FilenameLengthFilter;,
        Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;,
        Lcom/lifevibes/trimapp/Trim_Share$ThumbnailImage;,
        Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;,
        Lcom/lifevibes/trimapp/Trim_Share$VideoViewListener;,
        Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;,
        Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;
    }
.end annotation


# static fields
.field public static CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String; = null

.field public static CURRENT_MODEL_TYPE:I = 0x0

.field static final DECODER_USER_THUMBNAIL:I = 0x1

.field static final DECODER_USER_TRIMMING:I = 0x2

.field static final DECODER_USER_UNKNOWN:I = 0x0

.field static final DECODER_USER_VIDEOPLAYER:I = 0x4

.field static final DIALOG_CHOICE_FILENAME:I = 0x3

.field static final DIALOG_FILENAME:I = 0x1

.field static final DIALOG_PROGRESS:I = 0x2

.field static final DIALOG_UNSUPPORTED_FILESIZE:I = 0x4

.field static final DIALOG_UNSUPPORTED_TYPE:I = 0x5

.field public static final GUI_BASE_SMARTPHONE:I = -0x80000000

.field public static final GUI_BASE_TABLET:I = 0x40000000

.field public static final LOG_VIEW:Z = false

.field public static final MAXIMUM_FILE_COUNT:I = 0x64

.field static final MAX_FILENAME_LENGTH:I = 0x32

.field static final MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

.field public static final MINIMUM_DURATION:I = 0x3e8

.field public static final MODEL_TYPE_J:I = -0x7fffffff

.field public static final MODEL_TYPE_LT03:I = 0x40000002

.field static final MSG_ACTION_PLAYVIDEO:I = 0x7d2

.field static final MSG_ACTION_THUMBNAIL:I = 0x7d0

.field static final MSG_ACTION_TRIMMING:I = 0x7d1

.field static final MSG_ARG_PAUSING_VIDEO:I = 0x65

.field static final MSG_ARG_PLAYING_VIDEO:I = 0x64

.field static final MSG_ARG_STOPPING_VIDEO:I = 0x66

.field static final MSG_DIALOG_PROGRESS:I = 0x64

.field static final MSG_REFRESH_DISPLAY:I = 0x65

.field static final MSG_RESTORE_USER_HANDLE_POS:I = 0x69

.field static final MSG_SHOW_KEYPAD:I = 0x6a

.field static final MSG_UPDATE_PLAYPAUSE:I = 0x68

.field static final MSG_UPDATE_VIDEO_PROGRESS:I = 0x67

.field public static final OUTPUT_EXT:Ljava/lang/String; = ".mp4"

.field private static final OUTPUT_FILENAME:Ljava/lang/String; = "photoring_temp"

.field public static final SUPPORT_SCARED_THUMBNAIL:Z = true

.field public static SUPPORT_VIDEO_ROTATION:Z = false

.field public static final TAG:Ljava/lang/String; = "TrimApp"

.field public static final TAG_CYCLE:Ljava/lang/String; = "TrimApp_Cycle"

.field static final THUMBNAIL_MASK_ALL:I = 0xf

.field static final THUMBNAIL_MASK_MAIN:I = 0x2

.field static final THUMBNAIL_MASK_TN:I = 0x4

.field static final THUMBNAIL_MASK_TN_ZOOM:I = 0x8

.field static final THUMBNAIL_MASK_UNKNOWN:I = 0x0

.field public static VIDEOVIEW_HEIGHT:I = 0x0

.field static final VIDEOVIEW_STATE_INTURRUPTED:I = 0x2

.field static final VIDEOVIEW_STATE_LONG_PRESS:I = 0x3

.field static final VIDEOVIEW_STATE_LONG_RELEASE:I = 0x4

.field static final VIDEOVIEW_STATE_NONE:I = 0x0

.field static final VIDEOVIEW_STATE_STARTED:I = 0x1

.field public static VIDEOVIEW_WIDTH:I

.field private static mActionWaitWatcherPlay:I

.field private static mActionWaitWatcherTrim:I

.field static final mInvalidFileNameChar:[Ljava/lang/String;


# instance fields
.field private final mActionHandler:Landroid/os/Handler;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mAudioReceiver:Landroid/content/BroadcastReceiver;

.field private mControlLayout:Landroid/widget/RelativeLayout;

.field private mCurrentDecoderUser:I

.field private mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

.field private mDragging:Z

.field private mEditTextName:Landroid/widget/EditText;

.field private mFileCount:I

.field private mFilenameDialog:Landroid/app/AlertDialog;

.field private final mHandler:Landroid/os/Handler;

.field private final mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

.field private mIsActivityFinishing:Z

.field private mIsConfigChanged:Z

.field private mIsMultiWindowError:Z

.field private mIvFullImage:[Landroid/widget/ImageView;

.field private mIvThumbnails:[Landroid/widget/ImageView;

.field private mIvThumbnailsZoom:[Landroid/widget/ImageView;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

.field private mMenuItemDone:Landroid/view/MenuItem;

.field private mNewIntent:Z

.field private mNumberOfImages:I

.field private mObserver:Landroid/os/FileObserver;

.field private mOnTrimmingFile:Ljava/lang/String;

.field private mOverwriteFilename:Z

.field private mPauseButton:Landroid/view/View;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPlayButton:Landroid/view/View;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreviewState:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field public final mProgressListener:Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;

.field private mProgressPercentFormat:Ljava/text/NumberFormat;

.field private mRefeshdisplay:Z

.field private mRestart:Z

.field private mSaveHandlePosition:I

.field public mSaveLeftBarTime:I

.field public mSaveRightBarTime:I

.field public mSaveUserBarTime:I

.field private mStorage:Lcom/lifevibes/trimapp/util/Storage;

.field private mThumbnailMask:I

.field private mThumbnailOffset:I

.field private mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

.field private mThumbnailTaskUid:J

.field private mThumbnail_mask_in_count:I

.field private mThumbnail_released:Z

.field private mTotalDuration:I

.field private mTrimButton:Landroid/view/View;

.field private final mTrimmedFiles:[Ljava/lang/String;

.field private mTrimmingDone:Z

.field public final mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;

.field private mTrimmingSuccess:Z

.field private mUnsupportedShowing:Z

.field private final mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

.field private mVideoOnprepared:Z

.field private mVideoRotation:I

.field private mVideoView:Landroid/widget/VideoView;

.field private mWindowDisplay:Landroid/view/Display;

.field private titleActionBar:Landroid/app/ActionBar;

.field private toastMsg:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v0, -0x7fffffff

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    sput-boolean v4, Lcom/lifevibes/trimapp/Trim_Share;->SUPPORT_VIDEO_ROTATION:Z

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v3

    const-string v1, "/"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share;->mInvalidFileNameChar:[Ljava/lang/String;

    sput v3, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherPlay:I

    sput v3, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x64

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    new-instance v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$1;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mUnsupportedShowing:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRefeshdisplay:Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoOnprepared:Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$1;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$1;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$2;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$2;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$3;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$3;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$4;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$4;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$15;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$15;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$16;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$16;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/Trim_Share;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/Storage;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    return v0
.end method

.method static synthetic access$1300(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    return v0
.end method

.method static synthetic access$1402(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    return p1
.end method

.method static synthetic access$1500(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    return v0
.end method

.method static synthetic access$1772(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    return v0
.end method

.method static synthetic access$1800(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I

    return v0
.end method

.method static synthetic access$1802(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I

    return p1
.end method

.method static synthetic access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    return-object v0
.end method

.method static synthetic access$200(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    return v0
.end method

.method static synthetic access$2000(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$212(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    return v0
.end method

.method static synthetic access$2200(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setVideoProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/lifevibes/trimapp/Trim_Share;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseTrimming()V

    return-void
.end method

.method static synthetic access$2700(Lcom/lifevibes/trimapp/Trim_Share;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V

    return-void
.end method

.method static synthetic access$2800(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseVideoPlayer()V

    return-void
.end method

.method static synthetic access$2900(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveHandlePosition:I

    return v0
.end method

.method static synthetic access$2902(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveHandlePosition:I

    return p1
.end method

.method static synthetic access$300(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    return-void
.end method

.method static synthetic access$3002(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoOnprepared:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsConfigChanged:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/lifevibes/trimapp/Trim_Share;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/lifevibes/trimapp/Trim_Share;->doTrimmingAction(II)V

    return-void
.end method

.method static synthetic access$3500(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3572(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3576(Lcom/lifevibes/trimapp/Trim_Share;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3600(Lcom/lifevibes/trimapp/Trim_Share;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->resolveUpdatePlayPauseMessage(I)V

    return-void
.end method

.method static synthetic access$3700(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    return v0
.end method

.method static synthetic access$3900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingDone:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/app/ActionBar;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$402(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingDone:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/lifevibes/trimapp/Trim_Share;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V

    return-void
.end method

.method static synthetic access$4300(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/lifevibes/trimapp/Trim_Share;)J
    .locals 2

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J

    return-wide v0
.end method

.method static synthetic access$4402(Lcom/lifevibes/trimapp/Trim_Share;J)J
    .locals 1

    iput-wide p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J

    return-wide p1
.end method

.method static synthetic access$4500(Lcom/lifevibes/trimapp/Trim_Share;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    return v0
.end method

.method static synthetic access$4600(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    return-object v0
.end method

.method static synthetic access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/lifevibes/trimapp/Trim_Share;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsMultiWindowError:Z

    return v0
.end method

.method static synthetic access$602(Lcom/lifevibes/trimapp/Trim_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsMultiWindowError:Z

    return p1
.end method

.method static synthetic access$700(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$800(Lcom/lifevibes/trimapp/Trim_Share;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->doChoiceFilenameAction(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    return-object v0
.end method

.method private clearThumbnailImage(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    and-int/lit8 v0, p1, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    and-int/lit8 v0, p1, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    :goto_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private computeNewFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "_data LIKE \'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, v1, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_%.mp4\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/lifevibes/trimapp/Trim_Share;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_data ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3e8

    new-array v2, v1, [I

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v5, v5, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/lifevibes/trimapp/Trim_Share;->getFileTitleNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    aput v3, v2, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    array-length v3, v2

    move v1, v6

    :goto_1
    if-ge v1, v3, :cond_3

    aget v0, v2, v1

    if-eq v9, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "%03d"

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v8, v8, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".mp4"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    move-object v4, v0

    :cond_3
    return-object v4

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private doChoiceFilenameAction(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->isLowMemory(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v1, v1, v2

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mOverwriteFilename:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trimmed_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->isLowMemory(Z)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "trimmed_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, v1, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim_Share;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doTrimmingAction(II)V
    .locals 10

    const/16 v2, 0x7d1

    const v9, -0x7ffffffc

    const/4 v3, 0x0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_0
    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    const v0, 0x7f070018

    invoke-virtual {p0, v0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v2, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p2, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    sput v3, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v1, v0, v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    iput-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    int-to-float v3, v3

    const-wide/32 v4, 0x1e6666

    iget-object v8, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share$TrimmingListener;

    move v6, p1

    move v7, p2

    invoke-virtual/range {v0 .. v8}, Lcom/lifevibes/trimapp/util/MediaShare;->startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I

    move-result v0

    if-eqz v0, :cond_1

    if-ne v0, v9, :cond_5

    const v0, 0x7f070010

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V

    goto :goto_0

    :cond_5
    if-ne v0, v9, :cond_6

    const v0, 0x7f070013

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V

    goto :goto_0

    :cond_6
    const v0, 0x7f07001d

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V

    goto :goto_0
.end method

.method private enableStatusBarOpenByNotification()V
    .locals 5

    const-string v0, "TrimApp"

    const-string v1, "start enableStatusBarByNotification"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const-string v1, "android.view.WindowManager$LayoutParams"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "SAMSUNG_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const-string v3, "samsungFlags"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    or-int/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Class.getDeclaredField() NoSuchFieldException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Field.getInt/setInt IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Field.getInt/setInt IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Class.forName() ClassNotFoundException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getEditTextFilter()[Landroid/text/InputFilter;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share$FilenameLengthFilter;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim_Share$FilenameLengthFilter;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    aput-object v2, v0, v1

    const/4 v1, 0x0

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share$17;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim_Share$17;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static getFileTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static getFileTitleNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    const-string v1, ".mp4"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLowMemory(Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v3, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v4

    sub-int v3, v4, v3

    int-to-float v3, v3

    int-to-float v0, v0

    div-float v0, v3, v0

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-float v4, v4

    mul-float/2addr v0, v4

    float-to-long v4, v0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, v0, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v4, v5, v0}, Lcom/lifevibes/trimapp/util/Storage;->isLowMemory(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f070017

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private isVideoPlaying()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private prepareTrimming()V
    .locals 9

    const/4 v8, 0x5

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const-string v2, "-trim-%02d-%02d-%02d-%02d-%02d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    const/16 v5, 0xc

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0xd

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const-string v4, ".mp4"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    rsub-int/lit8 v4, v4, 0x32

    add-int/2addr v2, v3

    if-le v2, v4, :cond_0

    sub-int v2, v4, v3

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/Trim_Share;->showDialog(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method private regPhoneStateListener()V
    .locals 3

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$5;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$5;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method private releaseDecoderInuse(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseThumbnailTask()V

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    if-eq v0, p1, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseTrimming()V

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    if-eq v0, p1, :cond_2

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseVideoPlayer()V

    :cond_2
    return-void
.end method

.method private releaseDecoderInuseAndWait()V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuse(I)V

    :goto_0
    const/16 v1, 0x32

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private releaseThumbnailTask()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTaskUid:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mGetThumbnailsDone:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->cancelThumbnail()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->cancel(Z)Z

    :cond_1
    return-void
.end method

.method private releaseTrimming()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->releaseTrimming()Z

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private releaseVideoPlayer()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->isVideoPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v3}, Landroid/widget/VideoView;->setTag(Ljava/lang/Object;)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoOnprepared:Z

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->resolveUpdatePlayPauseMessage(I)V

    goto :goto_0
.end method

.method private resolveUpdatePlayPauseMessage(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->setVideoViewVisibility(Z)V

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->updatePlayPauseButton(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->updateButtonsEnabled()V

    return-void

    :cond_0
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->updatePlayPauseButton(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->setVideoViewVisibility(Z)V

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->updatePlayPauseButton(Z)V

    goto :goto_0
.end method

.method private returnToInvoker()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingDone:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    aget-object v0, v0, v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-boolean v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingDone:Z

    if-eqz v4, :cond_2

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v2, "overwrite"

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/lifevibes/trimapp/Trim_Share;->setResult(ILandroid/content/Intent;)V

    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "intent.action.TRIMAPP_RESULT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v3, "overwrite"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->sendBroadcast(Landroid/content/Intent;)V

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->finish()V

    return-void

    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/lifevibes/trimapp/Trim_Share;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private setMenuItemDoneEnable(Z)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method private setTargetProject(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const v0, -0x7fffffff

    goto :goto_0

    :pswitch_1
    const v0, 0x40000002    # 2.0000005f

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setVideoProgress()I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v3, v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->moveUserHandleTo(IZ)V

    if-lt v0, v2, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->isVideoPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseVideoPlayer()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private setupUIComponents()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/high16 v4, 0x7f060000

    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mControlLayout:Landroid/widget/RelativeLayout;

    const v0, 0x7f060014

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    const v0, 0x7f060015

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_1

    const v0, 0x7f060005

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    const v0, 0x7f060006

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    move-object v1, v0

    :goto_0
    const v0, 0x7f060019

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$1;)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v3, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setOnDoubleSeekBarListener(Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;)V

    const v0, 0x7f060010

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v5}, Landroid/widget/VideoView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v5}, Landroid/widget/VideoView;->setVisibility(I)V

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$VideoViewListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim_Share$VideoViewListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$1;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2, v0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const v2, 0x7f060014

    invoke-virtual {v0, v6, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusDownId(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const v2, 0x7f060006

    invoke-virtual {v0, v7, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusDownId(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    const v0, 0x7f060001

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setNextFocusUpId(I)V

    :goto_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getDisplayedNumberOfImages(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const v1, 0x7f060001

    invoke-virtual {v0, v6, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusDownId(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v7, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusUpId(II)V

    goto :goto_1

    :cond_1
    move-object v1, v2

    goto/16 :goto_0
.end method

.method private setupUIImageViews()V
    .locals 7

    const/4 v6, 0x0

    const v5, 0x7f030005

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    const v0, 0x7f060011

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v1, v0, :cond_0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    aput-object v0, v4, v1

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v4, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v2, v0, :cond_1

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    aput-object v0, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private stopTrimming(I)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseTrimming()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    :cond_0
    return-void
.end method

.method private trimButtonClicked()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v3, v3, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lifevibes/trimapp/util/MediaShare;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f070013

    invoke-virtual {p0, v0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    sub-int v0, v1, v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    const v0, 0x7f070016

    invoke-virtual {p0, v0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    goto :goto_0

    :cond_1
    sput v4, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherTrim:I

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mOverwriteFilename:Z

    invoke-direct {p0, v5}, Lcom/lifevibes/trimapp/Trim_Share;->doChoiceFilenameAction(I)V

    goto :goto_0
.end method

.method private unregPhoneStateListener()V
    .locals 3

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method


# virtual methods
.method public doPauseAction()Z
    .locals 4

    const/16 v3, 0x68

    const/16 v2, 0x67

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->isVideoPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public doPlayPauseAction()V
    .locals 5

    const/16 v4, 0x7d2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    :cond_0
    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherPlay:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    const v0, 0x7f070018

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherPlay:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherPlay:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v4, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f070024

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    goto :goto_0

    :cond_4
    sput v2, Lcom/lifevibes/trimapp/Trim_Share;->mActionWaitWatcherPlay:I

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoOnprepared:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v3, v3, v4

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/VideoView;->setTag(Ljava/lang/Object;)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoOnprepared:Z

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUpdateTimeField(I)V

    :cond_5
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->isVideoPlaying()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->doPauseAction()Z

    goto :goto_0

    :cond_6
    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->start()V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x68

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuseAndWait()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 6

    const/16 v5, 0x7d2

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->trimButtonClicked()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    sub-int v0, v1, v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveHandlePosition:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuse(I)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    :cond_2
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v5, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->doPlayPauseAction()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->onBackPressed()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f060005 -> :sswitch_0
        0x7f060006 -> :sswitch_2
        0x7f060014 -> :sswitch_1
        0x7f060015 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    const-string v1, "TrimApp_Cycle"

    const-string v2, "==> onConfigurationChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    sget v1, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->clearLongPress(Z)V

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveLeftBarTime:I

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveLeftBarTime:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveRightBarTime:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveUserBarTime:I

    :cond_2
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_3

    iput-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsConfigChanged:Z

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setupViews()V

    goto :goto_0

    :cond_3
    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsConfigChanged:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuseAndWait()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setupViews()V

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveLeftBarTime:I

    if-le v1, v5, :cond_4

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveLeftBarTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveRightBarTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveUserBarTime:I

    :goto_1
    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->refreshAllDisplay(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mUnsupportedShowing:Z

    if-nez v3, :cond_0

    if-le v2, v5, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v3, v2, v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->restoreHandleState(III)V

    goto :goto_0

    :cond_4
    move v1, v0

    move v2, v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v0, 0x0

    const v8, 0x7f07000f

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim_Share;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "TrimApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Trim APP version name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v1, v6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->setTargetProject(I)I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    aget-object v0, v1, v7

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f070011

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->regPhoneStateListener()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->enableStatusBarOpenByNotification()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mWindowDisplay:Landroid/view/Display;

    iput v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    iput v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPowerManager:Landroid/os/PowerManager;

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressPercentFormat:Ljava/text/NumberFormat;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressPercentFormat:Ljava/text/NumberFormat;

    invoke-virtual {v0, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    iput v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    iput v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    sget-boolean v0, Lcom/lifevibes/trimapp/Trim_Share;->SUPPORT_VIDEO_ROTATION:Z

    if-eqz v0, :cond_5

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    if-eqz v0, :cond_5

    :try_start_1
    const-string v1, "android.media.MediaMetadataRetriever"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "METADATA_KEY_VIDEO_ROTATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    if-ne v1, v6, :cond_6

    const/16 v1, 0x5a

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    :cond_4
    :goto_2
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    :try_start_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_5
    new-instance v0, Lcom/lifevibes/trimapp/util/Storage;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/Storage;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->setOutDirectoryForPhotoring()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->checkAvaliableLimitMemory()Z

    move-result v0

    if-nez v0, :cond_8

    const v0, 0x7f070017

    invoke-virtual {p0, v0, v6}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "RuntimeException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v8, v5}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto/16 :goto_0

    :cond_6
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    if-ne v1, v7, :cond_7

    const/16 v1, 0xb4

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    goto :goto_2

    :cond_7
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    const/16 v1, 0x10e

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    goto :goto_2

    :cond_8
    sget-object v0, Lcom/lifevibes/trimapp/util/Storage;->PROJECT_DIR:Ljava/lang/String;

    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v1

    if-nez v1, :cond_a

    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.trim/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_a
    new-instance v1, Lcom/lifevibes/trimapp/util/MediaShare;

    sget-object v2, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/lifevibes/trimapp/util/MediaShare;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/util/MediaShare;->loadLibrary(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_b
    invoke-virtual {p0, v8, v5}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_d

    :cond_d
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setupViews()V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto/16 :goto_3

    :catch_3
    move-exception v1

    goto/16 :goto_1

    :catch_4
    move-exception v1

    goto/16 :goto_1

    :catch_5
    move-exception v1

    goto/16 :goto_1

    :catch_6
    move-exception v1

    goto/16 :goto_1

    :catch_7
    move-exception v1

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5

    const v4, 0x104000a

    const/high16 v3, 0x1040000

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    packed-switch p1, :pswitch_data_0

    move-object v0, v1

    :goto_0
    return-object v0

    :pswitch_0
    const/high16 v2, 0x7f030000

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f06000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getEditTextFilter()[Landroid/text/InputFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mEditTextName:Landroid/widget/EditText;

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share$7;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim_Share$7;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$8;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$8;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$9;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$9;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    const v1, 0x7f070003

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07000e

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lifevibes/trimapp/Trim_Share$10;

    invoke-direct {v3, p0}, Lcom/lifevibes/trimapp/Trim_Share$10;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070002

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f040000

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share$11;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim_Share$11;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_3
    const v0, 0x7f070013

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    const v0, 0x7f070015

    :cond_0
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mUnsupportedShowing:Z

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$13;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$13;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$12;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$12;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f090000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f06001c

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v1, 0x7f06001e

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->unregPhoneStateListener()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuseAndWait()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mControlLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_4

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->setOutDirectoryForPhotoring()V

    sget-boolean v0, Lcom/lifevibes/trimapp/Trim_Share;->SUPPORT_VIDEO_ROTATION:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    if-eqz v0, :cond_1

    :try_start_0
    const-string v1, "android.media.MediaMetadataRetriever"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "METADATA_KEY_VIDEO_ROTATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    if-ne v1, v4, :cond_5

    const/16 v1, 0x5a

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    :cond_0
    :goto_1
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->setIntent(Landroid/content/Intent;)V

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    :cond_2
    :goto_3
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_3

    :cond_5
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    const/16 v1, 0xb4

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    goto :goto_1

    :cond_6
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/16 v1, 0x10e

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoRotation:I

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0

    :catch_4
    move-exception v1

    goto :goto_0

    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->onBackPressed()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->trimButtonClicked()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f06001d -> :sswitch_0
        0x7f06001e -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->doPauseAction()Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuseAndWait()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->clearLongPress(Z)V

    :cond_0
    invoke-virtual {p0, v2, v2}, Lcom/lifevibes/trimapp/Trim_Share;->overridePendingTransition(II)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share$14;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share$14;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onRestart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 7

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "TrimApp_Cycle"

    const-string v3, "==> onResume"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v3, v3, v4

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_7

    const v0, 0x7f070011

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    move v0, v1

    :goto_0
    new-instance v3, Lcom/lifevibes/trimapp/Trim_Share$6;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v3, p0, v4}, Lcom/lifevibes/trimapp/Trim_Share$6;-><init>(Lcom/lifevibes/trimapp/Trim_Share;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mObserver:Landroid/os/FileObserver;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mObserver:Landroid/os/FileObserver;

    invoke-virtual {v3}, Landroid/os/FileObserver;->startWatching()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/lifevibes/trimapp/Trim_Share;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsMultiWindowError:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.VIDEOCAPABILITY"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/lifevibes/trimapp/Trim_Share;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mUnsupportedShowing:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v3, v4, :cond_3

    const/4 v3, 0x6

    invoke-virtual {p0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    :cond_0
    :goto_1
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRestart:Z

    :cond_1
    :goto_2
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRefeshdisplay:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_1

    :cond_4
    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRefeshdisplay:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFileCount:I

    aget-object v1, v1, v3

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->refreshAllDisplay(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNewIntent:Z

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    goto :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mRefeshdisplay:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    return-void
.end method

.method public onStop()V
    .locals 3

    const/4 v0, 0x0

    const-string v1, "TrimApp_Cycle"

    const-string v2, "==> onStop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v1, v2, :cond_0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuseAndWait()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    const-string v0, "TrimApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged() hasFocus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x1006

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    return-void
.end method

.method public refreshAllDisplay(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    if-ne v0, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-virtual {v0, p1, v1}, Lcom/lifevibes/trimapp/util/MediaShare;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    if-eqz v0, :cond_3

    const v1, 0x40000006    # 2.0000014f

    if-ne v0, v1, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->showDialog(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->showDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_4

    const v0, 0x7f070014

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->removeDialog(I)V

    invoke-virtual {p0, v5}, Lcom/lifevibes/trimapp/Trim_Share;->removeDialog(I)V

    invoke-virtual {p0, v4}, Lcom/lifevibes/trimapp/Trim_Share;->removeDialog(I)V

    iput v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_mask_in_count:I

    iput v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveLeftBarTime:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveRightBarTime:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mSaveUserBarTime:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setStartTime(I)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I

    if-lez v0, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTotalDuration:I

    iput v1, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    :cond_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setEndTime(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->refreshDisplay()V

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->clearThumbnailImage(I)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->updateButtonsEnabled()V

    iput v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iput-boolean v4, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_0
.end method

.method public requestTrimming(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/16 v8, 0x7d1

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v7}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuse(I)V

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f070020

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".mp4"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_2
    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/Trim_Share;->showDialog(I)V

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->setProgressDialogValue(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v2, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v2

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v3, v3, 0x1

    if-eq v3, v1, :cond_4

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput v8, v1, Landroid/os/Message;->what:I

    iput v0, v1, Landroid/os/Message;->arg1:I

    iput v2, v1, Landroid/os/Message;->arg2:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v3, "TrimApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v3, "TrimApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07001e

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->showToast(II)V

    move v0, v1

    goto/16 :goto_2

    :cond_5
    invoke-direct {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->doTrimmingAction(II)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method

.method public setProgressDialogValue(I)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_0
    return-void
.end method

.method public setVideoViewVisibility(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setupViews()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_WIDTH:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim_Share;->VIDEOVIEW_HEIGHT:I

    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->setContentView(I)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setupUIComponents()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share;->setupUIImageViews()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share;->onButtonClick(Landroid/view/View;)V

    return-void
.end method

.method public showToast(II)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public showToast(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public startThumbnailTask(I)V
    .locals 6

    const/16 v5, 0x7d0

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share;->doPauseAction()Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    :cond_2
    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->releaseDecoderInuse(I)V

    :cond_3
    :goto_1
    and-int/lit8 v0, p1, 0x2

    if-eq v0, v1, :cond_4

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share;->clearThumbnailImage(I)V

    :cond_4
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    if-nez v0, :cond_6

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    if-ne v0, v4, :cond_3

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mIsReleased:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mThumbnail_released:Z

    if-eqz v0, :cond_0

    :cond_7
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v5, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public updateButtonsEnabled()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->isLongPressing()Z

    move-result v0

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v3, :cond_4

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :goto_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getStartTime()I

    move-result v1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v0, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getEndTime()I

    move-result v1

    if-eq v0, v1, :cond_8

    :cond_5
    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v3, :cond_8

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mMenuItemDone:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/Trim_Share;->setMenuItemDoneEnable(Z)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mTrimButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    goto/16 :goto_0
.end method

.method public updatePlayPauseButton(Z)V
    .locals 7

    const/high16 v6, 0x7f060000

    const/16 v5, 0x8

    const/4 v2, 0x0

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_2

    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    and-int/2addr v0, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const v1, 0x7f060015

    invoke-virtual {v0, v3, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusDownId(II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPauseButton:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->isLongPressing()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_6

    sget v0, Lcom/lifevibes/trimapp/Trim_Share;->CURRENT_MODEL_TYPE:I

    and-int/2addr v0, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    const v1, 0x7f060014

    invoke-virtual {v0, v3, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setNextFocusDownId(II)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    invoke-virtual {v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_0
.end method
