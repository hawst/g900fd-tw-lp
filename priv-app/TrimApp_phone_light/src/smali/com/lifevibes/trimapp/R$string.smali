.class public final Lcom/lifevibes/trimapp/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f070000

.field public static final button_cancel:I = 0x7f070009

.field public static final button_discard:I = 0x7f07002a

.field public static final button_done:I = 0x7f07000a

.field public static final button_pause:I = 0x7f070008

.field public static final button_play:I = 0x7f070006

.field public static final button_preview:I = 0x7f070007

.field public static final button_rotate:I = 0x7f070026

.field public static final button_save:I = 0x7f070029

.field public static final button_trim:I = 0x7f070005

.field public static final button_undo:I = 0x7f07000b

.field public static final during_call:I = 0x7f070024

.field public static final menu_newName:I = 0x7f07000d

.field public static final menu_originalName:I = 0x7f07000c

.field public static final message_alreadyExistsFile:I = 0x7f070012

.field public static final message_error_code:I = 0x7f07001b

.field public static final message_fileNotExist:I = 0x7f070011

.field public static final message_help:I = 0x7f07002e

.field public static final message_invalidCharacter:I = 0x7f07001f

.field public static final message_maximum:I = 0x7f070021

.field public static final message_nameEmpty:I = 0x7f070020

.field public static final message_notEnoughSpace:I = 0x7f070017

.field public static final message_notResponding:I = 0x7f070018

.field public static final message_play_bar:I = 0x7f070022

.field public static final message_save_your_changes_or_discard_them:I = 0x7f070028

.field public static final message_securityException:I = 0x7f07001e

.field public static final message_shortDuration:I = 0x7f070016

.field public static final message_stop_trimming:I = 0x7f070027

.field public static final message_tooLargeFile:I = 0x7f070015

.field public static final message_tooSmallFile:I = 0x7f070014

.field public static final message_trim_bar:I = 0x7f070023

.field public static final message_trimming:I = 0x7f07000e

.field public static final message_trimmingCancel:I = 0x7f07001c

.field public static final message_trimmingFail:I = 0x7f07001d

.field public static final message_trimming_success:I = 0x7f070019

.field public static final message_unable_to_trim:I = 0x7f070025

.field public static final message_unexpectedData:I = 0x7f070010

.field public static final message_unexpectedError:I = 0x7f07000f

.field public static final message_unsupportedFile:I = 0x7f070013

.field public static final message_warning_code:I = 0x7f07001a

.field public static final title_newFilename:I = 0x7f070003

.field public static final title_saveas:I = 0x7f07002b

.field public static final title_trim:I = 0x7f070002

.field public static final title_trimfile:I = 0x7f07002c

.field public static final title_unabletotrimfile:I = 0x7f070004

.field public static final title_video_trimmer:I = 0x7f07002d

.field public static final trim_label:I = 0x7f070001


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
