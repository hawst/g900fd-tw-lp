.class public Lcom/lifevibes/trimapp/Trim_Share_Luncher;
.super Landroid/app/Activity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;,
        Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;
    }
.end annotation


# static fields
.field public static CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String; = null

.field static final DECODER_USER_THUMBNAIL:I = 0x1

.field static final DECODER_USER_TRIMMING:I = 0x2

.field static final DECODER_USER_UNKNOWN:I = 0x0

.field static final DECODER_USER_VIDEOPLAYER:I = 0x4

.field static final DIALOG_CHOICE_FILENAME:I = 0x3

.field static final DIALOG_FILENAME:I = 0x1

.field static final DIALOG_PROGRESS:I = 0x2

.field static final DIALOG_UNSUPPORTED_FILESIZE:I = 0x4

.field static final DIALOG_UNSUPPORTED_TYPE:I = 0x5

.field public static final LINITED_DURATION:I = 0x3a98

.field public static final LOG_VIEW:Z = false

.field public static final MAXIMUM_FILE_COUNT:I = 0x64

.field static final MAX_FILENAME_LENGTH:I = 0x32

.field static final MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

.field static final MSG_DIALOG_PROGRESS:I = 0x64

.field public static final OUTPUT_EXT:Ljava/lang/String; = ".mp4"

.field private static final OUTPUT_FILENAME:Ljava/lang/String; = "photoring_temp"

.field public static SUPPORT_MULT_ORIENTATION:Z = false

.field public static SUPPORT_VIDEO_ROTATION:Z = false

.field public static final TAG:Ljava/lang/String; = "TrimApp_Share_Luncher"

.field public static final TrimUIOption:Ljava/lang/String; = "PostProcessedTrim"

.field static final mInvalidFileNameChar:[Ljava/lang/String;


# instance fields
.field private mCurrentDecoderUser:I

.field private mEditTextName:Landroid/widget/EditText;

.field private mFileCount:I

.field private mFilenameDialog:Landroid/app/AlertDialog;

.field private final mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

.field private mIsActivityFinishing:Z

.field private mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

.field private mOnTrimmingFile:Ljava/lang/String;

.field private mProgressDialog_V19:Landroid/app/ProgressDialog;

.field public final mProgressListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;

.field private mProgressPercentFormat:Ljava/text/NumberFormat;

.field private mStorage:Lcom/lifevibes/trimapp/util/Storage;

.field private mTotalDuration:I

.field private final mTrimmedFiles:[Ljava/lang/String;

.field private mTrimmingDone:Z

.field public final mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;

.field private mTrimmingSuccess:Z

.field private mUIOption:Ljava/lang/String;

.field private mVideoRotation:I

.field private toastMsg:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sput-boolean v3, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->SUPPORT_MULT_ORIENTATION:Z

    sput-boolean v4, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->SUPPORT_VIDEO_ROTATION:Z

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v3

    const-string v1, "/"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mInvalidFileNameChar:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x64

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->toastMsg:Landroid/widget/Toast;

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;Lcom/lifevibes/trimapp/Trim_Share_Luncher$1;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;

    new-instance v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mIsActivityFinishing:Z

    return-void
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    return-void
.end method

.method static synthetic access$1000(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->releaseTrimming()V

    return-void
.end method

.method static synthetic access$1100(Lcom/lifevibes/trimapp/Trim_Share_Luncher;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->stopTrimming(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Lcom/lifevibes/trimapp/util/MediaShare;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    return-object v0
.end method

.method static synthetic access$200(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mIsActivityFinishing:Z

    return v0
.end method

.method static synthetic access$300(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/lifevibes/trimapp/Trim_Share_Luncher;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingDone:Z

    return p1
.end method

.method static synthetic access$500(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingSuccess:Z

    return v0
.end method

.method static synthetic access$502(Lcom/lifevibes/trimapp/Trim_Share_Luncher;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingSuccess:Z

    return p1
.end method

.method static synthetic access$600(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mOnTrimmingFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)Lcom/lifevibes/trimapp/util/Storage;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    return-object v0
.end method

.method static synthetic access$800(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    return v0
.end method

.method static synthetic access$812(Lcom/lifevibes/trimapp/Trim_Share_Luncher;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    return-object v0
.end method

.method private doTrimmingAction(II)V
    .locals 10

    const v9, -0x7ffffffc

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v1, v0, v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mOnTrimmingFile:Ljava/lang/String;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingSuccess:Z

    const-string v0, "TrimApp_Share_Luncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bebingTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "endTime="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    int-to-float v3, v3

    const-wide/32 v4, 0x1e6666

    iget-object v8, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$TrimmingListener;

    move v6, p1

    move v7, p2

    invoke-virtual/range {v0 .. v8}, Lcom/lifevibes/trimapp/util/MediaShare;->startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I

    move-result v0

    if-eqz v0, :cond_0

    if-ne v0, v9, :cond_2

    const v0, 0x7f070010

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->stopTrimming(I)V

    goto :goto_0

    :cond_2
    if-ne v0, v9, :cond_3

    const v0, 0x7f070013

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->stopTrimming(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f07001d

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->stopTrimming(I)V

    goto :goto_0
.end method

.method public static getFileTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static getFileTitleNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    const-string v1, ".mp4"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releaseTrimming()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->releaseTrimming()Z

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private requestFileNameDialog()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "trimmed_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, v1, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private returnToInvoker()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingDone:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v2, v2, v3

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->setResult(ILandroid/content/Intent;)V

    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.action.TRIMAPP_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->finish()V

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private stopTrimming(I)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmingSuccess:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->releaseTrimming()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "TrimApp_Share_Luncher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p2, p3}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->finish()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->toastMsg:Landroid/widget/Toast;

    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "TrimApp_Share_Luncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Trim APP version name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v1, v6

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    const-string v0, "TrimApp_Share_Luncher"

    const-string v1, "==> onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v0, "uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const-string v0, "option"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mUIOption:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    const v0, 0x7f070011

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressPercentFormat:Ljava/text/NumberFormat;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressPercentFormat:Ljava/text/NumberFormat;

    invoke-virtual {v0, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    iput v5, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    sget-boolean v0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->SUPPORT_VIDEO_ROTATION:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    if-eqz v0, :cond_0

    :try_start_1
    const-string v1, "android.media.MediaMetadataRetriever"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "METADATA_KEY_VIDEO_ROTATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    const/16 v1, 0x5a

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    :cond_5
    :goto_2
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    :try_start_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TrimApp_Share_Luncher"

    const-string v1, "RuntimeException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    goto/16 :goto_0

    :cond_6
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    if-ne v1, v6, :cond_7

    const/16 v1, 0xb4

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    goto :goto_2

    :cond_7
    iget v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    const/16 v1, 0x10e

    iput v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mVideoRotation:I

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_1

    :catch_6
    move-exception v1

    goto :goto_1

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    const/4 v1, 0x0

    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    const v2, 0x7f07000e

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lifevibes/trimapp/Trim_Share_Luncher$1;

    invoke-direct {v3, p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$1;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressListener:Lcom/lifevibes/trimapp/Trim_Share_Luncher$ProgressDialogListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_2
    const v0, 0x7f070013

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    const v0, 0x7f070015

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/lifevibes/trimapp/Trim_Share_Luncher$3;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$3;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/Trim_Share_Luncher$2;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher$2;-><init>(Lcom/lifevibes/trimapp/Trim_Share_Luncher;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "TrimApp_Share_Luncher"

    const-string v3, "onResume"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/lifevibes/trimapp/util/Storage;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/Storage;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->setOutDirectoryForPhotoring()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->checkAvaliableLimitMemory()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f070017

    invoke-virtual {p0, v0, v1}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/lifevibes/trimapp/util/Storage;->PROJECT_DIR:Ljava/lang/String;

    const-string v3, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/.trim/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    new-instance v3, Lcom/lifevibes/trimapp/util/MediaShare;

    sget-object v4, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    invoke-direct {v3, p0, v4}, Lcom/lifevibes/trimapp/util/MediaShare;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v3, v0}, Lcom/lifevibes/trimapp/util/MediaShare;->loadLibrary(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->releaseTrimming()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-virtual {v0, v3, v4}, Lcom/lifevibes/trimapp/util/MediaShare;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    if-eqz v0, :cond_6

    const v1, 0x40000006    # 2.0000014f

    if-ne v0, v1, :cond_5

    const v0, 0x7f070015

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    :goto_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->releaseTrimming()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->returnToInvoker()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    goto :goto_0

    :cond_5
    const v0, 0x7f070013

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    goto :goto_1

    :cond_6
    const-string v0, "TrimApp_Share_Luncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Trim UI option="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mUIOption:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I

    const-string v0, "TrimApp_Share_Luncher"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mTotalDuration"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mUIOption:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mUIOption:Ljava/lang/String;

    const-string v3, "PostProcessedTrim"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    iget v3, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTotalDuration:I

    const/16 v4, 0x3afc

    if-le v3, v4, :cond_8

    move v3, v1

    :goto_3
    or-int/2addr v0, v3

    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "uri"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/16 v3, 0xc8

    invoke-virtual {p0, v0, v3}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mIsActivityFinishing:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->releaseTrimming()Z

    :cond_7
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mIsActivityFinishing:Z

    if-eqz v0, :cond_9

    :goto_4
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    goto/16 :goto_0

    :cond_8
    move v3, v2

    goto :goto_3

    :cond_9
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mIsActivityFinishing:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->requestFileNameDialog()V

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_2
.end method

.method public requestTrimming(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f070020

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".mp4"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    const v0, 0x7f070012

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    move v0, v1

    :cond_2
    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFilenameDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mOnTrimmingFile:Ljava/lang/String;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mCurrentDecoderUser:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showDialog(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mProgressDialog_V19:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-direct {p0, v2, v0}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->doTrimmingAction(II)V

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "TrimApp_Share_Luncher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v3, "TrimApp_Share_Luncher"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07001e

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->showToast(II)V

    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method public showToast(II)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share_Luncher;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
