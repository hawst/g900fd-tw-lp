.class Lcom/lifevibes/trimapp/Trim$VideoViewListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VideoViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim$VideoViewListener;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->moveUserHandleTo(IZ)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/Trim;->access$1002(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->releaseVideoPlayer()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2500(Lcom/lifevibes/trimapp/Trim;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1002(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->releaseVideoPlayer()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$2500(Lcom/lifevibes/trimapp/Trim;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6

    const-wide/16 v4, 0xc8

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    # setter for: Lcom/lifevibes/trimapp/Trim;->mSaveHandlePosition:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$2602(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x68

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x67

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$2702(Lcom/lifevibes/trimapp/Trim;Z)Z

    return-void
.end method
