.class public Lcom/lifevibes/trimapp/Trim;
.super Landroid/app/Activity;

# interfaces
.implements Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;,
        Lcom/lifevibes/trimapp/Trim$ThumbnailTask;,
        Lcom/lifevibes/trimapp/Trim$ThumbnailImage;,
        Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;,
        Lcom/lifevibes/trimapp/Trim$VideoViewListener;,
        Lcom/lifevibes/trimapp/Trim$VideoSurfaceListener;,
        Lcom/lifevibes/trimapp/Trim$TrimmingListener;,
        Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;
    }
.end annotation


# static fields
.field public static CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String; = null

.field public static CURRENT_MODEL_TYPE:I = 0x0

.field static final DECODER_USER_THUMBNAIL:I = 0x1

.field static final DECODER_USER_TRIMMING:I = 0x2

.field static final DECODER_USER_UNKNOWN:I = 0x0

.field static final DECODER_USER_VIDEOPLAYER:I = 0x4

.field public static final GUI_BASE_SMARTPHONE:I = -0x80000000

.field public static final GUI_BASE_TABLET:I = 0x40000000

.field public static final LOG_VIEW:Z = false

.field public static final MAXIMUM_FILE_COUNT:I = 0x64

.field static final MAX_FILENAME_LENGTH:I = 0x32

.field static final MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

.field public static final MINIMUM_DURATION:I = 0x3e8

.field public static final MODEL_TYPE_J:I = -0x7fffffff

.field public static final MODEL_TYPE_LT03:I = 0x40000002

.field static final MSG_ACTION_PLAYVIDEO:I = 0x7d2

.field static final MSG_ACTION_THUMBNAIL:I = 0x7d0

.field static final MSG_ACTION_TRIMMING:I = 0x7d1

.field static final MSG_ARG_PAUSING_VIDEO:I = 0x65

.field static final MSG_ARG_PLAYING_VIDEO:I = 0x64

.field static final MSG_ARG_STOPPING_VIDEO:I = 0x66

.field static final MSG_DIALOG_PROGRESS:I = 0x64

.field static final MSG_REFRESH_DISPLAY:I = 0x65

.field static final MSG_RESTORE_USER_HANDLE_POS:I = 0x69

.field static final MSG_SHOW_KEYPAD:I = 0x6a

.field static final MSG_UPDATE_PLAYPAUSE:I = 0x68

.field static final MSG_UPDATE_VIDEO_PROGRESS:I = 0x67

.field public static final SUPPORT_SCARED_THUMBNAIL:Z = true

.field public static SUPPORT_VIDEO_ROTATION:Z = false

.field public static final TAG:Ljava/lang/String; = "TrimApp"

.field public static final TAG_CYCLE:Ljava/lang/String; = "TrimApp_Cycle"

.field static final THUMBNAIL_MASK_ALL:I = 0xf

.field static final THUMBNAIL_MASK_MAIN:I = 0x2

.field static final THUMBNAIL_MASK_TN:I = 0x4

.field static final THUMBNAIL_MASK_TN_ZOOM:I = 0x8

.field static final THUMBNAIL_MASK_UNKNOWN:I = 0x0

.field public static VIDEOVIEW_HEIGHT:I = 0x0

.field static final VIDEOVIEW_STATE_INTURRUPTED:I = 0x2

.field static final VIDEOVIEW_STATE_LONG_PRESS:I = 0x3

.field static final VIDEOVIEW_STATE_LONG_RELEASE:I = 0x4

.field static final VIDEOVIEW_STATE_NONE:I = 0x0

.field static final VIDEOVIEW_STATE_STARTED:I = 0x1

.field public static VIDEOVIEW_WIDTH:I

.field private static mActionWaitWatcherPlay:I

.field private static mActionWaitWatcherTrim:I

.field static final mInvalidFileNameChar:[Ljava/lang/String;


# instance fields
.field private final mActionHandler:Landroid/os/Handler;

.field private final mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mAudioReceiver:Landroid/content/BroadcastReceiver;

.field private mControlLayout:Landroid/widget/RelativeLayout;

.field private mCurrentDecoderUser:I

.field private mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

.field private mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

.field private mDragging:Z

.field private mEditTextName:Landroid/widget/EditText;

.field private mFileCount:I

.field private mFilenameDialog:Landroid/app/AlertDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mHelpDialog:Landroid/app/Dialog;

.field private final mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

.field private mIsActivityFinishing:Z

.field private mIsConfigChanged:Z

.field private mIsMultiWindowError:Z

.field private mIvFullImage:[Landroid/widget/ImageView;

.field private mIvThumbnails:[Landroid/widget/ImageView;

.field private mIvThumbnailsZoom:[Landroid/widget/ImageView;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

.field private mMenuItemDone:Landroid/view/MenuItem;

.field private mNewIntent:Z

.field private mNumberOfImages:I

.field private mObserver:Landroid/os/FileObserver;

.field private mOnTrimmingFile:Ljava/lang/String;

.field private mOverwriteFilename:Z

.field private mPauseButton:Landroid/view/View;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPlayButton:Landroid/view/View;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPreviewState:I

.field public final mProgressListener:Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;

.field private mProgressPercentFormat:Ljava/text/NumberFormat;

.field private mRefeshdisplay:Z

.field private mRestart:Z

.field private mSaveHandlePosition:I

.field public mSaveLeftBarTime:I

.field public mSaveRightBarTime:I

.field public mSaveUserBarTime:I

.field private mStorage:Lcom/lifevibes/trimapp/util/Storage;

.field private mThumbnailMask:I

.field private mThumbnailOffset:I

.field private mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

.field private mThumbnailTaskUid:J

.field private mThumbnail_mask_in_count:I

.field private mThumbnail_released:Z

.field private mTotalDuration:I

.field private final mTrimmedFiles:[Ljava/lang/String;

.field private mTrimmingDone:Z

.field public final mTrimmingListener:Lcom/lifevibes/trimapp/Trim$TrimmingListener;

.field private mTrimmingSuccess:Z

.field private mUnsupportedShowing:Z

.field private final mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

.field private mVideoOnprepared:Z

.field private mVideoRotation:I

.field private mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

.field private mWindowDisplay:Landroid/view/Display;

.field private titleActionBar:Landroid/app/ActionBar;

.field private toastMsg:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v0, -0x7fffffff

    sput v0, Lcom/lifevibes/trimapp/Trim;->CURRENT_MODEL_TYPE:I

    sput-boolean v4, Lcom/lifevibes/trimapp/Trim;->SUPPORT_VIDEO_ROTATION:Z

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v3

    const-string v1, "/"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/lifevibes/trimapp/Trim;->mInvalidFileNameChar:[Ljava/lang/String;

    sput v3, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherPlay:I

    sput v3, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/lifevibes/trimapp/Trim;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x64

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    new-instance v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$TrimmingListener;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$TrimmingListener;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim$TrimmingListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mProgressListener:Lcom/lifevibes/trimapp/Trim$ProgressDialogListener;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mRefeshdisplay:Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I

    iput-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$1;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$1;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$2;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$2;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$3;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$3;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$4;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$4;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$7;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$7;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$8;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$8;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/Trim;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    return v0
.end method

.method static synthetic access$1002(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/lifevibes/trimapp/Trim;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$ThumbnailTask;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    return v0
.end method

.method static synthetic access$1372(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    return v0
.end method

.method static synthetic access$1400(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailOffset:I

    return v0
.end method

.method static synthetic access$1402(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailOffset:I

    return p1
.end method

.method static synthetic access$1500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/util/MediaShare;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mDragging:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mDragging:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->setVideoProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    return v0
.end method

.method static synthetic access$200(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    return v0
.end method

.method static synthetic access$2000(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/lifevibes/trimapp/Trim;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$212(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    return v0
.end method

.method static synthetic access$2200(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mOverwriteFilename:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseTrimming()V

    return-void
.end method

.method static synthetic access$2400(Lcom/lifevibes/trimapp/Trim;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseVideoPlayer()V

    return-void
.end method

.method static synthetic access$2600(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mSaveHandlePosition:I

    return v0
.end method

.method static synthetic access$2602(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveHandlePosition:I

    return p1
.end method

.method static synthetic access$2702(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsConfigChanged:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    return p1
.end method

.method static synthetic access$300(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    return-void
.end method

.method static synthetic access$3000(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/lifevibes/trimapp/Trim;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/lifevibes/trimapp/Trim;->doTrimmingAction(II)V

    return-void
.end method

.method static synthetic access$3200(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3272(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3276(Lcom/lifevibes/trimapp/Trim;I)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    return v0
.end method

.method static synthetic access$3300(Lcom/lifevibes/trimapp/Trim;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim;->resolveUpdatePlayPauseMessage(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/lifevibes/trimapp/Trim;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mEditTextName:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/lifevibes/trimapp/Trim;)Landroid/os/PowerManager;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPowerManager:Landroid/os/PowerManager;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    return v0
.end method

.method static synthetic access$3700(Lcom/lifevibes/trimapp/Trim;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/lifevibes/trimapp/Trim;)Landroid/app/ActionBar;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/lifevibes/trimapp/Trim;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim;->setMenuItemDoneEnable(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingDone:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/lifevibes/trimapp/Trim;)J
    .locals 2

    iget-wide v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTaskUid:J

    return-wide v0
.end method

.method static synthetic access$4002(Lcom/lifevibes/trimapp/Trim;J)J
    .locals 1

    iput-wide p1, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTaskUid:J

    return-wide p1
.end method

.method static synthetic access$402(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingDone:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    return v0
.end method

.method static synthetic access$4200(Lcom/lifevibes/trimapp/Trim;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    return-object v0
.end method

.method static synthetic access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/lifevibes/trimapp/Trim;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsMultiWindowError:Z

    return v0
.end method

.method static synthetic access$602(Lcom/lifevibes/trimapp/Trim;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/Trim;->mIsMultiWindowError:Z

    return p1
.end method

.method static synthetic access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/lifevibes/trimapp/Trim;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    return v0
.end method

.method static synthetic access$900(Lcom/lifevibes/trimapp/Trim;)[Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private clearThumbnailImage(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    and-int/lit8 v0, p1, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    and-int/lit8 v0, p1, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    :goto_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private computeNewFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "_data LIKE \'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, v1, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_%.mp4\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/lifevibes/trimapp/Trim;->MEDIA_VIDEO_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_data ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    const/16 v1, 0x3e8

    new-array v2, v1, [I

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v5, v5, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/lifevibes/trimapp/Trim;->getFileTitleNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    aput v3, v2, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    array-length v3, v2

    move v1, v6

    :goto_1
    if-ge v1, v3, :cond_3

    aget v0, v2, v1

    if-eq v9, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "_"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "%03d"

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/io/File;

    iget-object v7, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v7, v0}, Lcom/lifevibes/trimapp/util/Storage;->getOutputFullPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    move-object v4, v0

    :cond_3
    return-object v4

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private doChoiceFilenameAction(I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, v5}, Lcom/lifevibes/trimapp/Trim;->isLowMemory(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim;->mOverwriteFilename:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trimmed_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/lifevibes/trimapp/Trim;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/Trim;->isLowMemory(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->getFileTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x32

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    if-le v2, v1, :cond_1

    add-int/lit8 v2, v1, -0x5

    invoke-virtual {v0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    add-int/lit8 v1, v1, -0x5

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->computeNewFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->prepareTrimming()V

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v5, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(ILjava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doTrimmingAction(II)V
    .locals 10

    const/16 v2, 0x7d1

    const v9, -0x7ffffffc

    const/4 v4, 0x0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_0
    sget v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    const v0, 0x7f070018

    invoke-virtual {p0, v0, v4}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v2, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput p2, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    sput v4, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v0, v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v0, v3

    float-to-int v3, v0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    int-to-float v3, v3

    const-wide/16 v4, 0x0

    iget-object v8, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingListener:Lcom/lifevibes/trimapp/Trim$TrimmingListener;

    move v6, p1

    move v7, p2

    invoke-virtual/range {v0 .. v8}, Lcom/lifevibes/trimapp/util/MediaShare;->startTrimming(Ljava/lang/String;Ljava/lang/String;FJIILcom/lifevibes/trimapp/util/MediaShare$MediaShareTrimListener;)I

    move-result v0

    if-eqz v0, :cond_1

    if-ne v0, v9, :cond_4

    const v0, 0x7f070010

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V

    goto :goto_0

    :cond_4
    if-ne v0, v9, :cond_5

    const v0, 0x7f070013

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V

    goto :goto_0

    :cond_5
    const v0, 0x7f07001d

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V

    goto :goto_0
.end method

.method private enableStatusBarOpenByNotification()V
    .locals 5

    const-string v0, "TrimApp"

    const-string v1, "start enableStatusBarByNotification"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const-string v1, "android.view.WindowManager$LayoutParams"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "SAMSUNG_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const-string v3, "samsungFlags"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    or-int/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Class.getDeclaredField() NoSuchFieldException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Field.getInt/setInt IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Field.getInt/setInt IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_3
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "Class.forName() ClassNotFoundException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getEditTextFilter()[Landroid/text/InputFilter;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x1

    new-instance v2, Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim$FilenameLengthFilter;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    aput-object v2, v0, v1

    const/4 v1, 0x0

    new-instance v2, Lcom/lifevibes/trimapp/Trim$9;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/Trim$9;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static getFileTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static getFileTitleNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    const-string v1, ".mp4"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLowMemory(Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v3, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v4

    sub-int v3, v4, v3

    int-to-float v3, v3

    int-to-float v0, v0

    div-float v0, v3, v0

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-float v4, v4

    mul-float/2addr v0, v4

    float-to-long v4, v0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, v0, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v4, v5, v0}, Lcom/lifevibes/trimapp/util/Storage;->isLowMemory(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f070017

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private isModifiedAppValues()Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    int-to-float v4, v3

    add-float/2addr v0, v4

    const/high16 v4, 0x43b40000    # 360.0f

    rem-float/2addr v0, v4

    float-to-int v0, v0

    if-eq v0, v3, :cond_4

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v3, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getStartTime()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getEndTime()I

    move-result v4

    if-eq v3, v4, :cond_3

    :cond_0
    move v3, v1

    :goto_1
    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public static isTFT_LCD_Device()Z
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "mega2"

    aput-object v1, v0, v2

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v1, v2

    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v4, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TrimApp"

    const-string v1, "this device is TFT DEVICE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :cond_0
    return v2

    :catch_0
    move-exception v0

    const-string v6, "TrimApp"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "error:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private isVideoPlaying()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isPlaying()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static is_MODEL_T_devices()Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "trhlte"

    aput-object v1, v0, v2

    const-string v1, "trlte"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v4, "trelte"

    aput-object v4, v0, v1

    const/4 v1, 0x3

    const-string v4, "tre3g"

    aput-object v4, v0, v1

    const/4 v1, 0x4

    const-string v4, "tblte"

    aput-object v4, v0, v1

    const/4 v1, 0x5

    const-string v4, "SC-01G"

    aput-object v4, v0, v1

    const/4 v1, 0x6

    const-string v4, "SCL24"

    aput-object v4, v0, v1

    const/4 v1, 0x7

    const-string v4, "tbelte"

    aput-object v4, v0, v1

    const/16 v1, 0x8

    const-string v4, "trhplte"

    aput-object v4, v0, v1

    const/16 v1, 0x9

    const-string v4, "tbhplte"

    aput-object v4, v0, v1

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "TrimApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deviceName= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move v1, v2

    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TrimApp"

    const-string v1, "this device is MODEL_T"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private prepareTrimming()V
    .locals 9

    const/4 v8, 0x5

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const-string v2, "-trim-%02d-%02d-%02d-%02d-%02d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    const/16 v5, 0xc

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0xd

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const-string v4, ".mp4"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    rsub-int/lit8 v4, v4, 0x32

    add-int/2addr v2, v3

    if-le v2, v4, :cond_0

    sub-int v2, v4, v3

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(ILjava/lang/String;)V

    return-void
.end method

.method private regPhoneStateListener()V
    .locals 3

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    new-instance v1, Lcom/lifevibes/trimapp/Trim$5;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/Trim$5;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method private releaseDecoderInuse(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseThumbnailTask()V

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    if-eq v0, p1, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseTrimming()V

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    if-eq v0, p1, :cond_2

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseVideoPlayer()V

    :cond_2
    return-void
.end method

.method private releaseDecoderInuseAndWait()V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuse(I)V

    :goto_0
    const/16 v1, 0x32

    if-ge v0, v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private releaseThumbnailTask()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTaskUid:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mGetThumbnailsDone:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->cancelThumbnail()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->cancel(Z)Z

    :cond_1
    return-void
.end method

.method private releaseTrimming()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->releaseTrimming()Z

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getDialogId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getShowsDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private releaseVideoPlayer()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isVideoPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v3}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v3}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setTag(Ljava/lang/Object;)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mDragging:Z

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->resolveUpdatePlayPauseMessage(I)V

    goto :goto_0
.end method

.method private resolveUpdatePlayPauseMessage(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim;->setVideoViewVisibility(Z)V

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim;->updatePlayPauseButton(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->updateButtonsEnabled()V

    return-void

    :cond_0
    const/16 v0, 0x65

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim;->updatePlayPauseButton(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim;->setVideoViewVisibility(Z)V

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim;->updatePlayPauseButton(Z)V

    goto :goto_0
.end method

.method private returnToInvoker()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingDone:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    aget-object v0, v0, v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-boolean v4, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingDone:Z

    if-eqz v4, :cond_2

    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v2, "overwrite"

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v3}, Lcom/lifevibes/trimapp/Trim;->setResult(ILandroid/content/Intent;)V

    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "intent.action.TRIMAPP_RESULT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v3, "overwrite"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim;->sendBroadcast(Landroid/content/Intent;)V

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->finish()V

    return-void

    :cond_2
    invoke-virtual {p0, v2, v3}, Lcom/lifevibes/trimapp/Trim;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private rotateButtonClicked()V
    .locals 5

    const/high16 v4, 0x43340000    # 180.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v3}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setRotation(F)V

    :goto_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->arrangeView()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->updateButtonsEnabled()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v4}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setRotation(F)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setRotation(F)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setRotation(F)V

    goto :goto_1
.end method

.method private setMenuItemDoneEnable(Z)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMenuItemDone:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMenuItemDone:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method private setTargetProject(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const v0, -0x7fffffff

    goto :goto_0

    :pswitch_1
    const v0, 0x40000002    # 2.0000005f

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setVideoProgress()I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mDragging:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getCurrentPosition()I

    move-result v0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v3, v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->moveUserHandleTo(IZ)V

    if-lt v0, v2, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isVideoPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->pause()V

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseVideoPlayer()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private setupUIComponents()V
    .locals 4

    const/4 v2, 0x0

    const/high16 v3, 0x7f060000

    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mControlLayout:Landroid/widget/RelativeLayout;

    const v0, 0x7f060014

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    const v0, 0x7f060015

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    const v0, 0x7f060018

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    new-instance v0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;-><init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setOnDoubleSeekBarListener(Lcom/lifevibes/trimapp/widget/DoubleSeekBar$OnDoubleSeekBarListener;)V

    const v0, 0x7f060010

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setFocusable(Z)V

    new-instance v0, Lcom/lifevibes/trimapp/Trim$VideoSurfaceListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim$VideoSurfaceListener;-><init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setOnSurfaceListener(Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;)V

    new-instance v0, Lcom/lifevibes/trimapp/Trim$VideoViewListener;

    invoke-direct {v0, p0, v2}, Lcom/lifevibes/trimapp/Trim$VideoViewListener;-><init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v1, 0x1

    const v2, 0x7f060001

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setNextFocusDownId(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setNextFocusUpId(II)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getDisplayedNumberOfImages(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    return-void
.end method

.method private setupUIImageViews()V
    .locals 7

    const/4 v6, 0x0

    const v5, 0x7f030005

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    const v0, 0x7f060011

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setImageView(Landroid/widget/ImageView;)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v1, v0, :cond_0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    aput-object v0, v4, v1

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v4, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v2, v0, :cond_1

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnailsZoom:[Landroid/widget/ImageView;

    aput-object v0, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private showHelpDialog()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "RanBefore"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    :cond_0
    return v0
.end method

.method private stopTrimming(I)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseTrimming()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, v0}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    :cond_0
    return-void
.end method

.method private trimButtonClicked()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v3, v3, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mVideoType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/lifevibes/trimapp/util/MediaShare;->isNullEncordingAvailable(III)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f070013

    invoke-virtual {p0, v0, v4}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    sub-int v0, v1, v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    const v0, 0x7f070016

    invoke-virtual {p0, v0, v4}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    goto :goto_0

    :cond_1
    sput v4, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherTrim:I

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim;->mOverwriteFilename:Z

    invoke-direct {p0, v5}, Lcom/lifevibes/trimapp/Trim;->doChoiceFilenameAction(I)V

    goto :goto_0
.end method

.method private unregPhoneStateListener()V
    .locals 3

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method


# virtual methods
.method public RemoveDialog()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    iput-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    return-void
.end method

.method public ShowDialog(I)V
    .locals 5

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "TrimApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prev.getTag()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    new-instance v0, Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->setArguments(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/widget/TrimDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public ShowDialog(ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    new-instance v0, Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->setArguments(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/widget/TrimDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public doPauseAction()Z
    .locals 4

    const/16 v3, 0x68

    const/16 v2, 0x67

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isVideoPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->pause()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x65

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public doPlayPauseAction()V
    .locals 5

    const/16 v4, 0x7d2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    :cond_0
    sget v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherPlay:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    const v0, 0x7f070018

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherPlay:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherPlay:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v4, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f070024

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    goto :goto_0

    :cond_4
    sput v2, Lcom/lifevibes/trimapp/Trim;->mActionWaitWatcherPlay:I

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v3, v3, v4

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setTag(Ljava/lang/Object;)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoOnprepared:Z

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setUpdateTimeField(I)V

    :cond_5
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isVideoPlaying()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPauseAction()Z

    goto :goto_0

    :cond_6
    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->start()V

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x68

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x64

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isModifiedAppValues()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPauseAction()Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 6

    const/16 v5, 0x7d2

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->trimButtonClicked()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    sub-int v0, v1, v0

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mSaveHandlePosition:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuse(I)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    :cond_2
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v5, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPlayPauseAction()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->onBackPressed()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f060005 -> :sswitch_0
        0x7f060006 -> :sswitch_2
        0x7f060014 -> :sswitch_1
        0x7f060015 -> :sswitch_1
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    const-string v1, "TrimApp_Cycle"

    const-string v2, "==> onConfigurationChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    sget v1, Lcom/lifevibes/trimapp/Trim;->CURRENT_MODEL_TYPE:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->invalidateOptionsMenu()V

    :cond_2
    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->clearLongPress(Z)V

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveLeftBarTime:I

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveLeftBarTime:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveRightBarTime:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveUserBarTime:I

    :cond_3
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_4

    iput-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mIsConfigChanged:Z

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->setupViews()V

    goto :goto_0

    :cond_4
    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsConfigChanged:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->setupViews()V

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    :goto_1
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setIsUpdate(Z)V

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveLeftBarTime:I

    if-le v1, v5, :cond_7

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mSaveLeftBarTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mSaveRightBarTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mSaveUserBarTime:I

    :goto_2
    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/lifevibes/trimapp/Trim;->refreshAllDisplay(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    if-nez v3, :cond_0

    if-le v2, v5, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v3, v2, v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->restoreHandleState(III)V

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v2, v2, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    goto :goto_1

    :cond_7
    move v1, v0

    move v2, v0

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    const/16 v9, 0x5a

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/lifevibes/trimapp/Trim;->isTFT_LCD_Device()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080004

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->setTheme(I)V

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/lifevibes/trimapp/Trim;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v2, "TrimApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Trim APP version name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TrimApp"

    const-string v2, "TrimApp support for android L-version"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v0, v1, v6

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->setTargetProject(I)I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim;->CURRENT_MODEL_TYPE:I

    aget-object v0, v1, v7

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lifevibes/trimapp/Trim;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    const v0, 0x7f070011

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto/16 :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->regPhoneStateListener()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->enableStatusBarOpenByNotification()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mWindowDisplay:Landroid/view/Display;

    iput v5, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    iput v5, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPowerManager:Landroid/os/PowerManager;

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mProgressPercentFormat:Ljava/text/NumberFormat;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mProgressPercentFormat:Ljava/text/NumberFormat;

    invoke-virtual {v0, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    iput v5, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    iput v5, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    sget-boolean v0, Lcom/lifevibes/trimapp/Trim;->SUPPORT_VIDEO_ROTATION:Z

    if-eqz v0, :cond_6

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    if-eqz v0, :cond_6

    :try_start_1
    const-string v1, "android.media.MediaMetadataRetriever"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "METADATA_KEY_VIDEO_ROTATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v1, v6, :cond_7

    iput v9, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    :cond_5
    :goto_2
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    :try_start_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_6
    new-instance v0, Lcom/lifevibes/trimapp/util/Storage;

    invoke-direct {v0}, Lcom/lifevibes/trimapp/util/Storage;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/util/Storage;->setOutDirectory(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/Storage;->checkAvaliableLimitMemory()Z

    move-result v0

    if-nez v0, :cond_9

    const v0, 0x7f070017

    invoke-virtual {p0, v0, v6}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v0, "TrimApp"

    const-string v1, "RuntimeException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto/16 :goto_0

    :cond_7
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v1, v7, :cond_8

    const/16 v1, 0xb4

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    goto :goto_2

    :cond_8
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v1, v8, :cond_5

    const/16 v1, 0x10e

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    goto :goto_2

    :cond_9
    sget-object v0, Lcom/lifevibes/trimapp/util/Storage;->PROJECT_DIR:Ljava/lang/String;

    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v1

    if-nez v1, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.trim/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_b
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v1, :cond_c

    :cond_c
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->setupViews()V

    new-instance v1, Lcom/lifevibes/trimapp/util/MediaShare;

    sget-object v2, Lcom/lifevibes/trimapp/Trim;->CURRENT_MEDIASHARE_INFORMATION:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/lifevibes/trimapp/util/MediaShare;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v1, v0}, Lcom/lifevibes/trimapp/util/MediaShare;->loadLibrary(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    :cond_d
    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/util/MediaShare;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    if-eqz v0, :cond_10

    const v1, 0x40000006    # 2.0000014f

    if-ne v0, v1, :cond_f

    invoke-virtual {p0, v8}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    goto/16 :goto_0

    :cond_10
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-eq v0, v9, :cond_11

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_12

    :cond_11
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    :goto_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v6}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setIsUpdate(Z)V

    goto/16 :goto_0

    :cond_12
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    goto :goto_4

    :catch_2
    move-exception v1

    goto/16 :goto_3

    :catch_3
    move-exception v1

    goto/16 :goto_1

    :catch_4
    move-exception v1

    goto/16 :goto_1

    :catch_5
    move-exception v1

    goto/16 :goto_1

    :catch_6
    move-exception v1

    goto/16 :goto_1

    :catch_7
    move-exception v1

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f090000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v1, 0x7f06001e

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mMenuItemDone:Landroid/view/MenuItem;

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->setMenuItemDoneEnable(Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->unregPhoneStateListener()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDialogCancel(I)V
    .locals 2

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    if-ne v0, v1, :cond_0

    const v0, 0x7f07001c

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->stopTrimming(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPlayPauseAction()V

    goto :goto_0
.end method

.method public onDialogDismess(I)V
    .locals 6

    const/4 v5, 0x1

    const-string v0, "TrimApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** Dialog onDismiss mDialogId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmingSuccess:Z

    if-eqz v0, :cond_1

    const v0, 0x7f070019

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/lifevibes/trimapp/Trim;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/lifevibes/trimapp/Trim;->showToast(Ljava/lang/CharSequence;I)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsConfigChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onDialogMessage(IILjava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v0, -0x2

    const/4 v1, 0x2

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-ne p2, v3, :cond_2

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v1, :cond_1

    iput v4, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPauseAction()Z

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v0, v0, Lcom/lifevibes/trimapp/util/Storage;->OUTPUT_DIR:Ljava/lang/String;

    invoke-virtual {p0, v0, p3}, Lcom/lifevibes/trimapp/Trim;->requestTrimming(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPlayPauseAction()V

    goto :goto_0

    :pswitch_2
    const-string v0, "Unsupported showing"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    goto :goto_0

    :cond_3
    const-string v0, "Distroy Appcition"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :pswitch_3
    if-ne p2, v0, :cond_4

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :cond_4
    if-ne p2, v3, :cond_5

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->trimButtonClicked()V

    goto :goto_0

    :cond_5
    const/4 v0, -0x3

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPlayPauseAction()V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v4}, Lcom/lifevibes/trimapp/Trim;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "RanBefore"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 9

    const/16 v8, 0x10e

    const/16 v7, 0x5a

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_4

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mStorage:Lcom/lifevibes/trimapp/util/Storage;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/util/Storage;->setOutDirectory(Ljava/lang/String;)V

    sget-boolean v0, Lcom/lifevibes/trimapp/Trim;->SUPPORT_VIDEO_ROTATION:Z

    if-eqz v0, :cond_1

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    if-eqz v0, :cond_1

    :try_start_0
    const-string v1, "android.media.MediaMetadataRetriever"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "METADATA_KEY_VIDEO_ROTATION"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v1, v4, :cond_5

    iput v7, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    :cond_0
    :goto_1
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    :cond_1
    iput-boolean v5, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v2, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/trimapp/util/MediaShare;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    if-eqz v0, :cond_8

    const v1, 0x40000006    # 2.0000014f

    if-ne v0, v1, :cond_7

    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    :goto_3
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->clearThumbnailImage(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setStartTime(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setEndTime(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->refreshDisplay()V

    :cond_2
    :goto_4
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_4

    :cond_4
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_4

    :cond_5
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    const/16 v1, 0xb4

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    goto :goto_1

    :cond_6
    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v1, v6, :cond_0

    iput v8, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    goto :goto_1

    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    goto :goto_3

    :cond_8
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-eq v0, v7, :cond_9

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoRotation:I

    if-ne v0, v8, :cond_b

    :cond_9
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    :goto_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0, v4}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setIsUpdate(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setRotation(F)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->arrangeView()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->invalidate()V

    :cond_a
    invoke-virtual {p0, p1}, Lcom/lifevibes/trimapp/Trim;->setIntent(Landroid/content/Intent;)V

    iput-boolean v4, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mWidth:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoWidth(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mHeight:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoHight(I)V

    goto :goto_5

    :catch_0
    move-exception v1

    goto/16 :goto_2

    :catch_1
    move-exception v1

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto/16 :goto_0

    :catch_3
    move-exception v1

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0

    :catch_5
    move-exception v1

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->onBackPressed()V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->trimButtonClicked()V

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->rotateButtonClicked()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f06001c -> :sswitch_2
        0x7f06001d -> :sswitch_0
        0x7f06001e -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPauseAction()Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->clearLongPress(Z)V

    :cond_0
    invoke-virtual {p0, v2, v2}, Lcom/lifevibes/trimapp/Trim;->overridePendingTransition(II)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onRestart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "TrimApp_Cycle"

    const-string v3, "==> onResume"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v3, v3, v4

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_a

    const v0, 0x7f070011

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    :goto_0
    new-instance v3, Lcom/lifevibes/trimapp/Trim$6;

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v5, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v4, v4, v5

    invoke-direct {v3, p0, v4}, Lcom/lifevibes/trimapp/Trim$6;-><init>(Lcom/lifevibes/trimapp/Trim;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mObserver:Landroid/os/FileObserver;

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mObserver:Landroid/os/FileObserver;

    invoke-virtual {v3}, Landroid/os/FileObserver;->startWatching()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mAudioReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/lifevibes/trimapp/Trim;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mIsMultiWindowError:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.VIDEOCAPABILITY"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/Trim;->mVideoCapabilityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/lifevibes/trimapp/Trim;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v3, v4, :cond_5

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mNeedLoopRestart:Z

    :cond_0
    :goto_1
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mRestart:Z

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->updateGrayScale()V

    :cond_2
    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mRefeshdisplay:Z

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/Trim;->mUnsupportedShowing:Z

    if-nez v1, :cond_3

    invoke-static {}, Lcom/lifevibes/trimapp/Trim;->is_MODEL_T_devices()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->showHelpDialog()Z

    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    :cond_4
    return-void

    :cond_5
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_1

    :cond_6
    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mRefeshdisplay:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    if-eqz v3, :cond_8

    :cond_7
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mTrimmedFiles:[Ljava/lang/String;

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mFileCount:I

    aget-object v1, v1, v3

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/Trim;->refreshAllDisplay(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mNewIntent:Z

    goto :goto_2

    :cond_8
    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iget v4, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v3, v4, :cond_9

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_2

    :cond_9
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iput-boolean v1, v3, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_2

    :cond_a
    move v0, v2

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mRefeshdisplay:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    return-void
.end method

.method public onStop()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "TrimApp_Cycle"

    const-string v1, "==> onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v0, v1, :cond_0

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuseAndWait()V

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    const-string v0, "TrimApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged() hasFocus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x1006

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    return-void
.end method

.method public refreshAllDisplay(Ljava/lang/String;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    invoke-virtual {v0, p1, v1}, Lcom/lifevibes/trimapp/util/MediaShare;->getProperties(Ljava/lang/String;Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;)I

    move-result v0

    if-eqz v0, :cond_3

    const v1, 0x40000006    # 2.0000014f

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v0, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_4

    const v0, 0x7f070014

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->returnToInvoker()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->RemoveDialog()V

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_mask_in_count:I

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim;->mSaveLeftBarTime:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim;->mSaveRightBarTime:I

    iput v3, p0, Lcom/lifevibes/trimapp/Trim;->mSaveUserBarTime:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setStartTime(I)V

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I

    if-lez v0, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mTotalDuration:I

    iput v1, v0, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    :cond_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mInputProperties:Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;

    iget v1, v1, Lcom/lifevibes/trimapp/util/MediaShare$MediaShareProperties;->mDurationMillis:I

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setEndTime(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->refreshDisplay()V

    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/Trim;->clearThumbnailImage(I)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->updateButtonsEnabled()V

    iput v2, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailOffset:I

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mNeedLoopRestart:Z

    goto :goto_0
.end method

.method public requestTrimming(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/16 v8, 0x7d1

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v7}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuse(I)V

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f070020

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    invoke-static {v3}, Lcom/lifevibes/trimapp/util/Storage;->addOutputExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    const v0, 0x7f070012

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    :cond_2
    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mFilenameDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mFilenameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mOnTrimmingFile:Ljava/lang/String;

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/Trim;->ShowDialog(I)V

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/Trim;->setProgressDialogValue(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v2, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->getTimeMillis(I)I

    move-result v2

    iget v3, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v3, v3, 0x1

    if-eq v3, v1, :cond_4

    iget v1, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput v8, v1, Landroid/os/Message;->what:I

    iput v0, v1, Landroid/os/Message;->arg1:I

    iput v2, v1, Landroid/os/Message;->arg2:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v3, "TrimApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07000f

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v3, "TrimApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createNewFile()\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f07001e

    invoke-virtual {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->showToast(II)V

    move v0, v1

    goto/16 :goto_2

    :cond_5
    invoke-direct {p0, v0, v2}, Lcom/lifevibes/trimapp/Trim;->doTrimmingAction(II)V

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method

.method public setProgressDialogValue(I)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDialog:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v0, p1}, Lcom/lifevibes/trimapp/widget/TrimDialog;->setProgress(I)V

    return-void
.end method

.method public setVideoViewVisibility(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mIvFullImage:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setupViews()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim;->VIDEOVIEW_WIDTH:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mWindowDisplay:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/lifevibes/trimapp/Trim;->VIDEOVIEW_HEIGHT:I

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->setContentView(I)V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->setupUIComponents()V

    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->setupUIImageViews()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/Trim;->onButtonClick(Landroid/view/View;)V

    return-void
.end method

.method public showToast(II)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public showToast(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->toastMsg:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public startThumbnailTask(I)V
    .locals 6

    const/16 v5, 0x7d0

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x2

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mIsActivityFinishing:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/Trim;->doPauseAction()Z

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    :cond_2
    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/Trim;->releaseDecoderInuse(I)V

    :cond_3
    :goto_1
    and-int/lit8 v0, p1, 0x2

    if-eq v0, v1, :cond_4

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim;->clearThumbnailImage(I)V

    :cond_4
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    if-nez v0, :cond_6

    new-instance v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-ne v0, v4, :cond_3

    iput v1, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    iget-boolean v0, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mIsReleased:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/Trim;->mThumbnail_released:Z

    if-eqz v0, :cond_0

    :cond_7
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v5, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mActionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public updateButtonsEnabled()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->isLongPressing()Z

    move-result v0

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mPreviewState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/Trim;->setMenuItemDoneEnable(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    :goto_1
    invoke-direct {p0}, Lcom/lifevibes/trimapp/Trim;->isModifiedAppValues()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/Trim;->mCurrentDecoderUser:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/Trim;->setMenuItemDoneEnable(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->titleActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/Trim;->setMenuItemDoneEnable(Z)V

    goto :goto_0
.end method

.method public updatePlayPauseButton(Z)V
    .locals 6

    const/high16 v5, 0x7f060000

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setSideHandleEnabled(Z)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setUserHandleMode(I)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setNextFocusDownId(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mPauseButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setSideHandleEnabled(Z)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->isLongPressing()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setUserHandleMode(I)V

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim;->mPlayButton:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->setUserHandleMode(I)V

    goto :goto_1
.end method
