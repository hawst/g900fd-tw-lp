.class Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoubleSeekBarListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    return-void
.end method


# virtual methods
.method public onHandleChanged(IIZ)V
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailOffset:I
    invoke-static {v0, p2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1802(Lcom/lifevibes/trimapp/Trim_Share;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    goto :goto_0
.end method

.method public onLongPressed(I)V
    .locals 3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->clearAllImageViewFromLinear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnailsZoom:[Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1300(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, 0x3

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1402(Lcom/lifevibes/trimapp/Trim_Share;I)I

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->startThumbnailTask(I)V

    return-void
.end method

.method public onLongReleased(I)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->clearAllImageViewFromLinear()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mNumberOfImages:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1200(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim_Share;->access$1100(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIvThumbnails:[Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim_Share;->access$1500(Lcom/lifevibes/trimapp/Trim_Share;)[Landroid/widget/ImageView;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    iget v0, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1600(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;

    move-result-object v0

    iput v1, v0, Lcom/lifevibes/trimapp/Trim_Share$ThumbnailTask;->mCurrentMask:I

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/16 v1, -0x9

    # &= operator for: Lcom/lifevibes/trimapp/Trim_Share;->mThumbnailMask:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1772(Lcom/lifevibes/trimapp/Trim_Share;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1400(Lcom/lifevibes/trimapp/Trim_Share;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, 0x4

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mPreviewState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$1402(Lcom/lifevibes/trimapp/Trim_Share;I)I

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim_Share;->updateButtonsEnabled()V

    return-void
.end method

.method public onProgressChanged(IZ)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->seekTo(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, 0x1

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2002(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public onStopTrackingTouch()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/Trim_Share;->mDragging:Z
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2002(Lcom/lifevibes/trimapp/Trim_Share;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->setVideoProgress()I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2200(Lcom/lifevibes/trimapp/Trim_Share;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mVideoView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$500(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2100(Lcom/lifevibes/trimapp/Trim_Share;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
