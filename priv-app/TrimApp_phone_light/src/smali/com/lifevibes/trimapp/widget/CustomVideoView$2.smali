.class Lcom/lifevibes/trimapp/widget/CustomVideoView$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/widget/CustomVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    const/4 v2, 0x5

    const-string v0, "CustomVideoView"

    const-string v1, "OnCompletionListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$702(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I
    invoke-static {v0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$1002(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$1100(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$1100(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$900(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_0
    return-void
.end method
