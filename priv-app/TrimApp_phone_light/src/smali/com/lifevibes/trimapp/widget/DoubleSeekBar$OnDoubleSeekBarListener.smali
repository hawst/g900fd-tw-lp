.class public interface abstract Lcom/lifevibes/trimapp/widget/DoubleSeekBar$OnDoubleSeekBarListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDoubleSeekBarListener"
.end annotation


# virtual methods
.method public abstract onHandleChanged(IIZ)V
.end method

.method public abstract onLongPressed(I)V
.end method

.method public abstract onLongReleased(I)V
.end method

.method public abstract onProgressChanged(IZ)V
.end method

.method public abstract onStartTrackingTouch()V
.end method

.method public abstract onStopTrackingTouch()V
.end method
