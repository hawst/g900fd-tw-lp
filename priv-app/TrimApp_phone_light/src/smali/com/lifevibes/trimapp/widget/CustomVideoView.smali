.class public Lcom/lifevibes/trimapp/widget/CustomVideoView;
.super Landroid/view/TextureView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;,
        Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;
    }
.end annotation


# static fields
.field public static final STATE_ERROR:I = -0x1

.field public static final STATE_IDLE:I = 0x0

.field public static final STATE_PAUSED:I = 0x4

.field public static final STATE_PLAYBACK_COMPLETED:I = 0x5

.field public static final STATE_PLAYING:I = 0x3

.field public static final STATE_PREPARED:I = 0x2

.field public static final STATE_PREPARING:I = 0x1


# instance fields
.field private final LOG_VIEW:Z

.field private final TAG:Ljava/lang/String;

.field private isUpdate:Z

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mImageView:Landroid/widget/ImageView;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mSizeChangeedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSurfaceHeight:I

.field private mSurfaceListener:Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;

.field private mSurfaceWidth:I

.field private mTargetState:I

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private tvListner:Landroid/view/TextureView$SurfaceTextureListener;

.field private tvSurface:Landroid/graphics/SurfaceTexture;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    const-string v0, "CustomVideoView"

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->LOG_VIEW:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$3;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$3;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$4;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$4;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSizeChangeedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->initVideoView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "CustomVideoView"

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->LOG_VIEW:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$2;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$3;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$3;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$4;

    invoke-direct {v0, p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView$4;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSizeChangeedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->initVideoView()V

    return-void
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/graphics/SurfaceTexture;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    return v0
.end method

.method static synthetic access$1002(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    return p1
.end method

.method static synthetic access$102(Lcom/lifevibes/trimapp/widget/CustomVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$300(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$302(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$402(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    return p1
.end method

.method static synthetic access$502(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceListener:Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;

    return-object v0
.end method

.method static synthetic access$702(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    return p1
.end method

.method static synthetic access$800(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method private initVideoView()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "CustomVideoView"

    const-string v1, "initVideoView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVisibility(I)V

    new-instance v0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;Lcom/lifevibes/trimapp/widget/CustomVideoView$1;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvListner:Landroid/view/TextureView$SurfaceTextureListener;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvListner:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    return-void
.end method

.method private isInPlaybackState()Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "CustomVideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isInPlaybackState mCurrentState= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private release(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string v0, "CustomVideoView"

    const-string v1, "release"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    invoke-virtual {p0, v3}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setTag(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    :cond_0
    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setKeepScreenOn(Z)V

    return-void
.end method


# virtual methods
.method public arrangeView()V
    .locals 6

    const/16 v5, 0x11

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getRotation()F

    move-result v0

    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    :goto_0
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    mul-int/2addr v3, v0

    iget v4, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    mul-int/2addr v4, v1

    if-le v3, v4, :cond_4

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    div-int/2addr v0, v1

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_1
    :goto_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v0, v1, v3, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->invalidate()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v1, v3, v2, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    :cond_2
    return-void

    :cond_3
    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    mul-int/2addr v3, v0

    iget v4, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    mul-int/2addr v1, v4

    if-ge v3, v1, :cond_1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    div-int/2addr v0, v1

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_1
.end method

.method public getCurrentPosition()I
    .locals 1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentVideoState()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    return v0
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0
.end method

.method public getImageView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getTragetVideoState()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    return v0
.end method

.method public getVideoViewHight()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    return v0
.end method

.method public getVideoViewWidth()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/TextureView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    sget v0, Lcom/lifevibes/trimapp/Trim;->CURRENT_MODEL_TYPE:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    sget v0, Lcom/lifevibes/trimapp/Trim;->CURRENT_MODEL_TYPE:I

    const v1, 0x40000002    # 2.0000005f

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setIsUpdate(Z)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    if-ne v0, v2, :cond_0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    iput p2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->arrangeView()V

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/TextureView;->onSizeChanged(IIII)V

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    if-ne v0, v2, :cond_2

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    iput p2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    :goto_1
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->arrangeView()V

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    goto :goto_0

    :cond_1
    iput p3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I

    iput p4, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I

    goto :goto_1

    :cond_2
    const-string v0, "CustomVideoView"

    const-string v1, "Nomal size change!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onVisibilityChanged(Landroid/view/View;I)V

    return-void
.end method

.method public openVideo()V
    .locals 4

    const/4 v3, -0x1

    const-string v0, "CustomVideoView"

    const-string v1, "openVideo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->release(Z)V

    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Landroid/view/Surface;

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;

    invoke-direct {v1, v2}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    :catch_0
    move-exception v0

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    const/4 v2, 0x4

    const-string v0, "CustomVideoView"

    const-string v1, "pause 1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CustomVideoView"

    const-string v1, "pause 2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CustomVideoView"

    const-string v1, "pause 3"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    :cond_0
    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setKeepScreenOn(Z)V

    return-void
.end method

.method public seekTo(I)V
    .locals 2

    const-string v0, "CustomVideoView"

    const-string v1, "seekTo 1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CustomVideoView"

    const-string v1, "seekTo 2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method

.method public setImageView(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    return-void
.end method

.method public setIsUpdate(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isUpdate:Z

    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setOnSurfaceListener(Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceListener:Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;

    return-void
.end method

.method public setRotation(F)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setRotation(F)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/TextureView;->setRotation(F)V

    return-void
.end method

.method public setVideoHight(I)V
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->openVideo()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->invalidate()V

    return-void
.end method

.method public setVideoWidth(I)V
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I

    return-void
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x3

    const-string v0, "CustomVideoView"

    const-string v1, "start 1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CustomVideoView"

    const-string v1, "start 2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    :cond_0
    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->setKeepScreenOn(Z)V

    return-void
.end method

.method public stopPlayback()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "CustomVideoView"

    const-string v1, "stopPlayback 1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const-string v0, "CustomVideoView"

    const-string v1, "stopPlayback 2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I

    iput v2, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I

    :cond_0
    return-void
.end method
