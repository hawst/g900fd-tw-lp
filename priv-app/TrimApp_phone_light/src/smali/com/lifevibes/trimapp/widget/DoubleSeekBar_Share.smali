.class public Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;
.super Landroid/view/ViewGroup;

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$1;,
        Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;,
        Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;
    }
.end annotation


# static fields
.field public static final HANDLE_ID_LEFT:I = 0x1

.field public static final HANDLE_ID_NONE:I = 0x0

.field public static final HANDLE_ID_RIGHT:I = 0x2

.field public static final HANDLE_ID_USER:I = 0x4

.field public static final LOG_VIEW:Z = false

.field public static final LONG_PRESS_DURATION:I = 0x3a98

.field public static final MAX_PROMIMITY:I = 0x14

.field public static final TAG:Ljava/lang/String; = "TrimApp_DSB"

.field public static final USER_HANDLE_DISABLE:I = 0x3

.field public static final USER_HANDLE_NORMAL:I = 0x1

.field public static final USER_HANDLE_PROGRESS:I = 0x2


# instance fields
.field private HANDLE_HEIGHT:I

.field private HANDLE_LEFT_X:I

.field private HANDLE_MARGIN_T:I

.field private HANDLE_PADDING:I

.field private HANDLE_PADDING_SPACE:I

.field private HANDLE_RIGHT_X:I

.field private HANDLE_USER_HEIGHT:I

.field private HANDLE_USER_MARGIN_T:I

.field private HANDLE_USER_WIDTH:I

.field private HANDLE_USER_WIDTH_HALF:I

.field private HANDLE_WIDTH:I

.field private HANDLE_WIDTH_HALF:I

.field private IMAGE_CONTAINER_EMPTY:I

.field private IMAGE_CONTAINER_HEIGHT:I

.field private IMAGE_CONTAINER_MARGIN_LR:I

.field private IMAGE_CONTAINER_MARGIN_T:I

.field private IMAGE_CONTAINER_PADDING:I

.field private IMAGE_HEIGHT:I

.field private IMAGE_WIDTH:I

.field private LAYOUT_HANDLE_LEFT_X:I

.field private LAYOUT_HANDLE_RIGHT_X:I

.field private LAYOUT_HANDLE_USER_X:I

.field public LAYOUT_HEIGHT:I

.field private final LONGPRESS_DELAY_OFFSET:I

.field private TIME_HEIGHT:I

.field private TIME_MARGIN_LR:I

.field private TIME_MARGIN_T:I

.field private TIME_WIDTH:I

.field private mCanvas:Landroid/graphics/Canvas;

.field private mChangeTimeToPos:I

.field private final mContext:Landroid/content/Context;

.field private mEndTime:I

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mHandleGap:I

.field private mHasPerformedLongPress:Z

.field private mInLongPress:Z

.field private mInitGrayScale:Ljava/lang/Boolean;

.field private mIvLeftHandle:Landroid/widget/ImageView;

.field private mIvRightHandle:Landroid/widget/ImageView;

.field private mIvUserHandle:Landroid/widget/ImageView;

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

.field private mLeftFrameLayout:Landroid/widget/FrameLayout;

.field private mLeftPos:I

.field private mLeftTime:I

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

.field private mLongDuration:I

.field private final mLongPressHandler:Landroid/os/Handler;

.field private mPaint:Landroid/graphics/Paint;

.field private mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

.field private mPressedHandle:I

.field private mRestoreCurrentTime:Z

.field private mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

.field private mRightFrameLayout:Landroid/widget/FrameLayout;

.field private mRightPos:I

.field private mRightTime:I

.field private mSaveEndTime:I

.field private mSaveLayoutLeft:I

.field private mSaveStartTime:I

.field private mSaveUnhandleTime:I

.field private mStartTime:I

.field private mTouchSlop:I

.field private mTvLeftTime:Landroid/widget/TextView;

.field private final mTvPlayTime:Landroid/widget/TextView;

.field private mTvRightTime:Landroid/widget/TextView;

.field private mUpdateTimeField:I

.field private mUpdateTimeText:I

.field private mUserHandleMode:I

.field private mUserPos:I

.field private mUserTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_LEFT_X:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_RIGHT_X:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_USER_X:I

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvPlayTime:Landroid/widget/TextView;

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRestoreCurrentTime:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInitGrayScale:Ljava/lang/Boolean;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mCanvas:Landroid/graphics/Canvas;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

    iput v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionX:F

    iput v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionY:F

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTouchSlop:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongPressHandler:Landroid/os/Handler;

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LONGPRESS_DELAY_OFFSET:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeField:I

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setupViewUIAttrs(Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->initDoubleSeekBar()V

    return-void
.end method

.method static synthetic access$002(Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHasPerformedLongPress:Z

    return p1
.end method

.method private endLongPress(Z)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v1, v2, :cond_3

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveLayoutLeft:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosByTime(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveUnhandleTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v3, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    :goto_1
    invoke-virtual {p0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    invoke-interface {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onLongReleased(I)V

    :cond_2
    const/4 v0, 0x7

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v1, v3, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveLayoutLeft:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosByTime(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveUnhandleTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v2, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    goto :goto_1
.end method

.method private getPosByTime(I)I
    .locals 6

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    sub-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    :cond_0
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    sub-int v1, p1, v1

    int-to-long v2, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-long v4, v1

    mul-long/2addr v2, v4

    long-to-float v1, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    :cond_1
    return v0
.end method

.method private getValidLeftPosition(II)I
    .locals 3

    const/4 v1, 0x2

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return p2

    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    if-gez p2, :cond_1

    const/4 p2, 0x0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    sub-int/2addr v1, v2

    if-le p2, v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    sub-int p2, v0, v1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    if-le p2, v1, :cond_2

    iget p2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    add-int/2addr v1, v2

    if-ge p2, v1, :cond_0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    add-int p2, v0, v1

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v1

    if-ge p2, v0, :cond_4

    move p2, v0

    :cond_3
    :goto_1
    invoke-virtual {p0, p2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosToTimeMillis(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    if-le v0, v1, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    goto :goto_0

    :cond_4
    if-le p2, v1, :cond_3

    move p2, v1

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private layoutGrayScale(II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInitGrayScale:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mCanvas:Landroid/graphics/Canvas;

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    invoke-virtual {v3, v6}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v4, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v4, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v7}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

    new-instance v2, Landroid/graphics/drawable/ClipDrawable;

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4, v7}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_HEIGHT:I

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_HEIGHT:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v5, v2

    invoke-direct {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInitGrayScale:Ljava/lang/Boolean;

    :cond_1
    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

    if-nez v2, :cond_3

    :cond_2
    const-string v0, "TrimApp_DSB"

    const-string v1, "layoutGrayScale null exception!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0, v6, v6, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLeft()I

    move-result v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v2

    sub-int/2addr v0, v2

    if-gez v0, :cond_4

    move v0, v1

    :cond_4
    mul-int/lit16 v0, v0, 0x2710

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int v2, v0, v2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v0

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    add-int/2addr v3, v4

    sub-int/2addr v0, v3

    if-gez v0, :cond_5

    :goto_1
    mul-int/lit16 v0, v1, 0x2710

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    div-int/2addr v0, v1

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameClip:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameClip:Landroid/graphics/drawable/ClipDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V

    goto/16 :goto_0

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method private layoutHandles(IIII)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_RIGHT_X:I

    sub-int v0, p3, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_LEFT_X:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    int-to-long v0, v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    xor-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    int-to-long v0, v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    xor-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    :cond_2
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    int-to-long v0, v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    xor-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    :cond_3
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_LEFT_X:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_LEFT_X:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v5, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_LEFT_X:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    add-int/2addr v1, v0

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_MARGIN_T:I

    add-int/2addr v2, p2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_HEIGHT:I

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/ImageView;->layout(IIII)V

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_RIGHT_X:I

    sub-int v0, p3, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_RIGHT_X:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v6, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_RIGHT_X:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    sub-int/2addr v1, v4

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    add-int/2addr v1, v0

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/ImageView;->layout(IIII)V

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_EMPTY:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH_HALF:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_USER_X:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    invoke-direct {p0, v7, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HANDLE_USER_X:I

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH:I

    add-int/2addr v2, v1

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_MARGIN_T:I

    add-int/2addr v3, p2

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_HEIGHT:I

    add-int/2addr v4, v3

    iget-object v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    invoke-virtual {v5, v1, v3, v2, v4}, Landroid/widget/ImageView;->layout(IIII)V

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_4
    invoke-direct {p0, p1, p3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->layoutGrayScale(II)V

    return-void
.end method

.method private layoutImageContainer(IIII)V
    .locals 5

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_LR:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_LR:I

    sub-int v1, p3, v1

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_T:I

    add-int/2addr v2, p2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_HEIGHT:I

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    invoke-direct {p0, v2, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->layoutLinearHorizontal(II)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/FrameLayout;->layout(IIII)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/FrameLayout;->layout(IIII)V

    return-void
.end method

.method private layoutLinearHorizontal(II)V
    .locals 9

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_PADDING:I

    sub-int v2, p2, p1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v8, v0

    move v0, v1

    move v1, v8

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_1

    add-int/lit8 v0, v0, 0x0

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_HEIGHT:I

    sub-int v5, v2, v5

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_WIDTH:I

    add-int/2addr v6, v0

    iget v7, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_HEIGHT:I

    add-int/2addr v7, v5

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_WIDTH:I

    add-int/2addr v0, v4

    goto :goto_1

    :cond_2
    return-void
.end method

.method private layoutTimeText(IIII)V
    .locals 5

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_LR:I

    add-int/2addr v0, p1

    div-int/lit8 v1, p3, 0x2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_T:I

    add-int/2addr v2, p2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_HEIGHT:I

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/TextView;->layout(IIII)V

    div-int/lit8 v0, p3, 0x2

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_LR:I

    sub-int v1, p3, v1

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_T:I

    add-int/2addr v2, p2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_HEIGHT:I

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    invoke-virtual {v4, v0, v2, v1, v3}, Landroid/widget/TextView;->layout(IIII)V

    return-void
.end method

.method private onStartTrackingTouch()V
    .locals 1

    const/4 v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onStartTrackingTouch()V

    :cond_0
    return-void
.end method

.method private onStopTrackingTouch()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    invoke-interface {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onStopTrackingTouch()V

    :cond_0
    return-void
.end method

.method private postCheckForLongClick(I)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHasPerformedLongPress:Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    if-nez v0, :cond_0

    new-instance v0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;-><init>(Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$1;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongPressHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    add-int/2addr v2, p1

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private removeLongPressCallback()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongPressHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPendingCheckForLongPress:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$CheckForLongPress;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private setupUIComponents()V
    .locals 8

    const/4 v7, 0x0

    const v6, 0x7f030004

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    if-nez v1, :cond_1

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    if-nez v1, :cond_2

    invoke-virtual {v0, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameLayout:Landroid/widget/FrameLayout;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameLayout:Landroid/widget/FrameLayout;

    if-nez v0, :cond_4

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    const v1, 0x7f020018

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_5
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    if-nez v0, :cond_6

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForHandleDescription(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_6
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    if-nez v0, :cond_7

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-direct {p0, v4}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForHandleDescription(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->addView(Landroid/view/View;)V

    :cond_7
    return-void
.end method

.method private setupViewUIAttrs(Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/lifevibes/trimapp/R$styleable;->DoubleSeekbarStyleable:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_HEIGHT:I

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_MARGIN_T:I

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING_SPACE:I

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_HEIGHT:I

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_MARGIN_T:I

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH:I

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_HEIGHT:I

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_LR:I

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_T:I

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_PADDING:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_HEIGHT:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_WIDTH:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_HEIGHT:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_WIDTH:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_LR:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->TIME_MARGIN_T:I

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HEIGHT:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_LR:I

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_PADDING:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_EMPTY:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_EMPTY:I

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_LEFT_X:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_EMPTY:I

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_PADDING:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_RIGHT_X:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH_HALF:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method private startLongPress()V
    .locals 10

    const/4 v9, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v7, 0x2

    const/4 v6, 0x1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    const/16 v0, 0x3a98

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    int-to-long v2, v0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    int-to-long v4, v0

    mul-long/2addr v2, v4

    long-to-float v0, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    add-float/2addr v0, v8

    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveLayoutLeft:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveUnhandleTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    :goto_1
    iput-boolean v6, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInitGrayScale:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    invoke-interface {v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onLongPressed(I)V

    :cond_1
    const/4 v0, 0x7

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUpdateTimeField(I)V

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    int-to-long v2, v0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    int-to-long v4, v0

    mul-long/2addr v2, v4

    long-to-float v0, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    add-float/2addr v0, v8

    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosition(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveLayoutLeft:I

    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveUnhandleTime:I

    iput v9, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private stringForHandleDescription(I)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private stringForPlayTime(I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    if-gez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    div-int/lit16 v0, p1, 0x3e8

    rem-int/lit8 v1, v0, 0x3c

    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    div-int/lit16 v0, v0, 0xe10

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mFormatter:Ljava/util/Formatter;

    const-string v4, "%02d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v0

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateTimeTextAndSendListener(IZ)V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v6, 0x4

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    if-ne v3, v7, :cond_2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v3, v6, :cond_2

    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v4, v6, :cond_1

    :goto_0
    invoke-interface {v3, v2, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onProgressChanged(IZ)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    and-int/lit8 v3, p1, 0x1

    if-eq v3, v0, :cond_3

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeField:I

    if-ne v3, v0, :cond_a

    :cond_3
    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v3

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForHandleDescription(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-eq v4, v0, :cond_4

    iget-boolean v4, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRestoreCurrentTime:Z

    if-ne v4, v0, :cond_a

    :cond_4
    iput-boolean v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRestoreCurrentTime:Z

    move v1, v3

    :goto_2
    and-int/lit8 v0, p1, 0x2

    if-ne v0, v7, :cond_9

    invoke-virtual {p0, v7}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v0

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvRightTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForHandleDescription(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v3, v7, :cond_9

    :goto_3
    and-int/lit8 v1, p1, 0x4

    if-eq v1, v6, :cond_5

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeField:I

    if-ne v1, v6, :cond_8

    :cond_5
    invoke-virtual {p0, v6}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getTimeMillis(I)I

    move-result v1

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    if-eq v3, v7, :cond_6

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-eq v3, v6, :cond_6

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeField:I

    if-ne v3, v6, :cond_7

    :cond_6
    iget-object v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTvLeftTime:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->stringForTime(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-ne v3, v6, :cond_8

    move v0, v1

    :cond_8
    if-le v0, v2, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    invoke-interface {v1, v2, v0, p2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;->onHandleChanged(IIZ)V

    goto :goto_1

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public addImageViewToLinear(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public clearAllImageViewFromLinear()V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public clearLongPress(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->endLongPress(Z)V

    return-void
.end method

.method public getDisplayedNumberOfImages(I)I
    .locals 2

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_CONTAINER_MARGIN_LR:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_WIDTH:I

    div-int/2addr v0, v1

    return v0
.end method

.method public getDuration()I
    .locals 3

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    if-ge v0, v2, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    :cond_0
    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    :cond_1
    sub-int v0, v1, v0

    return v0
.end method

.method public getEndTime()I
    .locals 2

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveEndTime:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    goto :goto_0
.end method

.method public getImageHeight()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_HEIGHT:I

    return v0
.end method

.method public getImageWidth()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->IMAGE_WIDTH:I

    return v0
.end method

.method public getPosToTimeMillis(I)I
    .locals 6

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    sub-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLongDuration:I

    :cond_0
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    int-to-long v2, p1

    int-to-long v4, v0

    mul-long/2addr v2, v4

    long-to-float v0, v2

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHandleGap:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v0, v2

    float-to-int v0, v0

    add-int/2addr v0, v1

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    :cond_1
    return v0
.end method

.method public getPosition(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getStartTime()I
    .locals 2

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mSaveStartTime:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    goto :goto_0
.end method

.method public getTimeMillis(I)I
    .locals 2

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    :cond_0
    return v0

    :pswitch_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public initDoubleSeekBar()V
    .locals 3

    const v1, 0x7fffffff

    const/4 v0, 0x0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRestoreCurrentTime:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mFormatBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mFormatter:Ljava/util/Formatter;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTouchSlop:I

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setupUIComponents()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    return-void
.end method

.method public isLongPressing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    return v0
.end method

.method public moveUserHandleTo(IZ)V
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    if-ge p1, v0, :cond_1

    iget p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    :cond_0
    :goto_0
    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    if-eqz p2, :cond_2

    const/4 v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :goto_1
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    return-void

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    if-le p1, v0, :cond_0

    iget p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3

    const/4 v2, 0x1

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/high16 v1, 0x7f060000

    if-ne v0, v1, :cond_1

    iput v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f060001

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->endLongPress(Z)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7

    const/4 v6, 0x2

    const/4 v0, 0x0

    const v5, 0x7f060001

    const/high16 v4, 0x7f060000

    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq v3, v4, :cond_2

    if-ne v3, v5, :cond_0

    :cond_2
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    if-ne v3, v4, :cond_4

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    add-int/lit8 v0, v0, -0x5

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    move v0, v1

    goto :goto_0

    :cond_4
    if-ne v3, v5, :cond_3

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    add-int/lit8 v0, v0, -0x5

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v6, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    goto :goto_1

    :sswitch_1
    if-ne v3, v4, :cond_6

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    add-int/lit8 v0, v0, 0x5

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v1, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    move v0, v1

    goto :goto_0

    :cond_6
    if-ne v3, v5, :cond_5

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    add-int/lit8 v0, v0, 0x5

    iget v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v6, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    goto :goto_2

    :sswitch_2
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->startLongPress()V

    :goto_3
    move v0, v1

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->endLongPress(Z)V

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    const/4 v0, 0x7

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->layoutTimeText(IIII)V

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->layoutImageContainer(IIII)V

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->layoutHandles(IIII)V

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_1
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    invoke-direct {p0, v0, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->updateTimeTextAndSendListener(IZ)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->LAYOUT_HEIGHT:I

    invoke-virtual {p0, v0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    const/4 v10, 0x2

    const v9, 0x7f060001

    const/high16 v8, 0x7f060000

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    if-eq v4, v8, :cond_0

    if-ne v4, v9, :cond_4

    :cond_0
    move v2, v1

    :goto_0
    and-int/lit16 v5, v3, 0xff

    packed-switch v5, :pswitch_data_0

    :cond_1
    move v1, v0

    :goto_1
    if-eqz v2, :cond_2

    packed-switch v3, :pswitch_data_1

    :cond_2
    :goto_2
    move v0, v1

    :cond_3
    :goto_3
    return v0

    :cond_4
    const v2, 0x7f060002

    if-ne v4, v2, :cond_5

    move v2, v0

    goto :goto_0

    :cond_5
    invoke-direct {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->endLongPress(Z)V

    goto :goto_3

    :pswitch_0
    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    if-nez v5, :cond_3

    if-ne v4, v8, :cond_6

    iput v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    iget-object v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setPressed(Z)V

    invoke-virtual {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUpdateTimeField(I)V

    goto :goto_1

    :cond_6
    if-ne v4, v9, :cond_7

    iput v10, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    iget-object v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setPressed(Z)V

    goto :goto_1

    :cond_7
    const v5, 0x7f060002

    if-ne v4, v5, :cond_1

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    if-ne v5, v1, :cond_9

    const/4 v5, 0x4

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    :cond_8
    :goto_4
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUpdateTimeField(I)V

    goto :goto_1

    :cond_9
    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    if-ne v5, v10, :cond_8

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->onStartTrackingTouch()V

    goto :goto_4

    :pswitch_1
    const v5, 0x7f060002

    if-ne v4, v5, :cond_b

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget v7, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_USER_WIDTH_HALF:I

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    :cond_a
    :goto_5
    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    goto :goto_1

    :cond_b
    if-ne v4, v8, :cond_c

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget v7, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v1, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-virtual {p0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosToTimeMillis(I)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v6, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    sub-int/2addr v5, v6

    const/16 v6, 0x3a98

    if-le v5, v6, :cond_a

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    add-int/lit16 v5, v5, 0x3a98

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    invoke-direct {p0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosByTime(I)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v10, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    goto :goto_5

    :cond_c
    if-ne v4, v9, :cond_a

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget v7, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->HANDLE_WIDTH_HALF:I

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-direct {p0, v10, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    invoke-virtual {p0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosToTimeMillis(I)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v6, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    sub-int/2addr v5, v6

    const/16 v6, 0x3a98

    if-le v5, v6, :cond_a

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    add-int/lit16 v5, v5, -0x3a98

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    invoke-direct {p0, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getPosByTime(I)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    invoke-direct {p0, v1, v5}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->getValidLeftPosition(II)I

    move-result v5

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    goto/16 :goto_5

    :pswitch_2
    if-eq v4, v8, :cond_d

    if-ne v4, v9, :cond_f

    :cond_d
    invoke-direct {p0, v1}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->endLongPress(Z)V

    iget-object v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setPressed(Z)V

    iget-object v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setPressed(Z)V

    :cond_e
    :goto_6
    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mPressedHandle:I

    goto/16 :goto_1

    :cond_f
    const v5, 0x7f060002

    if-ne v4, v5, :cond_e

    iget v5, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    if-ne v5, v10, :cond_e

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->onStopTrackingTouch()V

    goto :goto_6

    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionY:F

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHasPerformedLongPress:Z

    if-ne v4, v8, :cond_11

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    int-to-float v0, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionX:F

    :cond_10
    :goto_7
    const/16 v0, 0x1f4

    invoke-direct {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->postCheckForLongClick(I)V

    goto/16 :goto_2

    :cond_11
    if-ne v4, v9, :cond_10

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    int-to-float v0, v0

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionX:F

    goto :goto_7

    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    if-ne v4, v8, :cond_14

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftPos:I

    int-to-float v0, v0

    :cond_12
    :goto_8
    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionX:F

    sub-float v0, v3, v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLastMotionY:F

    sub-float v2, v3, v2

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTouchSlop:I

    if-ge v0, v3, :cond_13

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mTouchSlop:I

    if-lt v2, v0, :cond_2

    :cond_13
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHasPerformedLongPress:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->removeLongPressCallback()V

    goto/16 :goto_2

    :cond_14
    if-ne v4, v9, :cond_12

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightPos:I

    int-to-float v0, v0

    goto :goto_8

    :pswitch_5
    iget-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mHasPerformedLongPress:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->removeLongPressCallback()V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public performLongClick()Z
    .locals 1

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->startLongPress()V

    const/4 v0, 0x1

    return v0
.end method

.method public refreshDisplay()V
    .locals 4

    const/4 v3, 0x7

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInLongPress:Z

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    add-int/lit16 v0, v0, 0x3a98

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iget v1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    :cond_0
    iget v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    iput v3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeText:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->setUserHandleMode(I)V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mInitGrayScale:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->requestLayout()V

    return-void
.end method

.method public restoreHandleState(III)V
    .locals 1

    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mLeftTime:I

    iput p2, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRightTime:I

    iput p3, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserTime:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mRestoreCurrentTime:Z

    const/4 v0, 0x7

    iput v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mChangeTimeToPos:I

    return-void
.end method

.method public setEndTime(I)V
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mEndTime:I

    return-void
.end method

.method public setNextFocusDownId(II)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setNextFocusUpId(II)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvLeftHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvRightHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnDoubleSeekBarListener(Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mListener:Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share$OnDoubleSeekBar_Listener;

    return-void
.end method

.method public setStartTime(I)V
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mStartTime:I

    return-void
.end method

.method public setUpdateTimeField(I)V
    .locals 0

    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUpdateTimeField:I

    return-void
.end method

.method public setUserHandleMode(I)V
    .locals 2

    iput p1, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mUserHandleMode:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/DoubleSeekBar_Share;->mIvUserHandle:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
