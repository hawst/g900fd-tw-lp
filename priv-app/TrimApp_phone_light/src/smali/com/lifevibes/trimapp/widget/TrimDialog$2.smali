.class Lcom/lifevibes/trimapp/widget/TrimDialog$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lifevibes/trimapp/widget/TrimDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/widget/TrimDialog;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/TrimDialog$2;->this$0:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog$2;->this$0:Lcom/lifevibes/trimapp/widget/TrimDialog;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog$2;->this$0:Lcom/lifevibes/trimapp/widget/TrimDialog;

    # getter for: Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->access$100(Lcom/lifevibes/trimapp/widget/TrimDialog;)Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/lifevibes/trimapp/widget/TrimDialog$2;->this$0:Lcom/lifevibes/trimapp/widget/TrimDialog;

    # getter for: Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/lifevibes/trimapp/widget/TrimDialog;->access$000(Lcom/lifevibes/trimapp/widget/TrimDialog;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, p2, v2}, Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;->onDialogMessage(IILjava/lang/String;)V

    return-void
.end method
