.class Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/widget/CustomVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;Lcom/lifevibes/trimapp/widget/CustomVideoView$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;-><init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;
    invoke-static {v0, p1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$102(Lcom/lifevibes/trimapp/widget/CustomVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$200(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$300(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceWidth:I
    invoke-static {v0, p2}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$402(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceHeight:I
    invoke-static {v0, p3}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$502(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    :cond_1
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->tvSurface:Landroid/graphics/SurfaceTexture;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$100(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceListener:Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$600(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$TextureListener;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mSurfaceListener:Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$600(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/lifevibes/trimapp/widget/CustomVideoView$OnSurfaceListener;->onSurfaceChanged(II)V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    return-void
.end method
