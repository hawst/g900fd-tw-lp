.class Lcom/lifevibes/trimapp/widget/CustomVideoView$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/widget/CustomVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;


# direct methods
.method constructor <init>(Lcom/lifevibes/trimapp/widget/CustomVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    const-string v0, "CustomVideoView"

    const-string v1, "onPrepared"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    const/4 v1, 0x2

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mCurrentState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$702(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$800(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$800(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$900(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoWidth:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$202(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    # setter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mVideoHeight:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$302(Lcom/lifevibes/trimapp/widget/CustomVideoView;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mTargetState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$1000(Lcom/lifevibes/trimapp/widget/CustomVideoView;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const-string v0, "CustomVideoView"

    const-string v1, "start play in onPrepared!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/CustomVideoView$1;->this$0:Lcom/lifevibes/trimapp/widget/CustomVideoView;

    # getter for: Lcom/lifevibes/trimapp/widget/CustomVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->access$900(Lcom/lifevibes/trimapp/widget/CustomVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :cond_1
    return-void
.end method
