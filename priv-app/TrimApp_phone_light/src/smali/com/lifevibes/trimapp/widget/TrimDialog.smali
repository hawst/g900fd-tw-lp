.class public Lcom/lifevibes/trimapp/widget/TrimDialog;
.super Landroid/app/DialogFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lifevibes/trimapp/widget/TrimDialog$FilenameLengthFilter;,
        Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;
    }
.end annotation


# static fields
.field public static final DIALOG_EXIT_APP:I = 0x5

.field public static final DIALOG_FILENAME:I = 0x1

.field public static final DIALOG_HELP:I = 0x6

.field public static final DIALOG_MESSAGE_DISTROY_APP:Ljava/lang/String; = "Distroy Appcition"

.field public static final DIALOG_MESSAGE_UNSUPPORTED_SHOWING:Ljava/lang/String; = "Unsupported showing"

.field public static final DIALOG_PROGRESS:I = 0x2

.field public static final DIALOG_UNSUPPORTED_FILESIZE:I = 0x3

.field public static final DIALOG_UNSUPPORTED_TYPE:I = 0x4

.field static final MAX_FILENAME_LENGTH:I = 0x32

.field static final mInvalidFileNameChar:[Ljava/lang/String;


# instance fields
.field private final TAG:Ljava/lang/String;

.field mDialog:Landroid/app/Dialog;

.field private mDialogId:I

.field private mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

.field private mEditTextName:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mInvalidFileNameChar:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const-string v0, "TrimDialog"

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/lifevibes/trimapp/widget/TrimDialog;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/lifevibes/trimapp/widget/TrimDialog;)Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;
    .locals 1

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    return-object v0
.end method

.method private getEditTextFilter()[Landroid/text/InputFilter;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/lifevibes/trimapp/widget/TrimDialog$FilenameLengthFilter;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$FilenameLengthFilter;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/lifevibes/trimapp/widget/TrimDialog$10;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$10;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    aput-object v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    iget v1, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogId:I

    invoke-interface {v0, v1}, Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;->onDialogDismess(I)V

    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V

    return-void
.end method

.method public getDialogId()I
    .locals 1

    iget v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogId:I

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    check-cast p1, Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    iput-object p1, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    iget v1, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogId:I

    invoke-interface {v0, v1}, Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;->onDialogCancel(I)V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    const v8, 0x104000a

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/high16 v5, 0x1040000

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "TrimDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogId:I

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "filename"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v4, 0x7f080002

    invoke-direct {v0, v2, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v2, 0x7f030000

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f06000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getEditTextFilter()[Landroid/text/InputFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mEditTextName:Landroid/widget/EditText;

    new-instance v2, Lcom/lifevibes/trimapp/widget/TrimDialog$1;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$1;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/lifevibes/trimapp/widget/TrimDialog$2;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$2;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/lifevibes/trimapp/widget/TrimDialog$3;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$3;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v5, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    const v1, 0x7f07002b

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1030011

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v7}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    const v1, 0x7f030001

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    const v1, 0x7f06000e

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/lifevibes/trimapp/widget/TrimDialog$4;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$4;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    const v2, 0x7f07002c

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(I)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/lifevibes/trimapp/widget/TrimDialog$5;

    invoke-direct {v3, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$5;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    :pswitch_3
    const v0, 0x7f070013

    const/4 v1, 0x3

    if-ne v2, v1, :cond_0

    const v0, 0x7f070015

    :cond_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogListener:Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;

    const-string v3, "Unsupported showing"

    invoke-interface {v1, v2, v6, v3}, Lcom/lifevibes/trimapp/widget/TrimDialog$DialogListener;->onDialogMessage(IILjava/lang/String;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070004

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/widget/TrimDialog$6;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$6;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/lifevibes/trimapp/widget/TrimDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070028

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/lifevibes/trimapp/widget/TrimDialog$9;

    invoke-direct {v1, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$9;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v5, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07002a

    new-instance v2, Lcom/lifevibes/trimapp/widget/TrimDialog$8;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$8;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070029

    new-instance v2, Lcom/lifevibes/trimapp/widget/TrimDialog$7;

    invoke-direct {v2, p0}, Lcom/lifevibes/trimapp/widget/TrimDialog$7;-><init>(Lcom/lifevibes/trimapp/widget/TrimDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setProgress(I)V
    .locals 2

    iget v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialogId:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/widget/TrimDialog;->mDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    :cond_0
    return-void
.end method
