.class Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim_Share;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressDialogListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim_Share;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim_Share;Lcom/lifevibes/trimapp/Trim_Share$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;-><init>(Lcom/lifevibes/trimapp/Trim_Share;)V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$1900(Lcom/lifevibes/trimapp/Trim_Share;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    const v1, 0x7f07001c

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->stopTrimming(I)V
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2700(Lcom/lifevibes/trimapp/Trim_Share;I)V

    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mTrimmingSuccess:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2400(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # invokes: Lcom/lifevibes/trimapp/Trim_Share;->returnToInvoker()V
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$300(Lcom/lifevibes/trimapp/Trim_Share;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2500(Lcom/lifevibes/trimapp/Trim_Share;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mOnTrimmingFile:Ljava/lang/String;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim_Share;->access$2500(Lcom/lifevibes/trimapp/Trim_Share;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsConfigChanged:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$3100(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    # getter for: Lcom/lifevibes/trimapp/Trim_Share;->mIsActivityFinishing:Z
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim_Share;->access$2300(Lcom/lifevibes/trimapp/Trim_Share;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim_Share$ProgressDialogListener;->this$0:Lcom/lifevibes/trimapp/Trim_Share;

    invoke-virtual {v1}, Lcom/lifevibes/trimapp/Trim_Share;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim_Share;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method
