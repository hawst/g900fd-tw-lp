.class Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/lifevibes/trimapp/widget/DoubleSeekBar$OnDoubleSeekBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/Trim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DoubleSeekBarListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lifevibes/trimapp/Trim;


# direct methods
.method private constructor <init>(Lcom/lifevibes/trimapp/Trim;)V
    .locals 0

    iput-object p1, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/lifevibes/trimapp/Trim;Lcom/lifevibes/trimapp/Trim$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;-><init>(Lcom/lifevibes/trimapp/Trim;)V

    return-void
.end method


# virtual methods
.method public onHandleChanged(IIZ)V
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # setter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailOffset:I
    invoke-static {v0, p2}, Lcom/lifevibes/trimapp/Trim;->access$1402(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mMediaShareApi:Lcom/lifevibes/trimapp/util/MediaShare;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/util/MediaShare;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/util/MediaShare;->getTrimmingStatus()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    goto :goto_0
.end method

.method public onLongPressed(I)V
    .locals 3

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->clearAllImageViewFromLinear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$800(Lcom/lifevibes/trimapp/Trim;)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v1}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v1

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIvThumbnailsZoom:[Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$900(Lcom/lifevibes/trimapp/Trim;)[Landroid/widget/ImageView;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1000(Lcom/lifevibes/trimapp/Trim;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1000(Lcom/lifevibes/trimapp/Trim;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1000(Lcom/lifevibes/trimapp/Trim;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x3

    # setter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1002(Lcom/lifevibes/trimapp/Trim;I)I

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/lifevibes/trimapp/Trim;->startThumbnailTask(I)V

    return-void
.end method

.method public onLongReleased(I)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->clearAllImageViewFromLinear()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mNumberOfImages:I
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$800(Lcom/lifevibes/trimapp/Trim;)I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mDoubleSeekBar:Lcom/lifevibes/trimapp/widget/DoubleSeekBar;
    invoke-static {v2}, Lcom/lifevibes/trimapp/Trim;->access$700(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/DoubleSeekBar;

    move-result-object v2

    iget-object v3, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mIvThumbnails:[Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/lifevibes/trimapp/Trim;->access$1100(Lcom/lifevibes/trimapp/Trim;)[Landroid/widget/ImageView;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/lifevibes/trimapp/widget/DoubleSeekBar;->addImageViewToLinear(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    iget v0, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mCurrentMask:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mThumbnailTask:Lcom/lifevibes/trimapp/Trim$ThumbnailTask;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1200(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/Trim$ThumbnailTask;

    move-result-object v0

    iput v1, v0, Lcom/lifevibes/trimapp/Trim$ThumbnailTask;->mCurrentMask:I

    :cond_1
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/16 v1, -0x9

    # &= operator for: Lcom/lifevibes/trimapp/Trim;->mThumbnailMask:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1372(Lcom/lifevibes/trimapp/Trim;I)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1000(Lcom/lifevibes/trimapp/Trim;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x4

    # setter for: Lcom/lifevibes/trimapp/Trim;->mPreviewState:I
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1002(Lcom/lifevibes/trimapp/Trim;I)I

    :cond_2
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/Trim;->updateButtonsEnabled()V

    return-void
.end method

.method public onProgressChanged(IZ)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->seekTo(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x1

    # setter for: Lcom/lifevibes/trimapp/Trim;->mDragging:Z
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1602(Lcom/lifevibes/trimapp/Trim;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public onStopTrackingTouch()V
    .locals 2

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    const/4 v1, 0x0

    # setter for: Lcom/lifevibes/trimapp/Trim;->mDragging:Z
    invoke-static {v0, v1}, Lcom/lifevibes/trimapp/Trim;->access$1602(Lcom/lifevibes/trimapp/Trim;Z)Z

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # invokes: Lcom/lifevibes/trimapp/Trim;->setVideoProgress()I
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1800(Lcom/lifevibes/trimapp/Trim;)I

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mVideoView:Lcom/lifevibes/trimapp/widget/CustomVideoView;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$500(Lcom/lifevibes/trimapp/Trim;)Lcom/lifevibes/trimapp/widget/CustomVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lifevibes/trimapp/widget/CustomVideoView;->start()V

    iget-object v0, p0, Lcom/lifevibes/trimapp/Trim$DoubleSeekBarListener;->this$0:Lcom/lifevibes/trimapp/Trim;

    # getter for: Lcom/lifevibes/trimapp/Trim;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/lifevibes/trimapp/Trim;->access$1700(Lcom/lifevibes/trimapp/Trim;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
