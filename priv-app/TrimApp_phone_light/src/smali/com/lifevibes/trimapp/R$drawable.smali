.class public final Lcom/lifevibes/trimapp/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lifevibes/trimapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final btn_help_selector:I = 0x7f020000

.field public static final btn_pause_selector:I = 0x7f020001

.field public static final btn_pause_selector_ripple:I = 0x7f020002

.field public static final btn_play_selector:I = 0x7f020003

.field public static final btn_play_selector_ripple:I = 0x7f020004

.field public static final dsb_background:I = 0x7f020005

.field public static final easy_video_control_pause:I = 0x7f020006

.field public static final easy_video_control_pause_focused:I = 0x7f020007

.field public static final easy_video_control_pause_pressed:I = 0x7f020008

.field public static final easy_video_control_play:I = 0x7f020009

.field public static final easy_video_control_play_focused:I = 0x7f02000a

.field public static final easy_video_control_play_pressed:I = 0x7f02000b

.field public static final frame_background:I = 0x7f02000c

.field public static final handle_left_selector:I = 0x7f02000d

.field public static final handle_right_selector:I = 0x7f02000e

.field public static final icon:I = 0x7f02000f

.field public static final overlay_help_button_focused:I = 0x7f020010

.field public static final overlay_help_button_normal:I = 0x7f020011

.field public static final overlay_help_button_pressed:I = 0x7f020012

.field public static final overlay_help_hand_left:I = 0x7f020013

.field public static final overlay_help_hand_right:I = 0x7f020014

.field public static final ripple_effect:I = 0x7f020015

.field public static final selector_menu_text_btn:I = 0x7f020016

.field public static final selector_menu_text_btn_tft_lcd:I = 0x7f020017

.field public static final trimming_handle_user:I = 0x7f020018

.field public static final video_player_trim_handler:I = 0x7f020019

.field public static final video_player_trim_handler_focused:I = 0x7f02001a

.field public static final video_player_trim_handler_pressed:I = 0x7f02001b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
