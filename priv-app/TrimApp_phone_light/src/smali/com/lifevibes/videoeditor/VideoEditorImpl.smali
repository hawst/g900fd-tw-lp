.class public Lcom/lifevibes/videoeditor/VideoEditorImpl;
.super Ljava/lang/Object;
.source "VideoEditorImpl.java"

# interfaces
.implements Lcom/lifevibes/videoeditor/VideoEditor;


# static fields
.field private static final CODECCONFIG_FILENAME:Ljava/lang/String; = "CodecConfig.xml"

.field private static final TAG:Ljava/lang/String; = "VideoEditorImpl"

.field private static final TAG_CONFIG_DEC_EXPORT:Ljava/lang/String; = "ExportConfigDecoder"

.field private static final TAG_CONFIG_DEC_TN_ACCURATE_MODE:Ljava/lang/String; = "ThumbnailAccurateMode"

.field private static final TAG_CONFIG_DEC_TN_FAST_MODE:Ljava/lang/String; = "ThumbnailFastMode"

.field private static final TAG_CONFIG_ENC_EXPORT:Ljava/lang/String; = "ExportConfigEncoder"

.field private static final TAG_EXPORT:Ljava/lang/String; = "ExportCodecConfig"

.field private static final TAG_TN:Ljava/lang/String; = "ThumbnailCodecConfig"


# instance fields
.field private mAspectRatio:I

.field private mClipsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDurationMs:J

.field private mIsFirstVideoItemAdded:Z

.field private mIsList3D:Z

.field private final mLock:Ljava/util/concurrent/Semaphore;

.field private mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

.field private final mMediaAudioItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaAudioItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mMediaItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/lifevibes/videoeditor/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mProjectPath:Ljava/lang/String;

.field private modifiedBitrate:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "projectPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mClipsList:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    .line 131
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1, v1}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    .line 132
    new-instance v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, p1, v1, p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;-><init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Lcom/lifevibes/videoeditor/VideoEditor;)V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    .line 133
    iput-object p1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    .line 134
    const/4 v0, 0x3

    iput v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 135
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    .line 136
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 2
    .param p1, "projectPath"    # Ljava/lang/String;
    .param p2, "codecConfigInputStreamCtx"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mClipsList:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    .line 148
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1, v1}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    .line 149
    new-instance v0, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, p1, v1, p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;-><init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Lcom/lifevibes/videoeditor/VideoEditor;)V

    iput-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    .line 150
    iput-object p1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    .line 151
    const/4 v0, 0x3

    iput v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 152
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    .line 154
    if-eqz p2, :cond_0

    .line 156
    invoke-virtual {p2}, Ljava/io/InputStream;->available()I

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    :try_start_0
    invoke-direct {p0, p2}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->readCodecConfigFile(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "projectPath"    # Ljava/lang/String;
    .param p2, "codecConfigPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mClipsList:Ljava/util/List;

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    .line 177
    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, v2, v2}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    .line 178
    new-instance v1, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, p1, v2, p0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;-><init>(Ljava/lang/String;Ljava/util/concurrent/Semaphore;Lcom/lifevibes/videoeditor/VideoEditor;)V

    iput-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    .line 179
    iput-object p1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    .line 180
    const/4 v1, 0x3

    iput v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 181
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    .line 182
    new-instance v0, Ljava/io/File;

    const-string v1, "CodecConfig.xml"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .local v0, "CodecConfigXml":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    :try_start_0
    invoke-direct {p0, p2}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->readCodecConfigFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private computeTimelineDuration()V
    .locals 8

    .prologue
    .line 1243
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    .line 1244
    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 1245
    .local v2, "mediaItemsCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1246
    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lifevibes/videoeditor/MediaItem;

    .line 1247
    .local v1, "mediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    iget-wide v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    invoke-virtual {v1}, Lcom/lifevibes/videoeditor/MediaItem;->getTimelineDuration()J

    move-result-wide v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    .line 1245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1249
    .end local v1    # "mediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    :cond_0
    return-void
.end method

.method private doXMLParsing(Ljava/io/InputStream;)V
    .locals 17
    .param p1, "fis"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1319
    const/4 v13, 0x0

    .line 1321
    .local v13, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v8

    .line 1322
    .local v8, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v15, 0x1

    invoke-virtual {v8, v15}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 1323
    invoke-virtual {v8}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v13

    .line 1324
    const-string v15, "UTF-8"

    move-object/from16 v0, p1

    invoke-interface {v13, v0, v15}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1325
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v7

    .line 1327
    .local v7, "eventType":I
    const/4 v5, 0x0

    .line 1328
    .local v5, "UNDEFINED_MODE":I
    const/4 v4, 0x1

    .line 1329
    .local v4, "THUMBNAIL_MODE":I
    const/4 v2, 0x2

    .line 1330
    .local v2, "EXPORT_MODE":I
    const/4 v3, 0x3

    .line 1331
    .local v3, "FAST_THUMBNAIL_MODE":I
    const/4 v1, 0x4

    .line 1332
    .local v1, "ACCURATE_THUMBNAIL_MODE":I
    const/4 v14, 0x0

    .line 1333
    .local v14, "thumbnailMode":I
    const/4 v11, 0x0

    .line 1334
    .local v11, "mediaOperationMode":I
    const/4 v6, 0x0

    .line 1335
    .local v6, "codecType":I
    const/4 v9, 0x0

    .line 1336
    .local v9, "isDecoder":Z
    const/4 v10, 0x0

    .line 1338
    .local v10, "isHWCodec":Z
    :goto_0
    const/4 v15, 0x1

    if-eq v7, v15, :cond_d

    .line 1339
    packed-switch v7, :pswitch_data_0

    .line 1399
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    goto :goto_0

    .line 1341
    :pswitch_1
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 1343
    .local v12, "name":Ljava/lang/String;
    const-string v15, "ThumbnailCodecConfig"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1344
    const/4 v11, 0x1

    .line 1349
    :cond_1
    :goto_2
    const-string v15, "ThumbnailFastMode"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1350
    const/4 v14, 0x3

    .line 1351
    const/4 v9, 0x1

    goto :goto_1

    .line 1345
    :cond_2
    const-string v15, "ExportCodecConfig"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1346
    const/4 v11, 0x2

    goto :goto_2

    .line 1352
    :cond_3
    const-string v15, "ThumbnailAccurateMode"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1353
    const/4 v14, 0x4

    .line 1354
    const/4 v9, 0x1

    goto :goto_1

    .line 1355
    :cond_4
    const-string v15, "ExportConfigDecoder"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1356
    const/4 v9, 0x1

    goto :goto_1

    .line 1357
    :cond_5
    const-string v15, "ExportConfigEncoder"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1358
    const/4 v9, 0x0

    goto :goto_1

    .line 1363
    .end local v12    # "name":Ljava/lang/String;
    :pswitch_2
    const/4 v15, 0x1

    if-ne v9, v15, :cond_8

    .line 1364
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    const-string v16, "SWCODEC"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1365
    const/4 v6, 0x1

    .line 1377
    :cond_6
    :goto_3
    const/4 v15, 0x1

    if-ne v11, v15, :cond_b

    .line 1379
    const/4 v15, 0x4

    if-ne v14, v15, :cond_a

    .line 1380
    sput v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigAccurateMode:I

    goto :goto_1

    .line 1366
    :cond_7
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    const-string v16, "HWCODEC"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1367
    const/4 v6, 0x2

    goto :goto_3

    .line 1370
    :cond_8
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    const-string v16, "SWCODEC"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1371
    const/4 v6, 0x3

    goto :goto_3

    .line 1372
    :cond_9
    invoke-interface {v13}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v15

    const-string v16, "HWCODEC"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1373
    const/4 v6, 0x4

    goto :goto_3

    .line 1381
    :cond_a
    const/4 v15, 0x3

    if-ne v14, v15, :cond_0

    .line 1382
    sput v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mTNDecConfigFastMode:I

    goto/16 :goto_1

    .line 1384
    :cond_b
    const/4 v15, 0x2

    if-ne v11, v15, :cond_0

    .line 1386
    const/4 v15, 0x1

    if-ne v9, v15, :cond_c

    .line 1387
    sput v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportDecConfig:I

    goto/16 :goto_1

    .line 1389
    :cond_c
    sput v6, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->mExportEncConfig:I

    goto/16 :goto_1

    .line 1401
    :cond_d
    const/4 v13, 0x0

    .line 1402
    return-void

    .line 1339
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private lock()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 1257
    const-string v0, "VideoEditorImpl"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1258
    const-string v0, "VideoEditorImpl"

    const-string v1, "lock: grabbing semaphore"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1260
    :cond_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1261
    const-string v0, "VideoEditorImpl"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1262
    const-string v0, "VideoEditorImpl"

    const-string v1, "lock: grabbed semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    :cond_1
    return-void
.end method

.method private lock(J)Z
    .locals 5
    .param p1, "timeoutMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 1275
    const-string v1, "VideoEditorImpl"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1276
    const-string v1, "VideoEditorImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "lock: grabbing semaphore with timeout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1279
    :cond_0
    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 1280
    .local v0, "acquireSem":Z
    const-string v1, "VideoEditorImpl"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1281
    const-string v1, "VideoEditorImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "lock: grabbed semaphore status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    :cond_1
    return v0
.end method

.method private readCodecConfigFile(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "codecConfigInputStrCtx"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1302
    invoke-direct {p0, p1}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->doXMLParsing(Ljava/io/InputStream;)V

    .line 1303
    return-void
.end method

.method private readCodecConfigFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "codecConfigPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1310
    new-instance v0, Ljava/io/File;

    const-string v2, "CodecConfig.xml"

    invoke-direct {v0, p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    .local v0, "file":Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1313
    .local v1, "fis":Ljava/io/FileInputStream;
    invoke-direct {p0, v1}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->doXMLParsing(Ljava/io/InputStream;)V

    .line 1314
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 1315
    return-void
.end method

.method private setOptimalConfiguration(I)I
    .locals 5
    .param p1, "bitrate"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x5

    .line 627
    const/4 v0, 0x0

    .line 628
    .local v0, "height":I
    const v1, 0x7a1200

    if-lt p1, v1, :cond_0

    .line 629
    const p1, 0x7a1200

    .line 630
    const/16 v0, 0x438

    .line 631
    const/4 v1, 0x2

    iput v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 686
    :goto_0
    iput p1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->modifiedBitrate:I

    .line 687
    return v0

    .line 633
    :cond_0
    const v1, 0x4c4b40

    if-lt p1, v1, :cond_1

    .line 634
    const p1, 0x4c4b40

    .line 635
    const/16 v0, 0x2d0

    .line 636
    iput v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 638
    :cond_1
    const v1, 0x1e8480

    if-lt p1, v1, :cond_2

    .line 639
    const p1, 0x1e8480

    .line 640
    const/16 v0, 0x1e0

    .line 641
    iput v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 643
    :cond_2
    const v1, 0xc3500

    if-lt p1, v1, :cond_3

    .line 644
    const p1, 0xc3500

    .line 645
    const/16 v0, 0x1e0

    .line 646
    iput v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 648
    :cond_3
    const v1, 0x7d000

    if-lt p1, v1, :cond_4

    .line 649
    const p1, 0x7d000

    .line 650
    const/16 v0, 0x1e0

    .line 651
    iput v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 653
    :cond_4
    const v1, 0x5dc00

    if-lt p1, v1, :cond_5

    .line 654
    const p1, 0x5dc00

    .line 655
    const/16 v0, 0x120

    .line 656
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 658
    :cond_5
    const v1, 0x3e800

    if-lt p1, v1, :cond_6

    .line 659
    const p1, 0x3e800

    .line 660
    const/16 v0, 0x120

    .line 661
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 663
    :cond_6
    const v1, 0x2ee00

    if-lt p1, v1, :cond_7

    .line 664
    const p1, 0x2ee00

    .line 665
    const/16 v0, 0x90

    .line 666
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 669
    :cond_7
    const v1, 0x1f400

    if-lt p1, v1, :cond_8

    .line 670
    const p1, 0x1f400

    .line 671
    const/16 v0, 0x90

    .line 672
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 675
    :cond_8
    const v1, 0x17700

    if-lt p1, v1, :cond_9

    .line 676
    const p1, 0x17700

    .line 677
    const/16 v0, 0x90

    .line 678
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0

    .line 682
    :cond_9
    const p1, 0xfa00

    .line 683
    const/16 v0, 0x90

    .line 684
    iput v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    goto :goto_0
.end method

.method private unlock()V
    .locals 2

    .prologue
    .line 1291
    const-string v0, "VideoEditorImpl"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1292
    const-string v0, "VideoEditorImpl"

    const-string v1, "unlock: releasing semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1295
    return-void
.end method


# virtual methods
.method public addMediaAudioItem(Lcom/lifevibes/videoeditor/MediaAudioItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/lifevibes/videoeditor/MediaAudioItem;

    .prologue
    const/4 v1, 0x1

    .line 228
    if-nez p1, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Media item is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Media item already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 239
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More than one media item cannot be added for MediaShare configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    :cond_3
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    return-void
.end method

.method public declared-synchronized addMediaItem(Lcom/lifevibes/videoeditor/MediaItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/lifevibes/videoeditor/MediaItem;

    .prologue
    const/4 v1, 0x1

    .line 278
    monitor-enter p0

    if-nez p1, :cond_0

    .line 279
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Media item is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 284
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Media item already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/lifevibes/videoeditor/MediaItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 288
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More than one media item cannot be added for MediaShare configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :cond_3
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 291
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More than one media item cannot be added for MediaShare configuration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_4
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->computeTimelineDuration()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 300
    monitor-exit p0

    return-void
.end method

.method public cancelExport(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v0, p1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stop(Ljava/lang/String;)V

    .line 309
    :cond_0
    return-void
.end method

.method public cancelFit2Share(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1168
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v0, p1}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->stop(Ljava/lang/String;)V

    .line 1170
    :cond_0
    return-void
.end method

.method public export(Ljava/lang/String;JLcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .locals 20
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "outputsize"    # J
    .param p4, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 698
    const/4 v6, 0x0

    .line 699
    .local v6, "height":I
    const/4 v14, 0x0

    .line 700
    .local v14, "bitrate":I
    const/16 v16, 0x0

    .line 701
    .local v16, "duration":I
    const/4 v2, 0x0

    .line 703
    .local v2, "aspectRatioSet":I
    if-nez p1, :cond_0

    .line 704
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "export: filename is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 706
    :cond_0
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 707
    .local v19, "tempPathFile":Ljava/io/File;
    if-nez v19, :cond_1

    .line 708
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "can not be created"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 710
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 711
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No MediaItems added"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 713
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-gtz v3, :cond_3

    .line 714
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Size is Zero"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 716
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    long-to-int v0, v4

    move/from16 v16, v0

    .line 721
    move-wide/from16 v0, p2

    long-to-double v4, v0

    move-wide/from16 v0, p2

    long-to-double v10, v0

    const-wide v12, 0x3fa47ae147ae147bL    # 0.04

    mul-double/2addr v10, v12

    sub-double/2addr v4, v10

    move/from16 v0, v16

    int-to-double v10, v0

    div-double/2addr v4, v10

    const-wide/high16 v10, 0x4020000000000000L    # 8.0

    mul-double/2addr v4, v10

    double-to-int v14, v4

    .line 726
    add-int/lit16 v14, v14, -0x2fa8

    .line 728
    const v3, 0xfa00

    if-gt v14, v3, :cond_4

    .line 730
    move-wide/from16 v0, p2

    long-to-double v4, v0

    move-wide/from16 v0, p2

    long-to-double v10, v0

    const-wide v12, 0x3fa47ae147ae147bL    # 0.04

    mul-double/2addr v10, v12

    sub-double/2addr v4, v10

    const-wide v10, 0x40ef400000000000L    # 64000.0

    div-double/2addr v4, v10

    const-wide/high16 v10, 0x4020000000000000L    # 8.0

    mul-double/2addr v4, v10

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v10

    double-to-float v15, v4

    .line 733
    .local v15, "dur":F
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "export: limit file duration to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 738
    .end local v15    # "dur":F
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->setOptimalConfiguration(I)I

    move-result v6

    .line 739
    const/4 v8, 0x1

    .line 740
    .local v8, "audioCodec":I
    const/4 v9, 0x4

    .line 742
    .local v9, "videoCodec":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 747
    const/16 v18, 0x0

    .line 749
    .local v18, "semAcquireDone":Z
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 750
    const/16 v18, 0x1

    .line 752
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v3, :cond_6

    .line 753
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The video editor is not initialized"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 757
    :catch_0
    move-exception v17

    .line 758
    .local v17, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "VideoEditorImpl"

    const-string v4, "Sem acquire NOT successful in export"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 760
    if-eqz v18, :cond_5

    .line 761
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 767
    .end local v17    # "ex":Ljava/lang/InterruptedException;
    :cond_5
    :goto_0
    move-object/from16 v0, p0

    iput v2, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 772
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v3, v4, p2

    if-lez v3, :cond_8

    .line 776
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    .line 777
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Generated File size > outputsize; reduce Input file duration and retry"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 755
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->modifiedBitrate:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    move-object/from16 v4, p1

    move-wide/from16 v10, p2

    move-object/from16 v13, p4

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 760
    if-eqz v18, :cond_5

    .line 761
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 760
    :catchall_0
    move-exception v3

    if-eqz v18, :cond_7

    .line 761
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_7
    throw v3

    .line 780
    :cond_8
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v4

    return-wide v4
.end method

.method public export(Ljava/lang/String;IIIILcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 16
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "height"    # I
    .param p3, "bitrate"    # I
    .param p4, "audioCodec"    # I
    .param p5, "videoCodec"    # I
    .param p6, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 318
    if-nez p1, :cond_0

    .line 319
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "export: filename is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 321
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 322
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No MediaItems added"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 324
    :cond_1
    packed-switch p4, :pswitch_data_0

    .line 330
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported audio codec type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 331
    .local v14, "message":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 335
    .end local v14    # "message":Ljava/lang/String;
    :pswitch_0
    packed-switch p5, :pswitch_data_1

    .line 344
    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported video codec type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 345
    .restart local v14    # "message":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 349
    .end local v14    # "message":Ljava/lang/String;
    :pswitch_2
    sparse-switch p2, :sswitch_data_0

    .line 362
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Height incorrect"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 365
    :sswitch_0
    sparse-switch p3, :sswitch_data_1

    .line 384
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Bitrate incorrect"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 387
    :sswitch_1
    const/4 v3, 0x1

    move/from16 v0, p5

    if-ne v0, v3, :cond_2

    .line 388
    sparse-switch p2, :sswitch_data_2

    .line 401
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Height incorrect for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 390
    :sswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getAspectRatio()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 391
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Aspect ratio not correct for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 396
    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getAspectRatio()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    .line 397
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Aspect ratio not correct for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 405
    :cond_2
    const/4 v15, 0x0

    .line 407
    .local v15, "semAcquireDone":Z
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 408
    const/4 v15, 0x1

    .line 410
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v3, :cond_4

    .line 411
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The video editor is not initialized"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :catch_0
    move-exception v2

    .line 416
    .local v2, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "VideoEditorImpl"

    const-string v4, "Sem acquire NOT successful in export"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 418
    if-eqz v15, :cond_3

    .line 419
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 422
    .end local v2    # "ex":Ljava/lang/InterruptedException;
    :cond_3
    :goto_0
    return-void

    .line 413
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    move-object/from16 v4, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v13, p6

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 418
    if-eqz v15, :cond_3

    .line 419
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 418
    :catchall_0
    move-exception v3

    if-eqz v15, :cond_5

    .line 419
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_5
    throw v3

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 335
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 349
    :sswitch_data_0
    .sparse-switch
        0x60 -> :sswitch_0
        0x78 -> :sswitch_0
        0x90 -> :sswitch_0
        0xf0 -> :sswitch_0
        0x120 -> :sswitch_0
        0x168 -> :sswitch_0
        0x1e0 -> :sswitch_0
        0x2d0 -> :sswitch_0
        0x438 -> :sswitch_0
    .end sparse-switch

    .line 365
    :sswitch_data_1
    .sparse-switch
        0x6d60 -> :sswitch_1
        0x9c40 -> :sswitch_1
        0xfa00 -> :sswitch_1
        0x17700 -> :sswitch_1
        0x1f400 -> :sswitch_1
        0x2ee00 -> :sswitch_1
        0x3e800 -> :sswitch_1
        0x5dc00 -> :sswitch_1
        0x7d000 -> :sswitch_1
        0xc3500 -> :sswitch_1
        0xf4240 -> :sswitch_1
        0x124f80 -> :sswitch_1
        0x16e360 -> :sswitch_1
        0x1e8480 -> :sswitch_1
        0x4c4b40 -> :sswitch_1
        0x7a1200 -> :sswitch_1
    .end sparse-switch

    .line 388
    :sswitch_data_2
    .sparse-switch
        0x60 -> :sswitch_2
        0x90 -> :sswitch_3
        0x120 -> :sswitch_3
    .end sparse-switch
.end method

.method public export(Ljava/lang/String;IIJIILcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 16
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "height"    # I
    .param p3, "bitrate"    # I
    .param p4, "outputFileSize"    # J
    .param p6, "audioCodec"    # I
    .param p7, "videoCodec"    # I
    .param p8, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    if-nez p1, :cond_0

    .line 428
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "export: filename is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 430
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 431
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No MediaItems added"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 433
    :cond_1
    packed-switch p6, :pswitch_data_0

    .line 439
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported audio codec type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 440
    .local v14, "message":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 444
    .end local v14    # "message":Ljava/lang/String;
    :pswitch_0
    packed-switch p7, :pswitch_data_1

    .line 453
    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported video codec type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 454
    .restart local v14    # "message":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 458
    .end local v14    # "message":Ljava/lang/String;
    :pswitch_2
    sparse-switch p2, :sswitch_data_0

    .line 471
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Height incorrect"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 474
    :sswitch_0
    sparse-switch p3, :sswitch_data_1

    .line 493
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Bitrate incorrect"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 496
    :sswitch_1
    const/4 v3, 0x1

    move/from16 v0, p7

    if-ne v0, v3, :cond_2

    .line 497
    sparse-switch p2, :sswitch_data_2

    .line 510
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Argument Height incorrect for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 499
    :sswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getAspectRatio()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 500
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Aspect ratio not correct for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 505
    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getAspectRatio()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    .line 506
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Aspect ratio not correct for H263 output codec"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 514
    :cond_2
    const/4 v15, 0x0

    .line 516
    .local v15, "semAcquireDone":Z
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 517
    const/4 v15, 0x1

    .line 519
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v3, :cond_4

    .line 520
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The video editor is not initialized"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524
    :catch_0
    move-exception v2

    .line 525
    .local v2, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "VideoEditorImpl"

    const-string v4, "Sem acquire NOT successful in export"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 527
    if-eqz v15, :cond_3

    .line 528
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 531
    .end local v2    # "ex":Ljava/lang/InterruptedException;
    :cond_3
    :goto_0
    return-void

    .line 522
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    move-object/from16 v4, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p6

    move/from16 v9, p7

    move-wide/from16 v10, p4

    move-object/from16 v13, p8

    invoke-virtual/range {v3 .. v13}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 527
    if-eqz v15, :cond_3

    .line 528
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 527
    :catchall_0
    move-exception v3

    if-eqz v15, :cond_5

    .line 528
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_5
    throw v3

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 444
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 458
    :sswitch_data_0
    .sparse-switch
        0x60 -> :sswitch_0
        0x78 -> :sswitch_0
        0x90 -> :sswitch_0
        0xf0 -> :sswitch_0
        0x120 -> :sswitch_0
        0x168 -> :sswitch_0
        0x1e0 -> :sswitch_0
        0x2d0 -> :sswitch_0
        0x438 -> :sswitch_0
    .end sparse-switch

    .line 474
    :sswitch_data_1
    .sparse-switch
        0x6d60 -> :sswitch_1
        0x9c40 -> :sswitch_1
        0xfa00 -> :sswitch_1
        0x17700 -> :sswitch_1
        0x1f400 -> :sswitch_1
        0x2ee00 -> :sswitch_1
        0x3e800 -> :sswitch_1
        0x5dc00 -> :sswitch_1
        0x7d000 -> :sswitch_1
        0xc3500 -> :sswitch_1
        0xf4240 -> :sswitch_1
        0x124f80 -> :sswitch_1
        0x16e360 -> :sswitch_1
        0x1e8480 -> :sswitch_1
        0x4c4b40 -> :sswitch_1
        0x7a1200 -> :sswitch_1
    .end sparse-switch

    .line 497
    :sswitch_data_2
    .sparse-switch
        0x60 -> :sswitch_2
        0x90 -> :sswitch_3
        0x120 -> :sswitch_3
    .end sparse-switch
.end method

.method public export(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 12
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 572
    const/4 v7, 0x0

    .line 573
    .local v7, "maxDurationMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    if-nez p1, :cond_0

    .line 574
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "export: filename is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_0
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 577
    .local v11, "tempPathFile":Ljava/io/File;
    if-nez v11, :cond_1

    .line 578
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "can not be created"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 581
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No MediaItems added"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_2
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 584
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "maxDurationMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    check-cast v7, Lcom/lifevibes/videoeditor/MediaItem;

    .line 587
    .restart local v7    # "maxDurationMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    :cond_3
    const-wide/16 v8, 0x0

    .line 589
    .local v8, "maximumDuration":J
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    instance-of v0, v7, Lcom/lifevibes/videoeditor/MediaVideoItem;

    if-eqz v0, :cond_4

    .line 590
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "maxDurationMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    check-cast v7, Lcom/lifevibes/videoeditor/MediaItem;

    .line 591
    .restart local v7    # "maxDurationMediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v7}, Lcom/lifevibes/videoeditor/MediaItem;->getWidth()I

    move-result v1

    invoke-virtual {v7}, Lcom/lifevibes/videoeditor/MediaItem;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->getAspectRatio(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->setAspectRatio(I)V

    .line 593
    :cond_4
    const/4 v10, 0x0

    .line 595
    .local v10, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 596
    const/4 v10, 0x1

    .line 598
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v0, :cond_6

    .line 599
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The video editor is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :catch_0
    move-exception v6

    .line 604
    .local v6, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v0, "VideoEditorImpl"

    const-string v1, "Sem acquire NOT successful in export"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 606
    if-eqz v10, :cond_5

    .line 607
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 610
    .end local v6    # "ex":Ljava/lang/InterruptedException;
    :cond_5
    :goto_0
    return-void

    .line 601
    :cond_6
    :try_start_2
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    iget-object v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    move-object v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 606
    if-eqz v10, :cond_5

    .line 607
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 606
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_7

    .line 607
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_7
    throw v0
.end method

.method public exportAs2D(Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 536
    if-nez p1, :cond_0

    .line 537
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "export: filename is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 540
    :cond_0
    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 541
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "This Export kind is not supported for MediaAudioItems."

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 543
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 544
    .local v2, "tempPathFile":Ljava/io/File;
    if-nez v2, :cond_2

    .line 545
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "can not be created"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 548
    :cond_2
    const/4 v1, 0x0

    .line 550
    .local v1, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 551
    const/4 v1, 0x1

    .line 553
    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v3, :cond_4

    .line 554
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The video editor is not initialized"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :catch_0
    move-exception v0

    .line 558
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "VideoEditorImpl"

    const-string v4, "Sem acquire NOT successful in export"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 560
    if-eqz v1, :cond_3

    .line 561
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 566
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :cond_3
    :goto_0
    return-void

    .line 556
    :cond_4
    :try_start_2
    iget-object v3, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    iget-object v4, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    iget-object v5, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-virtual {v3, p1, v4, v5, p2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->exportAs2D(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 560
    if-eqz v1, :cond_3

    .line 561
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 560
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_5

    .line 561
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_5
    throw v3
.end method

.method public fit2Share(Ljava/lang/String;Ljava/lang/String;JLcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .locals 39
    .param p1, "fileIn"    # Ljava/lang/String;
    .param p2, "fileOut"    # Ljava/lang/String;
    .param p3, "maxFileSize"    # J
    .param p5, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1007
    const/4 v8, 0x0

    .line 1008
    .local v8, "height":I
    const/16 v18, 0x0

    .line 1009
    .local v18, "bitrate":I
    const/16 v21, 0x0

    .line 1011
    .local v21, "duration":I
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1012
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "fit2Share: filename is null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1015
    :cond_1
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1016
    .local v32, "tempPathFile":Ljava/io/File;
    if-nez v32, :cond_2

    .line 1017
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "can not be created"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1020
    :cond_2
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1021
    .local v26, "inputFile":Ljava/io/File;
    if-nez v26, :cond_3

    .line 1022
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "can not be created"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1024
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v5, p3, v6

    if-gtz v5, :cond_4

    .line 1025
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "fit2Share: File Size is Zero"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1028
    :cond_4
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1029
    .local v14, "mediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const/16 v28, 0x0

    .line 1035
    .local v28, "mediaVidItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    :try_start_0
    new-instance v28, Lcom/lifevibes/videoeditor/MediaVideoItem;

    .end local v28    # "mediaVidItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    const-string v5, "mediaVidItem"

    const/4 v6, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v5, v2, v6}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1042
    .restart local v28    # "mediaVidItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    move-object/from16 v0, v28

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    .line 1046
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v5, v6, p3

    if-gtz v5, :cond_9

    .line 1048
    const-string v5, "FIT2SHARE"

    const-string v6, "Copying inputfile to output"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1053
    .local v16, "OutputStream":Ljava/io/FileOutputStream;
    new-instance v4, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1055
    .local v4, "InputStream":Ljava/io/FileInputStream;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->setFit2ShareFile(Ljava/lang/String;)V

    .line 1056
    const/4 v5, 0x1

    new-array v0, v5, [B

    move-object/from16 v19, v0

    .line 1058
    .local v19, "buffer":[B
    const-wide/16 v30, 0x0

    .line 1059
    .local v30, "prevProgress":J
    const-wide/16 v34, 0x0

    .line 1060
    .local v34, "totalBytesRead":J
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->length()J

    move-result-wide v24

    .line 1061
    .local v24, "fileLength":J
    move-object/from16 v33, p0

    .line 1062
    .local v33, "x":Lcom/lifevibes/videoeditor/VideoEditor;
    :cond_5
    :goto_0
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v27

    .local v27, "length":I
    if-lez v27, :cond_7

    .line 1063
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 1065
    if-eqz p5, :cond_5

    .line 1066
    move/from16 v0, v27

    int-to-long v6, v0

    add-long v34, v34, v6

    .line 1067
    const-wide/16 v6, 0x64

    mul-long v6, v6, v34

    div-long v6, v6, v24

    cmp-long v5, v6, v30

    if-eqz v5, :cond_6

    .line 1068
    const-wide/16 v6, 0x0

    cmp-long v5, v30, v6

    if-eqz v5, :cond_6

    .line 1069
    move-wide/from16 v0, v30

    long-to-int v5, v0

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v5}, Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V

    .line 1072
    :cond_6
    const-wide/16 v6, 0x64

    mul-long v6, v6, v34

    div-long v30, v6, v24

    goto :goto_0

    .line 1037
    .end local v4    # "InputStream":Ljava/io/FileInputStream;
    .end local v16    # "OutputStream":Ljava/io/FileOutputStream;
    .end local v19    # "buffer":[B
    .end local v24    # "fileLength":J
    .end local v27    # "length":I
    .end local v28    # "mediaVidItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    .end local v30    # "prevProgress":J
    .end local v33    # "x":Lcom/lifevibes/videoeditor/VideoEditor;
    .end local v34    # "totalBytesRead":J
    :catch_0
    move-exception v22

    .line 1038
    .local v22, "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can not create an Media Video Item of fileIn  Issue = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1076
    .end local v22    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v4    # "InputStream":Ljava/io/FileInputStream;
    .restart local v16    # "OutputStream":Ljava/io/FileOutputStream;
    .restart local v19    # "buffer":[B
    .restart local v24    # "fileLength":J
    .restart local v27    # "length":I
    .restart local v28    # "mediaVidItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    .restart local v30    # "prevProgress":J
    .restart local v33    # "x":Lcom/lifevibes/videoeditor/VideoEditor;
    .restart local v34    # "totalBytesRead":J
    :cond_7
    if-eqz p5, :cond_8

    .line 1078
    move-wide/from16 v0, v30

    long-to-int v5, v0

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v5}, Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V

    .line 1081
    :cond_8
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->flush()V

    .line 1082
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    .line 1083
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 1084
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1159
    .end local v4    # "InputStream":Ljava/io/FileInputStream;
    .end local v16    # "OutputStream":Ljava/io/FileOutputStream;
    .end local v19    # "buffer":[B
    .end local v24    # "fileLength":J
    .end local v27    # "length":I
    .end local v30    # "prevProgress":J
    .end local v33    # "x":Lcom/lifevibes/videoeditor/VideoEditor;
    .end local v34    # "totalBytesRead":J
    :goto_1
    return-wide v6

    .line 1090
    :cond_9
    invoke-virtual/range {v28 .. v28}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getTimelineDuration()J

    move-result-wide v6

    const-wide/16 v12, 0x3e8

    div-long/2addr v6, v12

    long-to-int v0, v6

    move/from16 v21, v0

    .line 1095
    move-wide/from16 v0, p3

    long-to-double v6, v0

    move-wide/from16 v0, p3

    long-to-double v12, v0

    const-wide v36, 0x3fa47ae147ae147bL    # 0.04

    mul-double v12, v12, v36

    sub-double/2addr v6, v12

    move/from16 v0, v21

    int-to-double v12, v0

    div-double/2addr v6, v12

    const-wide/high16 v12, 0x4020000000000000L    # 8.0

    mul-double/2addr v6, v12

    double-to-int v0, v6

    move/from16 v18, v0

    .line 1100
    move/from16 v0, v18

    add-int/lit16 v0, v0, -0x2fa8

    move/from16 v18, v0

    .line 1102
    const v5, 0xfa00

    move/from16 v0, v18

    if-gt v0, v5, :cond_a

    .line 1104
    move-wide/from16 v0, p3

    long-to-double v6, v0

    move-wide/from16 v0, p3

    long-to-double v12, v0

    const-wide v36, 0x3fa47ae147ae147bL    # 0.04

    mul-double v12, v12, v36

    sub-double/2addr v6, v12

    const-wide v12, 0x40ef400000000000L    # 64000.0

    div-double/2addr v6, v12

    const-wide/high16 v12, 0x4020000000000000L    # 8.0

    mul-double/2addr v6, v12

    const-wide v12, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v12

    double-to-float v0, v6

    move/from16 v20, v0

    .line 1107
    .local v20, "dur":F
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fit2share: limit file duration to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1111
    .end local v20    # "dur":F
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    move/from16 v17, v0

    .line 1113
    .local v17, "aspectRatioSet":I
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->setOptimalConfiguration(I)I

    move-result v8

    .line 1115
    move-object/from16 v0, p0

    iget v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    move/from16 v0, v17

    if-ne v0, v5, :cond_b

    .line 1116
    const/4 v5, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setRenderingMode(I)V

    .line 1119
    :cond_b
    const/4 v10, 0x1

    .line 1120
    .local v10, "audioCodec":I
    const/4 v11, 0x4

    .line 1121
    .local v11, "videoCodec":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    move/from16 v17, v0

    .line 1125
    const/16 v29, 0x0

    .line 1127
    .local v29, "semAcquireDone":Z
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 1128
    const/16 v29, 0x1

    .line 1129
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v5, :cond_d

    .line 1130
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "The video editor is not initialized"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1134
    :catch_1
    move-exception v23

    .line 1135
    .local v23, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v5, "VideoEditorImpl"

    const-string v6, "Sem acquire NOT successful in export"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1137
    if-eqz v29, :cond_c

    .line 1138
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 1145
    .end local v23    # "ex":Ljava/lang/InterruptedException;
    :cond_c
    :goto_2
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 1150
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v5, v6, p3

    if-lez v5, :cond_f

    .line 1154
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->delete()Z

    .line 1155
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Generated File size > outputsize; reduce Input file duration and retry"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1132
    :cond_d
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->modifiedBitrate:I

    move-object/from16 v6, p2

    move-wide/from16 v12, p3

    move-object/from16 v15, p5

    invoke-virtual/range {v5 .. v15}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1137
    if-eqz v29, :cond_c

    .line 1138
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_2

    .line 1137
    :catchall_0
    move-exception v5

    if-eqz v29, :cond_e

    .line 1138
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_e
    throw v5

    .line 1159
    :cond_f
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->length()J

    move-result-wide v6

    goto/16 :goto_1
.end method

.method public fit2ShareMMS(Ljava/lang/String;Ljava/lang/String;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)J
    .locals 36
    .param p1, "fileIn"    # Ljava/lang/String;
    .param p2, "fileOut"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 790
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 791
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "fit2ShareMMS: filename is null"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 794
    :cond_1
    new-instance v30, Ljava/io/File;

    move-object/from16 v0, v30

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 795
    .local v30, "tempPathFile":Ljava/io/File;
    if-nez v30, :cond_2

    .line 796
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "can not be created"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 799
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-nez v5, :cond_3

    .line 800
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "The video editor is not initialized"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 806
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    move/from16 v18, v0

    .line 807
    .local v18, "aspectRatioSet":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    const/4 v6, 0x5

    if-eq v5, v6, :cond_4

    .line 808
    const/4 v5, 0x5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 811
    :cond_4
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 812
    .local v14, "mmsMediaItemsList":Ljava/util/List;, "Ljava/util/List<Lcom/lifevibes/videoeditor/MediaItem;>;"
    const/16 v26, 0x0

    .line 818
    .local v26, "mmsMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    :try_start_0
    new-instance v26, Lcom/lifevibes/videoeditor/MediaVideoItem;

    .end local v26    # "mmsMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    const-string v5, "mmsMediaItem"

    const/4 v6, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v5, v2, v6}, Lcom/lifevibes/videoeditor/MediaVideoItem;-><init>(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 825
    .restart local v26    # "mmsMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    move-object/from16 v0, v26

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 831
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 832
    .local v4, "InFileHandle":Ljava/io/File;
    if-eqz v4, :cond_9

    .line 836
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v12, 0x4b000

    cmp-long v5, v6, v12

    if-gtz v5, :cond_9

    .line 840
    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getFps()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->GetClosestVideoFrameRate(I)I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_9

    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getVideoProfile()I

    move-result v5

    const/4 v6, 0x4

    if-gt v5, v6, :cond_9

    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAudioType()I

    move-result v5

    const/4 v6, 0x1

    if-gt v5, v6, :cond_9

    .line 848
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 849
    .local v17, "OutputStream":Ljava/io/FileOutputStream;
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 851
    .local v16, "InputStream":Ljava/io/FileInputStream;
    const/4 v5, 0x1

    new-array v0, v5, [B

    move-object/from16 v19, v0

    .line 853
    .local v19, "buffer":[B
    const-wide/16 v28, 0x0

    .line 854
    .local v28, "prevProgress":J
    const-wide/16 v32, 0x0

    .line 855
    .local v32, "totalBytesRead":J
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v24

    .line 856
    .local v24, "fileLength":J
    :cond_5
    :goto_0
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v23

    .local v23, "length":I
    if-lez v23, :cond_7

    .line 857
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    .line 859
    if-eqz p3, :cond_5

    .line 860
    move/from16 v0, v23

    int-to-long v6, v0

    add-long v32, v32, v6

    .line 861
    const-wide/16 v6, 0x64

    mul-long v6, v6, v32

    div-long v6, v6, v24

    cmp-long v5, v6, v28

    if-eqz v5, :cond_6

    .line 862
    const-wide/16 v6, 0x0

    cmp-long v5, v28, v6

    if-eqz v5, :cond_6

    .line 863
    move-wide/from16 v0, v28

    long-to-int v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v5}, Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V

    .line 866
    :cond_6
    const-wide/16 v6, 0x64

    mul-long v6, v6, v32

    div-long v28, v6, v24

    goto :goto_0

    .line 820
    .end local v4    # "InFileHandle":Ljava/io/File;
    .end local v16    # "InputStream":Ljava/io/FileInputStream;
    .end local v17    # "OutputStream":Ljava/io/FileOutputStream;
    .end local v19    # "buffer":[B
    .end local v23    # "length":I
    .end local v24    # "fileLength":J
    .end local v26    # "mmsMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    .end local v28    # "prevProgress":J
    .end local v32    # "totalBytesRead":J
    :catch_0
    move-exception v21

    .line 821
    .local v21, "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can not create an Media Video Item of fileIn file for MMS  Issue = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v21 .. v21}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 869
    .end local v21    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v4    # "InFileHandle":Ljava/io/File;
    .restart local v16    # "InputStream":Ljava/io/FileInputStream;
    .restart local v17    # "OutputStream":Ljava/io/FileOutputStream;
    .restart local v19    # "buffer":[B
    .restart local v23    # "length":I
    .restart local v24    # "fileLength":J
    .restart local v26    # "mmsMediaItem":Lcom/lifevibes/videoeditor/MediaVideoItem;
    .restart local v28    # "prevProgress":J
    .restart local v32    # "totalBytesRead":J
    :cond_7
    if-eqz p3, :cond_8

    .line 871
    move-wide/from16 v0, v28

    long-to-int v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-interface {v0, v1, v2, v5}, Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;->onProgress(Lcom/lifevibes/videoeditor/VideoEditor;Ljava/lang/String;I)V

    .line 875
    :cond_8
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->flush()V

    .line 876
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V

    .line 877
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V

    .line 879
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 997
    .end local v16    # "InputStream":Ljava/io/FileInputStream;
    .end local v17    # "OutputStream":Ljava/io/FileOutputStream;
    .end local v19    # "buffer":[B
    .end local v23    # "length":I
    .end local v24    # "fileLength":J
    .end local v28    # "prevProgress":J
    .end local v32    # "totalBytesRead":J
    :goto_1
    return-wide v6

    .line 887
    :cond_9
    const/16 v8, 0x90

    .line 888
    .local v8, "height":I
    const/4 v10, 0x1

    .line 889
    .local v10, "audioCodec":I
    const/4 v11, 0x1

    .line 891
    .local v11, "videoCodec":I
    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getAspectRatio()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_a

    .line 892
    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lcom/lifevibes/videoeditor/MediaVideoItem;->setRenderingMode(I)V

    .line 898
    :cond_a
    const-wide/32 v6, 0x47400

    invoke-virtual/range {v26 .. v26}, Lcom/lifevibes/videoeditor/MediaVideoItem;->getDuration()J

    move-result-wide v12

    const-wide/16 v34, 0x3e8

    div-long v12, v12, v34

    div-long/2addr v6, v12

    const-wide/16 v12, 0x8

    mul-long/2addr v6, v12

    long-to-int v9, v6

    .line 906
    .local v9, "bitrate":I
    add-int/lit16 v9, v9, -0x2fa8

    .line 911
    const v5, 0x7a1200

    if-ge v9, v5, :cond_c

    const v5, 0x4c4b40

    if-lt v9, v5, :cond_c

    .line 913
    const v9, 0x4c4b40

    .line 962
    :goto_2
    const/16 v27, 0x0

    .line 964
    .local v27, "semAcquireDone":Z
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 965
    const/16 v27, 0x1

    .line 966
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    const-wide/32 v12, 0x4b000

    move-object/from16 v6, p2

    move-object/from16 v15, p3

    invoke-virtual/range {v5 .. v15}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->export(Ljava/lang/String;Ljava/lang/String;IIIIJLjava/util/List;Lcom/lifevibes/videoeditor/VideoEditor$ExportProgressListener;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 972
    if-eqz v27, :cond_b

    .line 973
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 979
    :cond_b
    :goto_3
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    .line 985
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v12, 0x4b000

    cmp-long v5, v6, v12

    if-lez v5, :cond_1a

    .line 989
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z

    .line 990
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Generated File size > MAX_MMS_CLIP_SIZE; reduce Input file duration and retry"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 914
    .end local v27    # "semAcquireDone":Z
    :cond_c
    const v5, 0x4c4b40

    if-ge v9, v5, :cond_d

    const v5, 0x1e8480

    if-lt v9, v5, :cond_d

    .line 916
    const v9, 0x1e8480

    goto :goto_2

    .line 917
    :cond_d
    const v5, 0x1e8480

    if-ge v9, v5, :cond_e

    const v5, 0x16e360

    if-lt v9, v5, :cond_e

    .line 919
    const v9, 0x16e360

    goto :goto_2

    .line 920
    :cond_e
    const v5, 0x16e360

    if-ge v9, v5, :cond_f

    const v5, 0x124f80

    if-lt v9, v5, :cond_f

    .line 922
    const v9, 0x124f80

    goto :goto_2

    .line 923
    :cond_f
    const v5, 0x124f80

    if-ge v9, v5, :cond_10

    const v5, 0xf4240

    if-lt v9, v5, :cond_10

    .line 925
    const v9, 0xf4240

    goto :goto_2

    .line 926
    :cond_10
    const v5, 0xf4240

    if-ge v9, v5, :cond_11

    const v5, 0xc3500

    if-lt v9, v5, :cond_11

    .line 928
    const v9, 0xc3500

    goto :goto_2

    .line 929
    :cond_11
    const v5, 0xc3500

    if-ge v9, v5, :cond_12

    const v5, 0x7d000

    if-lt v9, v5, :cond_12

    .line 931
    const v9, 0x7d000

    goto/16 :goto_2

    .line 932
    :cond_12
    const v5, 0x7d000

    if-ge v9, v5, :cond_13

    const v5, 0x5dc00

    if-lt v9, v5, :cond_13

    .line 934
    const v9, 0x5dc00

    goto/16 :goto_2

    .line 935
    :cond_13
    const v5, 0x5dc00

    if-ge v9, v5, :cond_14

    const v5, 0x3e800

    if-lt v9, v5, :cond_14

    .line 937
    const v9, 0x3e800

    goto/16 :goto_2

    .line 938
    :cond_14
    const v5, 0x3e800

    if-ge v9, v5, :cond_15

    const v5, 0x2ee00

    if-lt v9, v5, :cond_15

    .line 940
    const v9, 0x2ee00

    goto/16 :goto_2

    .line 941
    :cond_15
    const v5, 0x2ee00

    if-ge v9, v5, :cond_16

    const v5, 0x1f400

    if-lt v9, v5, :cond_16

    .line 943
    const v9, 0x1f400

    goto/16 :goto_2

    .line 944
    :cond_16
    const v5, 0x1f400

    if-ge v9, v5, :cond_17

    const v5, 0x17700

    if-lt v9, v5, :cond_17

    .line 946
    const v9, 0x17700

    goto/16 :goto_2

    .line 947
    :cond_17
    const v5, 0x17700

    if-ge v9, v5, :cond_18

    const v5, 0xfa00

    if-lt v9, v5, :cond_18

    .line 949
    const v9, 0xfa00

    goto/16 :goto_2

    .line 952
    :cond_18
    const/16 v20, 0x7d00

    .line 955
    .local v20, "dur":I
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fit2ShareMMS: limit file duration to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 969
    .end local v20    # "dur":I
    .restart local v27    # "semAcquireDone":Z
    :catch_1
    move-exception v22

    .line 970
    .local v22, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v5, "VideoEditorImpl"

    const-string v6, "Sem acquire NOT successful in export"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 972
    if-eqz v27, :cond_b

    .line 973
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto/16 :goto_3

    .line 972
    .end local v22    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    if-eqz v27, :cond_19

    .line 973
    invoke-direct/range {p0 .. p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_19
    throw v5

    .line 997
    :cond_1a
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->length()J

    move-result-wide v6

    goto/16 :goto_1
.end method

.method public getAspectRatio()I
    .locals 2

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1177
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Aspect Ratio not supported for MediaAudioItems."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1179
    :cond_0
    iget v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I

    return v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 1190
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    .line 1191
    iget-wide v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mDurationMs:J

    return-wide v0
.end method

.method declared-synchronized getMediaAudioItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaAudioItem;
    .locals 3
    .param p1, "mediaItemId"    # Ljava/lang/String;

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/lifevibes/videoeditor/MediaAudioItem;

    .line 262
    .local v1, "mediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    invoke-virtual {v1}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 266
    .end local v1    # "mediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    :goto_0
    monitor-exit p0

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 261
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method getNativeContext()Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mProjectPath:Ljava/lang/String;

    return-object v0
.end method

.method public release()V
    .locals 4

    .prologue
    .line 1213
    const/4 v1, 0x0

    .line 1215
    .local v1, "semAcquireDone":Z
    :try_start_0
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->lock()V

    .line 1216
    const/4 v1, 0x1

    .line 1218
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    if-eqz v2, :cond_0

    .line 1219
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1220
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;->releaseNativeHelper()V

    .line 1221
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMANativeHelper:Lcom/lifevibes/videoeditor/MediaArtistNativeHelper;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1226
    :cond_0
    if-eqz v1, :cond_1

    .line 1227
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    .line 1230
    :cond_1
    :goto_0
    return-void

    .line 1223
    :catch_0
    move-exception v0

    .line 1224
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "VideoEditorImpl"

    const-string v3, "Sem acquire NOT successful in export"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1226
    if-eqz v1, :cond_1

    .line 1227
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    goto :goto_0

    .line 1226
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    .line 1227
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->unlock()V

    :cond_2
    throw v2
.end method

.method public declared-synchronized removeMediaAudioItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaAudioItem;
    .locals 4
    .param p1, "mediaItemId"    # Ljava/lang/String;

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/lifevibes/videoeditor/MediaAudioItem;

    invoke-virtual {v2}, Lcom/lifevibes/videoeditor/MediaAudioItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "firstItemString":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->getMediaAudioItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaAudioItem;

    move-result-object v1

    .line 250
    .local v1, "mediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    if-eqz v1, :cond_0

    .line 255
    iget-object v2, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaAudioItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    :cond_0
    monitor-exit p0

    return-object v1

    .line 248
    .end local v0    # "firstItemString":Ljava/lang/String;
    .end local v1    # "mediaItem":Lcom/lifevibes/videoeditor/MediaAudioItem;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized removeMediaItem(Ljava/lang/String;)Lcom/lifevibes/videoeditor/MediaItem;
    .locals 3
    .param p1, "mediaItemId"    # Ljava/lang/String;

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 208
    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/lifevibes/videoeditor/MediaItem;

    .line 209
    .local v0, "mediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    if-eqz v0, :cond_0

    .line 210
    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mMediaItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 213
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mIsList3D:Z

    .line 214
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mIsFirstVideoItemAdded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    .end local v0    # "mediaItem":Lcom/lifevibes/videoeditor/MediaItem;
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setAspectRatio(I)V
    .locals 1
    .param p1, "aspectRatio"    # I

    .prologue
    .line 1236
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/lifevibes/videoeditor/VideoEditorImpl;->mAspectRatio:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1237
    monitor-exit p0

    return-void

    .line 1236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method updateTimelineDuration()V
    .locals 0

    .prologue
    .line 1198
    invoke-direct {p0}, Lcom/lifevibes/videoeditor/VideoEditorImpl;->computeTimelineDuration()V

    .line 1199
    return-void
.end method
