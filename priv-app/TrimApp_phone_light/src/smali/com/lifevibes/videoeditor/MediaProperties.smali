.class public Lcom/lifevibes/videoeditor/MediaProperties;
.super Ljava/lang/Object;
.source "MediaProperties.java"


# static fields
.field public static final ACODEC_AAC_LC:I = 0x2

.field public static final ACODEC_AAC_PLUS:I = 0x3

.field public static final ACODEC_AMRNB:I = 0x1

.field public static final ACODEC_AMRWB:I = 0x8

.field public static final ACODEC_ENHANCED_AAC_PLUS:I = 0x4

.field public static final ACODEC_EVRC:I = 0x6

.field public static final ACODEC_MP3:I = 0x5

.field public static final ACODEC_NO_AUDIO:I = 0x0

.field public static final ACODEC_OGG:I = 0x9

.field private static final ASPECT_RATIOS:[I

.field public static final ASPECT_RATIO_11_9:I = 0x5

.field private static final ASPECT_RATIO_11_9_RESOLUTIONS:[Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASPECT_RATIO_16_9:I = 0x2

.field private static final ASPECT_RATIO_16_9_RESOLUTIONS:[Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASPECT_RATIO_3_2:I = 0x1

.field private static final ASPECT_RATIO_3_2_RESOLUTIONS:[Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASPECT_RATIO_4_3:I = 0x3

.field private static final ASPECT_RATIO_4_3_RESOLUTIONS:[Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASPECT_RATIO_5_3:I = 0x4

.field private static final ASPECT_RATIO_5_3_RESOLUTIONS:[Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ASPECT_RATIO_UNDEFINED:I = 0x0

.field public static final AUDIO_MAX_TRACK_COUNT:I = 0x1

.field public static final AUDIO_MAX_VOLUME_PERCENT:I = 0x64

.field public static final BITRATE_128K:I = 0x1f400

.field public static final BITRATE_192K:I = 0x2ee00

.field public static final BITRATE_1M:I = 0xf4240

.field public static final BITRATE_1_2M:I = 0x124f80

.field public static final BITRATE_1_5M:I = 0x16e360

.field public static final BITRATE_256K:I = 0x3e800

.field public static final BITRATE_28K:I = 0x6d60

.field public static final BITRATE_2M:I = 0x1e8480

.field public static final BITRATE_384K:I = 0x5dc00

.field public static final BITRATE_40K:I = 0x9c40

.field public static final BITRATE_512K:I = 0x7d000

.field public static final BITRATE_5M:I = 0x4c4b40

.field public static final BITRATE_64K:I = 0xfa00

.field public static final BITRATE_800K:I = 0xc3500

.field public static final BITRATE_8M:I = 0x7a1200

.field public static final BITRATE_96K:I = 0x17700

.field public static final DEFAULT_CHANNEL_COUNT:I = 0x1

.field public static final DEFAULT_SAMPLING_FREQUENCY:I = 0x7d00

.field public static final DISPLAY_ANGLE_0:I = 0x64

.field public static final DISPLAY_ANGLE_180:I = 0x66

.field public static final DISPLAY_ANGLE_270:I = 0x67

.field public static final DISPLAY_ANGLE_90:I = 0x65

.field public static final DISPLAY_ANGLE_UNCHANGED:I = 0x0

.field public static final FILE_3GP:I = 0x0

.field public static final FILE_AMR:I = 0x2

.field public static final FILE_AVI:I = 0xb

.field public static final FILE_JPEG:I = 0x5

.field public static final FILE_M4V:I = 0xa

.field public static final FILE_MKV:I = 0xc

.field public static final FILE_MP3:I = 0x3

.field public static final FILE_MP4:I = 0x1

.field public static final FILE_PNG:I = 0x8

.field public static final FILE_UNSUPPORTED:I = 0xff

.field public static final H263_PROFILE_0_LEVEL_10:I = 0x0

.field public static final H263_PROFILE_0_LEVEL_20:I = 0x1

.field public static final H263_PROFILE_0_LEVEL_30:I = 0x2

.field public static final H263_PROFILE_0_LEVEL_40:I = 0x3

.field public static final H263_PROFILE_0_LEVEL_45:I = 0x4

.field public static final H264_PROFILE_0_LEVEL_1:I = 0x96

.field public static final H264_PROFILE_0_LEVEL_1B:I = 0x97

.field public static final H264_PROFILE_0_LEVEL_1_1:I = 0x98

.field public static final H264_PROFILE_0_LEVEL_1_2:I = 0x99

.field public static final H264_PROFILE_0_LEVEL_1_3:I = 0x9a

.field public static final H264_PROFILE_0_LEVEL_2:I = 0x9b

.field public static final H264_PROFILE_0_LEVEL_2_1:I = 0x9c

.field public static final H264_PROFILE_0_LEVEL_2_2:I = 0x9d

.field public static final H264_PROFILE_0_LEVEL_3:I = 0x9e

.field public static final H264_PROFILE_0_LEVEL_3_1:I = 0x9f

.field public static final H264_PROFILE_0_LEVEL_3_2:I = 0xa0

.field public static final H264_PROFILE_0_LEVEL_4:I = 0xa1

.field public static final H264_PROFILE_0_LEVEL_4_1:I = 0xa2

.field public static final H264_PROFILE_0_LEVEL_4_2:I = 0xa3

.field public static final H264_PROFILE_0_LEVEL_5:I = 0xa4

.field public static final H264_PROFILE_0_LEVEL_5_1:I = 0xa5

.field public static final H264_PROFILE_H_LEVEL_1:I = 0xd2

.field public static final H264_PROFILE_H_LEVEL_1B:I = 0xd3

.field public static final H264_PROFILE_H_LEVEL_1_1:I = 0xd4

.field public static final H264_PROFILE_H_LEVEL_1_2:I = 0xd5

.field public static final H264_PROFILE_H_LEVEL_1_3:I = 0xd6

.field public static final H264_PROFILE_H_LEVEL_2:I = 0xd7

.field public static final H264_PROFILE_H_LEVEL_2_1:I = 0xd8

.field public static final H264_PROFILE_H_LEVEL_2_2:I = 0xd9

.field public static final H264_PROFILE_H_LEVEL_3:I = 0xda

.field public static final H264_PROFILE_H_LEVEL_3_1:I = 0xdb

.field public static final H264_PROFILE_H_LEVEL_3_2:I = 0xdc

.field public static final H264_PROFILE_H_LEVEL_4:I = 0xdd

.field public static final H264_PROFILE_H_LEVEL_4_1:I = 0xde

.field public static final H264_PROFILE_H_LEVEL_4_2:I = 0xdf

.field public static final H264_PROFILE_H_LEVEL_5:I = 0xe0

.field public static final H264_PROFILE_H_LEVEL_5_1:I = 0xe1

.field public static final H264_PROFILE_M_LEVEL_1:I = 0xb4

.field public static final H264_PROFILE_M_LEVEL_1B:I = 0xb5

.field public static final H264_PROFILE_M_LEVEL_1_1:I = 0xb6

.field public static final H264_PROFILE_M_LEVEL_1_2:I = 0xb7

.field public static final H264_PROFILE_M_LEVEL_1_3:I = 0xb8

.field public static final H264_PROFILE_M_LEVEL_2:I = 0xb9

.field public static final H264_PROFILE_M_LEVEL_2_1:I = 0xba

.field public static final H264_PROFILE_M_LEVEL_2_2:I = 0xbb

.field public static final H264_PROFILE_M_LEVEL_3:I = 0xbc

.field public static final H264_PROFILE_M_LEVEL_3_1:I = 0xbd

.field public static final H264_PROFILE_M_LEVEL_3_2:I = 0xbe

.field public static final H264_PROFILE_M_LEVEL_4:I = 0xbf

.field public static final H264_PROFILE_M_LEVEL_4_1:I = 0xc0

.field public static final H264_PROFILE_M_LEVEL_4_2:I = 0xc1

.field public static final H264_PROFILE_M_LEVEL_5:I = 0xc2

.field public static final H264_PROFILE_M_LEVEL_5_1:I = 0xc3

.field public static final HEIGHT_1080:I = 0x438

.field public static final HEIGHT_120:I = 0x78

.field public static final HEIGHT_144:I = 0x90

.field public static final HEIGHT_240:I = 0xf0

.field public static final HEIGHT_288:I = 0x120

.field public static final HEIGHT_360:I = 0x168

.field public static final HEIGHT_480:I = 0x1e0

.field public static final HEIGHT_720:I = 0x2d0

.field public static final HEIGHT_96:I = 0x60

.field public static final MAX_MMS_CLIP_MOOV_SIZE:I = 0x3c00

.field public static final MAX_MMS_CLIP_SIZE:I = 0x4b000

.field public static final MPEG4_SP_LEVEL_0:I = 0x32

.field public static final MPEG4_SP_LEVEL_0B:I = 0x33

.field public static final MPEG4_SP_LEVEL_1:I = 0x34

.field public static final MPEG4_SP_LEVEL_2:I = 0x35

.field public static final MPEG4_SP_LEVEL_3:I = 0x36

.field public static final MPEG4_SP_LEVEL_4A:I = 0x37

.field public static final MPEG4_SP_LEVEL_5:I = 0x38

.field public static final MPEG4_SP_LEVEL_6:I = 0x39

.field public static final SAMPLES_PER_FRAME_AAC:I = 0x400

.field public static final SAMPLES_PER_FRAME_AMRNB:I = 0xa0

.field public static final SAMPLES_PER_FRAME_AMRWB:I = 0x140

.field public static final SAMPLES_PER_FRAME_EAAC:I = 0x800

.field public static final SAMPLES_PER_FRAME_MP3:I = 0x480

.field private static final SUPPORTED_ACODECS:[I

.field private static final SUPPORTED_BITRATES:[I

.field private static final SUPPORTED_VCODECS:[I

.field private static final SUPPORTED_VCODEC_PROFILE_LEVELS:[I

.field private static final SUPPORTED_VIDEO_FILE_FORMATS:[I

.field public static final UNSUPPORTED_PROFILE_LEVEL:I = 0xff

.field public static final VCODEC_H263:I = 0x1

.field public static final VCODEC_H264BP:I = 0x4

.field public static final VCODEC_H264HP:I = 0x6

.field public static final VCODEC_H264MP:I = 0x5

.field public static final VCODEC_MPEG4:I = 0x2

.field public static final VCODEC_NOVIDEO:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x1e0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 89
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIOS:[I

    .line 101
    new-array v0, v7, [Landroid/util/Pair;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x2d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v5

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x438

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x2d0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_3_2_RESOLUTIONS:[Landroid/util/Pair;

    .line 108
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/util/Pair;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x60

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v5

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0xa0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x78

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x140

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xf0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x280

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v8

    const/4 v1, 0x4

    new-instance v2, Landroid/util/Pair;

    const/16 v3, 0x3c0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x2d0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_4_3_RESOLUTIONS:[Landroid/util/Pair;

    .line 118
    new-array v0, v6, [Landroid/util/Pair;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x320

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_5_3_RESOLUTIONS:[Landroid/util/Pair;

    .line 124
    new-array v0, v7, [Landroid/util/Pair;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0xb0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x90

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v5

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x160

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x120

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_11_9_RESOLUTIONS:[Landroid/util/Pair;

    .line 131
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/util/Pair;

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x280

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x168

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v5

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x356

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v6

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x500

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x2d0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v7

    new-instance v1, Landroid/util/Pair;

    const/16 v2, 0x780

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x438

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v0, v8

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_16_9_RESOLUTIONS:[Landroid/util/Pair;

    .line 162
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_BITRATES:[I

    .line 195
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_VCODECS:[I

    .line 276
    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_VCODEC_PROFILE_LEVELS:[I

    .line 357
    new-array v0, v8, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_ACODECS:[I

    .line 411
    new-array v0, v8, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_VIDEO_FILE_FORMATS:[I

    return-void

    .line 89
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
    .end array-data

    .line 162
    :array_1
    .array-data 4
        0x6d60
        0x9c40
        0xfa00
        0x17700
        0x1f400
        0x2ee00
        0x3e800
        0x5dc00
        0x7d000
        0xc3500
        0xf4240
        0x124f80
        0x16e360
        0x1e8480
        0x4c4b40
        0x7a1200
    .end array-data

    .line 195
    :array_2
    .array-data 4
        0x1
        0x2
        0x4
        0x5
        0x6
    .end array-data

    .line 276
    :array_3
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x32
        0x33
        0x34
        0x35
        0x36
        0x37
        0x38
        0x96
        0x97
        0x98
        0x99
        0x9a
        0x9b
        0x9c
        0x9d
        0x9e
        0x9f
        0xa0
        0xa1
        0xa2
        0xa3
        0xa4
        0xa5
        0xb4
        0xb5
        0xb6
        0xb7
        0xb8
        0xb9
        0xba
        0xbb
        0xbc
        0xbd
        0xbe
        0xbf
        0xc0
        0xc1
        0xc2
        0xc3
        0xd2
        0xd3
        0xd4
        0xd5
        0xd6
        0xd7
        0xd8
        0xd9
        0xda
        0xdb
        0xdc
        0xdd
        0xde
        0xdf
        0xe0
        0xe1
        0xff
    .end array-data

    .line 357
    :array_4
    .array-data 4
        0x2
        0x1
        0x8
    .end array-data

    .line 411
    :array_5
    .array-data 4
        0x0
        0x1
        0xa
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 431
    return-void
.end method

.method public static getAllSupportedAspectRatios()[I
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIOS:[I

    return-object v0
.end method

.method public static getSupportedAudioCodecs()[I
    .locals 1

    .prologue
    .line 494
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_ACODECS:[I

    return-object v0
.end method

.method public static getSupportedAudioTrackCount()I
    .locals 1

    .prologue
    .line 522
    const/4 v0, 0x1

    return v0
.end method

.method public static getSupportedMaxVolume()I
    .locals 1

    .prologue
    .line 515
    const/16 v0, 0x64

    return v0
.end method

.method public static getSupportedResolutions(I)[Landroid/util/Pair;
    .locals 4
    .param p0, "aspectRatio"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449
    packed-switch p0, :pswitch_data_0

    .line 476
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown aspect ratio: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 451
    :pswitch_0
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_3_2_RESOLUTIONS:[Landroid/util/Pair;

    .line 480
    .local v0, "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    return-object v0

    .line 456
    .end local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_1
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_4_3_RESOLUTIONS:[Landroid/util/Pair;

    .line 457
    .restart local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 461
    .end local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_2
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_5_3_RESOLUTIONS:[Landroid/util/Pair;

    .line 462
    .restart local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 466
    .end local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_3
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_11_9_RESOLUTIONS:[Landroid/util/Pair;

    .line 467
    .restart local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 471
    .end local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :pswitch_4
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->ASPECT_RATIO_16_9_RESOLUTIONS:[Landroid/util/Pair;

    .line 472
    .restart local v0    # "resolutions":[Landroid/util/Pair;, "[Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    goto :goto_0

    .line 449
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getSupportedVideoBitrates()[I
    .locals 1

    .prologue
    .line 508
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_BITRATES:[I

    return-object v0
.end method

.method public static getSupportedVideoCodecs()[I
    .locals 1

    .prologue
    .line 487
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_VCODECS:[I

    return-object v0
.end method

.method public static getSupportedVideoFileFormat()[I
    .locals 1

    .prologue
    .line 501
    sget-object v0, Lcom/lifevibes/videoeditor/MediaProperties;->SUPPORTED_VIDEO_FILE_FORMATS:[I

    return-object v0
.end method
