.class public Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncAdapter.java"


# static fields
.field private static final BIND_INTENT:Ljava/lang/String; = "com.sec.android.sCloudSync.SNote3"

.field public static final META_NAME:Ljava/lang/String; = "SyncMetaDataSNote3"

.field private static final TAG:Ljava/lang/String; = "SyncAdapter-SNote3-Proxy"

.field private static final sServiceLock:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataSyncConnection:Landroid/content/ServiceConnection;

.field private mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "autoInitialize"    # Z

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 39
    iput-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;-><init>(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mDataSyncConnection:Landroid/content/ServiceConnection;

    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;Lcom/sec/android/sCloudSync/IDataSync/IDataSync;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;
    .param p1, "x1"    # Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    return-object v0
.end method

.method private declared-synchronized unbindService()V
    .locals 2

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mDataSyncConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_0
    monitor-exit p0

    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    const/4 v7, 0x1

    .line 95
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "onPerformSync - started."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0, p3}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->syncRequestControl(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v7, :cond_0

    .line 99
    invoke-static {p1, p3, p2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 100
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "onPerformSync - ignored and finished."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :goto_0
    return-void

    .line 104
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.sec.android.sCloudSync.SNote3"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    .local v1, "bindIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mDataSyncConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v5, v1, v6, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 107
    .local v0, "bResult":Z
    if-nez v0, :cond_1

    .line 108
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "bind DataSync service is FAILED."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_1
    sget-object v5, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    .line 114
    .local v2, "dataSyncResult":Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    invoke-virtual {v2, p2}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setBundle(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {v2, p5}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->setSyncResult(Landroid/content/SyncResult;)V

    .line 117
    iget-object v5, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    if-nez v5, :cond_2

    .line 119
    sget-object v6, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    monitor-enter v6

    .line 121
    :try_start_0
    sget-object v5, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :goto_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :cond_2
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    if-eqz v5, :cond_3

    .line 131
    iget-object v5, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v5, v6, p3, v2}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync;->performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;

    move-result-object v2

    .line 132
    invoke-virtual {v2}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->getSyncResult()Landroid/content/SyncResult;

    move-result-object v4

    .line 135
    .local v4, "syncResultfromDataSync":Landroid/content/SyncResult;
    iget-boolean v5, v4, Landroid/content/SyncResult;->databaseError:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->databaseError:Z

    .line 136
    iget-wide v6, v4, Landroid/content/SyncResult;->delayUntil:J

    iput-wide v6, p5, Landroid/content/SyncResult;->delayUntil:J

    .line 137
    iget-boolean v5, v4, Landroid/content/SyncResult;->fullSyncRequested:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 138
    iget-boolean v5, v4, Landroid/content/SyncResult;->moreRecordsToGet:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->moreRecordsToGet:Z

    .line 139
    iget-boolean v5, v4, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    .line 140
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numAuthExceptions:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 141
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    .line 142
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numDeletes:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numDeletes:J

    .line 143
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numEntries:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numEntries:J

    .line 144
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numInserts:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numInserts:J

    .line 145
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numIoExceptions:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numIoExceptions:J

    .line 146
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numParseExceptions:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numParseExceptions:J

    .line 147
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numSkippedEntries:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numSkippedEntries:J

    .line 148
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-object v6, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numUpdates:J

    iput-wide v6, v5, Landroid/content/SyncStats;->numUpdates:J

    .line 149
    iget-boolean v5, v4, Landroid/content/SyncResult;->tooManyDeletions:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->tooManyDeletions:Z

    .line 150
    iget-boolean v5, v4, Landroid/content/SyncResult;->tooManyRetries:Z

    iput-boolean v5, p5, Landroid/content/SyncResult;->tooManyRetries:Z

    .line 153
    invoke-direct {p0}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->unbindService()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 163
    .end local v4    # "syncResultfromDataSync":Landroid/content/SyncResult;
    :goto_2
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "onPerformSync - finished."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    :catch_0
    move-exception v3

    .line 123
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v7, "onPerformSync - interrupted."

    invoke-static {v5, v7}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 125
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 157
    :cond_3
    :try_start_4
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "DATA SYNC service is not connected."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 160
    :catch_1
    move-exception v3

    .line 161
    .local v3, "e":Landroid/os/RemoteException;
    const-string v5, "SyncAdapter-SNote3-Proxy"

    const-string v6, "onPerformSync: FAILED - IDataSync."

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onSyncCanceled()V
    .locals 3

    .prologue
    .line 176
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    .line 177
    const-string v1, "SyncAdapter-SNote3-Proxy"

    const-string v2, "onSyncCanceled - started."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    invoke-interface {v1}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync;->cancelSync()V

    .line 182
    invoke-direct {p0}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->unbindService()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :goto_0
    const-string v1, "SyncAdapter-SNote3-Proxy"

    const-string v2, "onSyncCanceled - finished."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void

    .line 186
    :cond_0
    :try_start_1
    sget-object v2, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 188
    :try_start_2
    sget-object v1, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 189
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SyncAdapter-SNote3-Proxy"

    const-string v2, "onSyncCanceled: FAILED - IDataSync."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected declared-synchronized syncRequestControl(Ljava/lang/String;)Z
    .locals 7
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mContext:Landroid/content/Context;

    const-string v5, "SyncMetaDataSNote3"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 80
    .local v1, "syncMeta":Landroid/content/SharedPreferences;
    const/4 v4, 0x1

    invoke-interface {v1, p1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 82
    .local v0, "firstRequest":Z
    if-ne v0, v2, :cond_0

    .line 84
    const-string v3, "SyncAdapter-SNote3-Proxy"

    const-string v4, "First request"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, p1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :goto_0
    monitor-exit p0

    return v2

    :cond_0
    move v2, v3

    goto :goto_0

    .line 79
    .end local v0    # "firstRequest":Z
    .end local v1    # "syncMeta":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
