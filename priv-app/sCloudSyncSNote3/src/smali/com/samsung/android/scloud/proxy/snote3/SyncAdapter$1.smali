.class Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;
.super Ljava/lang/Object;
.source "SyncAdapter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;->this$0:Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;->this$0:Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;

    invoke-static {p2}, Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->access$002(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;Lcom/sec/android/sCloudSync/IDataSync/IDataSync;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    .line 52
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;->this$0:Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;

    # getter for: Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    invoke-static {v0}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->access$000(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    move-result-object v0

    if-nez v0, :cond_0

    .line 53
    const-string v0, "SyncAdapter-SNote3-Proxy"

    const-string v1, "onServiceConnected : There is BindingService Error.."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :goto_0
    # getter for: Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->access$100()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 60
    :try_start_0
    # getter for: Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->sServiceLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->access$100()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 61
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    return-void

    .line 56
    :cond_0
    const-string v0, "SyncAdapter-SNote3-Proxy"

    const-string v1, "onServiceConnected : Binded.."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter$1;->this$0:Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->mService:Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;->access$002(Lcom/samsung/android/scloud/proxy/snote3/SyncAdapter;Lcom/sec/android/sCloudSync/IDataSync/IDataSync;)Lcom/sec/android/sCloudSync/IDataSync/IDataSync;

    .line 67
    const-string v0, "SyncAdapter-SNote3-Proxy"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/proxy/snote3/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void
.end method
