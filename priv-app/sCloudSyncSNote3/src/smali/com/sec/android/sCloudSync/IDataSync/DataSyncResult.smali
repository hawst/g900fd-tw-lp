.class public final Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
.super Ljava/lang/Object;
.source "DataSyncResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mSyncResult:Landroid/content/SyncResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult$1;

    invoke-direct {v0}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult$1;-><init>()V

    sput-object v0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-eqz p1, :cond_0

    .line 36
    sget-object v0, Landroid/content/SyncResult;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncResult;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mSyncResult:Landroid/content/SyncResult;

    .line 37
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mBundle:Landroid/os/Bundle;

    .line 39
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getSyncResult()Landroid/content/SyncResult;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mSyncResult:Landroid/content/SyncResult;

    return-object v0
.end method

.method public setBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mBundle:Landroid/os/Bundle;

    .line 26
    return-void
.end method

.method public setSyncResult(Landroid/content/SyncResult;)V
    .locals 0
    .param p1, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mSyncResult:Landroid/content/SyncResult;

    .line 16
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mSyncResult:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, p2}, Landroid/content/SyncResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 61
    return-void
.end method
