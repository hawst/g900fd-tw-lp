.class public final enum Lcom/sec/spp/push/notisvc/card/e;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/card/e;

.field public static final enum b:Lcom/sec/spp/push/notisvc/card/e;

.field public static final enum c:Lcom/sec/spp/push/notisvc/card/e;

.field private static final d:I

.field private static e:[Lcom/sec/spp/push/notisvc/card/e;

.field private static final synthetic f:[Lcom/sec/spp/push/notisvc/card/e;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/spp/push/notisvc/card/e;

    const-string v1, "REQUEST_RESOURCE"

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/notisvc/card/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/e;->a:Lcom/sec/spp/push/notisvc/card/e;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/e;

    const-string v1, "DISPLAY"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/notisvc/card/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/e;->b:Lcom/sec/spp/push/notisvc/card/e;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/e;

    const-string v1, "DISMISS"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/notisvc/card/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/e;->c:Lcom/sec/spp/push/notisvc/card/e;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/card/e;

    sget-object v1, Lcom/sec/spp/push/notisvc/card/e;->a:Lcom/sec/spp/push/notisvc/card/e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/notisvc/card/e;->b:Lcom/sec/spp/push/notisvc/card/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/card/e;->c:Lcom/sec/spp/push/notisvc/card/e;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/spp/push/notisvc/card/e;->f:[Lcom/sec/spp/push/notisvc/card/e;

    const-class v0, Lcom/sec/spp/push/notisvc/card/e;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    sput v0, Lcom/sec/spp/push/notisvc/card/e;->d:I

    sget v0, Lcom/sec/spp/push/notisvc/card/e;->d:I

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/card/e;

    sput-object v0, Lcom/sec/spp/push/notisvc/card/e;->e:[Lcom/sec/spp/push/notisvc/card/e;

    const-class v0, Lcom/sec/spp/push/notisvc/card/e;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/e;

    sget-object v2, Lcom/sec/spp/push/notisvc/card/e;->e:[Lcom/sec/spp/push/notisvc/card/e;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/e;->ordinal()I

    move-result v3

    aput-object v0, v2, v3

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/card/e;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/card/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/e;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/card/e;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/card/e;->f:[Lcom/sec/spp/push/notisvc/card/e;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/card/e;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
