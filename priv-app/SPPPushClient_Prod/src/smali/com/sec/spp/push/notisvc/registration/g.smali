.class Lcom/sec/spp/push/notisvc/registration/g;
.super Lcom/sec/spp/push/notisvc/agent/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/registration/f;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method private constructor <init>(Lcom/sec/spp/push/notisvc/registration/f;Landroid/content/Context;Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/registration/g;->a:Lcom/sec/spp/push/notisvc/registration/f;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/agent/h;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/spp/push/notisvc/registration/f;Landroid/content/Context;Lcom/sec/spp/push/notisvc/registration/k;Lcom/sec/spp/push/notisvc/registration/g;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/spp/push/notisvc/registration/g;-><init>(Lcom/sec/spp/push/notisvc/registration/f;Landroid/content/Context;Lcom/sec/spp/push/notisvc/registration/k;)V

    return-void
.end method

.method private a()V
    .locals 7

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    const-string v2, "regitimer:dvcRegi"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x36ee80

    add-long/2addr v3, v5

    iget-object v5, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v5}, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    return-void
.end method

.method private b()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "discard DVCRegi. regiStatusFlag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->a:Lcom/sec/spp/push/notisvc/registration/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/f;->a(Z)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "Callback Method. on Success. status is null."

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v2

    if-ne v1, v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DVCRegistration Success. status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;I)V

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/b;->g(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/h;->j(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-string v1, "DVCRegistration Success, but status already chaned."

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "successed status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", before status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/h;->j(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public b(I)V
    .locals 3

    const/16 v2, 0x1f4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DVCRegistration Fail. status : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->a:Lcom/sec/spp/push/notisvc/registration/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/f;->a(Z)V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x1e5

    if-ne p1, v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->a:Lcom/sec/spp/push/notisvc/registration/f;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/f;->a(Lcom/sec/spp/push/notisvc/registration/f;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->a:Lcom/sec/spp/push/notisvc/registration/f;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/f;->a(Lcom/sec/spp/push/notisvc/registration/f;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    const-string v0, "DVCRegistration. fail:invalid appid. retry spp regi"

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/f;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;I)V

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/push/a;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/push/a;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/g;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/push/a;->b(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->a()V

    goto :goto_0

    :cond_3
    const/16 v0, 0x190

    if-lt p1, v0, :cond_4

    if-ge p1, v2, :cond_4

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->b()V

    goto :goto_0

    :cond_4
    if-lt p1, v2, :cond_5

    const/16 v0, 0x258

    if-ge p1, v0, :cond_5

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->a()V

    goto :goto_0

    :cond_5
    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_6

    const/16 v0, 0x3eb

    if-eq p1, v0, :cond_6

    const/16 v0, 0x3ee

    if-ne p1, v0, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->a()V

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/g;->b()V

    goto :goto_0
.end method
