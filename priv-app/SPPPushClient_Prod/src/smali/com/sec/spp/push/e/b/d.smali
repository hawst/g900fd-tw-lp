.class public Lcom/sec/spp/push/e/b/d;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/jboss/netty/channel/ChannelPipelineFactory;


# instance fields
.field private final a:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/sec/spp/push/e/b/d;->a:Z

    return-void
.end method

.method public static a(Z)Lcom/sec/spp/push/e/b/d;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/e/b/d;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/e/b/d;-><init>(Z)V

    return-object v0
.end method


# virtual methods
.method public getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;
    .locals 4

    invoke-static {}, Lorg/jboss/netty/channel/Channels;->pipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/spp/push/e/b/d;->a:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/c/a;->a()Ljavax/net/ssl/SSLContext;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "samsung.com"

    const/16 v3, 0x1bb

    invoke-virtual {v1, v2, v3}, Ljavax/net/ssl/SSLContext;->createSSLEngine(Ljava/lang/String;I)Ljavax/net/ssl/SSLEngine;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    const-string v2, "ssl"

    new-instance v3, Lorg/jboss/netty/handler/ssl/SslHandler;

    invoke-direct {v3, v1}, Lorg/jboss/netty/handler/ssl/SslHandler;-><init>(Ljavax/net/ssl/SSLEngine;)V

    invoke-interface {v0, v2, v3}, Lorg/jboss/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lorg/jboss/netty/channel/ChannelHandler;)V

    :cond_0
    const-string v1, "decoder"

    new-instance v2, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;

    invoke-direct {v2}, Lcom/sec/pns/msg/frontend/MsgFrontendDecoderForDevice;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/jboss/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lorg/jboss/netty/channel/ChannelHandler;)V

    const-string v1, "encoder"

    new-instance v2, Lcom/sec/pns/msg/frontend/MsgFrontendEncoderForDevice;

    invoke-direct {v2}, Lcom/sec/pns/msg/frontend/MsgFrontendEncoderForDevice;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/jboss/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lorg/jboss/netty/channel/ChannelHandler;)V

    const-string v1, "message channel handler"

    new-instance v2, Lcom/sec/spp/push/e/b/e;

    invoke-direct {v2}, Lcom/sec/spp/push/e/b/e;-><init>()V

    invoke-interface {v0, v1, v2}, Lorg/jboss/netty/channel/ChannelPipeline;->addLast(Ljava/lang/String;Lorg/jboss/netty/channel/ChannelHandler;)V

    return-object v0
.end method
