.class Lcom/sec/spp/push/notisvc/b/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/b/c;

.field private b:Z

.field private final synthetic c:Lcom/sec/spp/push/notisvc/b/h;

.field private final synthetic d:Landroid/content/Context;

.field private final synthetic e:Ljava/lang/String;

.field private final synthetic f:Lcom/sec/spp/push/notisvc/b/j;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/b/c;Lcom/sec/spp/push/notisvc/b/h;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/b/j;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/b/d;->a:Lcom/sec/spp/push/notisvc/b/c;

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/b/d;->d:Landroid/content/Context;

    iput-object p4, p0, Lcom/sec/spp/push/notisvc/b/d;->e:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/spp/push/notisvc/b/d;->f:Lcom/sec/spp/push/notisvc/b/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/spp/push/notisvc/b/d;->b:Z

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/sec/spp/push/notisvc/b/d;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/spp/push/notisvc/b/d;->b:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/b/h;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/d;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/notisvc/b/h;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/d;->d:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/spp/push/notisvc/b/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Lcom/sec/spp/push/notisvc/b/h;->a(Landroid/content/Context;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/sec/spp/push/notisvc/card/j; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->f:Lcom/sec/spp/push/notisvc/b/j;

    const/high16 v4, 0x43960000    # 300.0f

    iget-object v5, p0, Lcom/sec/spp/push/notisvc/b/d;->d:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/sec/spp/push/notisvc/b/c;->a(FLandroid/content/Context;)F

    move-result v4

    float-to-int v6, v4

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/sec/spp/push/notisvc/b/j;->a(IIIIII)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->a:Lcom/sec/spp/push/notisvc/b/c;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/d;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/b/c;->a(Landroid/content/Context;)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/b/h;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/d;->c:Lcom/sec/spp/push/notisvc/b/h;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/b/h;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "NotSupportedTypeException. "

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
