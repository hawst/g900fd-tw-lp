.class Lcom/sec/spp/push/dlc/util/n;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/util/PushActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/dlc/util/PushActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/util/n;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.REQUEST_DEREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXTRA_PACKAGENAME"

    const-string v2, "com.sec.spp.push"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "EXTRA_INTENTFILTER"

    const-string v2, "com.sec.spp.push.DLC_REPLY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/util/n;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
