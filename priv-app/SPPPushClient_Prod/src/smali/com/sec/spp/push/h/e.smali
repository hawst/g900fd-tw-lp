.class Lcom/sec/spp/push/h/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/util/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/h/c;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/h/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    iput-object p2, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/h/e;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/sec/spp/push/util/j;)V
    .locals 3

    const/16 v0, -0x6b

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/util/j;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/e;

    iget-object v1, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/e;Ljava/lang/String;Z)V

    return-void
.end method

.method private c(Lcom/sec/spp/push/util/j;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/e;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/e;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/spp/push/h/c;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/e;

    iget-object v1, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/e;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Dereg] onFail. errorCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    iget-object v2, p0, Lcom/sec/spp/push/h/e;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v5, 0xfa8

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/c/b;->a(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReqest. onSuccess"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "msgInfo.resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "msgInfo.resultMsg="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "msgInfo.appId="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/e;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReq. onSuccess. But Error status"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    new-instance v1, Lcom/sec/spp/push/c/b;

    invoke-direct {v1}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;Lcom/sec/spp/push/c/b;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    invoke-static {v0}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;)Lcom/sec/spp/push/c/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/c/b;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;Lcom/sec/spp/push/c/b;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/e;

    iget-object v1, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/e;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isRegistrationTableEmpty() == true, STOP Service"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReqest.onSuccess. context==null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/sec/spp/push/h/e;->a:Lcom/sec/spp/push/h/c;

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/e;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/e;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/spp/push/h/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/e;->b(Lcom/sec/spp/push/util/j;)V

    :goto_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReq. DeReg onSuccess() About to execute next task in pending queue"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->f()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/e;->c(Lcom/sec/spp/push/util/j;)V

    goto :goto_2

    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendDeregistrationReqest. STOP Service isStopped="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0xfa4 -> :sswitch_0
    .end sparse-switch
.end method
