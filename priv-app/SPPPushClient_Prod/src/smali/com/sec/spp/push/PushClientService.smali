.class public Lcom/sec/spp/push/PushClientService;
.super Landroid/app/Service;


# instance fields
.field private final a:Lcom/sec/spp/push/f;

.field private final b:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/sec/spp/push/t;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/t;-><init>(Lcom/sec/spp/push/PushClientService;)V

    iput-object v0, p0, Lcom/sec/spp/push/PushClientService;->a:Lcom/sec/spp/push/f;

    new-instance v0, Lcom/sec/spp/push/u;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/u;-><init>(Lcom/sec/spp/push/PushClientService;)V

    iput-object v0, p0, Lcom/sec/spp/push/PushClientService;->b:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private declared-synchronized a(Landroid/content/Intent;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    const-string v0, "reqType"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "appId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "userdata"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "userSN"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "[PushClientService]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processPushRequest => REQTYPE : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", APPID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", USERDATA : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", USERSN "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/spp/push/h/c;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "[PushClientService]"

    const-string v1, "Error, Undefined requestType."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/spp/push/PushClientService;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/PushClientService;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[PushClientService]"

    const-string v1, "deInitPushClientService()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientService;->b()V

    const-string v0, "[PushClientService]"

    const-string v1, "<<--- PushClientService.deInitPushClientService -------START  --->>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/PushClientApplication;

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/PushClientApplication;->a(Z)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_1
    .catch Lcom/sec/spp/push/c/a; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->e()V

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->f()V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->e()V

    invoke-static {}, Lcom/sec/spp/push/c/b;->c()V

    invoke-static {v2}, Lcom/sec/spp/push/c/b;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->n()V

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/AlarmTimer;->c()V

    invoke-static {}, Lcom/sec/spp/push/update/Update;->c()V

    invoke-static {}, Lcom/sec/spp/push/f/c;->a()Lcom/sec/spp/push/f/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/f/c;->b()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->k()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->k()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/a/d;->b(Z)V

    invoke-static {}, Lcom/sec/spp/push/util/c;->b()V

    invoke-static {}, Lcom/sec/spp/push/monitor/a;->a()Lcom/sec/spp/push/monitor/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/monitor/a;->d()V

    const-string v0, "[PushClientService]"

    const-string v1, "<<--- PushClientService.deInitPushClientService -------END  --->>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "[PushClientService]"

    const-string v1, "ProvConnection already Disconected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "[PushClientService]"

    const-string v1, "PushConnection already Disconected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private c()V
    .locals 3

    const-string v0, "[PushClientService]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PushClient version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", PushClient server version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", PushClient platform version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientService;->d()V

    const-string v0, "[PushClientService]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Preferences was not set up yet.(VERSION:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Preferences.getVersion():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/k;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", so call setDefaultValue()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->F()V

    :cond_0
    const-string v0, "[PushClientService]"

    const-string v1, "onCreate.========default values============== "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->f(Z)V

    const-string v0, "[PushClientService]"

    const-string v1, "onCreate.========preference values=========== "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->f(Z)V

    return-void
.end method

.method private d()V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/control/a;->c()V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/spp/push/PushClientService;->startForeground(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "PushServiceStopAction"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/PushClientService;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/spp/push/PushClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method b()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/PushClientService;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[PushClientService]"

    const-string v2, " unregisterReceiver exception "

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/spp/push/util/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "[PushClientService]"

    const-string v1, "PushClientService.onBind()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/PushClientService;->a:Lcom/sec/spp/push/f;

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientService;->e()V

    :cond_0
    invoke-direct {p0}, Lcom/sec/spp/push/PushClientService;->c()V

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/util/c;->h()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/util/c;->a(J)V

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->a()V

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->a()V

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/AlarmTimer;->b()V

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientService;->a()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->f()V

    invoke-static {}, Lcom/sec/spp/push/monitor/a;->a()Lcom/sec/spp/push/monitor/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/monitor/a;->b()V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/log/collector/d;->q:Lcom/sec/spp/push/log/collector/d;

    invoke-static {p0, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;)V

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 9

    const/4 v1, 0x0

    const-string v0, "[PushClientService]"

    const-string v2, "PushClientService.onDestroy()"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/c/b;->b()Z

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/d/a/b;->h()Z

    move-result v2

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v3

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v4

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/spp/push/util/AlarmTimer;->d()Z

    move-result v5

    const-string v6, "[PushClientService]"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "PushClientService.onDestroy. isForceStopService:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isElementsInResponseMap:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isTasksInPendingQueue:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isSchedule:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "isRegistrationTableEmpty:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_3

    if-nez v2, :cond_0

    if-nez v3, :cond_0

    if-nez v5, :cond_0

    if-nez v4, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/spp/push/PushClientService;->a(Z)V

    const-string v2, "[PushClientService]"

    const-string v3, "HeartBeat.StopHeartBeat()"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->m()V

    const-string v2, "[PushClientService]"

    const-string v3, "<<--- PushClientService.onDestroy -------END  --->>"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->g()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v0, "[PushClientService]"

    const-string v2, "PushClientService.onDestroy. Network is not available."

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_1

    const-string v0, "[PushClientService]"

    const-string v1, "<<--- PushClientService.onDestroy -------Restart  --->>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_2
    return-void

    :cond_1
    const-string v0, "[PushClientService]"

    const-string v1, "<<--- PushClientService.onDestroy -------Do not Restart  --->>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    if-eqz p1, :cond_0

    const-string v0, "[PushClientService]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PushClientService.onUnbind()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "[PushClientService]"

    const-string v1, "PushClientService.onRebind()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 11

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    const-string v0, "[PushClientService]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[onStartCommand] PushClientService is started. intent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    const-string v0, "[PushClientService]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    :try_start_0
    const-string v0, "reqType"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/spp/push/PushClientService;->a(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/PushClientApplication;

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/PushClientApplication;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/c/b;->b()Z

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->x()Z

    move-result v3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/spp/push/d/a/b;->h()Z

    move-result v4

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v5

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/spp/push/util/AlarmTimer;->d()Z

    move-result v6

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v7

    const-string v8, "[PushClientService]"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "onStartCommand. F:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ", D:"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", E:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", T:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", S:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", R:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v4, :cond_1

    if-nez v5, :cond_1

    if-nez v6, :cond_1

    if-nez v7, :cond_4

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->h()V

    const-string v0, "[PushClientService]"

    const-string v1, "return Service.START_STICKY."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_1
    return v0

    :catch_0
    move-exception v0

    const-string v2, "[PushClientService]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onStartCommand. failed getIntExtra(). Remote Exception e "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1

    :cond_2
    const-string v0, "com.sec.spp.push.NET_CHECK_ALARM"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[PushClientService]"

    const-string v3, "AlarmTimer activate SPP service"

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    const-string v0, "[PushClientService]"

    const-string v3, "[onStartCommand] intent is null - do nothing."

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-string v0, "[PushClientService]"

    const-string v2, "Service.START_NOT_STICKY."

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    if-eqz p1, :cond_0

    const-string v0, "[PushClientService]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PushClientService.onUnbind()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "[PushClientService]"

    const-string v1, "PushClientService.onUnbind()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method
