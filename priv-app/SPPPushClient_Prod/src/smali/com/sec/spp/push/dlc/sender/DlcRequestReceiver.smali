.class public Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "user_list"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;[J)V

    return-void
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/dlc/util/f;->E(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_3

    :cond_2
    const-string v0, "Users is empty"

    sget-object v1, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-string v2, "user"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_4

    invoke-virtual {v0, v3, v4}, Landroid/os/UserManager;->getUserForSerialNumber(J)Landroid/os/UserHandle;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v1, "Send BR to User : User is null"

    sget-object v3, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Send BR to User : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Action : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", User : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/util/g;->k()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.sec.dlc.DLC_REQUEST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v0, "operation"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Operation : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "Operation is empty"

    sget-object v1, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/dlc/util/d;->a()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    const-string v2, "Send this intent to Subs"

    sget-object v3, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_2
    const-string v2, "com.sec.dlc.OPERATION_URGENT_START"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v0, "[URGENT_START]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "urgent_pkg"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "[Urgent Start] pkg name is null"

    sget-object v1, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->c:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v2, v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "com.sec.dlc.OPERATION_URGENT_STOP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "[URGENT_STOP]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->d:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v0, v5}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v2, "com.sec.dlc.OPERATION_LOGGING_START"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v0, "[LOGGING_START]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->e:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V

    goto :goto_0

    :cond_6
    const-string v2, "com.sec.dlc.OPERATION_LOGGING_STOP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, "[LOGGING_STOP]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V

    goto :goto_0

    :cond_7
    const-string v2, "com.sec.dlc.OPERATION_PROVISION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v0, "[PROVISION]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->m:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v0, v5}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "com.sec.dlc.OPERATION_PING"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v0, "[PING]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->a:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v1, v0, v5}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "com.sec.dlc.OPERATION_REBOOT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[OPERATION_REBOOT]"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->s(Landroid/content/Context;)V

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->v(Landroid/content/Context;)V

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->w(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/q;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DLC timer reset error : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->b:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/dlc/util/d;->a()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-nez v0, :cond_b

    const-string v0, "Send selfPing event to Subs"

    sget-object v4, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_b
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->ay:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SelfPing] b =  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", User : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/DlcRequestReceiver;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/spp/push/dlc/sender/SenderService;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->a:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->ay:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
