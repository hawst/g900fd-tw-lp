.class public Lcom/sec/spp/push/util/l;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/spp/push/util/l;->c:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/spp/push/util/l;->g:I

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->h:Ljava/lang/String;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/util/l;->f:Ljava/lang/Long;

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->i:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->k:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/spp/push/util/l;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/Long;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/util/l;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/spp/push/util/l;->b:Ljava/lang/String;

    iput p4, p0, Lcom/sec/spp/push/util/l;->d:I

    iput p3, p0, Lcom/sec/spp/push/util/l;->c:I

    iput-object p5, p0, Lcom/sec/spp/push/util/l;->e:Ljava/lang/String;

    iput p7, p0, Lcom/sec/spp/push/util/l;->g:I

    iput-object p8, p0, Lcom/sec/spp/push/util/l;->h:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/spp/push/util/l;->f:Ljava/lang/Long;

    iput-object p9, p0, Lcom/sec/spp/push/util/l;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/sec/spp/push/util/l;->j:Ljava/lang/String;

    iput-object p11, p0, Lcom/sec/spp/push/util/l;->k:Ljava/lang/String;

    iput-object p12, p0, Lcom/sec/spp/push/util/l;->l:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/l;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/l;->d:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->f:Ljava/lang/Long;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/l;->g:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/l;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/util/l;->l:Ljava/lang/String;

    goto :goto_0
.end method
