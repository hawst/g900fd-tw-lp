.class final enum Lcom/sec/spp/push/notisvc/registration/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/registration/d;

.field public static final enum b:Lcom/sec/spp/push/notisvc/registration/d;

.field private static final synthetic d:[Lcom/sec/spp/push/notisvc/registration/d;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/d;

    const-string v1, "ACCOUNT"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/spp/push/notisvc/registration/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/d;->a:Lcom/sec/spp/push/notisvc/registration/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/d;

    const-string v1, "SETUP_WIZARD"

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/spp/push/notisvc/registration/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/d;->b:Lcom/sec/spp/push/notisvc/registration/d;

    new-array v0, v4, [Lcom/sec/spp/push/notisvc/registration/d;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/d;->a:Lcom/sec/spp/push/notisvc/registration/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/d;->b:Lcom/sec/spp/push/notisvc/registration/d;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/d;->d:[Lcom/sec/spp/push/notisvc/registration/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/registration/d;->c:I

    return-void
.end method

.method private a()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/notisvc/registration/d;->c:I

    return v0
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/registration/d;)I
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/d;->a()I

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/d;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/registration/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/registration/d;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/d;->d:[Lcom/sec/spp/push/notisvc/registration/d;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/registration/d;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
