.class public final enum Lcom/sec/spp/push/notisvc/registration/m;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/registration/m;

.field public static final enum b:Lcom/sec/spp/push/notisvc/registration/m;

.field public static final enum c:Lcom/sec/spp/push/notisvc/registration/m;

.field public static final enum d:Lcom/sec/spp/push/notisvc/registration/m;

.field public static final enum e:Lcom/sec/spp/push/notisvc/registration/m;

.field private static final synthetic g:[Lcom/sec/spp/push/notisvc/registration/m;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/m;

    const-string v1, "DEREGISTERED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/spp/push/notisvc/registration/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->a:Lcom/sec/spp/push/notisvc/registration/m;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/m;

    const-string v1, "AGREE"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/spp/push/notisvc/registration/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->b:Lcom/sec/spp/push/notisvc/registration/m;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/m;

    const-string v1, "DISAGREE"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/spp/push/notisvc/registration/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->c:Lcom/sec/spp/push/notisvc/registration/m;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/m;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v3}, Lcom/sec/spp/push/notisvc/registration/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->d:Lcom/sec/spp/push/notisvc/registration/m;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/m;

    const-string v1, "ERROR"

    const/4 v2, -0x3

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/spp/push/notisvc/registration/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->e:Lcom/sec/spp/push/notisvc/registration/m;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/registration/m;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/m;->a:Lcom/sec/spp/push/notisvc/registration/m;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/m;->b:Lcom/sec/spp/push/notisvc/registration/m;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/m;->c:Lcom/sec/spp/push/notisvc/registration/m;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/m;->d:Lcom/sec/spp/push/notisvc/registration/m;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/m;->e:Lcom/sec/spp/push/notisvc/registration/m;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/m;->g:[Lcom/sec/spp/push/notisvc/registration/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/registration/m;->f:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/m;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/registration/m;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/registration/m;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->g:[Lcom/sec/spp/push/notisvc/registration/m;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/registration/m;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
