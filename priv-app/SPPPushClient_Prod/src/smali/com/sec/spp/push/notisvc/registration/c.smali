.class public Lcom/sec/spp/push/notisvc/registration/c;
.super Lcom/sec/spp/push/notisvc/registration/h;


# static fields
.field private static b:Z

.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/c;->c:Ljava/lang/Object;

    const-class v0, Lcom/sec/spp/push/notisvc/registration/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/h;-><init>()V

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/agree/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->e:Ljava/lang/String;

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/disagree/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->f:Ljava/lang/String;

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/deregister/"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->g:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/registration/c;->h:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/c;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const-string v1, "build. intent is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "agreement"

    sget v2, Lcom/sec/spp/push/notisvc/registration/c;->a:I

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "build. status is wrong. status : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "build. status is wrong. status : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "agreePackage"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    const-string v1, "buil. package name is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "setupWizard"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "build. package name is wrong. agree pkg : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "build. status:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", pkg:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/c;

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/notisvc/registration/c;-><init>(Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 9

    const-wide/32 v7, 0x493e0

    const/16 v6, 0xa

    const-string v0, "setAlarmAfterBoot."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_1

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->k(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v6, :cond_2

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    const-string v2, "regitimer:com.osp.app.signin"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v7

    const-string v5, "com.osp.app.signin"

    invoke-static {v5, v1}, Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;->a(Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    :cond_2
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->l(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v6, :cond_0

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    const-string v2, "regitimer:setupWizard"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v7

    const-string v5, "setupWizard"

    invoke-static {v5, v1}, Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;->a(Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 5

    const/16 v4, 0xa

    const/16 v3, -0x3e8

    if-nez p0, :cond_0

    const-string v0, "context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->k(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v3, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v4, :cond_3

    :cond_1
    const-string v0, "no more incomp account agree request."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->l(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v3, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v4, :cond_6

    :cond_2
    const-string v0, "no more incomp setup wizard request."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "request account agree : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.AGREE_REGI"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "agreement"

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "agreePackage"

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong account agree value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong account agree value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-eqz v1, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "request setup wizard request : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.AGREE_REGI"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "agreement"

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "agreePackage"

    const-string v2, "setupWizard"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong setup wizard value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong setup wizard value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setOnWorking : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p1, Lcom/sec/spp/push/notisvc/registration/c;->b:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/notisvc/registration/c;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/agree/"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/disagree/"

    goto :goto_0

    :cond_1
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/device/deregister/"

    goto :goto_0
.end method

.method c(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "checkBeforeRequest. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "checkBeforeRequest. Not yet Device Reg completed. Do Device Reg"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/c;->h(Landroid/content/Context;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0
.end method

.method d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 3

    const-string v0, "delayRequest."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "incom Status is null. status value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->h:Ljava/lang/String;

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;I)V

    :goto_1
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method e(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "saveData. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "saveData. incom Status is null. status value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->h:Ljava/lang/String;

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;I)V

    :goto_1
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method f(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->h:Ljava/lang/String;

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "agreePackage"

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/d;->a:Lcom/sec/spp/push/notisvc/registration/d;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/registration/d;->a(Lcom/sec/spp/push/notisvc/registration/d;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v1, "agreePackage"

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/d;->b:Lcom/sec/spp/push/notisvc/registration/d;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/registration/d;->a(Lcom/sec/spp/push/notisvc/registration/d;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    const-string v0, "getHttpBody. getAgreeBody JSONEXception"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method g(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/agent/h;
    .locals 6

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string v0, "getCallbackMethod. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/c;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v5

    :cond_0
    new-instance v0, Lcom/sec/spp/push/notisvc/registration/e;

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/registration/c;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/c;->i:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/registration/e;-><init>(Lcom/sec/spp/push/notisvc/registration/c;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;Lcom/sec/spp/push/notisvc/registration/e;)V

    move-object v5, v0

    goto :goto_0
.end method
