.class public Lcom/sec/spp/push/dlc/util/q;
.super Lcom/sec/spp/push/dlc/util/a;


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 3

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->y(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->x(Landroid/content/Context;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/sender/DlcForcedPvAlarmReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->p:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-wide/32 v1, 0x5265c00

    invoke-static {p0, v0, v1, v2}, Lcom/sec/spp/push/dlc/util/q;->a(Landroid/content/Context;Landroid/content/Intent;J)V

    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->y(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->w(Landroid/content/Context;)V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/sender/DlcForcedPvAlarmReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->p:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/sec/spp/push/dlc/util/q;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method
