.class public Lcom/sec/spp/push/receiver/ForceUpdateAlarmReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/receiver/ForceUpdateAlarmReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/receiver/ForceUpdateAlarmReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/update/a;->a()Lcom/sec/spp/push/update/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/update/a;->b()V

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.receiver.ForceUpdateAlarmReceiver"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v6, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    sget-object v0, Lcom/sec/spp/push/receiver/ForceUpdateAlarmReceiver;->a:Ljava/lang/String;

    const-string v1, "Alarm Setting. After one day, it will alram."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
