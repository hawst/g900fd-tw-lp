.class public Lcom/sec/spp/push/h/c;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/spp/push/h/a;

.field private static e:Lcom/sec/spp/push/h/c;

.field private static final f:Ljava/lang/Object;


# instance fields
.field private c:Lcom/sec/spp/push/c/b;

.field private final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/h/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/h/c;->f:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/h/c;->d:Ljava/lang/Object;

    new-instance v0, Lcom/sec/spp/push/h/a;

    invoke-direct {v0}, Lcom/sec/spp/push/h/a;-><init>()V

    sput-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/h/c;)Lcom/sec/spp/push/c/b;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/h/c;->c:Lcom/sec/spp/push/c/b;

    return-object v0
.end method

.method public static declared-synchronized a()Lcom/sec/spp/push/h/c;
    .locals 2

    const-class v1, Lcom/sec/spp/push/h/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/h/c;->e:Lcom/sec/spp/push/h/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/h/c;

    invoke-direct {v0}, Lcom/sec/spp/push/h/c;-><init>()V

    sput-object v0, Lcom/sec/spp/push/h/c;->e:Lcom/sec/spp/push/h/c;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/h/c;->e:Lcom/sec/spp/push/h/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(I)V
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/h/c;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/k;->j(I)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/sec/spp/push/h/c;Lcom/sec/spp/push/c/b;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/h/c;->c:Lcom/sec/spp/push/c/b;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/h/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    sget-object v1, Lcom/sec/spp/push/h/c;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    if-ne v0, v2, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.sec.android.fotaclient"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.sec.spp.push"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.policydm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/util/c;->a(Lcom/sec/spp/push/util/d;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->j()Z

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/a/d;->m()Z

    move-result v2

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v3

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/spp/push/e/a/g;->j()Z

    move-result v4

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/spp/push/e/a/g;->n()Z

    move-result v5

    sget-object v6, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "PV : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ", "

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", INIT : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->o()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->r()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 6

    new-instance v0, Lcom/sec/spp/push/util/p;

    invoke-direct {v0}, Lcom/sec/spp/push/util/p;-><init>()V

    const/4 v2, 0x0

    move v1, p6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/util/p;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p4, p5}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/p;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/h/c;->c()V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->C()I

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/spp/push/h/c;->j()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "Send Reg Noti to NotiSvc."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/control/a;->a()V

    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/c;->b(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "NotiSvc Registered. Skip noti sending"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b()V
    .locals 0

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/k;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/spp/push/h/c;->d()V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->C()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.dlc.SELF_PING_NOTI"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;J)V

    :cond_0
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "Send De-reg Noti to NotiSvc."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/control/a;->b()V

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/b/a;->a()Lcom/sec/spp/push/b/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/spp/push/b/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    :cond_3
    throw v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a(Landroid/os/Handler;)V
    .locals 8

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->c()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xb

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "string"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "] "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/sec/spp/push/util/e;Ljava/lang/String;Z)V
    .locals 7

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p3, :cond_2

    const-string v1, "com.sec.spp.Status"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/util/e;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/spp/push/util/e;->c()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/spp/push/util/e;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[DeregReply] appid : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", pkg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rCode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v3, "appId"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Error"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "userSN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-static {v0, p2}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v1, "com.sec.spp.Status"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected a(Lcom/sec/spp/push/util/p;Ljava/lang/String;Z)V
    .locals 8

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p3, :cond_2

    const-string v1, "com.sec.spp.Status"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/util/p;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/spp/push/util/p;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/spp/push/util/p;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/spp/push/util/p;->c()I

    move-result v4

    sget-object v5, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[RegReply] appid : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", regId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pkg : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", rCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v3, "appId"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "RegistrationID"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "Error"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "userSN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0, p2}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "com.sec.spp.Status"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doRegistration"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->g()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doRegistration. Network is not available."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v6, 0xfa7

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/g/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "[doRegistration] Device ID is empty. Do nothing"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v6, -0x6a

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doRegistration. AppId or userData shouldn\'t be empty."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    const/16 v6, 0xfa3

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/sec/spp/push/util/o;->d()I

    move-result v0

    if-le v0, v7, :cond_5

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] is already registered : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/16 v6, 0x3e8

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, v7

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/h/d;

    invoke-direct {v1, p0, p3}, Lcom/sec/spp/push/h/d;-><init>(Lcom/sec/spp/push/h/c;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 2

    new-instance v0, Lcom/sec/spp/push/util/e;

    invoke-direct {v0}, Lcom/sec/spp/push/util/e;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, p5, v1, p2, p1}, Lcom/sec/spp/push/util/e;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3, p4}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/e;Ljava/lang/String;Z)V

    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0, p1, p2}, Lcom/sec/spp/push/h/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    :goto_0
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteRegTableEntryByAppId(appId) returns "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deleteRegTableEntryByAppId()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deleteRegTableEntryByAppId()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deleteRegTableEntryByAppId()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_8
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[deleteRegTableEntryByAppId()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_2
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[saveRegInfo] appId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " regId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " userData : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " userSN : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v2, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v2}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v2, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v2, p1, p4}, Lcom/sec/spp/push/h/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lt v2, v0, :cond_3

    sget-object v2, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v2, p1, p4}, Lcom/sec/spp/push/h/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v0

    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :goto_1
    sget-object v3, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v3, p1, p2, p3, p4}, Lcom/sec/spp/push/h/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-ltz v3, :cond_2

    :goto_2
    :try_start_2
    invoke-direct {p0, p2, v2}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Z)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    :goto_3
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    :goto_4
    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[saveRegInfo()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    :goto_5
    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[saveRegInfo()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    :goto_6
    :try_start_8
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[saveRegInfo()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v7, v0

    move v0, v1

    move-object v1, v7

    :goto_7
    :try_start_a
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[saveRegInfo()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto/16 :goto_4

    :cond_2
    move v0, v1

    goto/16 :goto_2

    :cond_3
    move v2, v1

    goto/16 :goto_0

    :cond_4
    move v2, v1

    goto/16 :goto_1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/16 v5, 0xfa3

    const/4 v6, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doDeregistration"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doDeregistration(). Application id shouldn\'t be null."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v6, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doDeregistration(). Application id shouldn\'t be empty. appId.length():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doDeregistration(). userSN is empty."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/util/k;->g()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "doDeregistration. Network is not available so return false"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0xfa8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/spp/push/g/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "[doDeregistration] Device ID is empty. Do nothing"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, -0x6a

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, p1, p3}, Lcom/sec/spp/push/h/c;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "Already deregistratered in local DB. "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0x3e8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    const-string p1, "com.sec.spp.RegistrationFail"

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/spp/push/receiver/RegistrationNotiReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.sec.spp.push.ACTION_REGISTRATION_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "RegistrationID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "userdata"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "userSN"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0, p4}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[isAppAlreadyRegistered] userSN : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v2, "[isAppAlreadyRegistered()] appId is NULL"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string p2, "0"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1, p1, p2}, Lcom/sec/spp/push/h/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x1

    :goto_1
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v0, v1

    :goto_2
    :try_start_4
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v2, "getEntry : null"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v1

    :goto_3
    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[isAppAlreadyRegistered()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_4
    :try_start_8
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[isAppAlreadyRegistered()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :goto_5
    :try_start_a
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[isAppAlreadyRegistered()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :goto_6
    :try_start_c
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[isAppAlreadyRegistered()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :try_start_d
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_4
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto :goto_6

    :catch_5
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto :goto_5

    :catch_6
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto/16 :goto_4

    :catch_7
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto/16 :goto_3

    :cond_5
    move v1, v0

    goto/16 :goto_1
.end method

.method public declared-synchronized c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v2, "getRegId() argument is empty."

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1, p1, p2}, Lcom/sec/spp/push/h/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    :goto_1
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v1

    :cond_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v1

    :goto_2
    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getRegId()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_3
    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getRegId()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v1

    :goto_4
    :try_start_8
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getRegId()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :goto_5
    :try_start_a
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getRegId()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_5

    :catch_5
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_4

    :catch_6
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto/16 :goto_3

    :catch_7
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto/16 :goto_2

    :cond_5
    move-object v1, v0

    goto/16 :goto_1
.end method

.method protected c()V
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/h/c;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->C()I

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/k;->j(I)V

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reg App Count Inc : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->C()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/h/e;

    invoke-direct {v1, p0, p3, p2}, Lcom/sec/spp/push/h/e;-><init>(Lcom/sec/spp/push/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/sec/spp/push/d/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V

    return-void
.end method

.method public declared-synchronized d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1, p1, p2}, Lcom/sec/spp/push/h/a;->c(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v1

    :goto_2
    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAppId()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_3
    move-object v1, v0

    :goto_3
    const/4 v0, 0x0

    :try_start_6
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v0

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pkgName : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " userSN : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " appID :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :goto_4
    :try_start_9
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAppId()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :goto_5
    :try_start_b
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAppId()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :goto_6
    :try_start_d
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAppId()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :try_start_e
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_4
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_6

    :catch_5
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_5

    :catch_6
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto/16 :goto_4

    :catch_7
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto/16 :goto_2

    :cond_5
    move-object v1, v0

    goto/16 :goto_3
.end method

.method protected d()V
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/h/c;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->C()I

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/k;->j(I)V

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reg App Count Dec : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->C()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()V
    .locals 2

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v1, "registrationManager Destroying"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-class v1, Lcom/sec/spp/push/h/c;

    monitor-enter v1

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/h/c;->e:Lcom/sec/spp/push/h/c;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/h/c;->e:Lcom/sec/spp/push/h/c;

    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[broadcastDeleteRegInfoNoti] appID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " userSN"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/spp/push/receiver/RegistrationNotiReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.sec.spp.push.ACTION_DEREGISTRATION_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "userSN"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0, p2}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized f()Z
    .locals 6

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->c()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/spp/push/h/c;->a(I)V

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v0, v1

    :goto_0
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    :try_start_2
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isRegistrationTableEmpty. SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isRegistrationTableEmpty. IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isRegistrationTableEmpty. Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    goto :goto_1

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_2
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public declared-synchronized g()Ljava/util/ArrayList;
    .locals 7

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->c()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :try_start_2
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Reg App] Number of Reg Apps : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v1

    :cond_2
    const/4 v2, 0x0

    :try_start_3
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/spp/push/util/q;

    invoke-direct {v6, v2, v4, v3, v5}, Lcom/sec/spp/push/util/q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllRegisteredLists()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_6
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllRegisteredLists()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_8
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllRegisteredLists()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_1

    :catch_3
    move-exception v0

    :try_start_a
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllRegisteredLists()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_3
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
.end method

.method public declared-synchronized h()Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->c()Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    if-nez v2, :cond_2

    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/spp/push/h/c;->a(I)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v1, 0x4

    if-gt v3, v1, :cond_3

    if-nez v3, :cond_5

    :cond_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v1, v0

    :cond_6
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v4

    if-nez v4, :cond_8

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_7
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_8
    :try_start_6
    const-string v4, "pack_name"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "com.sec.android.fotaclient"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    add-int/lit8 v1, v1, 0x1

    :cond_9
    :goto_1
    if-ne v1, v3, :cond_6

    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    const-string v3, "FO state : true"

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_a
    if-eqz v2, :cond_b

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_b
    const/4 v0, 0x1

    goto :goto_0

    :cond_c
    :try_start_8
    const-string v5, "com.sec.spp.push"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_d
    const-string v5, "com.policydm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_e
    const-string v5, "com.sec.android.diagmonagent"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v4

    if-eqz v4, :cond_9

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_9
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getAppId()] SQLiteException with message ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_f

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_f
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :try_start_b
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getAppId()] NullPointerException with message ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_10

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_10
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :try_start_d
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getAppId()] IllegalStateException with message ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :try_start_e
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_11

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_11
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v1

    :try_start_f
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getAppId()] Exception with message ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_12

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_12
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_13

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_13
    if-eqz v2, :cond_14

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_14
    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0
.end method

.method public declared-synchronized i()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    sget-object v3, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v3}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v3, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v3}, Lcom/sec/spp/push/h/a;->c()Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    if-nez v2, :cond_2

    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    if-eq v3, v1, :cond_4

    :try_start_3
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_3
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v3

    if-nez v3, :cond_6

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_5
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_6
    :try_start_6
    const-string v3, "pack_name"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.spp.push"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v3

    if-eqz v3, :cond_c

    :try_start_7
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_7
    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_8
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_8
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onlyBigJoeRegistered() : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_9

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_9
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_a
    if-eqz v2, :cond_b

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    :cond_c
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_d

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_d
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized j()Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/util/g;->k()J

    move-result-wide v3

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    const-string v5, "1563e038015c430d"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, Lcom/sec/spp/push/h/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    if-nez v2, :cond_3

    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return v0

    :cond_3
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-lez v1, :cond_9

    :try_start_3
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[getAppId()] Exception with message ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_6
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_6
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_7
    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    :cond_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_a
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized k()Z
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/util/g;->k()J

    move-result-wide v3

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    const-string v5, "af06dccdddc4c3b0"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3}, Lcom/sec/spp/push/h/a;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    if-nez v2, :cond_3

    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return v0

    :cond_3
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-lez v1, :cond_9

    :try_start_3
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    sget-object v3, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isSamsungAccountReg"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_6
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_6
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_7
    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    :cond_9
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_a
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized l()Ljava/util/ArrayList;
    .locals 6

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->d()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    :try_start_2
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    :goto_0
    monitor-exit p0

    return-object v1

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllUserSN()] SQLiteException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_5
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllUserSN()] NullPointerException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_7
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllUserSN()] IllegalStateException with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :catch_3
    move-exception v0

    :try_start_9
    sget-object v2, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[getAllUserSN()] Exception with message ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_5
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method public declared-synchronized m()V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->a()Lcom/sec/spp/push/h/a;

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    const-string v1, "1563e038015c430d"

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/h/a;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/h/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/h/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/h/c;->b:Lcom/sec/spp/push/h/a;

    invoke-virtual {v1}, Lcom/sec/spp/push/h/a;->b()V

    :cond_1
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
