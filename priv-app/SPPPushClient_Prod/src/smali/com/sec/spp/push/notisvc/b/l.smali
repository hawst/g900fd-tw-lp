.class Lcom/sec/spp/push/notisvc/b/l;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/b/k;

.field private final synthetic b:I

.field private final synthetic c:I


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/b/k;II)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/b/l;->a:Lcom/sec/spp/push/notisvc/b/k;

    iput p2, p0, Lcom/sec/spp/push/notisvc/b/l;->b:I

    iput p3, p0, Lcom/sec/spp/push/notisvc/b/l;->c:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget v1, p0, Lcom/sec/spp/push/notisvc/b/l;->b:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/l;->a:Lcom/sec/spp/push/notisvc/b/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/b/k;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/sec/spp/push/notisvc/b/l;->b:I

    iget v2, p0, Lcom/sec/spp/push/notisvc/b/l;->c:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x7

    if-lt v0, v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "run. context is null. Cannot remove bufferView"

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-wide/16 v1, 0x5

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/b/l;->sleep(J)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/l;->a:Lcom/sec/spp/push/notisvc/b/k;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/notisvc/b/k;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ViewRiseUpHandler. Interrupted"

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
