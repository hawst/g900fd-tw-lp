.class public Lcom/sec/spp/push/dlc/util/PushActivity;
.super Landroid/app/Activity;


# instance fields
.field protected a:Landroid/os/Handler;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->b:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->b:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->setContentView(I)V

    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/g;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/g;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090025

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/i;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/i;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/j;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/j;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/k;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/k;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->b:Landroid/widget/TextView;

    new-instance v0, Lcom/sec/spp/push/dlc/util/l;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/util/l;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->a:Landroid/os/Handler;

    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/m;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/dlc/util/m;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090027

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/n;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/dlc/util/n;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090028

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/o;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/o;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090029

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/p;

    invoke-direct {v1, p0, p0}, Lcom/sec/spp/push/dlc/util/p;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/dlc/util/h;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/dlc/util/h;-><init>(Lcom/sec/spp/push/dlc/util/PushActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "Server : "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "GLD [https://ew1.dms-gld.bigdata.ssp.samsung.com:80/1.0/provision]\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "VERSION_NAME : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/util/b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090023

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/util/PushActivity;->a:Landroid/os/Handler;

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
