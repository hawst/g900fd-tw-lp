.class Lcom/sec/spp/push/notisvc/b/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/b/e;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/b/e;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const-string v0, "onNotificationClick."

    const-string v1, "NotiOverlayView"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/b/e;->a(Lcom/sec/spp/push/notisvc/b/e;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNotificationClick. context or map is null. ctx:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/b/e;->a(Lcom/sec/spp/push/notisvc/b/e;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", map: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NotiOverlayView"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/b/e;->c(Lcom/sec/spp/push/notisvc/b/e;)Lcom/sec/spp/push/notisvc/b/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/notisvc/b/c;->a(Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/b/e;->a(Lcom/sec/spp/push/notisvc/b/e;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "notiTag"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/b/e;->c(Lcom/sec/spp/push/notisvc/b/e;)Lcom/sec/spp/push/notisvc/b/c;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/b/e;->a(Lcom/sec/spp/push/notisvc/b/e;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/notisvc/b/c;->a(Landroid/content/Context;I)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "onNotificationClick. wrong displayId["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/b/f;->a:Lcom/sec/spp/push/notisvc/b/e;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/b/e;->b(Lcom/sec/spp/push/notisvc/b/e;)Ljava/util/Map;

    move-result-object v0

    const-string v2, "notiTag"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Cannot remove overlayview from viewlist."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NotiOverlayView"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "onNotificationClick. IllegalArgumentException. cannot send click event br"

    const-string v1, "NotiOverlayView"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v0, "onNotificationClick. IllegalArgumentException."

    const-string v1, "NotiOverlayView"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
