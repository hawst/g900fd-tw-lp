.class Lcom/sec/spp/push/notisvc/p;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "device_token: \""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NotiSvcActivity"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "<<Regi State>>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v6

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deviceToken : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    if-nez v6, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "ERROR : Cannot print regi state. dbhandler is null."

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "regId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SPPRegi : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DeviceRegi : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SetupAgree : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->l(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountAgree : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->k(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    const-class v0, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto/16 :goto_0

    :cond_1
    const-string v4, ""

    const-class v0, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "num of packages:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/p;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0, v4}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto/16 :goto_0

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/sec/spp/push/notisvc/a/b;->a(I)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_2

    move v0, v2

    move-object v5, v4

    move v2, v3

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v2, v4, :cond_4

    move v2, v0

    move-object v4, v5

    goto :goto_1

    :cond_4
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "[PACKAGE "

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "]\n"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "pkgName : "

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "\n"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "svc status : "

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "\n"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v9, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "version : "

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v6, v0}, Lcom/sec/spp/push/notisvc/a/b;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto/16 :goto_2
.end method
