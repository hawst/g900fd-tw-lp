.class public Lcom/sec/spp/push/notisvc/registration/l;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lorg/json/JSONArray;)Ljava/lang/String;
    .locals 8

    const/4 v2, 0x0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v1, "context or JsonArray is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v1, "DBHandler is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->c()Ljava/util/ArrayList;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v1, "The DB result Value in App Agree Table is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_3
    move v3, v2

    :goto_1
    :try_start_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lt v3, v1, :cond_4

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_4
    move v1, v2

    :goto_2
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_5

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    invoke-virtual {p1, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const-string v1, "context or package name is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-static {p0, p1}, Lcom/sec/spp/push/notisvc/registration/l;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/m;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/m;->b:Lcom/sec/spp/push/notisvc/registration/m;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/notisvc/registration/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/m;->c:Lcom/sec/spp/push/notisvc/registration/m;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/notisvc/registration/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/m;
    .locals 2

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v0, "context or package name is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/l;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->e:Lcom/sec/spp/push/notisvc/registration/m;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->e:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/a/b;->s(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    if-nez v1, :cond_3

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->d:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->b:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->c:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->a:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0

    :cond_9
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/m;->e:Lcom/sec/spp/push/notisvc/registration/m;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
