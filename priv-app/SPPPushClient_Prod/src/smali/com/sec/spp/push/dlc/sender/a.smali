.class public Lcom/sec/spp/push/dlc/sender/a;
.super Lcom/sec/spp/push/dlc/sender/aj;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/sender/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/aj;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/a;->b:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/a;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
