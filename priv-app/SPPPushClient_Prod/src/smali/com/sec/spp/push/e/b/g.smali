.class public Lcom/sec/spp/push/e/b/g;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;

.field private static f:Lcom/sec/spp/push/e/b/g;

.field private static synthetic h:[I


# instance fields
.field public a:Lcom/sec/spp/push/e/b;

.field private c:Landroid/os/HandlerThread;

.field private d:Landroid/os/Handler;

.field private e:Lcom/sec/spp/push/e/b/i;

.field private g:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/e/b/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/sec/spp/push/e/b;

    invoke-direct {v0}, Lcom/sec/spp/push/e/b;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "RequestDispatcherThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    sget-object v0, Lcom/sec/spp/push/e/b/i;->a:Lcom/sec/spp/push/e/b/i;

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    new-instance v0, Lcom/sec/spp/push/e/b/h;

    iget-object v1, p0, Lcom/sec/spp/push/e/b/g;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/spp/push/e/b/h;-><init>(Lcom/sec/spp/push/e/b/g;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->d:Landroid/os/Handler;

    return-void
.end method

.method private a(I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    return-void
.end method

.method private a(Landroid/os/Message;I)V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processError. message.arg1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",  internalErrorCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2, v2}, Lcom/sec/spp/push/d/a/b;->a(IILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/e/b/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/e/b/g;->l()V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/e/b/g;->a(Landroid/os/Message;I)V

    return-void
.end method

.method private b(Landroid/os/Message;I)V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processInitFailException. message.arg1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",  internalErrorCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, p2, v2}, Lcom/sec/spp/push/d/a/b;->a(IILjava/lang/Object;)V

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "Cancel pending tasks"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    const/16 v1, -0x68

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/e/b/g;->b(Landroid/os/Message;I)V

    return-void
.end method

.method public static declared-synchronized e()Lcom/sec/spp/push/e/b/g;
    .locals 2

    const-class v1, Lcom/sec/spp/push/e/b/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/b/g;->f:Lcom/sec/spp/push/e/b/g;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/e/b/g;

    invoke-direct {v0}, Lcom/sec/spp/push/e/b/g;-><init>()V

    sput-object v0, Lcom/sec/spp/push/e/b/g;->f:Lcom/sec/spp/push/e/b/g;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/e/b/g;->f:Lcom/sec/spp/push/e/b/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic i()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j()[I
    .locals 3

    sget-object v0, Lcom/sec/spp/push/e/b/g;->h:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/b/i;->values()[Lcom/sec/spp/push/e/b/i;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/e/b/i;->a:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/e/b/i;->b:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/e/b/i;->c:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/e/b/i;->d:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/spp/push/e/b/g;->h:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private declared-synchronized k()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "[Request WakeLock] acquire."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "[Request WakeLock] already acquired. Ignore."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Request WakeLock] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized l()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "[Request WakeLock] release."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "[Request WakeLock] already released. Ignore."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Request WakeLock] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/b/g;->g()V

    return-void
.end method

.method public a(Lcom/google/protobuf/MessageLite;II)V
    .locals 4

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->d:Landroid/os/Handler;

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "dispatchMessageRequest  requestHandler = NULL!"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->i()V

    invoke-direct {p0, p3}, Lcom/sec/spp/push/e/b/g;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/sec/spp/push/e/b/g;->k()V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/spp/push/e/b/g;->d:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, p3, p2, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "dispatchMessageRequest. Enqueueing fail "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dispatchMessageRequest. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3}, Lcom/sec/spp/push/e/b/g;->a(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/i;->a()Lcom/sec/spp/push/e/b/i;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "nextTargetServer :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "getTargetServerIp. context is null. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Error;

    const-string v1, "Sholudn\'t reach this code"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->j()[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/Error;

    const-string v1, "Sholudn\'t reach this code"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public d()I
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "getTargetServerPort. context is null. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Error;

    const-string v1, "Sholudn\'t reach this code"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->j()[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/e/b/g;->e:Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/Error;

    const-string v1, "Sholudn\'t reach this code"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->d(Landroid/content/Context;)I

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->f(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public f()V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "destroy()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    const-class v1, Lcom/sec/spp/push/e/b/g;

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x0

    :try_start_3
    sput-object v0, Lcom/sec/spp/push/e/b/g;->f:Lcom/sec/spp/push/e/b/g;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->c:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->d:Landroid/os/Handler;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :goto_1
    return-void

    :cond_1
    :try_start_5
    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v2, " destroy exception "

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/spp/push/util/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
.end method

.method public declared-synchronized g()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/b/g;->b:Ljava/lang/String;

    const-string v1, "prepareRequestWakeLock. context is null. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-class v2, Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/e/b/g;->g:Landroid/os/PowerManager$WakeLock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Lcom/sec/spp/push/e/b;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    return-object v0
.end method
