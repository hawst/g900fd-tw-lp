.class public Lcom/sec/spp/push/dlc/db/AppQuota;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private dataQuota:I

.field private pkg:Ljava/lang/String;

.field private wifiQuota:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/spp/push/dlc/db/a;

    invoke-direct {v0}, Lcom/sec/spp/push/dlc/db/a;-><init>()V

    sput-object v0, Lcom/sec/spp/push/dlc/db/AppQuota;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/dlc/db/AppQuota;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/spp/push/dlc/db/AppQuota;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/db/AppQuota;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->pkg:Ljava/lang/String;

    iput p2, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    iput p3, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    return-void
.end method

.method public static a(Ljava/lang/String;II)Lcom/sec/spp/push/dlc/db/AppQuota;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/dlc/db/AppQuota;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/spp/push/dlc/db/AppQuota;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->pkg:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->pkg:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    return-void
.end method

.method public b()I
    .locals 2

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    const/16 v1, -0x3e8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    mul-int/lit16 v0, v0, 0x400

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    goto :goto_0
.end method

.method public c()I
    .locals 2

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    const/16 v1, -0x3e8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    mul-int/lit16 v0, v0, 0x400

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->pkg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->wifiQuota:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/sec/spp/push/dlc/db/AppQuota;->dataQuota:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
