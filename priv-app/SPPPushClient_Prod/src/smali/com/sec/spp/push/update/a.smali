.class public Lcom/sec/spp/push/update/a;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;

.field private static c:Lcom/sec/spp/push/update/a;

.field private static final d:Ljava/lang/Object;


# instance fields
.field a:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private n:Landroid/content/BroadcastReceiver;

.field private o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/update/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/update/a;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const v1, 0x7f060009

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->e:Ljava/lang/String;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->f:Ljava/lang/String;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->g:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/spp/push/update/a;->h:I

    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/spp/push/update/a;->i:I

    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/spp/push/update/a;->j:I

    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/spp/push/update/a;->k:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->m:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/update/a;->n:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->G()I

    move-result v0

    iput v0, p0, Lcom/sec/spp/push/update/a;->o:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->H()I

    move-result v0

    iput v0, p0, Lcom/sec/spp/push/update/a;->p:I

    return-void
.end method

.method public static a()Lcom/sec/spp/push/update/a;
    .locals 2

    sget-object v0, Lcom/sec/spp/push/update/a;->c:Lcom/sec/spp/push/update/a;

    if-nez v0, :cond_1

    sget-object v1, Lcom/sec/spp/push/update/a;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/update/a;->c:Lcom/sec/spp/push/update/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/update/a;

    invoke-direct {v0}, Lcom/sec/spp/push/update/a;-><init>()V

    sput-object v0, Lcom/sec/spp/push/update/a;->c:Lcom/sec/spp/push/update/a;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/sec/spp/push/update/a;->c:Lcom/sec/spp/push/update/a;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/sec/spp/push/update/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/update/a;->h()V

    return-void
.end method

.method private a(I)Z
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/util/g;->l()I

    move-result v0

    if-ge v0, p1, :cond_0

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fv is higer version. fv : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Now Version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/g;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "now version is higer. fv : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Now Version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/g;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(II)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->G()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/update/a;->a()Lcom/sec/spp/push/update/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/spp/push/update/a;->a(II)V

    invoke-direct {v0}, Lcom/sec/spp/push/update/a;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {v0, p1}, Lcom/sec/spp/push/update/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v2, "Force Update start."

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0}, Lcom/sec/spp/push/update/a;->e()V

    :cond_0
    return-void
.end method

.method public static d()Landroid/content/Intent;
    .locals 7

    const/4 v0, 0x0

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.android.vending"

    const-string v1, "http://www.chaton.com/pushclientapk"

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v5

    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v6, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "There is Google play store."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "market://details?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private e()V
    .locals 7

    const/4 v6, 0x0

    iget v0, p0, Lcom/sec/spp/push/update/a;->o:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/spp/push/update/a;->p:I

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FV and ED is invalid. fv : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/spp/push/update/a;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Do Force Update. fv : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/spp/push/update/a;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/update/a;->g()V

    invoke-direct {p0}, Lcom/sec/spp/push/update/a;->f()V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.receiver.ForceUpdateAlarmReceiver"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v6, v2, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "Alarm Setting. After one day, it will alram."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/spp/push/update/b;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/update/b;-><init>(Lcom/sec/spp/push/update/a;)V

    iput-object v1, p0, Lcom/sec/spp/push/update/a;->n:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/update/a;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private g()V
    .locals 7

    const-wide/32 v5, 0x5265c00

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->g(Z)V

    iget v1, p0, Lcom/sec/spp/push/update/a;->o:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->k(I)V

    iget v1, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->l(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/sec/spp/push/update/a;->p:I

    int-to-long v3, v3

    mul-long/2addr v3, v5

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/k;->c(J)V

    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Save Preferences. fv : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/spp/push/update/a;->o:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expired date : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->I()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "system : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ed : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "oneday : 86400000"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sum : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    int-to-long v2, v2

    mul-long/2addr v2, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private h()V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "Display Popup Message."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/update/a;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/update/PopupMessage;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private i()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    const/16 v3, 0x1e

    if-le v2, v3, :cond_0

    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ED value is too big. ED : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/sec/spp/push/update/a;->p:I

    if-ge v2, v1, :cond_1

    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ED value is minus value. ED : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/spp/push/update/a;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 0

    iput p1, p0, Lcom/sec/spp/push/update/a;->o:I

    iput p2, p0, Lcom/sec/spp/push/update/a;->p:I

    return-void
.end method

.method public a(Lcom/sec/spp/push/util/n;)V
    .locals 5

    invoke-virtual {p1}, Lcom/sec/spp/push/util/n;->p()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/spp/push/util/n;->q()I

    move-result v1

    sget-object v2, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Check Prov. fv : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ed : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/sec/spp/push/update/a;->b(II)V

    return-void
.end method

.method public b()V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->G()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/spp/push/update/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v5}, Lcom/sec/spp/push/k;->g(Z)V

    invoke-virtual {v0, v6, v7}, Lcom/sec/spp/push/k;->c(J)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "Already Updated."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Display Notification. Expired Time(millis) : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->I()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Display Notification. Display Time : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->I()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    invoke-virtual {v0, v5}, Lcom/sec/spp/push/k;->g(Z)V

    invoke-virtual {v0, v6, v7}, Lcom/sec/spp/push/k;->c(J)V

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "Force Update Expired."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/spp/push/update/a;->c()V

    goto :goto_0
.end method

.method public c()V
    .locals 9

    const v8, 0x7f02000d

    const/16 v7, 0x10

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/spp/push/update/a;->b:Ljava/lang/String;

    const-string v1, "Display Force Update Notification"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/update/a;->d()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-static {v2, v5, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v0, "notification"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v1, v4, :cond_0

    new-instance v1, Landroid/app/Notification;

    iget-object v4, p0, Lcom/sec/spp/push/update/a;->e:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v1, v8, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/spp/push/update/a;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/spp/push/update/a;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v4, v5, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    :goto_0
    iput v7, v1, Landroid/app/Notification;->flags:I

    const v2, 0x1e240

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void

    :cond_0
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/update/a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/update/a;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/update/a;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v7, :cond_1

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    :goto_1
    iput-object v3, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    goto :goto_0

    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ne v2, v4, :cond_2

    const v2, 0x7f020036

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    :cond_2
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    goto :goto_1
.end method
