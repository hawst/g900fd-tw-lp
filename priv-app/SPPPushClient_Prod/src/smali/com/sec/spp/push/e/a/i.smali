.class Lcom/sec/spp/push/e/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/util/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/e/a/g;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/e/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/e/a/i;->a:Lcom/sec/spp/push/e/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 5

    const/4 v4, -0x1

    const/16 v3, -0x68

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Init.] onFail() errorCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/a/i;->a:Lcom/sec/spp/push/e/a/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    if-ne p1, v4, :cond_1

    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/c/b;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eq p1, v3, :cond_2

    const/4 v0, -0x2

    if-ne p1, v0, :cond_3

    :cond_2
    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/c/b;->a(I)V

    goto :goto_0

    :cond_3
    const/16 v0, -0x66

    if-ne p1, v0, :cond_4

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[Init.] Fail : INITIALIZATION_ALREADY_COMPLETED. "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[Init.] Fail : Execute next task in pending queue"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->f()V

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Init.] Fail : Not Handled with errorCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/e/a/i;->a:Lcom/sec/spp/push/e/a/g;

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/e/a/g;->a(Lcom/sec/spp/push/util/j;)V

    return-void
.end method
