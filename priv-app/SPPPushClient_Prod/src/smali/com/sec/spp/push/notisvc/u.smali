.class Lcom/sec/spp/push/notisvc/u;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const-string v0, "delete resource"

    const-string v1, "NotiSvcActivity"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "Context is null. cannot delete resources."

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "appDataPath is null. cannot delete resources."

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v3, "All resources are deleted"

    invoke-static {v2, v3}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/u;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/resource"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
