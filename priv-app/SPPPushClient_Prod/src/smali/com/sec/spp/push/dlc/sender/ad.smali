.class Lcom/sec/spp/push/dlc/sender/ad;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field a:Ljava/util/ArrayList;

.field b:I

.field c:Z

.field final synthetic d:Lcom/sec/spp/push/dlc/sender/u;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "SendAllSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    iput v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->b:I

    iput-boolean v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->c:Z

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 3

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/spp/push/dlc/util/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TotalSentSize : ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/db/Log;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v0

    iget v2, p0, Lcom/sec/spp/push/dlc/sender/ad;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->b:I

    goto :goto_0
.end method

.method private b()Lcom/sec/spp/push/dlc/sender/c;
    .locals 2

    iget-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "New Config available. TransTo [PV Forced St]"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->b(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/x;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->a(Landroid/os/Message;)V

    iput v5, p0, Lcom/sec/spp/push/dlc/sender/ad;->b:I

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->i(Landroid/content/Context;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->M:Lcom/sec/spp/push/dlc/sender/i;

    new-instance v3, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v1, v2, v0, v5, v3}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;IILandroid/os/Messenger;)V

    return-void
.end method

.method public b(Landroid/os/Message;)V
    .locals 6

    const/4 v5, 0x1

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->X:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "No data to send"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/ad;->b()Lcom/sec/spp/push/dlc/sender/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/sec/spp/push/dlc/sender/k;

    invoke-direct {v1}, Lcom/sec/spp/push/dlc/sender/k;-><init>()V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->h(Lcom/sec/spp/push/dlc/sender/SenderService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/dlc/sender/k;->b(Ljava/lang/String;)V

    const-string v2, "999"

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/dlc/sender/k;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/dlc/sender/k;->c(Ljava/lang/String;)V

    const-class v2, Lcom/sec/spp/push/dlc/db/Log;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const-string v0, "No data to send"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/ad;->b()Lcom/sec/spp/push/dlc/sender/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/dlc/sender/k;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/ad;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NumOfLogSent ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/k;->a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I

    goto/16 :goto_0

    :cond_4
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->H:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ad;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;Ljava/util/ArrayList;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/j;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/j;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, "logReply is null"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/j;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ad;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Lcom/sec/spp/push/dlc/sender/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v5, p0, Lcom/sec/spp/push/dlc/sender/ad;->c:Z

    goto/16 :goto_0

    :cond_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    if-eq v0, v1, :cond_7

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/ad;->b()Lcom/sec/spp/push/dlc/sender/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_8
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->al:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/dlc/util/e;->a()I

    move-result v0

    if-eq v0, v5, :cond_9

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/ad;->b()Lcom/sec/spp/push/dlc/sender/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ad;->d:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->d(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0
.end method

.method public c(Landroid/os/Message;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->c(Landroid/os/Message;)V

    return-void
.end method
