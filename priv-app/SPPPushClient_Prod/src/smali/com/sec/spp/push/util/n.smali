.class public Lcom/sec/spp/push/util/n;
.super Lcom/sec/spp/push/util/j;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/util/n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/spp/push/util/j;-><init>()V

    iput-object v1, p0, Lcom/sec/spp/push/util/n;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/spp/push/util/n;->c:Ljava/lang/String;

    iput v0, p0, Lcom/sec/spp/push/util/n;->d:I

    iput-object v1, p0, Lcom/sec/spp/push/util/n;->e:Ljava/lang/String;

    iput v0, p0, Lcom/sec/spp/push/util/n;->f:I

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/util/n;->b(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/util/n;->c(Ljava/lang/String;)V

    iput v0, p0, Lcom/sec/spp/push/util/n;->i:I

    iput-object v1, p0, Lcom/sec/spp/push/util/n;->j:Ljava/lang/String;

    iput v0, p0, Lcom/sec/spp/push/util/n;->k:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->l:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->m:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->n:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->o:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->q:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->r:I

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/spp/push/util/n;->j:Ljava/lang/String;

    const/16 v1, 0x50

    iput v1, p0, Lcom/sec/spp/push/util/n;->k:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->l:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->m:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->n:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->o:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->p:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->q:I

    iput v0, p0, Lcom/sec/spp/push/util/n;->r:I

    const-string v1, "&"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    :goto_0
    if-lt v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    aget-object v3, v1, v0

    const-string v4, "latest_version="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "latest_version="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/spp/push/util/n;->j:Ljava/lang/String;

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const-string v4, "wifi_port="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "wifi_port="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->k:I

    goto :goto_1

    :cond_4
    const-string v4, "ping_max="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "ping_max="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->l:I

    goto :goto_1

    :cond_5
    const-string v4, "ping_min="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "ping_min="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->m:I

    goto :goto_1

    :cond_6
    const-string v4, "ping_inc="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "ping_inc="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->n:I

    goto :goto_1

    :cond_7
    const-string v4, "ping_avg="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "ping_avg="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->o:I

    goto/16 :goto_1

    :cond_8
    const-string v4, "ci="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "ci="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->p:I

    goto/16 :goto_1

    :cond_9
    const-string v4, "fv="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "fv="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->q:I

    goto/16 :goto_1

    :cond_a
    const-string v4, "ed="

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "ed="

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/spp/push/util/n;->r:I

    goto/16 :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/spp/push/util/n;->b(ILjava/lang/String;)V

    iput-object p2, p0, Lcom/sec/spp/push/util/n;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/util/n;->c:Ljava/lang/String;

    iput p4, p0, Lcom/sec/spp/push/util/n;->d:I

    iput-object p5, p0, Lcom/sec/spp/push/util/n;->e:Ljava/lang/String;

    iput p6, p0, Lcom/sec/spp/push/util/n;->f:I

    iput p7, p0, Lcom/sec/spp/push/util/n;->i:I

    invoke-virtual {p0, p9}, Lcom/sec/spp/push/util/n;->b(Ljava/lang/String;)V

    invoke-virtual {p0, p10}, Lcom/sec/spp/push/util/n;->c(Ljava/lang/String;)V

    invoke-direct {p0, p8}, Lcom/sec/spp/push/util/n;->d(Ljava/lang/String;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/util/n;->g:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/util/n;->h:Ljava/lang/String;

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->d:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->e:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->f:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->i:I

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->k:I

    return v0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->l:I

    return v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->m:I

    return v0
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->n:I

    return v0
.end method

.method public n()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->o:I

    return v0
.end method

.method public o()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->p:I

    return v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->q:I

    return v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/util/n;->r:I

    return v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->g:Ljava/lang/String;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/n;->h:Ljava/lang/String;

    return-object v0
.end method
