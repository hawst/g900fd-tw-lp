.class Lcom/sec/spp/push/dlc/sender/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/k;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/net/URL;

.field private final synthetic d:Lcom/sec/spp/push/dlc/sender/d;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/dlc/sender/k;Ljava/lang/String;Ljava/net/URL;Lcom/sec/spp/push/dlc/sender/d;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/l;->a:Lcom/sec/spp/push/dlc/sender/k;

    iput-object p2, p0, Lcom/sec/spp/push/dlc/sender/l;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/dlc/sender/l;->c:Ljava/net/URL;

    iput-object p4, p0, Lcom/sec/spp/push/dlc/sender/l;->d:Lcom/sec/spp/push/dlc/sender/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const v4, 0xea60

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "DI"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/l;->a:Lcom/sec/spp/push/dlc/sender/k;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/k;->a(Lcom/sec/spp/push/dlc/sender/k;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/sec/spp/push/dlc/util/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/spp/push/dlc/sender/b;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/l;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/sec/spp/push/dlc/sender/b;-><init>(Ljava/lang/String;)V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/l;->b:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v0, v0

    :goto_1
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "body-length"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/sec/spp/push/dlc/sender/aj;

    invoke-direct {v3}, Lcom/sec/spp/push/dlc/sender/aj;-><init>()V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/l;->c:Ljava/net/URL;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->b:Lcom/sec/spp/push/dlc/sender/g;

    const-string v6, "Application/JSon"

    iget-object v8, p0, Lcom/sec/spp/push/dlc/sender/l;->d:Lcom/sec/spp/push/dlc/sender/d;

    move v5, v4

    invoke-static/range {v0 .. v8}, Lcom/sec/spp/push/dlc/sender/h;->a(Ljava/net/URL;Lcom/sec/spp/push/dlc/sender/g;Lcom/sec/spp/push/dlc/sender/ao;Lcom/sec/spp/push/dlc/sender/p;IILjava/lang/String;Ljava/util/List;Lcom/sec/spp/push/dlc/sender/d;)V

    return-void

    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "Accept-Encoding"

    const-string v2, "text"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/sec/spp/push/dlc/sender/ak;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/l;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Lcom/sec/spp/push/dlc/sender/ak;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v3, v0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/l;->a:Lcom/sec/spp/push/dlc/sender/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/k;->b()J

    move-result-wide v0

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/k;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
