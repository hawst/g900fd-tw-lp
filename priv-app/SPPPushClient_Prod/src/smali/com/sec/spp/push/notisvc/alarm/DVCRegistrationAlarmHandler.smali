.class public Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;
.super Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/c;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/c;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method public static a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;-><init>(Lcom/sec/spp/push/notisvc/registration/k;)V

    return-object v0
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    const-string v0, "DVCRegistration Alarm Received."

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "onReceive. ctx is null. do nothing"

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "onReceive. regiFlag is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "onReceive. Status is null"

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onReceive. Status is already changed. Alarm Status : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". current status : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v2

    if-ne v1, v2, :cond_4

    const/16 v1, -0x3e8

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;I)V

    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.BIGJOE_REGI"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "agreement"

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RegiStatusFlag Flag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->incompletedStatus:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
