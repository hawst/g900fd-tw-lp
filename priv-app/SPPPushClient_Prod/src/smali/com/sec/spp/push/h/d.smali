.class Lcom/sec/spp/push/h/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/util/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/h/c;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/h/c;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    iput-object p2, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/sec/spp/push/util/j;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->e()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->a()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/spp/push/h/c;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/p;

    iget-object v1, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/p;Ljava/lang/String;Z)V

    return-void
.end method

.method private c(Lcom/sec/spp/push/util/j;)V
    .locals 3

    const/16 v0, -0x6b

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/util/j;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/p;

    iget-object v1, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/p;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[Reg] onFail. errorCode="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", appId="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    iget-object v4, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v3, v2

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/c/b;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendRegistrationReq. onSuccess"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "msgInfo.regId="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "msgInfo.appId="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "msgInfo.userData="

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "msgInfo.ResultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "userSN :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->e()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->a()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lcom/sec/spp/push/util/p;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/p;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/spp/push/h/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/d;->b(Lcom/sec/spp/push/util/j;)V

    :goto_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->f()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/spp/push/h/d;->c(Lcom/sec/spp/push/util/j;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/h/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendRegistrationReq. onSuccess. But Error status"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    new-instance v1, Lcom/sec/spp/push/c/b;

    invoke-direct {v1}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;Lcom/sec/spp/push/c/b;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    invoke-static {v0}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;)Lcom/sec/spp/push/c/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/c/b;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/h/c;Lcom/sec/spp/push/c/b;)V

    iget-object v0, p0, Lcom/sec/spp/push/h/d;->a:Lcom/sec/spp/push/h/c;

    check-cast p1, Lcom/sec/spp/push/util/p;

    iget-object v1, p0, Lcom/sec/spp/push/h/d;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/h/c;->a(Lcom/sec/spp/push/util/p;Ljava/lang/String;Z)V

    goto :goto_1
.end method
