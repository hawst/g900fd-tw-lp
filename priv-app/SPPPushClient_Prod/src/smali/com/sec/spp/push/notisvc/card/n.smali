.class public Lcom/sec/spp/push/notisvc/card/n;
.super Lcom/sec/spp/push/notisvc/card/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/card/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJJLjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p17}, Lcom/sec/spp/push/notisvc/card/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIJJLjava/lang/String;IILjava/lang/String;)V

    return-void
.end method

.method private G()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "6"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "contentTitle"

    aput-object v1, v0, v2

    const-string v1, "contentText"

    aput-object v1, v0, v3

    const-string v1, "subContentText"

    aput-object v1, v0, v4

    :goto_0
    return-object v0

    :cond_1
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "9"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "contentTitle"

    aput-object v1, v0, v2

    const-string v1, "contentText"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_3
    const-string v1, "5"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "4"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contentTitle"

    aput-object v1, v0, v2

    const-string v1, "contentText"

    aput-object v1, v0, v3

    const-string v1, "subContentText"

    aput-object v1, v0, v4

    const-string v1, "streamingUrl"

    aput-object v1, v0, v5

    goto :goto_0

    :cond_5
    const-string v1, "7"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-array v0, v2, [Ljava/lang/String;

    goto :goto_0

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCardDataTxtList. invalid templateType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()[Ljava/lang/String;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->p()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "6"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "largeIcon"

    aput-object v1, v0, v2

    :goto_0
    return-object v0

    :cond_1
    const-string v1, "3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "5"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "4"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "largeIcon"

    aput-object v1, v0, v2

    const-string v1, "bigPicture"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_3
    const-string v1, "7"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "banner"

    aput-object v1, v0, v2

    goto :goto_0

    :cond_4
    const-string v1, "9"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "banner"

    aput-object v1, v0, v2

    const-string v1, "largeIcon"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_5
    const-string v1, "8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "banner"

    aput-object v1, v0, v2

    const-string v1, "largeIcon"

    aput-object v1, v0, v3

    const-string v1, "bigPicture"

    aput-object v1, v0, v4

    goto :goto_0

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCardDataImgList. invalid templateType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()V
    .locals 7

    const v6, 0x7f02000e

    const v5, 0x7f02000d

    const v4, 0x7f02000a

    const v3, 0x7f020004

    const/16 v2, 0x13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "default noti icon : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "setDefaultNotiIcons. sevicename is null"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f02001e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    invoke-virtual {p0, v3}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChatON_canada"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f02001f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    invoke-virtual {p0, v3}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Hub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f020022

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_3

    const v0, 0x7f020023

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f020022

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Music"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f020028

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_5

    const v0, 0x7f020029

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_5
    const v0, 0x7f020028

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f020032

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_7

    const v0, 0x7f020033

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto :goto_0

    :cond_7
    const v0, 0x7f020032

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Books"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f02001c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_9

    const v0, 0x7f02001d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f02001c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const v0, 0x7f020020

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_b

    const v0, 0x7f020021

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_b
    const v0, 0x7f020020

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Learning"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const v0, 0x7f020024

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_d

    const v0, 0x7f020025

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_d
    const v0, 0x7f020024

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Apps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const v0, 0x7f02001a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_f

    const v0, 0x7f02001b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_f
    const v0, 0x7f02001a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Wallet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const v0, 0x7f02002c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_11

    const v0, 0x7f02002d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_11
    const v0, 0x7f02002c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Swingo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const v0, 0x7f020030

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_13

    const v0, 0x7f020031

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_13
    const v0, 0x7f020030

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung WatchON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const v0, 0x7f020034

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_15

    const v0, 0x7f020035

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_15
    const v0, 0x7f020034

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung Account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const v0, 0x7f02002a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_17

    const v0, 0x7f02002b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_17
    const v0, 0x7f02002a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "s console"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const v0, 0x7f02002e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_19

    const v0, 0x7f02002f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_19
    const v0, 0x7f02002e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "samsung link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const v0, 0x7f020026

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1b

    const v0, 0x7f020027

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1b
    const v0, 0x7f020026

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1c
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "kindle for samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p0, v6}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1d

    const v0, 0x7f02000f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1d
    invoke-virtual {p0, v6}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1e
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "group play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-virtual {p0, v4}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1f

    const v0, 0x7f02000b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_1f
    invoke-virtual {p0, v4}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_20
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "milk music"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const v0, 0x7f020010

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_21

    const v0, 0x7f020011

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_21
    const v0, 0x7f020010

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_22
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "papergarden"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    const v0, 0x7f020012

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_23

    const v0, 0x7f020013

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_23
    const v0, 0x7f020012

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_24
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "shealth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    const v0, 0x7f020017

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_25

    const v0, 0x7f020018

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_25
    const v0, 0x7f020017

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_26
    invoke-virtual {p0, v5}, Lcom/sec/spp/push/notisvc/card/n;->b(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_27

    const v0, 0x7f020036

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0

    :cond_27
    invoke-virtual {p0, v5}, Lcom/sec/spp/push/notisvc/card/n;->a(I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCardImgData. Invalid params. ctx:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", key:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/card/n;->I()V

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/a;->a()Lcom/sec/spp/push/notisvc/agent/a;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/sec/spp/push/notisvc/card/c;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/d;->a()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p4, v2}, Lcom/sec/spp/push/notisvc/agent/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "largeIcon"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->j(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCardImgData. path is null. key:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    :cond_3
    const-string v1, "bigPicture"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->k(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v1, "banner"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->l(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCardImgData. invali key:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCardTxtData. Invalid params. key:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const-string v0, "contentTitle"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->g(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string v0, "contentText"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->h(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "subContentText"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->i(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "streamingUrl"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCardTxtData. invali key:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method private c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/16 v4, 0x2e

    const/16 v3, 0x23

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "shortenTickerByScreenSize. invalid params. ticker:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->f(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/b;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "shortenTickerByScreenSize. screenType is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    sget-object v1, Lcom/sec/spp/push/notisvc/b;->a:Lcom/sec/spp/push/notisvc/b;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_3
    :goto_0
    return-object p2

    :cond_4
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->i:Ljava/lang/String;

    return-object v0
.end method

.method public B()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/notisvc/card/n;->g:I

    return v0
.end method

.method public C()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/notisvc/card/n;->h:I

    return v0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->k:Ljava/lang/String;

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/sec/spp/push/notisvc/card/n;->g:I

    return-void
.end method

.method public a(Landroid/content/Context;Lorg/json/JSONObject;)V
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/sec/spp/push/notisvc/card/a;->a(Landroid/content/Context;Lorg/json/JSONObject;)V

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "parsingResource. invalid params. ctx:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", jo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const-string v0, "ticker"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\p{space}"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/spp/push/notisvc/card/n;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/card/n;->f(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/card/n;->G()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/card/n;->H()[Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    :cond_2
    const-string v0, "parsingResource. cannot parse resource"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/n;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    move v0, v1

    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_4

    :goto_1
    array-length v0, v3

    if-lt v1, v0, :cond_5

    return-void

    :cond_4
    aget-object v4, v2, v0

    invoke-direct {p0, p2, v4}, Lcom/sec/spp/push/notisvc/card/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->c()Ljava/lang/String;

    move-result-object v0

    aget-object v2, v3, v1

    invoke-direct {p0, p1, v0, p2, v2}, Lcom/sec/spp/push/notisvc/card/n;->a(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/sec/spp/push/notisvc/card/n;->h:I

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->b:Ljava/lang/String;

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->c:Ljava/lang/String;

    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->d:Ljava/lang/String;

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->e:Ljava/lang/String;

    return-void
.end method

.method public j(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->i:Ljava/lang/String;

    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->j:Ljava/lang/String;

    return-void
.end method

.method public l(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->k:Ljava/lang/String;

    return-void
.end method

.method public m(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/n;->f:Ljava/lang/String;

    return-void
.end method

.method public v()Ljava/util/Map;
    .locals 3

    invoke-super {p0}, Lcom/sec/spp/push/notisvc/card/a;->v()Ljava/util/Map;

    move-result-object v0

    const-string v1, "ticker"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->w()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "contentTitle"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->x()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "contentText"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->y()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "subContentText"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->z()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "streamingUrl"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->F()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "smallIconId"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->B()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "largeIconId"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->C()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "largeIconPath"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->A()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "bigPicPath"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->D()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "bannerPicPath"

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/n;->E()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->d:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/n;->e:Ljava/lang/String;

    return-object v0
.end method
