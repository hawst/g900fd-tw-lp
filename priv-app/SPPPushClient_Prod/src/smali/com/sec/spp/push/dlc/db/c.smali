.class public Lcom/sec/spp/push/dlc/db/c;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/sec/spp/push/dlc/db/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/db/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "logcollector.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/c;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/dlc/db/c;->b:Lcom/sec/spp/push/dlc/db/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/dlc/db/c;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/db/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/spp/push/dlc/db/c;->b:Lcom/sec/spp/push/dlc/db/c;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/dlc/db/c;->b:Lcom/sec/spp/push/dlc/db/c;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const-string v0, "CREATE TABLE log(id INTEGER PRIMARY KEY AUTOINCREMENT, insert_time BIGINT, svccode TEXT, category TEXT, timestamp INTEGER, activity TEXT, userid TEXT, deviceid TEXT, appver TEXT, body TEXT, pkg TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE urgent_log(id INTEGER PRIMARY KEY AUTOINCREMENT, svccode TEXT, category TEXT, timestamp INTEGER, activity TEXT, userid TEXT, deviceid TEXT, appver TEXT, body TEXT, pkg TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE summary_log(id INTEGER PRIMARY KEY AUTOINCREMENT, idx TEXT, svccode TEXT, category TEXT, timestamp INTEGER, activity TEXT, userid TEXT, deviceid TEXT, appver TEXT, body TEXT, pkg TEXT,sum1 INTEGER,sum2 INTEGER,sum3 INTEGER,sum4 INTEGER,sum5 INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DB Created"

    sget-object v1, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroid/database/sqlite/SQLiteOpenHelper;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    const-string v0, ""

    sget-object v1, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[onUpgrade] oldVersino : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    if-ge p2, v0, :cond_0

    :try_start_0
    const-string v0, "ALTER TABLE log ADD COLUMN insert_time BIGINT NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x4

    if-ge p2, v0, :cond_1

    const-string v0, "DROP TABLE IF EXISTS summary_log"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE summary_log(id INTEGER PRIMARY KEY AUTOINCREMENT, idx TEXT, svccode TEXT, category TEXT, timestamp INTEGER, activity TEXT, userid TEXT, deviceid TEXT, appver TEXT, body TEXT, pkg TEXT,sum1 INTEGER,sum2 INTEGER,sum3 INTEGER,sum4 INTEGER,sum5 INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_1
    sget-object v0, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    const-string v1, "DB Upgrade Done"

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[onUpgrade] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
