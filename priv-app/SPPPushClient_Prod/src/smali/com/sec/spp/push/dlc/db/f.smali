.class Lcom/sec/spp/push/dlc/db/f;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/db/e;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/db/e;Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/sec/spp/push/dlc/db/f;->a:Lcom/sec/spp/push/dlc/db/e;

    const-string v0, "dlc_regi_db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const-string v0, "CREATE TABLE dlc_regi_table (package TEXT PRIMARY KEY,wifiquota INTEGER NOT NULL,wifius INTEGER, dataquota INTEGER NOT NULL,dataus INTEGER,data_over TINYINT(1), intent TEXT NOT NULL, loadedSize INTEGER NOT NULL);"

    invoke-static {}, Lcom/sec/spp/push/dlc/db/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CREATE TABLE dlc_regi_table (package TEXT PRIMARY KEY,wifiquota INTEGER NOT NULL,wifius INTEGER, dataquota INTEGER NOT NULL,dataus INTEGER,data_over TINYINT(1), intent TEXT NOT NULL, loadedSize INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    :try_start_0
    const-string v0, "ALTER TABLE dlc_regi_table ADD COLUMN loadedSize INTEGER NOT NULL DEFAULT \'0\'"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_0
    const-string v0, "DB Upgrade Done"

    invoke-static {}, Lcom/sec/spp/push/dlc/db/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[onUpgrade] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/db/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
