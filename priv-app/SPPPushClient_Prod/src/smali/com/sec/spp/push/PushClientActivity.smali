.class public Lcom/sec/spp/push/PushClientActivity;
.super Landroid/app/Activity;


# instance fields
.field a:Landroid/widget/Button;

.field b:Landroid/widget/Button;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/os/Handler;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->c:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/PushClientActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/r;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/r;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    return-object v0
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/s;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/s;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    return-object v0
.end method

.method static synthetic b(Lcom/sec/spp/push/PushClientActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/spp/push/PushClientActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->e:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->setContentView(I)V

    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->a:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->a:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientActivity;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->b:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientActivity;->a()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090030

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/l;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/l;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09002d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->c:Landroid/widget/TextView;

    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/m;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/m;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/spp/push/n;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/n;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->d:Landroid/os/Handler;

    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/o;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/o;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/sec/spp/push/p;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/p;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    iput-object v0, p0, Lcom/sec/spp/push/PushClientActivity;->e:Landroid/os/Handler;

    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/PushClientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/q;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/q;-><init>(Lcom/sec/spp/push/PushClientActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
