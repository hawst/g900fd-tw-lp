.class Lcom/sec/spp/push/notisvc/agent/c;
.super Lcom/sec/spp/push/notisvc/agent/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/agent/b;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/sec/spp/push/notisvc/w;


# direct methods
.method private constructor <init>(Lcom/sec/spp/push/notisvc/agent/b;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/w;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/agent/c;->a:Lcom/sec/spp/push/notisvc/agent/b;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/agent/h;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/agent/c;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/agent/c;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/spp/push/notisvc/agent/c;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/spp/push/notisvc/agent/c;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/spp/push/notisvc/agent/c;->f:Lcom/sec/spp/push/notisvc/w;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/spp/push/notisvc/agent/b;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/w;Lcom/sec/spp/push/notisvc/agent/c;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/sec/spp/push/notisvc/agent/c;-><init>(Lcom/sec/spp/push/notisvc/agent/b;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/w;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    const-string v0, "download() result : success"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/x;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/x;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/sec/spp/push/notisvc/x;->a(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/c;->d:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/agent/c;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/c;->a:Lcom/sec/spp/push/notisvc/agent/b;

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/agent/c;->e:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/spp/push/notisvc/agent/b;->a(Lcom/sec/spp/push/notisvc/agent/b;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->f:Lcom/sec/spp/push/notisvc/w;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/notisvc/w;->a(Landroid/content/Context;Lcom/sec/spp/push/notisvc/x;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->c:Ljava/lang/String;

    const/16 v2, 0x3ef

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/notisvc/x;->a(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->f:Lcom/sec/spp/push/notisvc/w;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/notisvc/w;->b(Landroid/content/Context;Lcom/sec/spp/push/notisvc/x;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 3

    const-string v0, "download() download fail"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/x;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/x;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/sec/spp/push/notisvc/x;->a(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/c;->f:Lcom/sec/spp/push/notisvc/w;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/notisvc/w;->b(Landroid/content/Context;Lcom/sec/spp/push/notisvc/x;)V

    return-void
.end method
