.class public Lcom/sec/spp/push/notisvc/registration/f;
.super Lcom/sec/spp/push/notisvc/registration/h;


# static fields
.field private static b:Z

.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/String;


# instance fields
.field private e:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/f;->c:Ljava/lang/Object;

    const-class v0, Lcom/sec/spp/push/notisvc/registration/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/h;-><init>()V

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/f;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const-string v1, "build. intent is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "agreement"

    sget v2, Lcom/sec/spp/push/notisvc/registration/f;->a:I

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v3, v2}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v3, v2}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "build : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/f;

    invoke-direct {v0, v2}, Lcom/sec/spp/push/notisvc/registration/f;-><init>(Lcom/sec/spp/push/notisvc/registration/k;)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "build. status is wrong. status : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/registration/f;)Lcom/sec/spp/push/notisvc/registration/k;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 7

    const-string v0, "setAlarmAfterBoot."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_1

    const-string v0, "setAlarmAfterBoot. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;I)V

    :cond_2
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "status is null. Pref status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v2, 0xa

    if-le v0, v2, :cond_4

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    const-string v2, "regitimer:dvcRegi"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x493e0

    add-long/2addr v3, v5

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCRegistrationAlarmHandler;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "setAlarmAfterBoot. sppRegiStstus is DEREGISTER_INCOMPLETED. do spp dereg"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/push/a;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/push/a;-><init>()V

    invoke-virtual {v0, p0}, Lcom/sec/spp/push/notisvc/agent/push/a;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    if-nez p0, :cond_0

    const-string v0, "context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    const/16 v1, -0x3e8

    if-eq v0, v1, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_2

    :cond_1
    const-string v0, "no more incomp request."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(I)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong dvc regi value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong dvc regi value : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "request dvc reg : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.BIGJOE_REGI"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "agreement"

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method private l(Landroid/content/Context;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string v0, "isDeregAvailable. ctx is null"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x4

    new-array v3, v0, [I

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    aput v0, v3, v1

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    aput v0, v3, v2

    const/4 v0, 0x2

    sget-object v4, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v4

    aput v4, v3, v0

    const/4 v0, 0x3

    sget-object v4, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v4

    aput v4, v3, v0

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v0, "isDeregAvailable. dbHandler is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/database/SQLException;

    invoke-direct {v0}, Landroid/database/SQLException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    :goto_0
    array-length v5, v3

    if-lt v0, v5, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    return v1

    :cond_2
    aget v5, v3, v0

    invoke-virtual {v4, v5}, Lcom/sec/spp/push/notisvc/a/b;->a(I)Ljava/util/ArrayList;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v0, "isDeregAvailable. DBError. getAllSVCPackage is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    new-instance v0, Landroid/database/SQLException;

    invoke-direct {v0}, Landroid/database/SQLException;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_4

    const-string v0, "isDeregAvailable. There is agree or disagree status app. cannot dereg"

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setOnWorking : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p1, Lcom/sec/spp/push/notisvc/registration/f;->b:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 2

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/notisvc/registration/f;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/agent/registration/"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80/agent/deregistration/"

    goto :goto_0
.end method

.method c(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "checkBeforeRequest. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string v0, "SPP Reg status is incompleted. delay regi request"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/l;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SPP Reg is incompleted. Do SPP Reg"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;I)V

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/push/a;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/push/a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/agent/push/a;->a(Landroid/content/Context;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/registration/f;->l(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "checkBeforeRequest. Currently cannot dereg."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->c:Lcom/sec/spp/push/notisvc/registration/i;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-eq v0, v1, :cond_5

    const-string v0, "checkBeforeRequest. Not yet SPP Dereg. Do SPP Dereg"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;I)V

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/push/a;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/push/a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/agent/push/a;->b(Landroid/content/Context;)V

    :cond_5
    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto/16 :goto_0

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "checkBeforeRequest. invalid status:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto/16 :goto_0
.end method

.method d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    const-string v0, "delayRequest."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "incom Status is null. status value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;I)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0
.end method

.method e(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "saveData. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "saveData. incom Status is null. status value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->b()I

    move-result v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->a(Landroid/content/Context;I)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/spp/push/notisvc/d/b;->f(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;I)V

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0
.end method

.method f(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    const-string v1, "getDVCBody."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v1, "getDVCBody. Context is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->d(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "dataType"

    const-string v3, "wifi"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_1
    const-string v1, "platform"

    const-string v3, "android"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "osVersion"

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->b()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    :cond_1
    const-string v3, "language"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "bcVersion"

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "dataType"

    const-string v3, "mobile"

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    const-string v1, "getSVCBody. JSONEXception"

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method g(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/agent/h;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string v1, "getCallbackMethod. Context is null."

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/sec/spp/push/notisvc/registration/g;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/f;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/registration/k;->b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v2

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/sec/spp/push/notisvc/registration/g;-><init>(Lcom/sec/spp/push/notisvc/registration/f;Landroid/content/Context;Lcom/sec/spp/push/notisvc/registration/k;Lcom/sec/spp/push/notisvc/registration/g;)V

    move-object v0, v1

    goto :goto_0
.end method
