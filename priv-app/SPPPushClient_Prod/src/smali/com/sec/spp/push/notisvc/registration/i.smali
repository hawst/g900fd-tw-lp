.class public final enum Lcom/sec/spp/push/notisvc/registration/i;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/registration/i;

.field public static final enum b:Lcom/sec/spp/push/notisvc/registration/i;

.field public static final enum c:Lcom/sec/spp/push/notisvc/registration/i;

.field public static final enum d:Lcom/sec/spp/push/notisvc/registration/i;

.field private static final synthetic f:[Lcom/sec/spp/push/notisvc/registration/i;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/i;

    const-string v1, "CONTINUE"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/spp/push/notisvc/registration/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/i;

    const-string v1, "DELAY"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/registration/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/i;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/spp/push/notisvc/registration/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/i;->c:Lcom/sec/spp/push/notisvc/registration/i;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/i;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4, v6}, Lcom/sec/spp/push/notisvc/registration/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    new-array v0, v6, [Lcom/sec/spp/push/notisvc/registration/i;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->c:Lcom/sec/spp/push/notisvc/registration/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/i;->f:[Lcom/sec/spp/push/notisvc/registration/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/registration/i;->e:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/i;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/registration/i;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/registration/i;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/i;->f:[Lcom/sec/spp/push/notisvc/registration/i;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/registration/i;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
