.class Lcom/sec/spp/push/e/b/h;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/e/b/g;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/e/b/g;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a()I
    .locals 5

    const/16 v0, 0x1467

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v1, v1, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v1}, Lcom/sec/spp/push/e/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/g;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[GetPort] Same WiFi AP:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v0, v0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/util/g;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v2, v2, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[GetPort] Hit AP :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v0, v0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    iget-object v2, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v2, v2, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/b;->b(I)V

    :goto_1
    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v0, v0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b;->d()I

    move-result v0

    iget-object v2, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v2, v2, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/e/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[GetPort] No Hit AP:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v2, v2, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/e/b;->b(I)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v1, v1, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/e/b;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    iget-object v0, v0, Lcom/sec/spp/push/e/b/g;->a:Lcom/sec/spp/push/e/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b;->f()V

    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[HeartBeat] Not Connected."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[HeartBeat] Connected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v1

    const-wide/32 v2, 0x186a0

    const/16 v4, 0xc

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/sec/spp/push/d/a/b;->a(JLjava/lang/String;I)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/e/a/g;->a(Lcom/google/protobuf/MessageLite;)V
    :try_end_0
    .catch Lcom/sec/spp/push/c/e; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[HeartBeat] NetworkNotAvailableException."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    const/4 v1, -0x2

    invoke-static {v0, p1, v1}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    goto :goto_0
.end method

.method private b(Landroid/os/Message;)V
    .locals 5

    const/4 v4, -0x2

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[Init.] Target server is null "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/spp/push/e/b/h;->a()I

    move-result v1

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->f()Z

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/spp/push/e/a/g;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Lcom/sec/spp/push/c/e; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v1

    const-wide/32 v2, 0x186a0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/spp/push/d/a/b;->a(JLjava/lang/String;)V

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/e/a/g;->a(Lcom/google/protobuf/MessageLite;)V
    :try_end_1
    .catch Lcom/sec/spp/push/c/e; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage. NetworkNotAvailableException."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-static {v0, p1, v4}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/b/h;->a(I)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage. NetworkNotAvailableException."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/b/h;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-static {v0, p1, v4}, Lcom/sec/spp/push/e/b/g;->b(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleMessage. ConnectionException:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/c/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/b/h;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    const/16 v1, -0x68

    invoke-static {v0, p1, v1}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connected Server : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private c(Landroid/os/Message;)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "gld.push.samsungosp.com"

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/d;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/e/b/h;->a()I

    move-result v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[PROV] Connecting to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->e()Z

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/spp/push/e/a/d;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v1

    const-wide/32 v2, 0x186a0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/spp/push/d/a/b;->a(JLjava/lang/String;)V

    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/e/a/d;->a(Lcom/google/protobuf/MessageLite;)V
    :try_end_1
    .catch Lcom/sec/spp/push/c/e; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[PROV] Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/b/h;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    const/16 v1, -0x67

    invoke-static {v0, p1, v1}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[PROV] Already connected. IP : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/k;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/b/h;->a(I)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    const/4 v1, -0x2

    invoke-static {v0, p1, v1}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;Landroid/os/Message;I)V

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/b/h;->a:Lcom/sec/spp/push/e/b/g;

    invoke-static {v0}, Lcom/sec/spp/push/e/b/g;->a(Lcom/sec/spp/push/e/b/g;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/PushClientApplication;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/spp/push/PushClientApplication;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service is destroyed"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage. message.what:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v2, -0x64

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/spp/push/d/a/b;->a(IILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/spp/push/e/b/h;->c(Landroid/os/Message;)V

    goto :goto_0

    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_5

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_6

    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/spp/push/e/b/h;->b(Landroid/os/Message;)V

    goto :goto_0

    :cond_6
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/e/b/h;->a(Landroid/os/Message;)V

    goto :goto_0
.end method
