.class Lcom/sec/spp/push/u;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/PushClientService;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/PushClientService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/u;->a:Lcom/sec/spp/push/PushClientService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[PushClientService]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BroadcastReceiver Received : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PushServiceStopAction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    const-string v0, "[PushClientService]"

    const-string v1, "PushClientService.getPushConnectionManager().disconnect"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[PushClientService]"

    const-string v1, "Push Connection already Disconected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "com.sec.spp.action.SPP_REQUEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/u;->a:Lcom/sec/spp/push/PushClientService;

    invoke-static {v0, p2}, Lcom/sec/spp/push/PushClientService;->a(Lcom/sec/spp/push/PushClientService;Landroid/content/Intent;)V

    goto :goto_0
.end method
