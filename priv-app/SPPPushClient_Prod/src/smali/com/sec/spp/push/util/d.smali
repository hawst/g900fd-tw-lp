.class public final enum Lcom/sec/spp/push/util/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/util/d;

.field public static final enum b:Lcom/sec/spp/push/util/d;

.field private static final synthetic c:[Lcom/sec/spp/push/util/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/spp/push/util/d;

    const-string v1, "NORMAL_MODE"

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/util/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    new-instance v0, Lcom/sec/spp/push/util/d;

    const-string v1, "FOTA_ONLY_MODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/util/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/spp/push/util/d;

    sget-object v1, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/spp/push/util/d;->c:[Lcom/sec/spp/push/util/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/util/d;
    .locals 1

    const-class v0, Lcom/sec/spp/push/util/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/util/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/util/d;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/util/d;->c:[Lcom/sec/spp/push/util/d;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/util/d;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
