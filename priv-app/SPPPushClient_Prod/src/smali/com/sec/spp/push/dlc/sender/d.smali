.class public Lcom/sec/spp/push/dlc/sender/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private final b:Ljava/lang/String;

.field private final c:Lcom/sec/spp/push/dlc/sender/e;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/d;->a:Z

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/d;->b:Ljava/lang/String;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/e;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/e;-><init>(Lcom/sec/spp/push/dlc/sender/d;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/dlc/sender/d;)Lcom/sec/spp/push/dlc/sender/e;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/os/Message;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/os/Message;
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    invoke-static {v0, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/spp/push/dlc/sender/d;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "StateMachine.obtainMessage(what) EX what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " target="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HSM"

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final a(Landroid/os/Message;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/dlc/sender/e;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Landroid/os/Message;J)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/spp/push/dlc/sender/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public a(Lcom/sec/spp/push/dlc/sender/c;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iput-object p1, v0, Lcom/sec/spp/push/dlc/sender/e;->c:Lcom/sec/spp/push/dlc/sender/c;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iput-object p1, v0, Lcom/sec/spp/push/dlc/sender/e;->b:Lcom/sec/spp/push/dlc/sender/c;

    return-void
.end method

.method public final a(Lcom/sec/spp/push/dlc/sender/i;)V
    .locals 2

    iget-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/d;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StateMachine.deferMessage EX mDeferredMessages="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v1, v1, Lcom/sec/spp/push/dlc/sender/e;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HSM"

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/d;->a()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v1, v1, Lcom/sec/spp/push/dlc/sender/e;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    return-void
.end method

.method public b()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/dlc/sender/e;->removeMessages(I)V

    return-void
.end method

.method public final b(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v0, v0, Lcom/sec/spp/push/dlc/sender/e;->a:Lcom/sec/spp/push/dlc/sender/c;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ADD DEFERRED MESSAGE"

    const-string v1, "HSM"

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v0, v0, Lcom/sec/spp/push/dlc/sender/e;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Lcom/sec/spp/push/dlc/sender/c;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iput-object p1, v0, Lcom/sec/spp/push/dlc/sender/e;->b:Lcom/sec/spp/push/dlc/sender/c;

    return-void
.end method

.method public final c(Lcom/sec/spp/push/dlc/sender/c;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v1, v1, Lcom/sec/spp/push/dlc/sender/e;->a:Lcom/sec/spp/push/dlc/sender/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] TRANS TO ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HSM"

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iget-object v1, v1, Lcom/sec/spp/push/dlc/sender/e;->a:Lcom/sec/spp/push/dlc/sender/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] TRANS TO ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HSM"

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/d;->c:Lcom/sec/spp/push/dlc/sender/e;

    iput-object p1, v0, Lcom/sec/spp/push/dlc/sender/e;->b:Lcom/sec/spp/push/dlc/sender/c;

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/d;->a()Landroid/os/Message;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->r:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    return-void
.end method
