.class Lcom/sec/spp/push/d/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/d/a/a;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/d/a/b;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Z

.field private final synthetic d:Ljava/util/List;

.field private final synthetic e:I

.field private final synthetic f:Lcom/sec/spp/push/util/h;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/d/a/b;Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/d/a/g;->a:Lcom/sec/spp/push/d/a/b;

    iput-object p2, p0, Lcom/sec/spp/push/d/a/g;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/sec/spp/push/d/a/g;->c:Z

    iput-object p4, p0, Lcom/sec/spp/push/d/a/g;->d:Ljava/util/List;

    iput p5, p0, Lcom/sec/spp/push/d/a/g;->e:I

    iput-object p6, p0, Lcom/sec/spp/push/d/a/g;->f:Lcom/sec/spp/push/util/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[SendNotiAck] onRequestExecute()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/g;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/g;->b:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/sec/spp/push/d/a/g;->c:Z

    iget-object v3, p0, Lcom/sec/spp/push/d/a/g;->d:Ljava/util/List;

    iget v4, p0, Lcom/sec/spp/push/d/a/g;->e:I

    iget-object v5, p0, Lcom/sec/spp/push/d/a/g;->f:Lcom/sec/spp/push/util/h;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V

    return-void
.end method

.method public a(I)V
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[SendNotiAck] onRequestCancel()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/g;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/g;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/g;->f:Lcom/sec/spp/push/util/h;

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;Lcom/sec/spp/push/util/h;I)V

    return-void
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/g;->b:Ljava/lang/String;

    return-object v0
.end method
