.class public Lcom/sec/spp/push/f/c;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Landroid/os/PowerManager$WakeLock;

.field private static c:Lcom/sec/spp/push/f/c;

.field private static final d:Ljava/lang/Object;


# instance fields
.field private e:Lcom/sec/spp/push/c/b;

.field private f:Ljava/util/ArrayList;

.field private g:Lcom/sec/spp/push/f/e;

.field private final h:Lcom/sec/spp/push/f/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/f/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/f/c;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/f/c;->e:Lcom/sec/spp/push/c/b;

    invoke-static {}, Lcom/sec/spp/push/f/c;->g()V

    new-instance v0, Lcom/sec/spp/push/f/a;

    invoke-direct {v0}, Lcom/sec/spp/push/f/a;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/f/c;->h:Lcom/sec/spp/push/f/a;

    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/spp/push/f/c;
    .locals 2

    const-class v1, Lcom/sec/spp/push/f/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/f/c;->c:Lcom/sec/spp/push/f/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/f/c;

    invoke-direct {v0}, Lcom/sec/spp/push/f/c;-><init>()V

    sput-object v0, Lcom/sec/spp/push/f/c;->c:Lcom/sec/spp/push/f/c;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/f/c;->c:Lcom/sec/spp/push/f/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(I)V
    .locals 5

    const-wide/16 v0, 0x1f4

    const/16 v2, 0xc

    if-ne p0, v2, :cond_1

    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Acquire NOTI_WAKE_LOCK for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    sget-object v2, Lcom/sec/spp/push/f/c;->d:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/f/c;->g()V

    sget-object v3, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3, v0, v1}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    return-void

    :cond_1
    const/4 v2, 0x3

    if-eq p0, v2, :cond_0

    const/16 v0, 0x9

    if-ne p0, v0, :cond_2

    const-wide/16 v0, 0x1388

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "This msgType doesn\'t need wakelock"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    :try_start_2
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "mWakeLock == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "ReliabilityLevel : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", Noti. Type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "notiId is empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    invoke-direct {p0, p3}, Lcom/sec/spp/push/f/c;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/f/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :try_start_0
    invoke-static {p5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    :goto_1
    iget-object v3, p0, Lcom/sec/spp/push/f/c;->f:Ljava/util/ArrayList;

    move-object v0, p0

    move-object v1, p4

    move v4, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/spp/push/f/c;->a(Ljava/lang/String;ZLjava/util/ArrayList;IJ)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-wide/16 v5, 0x0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x2

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/spp/push/h/c;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/sec/spp/push/util/l;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ResultCode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Lcom/sec/spp/push/c/b;

    invoke-direct {v1}, Lcom/sec/spp/push/c/b;-><init>()V

    iput-object v1, p0, Lcom/sec/spp/push/f/c;->e:Lcom/sec/spp/push/c/b;

    iget-object v1, p0, Lcom/sec/spp/push/f/c;->e:Lcom/sec/spp/push/c/b;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/c/b;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/f/c;->e:Lcom/sec/spp/push/c/b;

    return-void
.end method

.method public static c()V
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v3, "release NOTI_WAKE_LOCK"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    :try_start_0
    sget-object v2, Lcom/sec/spp/push/f/c;->d:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v3, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    sget-object v3, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v4, "Is all wake lock released? %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v2

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "just leave wakelock because SDK_VERSION "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static g()V
    .locals 4

    sget-object v1, Lcom/sec/spp/push/f/c;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/f/c;->b:Landroid/os/PowerManager$WakeLock;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/sec/spp/push/util/l;)V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "broadcastNotiIntent process is going on"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->c()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v2, "notificationId"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "appId"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "sender"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "msg"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "appData"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "timeStamp"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->f()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "sessionInfo"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "connectionTerm"

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->g()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "ack"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v2, "========================================================"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application  ID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Message         : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Messsage Sender : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v2, "========================================================"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/l;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 4

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "broadcastNotiAckIntent. notiAckId is empty."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.NotificationAckResultAction"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "notificationId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ackResult"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v2, "========================================================"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application  ID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notiAckId       : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bSuccess\t\t: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v2, "========================================================"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p4, p5}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;J)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "broadcastNotiAckIntent process is not going on. appId is empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/util/ArrayList;IJ)V
    .locals 7

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v6

    new-instance v0, Lcom/sec/spp/push/f/d;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/f/d;-><init>(Lcom/sec/spp/push/f/c;ZLjava/lang/String;J)V

    move-object v1, v6

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 10

    const/4 v9, 0x2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "ackIds is empty. return"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NotificationHandler element count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/f/c;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/util/l;

    iget-object v1, p0, Lcom/sec/spp/push/f/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->d()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->c()I

    move-result v1

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->l()Ljava/lang/String;

    move-result-object v5

    if-ne v2, v9, :cond_4

    invoke-direct {p0, v0}, Lcom/sec/spp/push/f/c;->b(Lcom/sec/spp/push/util/l;)V

    :cond_3
    :goto_1
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/f/c;->a(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-nez v2, :cond_6

    invoke-direct {p0, v4, v5}, Lcom/sec/spp/push/f/c;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-ne v1, v9, :cond_5

    iget-object v7, p0, Lcom/sec/spp/push/f/c;->h:Lcom/sec/spp/push/f/a;

    invoke-virtual {v7, v0}, Lcom/sec/spp/push/f/a;->a(Lcom/sec/spp/push/util/l;)V

    iget-object v7, p0, Lcom/sec/spp/push/f/c;->h:Lcom/sec/spp/push/f/a;

    invoke-virtual {v7}, Lcom/sec/spp/push/f/a;->a()V

    :cond_5
    sget-object v7, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v8, "Broadcast notification will be sent to app"

    invoke-static {v7, v8}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/f/c;->a(Lcom/sec/spp/push/util/l;)V

    goto :goto_1

    :cond_6
    const/4 v7, 0x1

    if-ne v2, v7, :cond_8

    iget-object v7, p0, Lcom/sec/spp/push/f/c;->g:Lcom/sec/spp/push/f/e;

    if-nez v7, :cond_7

    new-instance v7, Lcom/sec/spp/push/f/e;

    invoke-direct {v7}, Lcom/sec/spp/push/f/e;-><init>()V

    iput-object v7, p0, Lcom/sec/spp/push/f/c;->g:Lcom/sec/spp/push/f/e;

    :cond_7
    iget-object v7, p0, Lcom/sec/spp/push/f/c;->g:Lcom/sec/spp/push/f/e;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/spp/push/f/e;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    const/16 v7, 0xa

    if-lt v2, v7, :cond_9

    const/16 v7, 0x63

    if-gt v2, v7, :cond_9

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/f/c;->a(Lcom/sec/spp/push/util/l;)V

    goto :goto_1

    :cond_9
    const/16 v7, 0x64

    if-lt v2, v7, :cond_3

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/f/c;->a(Lcom/sec/spp/push/util/l;)V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    sget-object v0, Lcom/sec/spp/push/f/c;->a:Ljava/lang/String;

    const-string v1, "notificationHandlerDestroy is going on"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-class v1, Lcom/sec/spp/push/f/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/f/c;->c:Lcom/sec/spp/push/f/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/f/c;->c:Lcom/sec/spp/push/f/c;

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Lcom/sec/spp/push/f/e;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/f/c;->g:Lcom/sec/spp/push/f/e;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/f/e;

    invoke-direct {v0}, Lcom/sec/spp/push/f/e;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/f/c;->g:Lcom/sec/spp/push/f/e;

    goto :goto_0
.end method

.method public e()Lcom/sec/spp/push/f/a;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/f/c;->h:Lcom/sec/spp/push/f/a;

    return-object v0
.end method
