.class public Lcom/sec/spp/push/monitor/SystemStateMonitorService;
.super Landroid/app/IntentService;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "SystemStateMonitorService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private static a()V
    .locals 5

    const/4 v4, -0x1

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    const-string v2, "[SystemStateMonitorService]"

    const-string v3, "No Active Internet"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v2, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v2, :cond_0

    invoke-static {v4}, Lcom/sec/spp/push/monitor/SystemStateMonitorService;->a(I)V

    :cond_0
    invoke-static {v4}, Lcom/sec/spp/push/util/k;->b(I)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "[SystemStateMonitorService]"

    const-string v1, "RegistrationTable is empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_1
    .catch Lcom/sec/spp/push/c/a; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    const-string v0, "[SystemStateMonitorService]"

    const-string v1, "signalNoActive. No Network --> Stopping PUSH service........."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "[SystemStateMonitorService]"

    const-string v2, "[ProvChannel] Prov Connection already Disconected"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v0, "[SystemStateMonitorService]"

    const-string v1, "[PushChannel] Push Connection already Disconected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static a(I)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.ACTION_CHANNEL_NET_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.EXTRA_PREV_TYPE"

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TYPE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private static b()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "[SystemStateMonitorService]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "signalActive. [Current] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", [latestConnectedType] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/util/k;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", [New] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v1

    if-ne v1, v0, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    invoke-static {}, Lcom/sec/spp/push/util/g;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SystemStateMonitorService]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "signalActive. Last BSSID is : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new BSSID is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/spp/push/util/k;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-static {v0}, Lcom/sec/spp/push/util/k;->b(I)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v0, "[SystemStateMonitorService]"

    const-string v1, "RegistrationTable is empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v1

    if-ne v1, v0, :cond_2

    goto/16 :goto_0

    :cond_4
    invoke-static {v0}, Lcom/sec/spp/push/util/k;->c(I)V

    invoke-static {}, Lcom/sec/spp/push/util/g;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/util/k;->a(Ljava/lang/String;)V

    const-string v1, "[SystemStateMonitorService]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BSSIID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/g;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/d;->d()Z

    move-result v3

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    invoke-virtual {v2}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    :try_start_1
    invoke-virtual {v2}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_1
    .catch Lcom/sec/spp/push/c/a; {:try_start_1 .. :try_end_1} :catch_1

    :cond_6
    :goto_2
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/d;->n()V

    invoke-static {}, Lcom/sec/spp/push/c/b;->c()V

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/sec/spp/push/c/b;->b(J)V

    sget-boolean v1, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v1, :cond_7

    invoke-static {v0}, Lcom/sec/spp/push/monitor/SystemStateMonitorService;->a(I)V

    :cond_7
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->n()Z

    move-result v0

    if-eqz v0, :cond_8

    const-wide/16 v0, 0x2710

    invoke-static {v0, v1}, Lcom/sec/spp/push/c/b;->a(J)V

    :cond_8
    const-string v0, "[SystemStateMonitorService]"

    const-string v1, "signalActive. Start the PUSH service........."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v3, "[SystemStateMonitorService]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/spp/push/c/a;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v2, "[SystemStateMonitorService]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/spp/push/c/a;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "[SystemStateMonitorService]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SystemStateMonitorService] action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/monitor/c;->b:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/monitor/c;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/monitor/SystemStateMonitorService;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/spp/push/monitor/c;->a:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/monitor/c;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/monitor/SystemStateMonitorService;->a()V

    goto :goto_0
.end method
