.class Lcom/sec/spp/push/f/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/util/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/f/c;

.field private final synthetic b:Z

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:J


# direct methods
.method constructor <init>(Lcom/sec/spp/push/f/c;ZLjava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/f/d;->a:Lcom/sec/spp/push/f/c;

    iput-boolean p2, p0, Lcom/sec/spp/push/f/d;->b:Z

    iput-object p3, p0, Lcom/sec/spp/push/f/d;->c:Ljava/lang/String;

    iput-wide p4, p0, Lcom/sec/spp/push/f/d;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/f/c;->f()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[NotiAck] Fail errorCode:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", appId:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/spp/push/f/d;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/f/d;->a:Lcom/sec/spp/push/f/c;

    iget-object v1, p0, Lcom/sec/spp/push/f/d;->c:Ljava/lang/String;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/sec/spp/push/f/d;->d:J

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/f/c;->a(Ljava/lang/String;Ljava/lang/String;ZJ)V

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/f/c;->c()V

    return-void
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 6

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/f/c;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[NotiAck] Success()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/spp/push/f/d;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/f/d;->a:Lcom/sec/spp/push/f/c;

    iget-object v1, p0, Lcom/sec/spp/push/f/d;->c:Ljava/lang/String;

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/sec/spp/push/f/d;->d:J

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/f/c;->a(Ljava/lang/String;Ljava/lang/String;ZJ)V

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/f/c;->c()V

    return-void
.end method
