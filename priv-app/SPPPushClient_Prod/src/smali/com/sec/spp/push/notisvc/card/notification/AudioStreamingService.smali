.class public Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;
.super Landroid/app/Service;

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# static fields
.field private static a:Landroid/media/MediaPlayer;

.field private static final b:Ljava/lang/String;

.field private static f:Ljava/lang/String;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Landroid/net/Uri;

.field private e:Ljava/lang/String;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    return-void
.end method

.method public static a()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const-string v0, "stop(). release media player"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    sput-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    sput-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "initialize()"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    const-string v0, "playStatus"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    const-string v0, "mid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    const-string v0, "streamingUrl"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onStartCommand(). null params.  playStatus:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", streamingUrl:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->d:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "initialize(). streamingUrl="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notiTag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->c:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "initialize(). Illegal streamingUrl"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "initialize(). SecurityException"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v0, "initialize(). IllegalStateException"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    const-string v0, "initialize(). IOException"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "sendFeedback."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/c/c;->a()Lcom/sec/spp/push/notisvc/c/c;

    return-void
.end method

.method private b()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPlay. isPlaying : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "onPlay. already playing"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->b:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPause. isPlaying : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "onPlay. already puased"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    const-string v0, "onCompleted."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    return-void
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "onCreate. AudioStreamingService Start"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "onDestroy()"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onError. : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    const-string v1, "streaming_url_invalid"

    invoke-direct {p0, v0, v1}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    const-string v0, "onPrepared()"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->b:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    const-string v1, "played"

    invoke-direct {p0, v0, v1}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const/4 v0, 0x2

    if-eqz p1, :cond_0

    const-string v1, "mid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "onStartCommand(). null parameter"

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    if-nez v1, :cond_4

    const-string v1, "onStartCommand(). service start"

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Landroid/content/Intent;)V

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-nez v1, :cond_2

    const-string v1, "onStartCommand(). intent has no data"

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    :cond_3
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    const-string v2, "mid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_8

    const-string v1, "onStartCommand(). Service is alreadyOn"

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "playStatus"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b()V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/b;->b:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->e:Ljava/lang/String;

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/b;->c:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->c()V

    goto :goto_0

    :cond_7
    const-string v1, "onStartCommand(). Can\'t Play"

    sget-object v2, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->f:Ljava/lang/String;

    const-string v1, "mid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "onStartCommand(). new service start"

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/notification/b;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a()V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a(Landroid/content/Intent;)V

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/AudioStreamingService;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    goto/16 :goto_1
.end method
