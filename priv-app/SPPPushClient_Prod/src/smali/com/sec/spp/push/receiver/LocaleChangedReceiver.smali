.class public Lcom/sec/spp/push/receiver/LocaleChangedReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/receiver/LocaleChangedReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/receiver/LocaleChangedReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->h()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/receiver/LocaleChangedReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/spp/push/c/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/sec/spp/push/receiver/LocaleChangedReceiver;->a:Ljava/lang/String;

    const-string v1, "Locale Changed : DoReInit"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/receiver/LocaleChangedReceiver;->a()V

    return-void
.end method
