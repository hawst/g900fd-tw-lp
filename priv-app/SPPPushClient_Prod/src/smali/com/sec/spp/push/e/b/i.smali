.class public final enum Lcom/sec/spp/push/e/b/i;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/e/b/i;

.field public static final enum b:Lcom/sec/spp/push/e/b/i;

.field public static final enum c:Lcom/sec/spp/push/e/b/i;

.field public static final enum d:Lcom/sec/spp/push/e/b/i;

.field private static final e:I

.field private static f:[Lcom/sec/spp/push/e/b/i;

.field private static final synthetic g:[Lcom/sec/spp/push/e/b/i;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/spp/push/e/b/i;

    const-string v1, "Primary_1st_port"

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/e/b/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/e/b/i;->a:Lcom/sec/spp/push/e/b/i;

    new-instance v0, Lcom/sec/spp/push/e/b/i;

    const-string v1, "Primary_2nd_port"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/e/b/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/e/b/i;->b:Lcom/sec/spp/push/e/b/i;

    new-instance v0, Lcom/sec/spp/push/e/b/i;

    const-string v1, "Secondary_1st_port"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/e/b/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/e/b/i;->c:Lcom/sec/spp/push/e/b/i;

    new-instance v0, Lcom/sec/spp/push/e/b/i;

    const-string v1, "Secondary_2nd_port"

    invoke-direct {v0, v1, v5}, Lcom/sec/spp/push/e/b/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/e/b/i;->d:Lcom/sec/spp/push/e/b/i;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/spp/push/e/b/i;

    sget-object v1, Lcom/sec/spp/push/e/b/i;->a:Lcom/sec/spp/push/e/b/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/e/b/i;->b:Lcom/sec/spp/push/e/b/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/e/b/i;->c:Lcom/sec/spp/push/e/b/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/e/b/i;->d:Lcom/sec/spp/push/e/b/i;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/spp/push/e/b/i;->g:[Lcom/sec/spp/push/e/b/i;

    const-class v0, Lcom/sec/spp/push/e/b/i;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    sput v0, Lcom/sec/spp/push/e/b/i;->e:I

    sget v0, Lcom/sec/spp/push/e/b/i;->e:I

    new-array v0, v0, [Lcom/sec/spp/push/e/b/i;

    sput-object v0, Lcom/sec/spp/push/e/b/i;->f:[Lcom/sec/spp/push/e/b/i;

    const-class v0, Lcom/sec/spp/push/e/b/i;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/i;

    sget-object v2, Lcom/sec/spp/push/e/b/i;->f:[Lcom/sec/spp/push/e/b/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v3

    aput-object v0, v2, v3

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/sec/spp/push/e/b/i;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/e/b/i;->f:[Lcom/sec/spp/push/e/b/i;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/e/b/i;
    .locals 1

    const-class v0, Lcom/sec/spp/push/e/b/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/i;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/e/b/i;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/e/b/i;->g:[Lcom/sec/spp/push/e/b/i;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/e/b/i;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public a()Lcom/sec/spp/push/e/b/i;
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/util/k;->e()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sget v1, Lcom/sec/spp/push/e/b/i;->e:I

    rem-int/2addr v0, v1

    invoke-static {v0}, Lcom/sec/spp/push/e/b/i;->a(I)Lcom/sec/spp/push/e/b/i;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/spp/push/e/b/i;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    sget v1, Lcom/sec/spp/push/e/b/i;->e:I

    rem-int/2addr v0, v1

    invoke-static {v0}, Lcom/sec/spp/push/e/b/i;->a(I)Lcom/sec/spp/push/e/b/i;

    move-result-object v0

    goto :goto_0
.end method
