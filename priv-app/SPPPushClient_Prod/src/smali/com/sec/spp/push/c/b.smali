.class public Lcom/sec/spp/push/c/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:I

.field private static c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/c/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/sec/spp/push/c/b;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 4

    const-class v1, Lcom/sec/spp/push/c/b;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v2

    const-string v3, "ReconnectionAlarm"

    invoke-virtual {v2, v0, v3}, Lcom/sec/spp/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v2, "[ReInit] Cancel reconnection timer"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/a/g;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(J)V
    .locals 7

    const-class v6, Lcom/sec/spp/push/c/b;

    monitor-enter v6

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/c/b;->a()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/g;->c(Z)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v0

    const-string v2, "ReconnectionAlarm"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    add-long/2addr v3, p0

    new-instance v5, Lcom/sec/spp/push/c/c;

    invoke-direct {v5}, Lcom/sec/spp/push/c/c;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/util/a;)V

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ReInit] Set Try Interval : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/sec/spp/push/c/b;->c(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.DuplicationDeviceIdError"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    sget-object v1, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v2, "sendBroadcast : PUSH_DUPLICATION_DEVICEID_ERROR"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(Z)V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setForceStopService:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/spp/push/k;->a(Z)V

    return-void
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    if-gez p0, :cond_0

    sparse-switch p0, :sswitch_data_0

    const-string v0, "UNKNOWN INTERNAL ERROR CODE TYPE"

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "TIMEOUT"

    goto :goto_0

    :sswitch_1
    const-string v0, "NETWORK_NOT_AVAILABLE"

    goto :goto_0

    :sswitch_2
    const-string v0, "PROVISIONING_DATA_EXISTS"

    goto :goto_0

    :sswitch_3
    const-string v0, "INITIALIZATION_ALREADY_COMPLETED"

    goto :goto_0

    :sswitch_4
    const-string v0, "PROVISIONING_FAIL"

    goto :goto_0

    :sswitch_5
    const-string v0, "INITIALIZATION_FAIL"

    goto :goto_0

    :sswitch_6
    const-string v0, "EMPTY_DEVICE_ID"

    goto :goto_0

    :sswitch_7
    const-string v0, "DATABASE_ERROR"

    goto :goto_0

    :cond_0
    const/16 v0, 0x3e8

    if-lt p0, v0, :cond_1

    const/16 v0, 0xfac

    if-gt p0, v0, :cond_1

    sparse-switch p0, :sswitch_data_1

    const-string v0, "UNKNOWN MSG RESULT CODE TYPE"

    goto :goto_0

    :sswitch_8
    const-string v0, "SUCCESS"

    goto :goto_0

    :sswitch_9
    const-string v0, "UNKNOWN_MESSAGE_TYPE"

    goto :goto_0

    :sswitch_a
    const-string v0, "UNEXPECTED_MESSAGE"

    goto :goto_0

    :sswitch_b
    const-string v0, "INTERNAL_SERVER_ERROR"

    goto :goto_0

    :sswitch_c
    const-string v0, "INTERRUPTED"

    goto :goto_0

    :sswitch_d
    const-string v0, "BAD_REQUEST_FOR_PROVISION"

    goto :goto_0

    :sswitch_e
    const-string v0, "FAIL_TO_AUTHENTICATE"

    goto :goto_0

    :sswitch_f
    const-string v0, "INVALID_DEVICE_TOKEN_TO_REPROVISION"

    goto :goto_0

    :sswitch_10
    const-string v0, "PROVISION_EXCEPTION"

    goto :goto_0

    :sswitch_11
    const-string v0, "DUPLICATE_DEVICEID_TO_REPROVISION"

    goto :goto_0

    :sswitch_12
    const-string v0, "CONNECTION_MAX_EXCEEDED"

    goto :goto_0

    :sswitch_13
    const-string v0, "INVALID_STATE"

    goto :goto_0

    :sswitch_14
    const-string v0, "INVALID_DEVICE_TOKEN"

    goto :goto_0

    :sswitch_15
    const-string v0, "INVALID_APP_ID"

    goto :goto_0

    :sswitch_16
    const-string v0, "INVALID_REG_ID"

    goto :goto_0

    :sswitch_17
    const-string v0, "RESET_BY_NEW_INITIALIZATION"

    goto :goto_0

    :sswitch_18
    const-string v0, "REPROVISIONING_REQUIRED"

    goto :goto_0

    :sswitch_19
    const-string v0, "REGISTRATION_FAILED"

    goto :goto_0

    :sswitch_1a
    const-string v0, "DEREGISTRATION_FAILED"

    goto :goto_0

    :sswitch_1b
    const-string v0, "WRONG_DEVICE_TOKEN"

    goto :goto_0

    :sswitch_1c
    const-string v0, "WRONG_APP_ID"

    goto :goto_0

    :sswitch_1d
    const-string v0, "WRONG_REG_ID"

    goto :goto_0

    :sswitch_1e
    const-string v0, "UNSUPPORTED_PING_SPECIFICATION"

    goto :goto_0

    :cond_1
    const-string v0, "UNKNOWN CODE"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6b -> :sswitch_7
        -0x6a -> :sswitch_6
        -0x68 -> :sswitch_5
        -0x67 -> :sswitch_4
        -0x66 -> :sswitch_3
        -0x64 -> :sswitch_2
        -0x2 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x3e8 -> :sswitch_8
        0x7d0 -> :sswitch_9
        0x7d1 -> :sswitch_a
        0x7d2 -> :sswitch_b
        0x7d3 -> :sswitch_c
        0xbb8 -> :sswitch_d
        0xbb9 -> :sswitch_e
        0xbba -> :sswitch_f
        0xbbb -> :sswitch_10
        0xbbc -> :sswitch_11
        0xfa0 -> :sswitch_12
        0xfa1 -> :sswitch_13
        0xfa2 -> :sswitch_14
        0xfa3 -> :sswitch_15
        0xfa4 -> :sswitch_16
        0xfa5 -> :sswitch_17
        0xfa6 -> :sswitch_18
        0xfa7 -> :sswitch_19
        0xfa8 -> :sswitch_1a
        0xfa9 -> :sswitch_1b
        0xfaa -> :sswitch_1c
        0xfab -> :sswitch_1d
        0xfac -> :sswitch_1e
    .end sparse-switch
.end method

.method public static b(J)V
    .locals 0

    sput-wide p0, Lcom/sec/spp/push/c/b;->c:J

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, Lcom/sec/spp/push/util/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[New DeviceId] "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v1, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v2, "[New DeviceId] empty. "

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->v()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .locals 2

    const-class v0, Lcom/sec/spp/push/c/b;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/sec/spp/push/c/b;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic c(I)V
    .locals 0

    sput p0, Lcom/sec/spp/push/c/b;->b:I

    return-void
.end method

.method private static c(J)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.ACTION_RECONNECT_ALARM_SET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.EXTRA_SET_TIME"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static d()J
    .locals 2

    sget-wide v0, Lcom/sec/spp/push/c/b;->c:J

    return-wide v0
.end method

.method private d(I)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/d/a/b;->a(I)V

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/spp/push/c/b;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "stopService. context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    move-result v1

    sget-object v2, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "stopService. isStopped="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.ServiceAbnormallyStoppedAction"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v3, "sendBroadcast : SERVER_ABNORMALLY_STOPPED_ACTION"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-lt v2, v3, :cond_1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_1
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    sget v0, Lcom/sec/spp/push/c/b;->b:I

    return v0
.end method

.method private g()V
    .locals 6

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "reInitPushServerConnection. context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->b()V

    invoke-static {}, Lcom/sec/spp/push/c/b;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/spp/push/c/b;->b(J)V

    :cond_1
    const-class v1, Lcom/sec/spp/push/c/b;

    monitor-enter v1

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-wide v4, Lcom/sec/spp/push/c/b;->c:J

    sub-long/2addr v2, v4

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[ReInit] Retry Count : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v5, Lcom/sec/spp/push/c/b;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying re-Init for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/sec/spp/push/c/b;->b:I

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->h()I

    move-result v4

    if-lt v0, v4, :cond_2

    const-wide/32 v4, 0x35b60

    cmp-long v0, v4, v2

    if-lez v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/spp/push/c/b;->a(J)V

    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->h()Lcom/sec/spp/push/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b;->f()V

    :cond_4
    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->k()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method private h()I
    .locals 2

    const/4 v0, 0x4

    invoke-static {}, Lcom/sec/spp/push/util/k;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    return v0
.end method

.method private i()J
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "FOTA_ONLY_STATE"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/32 v0, 0xa4cb80

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x2710

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    const-class v2, Lcom/sec/spp/push/c/b;

    monitor-enter v2

    :try_start_0
    sget v3, Lcom/sec/spp/push/c/b;->b:I

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->j()J

    move-result-wide v0

    :cond_2
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    sget v3, Lcom/sec/spp/push/c/b;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x1

    if-lt v3, v4, :cond_2

    const-wide/32 v0, 0x493e0

    goto :goto_1
.end method

.method private j()J
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x41124f8000000000L    # 300000.0

    mul-double/2addr v0, v2

    const-wide v2, 0x40c3880000000000L    # 10000.0

    add-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method private k()V
    .locals 8

    const-string v0, "provisionAlarm"

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/g;->c(Z)V

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->i()J

    move-result-wide v6

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v0

    const-string v2, "provisionAlarm"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    add-long/2addr v3, v6

    new-instance v5, Lcom/sec/spp/push/c/d;

    invoke-direct {v5, p0}, Lcom/sec/spp/push/c/d;-><init>(Lcom/sec/spp/push/c/b;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/util/a;)V

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ReInit] Set Provisioning Alarm : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_0

    invoke-static {v6, v7}, Lcom/sec/spp/push/c/b;->c(J)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "redoProvisioning()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "reset the Push connection Data"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->l()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "Disconnect from Server"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/f; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "redoProvisioning. context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "redoProvisioning PushConnection disconnect:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/c/f;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/g/a;->a()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->h()V

    goto :goto_1
.end method

.method private m()V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->c(Z)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/spp/push/c/b;->a(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/sec/spp/push/c/b;->b(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->l()V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 4

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleError->resultCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/c/b;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleError. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->g()V

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->m()V

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->g()V

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/c/b;->a:Ljava/lang/String;

    const-string v1, "handleError. context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/g/a;->a()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->l()V

    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->g()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->l()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0, p1}, Lcom/sec/spp/push/c/b;->d(I)V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/sec/spp/push/c/b;->m()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x68 -> :sswitch_2
        -0x67 -> :sswitch_4
        -0x1 -> :sswitch_2
        0x7d0 -> :sswitch_0
        0x7d1 -> :sswitch_0
        0x7d2 -> :sswitch_1
        0x7d3 -> :sswitch_2
        0xbb8 -> :sswitch_5
        0xbb9 -> :sswitch_5
        0xbba -> :sswitch_4
        0xbbc -> :sswitch_6
        0xfa0 -> :sswitch_3
        0xfa1 -> :sswitch_2
        0xfa2 -> :sswitch_3
        0xfa5 -> :sswitch_1
        0xfa6 -> :sswitch_3
        0xfa7 -> :sswitch_1
        0xfa8 -> :sswitch_1
        0xfa9 -> :sswitch_3
    .end sparse-switch
.end method
