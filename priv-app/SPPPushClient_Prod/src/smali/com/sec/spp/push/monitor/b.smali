.class Lcom/sec/spp/push/monitor/b;
.super Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/spp/push/monitor/SystemStateMonitorService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v1, Lcom/sec/spp/push/monitor/c;->b:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/monitor/c;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/monitor/c;->b:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v0}, Lcom/sec/spp/push/monitor/c;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/monitor/b;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v1, Lcom/sec/spp/push/monitor/c;->a:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v1}, Lcom/sec/spp/push/monitor/c;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/monitor/c;->a:Lcom/sec/spp/push/monitor/c;

    invoke-virtual {v0}, Lcom/sec/spp/push/monitor/c;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/monitor/b;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
