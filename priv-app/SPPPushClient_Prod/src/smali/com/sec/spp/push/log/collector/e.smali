.class Lcom/sec/spp/push/log/collector/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/k;->b(J)V

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-static {v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)Landroid/content/ServiceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-static {v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "IllegalArgumentException. already dlc unbinded"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-static {p2}, Lcom/sec/spp/push/dlc/a/b;->a(Landroid/os/IBinder;)Lcom/sec/spp/push/dlc/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;Lcom/sec/spp/push/dlc/a/a;)V

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "DLC is connected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-static {v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-static {v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/e;->a()V

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/e;->b()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/e;->a:Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;Lcom/sec/spp/push/dlc/a/a;)V

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "DLC is disconnected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
