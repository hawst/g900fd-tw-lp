.class public Lcom/sec/spp/push/util/c;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/sec/spp/push/util/c;


# instance fields
.field private c:Lcom/sec/spp/push/util/d;

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J

.field private j:J

.field private k:Z

.field private final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[ConnectionState]"

    sput-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    iput-object v0, p0, Lcom/sec/spp/push/util/c;->c:Lcom/sec/spp/push/util/d;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/util/c;->l:Ljava/lang/Object;

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->d:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->e:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->f:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->g:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->h:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->i:J

    iput-wide v1, p0, Lcom/sec/spp/push/util/c;->j:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/spp/push/util/c;->k:Z

    invoke-direct {p0}, Lcom/sec/spp/push/util/c;->j()V

    return-void
.end method

.method public static declared-synchronized a()Lcom/sec/spp/push/util/c;
    .locals 2

    const-class v1, Lcom/sec/spp/push/util/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/util/c;->b:Lcom/sec/spp/push/util/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/util/c;

    invoke-direct {v0}, Lcom/sec/spp/push/util/c;-><init>()V

    sput-object v0, Lcom/sec/spp/push/util/c;->b:Lcom/sec/spp/push/util/c;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/util/c;->b:Lcom/sec/spp/push/util/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/spp/push/util/c;->k:Z

    return-void
.end method

.method public static declared-synchronized b()V
    .locals 2

    const-class v0, Lcom/sec/spp/push/util/c;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sec/spp/push/util/c;->b:Lcom/sec/spp/push/util/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private j()V
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/util/c;->a(Lcom/sec/spp/push/util/d;)V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/util/c;->a(Lcom/sec/spp/push/util/d;)V

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 7

    iget-object v1, p0, Lcom/sec/spp/push/util/c;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/spp/push/util/c;->c()V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-string v0, "alarm"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.spp.push.ACTION_CONNECTION_STATE_CHECK"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v4, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CT : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", NT : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/spp/push/util/c;->a(Z)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/sec/spp/push/util/d;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/util/c;->c:Lcom/sec/spp/push/util/d;

    return-void
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->d:J

    return-void
.end method

.method public c()V
    .locals 6

    iget-object v1, p0, Lcom/sec/spp/push/util/c;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-string v0, "alarm"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.spp.push.ACTION_CONNECTION_STATE_CHECK"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v4, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    const-string v4, "Connection Status Alarm Unscheduled"

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/util/c;->a(Z)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->e:J

    return-void
.end method

.method public d(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->g:J

    return-void
.end method

.method public d()Z
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Last Ping Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->i:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Next Ping Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->j:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Last Init Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->h:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Last Init Start Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->g:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Last Prov Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->f:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Last Prov Start Time] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/sec/spp/push/util/c;->e:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "[SPP Service Started] "

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/PushClientApplication;

    invoke-virtual {v0}, Lcom/sec/spp/push/PushClientApplication;->a()Z

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/sec/spp/push/util/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    const-string v1, "[Abnormal State] "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_0

    :cond_1
    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->h:J

    iget-wide v7, p0, Lcom/sec/spp/push/util/c;->g:J

    cmp-long v0, v5, v7

    if-gez v0, :cond_2

    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->g:J

    invoke-virtual {p0}, Lcom/sec/spp/push/util/c;->i()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    const-string v3, "[Abnormal State] "

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    move v0, v2

    goto/16 :goto_0

    :cond_2
    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->f:J

    iget-wide v7, p0, Lcom/sec/spp/push/util/c;->e:J

    cmp-long v0, v5, v7

    if-gez v0, :cond_3

    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->e:J

    invoke-virtual {p0}, Lcom/sec/spp/push/util/c;->i()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    const-string v3, "[Abnormal State] "

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    move v0, v2

    goto/16 :goto_0

    :cond_3
    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->j:J

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-eqz v0, :cond_4

    iget-wide v5, p0, Lcom/sec/spp/push/util/c;->j:J

    const-wide/32 v7, 0x493e0

    add-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "[Abnormal State] Ping Alarm isn\'t woke-up [Current Time] "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", [Ping Scheduled Time] "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/sec/spp/push/util/c;->j:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->l()V

    move v0, v2

    goto/16 :goto_0

    :cond_4
    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->isConnected()Z

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v2

    sget-object v3, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Channel : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Flag : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_5

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/a/g;->b(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    const-string v2, "Channel Connected : Not Available"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public e(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->i:J

    return-void
.end method

.method public e()Z
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->e:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->i:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/util/c;->d:J

    return-wide v0
.end method

.method public f(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->f:J

    return-void
.end method

.method public g()Lcom/sec/spp/push/util/d;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/util/c;->c:Lcom/sec/spp/push/util/d;

    return-object v0
.end method

.method public g(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->h:J

    return-void
.end method

.method public h()I
    .locals 4

    const v0, 0x3a980

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/g/a;->h(Landroid/content/Context;)I

    move-result v1

    const v2, 0xea60

    mul-int/2addr v1, v2

    if-ge v1, v0, :cond_0

    :goto_0
    sget-object v1, Lcom/sec/spp/push/util/c;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Check Interval = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public h(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/util/c;->j:J

    return-void
.end method

.method public i()I
    .locals 2

    const v0, 0x3a980

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/h/c;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/util/g;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const v0, 0x36ee80

    :cond_1
    return v0
.end method
