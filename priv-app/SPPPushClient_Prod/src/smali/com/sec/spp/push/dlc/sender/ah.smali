.class Lcom/sec/spp/push/dlc/sender/ah;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/u;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "UpdateRegiDBSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->a(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->T:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1, v2, v3}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;)V

    return-void
.end method

.method public b(Landroid/os/Message;)V
    .locals 6

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ag:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-nez v3, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/sec/spp/push/dlc/sender/s;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/sec/spp/push/dlc/sender/s;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "https://ew1.dms-gld.bigdata.ssp.samsung.com:80/1.0/getAppQuotaList"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/s;->a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/spp/push/dlc/sender/RegiReply;

    invoke-direct {v1}, Lcom/sec/spp/push/dlc/sender/RegiReply;-><init>()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/dlc/sender/RegiReply;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/RegiReply;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v0, "processMessage. Invalid RegiReply"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/RegiReply;->a()I

    move-result v0

    sget v1, Lcom/sec/spp/push/dlc/sender/RegiReply;->RESULT_GET_QUOTA_SUCCESS:I

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->V:Lcom/sec/spp/push/dlc/sender/i;

    new-instance v5, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v5, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    goto :goto_0

    :cond_5
    const-string v0, "regiDB update fail"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_7

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_7
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_8

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_8
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ai:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->arg1:I

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    if-ne v0, v1, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUCCESS : update regiDB. qv["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->B(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SUCCESS : update regiDB. qv["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->B(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ah;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_9
    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/q;->a(I)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v1

    if-nez v1, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FAIL : update regiDB -"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FAIL : update regiDB -"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/q;->a(I)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/q;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
