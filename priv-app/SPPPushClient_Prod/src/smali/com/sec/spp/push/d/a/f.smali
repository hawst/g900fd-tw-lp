.class Lcom/sec/spp/push/d/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/d/a/a;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/d/a/b;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;

.field private final synthetic e:Lcom/sec/spp/push/util/h;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/d/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/d/a/f;->a:Lcom/sec/spp/push/d/a/b;

    iput-object p2, p0, Lcom/sec/spp/push/d/a/f;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/spp/push/d/a/f;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/spp/push/d/a/f;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/spp/push/d/a/f;->e:Lcom/sec/spp/push/util/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReq - onRequestExecute()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/f;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/f;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/f;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/spp/push/d/a/f;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/spp/push/d/a/f;->e:Lcom/sec/spp/push/util/h;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/d/a/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V

    return-void
.end method

.method public a(I)V
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendDeregistrationReq - onRequestCancel()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/f;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/f;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/f;->e:Lcom/sec/spp/push/util/h;

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;Lcom/sec/spp/push/util/h;I)V

    return-void
.end method

.method public b()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/f;->b:Ljava/lang/String;

    return-object v0
.end method
