.class public Lcom/sec/spp/push/receiver/LdUpdateReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v1, "doReConnectionInit"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->h()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v2, "ConnectionException."

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/c/a;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v3, "needReConnectionInit."

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    sget-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "data is empty. data1 : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", data2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/b/a;->a()Lcom/sec/spp/push/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->c()Lcom/sec/spp/push/a/a;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v4, Landroid/location/Location;

    const-string v5, "old"

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v5

    invoke-virtual {v3}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v5, v6, v7}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/a/a;->e(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-virtual {v3}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-virtual {v4, v5, v6}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v4, v7, v8}, Landroid/location/Location;->setLongitude(D)V

    new-instance v5, Landroid/location/Location;

    const-string v6, "new"

    invoke-direct {v5, v6}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v6, p1, v7}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/spp/push/a/a;->e(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v3, p3}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v5, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v5, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v4, v5}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x408f400000000000L    # 1000.0

    cmpl-double v3, v3, v5

    if-lez v3, :cond_4

    sget-object v3, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v4, "distance is longer than re connection init distance."

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->b()V

    goto/16 :goto_0

    :cond_3
    :try_start_1
    sget-object v3, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v4, "first lData. re connection."

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->b()V

    goto/16 :goto_0

    :cond_4
    :try_start_2
    sget-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v3, "not need re connection."

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->b()V

    :cond_5
    move v0, v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v3, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->b()V

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/sec/spp/push/b/a;->b()V

    :cond_7
    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.sec.spp.push.ldUpdate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "update"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v2, Lcom/sec/spp/push/a/a;

    invoke-direct {v2}, Lcom/sec/spp/push/a/a;-><init>()V

    const-string v1, "package"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/a/a;->a(Ljava/lang/String;)V

    const-string v1, "data1"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/a/a;->b(Ljava/lang/String;)V

    const-string v1, "data2"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/a/a;->c(Ljava/lang/String;)V

    const-string v1, "data3"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/a/a;->d(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isUpdate : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ecData1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ecData2 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ecData3 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v1, "pkg is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/b/a;->a()Lcom/sec/spp/push/b/a;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/b/a;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/b/a;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/a;->a()Lcom/sec/spp/push/b/a;

    move-result-object v1

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/a;->b(Ljava/lang/String;)Lcom/sec/spp/push/a/a;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    const-string v2, "Same Data"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/b/a;->a(Lcom/sec/spp/push/a/a;Ljava/lang/String;)V

    :goto_1
    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    goto/16 :goto_0

    :cond_5
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/b/a;->b(Lcom/sec/spp/push/a/a;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_4
    sget-object v2, Lcom/sec/spp/push/receiver/LdUpdateReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    :cond_6
    throw v0
.end method
