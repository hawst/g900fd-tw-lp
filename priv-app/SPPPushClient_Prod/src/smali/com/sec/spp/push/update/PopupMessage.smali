.class public Lcom/sec/spp/push/update/PopupMessage;
.super Landroid/app/Activity;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/update/PopupMessage;->a:Ljava/lang/String;

    return-void
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/j;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/j;-><init>(Lcom/sec/spp/push/update/PopupMessage;)V

    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/update/PopupMessage;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/update/PopupMessage;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/k;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/k;-><init>(Lcom/sec/spp/push/update/PopupMessage;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/PopupMessage;->requestWindowFeature(I)Z

    const v0, 0x7f030018

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/PopupMessage;->setContentView(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v1, 0x3f333333    # 0.7f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual {p0}, Lcom/sec/spp/push/update/PopupMessage;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/update/PopupMessage;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f090069

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/PopupMessage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f090068

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/update/PopupMessage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/PopupMessage;->a()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/sec/spp/push/update/PopupMessage;->b()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/PopupMessage;->a:Ljava/lang/String;

    const-string v1, "Display Popup Message."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
