.class Lcom/sec/spp/push/e/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/jboss/netty/channel/ChannelFutureListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/e/a/a;

.field private final synthetic b:Lcom/google/protobuf/MessageLite;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/e/a/a;Lcom/google/protobuf/MessageLite;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/e/a/c;->a:Lcom/sec/spp/push/e/a/a;

    iput-object p2, p0, Lcom/sec/spp/push/e/a/c;->b:Lcom/google/protobuf/MessageLite;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public operationComplete(Lorg/jboss/netty/channel/ChannelFuture;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/spp/push/e/a/c;->b:Lcom/google/protobuf/MessageLite;

    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->getMessageType(Ljava/lang/Object;)B

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/e/a/a;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Write] Complete! msgType : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->h:Lcom/sec/spp/push/log/collector/d;

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IJ)V

    :cond_0
    return-void
.end method
