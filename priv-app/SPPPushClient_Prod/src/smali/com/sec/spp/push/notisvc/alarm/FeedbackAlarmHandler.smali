.class public Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;
.super Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/d;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/d;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;-><init>()V

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FeedbackAlarmCallBack. Feedback Alarm Received. mid:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "onReceive. ctx is null. do nothing"

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "onReceive. mid is null. do nothing"

    sget-object v1, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/notisvc/c/c;->a()Lcom/sec/spp/push/notisvc/c/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/spp/push/notisvc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mid:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/alarm/FeedbackAlarmHandler;->mid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
