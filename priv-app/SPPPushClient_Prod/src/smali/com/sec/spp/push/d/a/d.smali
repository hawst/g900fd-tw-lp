.class Lcom/sec/spp/push/d/a/d;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/d/a/b;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/d/a/b;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a()V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[SendNotiAck] WHAT_NOTIACK_NOT_REACHED"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/i;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget v2, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v2

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    iget-object v1, v2, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[SendNotiAck] removing messages asyncId= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget v4, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SendNotiAck] Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_3
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SendNotiAck] listener is null. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private a(Landroid/os/Message;)V
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SharedConstants.WHAT_INTERNAL_ERROR_PENDING_TASK. message.arg1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/spp/push/d/a/h;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SharedConstants.WHAT_INTERNAL_ERROR_PENDING_TASK. listenerinfo !null"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, v0, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_INTERNAL_ERROR_PENDING_TASK. listener is null so  not calling onFail."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[SendNotiAck] SharedConstants.WHAT_NOTIACK_REACHED"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/i;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget v2, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[SendNotiAck] Remove asyncId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget v3, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[SendNotiAck] listenerinfo !null"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/spp/push/util/j;

    invoke-direct {v2}, Lcom/sec/spp/push/util/j;-><init>()V

    const/16 v3, 0x3e8

    iget-object v0, v0, Lcom/sec/spp/push/d/a/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/sec/spp/push/util/j;->b(ILjava/lang/String;)V

    iget-object v0, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v0, v2}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SendNotiAck] Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :try_start_3
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SendNotiAck] listener is null. asyncId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/sec/spp/push/d/a/i;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private b(Landroid/os/Message;)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_INTERNAL_ERROR"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SharedConstants.WHAT_INTERNAL_ERROR Remove asyncId= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v2, "SharedConstants.WHAT_INTERNAL_ERROR. listenerinfo !null"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget-object v1, v1, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->getMessageType(Ljava/lang/Object;)B

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SharedConstants.WHAT_INTERNAL_ERROR. listener is null so  not calling onFail. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_INTERNAL_ERROR: IndexOutOfBoundsException - notiAcksUniqueIds size is 0"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private c()V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_PROV_NETWORK_NOT_AVAILABLE"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[PROV] Remove asyncId= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v3}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v2}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/d/a/b;->b(I)V

    iget-object v1, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v0, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    const/4 v1, -0x2

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_PROV_NETWORK_NOT_AVAILABLE. ignored"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Landroid/os/Message;)V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_TIMEOUT"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TIMEOUT] removing messages asyncId= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v0, v1, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    const/4 v2, -0x1

    iget-object v1, v1, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[TIMEOUT] listener : null. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[TIMEOUT] listenerinfo : null. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SharedConstants.WHAT_PUSH_NETWORK_NOT_AVAILABLE"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v2}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/util/ArrayList;I)Lcom/sec/spp/push/d/a/h;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v1}, Lcom/sec/spp/push/d/a/b;->i()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[PushNetlistenerList is empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/h;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "appId:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v2, :cond_2

    :try_start_0
    iget-object v2, v0, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    const/4 v3, -0x2

    iget-object v0, v0, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/sec/spp/push/util/h;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v2, "listener == mull"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v2, "listenerInfo == mull"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0}, Lcom/sec/pns/msg/frontend/MsgFrontendCommon;->getMessageType(Ljava/lang/Object;)B

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__ProvisionReply__"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->j(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__InitReply__"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->i(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->h(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->g(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->f(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->e(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 4

    const-string v0, "com.sec.chaton"

    const-string v0, "com.sec.chaton.receiver.PushReceiver"

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.chaton"

    const-string v3, "com.sec.chaton.receiver.PushReceiver"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "com.sec.spp.WifiPortChangeNotiAction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[WIFI State] sendWifiChangeNoti to Chat"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private e(Landroid/os/Message;)V
    .locals 16

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "__NotiGroup__"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;->getNotiElementsList()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;->getNotiElementsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "notiGroup.getNotiElementsList() is empty. break"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->n()V

    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;->getNotiElementsList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiGroup;->getNotiElementsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/sec/spp/push/f/c;->a()Lcom/sec/spp/push/f/c;

    move-result-object v1

    invoke-virtual {v1, v14}, Lcom/sec/spp/push/f/c;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;

    new-instance v1, Lcom/sec/spp/push/util/l;

    invoke-direct {v1}, Lcom/sec/spp/push/util/l;-><init>()V

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getMumId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getNotiId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getReliableLevel()I

    move-result v4

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getType()I

    move-result v5

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getAppData()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getTimeStamp()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getConnectionTerm()I

    move-result v8

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getSessionInfo()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getSender()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getAppId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiElement;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v1 .. v13}, Lcom/sec/spp/push/util/l;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/Long;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private f()V
    .locals 7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/sec/spp/push/receiver/ProviderInfoReceiver;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.sec.spp.push.ACTION_WIFI_INFO"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "wifi_addr"

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b/g;->h()Lcom/sec/spp/push/e/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "wifi_port"

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b/g;->h()Lcom/sec/spp/push/e/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/b;->b()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->l()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendWifiInfoToSubUsers : Empty User"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-string v5, "user"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v0, v3, v4}, Landroid/os/UserManager;->getUserForSerialNumber(J)Landroid/os/UserHandle;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sendWifiInfo To User : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;J)V

    goto :goto_1
.end method

.method private f(Landroid/os/Message;)V
    .locals 7

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->i:Lcom/sec/spp/push/log/collector/d;

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IJ)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__PingReply__"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;->getAsyncId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "__PingReply__ removing messages asyncId= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v3, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v3, "__PingReply__ listenerinfo !null"

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/spp/push/util/m;

    invoke-direct {v1}, Lcom/sec/spp/push/util/m;-><init>()V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "__PingReply__  ------pingReply.getCreatedTime() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;->getCreatedTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "__PingReply__  ------pingReply.getDelta() "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;->getDelta()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;->getCreatedTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingReply;->getDelta()I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/sec/spp/push/util/m;->a(Ljava/lang/Long;I)V

    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v0, v1}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "__PingReply__ listener is null so  not calling onSuccess. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private g(Landroid/os/Message;)V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__DeregistrationReply__"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;->getAsyncId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "__DeregistrationReply__ removing messages asyncId= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v3, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    const-string v3, "__DeregistrationReply__ listenerinfo !null"

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/spp/push/util/e;

    invoke-direct {v1}, Lcom/sec/spp/push/util/e;-><init>()V

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;->getResultCode()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;->getResultMsg()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationReply;->getUserData()Ljava/lang/String;

    move-result-object v0

    iget-object v5, v2, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v0, v5}, Lcom/sec/spp/push/util/e;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v0, v1}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->n()V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "__DeregistrationReply__ listener is null so  not calling onSuccess. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private h(Landroid/os/Message;)V
    .locals 7

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__RegistrationReply__"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;->getAsyncId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v6

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[RegReply] removing messages asyncId= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v6, :cond_1

    new-instance v0, Lcom/sec/spp/push/util/p;

    invoke-direct {v0}, Lcom/sec/spp/push/util/p;-><init>()V

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;->getResultCode()I

    move-result v1

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;->getResultMsg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;->getRegId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationReply;->getUserData()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v6, Lcom/sec/spp/push/d/a/h;->b:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/util/p;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v6, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v6, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v1, v0}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->n()V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[RegReply] listener is null so  not calling onSuccess. asyncId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private i(Landroid/os/Message;)V
    .locals 6

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getAsyncId()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[InitReply] removing asyncId= "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v3, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    new-instance v3, Lcom/sec/spp/push/util/i;

    invoke-direct {v3}, Lcom/sec/spp/push/util/i;-><init>()V

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultCode()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitReply;->getResultMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/sec/spp/push/util/i;->a(ILjava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b/g;->h()Lcom/sec/spp/push/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/b;->g()V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->e()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v0, v4, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->f()V

    :cond_0
    if-eqz v2, :cond_2

    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, v2, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v0, v3}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[InitReply]"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[InitReply] listener is null. asyncId: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/e/a/g;->a(Lcom/sec/spp/push/util/j;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[InitReply] listenerinfo is null. asyncId: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/e/a/g;->a(Lcom/sec/spp/push/util/j;)V

    goto :goto_0
.end method

.method private j(Landroid/os/Message;)V
    .locals 12

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v10, v0

    check-cast v10, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v11

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ProvReply] removing asyncId= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v2}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v1}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->b(I)V

    if-eqz v11, :cond_1

    new-instance v0, Lcom/sec/spp/push/util/n;

    invoke-direct {v0}, Lcom/sec/spp/push/util/n;-><init>()V

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getResultCode()I

    move-result v1

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getDeviceToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getPrimaryIp()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getPrimaryPort()I

    move-result v4

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getSecondaryIp()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getSecondaryPort()I

    move-result v6

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getPingInterval()I

    move-result v7

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getUserData()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getRegionId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionReply;->getRegionDomainName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, Lcom/sec/spp/push/util/n;->a(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/update/a;->a()Lcom/sec/spp/push/update/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/update/a;->a(Lcom/sec/spp/push/util/n;)V

    iget-object v1, v11, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v11, Lcom/sec/spp/push/d/a/h;->a:Lcom/sec/spp/push/util/h;

    invoke-interface {v1, v0}, Lcom/sec/spp/push/util/h;->a(Lcom/sec/spp/push/util/j;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[ProvReply] listener is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ProvReply] listener is null so not calling onSuccess. asyncId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v2}, Lcom/sec/spp/push/d/a/b;->d(Lcom/sec/spp/push/d/a/b;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/d;->a:Lcom/sec/spp/push/d/a/b;

    invoke-static {v0}, Lcom/sec/spp/push/d/a/b;->b(Lcom/sec/spp/push/d/a/b;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/PushClientApplication;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/spp/push/PushClientApplication;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service is destroyed"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->d(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->c(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->d()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->c()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->b()V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/d;->a()V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->b(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/d;->a(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
