.class public final enum Lcom/sec/spp/push/notisvc/card/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/card/d;

.field public static final enum b:Lcom/sec/spp/push/notisvc/card/d;

.field public static final enum c:Lcom/sec/spp/push/notisvc/card/d;

.field public static final enum d:Lcom/sec/spp/push/notisvc/card/d;

.field public static final enum e:Lcom/sec/spp/push/notisvc/card/d;

.field public static final enum f:Lcom/sec/spp/push/notisvc/card/d;

.field private static final g:I

.field private static h:[Ljava/lang/String;

.field private static final synthetic i:[Lcom/sec/spp/push/notisvc/card/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "jpg"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->a:Lcom/sec/spp/push/notisvc/card/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "png"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->b:Lcom/sec/spp/push/notisvc/card/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "bmp"

    invoke-direct {v0, v1, v5}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->c:Lcom/sec/spp/push/notisvc/card/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "JPG"

    invoke-direct {v0, v1, v6}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->d:Lcom/sec/spp/push/notisvc/card/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "PNG"

    invoke-direct {v0, v1, v7}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->e:Lcom/sec/spp/push/notisvc/card/d;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/d;

    const-string v1, "BMP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/notisvc/card/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->f:Lcom/sec/spp/push/notisvc/card/d;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/card/d;

    sget-object v1, Lcom/sec/spp/push/notisvc/card/d;->a:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/card/d;->b:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/notisvc/card/d;->c:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/notisvc/card/d;->d:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/notisvc/card/d;->e:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/notisvc/card/d;->f:Lcom/sec/spp/push/notisvc/card/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->i:[Lcom/sec/spp/push/notisvc/card/d;

    const-class v0, Lcom/sec/spp/push/notisvc/card/d;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    sput v0, Lcom/sec/spp/push/notisvc/card/d;->g:I

    sget v0, Lcom/sec/spp/push/notisvc/card/d;->g:I

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/sec/spp/push/notisvc/card/d;->h:[Ljava/lang/String;

    const-class v0, Lcom/sec/spp/push/notisvc/card/d;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/d;

    sget-object v2, Lcom/sec/spp/push/notisvc/card/d;->h:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/d;->ordinal()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/card/d;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/card/d;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/card/d;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/card/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/card/d;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/card/d;->i:[Lcom/sec/spp/push/notisvc/card/d;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/card/d;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
