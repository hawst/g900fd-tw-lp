.class Lcom/sec/spp/push/e/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/spp/push/util/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/e/a/d;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/e/a/d;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/util/g;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/k;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[PROV] curPlmn:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", savedPlmn:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/k;->b(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->o()V

    :cond_0
    return-void
.end method

.method private a(IIII)V
    .locals 2

    invoke-static {p1, p2, p3, p4}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(IIII)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->h()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[PROV] Wrong Ping value"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private c()Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/spp/push/g/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_2
    invoke-static {v2, v1}, Lcom/sec/spp/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v3, v1}, Lcom/sec/spp/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private d()V
    .locals 8

    const-string v0, "com.sec.chaton"

    const-string v0, "com.sec.chaton.receiver.PushUpdateVersionReceiver"

    invoke-static {}, Lcom/sec/spp/push/g/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1, v0}, Lcom/sec/spp/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/g;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v1

    :cond_2
    invoke-static {v2}, Lcom/sec/spp/push/util/g;->a(Z)Z

    move-result v3

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.sec.chaton"

    const-string v7, "com.sec.chaton.receiver.PushUpdateVersionReceiver"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v5, "com.sec.spp.push.VersionCheck"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "current_version"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "latest_version"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "need_update"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "force_update"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sendUpdateNoti to ChatON"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/k;->c(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/sec/spp/push/util/b;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 5

    const/16 v4, -0x67

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[PROV] Fail : errorCode= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eq p1, v4, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    :cond_0
    const-string v0, "460"

    invoke-static {}, Lcom/sec/spp/push/util/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    invoke-static {v0}, Lcom/sec/spp/push/e/a/d;->a(Lcom/sec/spp/push/e/a/d;)V

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->u()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    invoke-static {}, Lcom/sec/spp/push/g/a;->d()V

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/d/a/b;->a(I)V

    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/c/b;->a(I)V

    :goto_0
    return-void

    :cond_3
    if-ne p1, v3, :cond_4

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/d/a/b;->a(I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0xbbd

    if-ne p1, v0, :cond_5

    invoke-static {}, Lcom/sec/spp/push/g/a;->a()V

    invoke-static {}, Lcom/sec/spp/push/g/a;->d()V

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->l()V

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->h()V

    goto :goto_0

    :cond_5
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendProvisioningReq. Provisioning onFail() Not Handled with errorCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 15

    move-object/from16 v0, p1

    check-cast v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/n;->k()I

    move-result v8

    move-object/from16 v0, p1

    check-cast v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/n;->l()I

    move-result v9

    move-object/from16 v0, p1

    check-cast v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/n;->m()I

    move-result v10

    move-object/from16 v0, p1

    check-cast v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/n;->n()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_3

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/util/c;->f(J)V

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/util/d;->a:Lcom/sec/spp/push/util/d;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/util/c;->h()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/util/c;->a(J)V

    :cond_0
    move-object/from16 v0, p1

    check-cast v0, Lcom/sec/spp/push/util/n;

    invoke-virtual {v0}, Lcom/sec/spp/push/util/n;->a()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p1

    check-cast v1, Lcom/sec/spp/push/util/n;

    invoke-virtual {v1}, Lcom/sec/spp/push/util/n;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p1

    check-cast v2, Lcom/sec/spp/push/util/n;

    invoke-virtual {v2}, Lcom/sec/spp/push/util/n;->e()I

    move-result v2

    move-object/from16 v3, p1

    check-cast v3, Lcom/sec/spp/push/util/n;

    invoke-virtual {v3}, Lcom/sec/spp/push/util/n;->f()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p1

    check-cast v4, Lcom/sec/spp/push/util/n;

    invoke-virtual {v4}, Lcom/sec/spp/push/util/n;->g()I

    move-result v4

    move-object/from16 v5, p1

    check-cast v5, Lcom/sec/spp/push/util/n;

    invoke-virtual {v5}, Lcom/sec/spp/push/util/n;->h()I

    move-result v5

    move-object/from16 v6, p1

    check-cast v6, Lcom/sec/spp/push/util/n;

    invoke-virtual {v6}, Lcom/sec/spp/push/util/n;->i()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v7, p1

    check-cast v7, Lcom/sec/spp/push/util/n;

    invoke-virtual {v7}, Lcom/sec/spp/push/util/n;->j()I

    move-result v7

    move-object/from16 v12, p1

    check-cast v12, Lcom/sec/spp/push/util/n;

    invoke-virtual {v12}, Lcom/sec/spp/push/util/n;->o()I

    move-result v12

    move-object/from16 v13, p1

    check-cast v13, Lcom/sec/spp/push/util/n;

    invoke-virtual {v13}, Lcom/sec/spp/push/util/n;->r()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v14, p1

    check-cast v14, Lcom/sec/spp/push/util/n;

    invoke-virtual {v14}, Lcom/sec/spp/push/util/n;->s()Ljava/lang/String;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/sec/spp/push/g/a;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILjava/lang/String;IIIIIILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    check-cast p1, Lcom/sec/spp/push/util/n;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/spp/push/util/n;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/util/b;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/f;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/f;->d()V

    :cond_1
    invoke-direct {p0, v8, v9, v10, v11}, Lcom/sec/spp/push/e/a/f;->a(IIII)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/f;->a()V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    const-string v1, "com.sec.spp.push.test.ACTION_PROVISION_RESPONSE"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/spp/push/e/a/d;->a(Lcom/sec/spp/push/e/a/d;Ljava/lang/String;Z)V

    :cond_2
    invoke-direct {p0}, Lcom/sec/spp/push/e/a/f;->b()V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->t()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[PROV] onSuccess : Error "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/spp/push/e/a/f;->a:Lcom/sec/spp/push/e/a/d;

    const-string v1, "com.sec.spp.push.test.ACTION_PROVISION_RESPONSE"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/spp/push/e/a/d;->a(Lcom/sec/spp/push/e/a/d;Ljava/lang/String;Z)V

    :cond_4
    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/c/b;->a(I)V

    goto :goto_0
.end method
