.class public Lcom/sec/spp/push/notisvc/card/b;
.super Lcom/sec/spp/push/notisvc/w;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/card/a;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/notisvc/card/a;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/w;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/sec/spp/push/notisvc/x;)V
    .locals 6

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onSuccess. invalid parameters. ctx:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onSuccess. resultCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/spp/push/notisvc/x;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onSuccess. dbHandler is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "incomp_display"

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/notisvc/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/spp/push/notisvc/a/a;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/notisvc/a/b;->a(Ljava/lang/String;I)Lcom/sec/spp/push/notisvc/a/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    invoke-static {}, Lcom/sec/spp/push/notisvc/c/c;->a()Lcom/sec/spp/push/notisvc/c/c;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/notisvc/c/b;->b:Lcom/sec/spp/push/notisvc/c/b;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/c/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/c/b;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/card/a;->d(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method public b(Landroid/content/Context;Lcom/sec/spp/push/notisvc/x;)V
    .locals 8

    const-wide/32 v5, 0x36ee80

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onFail. invalid parameters. ctx:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onFail. resultCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/spp/push/notisvc/x;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/spp/push/notisvc/x;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/notisvc/c/b;->l:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v2, v3, v0}, Lcom/sec/spp/push/notisvc/card/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/c/b;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_0
    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "card_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v5

    new-instance v5, Lcom/sec/spp/push/notisvc/alarm/CardAlarmHandler;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/sec/spp/push/notisvc/card/e;->a:Lcom/sec/spp/push/notisvc/card/e;

    invoke-virtual {v6}, Lcom/sec/spp/push/notisvc/card/e;->name()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v7}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v1, v6, v7}, Lcom/sec/spp/push/notisvc/alarm/CardAlarmHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    goto/16 :goto_0

    :sswitch_1
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onFail. dbHandler is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ResourceDownloadHandler"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/a/b;->h(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/notisvc/c/b;->l:Lcom/sec/spp/push/notisvc/c/b;

    const-string v4, "C1009"

    invoke-static {p1, v1, v2, v3, v4}, Lcom/sec/spp/push/notisvc/card/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/c/b;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/spp/push/notisvc/a/b;->a(Ljava/lang/String;I)Lcom/sec/spp/push/notisvc/a/a;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "card_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    add-long/2addr v3, v5

    new-instance v5, Lcom/sec/spp/push/notisvc/alarm/CardAlarmHandler;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/sec/spp/push/notisvc/card/e;->a:Lcom/sec/spp/push/notisvc/card/e;

    invoke-virtual {v6}, Lcom/sec/spp/push/notisvc/card/e;->name()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v7}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v1, v6, v7}, Lcom/sec/spp/push/notisvc/alarm/CardAlarmHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->q:Lcom/sec/spp/push/notisvc/c/b;

    const-string v3, "wrongarg_1008"

    invoke-static {p1, v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/card/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/c/b;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/card/a;->a(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/card/b;->a:Lcom/sec/spp/push/notisvc/card/a;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/card/a;->b(Lcom/sec/spp/push/notisvc/card/a;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/notisvc/c/b;->l:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "C"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v2, v3, v0}, Lcom/sec/spp/push/notisvc/card/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/c/b;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1f4 -> :sswitch_1
        0x1f6 -> :sswitch_1
        0x1f7 -> :sswitch_1
        0x1f8 -> :sswitch_1
        0x3e9 -> :sswitch_3
        0x3ea -> :sswitch_0
        0x3eb -> :sswitch_1
        0x3ec -> :sswitch_3
        0x3ed -> :sswitch_3
        0x3ee -> :sswitch_1
        0x3ef -> :sswitch_1
        0x3f0 -> :sswitch_2
    .end sparse-switch
.end method
