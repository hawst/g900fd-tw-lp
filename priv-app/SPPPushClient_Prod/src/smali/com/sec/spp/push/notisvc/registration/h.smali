.class public abstract Lcom/sec/spp/push/notisvc/registration/h;
.super Ljava/lang/Object;


# static fields
.field protected static a:I

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    const/16 v0, -0x3e8

    sput v0, Lcom/sec/spp/push/notisvc/registration/h;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/d/b;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static h(Landroid/content/Context;)V
    .locals 3

    const-string v0, "broadcastDeviceReg."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_0

    const-string v0, "Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.BIGJOE_REGI"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "agreement"

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v2}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)V
    .locals 2

    const-string v0, "setAlarmAfterBoot."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p0, :cond_0

    const-string v0, "context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/f;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/c;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/a;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static j(Landroid/content/Context;)V
    .locals 2

    const-string v0, "doDelayedRequest."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "regId is still empty."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/f;->b(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/c;->b(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/sec/spp/push/notisvc/registration/a;->b(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method abstract a(Z)V
.end method

.method abstract a()Z
.end method

.method abstract b()Ljava/lang/String;
.end method

.method abstract c(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
.end method

.method abstract d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
.end method

.method abstract e(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;
.end method

.method abstract f(Landroid/content/Context;)Ljava/lang/String;
.end method

.method abstract g(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/agent/h;
.end method

.method public k(Landroid/content/Context;)V
    .locals 7

    const/4 v5, 0x0

    const-string v0, "sendRequest."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "sendRequest. Context is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->c(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->a:Lcom/sec/spp/push/notisvc/registration/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/registration/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sendRequest. is on working."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/registration/h;->a(Z)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "sendRequest. regId is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->j(Landroid/content/Context;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;I)V

    new-instance v0, Lcom/sec/spp/push/notisvc/agent/push/a;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/agent/push/a;-><init>()V

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/agent/push/a;->a(Landroid/content/Context;)V

    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/spp/push/notisvc/registration/h;->a(Z)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendRequest. regId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/sec/spp/push/notisvc/registration/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->g(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/agent/h;

    move-result-object v6

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/agent/d;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->e(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->d:Lcom/sec/spp/push/notisvc/registration/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "sendRequest. Save data fail."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/sec/spp/push/notisvc/registration/h;->a(Z)V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/sec/spp/push/notisvc/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->a()Lcom/sec/spp/push/notisvc/agent/d;

    move-result-object v0

    const-string v5, "POST"

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/spp/push/notisvc/agent/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->a()Lcom/sec/spp/push/notisvc/agent/d;

    move-result-object v1

    const-string v5, "POST"

    invoke-virtual/range {v1 .. v6}, Lcom/sec/spp/push/notisvc/agent/d;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V

    goto/16 :goto_0

    :cond_6
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->b:Lcom/sec/spp/push/notisvc/registration/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->d(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/registration/i;

    goto/16 :goto_0

    :cond_7
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/i;->c:Lcom/sec/spp/push/notisvc/registration/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "sendRequest. do nothing"

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "sendRequest. checkResult is error."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
