.class public Lcom/sec/spp/push/heartbeat/HeartBeat;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:Z

.field private static l:J

.field private static n:I

.field private static o:Lcom/sec/spp/push/heartbeat/b;


# instance fields
.field private m:Lcom/sec/spp/push/c/b;

.field private final p:Lcom/sec/spp/push/util/h;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x4

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->a:I

    const/4 v0, 0x5

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->b:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sput v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    sput-boolean v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->k:Z

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->l:J

    sget-object v0, Lcom/sec/spp/push/heartbeat/b;->a:Lcom/sec/spp/push/heartbeat/b;

    sput-object v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->o:Lcom/sec/spp/push/heartbeat/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Lcom/sec/spp/push/heartbeat/a;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/heartbeat/a;-><init>(Lcom/sec/spp/push/heartbeat/HeartBeat;)V

    iput-object v0, p0, Lcom/sec/spp/push/heartbeat/HeartBeat;->p:Lcom/sec/spp/push/util/h;

    return-void
.end method

.method public static a(I)V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_HANDLE_AOM_EVENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.EXTRA_AOM_INTERVAL"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendExecuteAomEventIntent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 9

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->u()V

    invoke-static {p1, p2, p3}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b(JZ)J

    move-result-wide v1

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, v1

    const-wide/16 v6, 0x0

    cmp-long v0, v1, v6

    if-nez v0, :cond_1

    const-string v0, "[HeartBeat]"

    const-string v6, "setAlarm. Next Time is now"

    invoke-static {v0, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v0, "alarm"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v6, v3, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "com.sec.spp.push.ACTION_SEND_PING_MESSAGE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "com.sec.spp.push.EXTRA_AOM_INTERVAL"

    invoke-virtual {v6, v7, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v7, 0x0

    const/high16 v8, 0x8000000

    invoke-static {v3, v7, v6, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const/4 v6, 0x2

    invoke-virtual {v0, v6, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Lcom/sec/spp/push/util/c;->h(J)V

    invoke-static {}, Lcom/sec/spp/push/util/o;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "REGRESSION TEST"

    const-string v0, "REGRESSION TEST"

    const-string v3, "[TC_03_03]"

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "REGRESSION TEST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Alarm will be called after "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v4, 0xea60

    div-long/2addr v1, v4

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Minutes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "[HeartBeat]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "setAlarm. Next Time is "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", after "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide/32 v7, 0xea60

    div-long v7, v1, v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Minutes"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setAlarm. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.sec.spp.push.EXTRA_ERROR_CODE"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail send ping with errorCode="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c(Z)V

    goto :goto_0

    :sswitch_0
    invoke-direct {p0, v4}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c(Z)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c()Z

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->r()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c()Z

    new-instance v1, Lcom/sec/spp/push/c/b;

    invoke-direct {v1}, Lcom/sec/spp/push/c/b;-><init>()V

    iput-object v1, p0, Lcom/sec/spp/push/heartbeat/HeartBeat;->m:Lcom/sec/spp/push/c/b;

    iget-object v1, p0, Lcom/sec/spp/push/heartbeat/HeartBeat;->m:Lcom/sec/spp/push/c/b;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/c/b;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/heartbeat/HeartBeat;->m:Lcom/sec/spp/push/c/b;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0xfa6 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic a(Lcom/sec/spp/push/heartbeat/HeartBeat;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->p()V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/heartbeat/HeartBeat;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b(I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.EXTRA_PING_PERIOD"

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(IIII)Z
    .locals 4

    const/16 v1, 0x3c

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    if-gt p1, v1, :cond_0

    if-lt p0, v0, :cond_0

    if-gt p0, v1, :cond_0

    if-lt p3, v0, :cond_0

    if-gt p3, v1, :cond_0

    if-lt p2, v0, :cond_0

    if-gt p2, v1, :cond_0

    if-lt p3, p1, :cond_0

    if-gt p3, p0, :cond_0

    if-gt p1, p0, :cond_0

    if-gt p2, p0, :cond_0

    add-int v1, p3, p2

    if-le v1, p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isValidPingValues() return "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method private static b(JZ)J
    .locals 7

    const-wide/32 v2, 0xa4cb80

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->k()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v4

    sget-object v5, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    if-ne v4, v5, :cond_2

    :cond_0
    :goto_0
    cmp-long v4, v2, v0

    if-gez v4, :cond_3

    if-nez p2, :cond_3

    const-string v4, "[HeartBeat]"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[setAlarm] timeInterval is less than minimum Interval. Set as : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/o;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "REGRESSION TEST"

    const-string v4, "REGRESSION TEST"

    const-string v5, "[TC_04_00]"

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "REGRESSION TEST"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[Error] Interval is less than minimum. Interval = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-wide v0

    :cond_2
    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/spp/push/h/c;->i()Z

    move-result v4

    if-nez v4, :cond_0

    move-wide v2, p0

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method private b(I)V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_HANDLE_PING_FAIL_EVENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.EXTRA_ERROR_CODE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendPingReqFailEvent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/sec/spp/push/heartbeat/b;->b:Lcom/sec/spp/push/heartbeat/b;

    sput-object v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->o:Lcom/sec/spp/push/heartbeat/b;

    const-string v0, "com.sec.spp.push.EXTRA_AOM_INTERVAL"

    const v1, 0x2dc6c0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->n:I

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->w()V

    return-void
.end method

.method private b(Z)V
    .locals 0

    sput-boolean p1, Lcom/sec/spp/push/heartbeat/HeartBeat;->k:Z

    return-void
.end method

.method private c(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(Z)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->d()V

    return-void
.end method

.method private d(Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "current mFailCount:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTopBaseCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_5

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    if-nez v0, :cond_3

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->b:I

    if-ge v0, v1, :cond_2

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mFailCount : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mTopBaseCount : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->b:I

    if-ne v0, v1, :cond_1

    sput v4, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    goto :goto_1

    :cond_3
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    if-ne v0, v4, :cond_4

    const-string v0, "[HeartBeat]"

    const-string v1, "adjustPingAlgorithmWIFI - fixed heartbeat interval"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    goto :goto_1

    :cond_5
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    if-nez v0, :cond_6

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    :cond_6
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    if-nez v0, :cond_7

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    if-lez v0, :cond_1

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    goto :goto_1

    :cond_7
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    if-ne v0, v4, :cond_1

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    if-eqz v0, :cond_1

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    goto :goto_1
.end method

.method private e(Z)V
    .locals 4

    const/4 v3, -0x1

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "adjustPingAlgorithmNetworkOperator. bSuccess:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[HeartBeat]"

    const-string v1, "[adjustPingAlgorithmNetworkOperator()] ping interval is fixed & success. return."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "[HeartBeat]"

    const-string v1, "[adjustPingAlgorithmNetworkOperator()]. ping interval is failed. reset."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->i()V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    goto :goto_0

    :cond_1
    const-string v0, "[HeartBeat]"

    const-string v1, "[adjustPingAlgorithmNetworkOperator()]. value is not fixed"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    if-lt v0, v1, :cond_5

    if-eqz p1, :cond_4

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    if-ne v0, v1, :cond_3

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    :cond_2
    :goto_1
    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "adjustPingAlgorithmNetworkOperator. : mPingTempInterval="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingFixedInterval="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    goto :goto_0

    :cond_3
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    add-int/2addr v0, v1

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    if-le v0, v1, :cond_2

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1

    :cond_4
    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1

    :cond_5
    if-eqz p1, :cond_6

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1

    :cond_6
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    if-ne v0, v1, :cond_7

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1

    :cond_7
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    if-ge v0, v1, :cond_2

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    sput v3, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1
.end method

.method public static declared-synchronized h()V
    .locals 8

    const-class v1, Lcom/sec/spp/push/heartbeat/HeartBeat;

    monitor-enter v1

    :try_start_0
    const-string v0, "[HeartBeat]"

    const-string v2, "updateNetworkOperatorPingVariables."

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "[HeartBeat]"

    const-string v2, "updateNetworkOperatorPingVariables. applicationContext is null"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->i(Landroid/content/Context;)I

    move-result v2

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->j(Landroid/content/Context;)I

    move-result v3

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->k(Landroid/content/Context;)I

    move-result v4

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->l(Landroid/content/Context;)I

    move-result v0

    invoke-static {}, Lcom/sec/spp/push/util/o;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "REGRESSION TEST"

    const-string v5, "REGRESSION TEST"

    const-string v6, "[TC_03_05]"

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "REGRESSION TEST"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "serverPingMax = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "REGRESSION TEST"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "serverPingMin = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "REGRESSION TEST"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "serverPingInc = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "REGRESSION TEST"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "serverPingAvg = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {v2, v3, v4, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(IIII)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/spp/push/k;->d(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/k;->e(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/spp/push/k;->f(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/k;->g(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_2
    const-string v0, "[HeartBeat]"

    const-string v2, "getServerPing value isn\'t valid. Set default ping values."

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/16 v2, 0x14

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->d(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->e(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->f(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->g(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public static j()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static k()I
    .locals 3

    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    move v1, v0

    :goto_0
    if-gtz v1, :cond_2

    :goto_1
    return v0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/g/a;->j(Landroid/content/Context;)I

    move-result v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static l()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_START_HEARTBEAT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendStartHeartbeatIntent :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static m()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_STOP_HEARTBEAT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendStopHeartbeatIntent :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static n()V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_RESCHEDULE_ALARM"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "resetHeartbeatAlarm : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static o()V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_PLMN_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendPlmnChangedIntent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private p()V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.sec.spp.push.ACTION_HANDLE_PING_SUCCESS_EVENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "com.sec.spp.push.test.ACTION_PING_RESPONSE"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendPingReqSuccessEvent : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private q()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/h/c;->g()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/util/q;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    const-string v2, "Reg App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " AppId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkg: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", regId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "Reg App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " AppId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pkg: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", userSN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", regId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/util/q;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private r()V
    .locals 2

    const-string v0, "[HeartBeat]"

    const-string v1, "Reset Connection"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->b()V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->h()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "[HeartBeat]"

    const-string v1, "ConnectionException."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private s()V
    .locals 3

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->n:I

    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(JZ)V

    return-void
.end method

.method private static t()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v2

    if-eq v2, v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v2

    const/16 v3, 0x9

    if-ne v2, v3, :cond_2

    :cond_0
    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    if-ne v2, v0, :cond_3

    :cond_1
    :goto_0
    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isFixedHeartBeatInterval : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_2
    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    if-ltz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private u()V
    .locals 5

    const-string v0, "[HeartBeat]"

    const-string v1, "cancelAlarm"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/spp/push/heartbeat/HeartBeat;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.sec.spp.push.ACTION_SEND_PING_MESSAGE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cancelAlarm. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private v()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->i()V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    return-void
.end method

.method private w()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "[HeartBeat]"

    const-string v1, "The HeartBeat had been stopped."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "[HeartBeat]"

    const-string v2, "======================"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PushServer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/e/b/g;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/e/b/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device token: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[HeartBeat]"

    const-string v1, "======================"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/spp/push/heartbeat/HeartBeat;->p:Lcom/sec/spp/push/util/h;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/d/a/b;->a(ZLcom/sec/spp/push/util/h;)V

    invoke-static {}, Lcom/sec/spp/push/f/c;->a()Lcom/sec/spp/push/f/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/f/c;->d()Lcom/sec/spp/push/f/e;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/f/f;->f:Lcom/sec/spp/push/f/f;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/f/e;->a(Lcom/sec/spp/push/f/f;Ljava/lang/String;)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "com.sec.spp.push.test.ACTION_PING_REQUEST"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[SendPingMessage] : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private x()V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[HeartBeat]"

    const-string v1, "Success. But isStarted() == false. return"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "[HeartBeat]"

    const-string v1, "Success send ping"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->y()V

    sget-object v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->o:Lcom/sec/spp/push/heartbeat/b;

    sget-object v1, Lcom/sec/spp/push/heartbeat/b;->b:Lcom/sec/spp/push/heartbeat/b;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->s()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c(Z)V

    goto :goto_0
.end method

.method private y()V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/spp/push/util/c;->e(J)V

    const-string v2, "[HeartBeat]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Set Last Ping Success Time : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->l:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const-string v0, "[HeartBeat]"

    const-string v1, "adjustPingAlgorithm. It\'s not time to adjustPingAlgoritm not yet"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sput-wide v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->l:J

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "adjustPingAlgorithm. update latestAdjustAlgorithmTime:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-wide v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->l:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/spp/push/heartbeat/HeartBeat;->d(Z)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/spp/push/heartbeat/HeartBeat;->e(Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    sget-boolean v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->k:Z

    return v0
.end method

.method public b()Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, "[HeartBeat]"

    const-string v2, "StartHeartBeat()"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->g()V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->d()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startHeart. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "[HeartBeat]"

    const-string v2, "stopHeartBeat()"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/spp/push/util/c;->h(J)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b(Z)V

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->u()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public d()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(JZ)V

    return-void
.end method

.method public e()I
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    :cond_0
    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->a:I

    mul-int/2addr v1, v2

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " min, isFixedHeartBeatInterval:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_2
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->a:I

    mul-int/2addr v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    :goto_1
    if-gtz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->i()V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    goto :goto_0

    :cond_4
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_1

    :cond_5
    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    goto :goto_0
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->a(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->b(I)V

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->h(I)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    sget v1, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->i(I)V

    goto :goto_0
.end method

.method public g()V
    .locals 5

    const-string v0, "[HeartBeat]"

    const-string v1, "loadPingVariables."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "[HeartBeat]"

    const-string v1, "loadPingVariables. applicationContext is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->i()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->k()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->j()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->a:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->l()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->b:I

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mFailCount:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTopBaseCount:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFiexd:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "[HeartBeat]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadPingVariables. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/util/g;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[HeartBeat]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "saved Plmn:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "[HeartBeat]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "current plmn:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->m()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->n()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->o()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->p()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->s()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->t()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load saved values., mPingMax:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingMin:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingInc:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingAvg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingTempInterval:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingFixedInterval:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFiexd:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->i()V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->f()V

    const-string v0, "[HeartBeat]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load init values., mPingMax:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingMin:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingInc:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingAvg:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mIsUpwardScenario:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingTempInterval:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPingFixedInterval:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFiexd:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->t()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public i()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "[HeartBeat]"

    const-string v1, "initPingVariables."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sput v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->c:I

    sput v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->d:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->l()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->b:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->m()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->e:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->n()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->f:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->o()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->g:I

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->p()I

    move-result v0

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    sget v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->h:I

    sput v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->i:I

    sput v2, Lcom/sec/spp/push/heartbeat/HeartBeat;->j:I

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "[HeartBeat]"

    const-string v1, "null Action"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "[HeartBeat]"

    const-string v1, "isRegistrationTableEmpty() == true, StopHeartBeat"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c()Z

    goto :goto_0

    :cond_2
    const-string v1, "com.sec.spp.push.ACTION_START_HEARTBEAT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b()Z

    goto :goto_0

    :cond_3
    const-string v1, "com.sec.spp.push.ACTION_STOP_HEARTBEAT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->c()Z

    goto :goto_0

    :cond_4
    const-string v1, "com.sec.spp.push.ACTION_RESCHEDULE_ALARM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->d()V

    goto :goto_0

    :cond_5
    const-string v1, "com.sec.spp.push.ACTION_EXCEPTION_OCCURED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(Z)V

    goto :goto_0

    :cond_6
    const-string v1, "com.sec.spp.push.ACTION_HANDLE_AOM_EVENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0, p2}, Lcom/sec/spp/push/heartbeat/HeartBeat;->b(Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    const-string v1, "com.sec.spp.push.ACTION_SEND_PING_MESSAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/sec/spp/push/util/o;->d()I

    move-result v0

    sget v1, Lcom/sec/spp/push/util/o;->d:I

    if-lt v0, v1, :cond_8

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->q()V

    :cond_8
    const-string v0, "android.intent.extra.ALARM_TARGET_TIME"

    const-wide/16 v1, 0x0

    invoke-virtual {p2, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "[HeartBeat]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onReceive() ------entered. setTime:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/heartbeat/b;->a:Lcom/sec/spp/push/heartbeat/b;

    sput-object v0, Lcom/sec/spp/push/heartbeat/HeartBeat;->o:Lcom/sec/spp/push/heartbeat/b;

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->w()V

    goto/16 :goto_0

    :cond_9
    const-string v1, "com.sec.spp.push.ACTION_HANDLE_PING_SUCCESS_EVENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->x()V

    goto/16 :goto_0

    :cond_a
    const-string v1, "com.sec.spp.push.ACTION_HANDLE_PING_FAIL_EVENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-direct {p0, p2}, Lcom/sec/spp/push/heartbeat/HeartBeat;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_b
    const-string v1, "com.sec.spp.push.ACTION_PLMN_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/heartbeat/HeartBeat;->v()V

    goto/16 :goto_0
.end method
