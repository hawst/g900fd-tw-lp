.class public Lcom/sec/spp/push/ActivitySelect;
.super Landroid/app/Activity;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/a;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/a;-><init>(Lcom/sec/spp/push/ActivitySelect;)V

    return-object v0
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/b;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/b;-><init>(Lcom/sec/spp/push/ActivitySelect;)V

    return-object v0
.end method

.method private c()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/c;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/c;-><init>(Lcom/sec/spp/push/ActivitySelect;)V

    return-object v0
.end method

.method private d()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/d;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/d;-><init>(Lcom/sec/spp/push/ActivitySelect;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->setContentView(I)V

    const v0, 0x7f090033

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->b:Landroid/widget/Button;

    const v0, 0x7f090035

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->c:Landroid/widget/Button;

    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->d:Landroid/widget/Button;

    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/ActivitySelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->e:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->b:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/ActivitySelect;->a()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->c:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/ActivitySelect;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->d:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/ActivitySelect;->c()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->e:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/ActivitySelect;->d()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "gld.push.samsungosp.com"

    const-string v1, "ec2-54-249-227-71.ap-northeast-1.compute.amazonaws.com"

    const-string v2, "175.41.248.198"

    const-string v3, "54.248.253.200"

    const-string v4, "gld.push.samsungosp.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const-string v1, "Server : PROD"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "gld.push.samsungosp.com"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const-string v0, "gld.push.samsungosp.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const-string v1, "Server : STG2"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const-string v0, "gld.push.samsungosp.com"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const-string v1, "Server : STG1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const-string v0, "gld.push.samsungosp.com"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const-string v1, "Server : DEV"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/ActivitySelect;->a:Landroid/widget/TextView;

    const-string v1, "Server : ERROR"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
