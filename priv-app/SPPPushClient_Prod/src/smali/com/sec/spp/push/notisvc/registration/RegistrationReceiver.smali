.class public Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const-string v0, "ctx or intent is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "actionName is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "intent action : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v2, "com.sec.spp.push.BIGJOE_REGI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p2}, Lcom/sec/spp/push/notisvc/registration/f;->a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/f;

    move-result-object v0

    :cond_3
    :goto_1
    if-nez v0, :cond_6

    const-string v0, "Registartion is null."

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/RegistrationReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "com.sec.spp.push.AGREE_REGI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {p2}, Lcom/sec/spp/push/notisvc/registration/c;->a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/c;

    move-result-object v0

    goto :goto_1

    :cond_5
    const-string v2, "com.sec.spp.push.NOTIFICATION_NOTICE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p2}, Lcom/sec/spp/push/notisvc/registration/a;->a(Landroid/content/Intent;)Lcom/sec/spp/push/notisvc/registration/a;

    move-result-object v0

    goto :goto_1

    :cond_6
    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/registration/h;->k(Landroid/content/Context;)V

    goto :goto_0
.end method
