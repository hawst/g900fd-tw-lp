.class public Lcom/sec/spp/push/dlc/db/DbService;
.super Landroid/app/Service;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:J

.field private static c:I

.field private static synthetic d:[I

.field private static synthetic e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    const-wide/32 v0, 0x5265c00

    sput-wide v0, Lcom/sec/spp/push/dlc/db/DbService;->b:J

    const v0, 0xdbba00

    sput v0, Lcom/sec/spp/push/dlc/db/DbService;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/spp/push/dlc/sender/q;
    .locals 12

    const/4 v2, 0x0

    const/4 v11, -0x1

    const/16 v10, -0x3e8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v0, "updateRegiDB. dbhandler is null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/db/e;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->c(Landroid/content/Context;)V

    if-eqz v6, :cond_1

    array-length v7, v6

    move v3, v2

    :goto_1
    if-lt v3, v7, :cond_2

    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    goto :goto_0

    :cond_2
    aget-object v8, v6, v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    if-nez v0, :cond_4

    invoke-static {v8, v10, v10}, Lcom/sec/spp/push/dlc/db/AppQuota;->a(Ljava/lang/String;II)Lcom/sec/spp/push/dlc/db/AppQuota;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/db/AppQuota;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/AppQuota;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v1, 0x1

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/AppQuota;->b()I

    move-result v9

    if-eq v9, v11, :cond_6

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/AppQuota;->c()I

    move-result v9

    if-ne v9, v11, :cond_7

    :cond_6
    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/AppQuota;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    :cond_7
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    :cond_8
    invoke-virtual {v5, v4}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/util/ArrayList;)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v0

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0
.end method

.method public static a(I)V
    .locals 0

    sput p0, Lcom/sec/spp/push/dlc/db/DbService;->c:I

    return-void
.end method

.method public static a(J)V
    .locals 0

    sput-wide p0, Lcom/sec/spp/push/dlc/db/DbService;->b:J

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;IILandroid/os/Messenger;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->au:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->av:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p4, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;JJLandroid/os/Messenger;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aw:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ax:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    if-eqz p6, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    if-eqz p3, :cond_1

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p4, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p3, :cond_1

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    if-eqz p4, :cond_2

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;Ljava/util/ArrayList;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p4, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_0
    if-eqz p3, :cond_1

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    if-eqz p3, :cond_1

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    if-eqz p4, :cond_2

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ap:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    if-eqz p5, :cond_3

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private a(Lcom/sec/spp/push/dlc/db/d;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->C(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/d;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/spp/push/dlc/util/f;->g(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILandroid/database/Cursor;Lcom/sec/spp/push/dlc/db/e;)V
    .locals 4

    const-string v0, "dataquota"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v0, "dataus"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " DataQuota ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] DataUsed ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] sentSize ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-lez p2, :cond_0

    add-int/2addr v0, p2

    :cond_0
    invoke-virtual {p4, p1, v0}, Lcom/sec/spp/push/dlc/db/e;->b(Ljava/lang/String;I)Lcom/sec/spp/push/dlc/sender/q;

    return-void
.end method

.method private a(Ljava/util/ArrayList;Z)V
    .locals 8

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v0, "adjustAppQuotaInfo. dbHandler is null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/db/Log;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " Prev ["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "] New ["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "] "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v7, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " sentSize ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/sec/spp/push/dlc/db/e;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_5
    if-nez p2, :cond_6

    invoke-direct {p0, v0, v1, v4, v3}, Lcom/sec/spp/push/dlc/db/DbService;->a(Ljava/lang/String;ILandroid/database/Cursor;Lcom/sec/spp/push/dlc/db/e;)V

    :goto_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_6
    invoke-direct {p0, v0, v1, v4, v3}, Lcom/sec/spp/push/dlc/db/DbService;->b(Ljava/lang/String;ILandroid/database/Cursor;Lcom/sec/spp/push/dlc/db/e;)V

    goto :goto_4

    :cond_7
    move v1, v2

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->C(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/db/d;->valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/db/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()[I
    .locals 3

    sget-object v0, Lcom/sec/spp/push/dlc/db/DbService;->d:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/dlc/db/d;->values()[Lcom/sec/spp/push/dlc/db/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->e:Lcom/sec/spp/push/dlc/db/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->d:Lcom/sec/spp/push/dlc/db/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/spp/push/dlc/db/DbService;->d:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;ILandroid/database/Cursor;Lcom/sec/spp/push/dlc/db/e;)V
    .locals 4

    const-string v0, "wifiquota"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v0, "wifius"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " wifiQuota ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] wifiUsed ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] sentSize ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-lez p2, :cond_0

    add-int/2addr v0, p2

    :cond_0
    invoke-virtual {p4, p1, v0}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;I)Lcom/sec/spp/push/dlc/sender/q;

    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->C(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/db/d;->valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/db/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()[I
    .locals 3

    sget-object v0, Lcom/sec/spp/push/dlc/db/DbService;->e:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/dlc/sender/i;->values()[Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->p:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4d

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->w:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4c

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->G:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4b

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->I:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4a

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->K:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_49

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->J:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_48

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->P:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2a

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_47

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->T:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2e

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_46

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->L:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_45

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->M:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_44

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->R:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2c

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_43

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->U:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2f

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_42

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->Q:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2b

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_41

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->O:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_40

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->S:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x2d

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_3f

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->N:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_3e

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->D:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_3d

    :goto_11
    :try_start_11
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->F:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_3c

    :goto_12
    :try_start_12
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->E:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_3b

    :goto_13
    :try_start_13
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aj:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3e

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_3a

    :goto_14
    :try_start_14
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->af:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3a

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_39

    :goto_15
    :try_start_15
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ak:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3f

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_38

    :goto_16
    :try_start_16
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ae:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x39

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_37

    :goto_17
    :try_start_17
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ag:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3b

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_36

    :goto_18
    :try_start_18
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->W:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x31

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_35

    :goto_19
    :try_start_19
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->X:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x32

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_34

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aa:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x35

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_33

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ah:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3c

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_32

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ab:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x36

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_31

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->Z:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x34

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_30

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ac:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x37

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_2f

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->Y:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x33

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_2e

    :goto_20
    :try_start_20
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ad:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x38

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_2d

    :goto_21
    :try_start_21
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ai:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x3d

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_2c

    :goto_22
    :try_start_22
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->al:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x40

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_2b

    :goto_23
    :try_start_23
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->V:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x30

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_2a

    :goto_24
    :try_start_24
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->H:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_29

    :goto_25
    :try_start_25
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->o:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_28

    :goto_26
    :try_start_26
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->y:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_27

    :goto_27
    :try_start_27
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_26

    :goto_28
    :try_start_28
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->e:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_25

    :goto_29
    :try_start_29
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_29
    .catch Ljava/lang/NoSuchFieldError; {:try_start_29 .. :try_end_29} :catch_24

    :goto_2a
    :try_start_2a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_2a} :catch_23

    :goto_2b
    :try_start_2b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_2b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2b .. :try_end_2b} :catch_22

    :goto_2c
    :try_start_2c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->a:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2c .. :try_end_2c} :catch_21

    :goto_2d
    :try_start_2d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->l:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_2d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2d .. :try_end_2d} :catch_20

    :goto_2e
    :try_start_2e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2e .. :try_end_2e} :catch_1f

    :goto_2f
    :try_start_2f
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->m:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_2f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2f .. :try_end_2f} :catch_1e

    :goto_30
    :try_start_30
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->C:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_30
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_30} :catch_1d

    :goto_31
    :try_start_31
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->h:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_31
    .catch Ljava/lang/NoSuchFieldError; {:try_start_31 .. :try_end_31} :catch_1c

    :goto_32
    :try_start_32
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->g:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_32
    .catch Ljava/lang/NoSuchFieldError; {:try_start_32 .. :try_end_32} :catch_1b

    :goto_33
    :try_start_33
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->x:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_33} :catch_1a

    :goto_34
    :try_start_34
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->n:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_34
    .catch Ljava/lang/NoSuchFieldError; {:try_start_34 .. :try_end_34} :catch_19

    :goto_35
    :try_start_35
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->q:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_35} :catch_18

    :goto_36
    :try_start_36
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->an:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x42

    aput v2, v0, v1
    :try_end_36
    .catch Ljava/lang/NoSuchFieldError; {:try_start_36 .. :try_end_36} :catch_17

    :goto_37
    :try_start_37
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->am:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x41

    aput v2, v0, v1
    :try_end_37
    .catch Ljava/lang/NoSuchFieldError; {:try_start_37 .. :try_end_37} :catch_16

    :goto_38
    :try_start_38
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->b:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_38
    .catch Ljava/lang/NoSuchFieldError; {:try_start_38 .. :try_end_38} :catch_15

    :goto_39
    :try_start_39
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->u:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_39
    .catch Ljava/lang/NoSuchFieldError; {:try_start_39 .. :try_end_39} :catch_14

    :goto_3a
    :try_start_3a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->i:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_3a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3a .. :try_end_3a} :catch_13

    :goto_3b
    :try_start_3b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->t:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_3b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3b .. :try_end_3b} :catch_12

    :goto_3c
    :try_start_3c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->k:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_3c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3c .. :try_end_3c} :catch_11

    :goto_3d
    :try_start_3d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->j:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_3d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3d .. :try_end_3d} :catch_10

    :goto_3e
    :try_start_3e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->c:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_3e} :catch_f

    :goto_3f
    :try_start_3f
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->d:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3f .. :try_end_3f} :catch_e

    :goto_40
    :try_start_40
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->r:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_40} :catch_d

    :goto_41
    :try_start_41
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->s:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_41
    .catch Ljava/lang/NoSuchFieldError; {:try_start_41 .. :try_end_41} :catch_c

    :goto_42
    :try_start_42
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ay:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x4d

    aput v2, v0, v1
    :try_end_42
    .catch Ljava/lang/NoSuchFieldError; {:try_start_42 .. :try_end_42} :catch_b

    :goto_43
    :try_start_43
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->au:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x49

    aput v2, v0, v1
    :try_end_43
    .catch Ljava/lang/NoSuchFieldError; {:try_start_43 .. :try_end_43} :catch_a

    :goto_44
    :try_start_44
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->av:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x4a

    aput v2, v0, v1
    :try_end_44
    .catch Ljava/lang/NoSuchFieldError; {:try_start_44 .. :try_end_44} :catch_9

    :goto_45
    :try_start_45
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aw:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x4b

    aput v2, v0, v1
    :try_end_45
    .catch Ljava/lang/NoSuchFieldError; {:try_start_45 .. :try_end_45} :catch_8

    :goto_46
    :try_start_46
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ax:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x4c

    aput v2, v0, v1
    :try_end_46
    .catch Ljava/lang/NoSuchFieldError; {:try_start_46 .. :try_end_46} :catch_7

    :goto_47
    :try_start_47
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x46

    aput v2, v0, v1
    :try_end_47
    .catch Ljava/lang/NoSuchFieldError; {:try_start_47 .. :try_end_47} :catch_6

    :goto_48
    :try_start_48
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x47

    aput v2, v0, v1
    :try_end_48
    .catch Ljava/lang/NoSuchFieldError; {:try_start_48 .. :try_end_48} :catch_5

    :goto_49
    :try_start_49
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x48

    aput v2, v0, v1
    :try_end_49
    .catch Ljava/lang/NoSuchFieldError; {:try_start_49 .. :try_end_49} :catch_4

    :goto_4a
    :try_start_4a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->az:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x4e

    aput v2, v0, v1
    :try_end_4a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4a .. :try_end_4a} :catch_3

    :goto_4b
    :try_start_4b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x43

    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_4b} :catch_2

    :goto_4c
    :try_start_4c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ap:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x44

    aput v2, v0, v1
    :try_end_4c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4c .. :try_end_4c} :catch_1

    :goto_4d
    :try_start_4d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aq:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    const/16 v2, 0x45

    aput v2, v0, v1
    :try_end_4d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4d .. :try_end_4d} :catch_0

    :goto_4e
    sput-object v0, Lcom/sec/spp/push/dlc/db/DbService;->e:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_4e

    :catch_1
    move-exception v1

    goto :goto_4d

    :catch_2
    move-exception v1

    goto :goto_4c

    :catch_3
    move-exception v1

    goto :goto_4b

    :catch_4
    move-exception v1

    goto :goto_4a

    :catch_5
    move-exception v1

    goto :goto_49

    :catch_6
    move-exception v1

    goto :goto_48

    :catch_7
    move-exception v1

    goto :goto_47

    :catch_8
    move-exception v1

    goto :goto_46

    :catch_9
    move-exception v1

    goto :goto_45

    :catch_a
    move-exception v1

    goto :goto_44

    :catch_b
    move-exception v1

    goto/16 :goto_43

    :catch_c
    move-exception v1

    goto/16 :goto_42

    :catch_d
    move-exception v1

    goto/16 :goto_41

    :catch_e
    move-exception v1

    goto/16 :goto_40

    :catch_f
    move-exception v1

    goto/16 :goto_3f

    :catch_10
    move-exception v1

    goto/16 :goto_3e

    :catch_11
    move-exception v1

    goto/16 :goto_3d

    :catch_12
    move-exception v1

    goto/16 :goto_3c

    :catch_13
    move-exception v1

    goto/16 :goto_3b

    :catch_14
    move-exception v1

    goto/16 :goto_3a

    :catch_15
    move-exception v1

    goto/16 :goto_39

    :catch_16
    move-exception v1

    goto/16 :goto_38

    :catch_17
    move-exception v1

    goto/16 :goto_37

    :catch_18
    move-exception v1

    goto/16 :goto_36

    :catch_19
    move-exception v1

    goto/16 :goto_35

    :catch_1a
    move-exception v1

    goto/16 :goto_34

    :catch_1b
    move-exception v1

    goto/16 :goto_33

    :catch_1c
    move-exception v1

    goto/16 :goto_32

    :catch_1d
    move-exception v1

    goto/16 :goto_31

    :catch_1e
    move-exception v1

    goto/16 :goto_30

    :catch_1f
    move-exception v1

    goto/16 :goto_2f

    :catch_20
    move-exception v1

    goto/16 :goto_2e

    :catch_21
    move-exception v1

    goto/16 :goto_2d

    :catch_22
    move-exception v1

    goto/16 :goto_2c

    :catch_23
    move-exception v1

    goto/16 :goto_2b

    :catch_24
    move-exception v1

    goto/16 :goto_2a

    :catch_25
    move-exception v1

    goto/16 :goto_29

    :catch_26
    move-exception v1

    goto/16 :goto_28

    :catch_27
    move-exception v1

    goto/16 :goto_27

    :catch_28
    move-exception v1

    goto/16 :goto_26

    :catch_29
    move-exception v1

    goto/16 :goto_25

    :catch_2a
    move-exception v1

    goto/16 :goto_24

    :catch_2b
    move-exception v1

    goto/16 :goto_23

    :catch_2c
    move-exception v1

    goto/16 :goto_22

    :catch_2d
    move-exception v1

    goto/16 :goto_21

    :catch_2e
    move-exception v1

    goto/16 :goto_20

    :catch_2f
    move-exception v1

    goto/16 :goto_1f

    :catch_30
    move-exception v1

    goto/16 :goto_1e

    :catch_31
    move-exception v1

    goto/16 :goto_1d

    :catch_32
    move-exception v1

    goto/16 :goto_1c

    :catch_33
    move-exception v1

    goto/16 :goto_1b

    :catch_34
    move-exception v1

    goto/16 :goto_1a

    :catch_35
    move-exception v1

    goto/16 :goto_19

    :catch_36
    move-exception v1

    goto/16 :goto_18

    :catch_37
    move-exception v1

    goto/16 :goto_17

    :catch_38
    move-exception v1

    goto/16 :goto_16

    :catch_39
    move-exception v1

    goto/16 :goto_15

    :catch_3a
    move-exception v1

    goto/16 :goto_14

    :catch_3b
    move-exception v1

    goto/16 :goto_13

    :catch_3c
    move-exception v1

    goto/16 :goto_12

    :catch_3d
    move-exception v1

    goto/16 :goto_11

    :catch_3e
    move-exception v1

    goto/16 :goto_10

    :catch_3f
    move-exception v1

    goto/16 :goto_f

    :catch_40
    move-exception v1

    goto/16 :goto_e

    :catch_41
    move-exception v1

    goto/16 :goto_d

    :catch_42
    move-exception v1

    goto/16 :goto_c

    :catch_43
    move-exception v1

    goto/16 :goto_b

    :catch_44
    move-exception v1

    goto/16 :goto_a

    :catch_45
    move-exception v1

    goto/16 :goto_9

    :catch_46
    move-exception v1

    goto/16 :goto_8

    :catch_47
    move-exception v1

    goto/16 :goto_7

    :catch_48
    move-exception v1

    goto/16 :goto_6

    :catch_49
    move-exception v1

    goto/16 :goto_5

    :catch_4a
    move-exception v1

    goto/16 :goto_4

    :catch_4b
    move-exception v1

    goto/16 :goto_3

    :catch_4c
    move-exception v1

    goto/16 :goto_2

    :catch_4d
    move-exception v1

    goto/16 :goto_1
.end method

.method private c()V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->q(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->r(Landroid/content/Context;)V

    invoke-static {p0, v3}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;I)V

    const-string v0, "DayLimitResetTimer started"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->n:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/db/DbService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sget-wide v5, Lcom/sec/spp/push/dlc/db/DbService;->b:J

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Context;)V
    .locals 6

    const-string v0, "Notify unavailable"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "notifyUnavailable. dbhandler is null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->b()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_4

    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :cond_4
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    const-string v0, "intent"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->aq:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "unavailable"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Send Notify unavailable BR to ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/spp/push/dlc/db/DbService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    :cond_9
    throw v0
.end method

.method private d()V
    .locals 7

    const-string v0, "DayLimitResetTimer rescheduled"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/dlc/db/DbService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->n:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/db/DbService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    sget-wide v5, Lcom/sec/spp/push/dlc/db/DbService;->b:J

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 6

    const-string v0, "Notify available"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "notifyAvailable. dbhandler is null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->b()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_4

    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :cond_4
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    const-string v0, "intent"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->aq:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "available"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Send Notify Available BR to ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/spp/push/dlc/db/DbService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_5

    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    :cond_9
    throw v0
.end method

.method private e()Z
    .locals 1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private f()Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->e(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()Z
    .locals 6

    const-wide/16 v4, 0x400

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->o(Landroid/content/Context;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->g(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v4

    sub-long/2addr v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "todayInserted size ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] limitADay ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Ljava/util/ArrayList;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "getAllRegAllInfo. dbhandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->b()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/sec/spp/push/dlc/db/h;

    invoke-direct {v3}, Lcom/sec/spp/push/dlc/db/h;-><init>()V

    const-string v4, "package"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/h;->a(Ljava/lang/String;)V

    const-string v4, "dataquota"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/h;->a(I)V

    const-string v4, "dataus"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/h;->c(I)V

    const-string v4, "wifiquota"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/h;->b(I)V

    const-string v4, "wifius"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/h;->d(I)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/sec/spp/push/dlc/sender/i;)V
    .locals 3

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->C(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/db/d;->valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/db/d;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DB State ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Action ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/dlc/db/DbService;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/d;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->h:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_4

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->e:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->n:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_5

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->aj:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->d:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->n:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_6

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_9

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->g()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->e:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->e()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_2
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_c

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->e:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->g()Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->d:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_c
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->e:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_d

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->m:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_e

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_e
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->f()Z

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->e:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_f
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->e()Z

    move-result v0

    if-eqz v0, :cond_10

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_10
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->g()Z

    move-result v0

    if-nez v0, :cond_11

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->d:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_11
    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto/16 :goto_0

    :pswitch_4
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_12

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->c:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->c(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_12
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->e()Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->b:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_13
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->g()Z

    move-result v0

    if-nez v0, :cond_14

    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->d:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    goto/16 :goto_0

    :cond_14
    sget-object v0, Lcom/sec/spp/push/dlc/db/d;->a:Lcom/sec/spp/push/dlc/db/d;

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/db/d;)V

    invoke-direct {p0, p0}, Lcom/sec/spp/push/dlc/db/DbService;->d(Landroid/content/Context;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "DbService Started"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->c()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "DbService Destroyed"

    sget-object v1, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 15

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/i;->valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v8

    invoke-static {}, Lcom/sec/spp/push/dlc/db/DbService;->b()[I

    move-result-object v1

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, v8}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    :cond_0
    :goto_0
    move/from16 v0, p3

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/db/DbService;->stopSelf(I)V

    invoke-super/range {p0 .. p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1

    :pswitch_1
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/db/Log;

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/sec/spp/push/dlc/db/b;->a(Lcom/sec/spp/push/dlc/db/Log;Z)J

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->a()V

    :goto_1
    invoke-virtual {p0, v8}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    goto :goto_0

    :cond_1
    const-string v1, "fail to insert log. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/db/Log;

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lcom/sec/spp/push/dlc/db/b;->a(Lcom/sec/spp/push/dlc/db/Log;Z)J

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto :goto_0

    :cond_2
    const-string v1, "fail to insert ulog. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/sec/spp/push/dlc/db/SummLog;

    if-nez v8, :cond_3

    const-string v1, "[ACTION_DB_INSERT_SUM_LOG] sLog is empty"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const-string v1, "[ACTION_DB_INSERT_SUM_LOG] invalid log value"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1, v2, v3}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    invoke-virtual {v1, v8}, Lcom/sec/spp/push/dlc/db/b;->a(Lcom/sec/spp/push/dlc/db/SummLog;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const-string v1, "Exception on insert sum log"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v4, "sum1"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const-string v5, "sum2"

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "sum3"

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "sum4"

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const-string v10, "sum5"

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    int-to-long v11, v4

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->l()J

    move-result-wide v13

    add-long/2addr v11, v13

    long-to-int v4, v11

    int-to-long v11, v5

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->m()J

    move-result-wide v13

    add-long/2addr v11, v13

    long-to-int v5, v11

    int-to-long v11, v6

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->n()J

    move-result-wide v13

    add-long/2addr v11, v13

    long-to-int v6, v11

    int-to-long v11, v7

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->o()J

    move-result-wide v13

    add-long/2addr v11, v13

    long-to-int v7, v11

    int-to-long v10, v10

    invoke-virtual {v8}, Lcom/sec/spp/push/dlc/db/SummLog;->p()J

    move-result-wide v12

    add-long/2addr v10, v12

    long-to-int v8, v10

    invoke-virtual/range {v1 .. v8}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/lang/String;Ljava/lang/String;IIIII)I

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto/16 :goto_0

    :cond_8
    const-string v1, "fail to insert sumlog. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/b;->b()V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto/16 :goto_0

    :cond_9
    const-string v1, "fail to delete all logs. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    :goto_3
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Ljava/util/ArrayList;Z)V

    :cond_a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->ak:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    :try_start_0
    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/db/Log;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/Log;->a()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/sec/spp/push/dlc/db/b;->b(J)V

    goto :goto_2

    :cond_c
    const-string v1, "fail to delete log data. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_f

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_e

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    :goto_5
    const/4 v1, 0x1

    invoke-direct {p0, v2, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Ljava/util/ArrayList;Z)V

    :cond_d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->al:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    :try_start_1
    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_e
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/db/Log;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/Log;->a()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/sec/spp/push/dlc/db/b;->b(J)V

    goto :goto_4

    :cond_f
    const-string v1, "fail to delete log data. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :pswitch_7
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/db/b;->b(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto/16 :goto_0

    :cond_10
    const-string v1, "fail to delete ulogs. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aw:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->ax:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ACTION_DB_DELETE_SUM_LOGS "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v5

    if-eqz v5, :cond_11

    invoke-virtual {v5, v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/db/b;->a(JJ)V

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto/16 :goto_0

    :cond_11
    const-string v1, "fail to delete sumlogs. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual {p0, v8}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    goto/16 :goto_0

    :pswitch_a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_13

    invoke-virtual {v3, v2}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move-object v2, v1

    :goto_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->Y:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    if-eqz v2, :cond_12

    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_12
    :try_start_2
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_13
    const-string v2, "fail to get ulogs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_6

    :pswitch_b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->au:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->h()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_15

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v1, v4}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move-object v2, v1

    :goto_7
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->W:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    if-eqz v2, :cond_14

    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_14
    :try_start_3
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_15
    const/4 v1, 0x0

    const-string v2, "fail to get logs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_7

    :pswitch_c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->au:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->h()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_17

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v1, v4}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move-object v2, v1

    :goto_8
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->X:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    if-eqz v2, :cond_16

    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_16
    :try_start_4
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_17
    const/4 v1, 0x0

    const-string v2, "fail to get logs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_8

    :pswitch_d
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->au:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_19

    invoke-virtual {v3, v2}, Lcom/sec/spp/push/dlc/db/b;->a(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move-object v2, v1

    :goto_9
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->Z:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    if-eqz v2, :cond_18

    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->at:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :cond_18
    :try_start_5
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_19
    const-string v2, "fail to get sumlogs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_9

    :pswitch_e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v3

    if-eqz v3, :cond_1a

    invoke-virtual {v3, v2}, Lcom/sec/spp/push/dlc/db/b;->c(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move v2, v1

    :goto_a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ab:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_6
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_1a
    const-string v2, "fail to get num of ulogs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    goto :goto_a

    :pswitch_f
    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->c()I

    move-result v1

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move v2, v1

    :goto_b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->aa:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_7
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_1b
    const-string v2, "fail to get num of logs. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    goto :goto_b

    :pswitch_10
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v2

    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->d()I

    move-result v1

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/b;->a()V

    move v2, v1

    :goto_c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ac:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_8
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_8

    goto/16 :goto_0

    :catch_8
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_1c
    const-string v2, "fail to get sum of bodies. dbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    goto :goto_c

    :pswitch_11
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/DbService;->d()V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;I)V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->c()[Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1d

    array-length v10, v9

    const/4 v2, 0x0

    move v7, v2

    :goto_d
    if-lt v7, v10, :cond_1f

    :cond_1d
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    invoke-virtual {p0, v8}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    :cond_1e
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->o:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {p0, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V

    goto/16 :goto_0

    :cond_1f
    aget-object v2, v9, v7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;IIII)Lcom/sec/spp/push/dlc/sender/q;

    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_d

    :pswitch_12
    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;

    move-result-object v1

    if-eqz v1, :cond_20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget v4, Lcom/sec/spp/push/dlc/db/DbService;->c:I

    int-to-long v4, v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/spp/push/dlc/db/b;->a(J)V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/b;->a()V

    goto/16 :goto_0

    :cond_20
    const-string v1, "fail to delete old data. dbHandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_13
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->af:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v4

    if-eqz v4, :cond_23

    invoke-virtual {v4, v2}, Lcom/sec/spp/push/dlc/db/e;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_21

    const-string v1, "ACTION_CHECK_REGI. cursor is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    iput v1, v3, Landroid/os/Message;->arg1:I

    goto/16 :goto_0

    :cond_21
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_22

    sget-object v5, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v5

    iput v5, v3, Landroid/os/Message;->arg1:I

    :goto_e
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/e;->a()V

    :goto_f
    :try_start_9
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_9

    goto/16 :goto_0

    :catch_9
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_22
    sget-object v5, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v5

    iput v5, v3, Landroid/os/Message;->arg1:I

    goto :goto_e

    :cond_23
    const-string v2, "ACTION_CHECK_REGI. dbhandler is null"

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    iput v2, v3, Landroid/os/Message;->arg1:I

    goto :goto_f

    :pswitch_14
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/sec/spp/push/dlc/sender/RegiReply;

    const/4 v1, 0x0

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/sender/RegiReply;->c()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2e

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/sender/RegiReply;->c()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/db/AppQuota;

    move-object v4, v1

    :goto_10
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/os/Messenger;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ap:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v4, :cond_25

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-eqz v1, :cond_24

    const/16 v3, -0x3e8

    const/16 v4, -0x3e8

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;IILjava/lang/String;I)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v3

    invoke-static {p0, v2}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move-object v1, v3

    :goto_11
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ad:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    iput v1, v3, Landroid/os/Message;->arg1:I

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/sender/RegiReply;->b()I

    move-result v1

    iput v1, v3, Landroid/os/Message;->arg2:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :try_start_a
    invoke-virtual {v8, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_a

    goto/16 :goto_0

    :catch_a
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_24
    const-string v1, "ACTION_REGISTER. dbhandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    goto :goto_11

    :cond_25
    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/AppQuota;->b()I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_26

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/AppQuota;->c()I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_27

    :cond_26
    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/AppQuota;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->i:Lcom/sec/spp/push/dlc/sender/q;

    goto :goto_11

    :cond_27
    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-eqz v1, :cond_28

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/AppQuota;->c()I

    move-result v3

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/db/AppQuota;->b()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;IILjava/lang/String;I)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v3

    invoke-static {p0, v2}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move-object v1, v3

    goto :goto_11

    :cond_28
    const-string v1, "ACTION_REGISTER. dbhandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    goto :goto_11

    :pswitch_15
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ar:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/spp/push/dlc/sender/RegiReply;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/RegiReply;->c()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    if-ne v2, v3, :cond_29

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/RegiReply;->b()I

    move-result v1

    invoke-static {p0, v1}, Lcom/sec/spp/push/dlc/util/f;->c(Landroid/content/Context;I)V

    :cond_29
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ai:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_b
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_b

    goto/16 :goto_0

    :catch_b
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_16
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v2

    if-eqz v2, :cond_2a

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move v2, v1

    :goto_12
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ae:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_c
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_c

    goto/16 :goto_0

    :catch_c
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_2a
    const-string v1, "ACTION_DEREGISTER. dbhandler is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    move v2, v1

    goto :goto_12

    :pswitch_17
    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v2

    if-eqz v2, :cond_2c

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/e;->b()Landroid/database/Cursor;

    move-result-object v3

    if-nez v3, :cond_2b

    const-string v3, "cannot get num of reg apps. cursor is null"

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_13
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move v2, v1

    :goto_14
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->ah:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v4}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v4

    iput v4, v3, Landroid/os/Message;->what:I

    iput v2, v3, Landroid/os/Message;->arg1:I

    :try_start_d
    invoke-virtual {v1, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_d

    goto/16 :goto_0

    :catch_d
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_2b
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GET_NUM_OF_REGI_APPS. count = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_13

    :cond_2c
    const-string v2, "cannot get num of reg apps. regDbHandler is null"

    sget-object v3, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    goto :goto_14

    :pswitch_18
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Messenger;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->ag:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v3

    if-eqz v3, :cond_2d

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->c()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    sget-object v5, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :goto_15
    :try_start_e
    invoke-virtual {v1, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_e

    goto/16 :goto_0

    :catch_e
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :cond_2d
    const-string v3, "cannot get all reg apps. regDbHandler is null"

    sget-object v4, Lcom/sec/spp/push/dlc/db/DbService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_15

    :pswitch_19
    invoke-virtual {p0, v8}, Lcom/sec/spp/push/dlc/db/DbService;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    goto/16 :goto_0

    :cond_2e
    move-object v4, v1

    goto/16 :goto_10

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_13
        :pswitch_14
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_18
        :pswitch_17
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_19
    .end packed-switch
.end method
