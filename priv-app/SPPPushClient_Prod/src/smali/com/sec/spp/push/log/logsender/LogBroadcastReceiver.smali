.class public Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const/16 v7, 0x64

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-object v0, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recieve broadcast msg - action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.spp.push.DLC_REPLY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aq:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    const-string v1, "recieve broadcast msg - extra : empty value"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "recieve broadcast msg - extra : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->am:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->az:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "regi result code : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eq v0, v7, :cond_2

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    :cond_2
    invoke-static {v6}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Z)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->an:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->az:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/logsender/LogBroadcastReceiver;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "regi result code : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-ne v0, v7, :cond_0

    invoke-static {v5}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Z)V

    goto :goto_0

    :cond_4
    const-string v0, "available"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/spp/push/k;->d(Z)V

    goto/16 :goto_0

    :cond_5
    const-string v0, "unavailable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/sec/spp/push/k;->d(Z)V

    goto/16 :goto_0
.end method
