.class public final enum Lcom/sec/spp/push/notisvc/card/notification/b;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/card/notification/b;

.field public static final enum b:Lcom/sec/spp/push/notisvc/card/notification/b;

.field public static final enum c:Lcom/sec/spp/push/notisvc/card/notification/b;

.field private static final synthetic e:[Lcom/sec/spp/push/notisvc/card/notification/b;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/spp/push/notisvc/card/notification/b;

    const-string v1, "STATUS_PAUSE"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/spp/push/notisvc/card/notification/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/notification/b;

    const-string v1, "STATUS_PLAY"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/spp/push/notisvc/card/notification/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->b:Lcom/sec/spp/push/notisvc/card/notification/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/card/notification/b;

    const-string v1, "STATUS_LOADING"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/spp/push/notisvc/card/notification/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->c:Lcom/sec/spp/push/notisvc/card/notification/b;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/card/notification/b;

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/b;->a:Lcom/sec/spp/push/notisvc/card/notification/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/b;->b:Lcom/sec/spp/push/notisvc/card/notification/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/notisvc/card/notification/b;->c:Lcom/sec/spp/push/notisvc/card/notification/b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->e:[Lcom/sec/spp/push/notisvc/card/notification/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/card/notification/b;->d:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/card/notification/b;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/notification/b;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/card/notification/b;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/card/notification/b;->e:[Lcom/sec/spp/push/notisvc/card/notification/b;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/card/notification/b;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
