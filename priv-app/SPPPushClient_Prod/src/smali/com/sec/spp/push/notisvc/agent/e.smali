.class Lcom/sec/spp/push/notisvc/agent/e;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/agent/d;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/sec/spp/push/notisvc/agent/h;


# direct methods
.method private constructor <init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/agent/e;->a:Lcom/sec/spp/push/notisvc/agent/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/agent/e;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/agent/e;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/spp/push/notisvc/agent/e;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;Lcom/sec/spp/push/notisvc/agent/e;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/sec/spp/push/notisvc/agent/e;-><init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_1

    const-string v0, "fileDown thread : Timeout!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Ljava/net/MalformedURLException;

    if-eqz v0, :cond_2

    const-string v0, "fileDown thread : MalformedURLException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_0

    const-string v0, "fileDown thread : IOException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3ee

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->a:Lcom/sec/spp/push/notisvc/agent/d;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/e;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/agent/d;->a(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "NETWORK_NOT_AVAILABLE"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    const-string v0, "fileDown thread starts."

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/e;->c:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v4, p0, Lcom/sec/spp/push/notisvc/agent/e;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/e;->c:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const-string v2, "GET"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const v2, 0x1d4c0

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "urlConnection encoding : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x400

    new-array v5, v2, [B

    new-instance v2, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/spp/push/notisvc/agent/e;->d:Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_2
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v2

    if-gez v2, :cond_3

    const v2, 0x19000

    :cond_3
    iget-object v6, p0, Lcom/sec/spp/push/notisvc/agent/e;->a:Lcom/sec/spp/push/notisvc/agent/d;

    iget-object v7, p0, Lcom/sec/spp/push/notisvc/agent/e;->d:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/spp/push/notisvc/agent/d;->a(Lcom/sec/spp/push/notisvc/agent/d;Ljava/lang/String;)J

    move-result-wide v6

    long-to-double v6, v6

    int-to-double v8, v2

    const-wide/high16 v10, 0x4004000000000000L    # 2.5

    mul-double/2addr v8, v10

    cmpg-double v2, v6, v8

    if-gtz v2, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Device has not enough memory. : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/e;->a:Lcom/sec/spp/push/notisvc/agent/d;

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/agent/e;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/notisvc/agent/d;->a(Lcom/sec/spp/push/notisvc/agent/d;Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v2, 0x3ec

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/agent/e;->a(Ljava/lang/Exception;)V

    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "FileDown. run(). cannot close fileOutput."

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    :try_start_2
    new-instance v6, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/sec/spp/push/notisvc/agent/e;->d:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    move v1, v3

    :goto_2
    invoke-virtual {v4, v5}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    if-nez v1, :cond_7

    const-string v1, "fileDown thread is finish."

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    :goto_3
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_6
    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_2

    :cond_7
    const-string v1, "fileDown thread is canceled."

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/e;->e:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v3, 0x3e9

    invoke-virtual {v1, v3}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3
.end method
