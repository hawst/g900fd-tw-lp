.class public Lcom/sec/spp/push/notisvc/card/k;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/util/Map;

.field private static c:Lcom/sec/spp/push/notisvc/card/k;

.field private static final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/spp/push/notisvc/card/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    sput-object v1, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    sput-object v1, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/k;->d:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    return-void
.end method

.method public static a()Lcom/sec/spp/push/notisvc/card/k;
    .locals 2

    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    if-nez v0, :cond_1

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/notisvc/card/k;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/card/k;-><init>()V

    sput-object v0, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->c:Lcom/sec/spp/push/notisvc/card/k;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/card/a;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/card/a;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/sec/spp/push/notisvc/card/a;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "UnsupportedOperationException. cannot put card."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "IllegalArgumentException. cannot put card."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "ClassCastException. cannot put card."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "UnsupportedOprerationException. cannot remove all cards."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/card/k;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "UnsupportedOprerationException. cannot remove card."

    sget-object v1, Lcom/sec/spp/push/notisvc/card/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
