.class public final enum Lcom/sec/spp/push/dlc/sender/g;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/dlc/sender/g;

.field public static final enum b:Lcom/sec/spp/push/dlc/sender/g;

.field public static final enum c:Lcom/sec/spp/push/dlc/sender/g;

.field public static final enum d:Lcom/sec/spp/push/dlc/sender/g;

.field public static final enum e:Lcom/sec/spp/push/dlc/sender/g;

.field private static final synthetic f:[Lcom/sec/spp/push/dlc/sender/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/spp/push/dlc/sender/g;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->a:Lcom/sec/spp/push/dlc/sender/g;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/g;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/dlc/sender/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->b:Lcom/sec/spp/push/dlc/sender/g;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/g;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/dlc/sender/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->c:Lcom/sec/spp/push/dlc/sender/g;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/g;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/spp/push/dlc/sender/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->d:Lcom/sec/spp/push/dlc/sender/g;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/g;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/sec/spp/push/dlc/sender/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->e:Lcom/sec/spp/push/dlc/sender/g;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/spp/push/dlc/sender/g;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->a:Lcom/sec/spp/push/dlc/sender/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->b:Lcom/sec/spp/push/dlc/sender/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->c:Lcom/sec/spp/push/dlc/sender/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->d:Lcom/sec/spp/push/dlc/sender/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/dlc/sender/g;->e:Lcom/sec/spp/push/dlc/sender/g;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/spp/push/dlc/sender/g;->f:[Lcom/sec/spp/push/dlc/sender/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/g;
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/sender/g;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/dlc/sender/g;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/dlc/sender/g;->f:[Lcom/sec/spp/push/dlc/sender/g;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/dlc/sender/g;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
