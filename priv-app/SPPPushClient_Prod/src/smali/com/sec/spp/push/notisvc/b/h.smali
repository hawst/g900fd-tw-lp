.class public abstract Lcom/sec/spp/push/notisvc/b/h;
.super Landroid/widget/LinearLayout;


# instance fields
.field final synthetic b:Lcom/sec/spp/push/notisvc/b/c;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/notisvc/b/c;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/b/h;->b:Lcom/sec/spp/push/notisvc/b/c;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Ljava/lang/String;)I
.end method

.method public abstract a()Lcom/sec/spp/push/notisvc/b/j;
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "destroyView. context is null"

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public abstract a(Landroid/content/Context;Ljava/util/Map;)V
.end method

.method public abstract b(Landroid/content/Context;Ljava/lang/String;)I
.end method
