.class public Lcom/sec/spp/push/notisvc/d/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Z

.field private static b:Z

.field private static c:Z

.field private static d:Z

.field private static e:Z

.field private static f:Z

.field private static g:Ljava/io/File;

.field private static h:Ljava/io/FileOutputStream;

.field private static i:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/spp/push/notisvc/d/a;->a:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->b:Z

    sput-boolean v1, Lcom/sec/spp/push/notisvc/d/a;->c:Z

    sput-boolean v1, Lcom/sec/spp/push/notisvc/d/a;->d:Z

    sput-boolean v1, Lcom/sec/spp/push/notisvc/d/a;->e:Z

    sput-boolean v1, Lcom/sec/spp/push/notisvc/d/a;->f:Z

    sput-object v2, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    sput-object v2, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    sput v1, Lcom/sec/spp/push/notisvc/d/a;->i:I

    return-void
.end method

.method public static a()V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "notiLog"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/notisvc/d/a;->i:I

    rem-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    sput-object v2, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v1, "LNoti"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const-string v1, "LNoti"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v1, "LNoti"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_3
    const-string v1, "LNoti"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_4
    const-string v1, "LNoti"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x1

    sget-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->b:Z

    if-eqz v0, :cond_0

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->b(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(ZZZZZ)V
    .locals 0

    sput-boolean p0, Lcom/sec/spp/push/notisvc/d/a;->b:Z

    sput-boolean p1, Lcom/sec/spp/push/notisvc/d/a;->c:Z

    sput-boolean p2, Lcom/sec/spp/push/notisvc/d/a;->d:Z

    sput-boolean p3, Lcom/sec/spp/push/notisvc/d/a;->e:Z

    sput-boolean p4, Lcom/sec/spp/push/notisvc/d/a;->f:Z

    return-void
.end method

.method private static a([B)V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0x500000

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    invoke-static {}, Lcom/sec/spp/push/notisvc/d/a;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    invoke-virtual {v0, p0}, Ljava/io/FileOutputStream;->write([B)V

    sget-object v0, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private static b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    const/16 v4, 0x20

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "MM/dd/yy hh:mm:ss"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    packed-switch p0, :pswitch_data_0

    const/16 v0, 0x56

    :goto_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/d/a;->a([B)V

    return-void

    :pswitch_0
    const/16 v0, 0x45

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x44

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x49

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x57

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x4

    sget-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->c:Z

    if-eqz v0, :cond_0

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->b(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static b()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget v2, Lcom/sec/spp/push/notisvc/d/a;->i:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/spp/push/notisvc/d/a;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "notiLog"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/sec/spp/push/notisvc/d/a;->i:I

    rem-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".txt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    sget-object v2, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v5, 0x500000

    cmp-long v2, v2, v5

    if-lez v2, :cond_1

    new-instance v2, Ljava/io/FileOutputStream;

    sget-object v3, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v2, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;

    :goto_1
    sput-object v4, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/io/FileOutputStream;

    sget-object v3, Lcom/sec/spp/push/notisvc/d/a;->g:Ljava/io/File;

    const/4 v5, 0x1

    invoke-direct {v2, v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v2, Lcom/sec/spp/push/notisvc/d/a;->h:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x3

    sget-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->d:Z

    if-eqz v0, :cond_0

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->b(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x2

    sget-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->e:Z

    if-eqz v0, :cond_0

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->b(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x5

    sget-boolean v0, Lcom/sec/spp/push/notisvc/d/a;->f:Z

    if-eqz v0, :cond_0

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, p0, p1}, Lcom/sec/spp/push/notisvc/d/a;->b(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
