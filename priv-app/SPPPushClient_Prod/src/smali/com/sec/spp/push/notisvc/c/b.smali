.class public final enum Lcom/sec/spp/push/notisvc/c/b;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum b:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum c:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum d:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum e:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum f:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum g:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum h:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum i:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum j:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum k:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum l:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum m:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum n:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum o:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum p:Lcom/sec/spp/push/notisvc/c/b;

.field public static final enum q:Lcom/sec/spp/push/notisvc/c/b;

.field private static final synthetic s:[Lcom/sec/spp/push/notisvc/c/b;


# instance fields
.field private final r:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "DELIVER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->a:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "DOWNLOADED"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->b:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "CONSUMED"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->c:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "CLICKED"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->d:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "WRONG_META_DATA"

    const/16 v2, -0x18

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->e:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "NOT_EXIST_SERVICE"

    const/4 v2, 0x5

    const/16 v3, -0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->f:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "UNKNOWN_AGREE"

    const/4 v2, 0x6

    const/16 v3, -0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->g:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "DISAGREE"

    const/4 v2, 0x7

    const/16 v3, -0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->h:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "DEREGISTERED"

    const/16 v2, 0x8

    const/16 v3, -0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->i:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "UNSUPPORTED_CARD_TYPE"

    const/16 v2, 0x9

    const/16 v3, -0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->j:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "UNSUPPORTED_CARD_UNDER_JELLYBEAN"

    const/16 v2, 0xa

    const/16 v3, -0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->k:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "DOWNLOAD_FAIL"

    const/16 v2, 0xb

    const/16 v3, -0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->l:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "CONTENTS_FILE_ERROR"

    const/16 v2, 0xc

    const/16 v3, -0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->m:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "CONSUME_FAIL"

    const/16 v2, 0xd

    const/16 v3, -0x3c

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->n:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "NONE_REACTION"

    const/16 v2, 0xe

    const/16 v3, -0x46

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->o:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "IGNORED"

    const/16 v2, 0xf

    const/16 v3, -0x50

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->p:Lcom/sec/spp/push/notisvc/c/b;

    new-instance v0, Lcom/sec/spp/push/notisvc/c/b;

    const-string v1, "CLIENT_INTERNAL_ERROR"

    const/16 v2, 0x10

    const/16 v3, -0x78

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/c/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->q:Lcom/sec/spp/push/notisvc/c/b;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/c/b;

    sget-object v1, Lcom/sec/spp/push/notisvc/c/b;->a:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/notisvc/c/b;->b:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/notisvc/c/b;->c:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/notisvc/c/b;->d:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/spp/push/notisvc/c/b;->e:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->f:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->g:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->h:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->i:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->j:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->k:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->l:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->m:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->n:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->o:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->p:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/spp/push/notisvc/c/b;->q:Lcom/sec/spp/push/notisvc/c/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/notisvc/c/b;->s:[Lcom/sec/spp/push/notisvc/c/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/c/b;->r:I

    return-void
.end method

.method public static a(I)Lcom/sec/spp/push/notisvc/c/b;
    .locals 3

    const-class v0, Lcom/sec/spp/push/notisvc/c/b;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/c/b;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/c/b;->a()I

    move-result v2

    if-ne v2, p0, :cond_0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/c/b;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/c/b;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/c/b;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/c/b;->s:[Lcom/sec/spp/push/notisvc/c/b;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/c/b;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/notisvc/c/b;->r:I

    return v0
.end method
