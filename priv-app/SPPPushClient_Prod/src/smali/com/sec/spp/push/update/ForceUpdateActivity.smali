.class public Lcom/sec/spp/push/update/ForceUpdateActivity;
.super Landroid/app/Activity;


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/c;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/c;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/update/ForceUpdateActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/d;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/d;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method static synthetic b(Lcom/sec/spp/push/update/ForceUpdateActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/e;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/e;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method static synthetic c(Lcom/sec/spp/push/update/ForceUpdateActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->j:Landroid/widget/EditText;

    return-object v0
.end method

.method private d()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/f;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/f;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method private e()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/g;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/g;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method private f()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/h;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/h;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method

.method private g()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/i;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/update/i;-><init>(Lcom/sec/spp/push/update/ForceUpdateActivity;)V

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->setContentView(I)V

    const/high16 v0, 0x7f090000

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->a:Landroid/widget/Button;

    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->b:Landroid/widget/Button;

    const v0, 0x7f090003

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->c:Landroid/widget/Button;

    const v0, 0x7f090004

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->d:Landroid/widget/Button;

    const v0, 0x7f090002

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->e:Landroid/widget/Button;

    const v0, 0x7f090006

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->f:Landroid/widget/Button;

    const v0, 0x7f090008

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->g:Landroid/widget/Button;

    const v0, 0x7f090005

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->i:Landroid/widget/EditText;

    const v0, 0x7f090007

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->j:Landroid/widget/EditText;

    const v0, 0x7f090009

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->a:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->a()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->b:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->c:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->c()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->d:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->d()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->e:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->e()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/update/ForceUpdateActivity;->g:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/update/ForceUpdateActivity;->g()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
