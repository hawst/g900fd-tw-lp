.class public Lcom/sec/spp/push/dlc/sender/h;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    new-instance v0, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;-><init>()V

    sput-object v0, Lcom/sec/spp/push/dlc/sender/h;->b:Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljavax/net/ssl/HttpsURLConnection;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;)Ljavax/net/ssl/HttpsURLConnection;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljavax/net/ssl/HttpsURLConnection;->setUseCaches(Z)V

    invoke-virtual {p0, p1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0, p2}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    invoke-virtual {p0, p3}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    if-eqz p4, :cond_0

    const-string v0, "Content-Type"

    invoke-virtual {p0, v0, p4}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p5, :cond_1

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return-object p0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljavax/net/ssl/HttpsURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Header values [ "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/net/URL;Lcom/sec/spp/push/dlc/sender/g;Lcom/sec/spp/push/dlc/sender/ao;Lcom/sec/spp/push/dlc/sender/p;IILjava/lang/String;Ljava/util/List;Lcom/sec/spp/push/dlc/sender/d;)V
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    const-string v0, "BKS"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    const-string v2, "weblog1234!@#$"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->b:Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/KeyManagementException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/sender/g;->toString()Ljava/lang/String;

    move-result-object v1

    move v2, p4

    move v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-static/range {v0 .. v5}, Lcom/sec/spp/push/dlc/sender/h;->a(Ljavax/net/ssl/HttpsURLConnection;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;)Ljavax/net/ssl/HttpsURLConnection;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    if-eqz p2, :cond_0

    :try_start_2
    invoke-virtual {p2, v0}, Lcom/sec/spp/push/dlc/sender/ao;->a(Ljavax/net/ssl/HttpsURLConnection;)J
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p2}, Lcom/sec/spp/push/dlc/sender/ao;->a()V

    :cond_0
    :try_start_3
    invoke-virtual {p3, v0}, Lcom/sec/spp/push/dlc/sender/p;->a(Ljavax/net/ssl/HttpsURLConnection;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    invoke-virtual {p3}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    :goto_0
    const/16 v2, 0x1a1

    :try_start_4
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HTTP Response Code ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "HTTP Response ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "HTTP Response ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b

    :goto_1
    sparse-switch v2, :sswitch_data_0

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_1
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_2
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_3
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_4
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_5
    move-exception v1

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto :goto_2

    :catch_6
    move-exception v1

    :try_start_5
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    const-string v1, "Write Timeout occurs"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Write Timeout occurs"

    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-virtual {p2}, Lcom/sec/spp/push/dlc/sender/ao;->a()V

    goto/16 :goto_2

    :catch_7
    move-exception v1

    :try_start_6
    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-virtual {p2}, Lcom/sec/spp/push/dlc/sender/ao;->a()V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lcom/sec/spp/push/dlc/sender/ao;->a()V

    throw v0

    :catch_8
    move-exception v1

    :try_start_7
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v1

    invoke-virtual {p8, v1}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v1

    const-string v2, "Read Timeout occurs"

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Read Timeout occurs"

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p8, v1}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    invoke-virtual {p3}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    goto/16 :goto_2

    :catch_9
    move-exception v1

    :try_start_8
    sget-object v2, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/spp/push/dlc/sender/a;

    invoke-direct {v2}, Lcom/sec/spp/push/dlc/sender/a;-><init>()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/sender/p;->a(Ljavax/net/ssl/HttpsURLConnection;)Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v1

    :try_start_a
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/p;->a()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :goto_3
    invoke-virtual {p3}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    goto/16 :goto_0

    :catch_a
    move-exception v1

    :try_start_b
    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    move-object v1, v6

    goto :goto_3

    :catchall_1
    move-exception v1

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    throw v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catchall_2
    move-exception v1

    invoke-virtual {p3}, Lcom/sec/spp/push/dlc/sender/p;->a()V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    throw v1

    :catch_b
    move-exception v0

    sget-object v3, Lcom/sec/spp/push/dlc/sender/h;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_0
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    :sswitch_1
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    :sswitch_2
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    :sswitch_3
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->ordinal()I

    move-result v0

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(I)Landroid/os/Message;

    move-result-object v0

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p8, v0}, Lcom/sec/spp/push/dlc/sender/d;->a(Landroid/os/Message;)V

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xcc -> :sswitch_1
        0x190 -> :sswitch_3
        0x194 -> :sswitch_2
    .end sparse-switch
.end method
