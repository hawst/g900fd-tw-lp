.class public Lcom/sec/spp/push/dlc/sender/s;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/sender/s;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/dlc/sender/s;->b:Ljava/lang/String;

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/s;->c:[Ljava/lang/String;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 5

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "cv"

    invoke-static {}, Lcom/sec/spp/push/dlc/util/b;->b()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "di"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/s;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/s;->c:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    const-string v0, "pkg"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/s;->c:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I
    .locals 4

    const/4 v0, -0x1

    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/s;->a()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/spp/push/dlc/sender/t;

    invoke-direct {v3, p0, v0, v1, p2}, Lcom/sec/spp/push/dlc/sender/t;-><init>(Lcom/sec/spp/push/dlc/sender/s;Ljava/lang/String;Ljava/net/URL;Lcom/sec/spp/push/dlc/sender/d;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
