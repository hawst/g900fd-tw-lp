.class Lcom/sec/spp/push/dlc/sender/y;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/u;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "PvSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->g(Lcom/sec/spp/push/dlc/sender/SenderService;)Lcom/sec/spp/push/dlc/sender/n;

    move-result-object v0

    const-string v1, "https://ew1.dms-gld.bigdata.ssp.samsung.com:80/1.0/provision"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/n;->a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->a(Landroid/os/Message;)V

    return-void
.end method

.method public b(Landroid/os/Message;)V
    .locals 6

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/dlc/sender/u;->c(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/m;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "pvReply is null"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/m;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Result Code is NULL"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/an;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/an;

    move-result-object v3

    if-nez v3, :cond_3

    const-string v0, "rcode is null"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v3, v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v3, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    sget-object v0, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v3, v0, :cond_5

    sget-object v0, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v3, v0, :cond_8

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/m;->b()I

    move-result v4

    iget-object v5, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v5}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/spp/push/dlc/util/f;->B(Landroid/content/Context;)I

    move-result v5

    if-eq v4, v5, :cond_6

    move v0, v1

    :cond_6
    sget-object v4, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v3}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/m;)V

    :cond_7
    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v3}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/spp/push/dlc/util/q;->a(Landroid/content/Context;)V

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "current qv="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->B(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pvReply qv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/m;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->f(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Lcom/sec/spp/push/dlc/util/e;->a()I

    move-result v0

    if-ne v0, v1, :cond_9

    const-string v0, "Current NetType is Wifi TransTo [SendAll St]"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->d(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/y;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->e(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0
.end method
