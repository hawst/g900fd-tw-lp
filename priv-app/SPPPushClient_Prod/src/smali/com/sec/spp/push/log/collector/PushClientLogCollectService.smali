.class public Lcom/sec/spp/push/log/collector/PushClientLogCollectService;
.super Landroid/app/IntentService;


# static fields
.field private static a:Z

.field private static synthetic d:[I


# instance fields
.field private b:Lcom/sec/spp/push/dlc/a/a;

.field private final c:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "PushClientLogCollectService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/log/collector/e;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/log/collector/e;-><init>(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V

    iput-object v0, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c:Landroid/content/ServiceConnection;

    return-void
.end method

.method private a(Lcom/sec/spp/push/log/collector/a;I)J
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/a;->d()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/a;->c()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/a;->e()J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Lcom/sec/spp/push/log/collector/b;I)J
    .locals 3

    const-wide/16 v0, 0x0

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/b;->d()J

    move-result-wide v0

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/b;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method private a(Lcom/sec/spp/push/log/collector/c;J)Lcom/sec/spp/push/log/collector/c;
    .locals 5

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/c;->e()J

    move-result-wide v0

    sub-long v0, p2, v0

    long-to-int v0, v0

    if-lez v0, :cond_0

    int-to-long v1, v0

    const-wide/32 v3, 0x186a5

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    :cond_0
    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "calcPingRouteTime invalid gap : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/c;->d()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    invoke-virtual {p1, v1, v2}, Lcom/sec/spp/push/log/collector/c;->a(J)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/c;->b()I

    move-result v1

    if-eqz v1, :cond_3

    if-ge v0, v1, :cond_4

    :cond_3
    invoke-virtual {p1, v0}, Lcom/sec/spp/push/log/collector/c;->b(I)V

    :cond_4
    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/c;->c()I

    move-result v1

    if-le v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/log/collector/c;->c(I)V

    goto :goto_0
.end method

.method private a(Lcom/sec/spp/push/b/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lcom/sec/spp/push/b/c;->h()Lcom/sec/spp/push/log/logsender/a;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/spp/push/log/logsender/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;)V
    .locals 3

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;I)V
    .locals 3

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "netType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IIJ)V
    .locals 3

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lastNetType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", newNetType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", curTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IJ)V
    .locals 3

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;Landroid/os/Messenger;)V
    .locals 3

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private declared-synchronized a(Landroid/content/Intent;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/util/g;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Not Owner"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->as:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/b/c;->g()Lcom/sec/spp/push/log/collector/a;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/spp/push/b/c;->f()Lcom/sec/spp/push/log/collector/b;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/spp/push/b/c;->h()Lcom/sec/spp/push/log/logsender/a;

    move-result-object v5

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/sec/spp/push/log/collector/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/sec/spp/push/log/collector/b;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/sec/spp/push/log/logsender/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v2}, Lcom/sec/spp/push/b/c;->b()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/log/collector/d;->o:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v3}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "string"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/sec/spp/push/b/c;)V
    .locals 3

    const/4 v0, -0x1

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/spp/push/b/c;->b(IJ)V

    return-void
.end method

.method private a(Lcom/sec/spp/push/b/c;I)V
    .locals 3

    if-nez p2, :cond_0

    const-string v0, "mo_pushRetryCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string v0, "wifi_pushRetryCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    goto :goto_0

    :cond_1
    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "increaseReconnCount invalid type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/sec/spp/push/b/c;IJ)V
    .locals 0

    invoke-virtual {p1, p2, p3, p4}, Lcom/sec/spp/push/b/c;->a(IJ)V

    return-void
.end method

.method private a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/a;J)V
    .locals 6

    const-wide/16 v4, 0x0

    if-nez p2, :cond_0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "ConnectionLogData is null "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/sec/spp/push/log/collector/a;->a()I

    move-result v0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fail Calculate Connect Duration - NA type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Lcom/sec/spp/push/log/collector/a;->b()J

    move-result-wide v1

    cmp-long v3, v1, v4

    if-gtz v3, :cond_2

    const-string v0, "[SPPLogCollecter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fail Calculate Connect Duration - invalid startTime : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sub-long v1, p3, v1

    cmp-long v3, v1, v4

    if-gtz v3, :cond_3

    const-string v0, "[SPPLogCollecter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fail Calculate Connect Duration - invalid gap : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p2, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/a;I)J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c(Lcom/sec/spp/push/b/c;IJ)V

    goto :goto_0
.end method

.method private a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/b;J)V
    .locals 5

    invoke-virtual {p2}, Lcom/sec/spp/push/log/collector/b;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "calcDisconnDuration invalid type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lcom/sec/spp/push/log/collector/b;->a()J

    move-result-wide v1

    sub-long v1, p3, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gtz v3, :cond_1

    const-string v0, "[SPPLogCollecter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "calcDisconnDuration invalid gap : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/b;I)J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;IJ)V

    invoke-direct {p0, p1, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c(Lcom/sec/spp/push/b/c;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->h()V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;Lcom/sec/spp/push/dlc/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b:Lcom/sec/spp/push/dlc/a/a;

    return-void
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a:Z

    return-void
.end method

.method private a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method static synthetic a()[I
    .locals 3

    sget-object v0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/log/collector/d;->values()[Lcom/sec/spp/push/log/collector/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->p:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->c:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->a:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->b:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->o:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->g:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->f:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->e:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->d:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->i:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->n:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->h:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->q:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_11
    sput-object v0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_11

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto :goto_e

    :catch_4
    move-exception v1

    goto :goto_d

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v1

    goto :goto_b

    :catch_7
    move-exception v1

    goto :goto_a

    :catch_8
    move-exception v1

    goto :goto_9

    :catch_9
    move-exception v1

    goto :goto_8

    :catch_a
    move-exception v1

    goto :goto_7

    :catch_b
    move-exception v1

    goto/16 :goto_6

    :catch_c
    move-exception v1

    goto/16 :goto_5

    :catch_d
    move-exception v1

    goto/16 :goto_4

    :catch_e
    move-exception v1

    goto/16 :goto_3

    :catch_f
    move-exception v1

    goto/16 :goto_2

    :catch_10
    move-exception v1

    goto/16 :goto_1
.end method

.method private declared-synchronized b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d()V

    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->g()Lcom/sec/spp/push/log/collector/a;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/a;J)V

    invoke-direct {p0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V

    const-string v0, "[SPPLogCollecter]"

    const-string v2, "Reset All Connection Data"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_1
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 5

    const-string v0, "com.sec.spp.push.REQUEST_REGISTER"

    const-string v0, "EXTRA_PACKAGENAME"

    const-string v0, "EXTRA_INTENTFILTER"

    const-string v0, "com.sec.spp.push.DLC_REPLY"

    invoke-static {}, Lcom/sec/spp/push/util/g;->k()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Not Owner"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Already DLC registered"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.sec.spp.push.dlc.util.DlcRegiReceiver"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "com.sec.spp.push.REQUEST_REGISTER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "EXTRA_PACKAGENAME"

    const-string v3, "com.sec.spp.push"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "EXTRA_INTENTFILTER"

    const-string v3, "com.sec.spp.push.DLC_REPLY"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "[SPPLogCollecter]"

    const-string v3, "Request register to DLC"

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a:Z

    goto :goto_0
.end method

.method private b(Lcom/sec/spp/push/b/c;)V
    .locals 3

    const/4 v0, -0x1

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/spp/push/b/c;->a(IJ)V

    return-void
.end method

.method private b(Lcom/sec/spp/push/b/c;I)V
    .locals 1

    if-nez p2, :cond_0

    const-string v0, "mo_ConnCnt"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string v0, "wifi_ConnCnt"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    goto :goto_0

    :cond_1
    const-string v0, "bt_ConnCnt"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    goto :goto_0
.end method

.method private b(Lcom/sec/spp/push/b/c;IJ)V
    .locals 1

    if-nez p2, :cond_1

    invoke-virtual {p1, p3, p4}, Lcom/sec/spp/push/b/c;->b(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p1, p3, p4}, Lcom/sec/spp/push/b/c;->c(J)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->i()V

    return-void
.end method

.method static synthetic c(Lcom/sec/spp/push/log/collector/PushClientLogCollectService;)Landroid/content/ServiceConnection;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private declared-synchronized c()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->E()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d()V

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V

    invoke-direct {p0, v1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V

    const-string v0, "[SPPLogCollecter]"

    const-string v2, "Reset All Connection Data"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private c(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method private c(Lcom/sec/spp/push/b/c;I)V
    .locals 1

    if-nez p2, :cond_1

    const-string v0, "mobileDisconnCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const-string v0, "wifiDisconnCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    goto :goto_0
.end method

.method private c(Lcom/sec/spp/push/b/c;IJ)V
    .locals 1

    if-nez p2, :cond_0

    invoke-virtual {p1, p3, p4}, Lcom/sec/spp/push/b/c;->d(J)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    invoke-virtual {p1, p3, p4}, Lcom/sec/spp/push/b/c;->e(J)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p3, p4}, Lcom/sec/spp/push/b/c;->f(J)V

    goto :goto_0
.end method

.method private d()V
    .locals 11

    const-wide/32 v9, 0x5265c00

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->E()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v4, v2, v0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v6

    const-wide/16 v7, 0x0

    cmp-long v7, v4, v7

    if-lez v7, :cond_0

    cmp-long v7, v4, v9

    if-lez v7, :cond_1

    :cond_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->n:Lcom/sec/spp/push/log/collector/d;

    invoke-static {v6, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;)V

    invoke-static {v6}, Lcom/sec/spp/push/receiver/OneDayCheckTimer;->a(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_1
    sub-long v4, v9, v4

    const-string v7, "[SPPLogCollecter]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "CurTime : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SentTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", NextTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6, v4, v5}, Lcom/sec/spp/push/receiver/OneDayCheckTimer;->a(Landroid/content/Context;J)V

    goto :goto_0
.end method

.method private declared-synchronized d(Landroid/content/Intent;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid net type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-nez v0, :cond_2

    :try_start_2
    const-string v0, "mo_timeoutCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_4
    const-string v0, "wifi_timeoutCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private d(Lcom/sec/spp/push/b/c;I)V
    .locals 1

    if-nez p2, :cond_0

    const-string v0, "mo_pingCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    :goto_0
    return-void

    :cond_0
    const-string v0, "wifi_pingCount"

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J

    goto :goto_0
.end method

.method private d(Lcom/sec/spp/push/b/c;IJ)V
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V

    const-string v0, "[SPPLogCollecter]"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDisconnectData NA type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2, p3, p4}, Lcom/sec/spp/push/b/c;->b(IJ)V

    goto :goto_0
.end method

.method private e()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->g()V

    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->f()V

    return-void
.end method

.method private declared-synchronized e(Landroid/content/Intent;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid net type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-nez v0, :cond_2

    :try_start_2
    const-string v0, "mo_crpCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_4
    const-string v0, "wifi_crpCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private e(Lcom/sec/spp/push/b/c;IJ)V
    .locals 2

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/b/c;->d()Lcom/sec/spp/push/log/collector/c;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Mobile Data is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/c;J)Lcom/sec/spp/push/log/collector/c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->a(Lcom/sec/spp/push/log/collector/c;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/sec/spp/push/b/c;->e()Lcom/sec/spp/push/log/collector/c;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Wifi Data is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0, p3, p4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/log/collector/c;J)Lcom/sec/spp/push/log/collector/c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/spp/push/b/c;->b(Lcom/sec/spp/push/log/collector/c;)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    const-string v0, "com.sec.spp.push"

    const-string v0, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private declared-synchronized f(Landroid/content/Intent;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid net type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-nez v0, :cond_2

    :try_start_2
    const-string v0, "mo_nnrCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_4
    const-string v0, "wifi_nnrCount"

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/b/c;->a(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v1, :cond_0

    :try_start_6
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private declared-synchronized g()V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, -0x1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->g()Lcom/sec/spp/push/log/collector/a;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/a;->a()I

    move-result v4

    if-eq v4, v7, :cond_0

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/a;->b()J

    move-result-wide v5

    cmp-long v5, v5, v8

    if-lez v5, :cond_0

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;IJ)V

    :cond_0
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/a;J)V

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->f()Lcom/sec/spp/push/log/collector/b;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/b;->b()I

    move-result v4

    if-eq v4, v7, :cond_2

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/b;->a()J

    move-result-wide v5

    cmp-long v5, v5, v8

    if-lez v5, :cond_2

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d(Lcom/sec/spp/push/b/c;IJ)V

    :cond_2
    invoke-direct {p0, v1, v0, v2, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/b;J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz v1, :cond_4

    :try_start_2
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_5

    :try_start_5
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private declared-synchronized g(Landroid/content/Intent;)V
    .locals 5

    const-wide/16 v3, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v3

    if-gtz v2, :cond_0

    const-string v2, "[SPPLogCollecter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid time value : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/spp/push/b/c;->a(J)V

    invoke-virtual {v2}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 11

    monitor-enter p0

    :try_start_0
    const-string v0, "BIZ"

    const-string v0, "028"

    const-string v0, "9999:8100"

    invoke-static {p0}, Lcom/sec/spp/push/util/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "device Id is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v8, "99.01.00"

    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b:Lcom/sec/spp/push/dlc/a/a;

    if-nez v0, :cond_2

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "mService is null. cannot send Log."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v10, :cond_1

    :try_start_2
    invoke-virtual {v10}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_3
    invoke-direct {p0, v10, v6}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_3

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "body is null. cannot send Log."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v10, :cond_1

    :try_start_4
    invoke-virtual {v10}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_5
    iget-object v0, p0, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b:Lcom/sec/spp/push/dlc/a/a;

    const-string v1, "BIZ"

    const-string v2, "028"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, "9999:8100"

    const/4 v7, 0x0

    invoke-interface/range {v0 .. v9}, Lcom/sec/spp/push/dlc/a/a;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sent log. resultCode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v10, :cond_1

    :try_start_6
    invoke-virtual {v10}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v10, :cond_1

    :try_start_8
    invoke-virtual {v10}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v10, :cond_4

    invoke-virtual {v10}, Lcom/sec/spp/push/b/c;->b()V

    :cond_4
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method private declared-synchronized h(Landroid/content/Intent;)V
    .locals 6

    const-wide/16 v4, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    if-eqz v0, :cond_1

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    const-string v1, "[SPPLogCollecter]"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid net type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    cmp-long v3, v1, v4

    if-gtz v3, :cond_2

    :try_start_1
    const-string v0, "[SPPLogCollecter]"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid time value : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    :try_start_3
    invoke-direct {p0, v3, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d(Lcom/sec/spp/push/b/c;I)V

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->e(Lcom/sec/spp/push/b/c;IJ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v3, :cond_0

    :try_start_6
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private declared-synchronized i()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Lcom/sec/spp/push/b/c;->b()V

    :cond_1
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private declared-synchronized i(Landroid/content/Intent;)V
    .locals 6

    const-wide/16 v4, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    cmp-long v4, v1, v4

    if-gtz v4, :cond_1

    :try_start_1
    const-string v0, "[SPPLogCollecter]"

    const-string v1, "handleActionConnectionTry Invalid value"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_3
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->g()Lcom/sec/spp/push/log/collector/a;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/a;J)V

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->f()Lcom/sec/spp/push/log/collector/b;

    move-result-object v4

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d(Lcom/sec/spp/push/b/c;IJ)V

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/b;J)V

    invoke-direct {p0, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "handleActionConnectionTry Invalid type"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_5
    invoke-virtual {v4}, Lcom/sec/spp/push/log/collector/b;->b()I

    move-result v1

    if-ne v1, v0, :cond_3

    invoke-direct {p0, v3, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_3
    if-eqz v3, :cond_0

    :try_start_6
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v3, :cond_0

    :try_start_8
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    :cond_4
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method private declared-synchronized j(Landroid/content/Intent;)V
    .locals 8

    const-wide/16 v4, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    cmp-long v4, v1, v4

    if-gtz v4, :cond_1

    :try_start_1
    const-string v0, "[SPPLogCollecter]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid time value : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_3
    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;IJ)V

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->f()Lcom/sec/spp/push/log/collector/b;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "Disconnect Data is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_5
    invoke-direct {p0, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(I)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "[SPPLogCollecter]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "handleActionConnected Invalid net type : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/b;J)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v3, :cond_0

    :try_start_6
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v3, :cond_0

    :try_start_8
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :cond_3
    :try_start_9
    invoke-direct {p0, v3, v0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;I)V

    invoke-direct {p0, v3, v4, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/b;J)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v3, :cond_0

    :try_start_a
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_b
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v3, :cond_0

    :try_start_c
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    :cond_4
    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0
.end method

.method private declared-synchronized k(Landroid/content/Intent;)V
    .locals 7

    const-wide/16 v5, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    sget-object v0, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->name()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-static {}, Lcom/sec/spp/push/b/c;->a()Lcom/sec/spp/push/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :try_start_1
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->g()Lcom/sec/spp/push/log/collector/a;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v0, "[SPPLogCollecter]"

    const-string v1, "handleActionDisconnect. data is null."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_3
    invoke-direct {p0, v3}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Lcom/sec/spp/push/b/c;)V

    cmp-long v5, v1, v5

    if-gtz v5, :cond_2

    const-string v0, "[SPPLogCollecter]"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[Disconnect] Invalid time value : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "[SPPLogCollecter]"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v3, :cond_0

    :try_start_5
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_6
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Lcom/sec/spp/push/b/c;Lcom/sec/spp/push/log/collector/a;J)V

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d(Lcom/sec/spp/push/b/c;IJ)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v3, :cond_0

    :try_start_7
    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    goto :goto_1

    :catchall_1
    move-exception v0

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/sec/spp/push/b/c;->b()V

    :cond_3
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/log/collector/d;->valueOf(Ljava/lang/String;)Lcom/sec/spp/push/log/collector/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->i(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->k(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->j(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->d(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->e(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->f(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->g(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_9
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->h(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_a
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_b
    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->e()V

    goto :goto_0

    :pswitch_c
    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->c()V

    goto :goto_0

    :pswitch_d
    invoke-direct {p0}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->b()V

    goto :goto_0

    :pswitch_e
    invoke-direct {p0, p1}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
