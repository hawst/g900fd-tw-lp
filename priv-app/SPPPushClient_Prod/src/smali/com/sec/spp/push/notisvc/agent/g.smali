.class Lcom/sec/spp/push/notisvc/agent/g;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/agent/d;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/sec/spp/push/notisvc/agent/h;


# direct methods
.method private constructor <init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/agent/g;->a:Lcom/sec/spp/push/notisvc/agent/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/agent/g;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/agent/g;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/spp/push/notisvc/agent/g;->d:Ljava/util/Map;

    iput-object p5, p0, Lcom/sec/spp/push/notisvc/agent/g;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    iput-object p6, p0, Lcom/sec/spp/push/notisvc/agent/g;->f:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;Lcom/sec/spp/push/notisvc/agent/g;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/sec/spp/push/notisvc/agent/g;-><init>(Lcom/sec/spp/push/notisvc/agent/d;Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/notisvc/agent/h;)V

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_1

    const-string v0, "HTTPs Transmit thread : Timeout!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Ljava/net/MalformedURLException;

    if-eqz v0, :cond_2

    const-string v0, "HTTPs Transmit thread : MalformedURLException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_3

    const-string v0, "HTTPs Transmit thread : IOException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3ee

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Ljava/security/KeyStoreException;

    if-eqz v0, :cond_4

    const-string v0, "HTTPs Transmit thread : KeyStoreException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3f3

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Ljava/security/NoSuchAlgorithmException;

    if-eqz v0, :cond_5

    const-string v0, "HTTPs Transmit thread : NoSuchAlgorithmException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3f4

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_5
    instance-of v0, p1, Ljava/security/cert/CertificateException;

    if-eqz v0, :cond_6

    const-string v0, "HTTPs Transmit thread : CertificateException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3f5

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0

    :cond_6
    instance-of v0, p1, Ljava/security/KeyManagementException;

    if-eqz v0, :cond_0

    const-string v0, "HTTPs Transmit thread : KeyManagementException!"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    const/16 v1, 0x3f6

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    const-string v0, "BKS"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    sget-object v0, Lcom/sec/spp/push/notisvc/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f040002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    const-string v2, "bigjoe2013"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    const-string v1, "TLS"

    invoke-static {v1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    const-string v0, "HttpsTransmit thread starts."

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/net/URL;

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/g;->c:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    new-instance v1, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;

    invoke-direct {v1}, Lorg/apache/http/conn/ssl/AllowAllHostnameVerifier;-><init>()V

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const v1, 0xd6d8

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    const-string v1, "content-type"

    const-string v2, "application/json"

    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/g;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "not has next()"

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "body : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->connect()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/g;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/agent/g;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    :cond_1
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Response Code : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/agent/g;->g:Lcom/sec/spp/push/notisvc/agent/h;

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/spp/push/notisvc/agent/h;->c(I)V

    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/agent/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f040003

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "header key : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", header value : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/agent/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/notisvc/agent/g;->a(Ljava/lang/Exception;)V

    goto :goto_2
.end method
