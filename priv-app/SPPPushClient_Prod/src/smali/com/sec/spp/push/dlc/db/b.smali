.class public Lcom/sec/spp/push/dlc/db/b;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private a:Lcom/sec/spp/push/dlc/db/c;

.field private c:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/db/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/sec/spp/push/dlc/db/c;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/c;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->a:Lcom/sec/spp/push/dlc/db/c;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->a:Lcom/sec/spp/push/dlc/db/c;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/c;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private a(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/Log;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v0, 0x0

    new-instance v2, Lcom/sec/spp/push/dlc/db/Log;

    invoke-direct {v2}, Lcom/sec/spp/push/dlc/db/Log;-><init>()V

    const/4 v3, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/spp/push/dlc/db/Log;->b(J)V

    const/4 v0, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/dlc/db/Log;->a(Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/Log;->b(Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/dlc/db/Log;->c(Ljava/lang/String;)V

    const/4 v3, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/Log;->d(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/Log;->a(J)V

    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/Log;->e(Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-interface {p1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/db/Log;->f(Ljava/lang/String;)V

    const/16 v1, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/Log;->g(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/Log;->h(Ljava/lang/String;)V

    return-object v2

    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/b;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const-string v1, "fail to open database. context is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "open database"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Lcom/sec/spp/push/dlc/db/b;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/dlc/db/b;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "fail to open database. SQLException."

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Landroid/database/Cursor;I)Ljava/util/ArrayList;
    .locals 13

    const/4 v5, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    return-object v6

    :cond_2
    invoke-direct {p0, p2}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/Log;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v8

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/db/Log;->j()Ljava/lang/String;

    move-result-object v9

    if-eqz v4, :cond_3

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    :cond_3
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v1, v10, :cond_7

    move v12, v2

    move v2, v3

    move v3, v1

    move v1, v12

    :cond_4
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "pkg : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", quota : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", used : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", logSize : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    const/4 v9, -0x1

    if-eq v2, v9, :cond_0

    const/16 v9, -0x3e8

    if-eq v2, v9, :cond_5

    add-int v9, v1, v8

    if-le v2, v9, :cond_0

    :cond_5
    if-eqz v0, :cond_0

    add-int/2addr v1, v8

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/db/h;->d(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_6

    invoke-virtual {p1, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/2addr v5, v8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2d

    add-int/lit8 v8, v8, 0x3c

    sub-int v8, p3, v8

    if-gt v5, v8, :cond_1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/db/h;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->c()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->e()I

    move-result v2

    move v12, v2

    move v2, v3

    move v3, v1

    move v1, v12

    goto/16 :goto_2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method private b(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/SummLog;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v0, 0x0

    new-instance v2, Lcom/sec/spp/push/dlc/db/SummLog;

    invoke-direct {v2}, Lcom/sec/spp/push/dlc/db/SummLog;-><init>()V

    const/4 v3, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/spp/push/dlc/db/SummLog;->b(J)V

    const/4 v0, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/dlc/db/SummLog;->a(Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/SummLog;->b(Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/spp/push/dlc/db/SummLog;->c(Ljava/lang/String;)V

    const/4 v3, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/SummLog;->d(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/SummLog;->a(J)V

    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/SummLog;->e(Ljava/lang/String;)V

    const/16 v0, 0x8

    invoke-interface {p1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/db/SummLog;->f(Ljava/lang/String;)V

    const/16 v1, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/SummLog;->g(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/spp/push/dlc/db/SummLog;->h(Ljava/lang/String;)V

    const/16 v1, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/spp/push/dlc/db/SummLog;->i(Ljava/lang/String;)V

    const/16 v0, 0xc

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/SummLog;->c(J)V

    const/16 v1, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/SummLog;->d(J)V

    const/16 v0, 0xe

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/SummLog;->e(J)V

    const/16 v1, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/SummLog;->f(J)V

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/sec/spp/push/dlc/db/SummLog;->g(J)V

    return-object v2

    :cond_0
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private b(Ljava/util/ArrayList;Landroid/database/Cursor;I)Ljava/util/ArrayList;
    .locals 13

    const/4 v5, 0x0

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    return-object v6

    :cond_2
    invoke-direct {p0, p2}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/Log;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v8

    invoke-virtual {v7}, Lcom/sec/spp/push/dlc/db/Log;->j()Ljava/lang/String;

    move-result-object v9

    if-eqz v4, :cond_3

    invoke-virtual {v4, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    :cond_3
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v1, v10, :cond_7

    move v12, v2

    move v2, v3

    move v3, v1

    move v1, v12

    :cond_4
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "pkg : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", quota : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", used : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", logSize : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    const/4 v9, -0x1

    if-eq v2, v9, :cond_0

    const/16 v9, -0x3e8

    if-eq v2, v9, :cond_5

    add-int v9, v1, v8

    if-le v2, v9, :cond_0

    :cond_5
    if-eqz v0, :cond_0

    add-int/2addr v1, v8

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/db/h;->c(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v3, v9, :cond_6

    invoke-virtual {p1, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/2addr v5, v8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2d

    add-int/lit8 v8, v8, 0x3c

    sub-int v8, p3, v8

    if-gt v5, v8, :cond_1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/db/h;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/h;->d()I

    move-result v2

    move v12, v2

    move v2, v3

    move v3, v1

    move v1, v12

    goto/16 :goto_2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;IIIII)I
    .locals 7

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "sum1"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sum2"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sum3"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sum4"

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sum5"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "pkg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "summary_log"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public a(Lcom/sec/spp/push/dlc/db/Log;Z)J
    .locals 5

    const/4 v4, 0x0

    if-nez p1, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "activity"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "body"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "category"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "svccode"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "timestamp"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "userid"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "deviceid"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "appver"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "pkg"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_1

    const-string v1, "insert_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "log"

    invoke-virtual {v1, v2, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "urgent_log"

    invoke-virtual {v1, v2, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lcom/sec/spp/push/dlc/db/SummLog;)J
    .locals 4

    if-nez p1, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "idx"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "activity"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "body"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "category"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "svccode"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "timestamp"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "userid"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "deviceid"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "appver"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "pkg"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "sum1"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "sum2"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->m()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "sum3"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "sum4"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "sum5"

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/SummLog;->p()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "summary_log"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getAppSumInfo. "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "idx"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "pkg"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "sum1"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "sum2"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "sum3"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "sum4"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "sum5"

    aput-object v1, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "idx"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " AND "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "pkg"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "summary_log"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "No sLog entry"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v4

    :cond_0
    move-object v4, v0

    goto :goto_0
.end method

.method public a(I)Ljava/util/ArrayList;
    .locals 10

    const/16 v9, 0xe

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/16 v0, 0x10

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "id"

    aput-object v0, v2, v8

    const/4 v0, 0x1

    const-string v1, "activity"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "body"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "category"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "svccode"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "timestamp"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "userid"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string v1, "deviceid"

    aput-object v1, v2, v0

    const/16 v0, 0x8

    const-string v1, "appver"

    aput-object v1, v2, v0

    const/16 v0, 0x9

    const-string v1, "pkg"

    aput-object v1, v2, v0

    const/16 v0, 0xa

    const-string v1, "idx"

    aput-object v1, v2, v0

    const/16 v0, 0xb

    const-string v1, "sum1"

    aput-object v1, v2, v0

    const/16 v0, 0xc

    const-string v1, "sum2"

    aput-object v1, v2, v0

    const/16 v0, 0xd

    const-string v1, "sum3"

    aput-object v1, v2, v0

    const-string v0, "sum4"

    aput-object v0, v2, v9

    const/16 v0, 0xf

    const-string v1, "sum5"

    aput-object v1, v2, v0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "summary_log"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "cs is null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v3

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "moveToFirst is false"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v8

    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/spp/push/dlc/db/b;->b(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/SummLog;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    if-lt v0, v9, :cond_2

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT activity,activity,body,category,svccode,timestamp,userid,deviceid,appver,pkg FROM log WHERE pkg=? UNION SELECT activity,activity,body,category,svccode,timestamp,userid,deviceid,appver,pkg FROM urgent_log WHERE pkg=?"

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "No log entry"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/spp/push/dlc/db/b;->a(Landroid/database/Cursor;)Lcom/sec/spp/push/dlc/db/Log;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a(Ljava/util/ArrayList;IZ)Ljava/util/ArrayList;
    .locals 8

    const/4 v3, 0x0

    if-nez p1, :cond_0

    const-string v0, "regAppInfo null"

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v3

    :cond_0
    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "activity"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "body"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "category"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "svccode"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "timestamp"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "userid"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string v1, "deviceid"

    aput-object v1, v2, v0

    const/16 v0, 0x8

    const-string v1, "appver"

    aput-object v1, v2, v0

    const/16 v0, 0x9

    const-string v1, "pkg"

    aput-object v1, v2, v0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "No log entry"

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    invoke-direct {p0, p1, v1, p2}, Lcom/sec/spp/push/dlc/db/b;->a(Ljava/util/ArrayList;Landroid/database/Cursor;I)Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v3, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, v1, p2}, Lcom/sec/spp/push/dlc/db/b;->b(Ljava/util/ArrayList;Landroid/database/Cursor;I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method public a()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "close database"

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->a:Lcom/sec/spp/push/dlc/db/c;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/db/c;->close()V

    iput-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->a:Lcom/sec/spp/push/dlc/db/c;

    return-void
.end method

.method public a(J)V
    .locals 4

    const-string v0, "insert_time < ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "log"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteOldLog. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(JJ)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "id>=? AND id<=?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "deleteSumLogs. where "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "summary_log"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public b(J)V
    .locals 4

    const-string v0, "id=?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "log"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "pkg=?"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "log"

    invoke-virtual {v2, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "urgent_log"

    invoke-virtual {v2, v3, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public c()I
    .locals 8

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_0

    const-string v1, "db is null"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "db is closed"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT activity,activity,body,category,svccode,timestamp,userid,deviceid,appver,pkg FROM log WHERE pkg=? UNION SELECT activity,activity,body,category,svccode,timestamp,userid,deviceid,appver,pkg FROM urgent_log WHERE pkg=?"

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1
.end method

.method public d()I
    .locals 9

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "body"

    aput-object v0, v2, v8

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/b;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "log"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "No log entry"

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return v8

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    add-int/2addr v0, v8

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SumOfBody="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/db/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v0

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_1
.end method
