.class Lcom/sec/spp/push/n;
.super Landroid/os/Handler;


# static fields
.field private static synthetic b:[I


# instance fields
.field final synthetic a:Lcom/sec/spp/push/PushClientActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/PushClientActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/n;->a:Lcom/sec/spp/push/PushClientActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic a()[I
    .locals 3

    sget-object v0, Lcom/sec/spp/push/n;->b:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/log/collector/d;->values()[Lcom/sec/spp/push/log/collector/d;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->p:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->c:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->a:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->b:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->o:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->g:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_6
    :try_start_6
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->f:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_7
    :try_start_7
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->e:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_8
    :try_start_8
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->d:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_9
    :try_start_9
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->i:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_a
    :try_start_a
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->n:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_b
    :try_start_b
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->h:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_c
    :try_start_c
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->q:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_d
    :try_start_d
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_e
    :try_start_e
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_f
    :try_start_f
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_10
    :try_start_10
    sget-object v1, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v1}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_11
    sput-object v0, Lcom/sec/spp/push/n;->b:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_11

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto :goto_e

    :catch_4
    move-exception v1

    goto :goto_d

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v1

    goto :goto_b

    :catch_7
    move-exception v1

    goto :goto_a

    :catch_8
    move-exception v1

    goto :goto_9

    :catch_9
    move-exception v1

    goto :goto_8

    :catch_a
    move-exception v1

    goto :goto_7

    :catch_b
    move-exception v1

    goto/16 :goto_6

    :catch_c
    move-exception v1

    goto/16 :goto_5

    :catch_d
    move-exception v1

    goto/16 :goto_4

    :catch_e
    move-exception v1

    goto/16 :goto_3

    :catch_f
    move-exception v1

    goto/16 :goto_2

    :catch_10
    move-exception v1

    goto/16 :goto_1
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/log/collector/d;->a(I)Lcom/sec/spp/push/log/collector/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/n;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/spp/push/n;->a:Lcom/sec/spp/push/PushClientActivity;

    invoke-static {v0}, Lcom/sec/spp/push/PushClientActivity;->b(Lcom/sec/spp/push/PushClientActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "string"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
    .end packed-switch
.end method
