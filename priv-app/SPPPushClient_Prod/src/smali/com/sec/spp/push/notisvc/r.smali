.class Lcom/sec/spp/push/notisvc/r;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "<<Feedback DB>>"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/a/b;->a(Landroid/content/Context;)Lcom/sec/spp/push/notisvc/a/b;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "ERROR : Cannot print feedback db. dbhandler is null."

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v3}, Lcom/sec/spp/push/notisvc/a/b;->b()Landroid/database/Cursor;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    const-string v1, "cursor is null. cannot print feedbackDb"

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    const-string v1, ""

    invoke-interface {v4}, Landroid/database/Cursor;->moveToLast()Z

    const/4 v0, 0x1

    move v8, v0

    move-object v0, v1

    move v1, v2

    move v2, v8

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-le v2, v5, :cond_2

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "num of feedbacks:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/r;->a:Lcom/sec/spp/push/notisvc/NotiSvcActivity;

    invoke-static {v1, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/spp/push/notisvc/a/b;->a()V

    goto :goto_0

    :cond_2
    const-string v5, "mid"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/notisvc/a/b;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    add-int/lit8 v1, v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "===========[FEEDBACK "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "]===========\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/notisvc/a/b;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/spp/push/notisvc/c/a;->a(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/c/a;

    move-result-object v6

    if-eqz v6, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "mid : "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "feedback : "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/spp/push/notisvc/c/a;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "feedback Status : "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/notisvc/a/b;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "feedback Retry Count : "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v5}, Lcom/sec/spp/push/notisvc/a/b;->p(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "================================\n\n"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToPrevious()Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method
