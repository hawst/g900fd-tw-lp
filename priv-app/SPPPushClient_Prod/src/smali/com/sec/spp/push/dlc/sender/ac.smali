.class Lcom/sec/spp/push/dlc/sender/ac;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/u;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "RunSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public b(Landroid/os/Message;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->a:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ay:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->b:Z

    iget-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->v(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/r;->a(Landroid/content/Context;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->U:Lcom/sec/spp/push/dlc/sender/i;

    new-instance v2, Landroid/os/Messenger;

    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1, v5, v2}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/r;->b(Landroid/content/Context;)V

    sput-boolean v3, Lcom/sec/spp/push/dlc/util/s;->a:Z

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ah:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_6

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-gtz v0, :cond_3

    const-string v0, "No registered app. Quit state."

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "No registered app. Quit state."

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "Service is stopped"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->y(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->b(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->R:Lcom/sec/spp/push/dlc/sender/i;

    new-instance v2, Landroid/os/Messenger;

    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1, v5, v2}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;)V

    goto/16 :goto_1

    :cond_6
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->aa:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_b

    iget v0, p1, Landroid/os/Message;->arg1:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NumOfLog ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->b:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/r;->b(Landroid/content/Context;)V

    :cond_7
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_9
    invoke-static {}, Lcom/sec/spp/push/dlc/util/e;->a()I

    move-result v0

    if-ne v0, v3, :cond_a

    const-string v0, "Current NetType is Wifi TransTo [SendAll St]"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->d(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->e(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_b
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->s:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->f(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_c
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->c:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_d

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    :cond_d
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_e
    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Z)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_f
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->d:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_11

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Urgent package ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ac;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_10
    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/SenderService;->b(Lcom/sec/spp/push/dlc/sender/SenderService;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->b(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->g(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_11
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->l:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_12

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_12
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->m:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_13

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->b(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_13
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->x:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_16

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_14

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_14

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->ap:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_15

    :cond_14
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_15
    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/spp/push/dlc/util/f;->e(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->f(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->h(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_16
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->y:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_19

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    if-eqz v0, :cond_17

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_17

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->ap:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_18

    :cond_17
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->b(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_18
    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/spp/push/dlc/util/f;->e(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->f(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->i(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_19
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->p:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_1a

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->k:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->m:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/i;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1

    :cond_1a
    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->k:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ac;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->j(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_1
.end method
