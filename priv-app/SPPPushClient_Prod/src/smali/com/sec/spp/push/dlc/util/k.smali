.class Lcom/sec/spp/push/dlc/util/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/util/PushActivity;

.field private final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/dlc/util/PushActivity;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    iput-object p2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const-wide/16 v4, 0x400

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Not provisioned"

    :goto_0
    iget-object v1, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "1. PV info \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "2. Daily Limit \nMobile ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->o(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->g(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Bytes\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wifi   ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->p(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->h(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Bytes\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "3. DB Status\n"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/PushActivity;->a(Lcom/sec/spp/push/dlc/util/PushActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "State ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/spp/push/dlc/util/k;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->C(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->R:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Messenger;

    iget-object v4, p0, Lcom/sec/spp/push/dlc/util/k;->a:Lcom/sec/spp/push/dlc/util/PushActivity;

    iget-object v4, v4, Lcom/sec/spp/push/dlc/util/PushActivity;->a:Landroid/os/Handler;

    invoke-direct {v3, v4}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v0, v1, v2, v3}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/m;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
