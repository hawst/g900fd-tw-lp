.class public final enum Lcom/sec/spp/push/dlc/sender/q;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum b:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum c:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum d:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum e:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum f:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum g:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum h:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum i:Lcom/sec/spp/push/dlc/sender/q;

.field public static final enum j:Lcom/sec/spp/push/dlc/sender/q;

.field private static final l:I

.field private static m:[Lcom/sec/spp/push/dlc/sender/q;

.field private static final synthetic n:[Lcom/sec/spp/push/dlc/sender/q;


# instance fields
.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_SUCCESS"

    const/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_ALREADY_REGISTERED"

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v5, v3}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_FAIL"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v6, v3}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->c:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_INVALID_PARAMS"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v7, v3}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->d:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_INTERNAL_ERROR"

    const/4 v3, -0x3

    invoke-direct {v1, v2, v8, v3}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->e:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_INTERNAL_DB_ERROR"

    const/4 v3, 0x5

    const/4 v4, -0x4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_HTTP_FAIL"

    const/4 v3, 0x6

    const/4 v4, -0x5

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->g:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_TIMEOUT"

    const/4 v3, 0x7

    const/4 v4, -0x6

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->h:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_BLOCKED_APP"

    const/16 v3, 0x8

    const/4 v4, -0x7

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->i:Lcom/sec/spp/push/dlc/sender/q;

    new-instance v1, Lcom/sec/spp/push/dlc/sender/q;

    const-string v2, "RESULT_PACKAGE_NOT_FOUND"

    const/16 v3, 0x9

    const/4 v4, -0x8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/spp/push/dlc/sender/q;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    const/16 v1, 0xa

    new-array v1, v1, [Lcom/sec/spp/push/dlc/sender/q;

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v2, v1, v0

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->c:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v2, v1, v6

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->d:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v2, v1, v7

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->e:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->f:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->g:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->h:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->i:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    aput-object v3, v1, v2

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->n:[Lcom/sec/spp/push/dlc/sender/q;

    const-class v1, Lcom/sec/spp/push/dlc/sender/q;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->size()I

    move-result v1

    sput v1, Lcom/sec/spp/push/dlc/sender/q;->l:I

    sget v1, Lcom/sec/spp/push/dlc/sender/q;->l:I

    new-array v1, v1, [Lcom/sec/spp/push/dlc/sender/q;

    sput-object v1, Lcom/sec/spp/push/dlc/sender/q;->m:[Lcom/sec/spp/push/dlc/sender/q;

    const-class v1, Lcom/sec/spp/push/dlc/sender/q;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/sender/q;

    sget-object v4, Lcom/sec/spp/push/dlc/sender/q;->m:[Lcom/sec/spp/push/dlc/sender/q;

    add-int/lit8 v2, v1, 0x1

    aput-object v0, v4, v1

    move v1, v2

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/dlc/sender/q;->k:I

    return-void
.end method

.method public static a(I)Lcom/sec/spp/push/dlc/sender/q;
    .locals 3

    const-class v0, Lcom/sec/spp/push/dlc/sender/q;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    if-ne v2, p0, :cond_0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/q;
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/dlc/sender/q;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/dlc/sender/q;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/dlc/sender/q;->n:[Lcom/sec/spp/push/dlc/sender/q;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/dlc/sender/q;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/dlc/sender/q;->k:I

    return v0
.end method
