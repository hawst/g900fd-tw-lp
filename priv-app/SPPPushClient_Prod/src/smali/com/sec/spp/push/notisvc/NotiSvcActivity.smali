.class public Lcom/sec/spp/push/notisvc/NotiSvcActivity;
.super Landroid/app/Activity;


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/widget/EditText;

.field private D:Landroid/widget/EditText;

.field private E:Landroid/widget/EditText;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/view/View$OnClickListener;

.field private H:Landroid/view/View$OnClickListener;

.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/EditText;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/Button;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/sec/spp/push/notisvc/d;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/d;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->G:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/sec/spp/push/notisvc/o;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/o;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->H:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(Ljava/lang/String;)J
    .locals 7

    const-wide/16 v0, 0x0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    return-wide v0

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Ljava/lang/String;)J

    move-result-wide v5

    add-long/2addr v0, v5

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v0, v5

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 5

    const v0, 0x7f09000c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a:Landroid/widget/Button;

    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b:Landroid/widget/Button;

    const v0, 0x7f09000e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->c:Landroid/widget/Button;

    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->d:Landroid/widget/Button;

    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->e:Landroid/widget/TextView;

    const v0, 0x7f090010

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->f:Landroid/widget/Button;

    const v0, 0x7f090011

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->g:Landroid/widget/Button;

    const v0, 0x7f090012

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->h:Landroid/widget/Button;

    const v0, 0x7f090013

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->i:Landroid/widget/Button;

    const v0, 0x7f090014

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->j:Landroid/widget/Button;

    const v0, 0x7f090016

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->k:Landroid/widget/EditText;

    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->l:Landroid/widget/EditText;

    const v0, 0x7f090019

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->m:Landroid/widget/Button;

    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->n:Landroid/widget/Button;

    const v0, 0x7f09001b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->o:Landroid/widget/Button;

    const v0, 0x7f09001c

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->p:Landroid/widget/Button;

    const v0, 0x7f09001d

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->q:Landroid/widget/Button;

    const v0, 0x7f09001e

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->r:Landroid/widget/Button;

    const v0, 0x7f09001f

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->s:Landroid/widget/Button;

    const v0, 0x7f09000b

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->t:Landroid/widget/Button;

    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->c()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->d()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->c:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->e()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->G:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->g:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->g()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->h:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->h()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->i:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->i()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->j:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->j()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->m:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->k()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->n:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->l()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->o:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->m()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->p:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->n()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->q:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->o()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->r:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->p()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->s:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->q()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->t:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->r()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->u:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->s()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    const-string v1, "https://STG1-Registration-243671819.eu-west-1.elb.amazonaws.com:80"

    const-string v2, "https://STG2-Registration-965007815.eu-west-1.elb.amazonaws.com:80"

    const-string v3, "http://54.228.232.87:80"

    const-string v4, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const-string v1, "Server : PROD"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const-string v1, "Server : STG2"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const-string v1, "Server : STG1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const-string v0, "https://ew1.reg.bigdata.ssp.samsung.com:80"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const-string v1, "Server : DEV"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->F:Landroid/widget/TextView;

    const-string v1, "Server : ERROR"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    if-eqz p1, :cond_4

    const-string v0, "com.sec.spp.push.NOTIFICATION_NOTICE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v1, "com.sec.chaton"

    :cond_1
    if-eqz v0, :cond_2

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v0, ""

    :cond_3
    const-string v2, "pkgName"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "version"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->t()V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_3

    array-length v4, v3

    :goto_1
    if-lt v1, v4, :cond_4

    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_4
    aget-object v5, v3, v1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Landroid/content/Context;Ljava/lang/String;)Z

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Ljava/lang/String;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->v:Landroid/widget/Button;

    return-object v0
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    :try_start_4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v3, :cond_3

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_3
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    :goto_3
    :try_start_7
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v3, :cond_4

    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_4
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_4
    if-eqz v3, :cond_5

    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :cond_6
    :goto_5
    throw v0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v1

    move-object v2, v0

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    goto :goto_2

    :catch_9
    move-exception v1

    move-object v2, v0

    goto :goto_2
.end method

.method private b()V
    .locals 0

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/spp/push/notisvc/NotiSvcActivity;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/p;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/p;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic c(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->w:Landroid/widget/Button;

    return-object v0
.end method

.method private d()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/q;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/q;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic d(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->x:Landroid/widget/Button;

    return-object v0
.end method

.method private e()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/r;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/r;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic e(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->y:Landroid/widget/Button;

    return-object v0
.end method

.method private f()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/s;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/s;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic f(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->z:Landroid/widget/Button;

    return-object v0
.end method

.method private g()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/t;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/t;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic g(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->A:Landroid/widget/Button;

    return-object v0
.end method

.method private h()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/u;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/u;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic h(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->B:Landroid/widget/EditText;

    return-object v0
.end method

.method private i()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/v;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/v;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic i(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->C:Landroid/widget/EditText;

    return-object v0
.end method

.method private j()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/e;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/e;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic j(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->D:Landroid/widget/EditText;

    return-object v0
.end method

.method private k()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/f;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/f;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method static synthetic k(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->E:Landroid/widget/EditText;

    return-object v0
.end method

.method private l()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/g;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/g;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private m()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/h;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/h;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private n()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/i;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/i;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private o()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/j;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/j;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private p()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/k;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/k;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private q()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/l;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/l;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private r()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/m;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/m;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private s()Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lcom/sec/spp/push/notisvc/n;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/notisvc/n;-><init>(Lcom/sec/spp/push/notisvc/NotiSvcActivity;)V

    return-object v0
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->a()V

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/NotiSvcActivity;->b()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
