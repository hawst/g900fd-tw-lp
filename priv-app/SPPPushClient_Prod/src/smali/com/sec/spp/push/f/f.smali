.class public final enum Lcom/sec/spp/push/f/f;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/f/f;

.field public static final enum b:Lcom/sec/spp/push/f/f;

.field public static final enum c:Lcom/sec/spp/push/f/f;

.field public static final enum d:Lcom/sec/spp/push/f/f;

.field public static final enum e:Lcom/sec/spp/push/f/f;

.field public static final enum f:Lcom/sec/spp/push/f/f;

.field private static final synthetic g:[Lcom/sec/spp/push/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_URGENT_START"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->a:Lcom/sec/spp/push/f/f;

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_URGENT_STOP"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->b:Lcom/sec/spp/push/f/f;

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_LOGGING_START"

    invoke-direct {v0, v1, v5}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->c:Lcom/sec/spp/push/f/f;

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_LOGGING_STOP"

    invoke-direct {v0, v1, v6}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->d:Lcom/sec/spp/push/f/f;

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_PROVISION"

    invoke-direct {v0, v1, v7}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->e:Lcom/sec/spp/push/f/f;

    new-instance v0, Lcom/sec/spp/push/f/f;

    const-string v1, "TYPE_ACTION_PING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/f/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/f/f;->f:Lcom/sec/spp/push/f/f;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/spp/push/f/f;

    sget-object v1, Lcom/sec/spp/push/f/f;->a:Lcom/sec/spp/push/f/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/f/f;->b:Lcom/sec/spp/push/f/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/f/f;->c:Lcom/sec/spp/push/f/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/f/f;->d:Lcom/sec/spp/push/f/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/f/f;->e:Lcom/sec/spp/push/f/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/f/f;->f:Lcom/sec/spp/push/f/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/f/f;->g:[Lcom/sec/spp/push/f/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/f/f;
    .locals 1

    const-class v0, Lcom/sec/spp/push/f/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/f/f;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/f/f;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/f/f;->g:[Lcom/sec/spp/push/f/f;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/f/f;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
