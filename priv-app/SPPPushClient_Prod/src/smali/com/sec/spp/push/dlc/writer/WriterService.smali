.class public Lcom/sec/spp/push/dlc/writer/WriterService;
.super Landroid/app/Service;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/spp/push/dlc/writer/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/writer/WriterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/dlc/writer/WriterService;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/writer/WriterService;->b()V

    return-void
.end method

.method private a(Lcom/sec/spp/push/dlc/db/Log;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/writer/WriterService;->b(Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/writer/WriterService;->c(Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->o(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;I)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/spp/push/dlc/writer/WriterService;Lcom/sec/spp/push/dlc/db/Log;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/writer/WriterService;->a(Lcom/sec/spp/push/dlc/db/Log;)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->F(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[SelfPing] Skip Check"

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/dlc/util/s;->a()Lcom/sec/spp/push/dlc/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/util/s;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/r;->a(Landroid/content/Context;)V

    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/spp/push/dlc/util/f;->b(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private b(Lcom/sec/spp/push/dlc/db/Log;)Z
    .locals 7

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->g(Landroid/content/Context;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    mul-long/2addr v0, v2

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->o(Landroid/content/Context;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "limitADay size ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] todayInserted ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] Log Size ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    int-to-long v4, v4

    add-long/2addr v4, v2

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    sget-object v4, Lcom/sec/spp/push/dlc/sender/i;->aj:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {p0, v4}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TodayInserted ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] over limitADay ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/sec/spp/push/dlc/db/Log;)Z
    .locals 10

    const/16 v1, -0x3e8

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v0, "DB Error at overAppQuota()"

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/spp/push/dlc/db/Log;->k()I

    move-result v5

    invoke-virtual {v3, v4}, Lcom/sec/spp/push/dlc/db/e;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move v0, v2

    goto :goto_0

    :cond_1
    const-string v0, "dataquota"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v7, "wifiquota"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    add-int v8, v0, v7

    const/4 v8, -0x1

    if-ne v0, v8, :cond_2

    const-string v0, "FAILED : This is blocked app"

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move v0, v2

    goto :goto_0

    :cond_2
    if-eq v0, v1, :cond_3

    if-ne v7, v1, :cond_5

    :cond_3
    move v0, v1

    :cond_4
    :goto_1
    const-string v7, "loadedSize"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " AppQuota ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] LoadedSize ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] request size ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eq v0, v1, :cond_6

    add-int v1, v7, v5

    if-ge v0, v1, :cond_6

    const-string v0, "FAILED : App quota overed"

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v2}, Lcom/sec/spp/push/dlc/db/e;->a(Ljava/lang/String;Z)Lcom/sec/spp/push/dlc/sender/q;

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    move v0, v2

    goto/16 :goto_0

    :cond_5
    if-ltz v7, :cond_4

    add-int/2addr v0, v7

    goto :goto_1

    :cond_6
    add-int v0, v7, v5

    invoke-virtual {v3, v4, v0}, Lcom/sec/spp/push/dlc/db/e;->c(Ljava/lang/String;I)Z

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {v3}, Lcom/sec/spp/push/dlc/db/e;->a()V

    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/writer/WriterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestSend PID ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pkg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, p1, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lcom/sec/spp/push/dlc/util/f;->d(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/sec/spp/push/dlc/db/e;->a(Landroid/content/Context;)Lcom/sec/spp/push/dlc/db/e;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "DB Error at isRegisteredPkg()"

    sget-object v2, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1, p1}, Lcom/sec/spp/push/dlc/db/e;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "Interal DB error : This App is not registered"

    sget-object v4, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "This App is not registered"

    sget-object v4, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "This App is not registered"

    sget-object v4, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    throw v0

    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/db/e;->a()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "com.sec.chaton"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/writer/WriterService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v3, "3.0.82"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v2, "3.0.20"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v1, "ChatON result : success"

    sget-object v2, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "ChatON PI not found"

    sget-object v2, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/writer/WriterService;->b:Lcom/sec/spp/push/dlc/writer/b;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "Service created"

    sget-object v1, Lcom/sec/spp/push/dlc/writer/WriterService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/sec/spp/push/dlc/writer/b;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/writer/b;-><init>(Lcom/sec/spp/push/dlc/writer/WriterService;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/writer/WriterService;->b:Lcom/sec/spp/push/dlc/writer/b;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
