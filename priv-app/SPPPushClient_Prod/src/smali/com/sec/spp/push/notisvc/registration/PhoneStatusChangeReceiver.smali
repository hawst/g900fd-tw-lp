.class public Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-class v0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->a:Ljava/lang/String;

    const-string v0, "com.sec.spp.BOOT_COMPLETE"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->b:Ljava/lang/String;

    const-string v0, "com.sec.spp.push.UPDATE_NOTICE"

    iput-object v0, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/16 v4, -0x3e8

    const/4 v1, 0x0

    const-string v0, "onReceive."

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->h(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v4, :cond_1

    const-string v0, "This device doesn\'t register yet."

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Intent Action is null."

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/PhoneStatusChangeReceiver;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.sec.spp.BOOT_COMPLETE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/l;->a()Lcom/sec/spp/push/notisvc/card/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/card/l;->b(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/c/c;->a()Lcom/sec/spp/push/notisvc/c/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/c/c;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/h;->i(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.sec.spp.push.UPDATE_NOTICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/spp/push/notisvc/card/l;->a()Lcom/sec/spp/push/notisvc/card/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/card/l;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    :cond_5
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    :cond_6
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    :cond_7
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v4, :cond_8

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;)I

    move-result v2

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->b()I

    move-result v3

    if-eq v2, v3, :cond_8

    move v0, v1

    :cond_8
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    :cond_9
    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/d/b;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/spp/push/notisvc/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    :cond_a
    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/sec/spp/push/notisvc/registration/h;->h(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
