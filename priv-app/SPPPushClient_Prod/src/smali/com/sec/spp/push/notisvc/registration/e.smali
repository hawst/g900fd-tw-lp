.class Lcom/sec/spp/push/notisvc/registration/e;
.super Lcom/sec/spp/push/notisvc/agent/h;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/notisvc/registration/c;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/sec/spp/push/notisvc/registration/k;


# direct methods
.method private constructor <init>(Lcom/sec/spp/push/notisvc/registration/c;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/notisvc/registration/e;->a:Lcom/sec/spp/push/notisvc/registration/c;

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/agent/h;-><init>()V

    iput-object p2, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/spp/push/notisvc/registration/c;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;Lcom/sec/spp/push/notisvc/registration/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/spp/push/notisvc/registration/e;-><init>(Lcom/sec/spp/push/notisvc/registration/c;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)V

    return-void
.end method

.method private a()V
    .locals 7

    new-instance v0, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;-><init>()V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "regitimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x36ee80

    add-long/2addr v3, v5

    iget-object v5, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v5, v6}, Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;->a(Ljava/lang/String;Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/alarm/DVCAgreementAlarmHandler;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/alarm/AlarmEventManager;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/notisvc/alarm/AlarmEventHandler;)V

    return-void
.end method

.method private b()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DVCnAgree is discard. pkg : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", regiStatusFlag : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/e;->a:Lcom/sec/spp/push/notisvc/registration/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/c;->a(Z)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Callback Method. on Success. status is null."

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->k(Landroid/content/Context;)I

    move-result v2

    if-ne v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account agree Success. status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/notisvc/d/b;->d(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    const-string v1, "Account agree Success, but status already changed."

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "successed status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", before status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/d/b;->k(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/h;->j(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    const-string v2, "setupWizard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v1

    iget-object v2, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/spp/push/notisvc/d/b;->l(Landroid/content/Context;)I

    move-result v2

    if-ne v1, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setup agree Success. status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/notisvc/d/b;->e(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_3
    const-string v1, "Setup agree Success, but status already changed."

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "successed status : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", before status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/spp/push/notisvc/d/b;->l(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/e;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/notisvc/registration/h;->j(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "agreePkg is wrong. agreePkg : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b(I)V
    .locals 3

    const/16 v2, 0x1f4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DVCnAgree regi Fail. agreePkg : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/notisvc/registration/e;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v1}, Lcom/sec/spp/push/notisvc/registration/k;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/notisvc/registration/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/notisvc/registration/e;->a:Lcom/sec/spp/push/notisvc/registration/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/notisvc/registration/c;->a(Z)V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/e;->a()V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x190

    if-lt p1, v0, :cond_1

    if-ge p1, v2, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/e;->b()V

    goto :goto_0

    :cond_1
    if-lt p1, v2, :cond_2

    const/16 v0, 0x258

    if-ge p1, v0, :cond_2

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/e;->a()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_3

    const/16 v0, 0x3eb

    if-eq p1, v0, :cond_3

    const/16 v0, 0x3ee

    if-ne p1, v0, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/e;->a()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/registration/e;->b()V

    goto :goto_0
.end method
