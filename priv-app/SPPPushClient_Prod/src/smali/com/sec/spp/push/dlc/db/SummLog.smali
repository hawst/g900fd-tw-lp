.class public Lcom/sec/spp/push/dlc/db/SummLog;
.super Lcom/sec/spp/push/dlc/db/Log;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private index:Ljava/lang/String;

.field private sum1:J

.field private sum2:J

.field private sum3:J

.field private sum4:J

.field private sum5:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/sec/spp/push/dlc/db/j;

    invoke-direct {v0}, Lcom/sec/spp/push/dlc/db/j;-><init>()V

    sput-object v0, Lcom/sec/spp/push/dlc/db/SummLog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/db/Log;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/db/Log;-><init>(Landroid/os/Parcel;)V

    invoke-direct {p0, p1}, Lcom/sec/spp/push/dlc/db/SummLog;->a(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJJ)V
    .locals 3

    invoke-direct/range {p0 .. p10}, Lcom/sec/spp/push/dlc/db/Log;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    iput-wide p12, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJJ)V
    .locals 2

    invoke-direct/range {p0 .. p10}, Lcom/sec/spp/push/dlc/db/Log;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p11, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    iput-wide p12, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    return-void
.end method

.method private a(Landroid/os/Parcel;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    return-void
.end method


# virtual methods
.method public c(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    return-void
.end method

.method public d(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    return-void
.end method

.method public f(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    return-void
.end method

.method public g(J)V
    .locals 0

    iput-wide p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    return-void
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    return-wide v0
.end method

.method public m()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    return-wide v0
.end method

.method public n()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    return-wide v0
.end method

.method public o()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    return-wide v0
.end method

.method public p()J
    .locals 2

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    return-wide v0
.end method

.method public q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/sec/spp/push/dlc/db/Log;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sum1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sum2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sum3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sum4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sum5="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/sec/spp/push/dlc/db/Log;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->index:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum1:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum2:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum3:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum4:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/sec/spp/push/dlc/db/SummLog;->sum5:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
