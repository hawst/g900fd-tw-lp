.class Lcom/sec/spp/push/b/d;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/b/c;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/b/c;Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/sec/spp/push/b/d;->a:Lcom/sec/spp/push/b/c;

    const-string v0, "log_data_db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/b/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CREATE TABLE log_data_table (id INTEGER PRIMARY KEY AUTOINCREMENT,wifi_pushRetryCount INTEGER,wifiDisconnCount INTEGER,wifi_breakDuration BIGINT,mo_pushRetryCount INTEGER,mobileDisconnCount INTEGER,mo_breakDuration BIGINT,wifi_timeoutCount INTEGER,wifi_crpCount INTEGER,wifi_nnrCount INTEGER,mo_timeoutCount INTEGER,mo_crpCount INTEGER,mo_nnrCount INTEGER,wifi_pingCount INTEGER,wifi_pingMinDuration INTEGER,wifi_pingMaxDuration INTEGER,wifi_pingAvgDuration INTEGER,mo_pingCount INTEGER,mo_pingMinDuration INTEGER,mo_pingMaxDuration INTEGER,mo_pingAvgDuration INTEGER,wifi_ConnCnt INTEGER,wifi_Duration INTEGER,mo_ConnCnt INTEGER,mo_Duration INTEGER,bt_ConnCnt INTEGER,bt_Duration INTEGER,disconnStartTime BIGINT,disconnType BIGINT,pingSentTime BIGINT,prevConnType INTEGER,prevConnTime BIGINT);"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "CREATE TABLE log_data_table (id INTEGER PRIMARY KEY AUTOINCREMENT,wifi_pushRetryCount INTEGER,wifiDisconnCount INTEGER,wifi_breakDuration BIGINT,mo_pushRetryCount INTEGER,mobileDisconnCount INTEGER,mo_breakDuration BIGINT,wifi_timeoutCount INTEGER,wifi_crpCount INTEGER,wifi_nnrCount INTEGER,mo_timeoutCount INTEGER,mo_crpCount INTEGER,mo_nnrCount INTEGER,wifi_pingCount INTEGER,wifi_pingMinDuration INTEGER,wifi_pingMaxDuration INTEGER,wifi_pingAvgDuration INTEGER,mo_pingCount INTEGER,mo_pingMinDuration INTEGER,mo_pingMaxDuration INTEGER,mo_pingAvgDuration INTEGER,wifi_ConnCnt INTEGER,wifi_Duration INTEGER,mo_ConnCnt INTEGER,mo_Duration INTEGER,bt_ConnCnt INTEGER,bt_Duration INTEGER,disconnStartTime BIGINT,disconnType BIGINT,pingSentTime BIGINT,prevConnType INTEGER,prevConnTime BIGINT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/b/c;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "INSERT INTO log_data_table(wifi_pushRetryCount,wifiDisconnCount,wifi_breakDuration,mo_pushRetryCount,mobileDisconnCount,mo_breakDuration,wifi_timeoutCount,wifi_crpCount,wifi_nnrCount,mo_timeoutCount,mo_crpCount,mo_nnrCount,wifi_pingCount,wifi_pingMinDuration,wifi_pingMaxDuration,wifi_pingAvgDuration,mo_pingCount,mo_pingMinDuration,mo_pingMaxDuration,mo_pingAvgDuration,wifi_ConnCnt,wifi_Duration,mo_ConnCnt,mo_Duration,bt_ConnCnt,bt_Duration,disconnStartTime,disconnType,pingSentTime,prevConnType,prevConnTime) VALUES (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0);"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "INSERT INTO log_data_table(wifi_pushRetryCount,wifiDisconnCount,wifi_breakDuration,mo_pushRetryCount,mobileDisconnCount,mo_breakDuration,wifi_timeoutCount,wifi_crpCount,wifi_nnrCount,mo_timeoutCount,mo_crpCount,mo_nnrCount,wifi_pingCount,wifi_pingMinDuration,wifi_pingMaxDuration,wifi_pingAvgDuration,mo_pingCount,mo_pingMinDuration,mo_pingMaxDuration,mo_pingAvgDuration,wifi_ConnCnt,wifi_Duration,mo_ConnCnt,mo_Duration,bt_ConnCnt,bt_Duration,disconnStartTime,disconnType,pingSentTime,prevConnType,prevConnTime) VALUES (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    return-void
.end method
