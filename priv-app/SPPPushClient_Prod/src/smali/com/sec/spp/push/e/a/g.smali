.class public Lcom/sec/spp/push/e/a/g;
.super Lcom/sec/spp/push/e/a/a;


# static fields
.field private static final d:Ljava/lang/String;

.field private static f:Z

.field private static g:Z

.field private static final k:Lcom/sec/spp/push/e/a/g;


# instance fields
.field private e:Lcom/sec/spp/push/c/b;

.field private h:Z

.field private final i:Ljava/lang/Object;

.field private final j:Ljava/lang/Object;

.field private final l:Lcom/sec/spp/push/e/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/spp/push/e/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    sput-boolean v1, Lcom/sec/spp/push/e/a/g;->f:Z

    sput-boolean v1, Lcom/sec/spp/push/e/a/g;->g:Z

    new-instance v0, Lcom/sec/spp/push/e/a/g;

    invoke-direct {v0}, Lcom/sec/spp/push/e/a/g;-><init>()V

    sput-object v0, Lcom/sec/spp/push/e/a/g;->k:Lcom/sec/spp/push/e/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/g;->j:Ljava/lang/Object;

    new-instance v0, Lcom/sec/spp/push/e/a/h;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/e/a/h;-><init>(Lcom/sec/spp/push/e/a/g;)V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/g;->l:Lcom/sec/spp/push/e/b/a;

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->f()V

    return-void
.end method

.method public static declared-synchronized g()Lcom/sec/spp/push/e/a/g;
    .locals 3

    const-class v1, Lcom/sec/spp/push/e/a/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/g;->k:Lcom/sec/spp/push/e/a/g;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v2, "PushConnectionManager.getInstance() returns null "

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/sec/spp/push/e/a/g;->k:Lcom/sec/spp/push/e/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic p()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .locals 6

    const-string v0, "session_timer"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/32 v2, 0x44aa200

    add-long v3, v0, v2

    new-instance v5, Lcom/sec/spp/push/e/a/j;

    invoke-direct {v5, p0}, Lcom/sec/spp/push/e/a/j;-><init>(Lcom/sec/spp/push/e/a/g;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v0

    const-string v2, "session_timer"

    invoke-virtual/range {v0 .. v5}, Lcom/sec/spp/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;JLcom/sec/spp/push/util/a;)V

    return-void
.end method

.method private r()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.event.SPP_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/k;->K()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ldUpdate"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    sget-object v1, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending BR to GL : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "ldUpdate"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "ldUpdate"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private s()V
    .locals 1

    new-instance v0, Lcom/sec/spp/push/update/Update;

    invoke-direct {v0}, Lcom/sec/spp/push/update/Update;-><init>()V

    invoke-virtual {v0}, Lcom/sec/spp/push/update/Update;->a()V

    return-void
.end method

.method private t()V
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/sec/spp/push/e/a/g;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/util/AlarmTimer;->a()Lcom/sec/spp/push/util/AlarmTimer;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sec/spp/push/util/AlarmTimer;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private u()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.ACTION_INITIALIZATION_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected a()Lorg/jboss/netty/channel/ChannelPipelineFactory;
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->f()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/e/b/d;->a(Z)Lcom/sec/spp/push/e/b/d;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/sec/spp/push/util/j;)V
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Init.] Success. ResultCode :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->b(Z)V

    invoke-static {}, Lcom/sec/spp/push/heartbeat/HeartBeat;->l()V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v1, "[Init.] Success. Execcute next task"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/d/a/b;->f()V

    :cond_0
    invoke-direct {p0}, Lcom/sec/spp/push/e/a/g;->q()V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->c:Lcom/sec/spp/push/log/collector/d;

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IJ)V

    invoke-static {}, Lcom/sec/spp/push/g/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/util/b;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/g;->r()V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->p()V

    invoke-static {}, Lcom/sec/spp/push/c/b;->c()V

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/sec/spp/push/c/b;->b(J)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/g;->u()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/spp/push/e/a/g;->b(Z)V

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Init.] onSuccess() But Fail : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/g;->e:Lcom/sec/spp/push/c/b;

    iget-object v0, p0, Lcom/sec/spp/push/e/a/g;->e:Lcom/sec/spp/push/c/b;

    invoke-virtual {p1}, Lcom/sec/spp/push/util/j;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/c/b;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/spp/push/e/a/g;->e:Lcom/sec/spp/push/c/b;

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;IZ)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/spp/push/e/a/a;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/e/a/g;->c:Lorg/jboss/netty/channel/Channel;

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-string v1, "message channel handler"

    invoke-interface {v0, v1}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/String;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/e;

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->m()Lcom/sec/spp/push/e/b/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/b/e;->a(Lcom/sec/spp/push/e/b/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lcom/sec/spp/push/c/a;

    invoke-direct {v0}, Lcom/sec/spp/push/c/a;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 6

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[Init.] setInitializing : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p1, Lcom/sec/spp/push/e/a/g;->f:Z

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/spp/push/util/c;->d(J)V

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[Init.] Update Init Try time : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/e/a/g;->c:Lorg/jboss/netty/channel/Channel;

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-string v1, "message channel handler"

    invoke-interface {v0, v1}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/String;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/b/e;->a(Lcom/sec/spp/push/e/b/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-super {p0}, Lcom/sec/spp/push/e/a/a;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Lcom/sec/spp/push/c/a;

    invoke-direct {v0}, Lcom/sec/spp/push/c/a;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-super {p0}, Lcom/sec/spp/push/e/a/a;->b()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)V
    .locals 6

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[STATE] setInitialized : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean p1, Lcom/sec/spp/push/e/a/g;->g:Z

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/spp/push/util/c;->g(J)V

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[STATE] Update Init time : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Z)V
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ReInitState : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/sec/spp/push/e/a/g;->h:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->b(Z)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->c(Z)V

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->close()Lorg/jboss/netty/channel/ChannelFuture;

    :cond_0
    return-void
.end method

.method public h()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v1, "[Init.] Error : context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/d;->j()Z

    move-result v1

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/e/a/d;->m()Z

    move-result v2

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v1, :cond_1

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->l()V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->j()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->n()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Init.] isInitialized : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isinitializing : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->j()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reInit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->n()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " return"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_5

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    goto :goto_0

    :cond_5
    invoke-static {}, Lcom/sec/spp/push/util/k;->g()Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v1, "[Init.] Network not available"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(I)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/h/c;->f()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v1, "[Init.] Reg Table : empty and Pending Q : empty"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/sec/spp/push/e/a/g;->s()V

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->a:Lcom/sec/spp/push/log/collector/d;

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/spp/push/log/collector/PushClientLogCollectService;->a(Landroid/content/Context;Lcom/sec/spp/push/log/collector/d;IJ)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/e/a/i;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/e/a/i;-><init>(Lcom/sec/spp/push/e/a/g;)V

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/util/h;)V

    goto/16 :goto_0
.end method

.method public i()Z
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/e/a/g;->g:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public j()Z
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/e/a/g;->f:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()V
    .locals 2

    sget-object v0, Lcom/sec/spp/push/e/a/g;->d:Ljava/lang/String;

    const-string v1, "destroy() called "

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->l()V

    return-void
.end method

.method public l()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->b(Z)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    return-void
.end method

.method public m()Lcom/sec/spp/push/e/b/a;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/e/a/g;->l:Lcom/sec/spp/push/e/b/a;

    return-object v0
.end method

.method public n()Z
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/spp/push/e/a/g;->h:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public o()V
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/e/a/g;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/sec/spp/push/e/a/g;->f:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/g;->c(Z)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/g;->t()V

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/g;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
