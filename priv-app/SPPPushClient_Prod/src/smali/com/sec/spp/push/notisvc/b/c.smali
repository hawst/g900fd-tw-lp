.class public Lcom/sec/spp/push/notisvc/b/c;
.super Lcom/sec/spp/push/notisvc/b/a;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/notisvc/b/a;-><init>()V

    return-void
.end method

.method static synthetic a(FLandroid/content/Context;)F
    .locals 1

    invoke-static {p0, p1}, Lcom/sec/spp/push/notisvc/b/c;->b(FLandroid/content/Context;)F

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/b/h;Lcom/sec/spp/push/notisvc/b/j;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 6

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getViewLayoutListener. IllegalArgument. ctx:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", templateType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", overlayView:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", viewDisplayHandler:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Lcom/sec/spp/push/notisvc/b/d;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/notisvc/b/d;-><init>(Lcom/sec/spp/push/notisvc/b/c;Lcom/sec/spp/push/notisvc/b/h;Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/b/j;)V

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/spp/push/notisvc/b/h;
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getOverlayViewByType. invalid params. cardType:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "3"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "5"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "4"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "6"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    new-instance v0, Lcom/sec/spp/push/notisvc/b/e;

    invoke-direct {v0, p0, p1}, Lcom/sec/spp/push/notisvc/b/e;-><init>(Lcom/sec/spp/push/notisvc/b/c;Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static b(FLandroid/content/Context;)F
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    mul-float/2addr v0, p0

    return v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/util/Map;)V
    .locals 6

    const-string v0, "display overlay type."

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "display. invalid params. ctx:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    :try_start_0
    const-string v0, "notiTag"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const-string v0, "mid"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "templateType"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-gez v2, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "display. wrong data. mid:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", templateType:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "notiTag"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "display. wrong data. displayId:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "notiTag"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    invoke-direct {p0, p1, v1}, Lcom/sec/spp/push/notisvc/b/c;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/spp/push/notisvc/b/h;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "display. cannot get overlayView. not supported type"

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/notisvc/card/j;

    invoke-direct {v0}, Lcom/sec/spp/push/notisvc/card/j;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3, p2}, Lcom/sec/spp/push/notisvc/b/h;->a(Landroid/content/Context;Ljava/util/Map;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/b/h;->a()Lcom/sec/spp/push/notisvc/b/j;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/b/h;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-direct {p0, p1, v1, v0, v3}, Lcom/sec/spp/push/notisvc/b/c;->a(Landroid/content/Context;Ljava/lang/String;Lcom/sec/spp/push/notisvc/b/h;Lcom/sec/spp/push/notisvc/b/j;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v4}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v0, p1, v1}, Lcom/sec/spp/push/notisvc/b/h;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {v0, p1, v1}, Lcom/sec/spp/push/notisvc/b/h;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v1, 0x7d2

    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    const v1, 0x1000208

    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/16 v1, 0x33

    iput v1, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    invoke-virtual {v3, p1, v0, v4}, Lcom/sec/spp/push/notisvc/b/j;->a(Landroid/content/Context;Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/i;->a()Lcom/sec/spp/push/notisvc/b/i;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Lcom/sec/spp/push/notisvc/b/i;->a(ILcom/sec/spp/push/notisvc/b/h;)Z

    return-void
.end method

.method public a(Landroid/content/Context;I)Z
    .locals 2

    if-eqz p1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clear. Invalid parameter. id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {}, Lcom/sec/spp/push/notisvc/b/i;->a()Lcom/sec/spp/push/notisvc/b/i;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/spp/push/notisvc/b/i;->a(I)Lcom/sec/spp/push/notisvc/b/h;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "clear. not exists in overlayView(not displayed):"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/notisvc/b/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/notisvc/d/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/sec/spp/push/notisvc/b/h;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/spp/push/notisvc/b/i;->a()Lcom/sec/spp/push/notisvc/b/i;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/spp/push/notisvc/b/i;->b(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method
