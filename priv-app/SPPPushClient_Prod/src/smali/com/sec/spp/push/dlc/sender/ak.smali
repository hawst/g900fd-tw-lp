.class public Lcom/sec/spp/push/dlc/sender/ak;
.super Lcom/sec/spp/push/dlc/sender/ao;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/io/BufferedInputStream;

.field private c:Ljava/io/DataOutputStream;

.field private d:Ljava/io/OutputStream;

.field private e:[B

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/dlc/sender/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/dlc/sender/ak;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    const/16 v1, 0x12c

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/ao;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->b:Ljava/io/BufferedInputStream;

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->d:Ljava/io/OutputStream;

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/ak;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ak;->f:Ljava/lang/String;

    const/4 v3, 0x0

    if-ge v0, v1, :cond_0

    :goto_0
    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/ak;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljavax/net/ssl/HttpsURLConnection;)J
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->f:Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->e:[B

    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/ak;->e:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->b:Ljava/io/BufferedInputStream;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    invoke-virtual {p1}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->d:Ljava/io/OutputStream;

    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ak;->d:Ljava/io/OutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    const/16 v0, 0x800

    new-array v2, v0, [B

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/sec/spp/push/dlc/sender/ak;->b:Ljava/io/BufferedInputStream;

    invoke-virtual {v3, v2}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Total write ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/ak;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    int-to-long v0, v0

    return-wide v0

    :cond_0
    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    invoke-virtual {v4, v2, v1, v3}, Ljava/io/DataOutputStream;->write([BII)V

    iget-object v4, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->b:Ljava/io/BufferedInputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->b:Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->c:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    :cond_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ak;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/ak;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method
