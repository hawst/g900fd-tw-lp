.class public final enum Lcom/sec/spp/push/notisvc/registration/k;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum b:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum c:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum d:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum e:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum f:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum g:Lcom/sec/spp/push/notisvc/registration/k;

.field public static final enum h:Lcom/sec/spp/push/notisvc/registration/k;

.field private static final synthetic j:[Lcom/sec/spp/push/notisvc/registration/k;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "REGISTER"

    invoke-direct {v0, v1, v6, v4}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "DEREGISTER"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "AGREE"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "DISAGREE"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "REGISTER_INCOMPLETED"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "DEREGISTER_INCOMPLETED"

    const/4 v2, 0x5

    const/16 v3, -0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "AGREE_INCOMPLETED"

    const/4 v2, 0x6

    const/16 v3, 0x66

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    new-instance v0, Lcom/sec/spp/push/notisvc/registration/k;

    const-string v1, "DISAGREE_INCOMPLETED"

    const/4 v2, 0x7

    const/16 v3, -0x66

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/spp/push/notisvc/registration/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/spp/push/notisvc/registration/k;

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/notisvc/registration/k;->j:[Lcom/sec/spp/push/notisvc/registration/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/sec/spp/push/notisvc/registration/k;->i:I

    return-void
.end method

.method public static a(I)Lcom/sec/spp/push/notisvc/registration/k;
    .locals 3

    const-class v0, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {v0}, Lcom/sec/spp/push/notisvc/registration/k;->a()I

    move-result v2

    if-ne v2, p0, :cond_0

    goto :goto_0
.end method

.method public static a(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0
.end method

.method public static b(Lcom/sec/spp/push/notisvc/registration/k;)Lcom/sec/spp/push/notisvc/registration/k;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->c:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->g:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->d:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->h:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->a:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->e:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/sec/spp/push/notisvc/registration/k;->b:Lcom/sec/spp/push/notisvc/registration/k;

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/notisvc/registration/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->f:Lcom/sec/spp/push/notisvc/registration/k;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/notisvc/registration/k;
    .locals 1

    const-class v0, Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/notisvc/registration/k;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/notisvc/registration/k;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/notisvc/registration/k;->j:[Lcom/sec/spp/push/notisvc/registration/k;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/notisvc/registration/k;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/notisvc/registration/k;->i:I

    return v0
.end method
