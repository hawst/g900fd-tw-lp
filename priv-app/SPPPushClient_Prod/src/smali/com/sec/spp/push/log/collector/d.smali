.class public final enum Lcom/sec/spp/push/log/collector/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/sec/spp/push/log/collector/d;

.field public static final enum b:Lcom/sec/spp/push/log/collector/d;

.field public static final enum c:Lcom/sec/spp/push/log/collector/d;

.field public static final enum d:Lcom/sec/spp/push/log/collector/d;

.field public static final enum e:Lcom/sec/spp/push/log/collector/d;

.field public static final enum f:Lcom/sec/spp/push/log/collector/d;

.field public static final enum g:Lcom/sec/spp/push/log/collector/d;

.field public static final enum h:Lcom/sec/spp/push/log/collector/d;

.field public static final enum i:Lcom/sec/spp/push/log/collector/d;

.field public static final enum j:Lcom/sec/spp/push/log/collector/d;

.field public static final enum k:Lcom/sec/spp/push/log/collector/d;

.field public static final enum l:Lcom/sec/spp/push/log/collector/d;

.field public static final enum m:Lcom/sec/spp/push/log/collector/d;

.field public static final enum n:Lcom/sec/spp/push/log/collector/d;

.field public static final enum o:Lcom/sec/spp/push/log/collector/d;

.field public static final enum p:Lcom/sec/spp/push/log/collector/d;

.field public static final enum q:Lcom/sec/spp/push/log/collector/d;

.field private static final r:I

.field private static s:[Lcom/sec/spp/push/log/collector/d;

.field private static final synthetic t:[Lcom/sec/spp/push/log/collector/d;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_CONNECTION_TRY"

    invoke-direct {v0, v1, v3}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->a:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_DISCONNECT"

    invoke-direct {v0, v1, v4}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->b:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_CONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->c:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_NET_TYPE_CHANGED"

    invoke-direct {v0, v1, v6}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->d:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_NET_TIMEOUT"

    invoke-direct {v0, v1, v7}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->e:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_NET_RESET_BY_PEER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->f:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_NET_NOT_REACHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->g:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_SEND_PING_MSG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->h:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_RECV_PING_REPLY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->i:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "EXTRA_INT1"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "EXTRA_INT2"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "EXTRA_LONG1"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_SPP_REGISTERED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_SEND_LOG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->n:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_FOR_DEBUG"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->o:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_BOOT_COMPLETE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->p:Lcom/sec/spp/push/log/collector/d;

    new-instance v0, Lcom/sec/spp/push/log/collector/d;

    const-string v1, "ACTION_SERVICE_RESTART"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/log/collector/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->q:Lcom/sec/spp/push/log/collector/d;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/spp/push/log/collector/d;

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->a:Lcom/sec/spp/push/log/collector/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->b:Lcom/sec/spp/push/log/collector/d;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->c:Lcom/sec/spp/push/log/collector/d;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->d:Lcom/sec/spp/push/log/collector/d;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/spp/push/log/collector/d;->e:Lcom/sec/spp/push/log/collector/d;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->f:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->g:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->h:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->i:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->j:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->k:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->l:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->m:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->n:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->o:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->p:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->q:Lcom/sec/spp/push/log/collector/d;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->t:[Lcom/sec/spp/push/log/collector/d;

    const-class v0, Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    sput v0, Lcom/sec/spp/push/log/collector/d;->r:I

    sget v0, Lcom/sec/spp/push/log/collector/d;->r:I

    new-array v0, v0, [Lcom/sec/spp/push/log/collector/d;

    sput-object v0, Lcom/sec/spp/push/log/collector/d;->s:[Lcom/sec/spp/push/log/collector/d;

    const-class v0, Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/log/collector/d;

    sget-object v2, Lcom/sec/spp/push/log/collector/d;->s:[Lcom/sec/spp/push/log/collector/d;

    invoke-virtual {v0}, Lcom/sec/spp/push/log/collector/d;->ordinal()I

    move-result v3

    aput-object v0, v2, v3

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/sec/spp/push/log/collector/d;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/log/collector/d;->s:[Lcom/sec/spp/push/log/collector/d;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/spp/push/log/collector/d;
    .locals 1

    const-class v0, Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/log/collector/d;

    return-object v0
.end method

.method public static values()[Lcom/sec/spp/push/log/collector/d;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/log/collector/d;->t:[Lcom/sec/spp/push/log/collector/d;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/spp/push/log/collector/d;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
