.class public Lcom/sec/spp/push/e/a/d;
.super Lcom/sec/spp/push/e/a/a;


# static fields
.field private static final d:Ljava/lang/String;

.field private static final e:Lcom/sec/spp/push/e/a/d;

.field private static f:I


# instance fields
.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private final k:Ljava/lang/Object;

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/lang/Object;

.field private final n:Lcom/sec/spp/push/e/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/e/a/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v0, Lcom/sec/spp/push/e/a/d;

    invoke-direct {v0}, Lcom/sec/spp/push/e/a/d;-><init>()V

    sput-object v0, Lcom/sec/spp/push/e/a/d;->e:Lcom/sec/spp/push/e/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/a;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/d;->k:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/d;->l:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/d;->m:Ljava/lang/Object;

    new-instance v0, Lcom/sec/spp/push/e/a/e;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/e/a/e;-><init>(Lcom/sec/spp/push/e/a/d;)V

    iput-object v0, p0, Lcom/sec/spp/push/e/a/d;->n:Lcom/sec/spp/push/e/b/a;

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->f()V

    return-void
.end method

.method private a(Lcom/sec/spp/push/k;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/sec/spp/push/util/g;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "460"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "460"

    iget-object v2, p0, Lcom/sec/spp/push/e/a/d;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    const-string v0, "apchina-gld.push.samsungosp.com"

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    const-string v0, "gld.push.samsungosp.com"

    goto :goto_0

    :cond_1
    iput-object v0, p0, Lcom/sec/spp/push/e/a/d;->j:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/spp/push/k;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/spp/push/e/a/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/d;->y()V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/e/a/d;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/e/a/d;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/spp/push/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/spp/push/util/k;->b()I

    move-result v4

    if-ne v4, v1, :cond_6

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WIFI toggle to country ip. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    :cond_0
    iget-boolean v2, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WIFI toggle to original ip. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    :cond_3
    iget-boolean v2, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    if-eqz v2, :cond_4

    :goto_2
    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    sget-object v1, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "WIFI toggle from "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    goto :goto_1

    :cond_6
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Mobile toggle to country ip. "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Mobile toggle to original ip. "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Mobile toggle from "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.EXTRA_PROV_SUCCEED"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static declared-synchronized g()Lcom/sec/spp/push/e/a/d;
    .locals 3

    const-class v1, Lcom/sec/spp/push/e/a/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->e:Lcom/sec/spp/push/e/a/d;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v2, "ProvConnectionManager getInstance() return null"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->e:Lcom/sec/spp/push/e/a/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u()I
    .locals 1

    sget v0, Lcom/sec/spp/push/e/a/d;->f:I

    return v0
.end method

.method private v()V
    .locals 5

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v3, "prov: Disconnecting Prov Connection."

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v2, "ServiceStateMonitor. Push connection disconnect"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_1
    .catch Lcom/sec/spp/push/c/a; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "prov: Exception is occured : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/c/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "ServiceStateMonitor. Connection already Disconected"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->l()V

    goto :goto_1
.end method

.method private w()J
    .locals 6

    const-wide/16 v0, 0x0

    sget v2, Lcom/sec/spp/push/e/a/d;->f:I

    if-gtz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->g()Lcom/sec/spp/push/util/d;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/util/d;->b:Lcom/sec/spp/push/util/d;

    if-ne v0, v1, :cond_1

    const-wide/32 v0, 0xa4cb80

    goto :goto_0

    :cond_1
    sget v0, Lcom/sec/spp/push/e/a/d;->f:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_2

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    sget v2, Lcom/sec/spp/push/e/a/d;->f:I

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit16 v0, v0, 0x2710

    int-to-long v0, v0

    :goto_1
    sget-object v2, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "retry after "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v0, Lcom/sec/spp/push/e/a/d;->f:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_3

    const-wide/32 v0, 0x493e0

    sget v2, Lcom/sec/spp/push/e/a/d;->f:I

    add-int/lit8 v2, v2, -0x4

    int-to-long v2, v2

    mul-long/2addr v0, v2

    goto :goto_1

    :cond_3
    const-wide/32 v0, 0x124f80

    goto :goto_1
.end method

.method private x()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.receiver.PROVISIONING_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v4, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method private y()V
    .locals 2

    const-string v0, "gld.push.samsungosp.com"

    const-string v1, "apchina-gld.push.samsungosp.com"

    invoke-direct {p0, v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a()Lorg/jboss/netty/channel/ChannelPipelineFactory;
    .locals 1

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/spp/push/e/b/b;->a(Z)Lcom/sec/spp/push/e/b/b;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;IZ)V
    .locals 3

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/spp/push/e/a/a;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/e/a/d;->c:Lorg/jboss/netty/channel/Channel;

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-string v2, "message channel handler"

    invoke-interface {v0, v2}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/String;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/c;

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->q()Lcom/sec/spp/push/e/b/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/b/c;->a(Lcom/sec/spp/push/e/b/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/sec/spp/push/c/a;

    invoke-direct {v0}, Lcom/sec/spp/push/c/a;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[STATE] Set isProvisioning="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/sec/spp/push/e/a/d;->h:Z

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/spp/push/util/c;->c(J)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 3

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->m:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/e/a/d;->c:Lorg/jboss/netty/channel/Channel;

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->getPipeline()Lorg/jboss/netty/channel/ChannelPipeline;

    move-result-object v0

    const-string v2, "message channel handler"

    invoke-interface {v0, v2}, Lorg/jboss/netty/channel/ChannelPipeline;->get(Ljava/lang/String;)Lorg/jboss/netty/channel/ChannelHandler;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/e/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/e/b/c;->a(Lcom/sec/spp/push/e/b/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-super {p0}, Lcom/sec/spp/push/e/a/a;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lcom/sec/spp/push/c/a;

    invoke-direct {v0}, Lcom/sec/spp/push/c/a;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-super {p0}, Lcom/sec/spp/push/e/a/a;->b()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b(Z)V
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setProvAlarmSet. setProvAlarmSet="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p1, p0, Lcom/sec/spp/push/e/a/d;->g:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()V
    .locals 3

    const/4 v2, 0x0

    sput v2, Lcom/sec/spp/push/e/a/d;->f:I

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->g:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->k:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->h:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iput-boolean v2, p0, Lcom/sec/spp/push/e/a/d;->i:Z

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->c()Lorg/jboss/netty/channel/Channel;

    move-result-object v0

    invoke-interface {v0}, Lorg/jboss/netty/channel/Channel;->close()Lorg/jboss/netty/channel/ChannelFuture;

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public declared-synchronized h()V
    .locals 7

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doProvisioningWithHandling.  Try Count : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/sec/spp/push/e/a/d;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "doProvisioningWithHandling. context == null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "PV : Alarm is already set"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/util/c;->f()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "[ProvAlarm] Abnormal state."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->b(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "PV : Already Provisioning. return"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->b(Z)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/d;->v()V

    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/d;->w()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {}, Lcom/sec/spp/push/util/c;->a()Lcom/sec/spp/push/util/c;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/spp/push/util/c;->b(J)V

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.spp.push.receiver.PROVISIONING_ACTION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v5, v4, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sget-object v4, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[Prov] Set Alarm. Next Time : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    sget v0, Lcom/sec/spp/push/e/a/d;->f:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/spp/push/e/a/d;->f:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public i()V
    .locals 2

    invoke-static {}, Lcom/sec/spp/push/g/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, ""

    invoke-static {}, Lcom/sec/spp/push/g/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "[PROV] Device IMEI is null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/c/b;

    invoke-direct {v0}, Lcom/sec/spp/push/c/b;-><init>()V

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/c/b;->a(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v1, "[PROV] provision is progressing"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    invoke-static {}, Lcom/sec/spp/push/d/a/b;->b()Lcom/sec/spp/push/d/a/b;

    move-result-object v0

    new-instance v1, Lcom/sec/spp/push/e/a/f;

    invoke-direct {v1, p0}, Lcom/sec/spp/push/e/a/f;-><init>(Lcom/sec/spp/push/e/a/d;)V

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/d/a/b;->b(Lcom/sec/spp/push/util/h;)V

    sget-boolean v0, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v0, :cond_1

    const-string v0, "com.sec.spp.push.test.ACTION_PROVISION_REQUEST"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j()Z
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isProvisioning. isProvisioning="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->h:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->h:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->l()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    invoke-direct {p0}, Lcom/sec/spp/push/e/a/d;->x()V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->k:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->h:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public m()Z
    .locals 4

    iget-object v1, p0, Lcom/sec/spp/push/e/a/d;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[STATE] isProvAlarmSet="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/sec/spp/push/e/a/d;->g:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/sec/spp/push/e/a/d;->g:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized n()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->o()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->b(Z)V

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->p()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/e/a/d;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()Z
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/sec/spp/push/e/a/d;->d:Ljava/lang/String;

    const-string v2, "Failed to cancel prov control alarm due to empty context"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.spp.push.receiver.PROVISIONING_ACTION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized p()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/sec/spp/push/e/a/d;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public q()Lcom/sec/spp/push/e/b/a;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/e/a/d;->n:Lcom/sec/spp/push/e/b/a;

    return-object v0
.end method

.method public r()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/sec/spp/push/e/a/d;->b(Z)V

    sput v1, Lcom/sec/spp/push/e/a/d;->f:I

    invoke-virtual {p0}, Lcom/sec/spp/push/e/a/d;->h()V

    goto :goto_0
.end method

.method public s()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v1

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lcom/sec/spp/push/e/a/d;->a(Lcom/sec/spp/push/k;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Lcom/sec/spp/push/k;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/sec/spp/push/g/a;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/k;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
