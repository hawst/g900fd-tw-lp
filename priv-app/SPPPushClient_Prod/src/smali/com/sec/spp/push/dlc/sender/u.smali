.class Lcom/sec/spp/push/dlc/sender/u;
.super Lcom/sec/spp/push/dlc/sender/d;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ShowToast"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/SenderService;

.field private final b:Lcom/sec/spp/push/dlc/sender/ac;

.field private final c:Lcom/sec/spp/push/dlc/sender/ae;

.field private final d:Lcom/sec/spp/push/dlc/sender/ad;

.field private final e:Lcom/sec/spp/push/dlc/sender/af;

.field private final f:Lcom/sec/spp/push/dlc/sender/y;

.field private final g:Lcom/sec/spp/push/dlc/sender/ag;

.field private final h:Lcom/sec/spp/push/dlc/sender/ai;

.field private final i:Lcom/sec/spp/push/dlc/sender/z;

.field private final j:Lcom/sec/spp/push/dlc/sender/w;

.field private final k:Lcom/sec/spp/push/dlc/sender/aa;

.field private final l:Lcom/sec/spp/push/dlc/sender/x;

.field private final m:Lcom/sec/spp/push/dlc/sender/ab;

.field private final n:Lcom/sec/spp/push/dlc/sender/v;

.field private final o:Lcom/sec/spp/push/dlc/sender/ah;


# direct methods
.method constructor <init>(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/u;->a:Lcom/sec/spp/push/dlc/sender/SenderService;

    invoke-direct {p0, p2}, Lcom/sec/spp/push/dlc/sender/d;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ac;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ac;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->b:Lcom/sec/spp/push/dlc/sender/ac;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ae;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ae;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->c:Lcom/sec/spp/push/dlc/sender/ae;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ad;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ad;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->d:Lcom/sec/spp/push/dlc/sender/ad;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/af;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/af;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->e:Lcom/sec/spp/push/dlc/sender/af;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/y;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/y;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->f:Lcom/sec/spp/push/dlc/sender/y;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ag;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ag;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->g:Lcom/sec/spp/push/dlc/sender/ag;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ai;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ai;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->h:Lcom/sec/spp/push/dlc/sender/ai;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/z;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/z;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->i:Lcom/sec/spp/push/dlc/sender/z;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/w;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/w;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->j:Lcom/sec/spp/push/dlc/sender/w;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/aa;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/aa;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->k:Lcom/sec/spp/push/dlc/sender/aa;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/x;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/x;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->l:Lcom/sec/spp/push/dlc/sender/x;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ab;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ab;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->m:Lcom/sec/spp/push/dlc/sender/ab;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/v;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/v;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->n:Lcom/sec/spp/push/dlc/sender/v;

    new-instance v0, Lcom/sec/spp/push/dlc/sender/ah;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/dlc/sender/ah;-><init>(Lcom/sec/spp/push/dlc/sender/u;)V

    iput-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->o:Lcom/sec/spp/push/dlc/sender/ah;

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->b:Lcom/sec/spp/push/dlc/sender/ac;

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/c;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->k:Lcom/sec/spp/push/dlc/sender/aa;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/x;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->l:Lcom/sec/spp/push/dlc/sender/x;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/y;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->f:Lcom/sec/spp/push/dlc/sender/y;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->a:Lcom/sec/spp/push/dlc/sender/SenderService;

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->f:Lcom/sec/spp/push/dlc/sender/i;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->a:Lcom/sec/spp/push/dlc/sender/SenderService;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/q;->b(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ad;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->d:Lcom/sec/spp/push/dlc/sender/ad;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ae;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->c:Lcom/sec/spp/push/dlc/sender/ae;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ah;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->o:Lcom/sec/spp/push/dlc/sender/ah;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ai;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->h:Lcom/sec/spp/push/dlc/sender/ai;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ab;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->m:Lcom/sec/spp/push/dlc/sender/ab;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/v;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->n:Lcom/sec/spp/push/dlc/sender/v;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/af;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->e:Lcom/sec/spp/push/dlc/sender/af;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/z;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->i:Lcom/sec/spp/push/dlc/sender/z;

    return-object v0
.end method

.method static synthetic l(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ag;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->g:Lcom/sec/spp/push/dlc/sender/ag;

    return-object v0
.end method

.method static synthetic m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/u;->a:Lcom/sec/spp/push/dlc/sender/SenderService;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/m;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/m;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/an;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/an;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->d:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleHttpError - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/u;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/sec/spp/push/dlc/sender/an;->f:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleHttpError - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/dlc/sender/u;->c()V

    goto :goto_0
.end method
