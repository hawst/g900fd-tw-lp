.class public Lcom/sec/spp/push/PushClientApplication;
.super Landroid/app/Application;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/io/File;

.field private static c:Ljava/io/FileOutputStream;

.field private static d:I

.field private static g:Landroid/content/Context;


# instance fields
.field private final e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/sec/spp/push/PushClientApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    sput-object v1, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    sput-object v1, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    const/4 v0, 0x0

    sput v0, Lcom/sec/spp/push/PushClientApplication;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/spp/push/PushClientApplication;->e:Z

    return-void
.end method

.method public static a([B)V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0x500000

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    if-nez v0, :cond_3

    const-string v0, "mFos is null"

    sget-object v1, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    invoke-virtual {v0, p0}, Ljava/io/FileOutputStream;->write([B)V

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static b()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 4

    const/4 v0, 0x1

    invoke-static {v0, v0, v0, v0, v0}, Lcom/sec/spp/push/dlc/util/c;->a(ZZZZZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dlcLog"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/sec/spp/push/PushClientApplication;->d:I

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    sput-object v1, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "DLC application created"

    sget-object v1, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {}, Lcom/sec/spp/push/notisvc/d/a;->a()V

    invoke-static {v0, v0, v0, v0, v0}, Lcom/sec/spp/push/notisvc/d/a;->a(ZZZZZ)V

    return-void
.end method

.method private static e()V
    .locals 6

    sget v0, Lcom/sec/spp/push/PushClientApplication;->d:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/spp/push/PushClientApplication;->d:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dlcLog"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/sec/spp/push/PushClientApplication;->d:I

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x500000

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    new-instance v0, Ljava/io/FileOutputStream;

    sget-object v2, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    :goto_0
    sput-object v1, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    :goto_1
    return-void

    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    sget-object v2, Lcom/sec/spp/push/PushClientApplication;->b:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/spp/push/PushClientApplication;->f:Z

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/spp/push/PushClientApplication;->f:Z

    return v0
.end method

.method public onCreate()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "PushClientApplication.onCreate()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    iput-boolean v2, p0, Lcom/sec/spp/push/PushClientApplication;->f:Z

    invoke-static {}, Lcom/sec/spp/push/h/c;->b()V

    invoke-static {}, Lcom/sec/spp/push/util/o;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "Push log off : This is Ship build version"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/util/o;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/PushClientApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/k;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientApplication;->c()V

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientApplication;->d()V

    sput-boolean v3, Lcom/sec/spp/push/util/o;->h:Z

    :goto_0
    return-void

    :cond_0
    invoke-static {v3, v2, v2, v2, v2}, Lcom/sec/spp/push/dlc/util/c;->a(ZZZZZ)V

    invoke-static {v3, v2, v2, v2, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(ZZZZZ)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_2

    invoke-static {}, Lcom/sec/spp/push/util/o;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "Push log off : This is Release version"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->a(Landroid/content/Context;Z)V

    invoke-static {v3, v2, v2, v2, v2}, Lcom/sec/spp/push/dlc/util/c;->a(ZZZZZ)V

    invoke-static {v3, v2, v2, v2, v2}, Lcom/sec/spp/push/notisvc/d/a;->a(ZZZZZ)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->a:Ljava/lang/String;

    const-string v1, "Push log on : This isn\'t Ship build version"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->a(Landroid/content/Context;Z)V

    sput-boolean v3, Lcom/sec/spp/push/util/o;->h:Z

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/spp/push/util/o;->a(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientApplication;->c()V

    invoke-direct {p0}, Lcom/sec/spp/push/PushClientApplication;->d()V

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 1

    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/PushClientApplication;->c:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
