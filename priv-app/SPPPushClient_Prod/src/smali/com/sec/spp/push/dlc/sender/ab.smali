.class Lcom/sec/spp/push/dlc/sender/ab;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/u;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "RegiSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->a(Landroid/os/Message;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enter. Error - pkg["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->w:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v3, 0x0

    new-instance v4, Landroid/os/Messenger;

    iget-object v5, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v5}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static {v1, v2, v3, v4, v0}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Landroid/os/Messenger;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Message;)V
    .locals 7

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->af:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v1, v2, :cond_6

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_2

    const-string v0, "processMessage. Error - no packagename"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v1, p1, Landroid/os/Message;->arg1:I

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    if-ne v1, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Check Regi = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Check Regi = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->j:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    new-instance v0, Lcom/sec/spp/push/dlc/sender/s;

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/spp/push/dlc/util/f;->n(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/s;-><init>([Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "https://ew1.dms-gld.bigdata.ssp.samsung.com:80/1.0/getAppQuotaList"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/s;->a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I

    goto :goto_0

    :cond_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    if-ne v0, v1, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Check Regi = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Check Regi = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->b:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_4
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Check Regi = FAIL CODE:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    sget-object v2, Lcom/sec/spp/push/dlc/sender/i;->ad:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v1, v2, :cond_b

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->ao:Lcom/sec/spp/push/dlc/sender/i;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/i;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_7
    if-nez v0, :cond_8

    const-string v0, "processMessage. Error - no packagename"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_9
    iget v0, p1, Landroid/os/Message;->arg1:I

    sget-object v1, Lcom/sec/spp/push/dlc/sender/q;->a:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v1}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v1

    if-ne v0, v1, :cond_a

    iget v0, p1, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/spp/push/dlc/util/f;->B(Landroid/content/Context;)I

    move-result v1

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->f(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v1, v0, :cond_10

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-instance v1, Lcom/sec/spp/push/dlc/sender/RegiReply;

    invoke-direct {v1}, Lcom/sec/spp/push/dlc/sender/RegiReply;-><init>()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/sec/spp/push/dlc/sender/RegiReply;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/RegiReply;

    move-result-object v2

    if-nez v2, :cond_d

    const-string v0, "processMessage. Invalid RegiReply"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->e:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_c
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/RegiReply;->a()I

    move-result v0

    sget v1, Lcom/sec/spp/push/dlc/sender/RegiReply;->RESULT_GET_QUOTA_SUCCESS:I

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->x:Lcom/sec/spp/push/dlc/sender/i;

    new-instance v5, Landroid/os/Messenger;

    iget-object v6, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v6}, Lcom/sec/spp/push/dlc/sender/u;->b()Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-static/range {v0 .. v5}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;Landroid/os/Messenger;)V

    goto/16 :goto_0

    :cond_e
    const-string v0, "abnormal state"

    invoke-static {}, Lcom/sec/spp/push/dlc/sender/SenderService;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->g:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_f
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_10
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->A:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v1, v0, :cond_13

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    const-string v0, "processMessage. Error - no packagename"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->g:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_12
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_13
    sget-object v0, Lcom/sec/spp/push/dlc/sender/i;->B:Lcom/sec/spp/push/dlc/sender/i;

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->z(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    const-string v0, "processMessage. Error - no packagename"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0

    :cond_14
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/f;->A(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/q;->h:Lcom/sec/spp/push/dlc/sender/q;

    invoke-virtual {v2}, Lcom/sec/spp/push/dlc/sender/q;->a()I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/sec/spp/push/dlc/sender/SenderService;->a(Lcom/sec/spp/push/dlc/sender/SenderService;Ljava/lang/String;I)V

    :cond_15
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/ab;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0
.end method
