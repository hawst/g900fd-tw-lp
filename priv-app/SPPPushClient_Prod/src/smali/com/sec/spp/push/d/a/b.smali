.class public Lcom/sec/spp/push/d/a/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static e:Lcom/sec/spp/push/d/a/b;


# instance fields
.field private c:Landroid/os/HandlerThread;

.field private d:Landroid/os/Handler;

.field private f:Ljava/util/Map;

.field private g:I

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/util/List;

.field private j:Landroid/os/PowerManager$WakeLock;

.field private final k:Ljava/lang/Object;

.field private final l:Ljava/lang/Object;

.field private final m:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/k;->g()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/spp/push/d/a/b;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    new-instance v0, Lcom/sec/spp/push/d/a/c;

    invoke-direct {v0, p0}, Lcom/sec/spp/push/d/a/c;-><init>(Lcom/sec/spp/push/d/a/b;)V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->m:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ResponseHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/sec/spp/push/d/a/d;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/spp/push/d/a/d;-><init>(Lcom/sec/spp/push/d/a/b;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/spp/push/d/a/b;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/d/a/b;Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/b;->b(Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/spp/push/d/a/b;Ljava/util/ArrayList;I)Lcom/sec/spp/push/d/a/h;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/sec/spp/push/d/a/b;->a(Ljava/util/ArrayList;I)Lcom/sec/spp/push/d/a/h;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;I)Lcom/sec/spp/push/d/a/h;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "getAllElementsInResponseMap. responseMapLock==null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v3, v2

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v3

    :goto_2
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAllElementsInResponseMap. Exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/h;

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    if-ne p2, v1, :cond_1

    move-object v3, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method private a(ILcom/sec/spp/push/d/a/h;)V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "putElementInResponseMap. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "getAllElementsInResponseMap. responseMapLock==null"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getAllElementsInResponseMap. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private a(Ljava/lang/Integer;)Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    monitor-exit v1

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    const/4 v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized b()Lcom/sec/spp/push/d/a/b;
    .locals 2

    const-class v1, Lcom/sec/spp/push/d/a/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/spp/push/d/a/b;->e:Lcom/sec/spp/push/d/a/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/spp/push/d/a/b;

    invoke-direct {v0}, Lcom/sec/spp/push/d/a/b;-><init>()V

    sput-object v0, Lcom/sec/spp/push/d/a/b;->e:Lcom/sec/spp/push/d/a/b;

    :cond_0
    sget-object v0, Lcom/sec/spp/push/d/a/b;->e:Lcom/sec/spp/push/d/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Ljava/lang/Integer;)Lcom/sec/spp/push/d/a/h;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v2, "getElementInResponseMap. responseMapLock==null"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/h;

    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getElementInResponseMap. Exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private b(JLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/spp/push/d/a/b;->b(JLjava/lang/String;I)V

    return-void
.end method

.method private b(JLjava/lang/String;I)V
    .locals 7

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "scheduleRequestTimeOut. context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.REQUEST_TIME_OUT_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sppRequest://com.sec.spp.push/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/16 v3, 0xc

    if-ne p4, v3, :cond_1

    const-string v3, "heartbeat"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    invoke-static {v1, v5, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    sget-object v4, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "setAlarm(com.sec.spp.push.REQUEST_TIME_OUT_ACTION) AsyncID : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Next Time is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/spp/push/d/a/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->p()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "unscheduleAlarm. context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.spp.push.REQUEST_TIME_OUT_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sppRequest://com.sec.spp.push/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_1
    invoke-static {v1, v5, v2, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/spp/push/d/a/b;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/spp/push/d/a/b;)I
    .locals 1

    iget v0, p0, Lcom/sec/spp/push/d/a/b;->g:I

    return v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.spp.push.REQUEST_TIME_OUT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "sppRequest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private l()I
    .locals 5

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/Integer;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[AsyncId Test] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " already exist."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x1

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_0
    return v1
.end method

.method private m()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.test.ACTION_INITIALIZATION_TRY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.sec.spp.push.test.EXTRA_CURRENT_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private n()Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest;
    .locals 6

    const-string v0, "C"

    const-string v0, "P"

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setClientVersion(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    invoke-static {}, Lcom/sec/spp/push/g/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setDeviceId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "device.model="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&device.os=android"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&spp.ver="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/k;->x()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "&code=3004"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/spp/push/k;->d(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "&id=new"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v3, "&sim.mcc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&sim.mnc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&net.mcc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&net.mnc="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setDeviceInfo(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[ProvReq] deviceToken: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    :cond_2
    sget-object v2, Lcom/sec/spp/push/d/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setDeviceType(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "C"

    invoke-virtual {v1, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setMethodType(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    :goto_0
    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest;

    move-result-object v0

    return-object v0

    :cond_3
    const-string v0, "P"

    invoke-virtual {v1, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;->setMethodType(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest$Builder;

    goto :goto_0
.end method

.method private declared-synchronized o()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "acquireResponseWakeLock. acquire."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "acquireResponseWakeLock. already acquired. Ignore."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized p()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "releaseResponseWakeLock. release."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "releaseResponseWakeLock. already released. Ignore."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "releaseResponseWakeLock  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private q()V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "clearTaskInPendingQueue."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearTaskInPendingQueue. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->k()V

    invoke-virtual {p0}, Lcom/sec/spp/push/d/a/b;->d()V

    return-void
.end method

.method public a(I)V
    .locals 5

    :try_start_0
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->q()V

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/a;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cancelTaskInPendingQueue. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    :try_start_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/a;

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cancelTaskInPendingQueue. taskType:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", appId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/sec/spp/push/d/a/a;->a(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1
.end method

.method public a(IILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-static {v0, v1, p1, p2, p3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/spp/push/d/a/b;->b(JLjava/lang/String;)V

    return-void
.end method

.method public a(JLjava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/spp/push/d/a/b;->b(JLjava/lang/String;I)V

    return-void
.end method

.method public a(Landroid/os/Message;Z)V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dispatchMessageRequest. bFirst="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->o()V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dispatchMessageResponse. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->p()V

    goto :goto_0
.end method

.method public a(Lcom/google/protobuf/MessageLite;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "responseHandler destroyed(null). return."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    goto :goto_0
.end method

.method public a(Lcom/sec/spp/push/d/a/a;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/spp/push/d/a/b;->b(Lcom/sec/spp/push/d/a/a;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addTaskToPendingQueue. taskType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addTaskToPendingQueue. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addTaskToPendingQueue. do not add. already exist. taskType:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", appId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method public a(Lcom/sec/spp/push/util/h;)V
    .locals 11

    const/4 v3, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[Init.] context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "[Init.] isInitialized : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", isInitializing : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/spp/push/e/a/g;->j()Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x66

    invoke-virtual {p0, v2, p1, v0}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;Lcom/sec/spp/push/util/h;I)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->b()V
    :try_end_0
    .catch Lcom/sec/spp/push/c/a; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/sec/spp/push/e/a/g;->a(Z)V

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[Init.] Need Provision"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/d;->h()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v3

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[Init.] asyncId ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/spp/push/d/a/h;

    invoke-direct {v1, p0, p1, v2}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    sget-object v4, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[Init.] listenerInfo ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v1}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    :try_start_1
    invoke-static {}, Lcom/sec/spp/push/b/a;->a()Lcom/sec/spp/push/b/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    :try_start_2
    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->c()Lcom/sec/spp/push/a/a;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    sget-object v5, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "pkg = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ecData1: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ecData2 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ecData3 :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->a()Ljava/lang/String;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v5, v6, v7}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/spp/push/a/a;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/a/a;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setDv(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    :cond_4
    sget-object v7, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "regId : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/a/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ", Data1 : "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ", Data2 :"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ", Data3 :"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLatitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLongitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v0

    if-ne v0, v10, :cond_8

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setConnectivity(I)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->h(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_3
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    :cond_6
    :goto_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLocale(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-static {}, Lcom/sec/spp/push/util/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setMcc(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-virtual {v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest;

    move-result-object v0

    sget-boolean v1, Lcom/sec/spp/push/util/o;->h:Z

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->m()V

    :cond_7
    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v0, v3, v2}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto/16 :goto_0

    :cond_8
    :try_start_3
    invoke-static {}, Lcom/sec/spp/push/util/k;->d()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setConnectivity(I)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_5
    :try_start_4
    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception occured when building LData : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->h(Z)V

    const-wide v5, 0x408f400000000000L    # 1000.0

    invoke-virtual {v4, v5, v6}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLatitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    const-wide v5, 0x408f400000000000L    # 1000.0

    invoke-virtual {v4, v5, v6}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLongitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    goto :goto_4

    :cond_9
    const-wide v5, 0x408f400000000000L    # 1000.0

    :try_start_5
    invoke-virtual {v4, v5, v6}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLatitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    const-wide v5, 0x408f400000000000L    # 1000.0

    invoke-virtual {v4, v5, v6}, Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;->setLongitude(D)Lcom/sec/pns/msg/frontend/MsgFrontend$InitRequest$Builder;

    invoke-static {}, Lcom/sec/spp/push/k;->a()Lcom/sec/spp/push/k;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/spp/push/k;->h(Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/sec/spp/push/b/a;->b()V

    :cond_a
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catch_2
    move-exception v1

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0}, Lcom/sec/spp/push/d/a/b;->a(Ljava/util/ArrayList;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Will be unscheduled asyncId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/spp/push/d/a/b;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/spp/push/d/a/b;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lcom/sec/spp/push/util/h;I)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "processInternalErrorFromPendingTask."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/d/a/h;

    invoke-direct {v0, p0, p2, p1}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-static {v1, v2, p3, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V
    .locals 7

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendRegistrationReq(). DEVICE_TYPE_MOBILE="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/d/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendRegistrationReq. context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendRegistrationReq - Connection isn\'t initialized, add reg task to pending queue"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/d/a/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/d/a/e;-><init>(Lcom/sec/spp/push/d/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/a;)V

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendRegistrationReq - returning"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v2, "sendRegistrationReq"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v1

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "registration(). asyncId ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "registration(). appId ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/spp/push/d/a/h;

    invoke-direct {v2, p0, p4, p1}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    invoke-virtual {v2, p1}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->setAppId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    if-eqz p2, :cond_2

    invoke-virtual {v2, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->setUserData(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setMumId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;

    :cond_3
    invoke-virtual {v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$RegistrationRequest;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V
    .locals 7

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[SendNotiAck] Start"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendNotificationAck. context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[SendNotiAck] Initialized : false. Add Pending Queue"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/d/a/g;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/spp/push/d/a/g;-><init>(Lcom/sec/spp/push/d/a/b;Ljava/lang/String;ZLjava/util/List;ILcom/sec/spp/push/util/h;)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/a;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/spp/push/PushClientService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    if-eqz p2, :cond_4

    new-instance v1, Lcom/sec/spp/push/d/a/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v0, 0x0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, p0, v4, v0}, Lcom/sec/spp/push/d/a/i;-><init>(Lcom/sec/spp/push/d/a/b;ILjava/lang/String;)V

    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/sec/spp/push/d/a/h;

    invoke-direct {v0, p0, p5, p1}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    move-result-object v1

    invoke-static {v2}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v2, "[SendNotiAck] ackIds is empty"

    invoke-static {v0, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1, p4}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->setAckType(I)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    invoke-virtual {v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[SendNotiAck] ackType : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0xb

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Lcom/sec/spp/push/d/a/i;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v4, ""

    invoke-direct {v0, p0, v1, v4}, Lcom/sec/spp/push/d/a/i;-><init>(Lcom/sec/spp/push/d/a/b;ILjava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v4, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[SendNotiAck] ackId:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;->addNotiIds(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$NotiAcks$Builder;

    goto :goto_2
.end method

.method public a(Z)V
    .locals 3

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processNetworkNotAvailableResponse. isProvisioning="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    goto :goto_0
.end method

.method public a(ZLcom/sec/spp/push/util/h;)V
    .locals 6

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[Send Ping] context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[Send Ping] Initialized = false"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/spp/push/e/a/g;->h()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v1

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[Send Ping] asyncId ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/spp/push/d/a/h;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p2, v3}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->setCreatedTime(J)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    if-eqz p1, :cond_2

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->g(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->setInterval(I)Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;

    sget-object v3, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[Send Ping] Server Ping : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->g(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$PingRequest;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeElementInResponseMap. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lcom/sec/spp/push/util/h;)V
    .locals 4

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "[ProvReq] Already Provisioned"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/e/a/d;->g()Lcom/sec/spp/push/e/a/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/e/a/d;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v0

    iput v0, p0, Lcom/sec/spp/push/d/a/b;->g:I

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ProvReq] asyncId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/spp/push/d/a/b;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/d/a/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/spp/push/d/a/b;->g:I

    invoke-direct {p0, v1, v0}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->n()Lcom/sec/pns/msg/frontend/MsgFrontend$ProvisionRequest;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v1

    iget v2, p0, Lcom/sec/spp/push/d/a/b;->g:I

    const/4 v3, 0x5

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V
    .locals 8

    const-wide/16 v6, 0x0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendDeregistrationReq(). DEVICE_TYPE_MOBILE="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/spp/push/d/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendDeregistrationReq. context is destroyed. So do not anything."

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/sec/spp/push/e/a/g;->g()Lcom/sec/spp/push/e/a/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/spp/push/e/a/g;->i()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendDeregistrationReq - Connection isn\'t initialized, add dereg task to pending queue"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/spp/push/d/a/f;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/spp/push/d/a/f;-><init>(Lcom/sec/spp/push/d/a/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/spp/push/util/h;)V

    invoke-virtual {p0, v0}, Lcom/sec/spp/push/d/a/b;->a(Lcom/sec/spp/push/d/a/a;)V

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "sendDeregistrationReq - returning"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v2, "sendDeregistrationReq"

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->l()I

    move-result v1

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "deregistration(). asyncId ="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_2

    new-instance v2, Lcom/sec/spp/push/d/a/h;

    invoke-direct {v2, p0, p4, p1}, Lcom/sec/spp/push/d/a/h;-><init>(Lcom/sec/spp/push/d/a/b;Lcom/sec/spp/push/util/h;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/sec/spp/push/d/a/b;->a(ILcom/sec/spp/push/d/a/h;)V

    :cond_2
    invoke-static {}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;->newBuilder()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setAsyncId(I)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    invoke-static {v0}, Lcom/sec/spp/push/g/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setDeviceToken(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    invoke-static {}, Lcom/sec/spp/push/h/c;->a()Lcom/sec/spp/push/h/c;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/sec/spp/push/h/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "deregistration(). regId ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setRegId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {v2, p2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setUserData(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v6

    if-eqz v3, :cond_6

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setMumId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;

    :cond_5
    :goto_1
    invoke-virtual {v2}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->build()Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest;

    move-result-object v0

    invoke-static {}, Lcom/sec/spp/push/e/b/g;->e()Lcom/sec/spp/push/e/b/g;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/spp/push/e/b/g;->a(Lcom/google/protobuf/MessageLite;II)V

    goto/16 :goto_0

    :cond_6
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {p3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v6

    if-nez v3, :cond_5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, "-"

    invoke-direct {v3, v0, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    sget-object v4, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Token count : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x2

    if-ne v0, v4, :cond_5

    const/4 v0, 0x0

    :goto_2
    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-nez v4, :cond_7

    sget-object v3, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Token : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "0"

    invoke-virtual {v2, v0}, Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;->setMumId(Ljava/lang/String;)Lcom/sec/pns/msg/frontend/MsgFrontend$DeregistrationRequest$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v3, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    :try_start_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Token : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public b(Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "processNotiAckReachedResponse. reached="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-static {v0, v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-static {v0, v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/sec/spp/push/d/a/b;->a(Landroid/os/Message;Z)V

    goto :goto_0
.end method

.method public b(Lcom/sec/spp/push/d/a/a;)Z
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v3

    invoke-interface {p1}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v4

    :try_start_0
    iget-object v5, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "isExistTaskInPendingQueue. isExistTask:"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", taskType:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", appId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/a;

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v7

    if-ne v7, v3, :cond_1

    const/4 v7, 0x3

    if-eq v3, v7, :cond_4

    const/4 v7, 0x4

    if-ne v3, v7, :cond_5

    :cond_4
    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_5
    const/4 v0, 0x6

    if-ne v3, v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isExistTaskInPendingQueue. Exception : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_2
.end method

.method public c()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->h:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/i;

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getFirstNotiAck="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/sec/spp/push/d/a/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/sec/spp/push/d/a/i;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "getFirstNotiAck="

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    goto :goto_0
.end method

.method public declared-synchronized d()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "PrepareResponseWakeLock : Failed"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-class v2, Lcom/sec/spp/push/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->j:Landroid/os/PowerManager$WakeLock;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "MessageController. destroy()"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/d/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->q()V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/spp/push/d/a/b;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/spp/push/d/a/b;->i()V

    :try_start_0
    invoke-static {}, Lcom/sec/spp/push/PushClientApplication;->b()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/sec/spp/push/d/a/b;->p()V

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v2, p0, Lcom/sec/spp/push/d/a/b;->c:Landroid/os/HandlerThread;

    iput-object v2, p0, Lcom/sec/spp/push/d/a/b;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const-class v1, Lcom/sec/spp/push/d/a/b;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/sec/spp/push/d/a/b;->e:Lcom/sec/spp/push/d/a/b;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/spp/push/d/a/b;->e:Lcom/sec/spp/push/d/a/b;

    :cond_1
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public f()V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/spp/push/d/a/a;

    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    :try_start_2
    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executeNextTaskInPendingQueue. taskType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", appId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/sec/spp/push/d/a/a;->a()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "executeNextTaskInPendingQueue. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g()Z
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->l:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    monitor-exit v2

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v2

    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isTasksInPendingQueue. Exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    monitor-exit v2

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v2

    :goto_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isElementsInResponseMap. Exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public i()V
    .locals 4

    sget-object v0, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    const-string v1, "clearElementsInResponseMap. remove all asyncId"

    invoke-static {v0, v1}, Lcom/sec/spp/push/util/o;->b(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/sec/spp/push/d/a/b;->k:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/d/a/b;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/sec/spp/push/d/a/b;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearElementsInResponseMap. Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/spp/push/util/o;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
