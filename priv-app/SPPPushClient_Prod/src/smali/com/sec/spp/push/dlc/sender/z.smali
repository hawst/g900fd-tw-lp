.class Lcom/sec/spp/push/dlc/sender/z;
.super Lcom/sec/spp/push/dlc/sender/c;


# instance fields
.field final synthetic a:Lcom/sec/spp/push/dlc/sender/u;


# direct methods
.method public constructor <init>(Lcom/sec/spp/push/dlc/sender/u;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    const-string v0, "PvUrgentSt"

    invoke-direct {p0, v0}, Lcom/sec/spp/push/dlc/sender/c;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/SenderService;->g(Lcom/sec/spp/push/dlc/sender/SenderService;)Lcom/sec/spp/push/dlc/sender/n;

    move-result-object v0

    const-string v1, "https://ew1.dms-gld.bigdata.ssp.samsung.com:80/1.0/provision"

    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-virtual {v0, v1, v2}, Lcom/sec/spp/push/dlc/sender/n;->a(Ljava/lang/String;Lcom/sec/spp/push/dlc/sender/d;)I

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->a(Landroid/os/Message;)V

    return-void
.end method

.method public b(Landroid/os/Message;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/sec/spp/push/dlc/sender/c;->b(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/i;->a(I)Lcom/sec/spp/push/dlc/sender/i;

    move-result-object v0

    sget-object v1, Lcom/sec/spp/push/dlc/sender/i;->z:Lcom/sec/spp/push/dlc/sender/i;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/m;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/m;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "pvReply is null"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/z;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/m;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/z;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/spp/push/dlc/sender/m;->k()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "Result Code is NULL"

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/z;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/an;->a(Ljava/lang/String;)Lcom/sec/spp/push/dlc/sender/an;

    move-result-object v1

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v1, v2, :cond_3

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v1, v2, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/spp/push/dlc/sender/z;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/spp/push/dlc/util/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->a(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/aa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v2}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v2

    sget-object v3, Lcom/sec/spp/push/dlc/sender/i;->v:Lcom/sec/spp/push/dlc/sender/i;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/spp/push/dlc/db/DbService;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/i;Landroid/os/Parcelable;)V

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-eq v1, v2, :cond_4

    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->b:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v1, v2, :cond_6

    :cond_4
    sget-object v2, Lcom/sec/spp/push/dlc/sender/an;->a:Lcom/sec/spp/push/dlc/sender/an;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/spp/push/dlc/util/f;->a(Landroid/content/Context;Lcom/sec/spp/push/dlc/sender/m;)V

    :cond_5
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v0}, Lcom/sec/spp/push/dlc/sender/u;->m(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/SenderService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/spp/push/dlc/util/q;->a(Landroid/content/Context;)V

    :cond_6
    iget-object v0, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    iget-object v1, p0, Lcom/sec/spp/push/dlc/sender/z;->a:Lcom/sec/spp/push/dlc/sender/u;

    invoke-static {v1}, Lcom/sec/spp/push/dlc/sender/u;->l(Lcom/sec/spp/push/dlc/sender/u;)Lcom/sec/spp/push/dlc/sender/ag;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/spp/push/dlc/sender/u;->c(Lcom/sec/spp/push/dlc/sender/c;)V

    goto/16 :goto_0
.end method
